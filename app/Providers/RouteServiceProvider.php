<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Route;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * The path to the "home" route for your application.
     *
     * @var string
     */
    public const HOME = '/';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        //

        parent::boot();
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $this->mapApiRoutes();

        $this->mapWebRoutes();

        //
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapWebRoutes()
    {
        $this->mapWebAdministradorRoutes();   //administracion
        $this->mapWebEmpleadoRoutes();      //gestor de personal
        $this->mapWebPublicRoutes();        //rutas publicas
    }

      protected $namespaceAdministracion = 'App\Http\Controllers\Administracion';

      protected function mapWebAdministradorRoutes()
      {
          Route::middleware('auth')
               ->namespace($this->namespaceAdministracion)
               ->prefix('admin')
               ->group(base_path('routes/web.php'));
      }

      protected $namespaceEmpleados = 'App\Http\Controllers\Empleados';

      protected function mapWebEmpleadoRoutes()
      {
          Route::middleware('auth')
               ->namespace($this->namespaceEmpleados)
               ->prefix('empleado')
               ->group(base_path('routes/web.php'));
      }

        protected function mapWebPublicRoutes()
      {
          Route::middleware('web')
               ->namespace($this->namespace)
               ->group(base_path('routes/web.php'));
      } 

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiRoutes()
    {
        Route::prefix('api')
             ->middleware('api')
             ->namespace($this->namespace)
             ->group(base_path('routes/api.php'));
    }
}
