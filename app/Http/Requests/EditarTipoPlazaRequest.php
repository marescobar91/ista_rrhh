<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class EditarTipoPlazaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
      public function rules()
    {
        return [
            //
            'tipo_plaza' => ['required', 'min:8', 'max:255', Rule::unique('emp_tipo_plaza')->ignore($this->id, 'id_tipo_plaza')],
            
        ];
    }


    public function messages()
    {
        return [

        'tipo_plaza.required'   => 'El :attribute es obligatorio.',
        'tipo_plaza.min'        => 'El :attribute debe contener mas de ocho letra.',
        'tipo_plaza.max'        => 'El :attribute debe contener max 255 letras.',
        

        ];
    }

    
    public function attributes()
    {
        return [

            'tipo_plaza'        => 'Nombre del Tipo de Plaza',
           

        ];

    }
}
