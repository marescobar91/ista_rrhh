<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EmpleadoInformacionFinancieraRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'banco'                 => 'required',
            'cuenta_banco'          => 'required|max:25|unique:emp_empleado,cuenta_banco',
            'tipoPlaza'             => 'required',
            'numero_partida'        => 'required',
            'numero_acuerdo'        => 'required',
            'numero_subpartida'     => 'required',
            'cifra_presupuestaria'  => 'required',
        ];
    }
    public function messages(){
        return [
            'banco.required'                    => 'El :attribute es obligatorio',
            'cuenta_banco.required'             => 'La :attribute es obligatoria',
            'cuenta_banco.max'                  => 'La :attribute no debe tener mas de 25 numeros',
            'cuenta_banco.unique'               => 'La :attribute ya ha sido registrada',
            'tipoPlaza.required'                => 'El :attribute es obligatorio',
            'numero_partida.required'           => 'El :attribute es obligatorio',
            'numero_acuerdo.required'           => 'El :attribute es obligatorio',
            'numero_subpartida.required'        => 'El :attribute es obligatorio',
            'cifra_presupuestaria.required'     => 'La :attribute es obligatoria',
        ];
    }
    public function attributes(){
        return [
            'banco'                 => 'Nombre del Banco',
            'cuenta_banco'          => 'Cuenta de Banco',
            'tipoPlaza'             => 'Tipo de Contrato',
            'numero_partida'        => 'Numero de Partida',
            'numero_acuerdo'        => 'Numero de Acuerdo',
            'numero_subpartida'     => 'Numero de Subpartida',
            'cifra_presupuestaria'  => 'Cifra Presupuestaria',
        ];
    }
}
