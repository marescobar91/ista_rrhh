<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class EditarCargoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
     public function rules()
    {
        return [
            //
            'nombre_cargo' => ['required', 'min:8', 'max:255', Rule::unique('emp_cargo', 'cargo')->ignore($this->id, 'id_cargo')],
            
        ];
    }


    public function messages()
    {
        return [

        'nombre_cargo.required'   => 'El :attribute es obligatorio.',
        'nombre_cargo.min'        => 'El :attribute debe contener mas de ocho letra.',
        'nombre_cargo.max'        => 'El :attribute debe contener max 255 letras.',
        

        ];
    }

    
    public function attributes()
    {
        return [

            'nombre_cargo'        => 'Nombre del Cargo',
           

        ];

    }
}
