<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class InformacionPersonalRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'codigo_empleado'   =>  'required|max:7|unique:emp_empleado,codigo_empleado',
        ];

    }
    public function messages(){
        return [
            'codigo_empleado.required'     => 'El :attribute es obligatorio',
            'codigo_empleado.max'          => 'El :attribute debe contener maximo 7 caracteres',
            'codigo_empleado.unique'       => 'El :attribute ya ha sido registrado',
        ];
    }
    public function attributes(){
        return [
            'codigo_empleado'   => 'Codigo de Empleado'
        ];

    }
}
