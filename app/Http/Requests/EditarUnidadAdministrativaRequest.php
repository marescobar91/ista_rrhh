<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class EditarUnidadAdministrativaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
     public function rules()
    {
        return [
            //
            'unidad_administrativa' => ['required', 'min:8', 'max:255', Rule::unique('emp_unidad_administrativa')->ignore($this->id, 'id_unidad_administrativa')],
            
        ];
    }


    public function messages()
    {
        return [

        'unidad_administrativa.required'   => 'El :attribute es obligatorio.',
        'unidad_administrativa.min'        => 'El :attribute debe contener mas de ocho letra.',
        'unidad_administrativa.max'        => 'El :attribute debe contener max 255 letras.',
        
    

        ];
    }

    
    public function attributes()
    {
        return [

            'unidad_administrativa'        => 'Nombre de la Unidad Administrativa',
           

        ];

    }
}
