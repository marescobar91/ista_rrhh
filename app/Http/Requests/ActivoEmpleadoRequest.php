<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ActivoEmpleadoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'fecha_reingreso'   =>  'required',
        ];
    }

      public function messages()
    {
        return [

        'fecha_reingreso.required'   => 'La :attribute es obligatorio.',

        ];
    }

    
    public function attributes()
    {
        return [

            'fecha_reingreso'        => 'Fecha de Reingreso',
           

        ];

    }


}
