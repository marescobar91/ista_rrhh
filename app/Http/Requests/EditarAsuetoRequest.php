<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class EditarAsuetoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
   public function rules()
    {
        return [
            
            'fecha' => ['required', Rule::unique('mar_asueto')->ignore($this->id, 'id_asueto')],
        ];
    }
     public function messages()
    {
        return [

        'fecha.required'                  => 'El :attribute es obligatorio.', 
        'fecha.unique'                   => 'El :attribute ya ha sido registrado',    

        ];
    }

    
    public function attributes()
    {
        return [

          
            'fecha'             => 'Fecha del Asueto',
           

        ];

    }
}
