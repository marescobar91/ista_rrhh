<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class EditarCentroRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
     public function rules()
    {
        return [
            //
            'centro' => ['required', 'min:5', 'max:255', Rule::unique('emp_centro')->ignore($this->id, 'id_centro')],
            
        ];
    }


    public function messages()
    {
        return [

        'centro.required'   => 'El :attribute es obligatorio.',
        'centro.min'        => 'El :attribute debe contener mas de cinco letra.',
        'centro.max'        => 'El :attribute debe contener max 255 letras.',
        
    

        ];
    }

    
    public function attributes()
    {
        return [

            'centro'        => 'Nombre del Centro',
           

        ];

    }
}
