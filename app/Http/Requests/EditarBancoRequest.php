<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class EditarBancoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        'nombre_banco' => ['required', 'min:8', 'max:255', Rule::unique('emp_banco')->ignore($this->id, 'id_banco')],
            
        ];
    }


    public function messages()
    {
        return [

        'nombre_banco.required'   => 'El :attribute es obligatorio.',
        'nombre_banco.min'        => 'El :attribute debe contener mas de ocho letra.',
        'nombre_banco.max'        => 'El :attribute debe contener max 255 letras.',
        
    

        ];
    }

    
    public function attributes()
    {
        return [

            'nombre_banco'        => 'Nombre del Banco',
           

        ];

    }
}
