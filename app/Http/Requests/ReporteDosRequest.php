<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ReporteDosRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    
      public function authorize()
    {
        return true;
    }
    public function rules()
    {
        return [
             'reporteDos' => 'required',
        ];
    }

     public function messages()
    {
        return [

        'reporteDos.required'   => 'La :attribute es obligatorio. Seleccione al menos uno.',

        ];
    }

    
    public function attributes()
    {
        return [

            'reporteDos'        => 'selección',

        ];

    }
}
