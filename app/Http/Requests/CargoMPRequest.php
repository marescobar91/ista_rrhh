<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CargoMPRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'cargo_mp' => 'required|min:8|max:255|unique:emp_cargo_mp,cargo_mp',
            
        ];
    }


    public function messages()
    {
        return [

        'cargo_mp.required'   => 'El :attribute es obligatorio.',
        'cargo_mp.min'        => 'El :attribute debe contener mas de ocho letra.',
        'cargo_mp.max'        => 'El :attribute debe contener max 255 letras.',
        'cargo_mp.unique'     => 'El :attribute ya ha sido registrado',
    

        ];
    }

    
    public function attributes()
    {
        return [

            'cargo_mp'        => 'Nombre del Cargo',
           

        ];

    }
}
