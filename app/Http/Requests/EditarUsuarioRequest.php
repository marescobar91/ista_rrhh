<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use App\Usuario;

class EditarUsuarioRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'nombre_usuario' => ['required', 'min:3', 'max:255', Rule::unique('admin_usuario')->ignore($this->id, 'id_usuario')],
            'email' => ['required', 'email', Rule::unique('admin_usuario')->ignore($this->id, 'id_usuario')],
              'rol'       =>  'required',
        ];
    }

    public function messages()
    {
        return [

        'nombre_usuario.required'   => 'El :attribute es obligatorio.',
        'nombre_usuario.min'        => 'El :attribute debe contener mas de tres letra.',
        'nombre_usuario.max'        => 'El :attribute debe contener max 255 letras.',

        'email.required'           => 'El :attribute es obligatorio.',
        'email.min'                => 'El :attribute debe contener mas de tres letra.',
        'email.max'                => 'El :attribute debe contener max 255 letras.', 
        'rol.required'             => 'El :attribute es obligatorio.',
       

        ];
    }

    
    public function attributes()
    {
        return [

            'nombre_usuario'        => 'Nombre de Usuario',
            'email'                 =>  'Correo',
            'rol'                   =>  'Rol'
           
        ];

    }
}
