<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EmpleadoDocumentoPersonalRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'tipo_documento'   =>  'required',
            'numero_documento' => 'required|max:15|unique:emp_empleado,numero_documento',
            'nit' => 'required|max:18|unique:emp_empleado,nit',
            'isss' => 'required|max:9|unique:emp_empleado,isss',
            'nivelAcademico' => 'required',
        ];
    }
    public function messages(){
        return [
            'tipo_documento.required'   => 'El :attribute es obligatorio',
            'numero_documento.required' => 'El :attribute es obligatorio',
            'numero_documento.max'      => 'El :attribute debe tener maximo 15 numeros',
            'numero_documento.unique'   => 'El :attribute ya ha sido registrado',
            'nit.required'              => 'El :attribute es obligatorio',
            'nit.max'                   => 'El :attribute debe tener maximo 18 numeros',
            'nit.unique'                => 'El :attribute ya ha sido registrado',
            'isss.required'             => 'El :attribute es obligatorio',
            'isss.numeric'              => 'El :attribute debe contener unicamente numeros',
            'isss.max'                  => 'El :attribute debe contener maximo 9 caracteres',
            'isss.unique'               => 'El :attribute ya ha sido registrado',
            'nivelAcademico.required'   => 'El :attribute es obligatorio',
        ];
    }
    public function attributes(){
        return [
            'tipo_documento'        => 'Tipo de Documento',
            'numero_documento'      => 'Numero de Documento',
            'nit'                   => 'Numero de NIT',
            'isss'                  => 'Numero de ISSS',
            'nivelAcademico'        => 'Numero de ISSS',
        ];
    }
}
