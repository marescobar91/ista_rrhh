<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class InactivoEmpleadoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'numero_acuerdo_retiro' =>  'required|max:30',
            'fecha_retiro'          =>  'required',
            'motivo_retiro'         =>  'required|max:255',
        ];
    }

    public function messages()
    {
        return [

        'numero_acuerdo_retiro.required'   => 'El :attribute es obligatorio.',
        'numero_acuerdo_retiro.max'        => 'El :attribute debe contener max 255 letras.',

        'fecha_retiro.required'             => 'La :attribute es obligatorio.',
        'motivo_retiro.required'            => 'El :attribute es obligatorio.',
       

        ];
    }

    
    public function attributes()
    {
        return [

            'numero_acuerdo_retiro'        => 'Numero de Acuerdo de Retiro',
            'fecha_retiro'                 => 'Fecha de Retiro',
            'motivo_retiro'                => 'Motivo de Retiro',
           

        ];

    }
}
