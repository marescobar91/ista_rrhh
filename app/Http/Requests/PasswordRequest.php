<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use App\Usuario;


class PasswordRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'password'           => ['required','string','min:8'],
            'newpassword'        => ['required','same:password','min:8'],
        ];
    }

        public function messages()
    {
        return [

        'password.required'         => 'La :attribute no funciona.',
        'password.string'           => 'La :attribute debe ser de tipo String.',
        'password.min:8'            => 'La :attribute debe contener minimo 8 caracteres',

        'newpassword.required'         => 'La :attribute debe repetir la contraseña',
        'newpassword.same:password'    => 'La :attribute Las contraseñas no coinciden.',
        'newpassword.min:8'            => 'La :attribute debe contener minimo 8 caracteres',

        ];
    }

    
    public function attributes()
    {
        return [

            'password'              => 'Nueva Contraseña',
            'newpassword'           => 'Confirmar Contraseña',
        ];

    }
}
