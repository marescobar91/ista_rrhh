<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DocumentosPersonalesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'numero_documento'  => 'required|max:15|unique:emp_empleado,numero_documento',
            'isss'              => 'required|max:9|unique:emp_empleado,isss',
            'nit'               => 'required|max:18|unique:emp_empleado,nit',
            'nup'               => 'required|max:15|unique:emp_empleado,nup',
        ];

    }
    public function messages(){
        return [
            'numero_documento.required' => 'El :attribute es obligatorio',
            'numero_documento.max'      => 'El :attribute debe contener maximo 15 caracteres',
            'numero_documento.unique'   => 'El :attribute ya ha sido registrado',
            'isss.required'             => 'El :attribute es obligatorio',
            'isss.max'                  => 'El :attribute debe contener maximo 9 caracteres',
            'isss.unique'               => 'El :attribute ya ha sido registrado',
            'nit.required'              => 'El :attribute es obligatorio',
            'nit.max'                   => 'El :attribute debe contener maximo 18 caracteres',
            'nit.unique'                => 'El :attribute ya ha sido registrado',
            'nup.required'              => 'El :attribute es obligatorio',
            'nup.max'                   => 'El :attribute debe contener maximo 15 caracteres',
            'nup.unique'                => 'El :attribute ya ha sido registrado',
        ];
    }
    public function attributes(){
        return [
            'numero_documento'  => 'Numero de Documento',
            'isss'              => 'Numero de ISSS',
            'nit'               => 'Numero de NIT',
            'nup'               => 'Numero de NUP'
        ];

    }
}
