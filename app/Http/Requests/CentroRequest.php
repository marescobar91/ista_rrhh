<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CentroRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'centro' => 'required|min:5|max:255|unique:emp_centro,centro',
            
        ];
    }


    public function messages()
    {
        return [

        'centro.required'   => 'El :attribute es obligatorio.',
        'centro.min'        => 'El :attribute debe contener mas de cinco letra.',
        'centro.max'        => 'El :attribute debe contener max 255 letras.',
        'centro.unique'     => 'El :attribute ya ha sido registrado',
    

        ];
    }

    
    public function attributes()
    {
        return [

            'centro'        => 'Nombre del Centro',
           

        ];

    }
}
