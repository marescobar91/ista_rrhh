<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EmpleadoInformacionLaboralRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'centro_id'         => 'required',
            'unidad_id'         => 'required',
            'oficina_id'        => 'required',
            'salario_actual'    => 'required',
            'cargoF'            => 'required',
            'cargoN'            => 'required',
            'cargoMP'           => 'required',
            'cargoHacienda'     => 'required',
        ];
    }
    public function messages(){
        return [
            'centro_id.required'            => 'El :attribute es obligatorio',
            'unidad_id.required'            => 'La :attribute es obligatoria',
            'oficina_id.required'           => 'La :attribute es obligatoria',
            'salario_actual.required'       => 'El :attribute es obligatorio',
            'cargoF.required'               => 'El :attribute es obligatorio',
            'cargoN.required'               => 'El :attribute es obligatorio',
            'cargoMP.required'              => 'El :attribute es obligatorio',
            'cargoHacienda.required'        => 'El :attribute es obligatorio',
        ];
    }
    public function attributes(){
        return [
            'centro_id'         => 'Centro',
            'unidad_id'         => 'Unidad Administrativa',
            'oficina_id'        => 'Oficina',
            'salario_actual'    => 'Salario Actual',
            'cargoF'            => 'Cargo Funcional',
            'cargoN'            => 'Cargo Nominal',
            'cargoMP'           => 'Cargo Funcional MP',
            'cargoHacienda'     => 'Cargo Nominal Hacienda',
        ];
    }
}
