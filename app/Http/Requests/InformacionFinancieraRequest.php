<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class InformacionFinancieraRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'pin_empleado'      => 'required|max:11|unique:emp_empleado,pin_empleado',
            'numero_expediente' => 'required|max:11|unique:emp_empleado,numero_expediente',
            'cuenta_banco'      => 'required|max:25|unique:emp_empleado,cuenta_banco',
        ];

    }
    public function messages(){
        return [
            'pin_empleado.required'        => 'El :attribute es obligatorio',
            'pin_empleado.max'             => 'El :attribute debe contener maximo 11 caracteres',
            'pin_empleado.unique'       => 'El :attribute ya ha sido registrado',
            'numero_expediente.required' => 'El :attribute es obligatorio',
            'numero_expediente.max'      => 'El :attribute debe contener maximo 11 caracteres',
            'numero_expediente.unique'   => 'El :attribute ya ha sido registrado',
            'cuenta_banco.required'     => 'El :attribute es obligatorio',
            'cuenta_banco.max'          => 'El :attribute debe contener maximo 25 caracteres',
            'cuenta_banco.unique'       => 'El :attribute ya ha sido registrado',
        ];
    }
    public function attributes(){
        return [
            'pin_empleado'      => 'Pin del Empleado',
            'numero_expediente' => 'Numero de Expediente',
            'cuenta_banco'      => 'Cuenta de Banco'
        ];

    }
}
