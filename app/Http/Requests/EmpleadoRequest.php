<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EmpleadoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'codigo_empleado'   =>  'required|max:7|unique:emp_empleado,codigo_empleado',
            'pin_empleado'      => 'required|max:11|unique:emp_empleado,pin_empleado',
            'numero_expediente' => 'required|max:11|unique:emp_empleado,numero_expediente',
            'numero_documento'  => 'required|max:15|unique:emp_empleado,numero_documento',
            'isss'              => 'required|max:9|unique:emp_empleado,isss',
            'nit'               => 'required|max:18|unique:emp_empleado,nit',
            'cuenta_banco'      => 'required|max:25|unique:emp_empleado,cuenta_banco',
        ];

    }
    public function messages(){
        return [
            'codigo_empleado.required'     => 'El :attribute es obligatorio',
            'codigo_empleado.max'          => 'El :attribute debe contener maximo 7 caracteres',
            'codigo_empleado.unique'       => 'El :attribute ya ha sido registrado',
            'pin_empleado.required'        => 'El :attribute es obligatorio',
            'pin_empleado.max'             => 'El :attribute debe contener maximo 11 caracteres',
            'pin_empleado.unique'       => 'El :attribute ya ha sido registrado',
            'numero_expediente.required' => 'El :attribute es obligatorio',
            'numero_expediente.max'      => 'El :attribute debe contener maximo 11 caracteres',
            'numero_expediente.unique'   => 'El :attribute ya ha sido registrado',
            'numero_documento.required' => 'El :attribute es obligatorio',
            'numero_documento.max'      => 'El :attribute debe contener maximo 15 caracteres',
            'numero_documento.unique'   => 'El :attribute ya ha sido registrado',
            'isss.required'             => 'El :attribute es obligatorio',
            'isss.max'                  => 'El :attribute debe contener maximo 9 caracteres',
            'isss.unique'               => 'El :attribute ya ha sido registrado',
            'nit.required'              => 'El :attribute es obligatorio',
            'nit.max'                   => 'El :attribute debe contener maximo 18 caracteres',
            'nit.unique'                => 'El :attribute ya ha sido registrado',
            'cuenta_banco.required'     => 'El :attribute es obligatorio',
            'cuenta_banco.max'          => 'El :attribute debe contener maximo 25 caracteres',
            'cuenta_banco.unique'       => 'El :attribute ya ha sido registrado',
        ];
    }
    public function attributes(){
        return [
            'codigo_empleado'   => 'Codigo de Empleado',
            'pin_empleado'      => 'Pin del Empleado',
            'numero_expediente' => 'Numero de Expediente',
            'numero_documento'  => 'Numero de Documento',
            'isss'              => 'Numero de ISSS',
            'nit'               => 'Numero de NIT',
            'cuenta_banco'      => 'Cuenta de Banco'
        ];

    }
}
