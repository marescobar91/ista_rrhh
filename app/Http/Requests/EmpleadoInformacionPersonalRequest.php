<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EmpleadoInformacionPersonalRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'pin_empleado'          => 'required|max:11|unique:emp_empleado,pin_empleado',
            'numero_expediente'     => 'required|max:11|unique:emp_empleado,numero_expediente',
            'codigo_empleado'   =>  'required|max:7|unique:emp_empleado,codigo_empleado',
            'nombre' => 'required|regex:/^[\pL\s\-]+$/u',
            'apellido' => 'required|regex:/^[\pL\s\-]+$/u',
            'estadoCivil' => 'required',
            'lugar_nacimiento' => 'required',
            'sexo' => 'required',
            'telefono' => 'required',
            'direccion' => 'required',
            'municipio_id' => 'required',
        ];
    }
    public function messages(){
        return [
            'pin_empleado.required'             => 'El :attribute es obligatorio',
            'pin_empleado.max'                  => 'El :attribute no debe tener mas de 11 numeros',
            'pin_empleado.unique'               => 'El :attribute ya ha sido registrado',
            'numero_expediente.required'        => 'El :attribute es obligatorio',
            'numero_expediente.max'             => 'El :attribute no debe tener mas de 11 numeros',
            'numero_expediente.unique'          => 'El :attribute ya ha sido registrado',
            'codigo_empleado.required'     => 'El :attribute es obligatorio',
            'codigo_empleado.max'          => 'El :attribute debe contener maximo 7 caracteres',
            'codigo_empleado.unique'       => 'El :attribute ya ha sido registrado',
            'nombre.required'        => 'El :attribute es obligatorio',
            'nombre.regex'        => 'El :attribute solo debe contener letras',
            'apellido.required'        => 'El :attribute es obligatorio',
            'apellido.regex'        => 'El :attribute solo debe contener letras',
            'estadoCivil.required'        => 'El :attribute es obligatorio',
            'lugar_nacimiento.required'        => 'El :attribute es obligatorio',
            'sexo.required'        => 'El :attribute es obligatorio',
            'telefono.required'        => 'El :attribute es obligatorio',
            'direccion.required'        => 'La :attribute es obligatoria',
            'municipio_id.required'        => 'El :attribute es obligatorio',
        ];
    }
    public function attributes(){
        return [
            'pin_empleado'          => 'Pin del Empleado',
            'numero_expediente'     => 'Numero de Expediente',
            'codigo_empleado'   => 'Codigo de Empleado',
            'nombre'      => 'Nombre del Empleado',
            'apellido' => 'Apellido del Empleado',
            'estadoCivil'  => 'Estado Civil del Empleado',
            'lugar_nacimiento'              => 'Lugar de Nacimiento',
            'sexo'               => 'Sexo del Empleado',
            'telefono'      => 'Numero de Telefono',
            'direccion'      => 'Direccion',
            'municipio_id'      => 'Municipio',
        ];
    }
}
