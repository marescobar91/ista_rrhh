<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CargoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
     public function rules()
    {
        return [
            //
            'nombre_cargo' => 'required|min:8|max:255|unique:emp_cargo,cargo',
            
        ];
    }


    public function messages()
    {
        return [

        'nombre_cargo.required'   => 'El :attribute es obligatorio.',
        'nombre_cargo.min'        => 'El :attribute debe contener mas de ocho letra.',
        'nombre_cargo.max'        => 'El :attribute debe contener max 255 letras.',
        'nombre_cargo.unique'     => 'El :attribute ya ha sido registrado',
    

        ];
    }

    
    public function attributes()
    {
        return [

            'nombre_cargo'        => 'Nombre del Cargo',
           

        ];

    }
}
