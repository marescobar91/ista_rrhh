<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class NivelAcademicoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'nivel_academico' => 'required|min:8|max:255|unique:emp_nivel_academico,nivel_academico',
            
        ];
    }


    public function messages()
    {
        return [

        'nivel_academico.required'   => 'El :attribute es obligatorio.',
        'nivel_academico.min'        => 'El :attribute debe contener mas de ocho letra.',
        'nivel_academico.max'        => 'El :attribute debe contener max 255 letras.',
        'nivel_academico.unique'     => 'El :attribute ya ha sido registrado',
    

        ];
    }

    
    public function attributes()
    {
        return [

            'nivel_academico'        => 'Nivel Academico',
           

        ];

    }
}
