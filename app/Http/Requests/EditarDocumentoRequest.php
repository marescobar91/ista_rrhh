<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EditarDocumentoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
     public function rules()
    {
        return [
            //
            'descripcion' => 'required|min:3|max:255',
           
        ];
    }
     public function messages()
    {
        return [

        'descripcion.required'   => 'La :attribute es obligatorio.',
        'descripcion.min'        => 'La :attribute debe contener al menos tres letra.',
        'descripcion.max'        => 'La :attribute debe contener máximo 255 letras.',

        
       

        ];
    }

    
    public function attributes()
    {
        return [

            'descripcion'        => 'Descripción del Documento',
           

        ];

    }
}
