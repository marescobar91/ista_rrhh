<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UsuarioRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'nombre_usuario' => ['required', 'min:3', 'max:255', 'unique:admin_usuario,nombre_usuario'],
            'email'         => ['required', 'email', 'unique:admin_usuario,email'],
            'password'       => 'required|min:8|max:20',
            'rol'            => 'required',     
        ];
    }

    public function messages()
    {
        return [

        'nombre_usuario.required'   => 'El :attribute es obligatorio.',
        'nombre_usuario.min'        => 'El :attribute debe contener mas de tres letra.',
        'nombre_usuario.max'        => 'El :attribute debe contener max 255 letras.',
        'nombre_usuario.unique'     =>  'El :attribute ya fue registrado',

        'email.required'           => 'El :attribute es obligatorio.',
        'email.min'                => 'El :attribute debe contener mas de tres letra.',
        'email.max'                => 'El :attribute debe contener max 255 letras.', 
        'email.unique'             => 'El :attribute ya fue registrado',

        'password.required'         => 'La :attribute es obligatoria',
        'password.min'              => 'La :attribute debe contener al menos 8 letras',
        'password.max'              => 'La :attribute debe contener máximo 20 letras',

        'rol.required'              => 'El :attribute es obligatorio',

        ];
    }

    
    public function attributes()
    {
        return [

            'nombre_usuario'        => 'Nombre de Usuario',
            'email'                 => 'Correo',
            'password'              =>  'Contraseña',
            'roles'                 =>  'Rol',

        ];

    }
}
