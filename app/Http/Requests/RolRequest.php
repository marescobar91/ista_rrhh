<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RolRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'nombre_rol' => 'required|min:3|max:255|unique:roles,name',
            'permisos'      =>  'required',
        ];
    }


    public function messages()
    {
        return [

        'nombre_rol.required'   => 'El :attribute es obligatorio.',
        'nombre_rol.min'        => 'El :attribute debe contener mas de tres letra.',
        'nombre_rol.max'        => 'El :attribute debe contener max 255 letras.',
        'nombre_rol.unique'     => 'El :attribute ya ha sido registrado',
        
        'permisos.required'     => 'El :attribute es obligatorio',  

        ];
    }

    
    public function attributes()
    {
        return [

            'nombre_rol'        => 'Nombre del Rol',
            'permisos'          => 'Permisos',

        ];

    }

}
