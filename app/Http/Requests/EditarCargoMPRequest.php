<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class EditarCargoMPRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
     public function rules()
    {
        return [
            //
             'cargo_mp' => ['required', 'min:8', 'max:255', Rule::unique('emp_cargo_mp')->ignore($this->id, 'id_cargo_mp')],
            
            
        ];
    }


    public function messages()
    {
        return [

        'cargo_hacienda.required'   => 'El :attribute es obligatorio.',
        'cargo_hacienda.min'        => 'El :attribute debe contener mas de ocho letra.',
        'cargo_hacienda.max'        => 'El :attribute debe contener max 255 letras.',
        'cargo_hacienda.unique'     => 'El :attribute ya ha sido registrado',
    

        ];
    }

    
    public function attributes()
    {
        return [

            'cargo_hacienda'        => 'Nombre del Cargo',
           

        ];

    }
}
