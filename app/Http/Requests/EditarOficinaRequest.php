<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class EditarOficinaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'oficina'                   => ['required', 'min:8', 'max:255', Rule::unique('emp_oficina')->ignore($this->id, 'id_oficina')],
            'unidad_administrativa'     =>  'required',
            'telefono_oficina'          =>  'required',
        ];
    }


    public function messages()
    {
        return [

        'oficina.required'                 => 'El :attribute es obligatorio.',
        'oficina.min'                      => 'El :attribute debe contener mas de ocho letra.',
        'oficina.max'                      => 'El :attribute debe contener max 255 letras.',
        'oficina.unique'                   => 'El :attribute ya ha sido registrado',

        'unidad_administrativa.required'   => 'El :attribute es obligatorio.',
        'telefono_oficina'                 => 'El :attribute es obligatorio',    

        ];
    }

    
    public function attributes()
    {
        return [

            'oficina'                      => 'Nombre de la Oficina',
            'unidad_administrativa'        => 'Nombre de la Unidad Administrativa',
            'telefono_oficina'             => 'Telefono de la Oficina',
           

        ];

    }
}
