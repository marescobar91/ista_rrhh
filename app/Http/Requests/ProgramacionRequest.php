<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProgramacionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'empleado'        => 'required',
            //'oficina'         => 'required',
            //'nombre'          => 'required',
            'actividad'       => 'required',
            'fecha_inicio'    => 'required',
            'fecha_final'     => 'required',
            "tipo_viatico"    => "required",
            'lugarMision'     => 'required',
            //'departamento'    => 'required', 
        ];
    }

    public function messages()
    {
        return [

        'empleado.required'   => 'Seleccione el :attribute',

        //'oficina.required'   => 'La :attribute es obligatoria.',

        //'nombre.required'   => 'El :attribute es obligatorio.',

        'actividad.required'   => 'Seleccione la :attribute',

        'fecha_inicio.required'   => 'La :attribute es obligatoria.',

        'fecha_final.required'   => 'La :attribute es obligatoria.',

        //'departamento.required'   => 'El :attribute es obligatorio.',

        'lugarMision.required'   => 'Seleccione :attribute',

        "tipo_viatico.required" => "Elija el :attribute",
        ];
    }

    public function attributes()
    {
        return [

            'empleado'              => 'Empleado',
            'oficina'               => 'Oficina',
            'nombre'                => 'Nombre Empleado',
            'actividad'             => 'Actividad',
            'fecha_inicio'          => 'Fecha Inicial',
            'fecha_final'           => 'Fecha Final',
            'lugarMision'           => 'Lugar de Mision',
            'departamento'          => 'Departamento',
            'tipo_viatico'          => 'Tipo de Viatico',

        ];

    }
}
