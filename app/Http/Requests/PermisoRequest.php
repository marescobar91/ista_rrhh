<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PermisoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'nombre_permiso' => 'required|min:3|max:255|unique:permissions,name',
        ];
    }

    public function messages()
    {
        return [

        'nombre_permiso.required'   => 'El :attribute es obligatorio.',
        'nombre_permiso.min'        => 'El :attribute debe contener mas de tres letra.',
        'nombre_permiso.max'        => 'El :attribute debe contener max 255 letras.',
        'nombre_permiso.unique'     => 'El :attribute ya ha sido registrado',

        ];
    }

    
    public function attributes()
    {
        return [

            'nombre_permiso'        => 'Nombre del Permiso',

        ];

    }
}
