<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UnidadAdministrativaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'unidad_administrativa' => 'required|min:8|max:255|unique:emp_unidad_administrativa,unidad_administrativa',
            
        ];
    }


    public function messages()
    {
        return [

        'unidad_administrativa.required'   => 'El :attribute es obligatorio.',
        'unidad_administrativa.min'        => 'El :attribute debe contener mas de ocho letra.',
        'unidad_administrativa.max'        => 'El :attribute debe contener max 255 letras.',
        'unidad_administrativa.unique'     => 'El :attribute ya ha sido registrado',
    

        ];
    }

    
    public function attributes()
    {
        return [

            'unidad_administrativa'        => 'Nombre de la Unidad Administrativa',
           

        ];

    }
}
