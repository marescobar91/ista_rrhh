<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AsuetoRequest extends FormRequest
{
    
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'asueto' => 'required|min:5|max:255|unique:mar_asueto,asueto',
            'fecha' => 'required|unique:mar_asueto,fecha',
        ];
    }
     public function messages()
    {
        return [

        'asueto.required'                  => 'El :attribute es obligatorio.',
        'asueto.min'                      => 'El :attribute debe contener mas de ocho letra.',
        'asueto.max'                      => 'El :attribute debe contener max 255 letras.',
        'asueto.unique'                   => 'El :attribute ya ha sido registrado',
        'fecha.required'                  => 'El :attribute es obligatorio.', 
        'fecha.unique'                   => 'El :attribute ya ha sido registrado',    

        ];
    }

    
    public function attributes()
    {
        return [

            'asueto'                      => 'Nombre de la Asueto',
            'fecha'             => 'Fecha del Asueto',
           

        ];

    }





}
