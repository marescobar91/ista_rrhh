<?php

namespace App\Http\Controllers\Empleados; 

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Barryvdh\DomPDF\Facade as PDF;
use App\Modelos\Empleado\UnidadAdministrativa;
use App\Exports\Empleados\UnidadesExport;
use App\Exports\Empleados\GeneroExport;
use App\Exports\Empleados\JefaturaExport;
use App\Exports\Empleados\ReporteUnoExport;
use App\Exports\Empleados\ReporteDosExport;
use App\Exports\Empleados\ReporteTresExport;
use App\Http\Requests\ReporteUnoRequest;
use App\Http\Requests\ReporteDosRequest;
use App\Http\Requests\ReporteTresRequest;
use Maatwebsite\Excel\Facades\Excel;
use Carbon\Carbon;


class ReporteController extends Controller
{   
    //funcion para los permisos de los reportes de empleados
    public function __construct(){  
        $this->middleware(['permission:consultar.reportes'], ['only'   => 'verUnidad', 'imprimirUnidad', 'verJefatura', 'imprimirJefatura', 'verGenero', 'imprimirGenero', 'filtroReporteDinamico', 'verReporteUno', 'verReporteDos', 'verReporteTres', 'imprimirReporteUno', 'imprimirReporteDos', 'imprimirReporteTres']);


    }
    /* Reporte de Empleados por unidad administrativa   Inicio */
    public function verUnidad(){
        //muestra las unidades 
        //se obtienen las unidades de la vista 'unidades_vw'
        $unidad = DB::table('unidades_vw')->OrderBy('unidad_administrativa')->get();
        $fecha = Carbon::now(new \DateTimeZone('America/El_Salvador'));
        $mes = $fecha->monthName;   //obtiene el mes actual
        $anio = $fecha->year;       //obtiene el año actual
        //se obtienen las unidades de la vista 'reporte_unidad_vw'
        $unidades = DB::table('reporte_unidad_vw')->OrderBy('unidad_administrativa')->get();
        return view('empleados.gestionReportes.verUnidad', compact('unidades', 'unidad','mes','anio'));
    }

    public function imprimirUnidad(){
        //se obtienen las unidades de la vista 'reporte_unidad_vw'
        $fecha = Carbon::now(new \DateTimeZone('America/El_Salvador'));
        $unidad = DB::table('unidades_vw')->OrderBy('unidad_administrativa')->get();
        
        $fechaF = $fecha->format('d-m-Y h:i:s');
        $mes = $fecha->monthName;   //obtiene el mes actual
        $anio = $fecha->year;       //obtiene el año actual
        //se obtienen las unidades de la vista 'unidades_vw'
        $unidades = DB::table('reporte_unidad_vw')->OrderBy('unidad_administrativa')->get();
        
        $pdf = \PDF::loadView('empleados.gestionReportes.imprimirUnidad', ['unidades' => $unidades], compact('mes','anio','fechaF', 'unidad'))->setWarnings(false);
        return $pdf->stream('unidad.pdf');
        
    	
    }


    public function excelUnidad(){
    $fecha = Carbon::now(new \DateTimeZone('America/El_Salvador'));
    return Excel::download(new UnidadesExport, 'unidad'. $fecha .'.xlsx');

    }
    /* Reporte de Empleados por unidad administrativa   Fin */

    /* Reporte de Empleados por Jefatura   Inicio */
    public function verJefatura(){
        //se obtiene las jefatura de la vista 'jefaturas_vw'
    	$jefatura = DB::table('jefaturas_vw')->get();
        $fecha = Carbon::now(new \DateTimeZone('America/El_Salvador'));
        $mes = $fecha->monthName;   //obtiene el mes actual
        $anio = $fecha->year;       //obtiene el año actual
        return view('empleados.gestionReportes.verJefaturas',compact('jefatura','mes','anio'));
    }

    public function imprimirJefatura(){
        //se obtiene las jefatura de la vista 'jefaturas_vw'
    	
        $fecha = Carbon::now(new \DateTimeZone('America/El_Salvador'));
      $fechaF = $fecha->format('d-m-Y h:i:s');
        $mes = $fecha->monthName;   //obtiene el mes actual
        $anio = $fecha->year;       //obtiene el año actual
        $jefatura = DB::table('jefaturas_vw')->get();
        $pdf = PDF::loadView('empleados.gestionReportes.imprimirJefaturas', ['jefatura' => $jefatura], compact('mes','anio','fechaF'))->setWarnings(false);
        return $pdf->stream('jefaturas.pdf');
       
    }


    public function excelJefatura(){
        $fecha = Carbon::now(new \DateTimeZone('America/El_Salvador'));
        return Excel::download(new JefaturaExport, 'jefatura' . $fecha . '.xlsx');

    }
    /* Reporte de Empleados por unidad administrativa   Fin */

    /* Reporte de Empleados Dinamico   Inicio */

    public function filtroReporteDinamico(){

        return view('empleados.gestionReportes.filtroReporteDinamico');

    }

    public function verReporteUno(ReporteUnoRequest $request){

        $dato = $_POST['reporteUno'];  //se recupera el array para el reporte uno
        $fecha = Carbon::now(new \DateTimeZone('America/El_Salvador'));
        $mes = $fecha->monthName;   //obtiene el mes actual
        $anio = $fecha->year;       //obtiene el año actual
        $contar = count($dato); 

        if(array_intersect($dato, array('1')) && $contar==1) {

            $empleado = DB::table('empleado_vw')->select('codigo_empleado', 'nombre', 'apellido', 'sexo', 'unidad_administrativa', 'oficina', 'departamento', 'cargo_mp')->get();
            $departamento ='Si';
            $direccion = 'No';
            return view('empleados.gestionReportes.verReporteUno', compact('empleado', 'dato', 'departamento', 'direccion','mes','anio'));
        }elseif (array_intersect($dato, array('2')) && $contar==1) {

         $empleado = DB::table('empleado_vw')->select('codigo_empleado', 'nombre', 'apellido', 'sexo', 'unidad_administrativa', 'oficina', 'direccion', 'cargo_mp')->get();
         $departamento ='No';
         $direccion = 'Si';
         return view('empleados.gestionReportes.verReporteUno', compact('empleado', 'dato', 'departamento', 'direccion','mes','anio'));
     }elseif (array_intersect($dato, array('1', '2'))){
        $empleado = DB::table('empleado_vw')->select('codigo_empleado', 'nombre', 'apellido', 'sexo', 'unidad_administrativa', 'oficina', 'departamento','direccion', 'cargo_mp')->get();
        $departamento ='Si';
        $direccion = 'Si';
        return view('empleados.gestionReportes.verReporteUno', compact('empleado', 'dato', 'departamento', 'direccion','mes','anio'));

    }


}

public function imprimirReporteUno(Request $request){

    $dato = $_POST['reporteUno'];   //se recupera el array para el reporte uno
    $contar = count($dato);
    $fecha = Carbon::now(new \DateTimeZone('America/El_Salvador'));
    $fechaF = $fecha->format('d-m-Y h:i:s');
        $mes = $fecha->monthName;   //obtiene el mes actual
        $anio = $fecha->year;       //obtiene el año actual

        if(array_intersect($dato, array('1')) && $contar==1) {

            $empleado = DB::table('empleado_vw')->select('codigo_empleado', 'nombre', 'apellido', 'sexo', 'unidad_administrativa', 'oficina', 'departamento', 'cargo_mp')->get();
            $departamento ='Si';
            $direccion = 'No';
           
        $pdf = PDF::loadView('empleados.gestionReportes.imprimirReporteUno', compact('empleado', 'departamento', 'direccion','mes','anio','fechaF'))->setPaper('A4', 'landscape')->setWarnings(false);
        return $pdf->stream('reporte_ubicaciones.pdf');


        }elseif (array_intersect($dato, array('2')) && $contar==1) {

        $empleado = DB::table('empleado_vw')->select('codigo_empleado', 'nombre', 'apellido', 'sexo', 'unidad_administrativa', 'oficina', 'direccion', 'cargo_mp')->get();
         $departamento ='No';
         $direccion = 'Si';
        $pdf = PDF::loadView('empleados.gestionReportes.imprimirReporteUno', compact('empleado', 'departamento', 'direccion','mes','anio','fechaF'))->setPaper('A4', 'landscape')->setWarnings(false);
         return $pdf->stream('reporte_ubicaciones.pdf');

     }elseif (array_intersect($dato, array('1', '2'))){
       $empleado = DB::table('empleado_vw')->select('codigo_empleado', 'nombre', 'apellido', 'sexo', 'unidad_administrativa', 'oficina', 'departamento','direccion', 'cargo_mp')->get();
        $departamento ='Si';
        $direccion = 'Si';
       $pdf = PDF::loadView('empleados.gestionReportes.imprimirReporteUno', compact('empleado', 'departamento', 'direccion','mes','anio','fechaF'))->setPaper('A4', 'landscape')->setWarnings(false);
        return $pdf->stream('reporte_ubicaciones.pdf');
    }
}


public function excelReporteUno(Request $request){

    $dato = $_POST['reporteUno'];   //se recupera el array para el reporte uno
    $contar = count($dato);
    $fecha = Carbon::now(new \DateTimeZone('America/El_Salvador'));
    $fechaF = $fecha->format('d-m-Y h:i:s');
    $mes = $fecha->monthName;   //obtiene el mes actual
    $anio = $fecha->year;       //obtiene el año actual

        if(array_intersect($dato, array('1')) && $contar==1) {
            $departamento ='Si';
            $direccion = 'No';
            return Excel::download(new ReporteUnoExport($departamento, $direccion), 'reporteUno'. $fecha. '.xlsx');

        }elseif (array_intersect($dato, array('2')) && $contar==1) {
         $departamento ='No';
         $direccion = 'Si';
         return Excel::download(new ReporteUnoExport($departamento, $direccion), 'reporteUno'. $fecha. '.xlsx');

     }elseif (array_intersect($dato, array('1', '2'))){
   
        $departamento ='Si';
        $direccion = 'Si';
        return Excel::download(new ReporteUnoExport($departamento, $direccion), 'reporteUno'. $fecha. '.xlsx');
    }
}


public function verReporteDos(ReporteDosRequest $request){

    $dato = $_POST['reporteDos'];  //recupra el array para el reporte dos
    $contar = count($dato);
    $fecha = Carbon::now(new \DateTimeZone('America/El_Salvador'));
    $mes = $fecha->monthName;   //obtiene el mes actual
        $anio = $fecha->year;       //obtiene el año actual

        if(array_intersect($dato, array('1')) && $contar==1) {

            $empleado = DB::table('empleado_vw')->select('codigo_empleado', 'nombre', 'apellido', 'sexo', 'unidad_administrativa', 'oficina', 'fecha_nacimiento', 'cargo_mp')->get();
            $fechaNacimiento ='Si';
            $edad = 'No';
            return view('empleados.gestionReportes.verReporteDos', compact('empleado', 'dato', 'fechaNacimiento', 'edad','mes','anio'));
        }elseif (array_intersect($dato, array('2')) && $contar==1) {

         $empleado = DB::table('empleado_vw')->select('codigo_empleado', 'nombre', 'apellido', 'sexo', 'unidad_administrativa', 'oficina', 'fecha_nacimiento', 'cargo_mp')->get();
         $fechaNacimiento ='No';
         $edad = 'Si';
         return view('empleados.gestionReportes.verReporteDos', compact('empleado', 'dato', 'fechaNacimiento', 'edad','mes','anio'));
     }elseif (array_intersect($dato, array('1', '2'))){
        $empleado = DB::table('empleado_vw')->select('codigo_empleado', 'nombre', 'apellido', 'sexo', 'unidad_administrativa', 'oficina', 'fecha_nacimiento', 'cargo_mp')->get();
        $fechaNacimiento ='Si';
        $edad = 'Si';
        return view('empleados.gestionReportes.verReporteDos', compact('empleado', 'dato', 'fechaNacimiento', 'edad','mes','anio'));

    }


}

public function imprimirReporteDos(Request $request){

    $dato = $_POST['reporteDos'];           //obtiene el array para el reporte dos
    $contar = count($dato);
    $fecha = Carbon::now(new \DateTimeZone('America/El_Salvador'));
    $fechaF = $fecha->format('d-m-Y h:i:s');
    $mes = $fecha->monthName;   //obtiene el mes actual
        $anio = $fecha->year;       //obtiene el año actual

        if(array_intersect($dato, array('1')) && $contar==1) {

        $empleado = DB::table('empleado_vw')->select('codigo_empleado', 'nombre', 'apellido', 'sexo', 'unidad_administrativa', 'oficina', 'fecha_nacimiento', 'cargo_mp')->get();
            $fechaNacimiento ='Si';
            $edad = 'No';
           $pdf = PDF::loadView('empleados.gestionReportes.imprimirReporteDos', compact('empleado', 'fechaNacimiento', 'edad','mes','anio','fechaF'))->setPaper('A4', 'landscape')->setWarnings(false);
        return $pdf->stream('reporte_personal.pdf');
           

        }elseif (array_intersect($dato, array('2')) && $contar==1) {

       $empleado = DB::table('empleado_vw')->select('codigo_empleado', 'nombre', 'apellido', 'sexo', 'unidad_administrativa', 'oficina', 'cargo_mp', 'fecha_nacimiento')->get();
         $fechaNacimiento ='No';
         $edad = 'Si';
    $pdf = PDF::loadView('empleados.gestionReportes.imprimirReporteDos', compact('empleado', 'fechaNacimiento', 'edad','mes','anio','fechaF'))->setPaper('A4', 'landscape')->setWarnings(false);
         return $pdf->stream('reporte_personal.pdf');
         return Excel::download(new ReporteDosExport($fechaNacimiento, $edad), 'reporteDos'. $fecha . '.xlsx'); 
     }elseif (array_intersect($dato, array('1', '2'))){
       $empleado = DB::table('empleado_vw')->select('codigo_empleado', 'nombre', 'apellido', 'sexo', 'unidad_administrativa', 'oficina', 'fecha_nacimiento', 'cargo_mp')->get();
        $fechaNacimiento ='Si';
        $edad = 'Si';
        $pdf = PDF::loadView('empleados.gestionReportes.imprimirReporteDos', compact('empleado', 'fechaNacimiento', 'edad','mes','anio','fechaF'))->setPaper('A4', 'landscape')->setWarnings(false);
        return $pdf->stream('reporte_personal.pdf');
    }
}

public function excelReporteDos(Request $request){

    $dato = $_POST['reporteDos'];           //obtiene el array para el reporte dos
    $contar = count($dato);
    $fecha = Carbon::now(new \DateTimeZone('America/El_Salvador'));
    $fechaF = $fecha->format('d-m-Y h:i:s');
    $mes = $fecha->monthName;   //obtiene el mes actual
    $anio = $fecha->year;       //obtiene el año actual

    if(array_intersect($dato, array('1')) && $contar==1) {
            $fechaNacimiento ='Si';
            $edad = 'No';
          
    return Excel::download(new ReporteDosExport($fechaNacimiento, $edad), 'reporteDos'. $fecha . '.xlsx'); 

        }elseif (array_intersect($dato, array('2')) && $contar==1) {

         $fechaNacimiento ='No';
         $edad = 'Si';
      
         return Excel::download(new ReporteDosExport($fechaNacimiento, $edad), 'reporteDos'. $fecha . '.xlsx'); 
     }elseif (array_intersect($dato, array('1', '2'))){
        $fechaNacimiento ='Si';
        $edad = 'Si';
        return Excel::download(new ReporteDosExport($fechaNacimiento, $edad), 'reporteDos'. $fecha . '.xlsx'); 
    }
}


public function verReporteTres(ReporteTresRequest $request){

    $dato = $_POST['reporteTres'];      //obtiene el array para el reporte tres
    $contar = count($dato);
    $fecha = Carbon::now(new \DateTimeZone('America/El_Salvador'));
    $mes = $fecha->monthName;   //obtiene el mes actual
        $anio = $fecha->year;       //obtiene el año actual

        if(array_intersect($dato, array('1')) && $contar==1) {

            $empleado = DB::table('empleado_vw')->select('codigo_empleado', 'nombre', 'apellido', 'sexo', 'unidad_administrativa', 'oficina', 'tipo_plaza', 'cargo_mp')->get();
            $tipoPlaza ='Si';
            $fechaIngreso = 'No';
            $salario = 'No';
            return view('empleados.gestionReportes.verReporteTres', compact('empleado', 'dato', 'tipoPlaza', 'fechaIngreso', 'salario','mes','anio'));
        }elseif (array_intersect($dato, array('2')) && $contar==1) {

         $empleado = DB::table('empleado_vw')->select('codigo_empleado', 'nombre', 'apellido', 'sexo', 'unidad_administrativa', 'oficina', 'fecha_ingreso', 'cargo_mp')->get();
         $tipoPlaza ='No';
         $fechaIngreso = 'Si';
         $salario = 'No';
         return view('empleados.gestionReportes.verReporteTres', compact('empleado', 'dato', 'tipoPlaza', 'fechaIngreso', 'salario','mes','anio'));
     }elseif (array_intersect($dato, array('3')) && $contar==1){
        $empleado = DB::table('empleado_vw')->select('codigo_empleado', 'nombre', 'apellido', 'sexo', 'unidad_administrativa', 'oficina', 'salario_actual', 'cargo_mp')->get();
        $tipoPlaza ='No';
        $fechaIngreso = 'No';
        $salario = 'Si';
        return view('empleados.gestionReportes.verReporteTres', compact('empleado', 'dato', 'tipoPlaza', 'fechaIngreso', 'salario','mes','anio'));

    }elseif (array_unique($dato) === array('1','2')) {
        $empleado = DB::table('empleado_vw')->select('codigo_empleado', 'nombre', 'apellido', 'sexo', 'unidad_administrativa', 'oficina', 'tipo_plaza', 'fecha_ingreso', 'cargo_mp')->get();
        $tipoPlaza ='Si';
        $fechaIngreso = 'Si';
        $salario = 'No';

        return view('empleados.gestionReportes.verReporteTres', compact('empleado', 'dato', 'tipoPlaza', 'fechaIngreso', 'salario','mes','anio'));

    }elseif (array_unique($dato) === array('1','3')) {
        $empleado = DB::table('empleado_vw')->select('codigo_empleado', 'nombre', 'apellido', 'sexo', 'unidad_administrativa', 'oficina', 'tipo_plaza', 'salario_actual', 'cargo_mp')->get();
        $tipoPlaza ='Si';
        $fechaIngreso= 'No';
        $salario = 'Si';
        return view('empleados.gestionReportes.verReporteTres', compact('empleado', 'dato', 'tipoPlaza', 'fechaIngreso', 'salario','mes','anio'));
    }elseif (array_unique($dato) === array('2','3')) {
        $empleado = DB::table('empleado_vw')->select('codigo_empleado', 'nombre', 'apellido', 'sexo', 'unidad_administrativa', 'oficina', 'fecha_ingreso', 'salario_actual', 'cargo_mp')->get();
        $tipoPlaza ='No';
        $fechaIngreso = 'Si';
        $salario = 'Si';
        return view('empleados.gestionReportes.verReporteTres', compact('empleado', 'dato', 'tipoPlaza', 'fechaIngreso', 'salario','mes','anio'));
    }elseif (array_intersect($dato, array('1', '2', '3'))) {
        $empleado = DB::table('empleado_vw')->select('codigo_empleado', 'nombre', 'apellido', 'sexo', 'unidad_administrativa', 'oficina', 'tipo_plaza','fecha_ingreso', 'salario_actual', 'cargo_mp')->get();
        $tipoPlaza ='Si';
        $fechaIngreso = 'Si';
        $salario = 'Si'; 
        return view('empleados.gestionReportes.verReporteTres', compact('empleado', 'dato', 'tipoPlaza', 'fechaIngreso', 'salario','mes','anio'));
    }

}

public function imprimirReporteTres(Request $request){

    $dato = $_POST['reporteTres'];              //obtiene el array para el reporte tres
    $contar = count($dato);
    $fecha = Carbon::now(new \DateTimeZone('America/El_Salvador'));
    $fechaF = $fecha->format('d-m-Y h:i:s');
    $mes = $fecha->monthName;   //obtiene el mes actual
        $anio = $fecha->year;       //obtiene el año actual

        if(array_intersect($dato, array('1')) && $contar==1) {

    $empleado = DB::table('empleado_vw')->select('codigo_empleado', 'nombre', 'apellido', 'sexo', 'unidad_administrativa', 'oficina', 'tipo_plaza', 'cargo_mp')->get();
            $tipoPlaza ='Si';
            $fechaIngreso = 'No';
            $salario = 'No';
    $pdf = PDF::loadView('empleados.gestionReportes.imprimirReporteTres', compact('empleado', 'tipoPlaza', 'fechaIngreso', 'salario','mes','anio','fechaF'))->setPaper('A4', 'landscape')->setWarnings(false);
    return $pdf->stream('reporte_laboral.pdf');
           
        }elseif (array_intersect($dato, array('2')) && $contar==1) {

        $empleado = DB::table('empleado_vw')->select('codigo_empleado', 'nombre', 'apellido', 'sexo', 'unidad_administrativa', 'oficina', 'fecha_ingreso', 'cargo_mp')->get();
         $tipoPlaza ='No';
         $fechaIngreso = 'Si';
         $salario = 'No';
        $pdf = PDF::loadView('empleados.gestionReportes.imprimirReporteTres', compact('empleado', 'tipoPlaza', 'fechaIngreso', 'salario','mes','anio','fechaF'))->setPaper('A4', 'landscape')->setWarnings(false);
        return $pdf->stream('reporte_laboral.pdf');
        
     }elseif (array_intersect($dato, array('3')) && $contar==1){
    $empleado = DB::table('empleado_vw')->select('codigo_empleado', 'nombre', 'apellido', 'sexo', 'unidad_administrativa', 'oficina', 'salario_actual', 'cargo_mp')->get();
        $tipoPlaza ='No';
        $fechaIngreso = 'No';
        $salario = 'Si';
    $pdf = PDF::loadView('empleados.gestionReportes.imprimirReporteTres', compact('empleado', 'tipoPlaza', 'fechaIngreso', 'salario','mes','anio','fechaF'))->setPaper('A4', 'landscape')->setWarnings(false);
    return $pdf->stream('reporte_laboral.pdf');
        
    }elseif (array_unique($dato) === array('1','2')) {
    $empleado = DB::table('empleado_vw')->select('codigo_empleado', 'nombre', 'apellido', 'sexo', 'unidad_administrativa', 'oficina', 'tipo_plaza', 'fecha_ingreso', 'cargo_mp')->get();
        $tipoPlaza ='Si';
        $fechaIngreso = 'Si';
        $salario = 'No';
    $pdf = PDF::loadView('empleados.gestionReportes.imprimirReporteTres', compact('empleado', 'tipoPlaza', 'fechaIngreso', 'salario','mes','anio','fechaF'))->setPaper('A4', 'landscape')->setWarnings(false);
    return $pdf->stream('reporte_laboral.pdf');
    
    }elseif (array_unique($dato) === array('1','3')) {
    $empleado = DB::table('empleado_vw')->select('codigo_empleado', 'nombre', 'apellido', 'sexo', 'unidad_administrativa', 'oficina', 'tipo_plaza', 'salario_actual', 'cargo_mp')->get();
        $tipoPlaza ='Si';
        $fechaIngreso = 'No';
        $salario = 'Si';
    $pdf = PDF::loadView('empleados.gestionReportes.imprimirReporteTres', compact('empleado', 'tipoPlaza', 'fechaIngreso', 'salario','mes','anio','fechaF'))->setPaper('A4', 'landscape')->setWarnings(false);
    return $pdf->stream('reporte_laboral.pdf');
         
    }elseif (array_unique($dato) === array('2','3')) {
    $empleado = DB::table('empleado_vw')->select('codigo_empleado', 'nombre', 'apellido', 'sexo', 'unidad_administrativa', 'oficina', 'fecha_ingreso', 'salario_actual', 'cargo_mp')->get();
        $tipoPlaza ='No';
        $fechaIngreso = 'Si';
        $salario = 'Si';
    $pdf = PDF::loadView('empleados.gestionReportes.imprimirReporteTres', compact('empleado', 'tipoPlaza', 'fechaIngreso', 'salario','mes','anio','fechaF'))->setPaper('A4', 'landscape')->setWarnings(false);
    return $pdf->stream('reporte_laboral.pdf');
        
    }elseif (array_intersect($dato, array('1', '2', '3'))) {
    $empleado = DB::table('empleado_vw')->select('codigo_empleado', 'nombre', 'apellido', 'sexo', 'unidad_administrativa', 'oficina', 'tipo_plaza','fecha_ingreso', 'salario_actual', 'cargo_mp')->get();
        $tipoPlaza ='Si';
        $fechaIngreso = 'Si';
        $salario = 'Si'; 
$pdf = PDF::loadView('empleados.gestionReportes.imprimirReporteTres', compact('empleado', 'tipoPlaza', 'fechaIngreso', 'salario','mes','anio','fechaF'))->setPaper('A4', 'landscape')->setWarnings(false);
return $pdf->stream('reporte_laboral.pdf');
         
    }

}


public function excelReporteTres(Request $request){

    $dato = $_POST['reporteTres'];              //obtiene el array para el reporte tres
    $contar = count($dato);
    $fecha = Carbon::now(new \DateTimeZone('America/El_Salvador'));
    $fechaF = $fecha->format('d-m-Y h:i:s');
    $mes = $fecha->monthName;   //obtiene el mes actual
    $anio = $fecha->year;       //obtiene el año actual

        if(array_intersect($dato, array('1')) && $contar==1) {
            $tipoPlaza ='Si';
            $fechaIngreso = 'No';
            $salario = 'No';
        return Excel::download(new ReporteTresExport($tipoPlaza, $fechaIngreso, $salario), 'reporteTres'. $fecha . '.xlsx');
        }elseif (array_intersect($dato, array('2')) && $contar==1) {
         $tipoPlaza ='No';
         $fechaIngreso = 'Si';
         $salario = 'No';
      
        return Excel::download(new ReporteTresExport($tipoPlaza, $fechaIngreso, $salario), 'reporteTres'. $fecha . '.xlsx');
     }elseif (array_intersect($dato, array('3')) && $contar==1){
        $tipoPlaza ='No';
        $fechaIngreso = 'No';
        $salario = 'Si';
        return Excel::download(new ReporteTresExport($tipoPlaza, $fechaIngreso, $salario), 'reporteTres'. $fecha . '.xlsx');

    }elseif (array_unique($dato) === array('1','2')) {
        $tipoPlaza ='Si';
        $fechaIngreso = 'Si';
        $salario = 'No';
    return Excel::download(new ReporteTresExport($tipoPlaza, $fechaIngreso, $salario), 'reporteTres'. $fecha . '.xlsx');

    }elseif (array_unique($dato) === array('1','3')) {
    
        $tipoPlaza ='Si';
        $fechaIngreso = 'No';
        $salario = 'Si';
         return Excel::download(new ReporteTresExport($tipoPlaza, $fechaIngreso, $salario), 'reporteTres'. $fecha . '.xlsx');
    }elseif (array_unique($dato) === array('2','3')) {
    
        $tipoPlaza ='No';
        $fechaIngreso = 'Si';
        $salario = 'Si';
    
        return Excel::download(new ReporteTresExport($tipoPlaza, $fechaIngreso, $salario), 'reporteTres'. $fecha . '.xlsx');
    }elseif (array_intersect($dato, array('1', '2', '3'))) {
       
        $tipoPlaza ='Si';
        $fechaIngreso = 'Si';
        $salario = 'Si'; 
    
    return Excel::download(new ReporteTresExport($tipoPlaza, $fechaIngreso, $salario), 'reporteTres'. $fecha . '.xlsx');
    }

}
/* Reporte de Empleados Dinamico   Fin */

/* Reporte de Empleados por Genero   Inicio */

public function verGenero(Request $request)
{   
    //se obtiene los generos de la vista 'genero_vw'
    $genero = DB::table('genero_vw')->select('Oficina',  DB::raw('COUNT(Hombre) as hombre'), DB::raw('COUNT(Mujer) as mujer'), DB::raw('COUNT(ID) as total'))->groupBy('Oficina')->get();
    $fecha = Carbon::now(new \DateTimeZone('America/El_Salvador'));
    $mes = $fecha->monthName;   //obtiene el mes actual
        $anio = $fecha->year;       //obtiene el año actual
        return view('empleados.gestionReportes.verGenero',compact('genero','mes','anio'));
    }
    public function imprimirGenero(){
        //se obtiene los generos de la vista 'genero_vw'
        $genero = DB::table('genero_vw')->select('Oficina',  DB::raw('COUNT(Hombre) as hombre'), DB::raw('COUNT(Mujer) as mujer'), DB::raw('COUNT(ID) as total'))->groupBy('Oficina')->get();
        $fecha = Carbon::now(new \DateTimeZone('America/El_Salvador'));
        $fechaF = $fecha->format('d-m-Y h:i:s');
        $mes = $fecha->monthName;   //obtiene el mes actual
        $anio = $fecha->year;       //obtiene el año actual

        $pdf = PDF::loadView('empleados.gestionReportes.imprimirGenero', ['genero' => $genero], compact('mes','anio','fechaF'))->setWarnings(false);
        return $pdf->stream('generos.pdf');
    }

    public function excelGenero(){
    
        $fecha = Carbon::now(new \DateTimeZone('America/El_Salvador'));
        return Excel::download(new GeneroExport, 'genero'. $fecha .'.xlsx');

    }

    /* Reporte de Empleados por Genero   Fin */


    public function verReportes(){
        return view('empleados.gestionReportes.verReportes');
    }


}

