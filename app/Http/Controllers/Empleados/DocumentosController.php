<?php

namespace App\Http\Controllers\Empleados;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Modelos\Empleado\Documento;
use App\Modelos\Empleado\Empleado;
use App\Http\Requests\DocumentoRequest;
use App\Http\Requests\EditarDocumentoRequest;
use DB;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;
use RealRashid\SweetAlert\Facades\Alert;

class DocumentosController extends Controller
{
    //funcion para los permisos de documentos
public function __construct(){

    $this->middleware(['permission:agregar.documentos'], ['only'    => ['agregarArchivosEmpleado', 'guardarArchivosEmpleado']]);
    $this->middleware(['permission:reemplazar.documentos'], ['only'   => ['editarArchivosEmpleado', 'reemplazarArchivosEmpleado']]);
    $this->middleware(['permission:consultar.documentos'], ['only'   => ['listarArchivosEmpleado', 'mostrarArchivosEmpleado']]);
    $this->middleware(['permission:eliminar.documentos'], ['only'   => 'eliminarArchivosEmpleado']);
    }

    public function agregarArchivosEmpleado($id){
        //muestra formulario para agregar archivos al empleado seleccionado
    $empleado = Empleado::find($id);    

    	return view('empleados.gestionEmpleados.agregarArchivo', compact('empleado'));
    }

    public function guardarArchivosEmpleado(DocumentoRequest $request, $id){
        //guarda los archivos adjuntos en el empleado seleccionado
    	$empleado = Empleado::find($id);
        $idEmpleado = $empleado->id_empleado;
        $documento = new Documento;
        if($request->file('documento')){
        $archivo   = $request->file('documento');
        $nombre    = Carbon::now()->second." " .$archivo->getClientOriginalName();
        $extension = $archivo->getClientOriginalExtension();
        //ruta de guardado para archivo adjunto
        $ruta = $request->file('documento')->storeAs('public/documentos/'. $empleado->codigo_empleado, $nombre);
            $documento->empleado_id = $empleado->id_empleado;
            $documento->nombre_documento = $nombre;
            $documento->ruta= $ruta;
            $documento->extension = $extension;
            $documento->descripcion = $request->descripcion;
            $documento->save();
        Alert::success('Guadar Documento', $empleado->nombre . ' ' .$empleado->apellido.' de forma existosa')->autoClose(2000);
                return redirect()->action('Empleados\DocumentosController@listarArchivosEmpleado', ['id' => $idEmpleado]);
        }
        Alert::error('Error',' ¡No hay archivo o documento!')->autoClose(2000);
     return redirect()->action('Empleados\DocumentosController@listarArchivosEmpleado', ['id' => $idEmpleado]);

    }

    public function listarArchivosEmpleado($id){
        //muestra listado de archivos para un empleado
    $empleado = Empleado::find($id);
    $documento = Documento::where('empleado_id', '=', $empleado->id_empleado)->get();
       
    return view('empleados.gestionEmpleados.listarArchivos', compact('documento', 'empleado'));
        
    }

    public function editarArchivosEmpleado($id){
        //muestra vista para editar los archivos del empleado
    $documento = Documento::find($id);
    $empleado = Empleado::find($documento->empleado_id);

    return view('empleados.gestionEmpleados.reemplazarArchivo', compact('documento', 'empleado'));
    }

    public function reemplazarArchivosEmpleado(EditarDocumentoRequest $request, $id){
        //reemplaza los archivos del empleado seleccionado
        $documentoAntiguo = Documento::find($id);
        $empleado = Empleado::find($documentoAntiguo->empleado_id);
        $idEmpleado = $empleado->id_empleado;
        if($request->file('documento')){
        $archivo   = $request->file('documento');
        $nombre    = Carbon::now()->second." " .$archivo->getClientOriginalName();
        $extension = $archivo->getClientOriginalExtension();
        //ruta de guardado para los archivos
        $ruta =     $request->file('documento')->storeAs('public/documentos/'. $empleado->codigo_empleado, $nombre);
        if($ruta!=null){
        $documentoActualizado = Documento::find($id);
        $documentoActualizado->empleado_id = $empleado->id_empleado;
        $documentoActualizado->nombre_documento = $nombre;
        $documentoActualizado->ruta= $ruta;
        $documentoActualizado->extension = $extension;
        $documentoActualizado->descripcion = $request->descripcion;
        if(Storage::delete($documentoAntiguo->ruta)){
        $documentoActualizado->update();
        Alert::warning('Actualizar Documento', $empleado->nombre . ' ' .$empleado->apellido.  ' de forma existosa')->autoClose(2000)->iconHtml('<i class="fa fa-pencil"></i>');

        return redirect()->action('Empleados\DocumentosController@listarArchivosEmpleado', ['id' => $idEmpleado]);
            }else{
        Alert::error('No se encontro Documento',$empleado->nombre . ' ' .$empleado->apellido.' del servidor ')->autoClose(2000)->iconHtml('<i class="fa fa-trash-o"></i>');
        return redirect()->action('Empleados\DocumentosController@listarArchivosEmpleado', ['id' => $idEmpleado]);
            }

        }else{
        Alert::error('ERROR', '¡No se ha podido guardar en el servidor!')->autoClose(2000);
        return redirect()->action('Empleados\DocumentosController@listarArchivosEmpleado', ['id' => $idEmpleado]);
        }
    }
        $documentoAntiguo->descripcion = $request->descripcion;
        $documentoAntiguo->update();
        Alert::warning('Actualizar descripcion', $empleado->nombre . ' ' .$empleado->apellido.' de forma existosa')->autoClose(2000)->iconHtml('<i class="fa fa-pencil"></i>');
     return redirect()->action('Empleados\DocumentosController@listarArchivosEmpleado', ['id' => $idEmpleado]);
    }


    public function eliminarArchivosEmpleado($id){
        // elimina los archivos del empleado seleccionado
        $documento = Documento::find($id);
        $empleado = Empleado::find($documento->empleado_id);
        $idEmpleado = $empleado->id_empleado;
        $documento->delete();
        Alert::error('Eliminar Documento', $documento->nombre_documento . ' de forma existosa')->autoClose(2000)->iconHtml('<i class="fa fa-trash-o"></i>');
    return redirect()->action('Empleados\DocumentosController@listarArchivosEmpleado', ['id'=>$idEmpleado]);


    }

    public function mostrarArchivosEmpleado($id){
        //muestra los archivos del empleado
    $documento = Documento::find($id);
    if($documento==null){
        Alert::error('ERROR', '¡No se encuentra el documento!')->autoClose(2000);
    return redirect()->action('Empleados\EmpleadosController@listarEmpleados');
    }else
        {
         $empleado = Empleado::find($documento->empleado_id);
        $idEmpleado = $empleado->id_empleado;

     //verificamos si el archivo existe y lo retornamos
         if (Storage::exists($documento->ruta))
         {
          
           return Storage::response($documento->ruta);

         }   
    }
}
   

}
