<?php

namespace App\Http\Controllers\Empleados;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Laracasts\Flash\Flash;
use Validator;
use App\Modelos\Empleado\Centro;
use App\Http\Requests\CentroRequest;
use App\Http\Requests\EditarCentroRequest;
use RealRashid\SweetAlert\Facades\Alert;
use Auth;
use DB;

class CentrosController extends Controller
{
    //funcion para los permisos de centros
     public function __construct(){

    $this->middleware(['permission:crear.catalogos'], ['only'    => ['crearCentros', 'guardarCentros']]);
    $this->middleware(['permission:editar.catalogos'], ['only'   => ['editarCentros', 'actualiz
        arCentros']]);
    $this->middleware(['permission:eliminar.catalogos'], ['only' => 'eliminarCentros']);
    $this->middleware(['permission:consultar.catalogos'], ['only'   => 'listarCentros']);
    $this->middleware(['permission:habilitar.catalogos'], ['only'   => 'habilitarCentros']); 
    $this->middleware(['permission:asignar.centro'], ['only'   => 'asignarCentros']);  
    $this->middleware(['permission:programar.centro'], ['only'   => 'programarCentros']);
    $this->middleware(['permission:activar.centro'], ['only'   => ['activarCentros', 'activarFinSemana', 'activarAsueto']]);
    $this->middleware(['permission:desactivar.centro'], ['only'   => ['desactivarCentros', 'desactivarFinSemana', 'desactivarAsueto']]);

 
    }  
    //
    public function crearCentros(Request $request){

        return view('empleados.gestionCentros.crear'); //llama a la vista de crear Centros

    }

    public function guardarCentros(CentroRequest $request){
        //guarda los datos del centro
        $centro = new Centro();
        $centro->centro = $request->centro;
        $centro->abreviatura = $request->abreviatura;
        $centro->encargado = $request->encargado;
        $centro->estado=1; //campo estado para habilitar o deshabilitar un centro Es decir eliminar
        $centro->activo = 0; //campo que activa los horarios de actividad de centros de viaticos
        $centro->fin_semana = 1; //campo que activa la validacion de no permitir ingresar programaciones en fin de semana
        $centro->asueto = 1; //campo que activa la validacion de no permitir ingresar programaciones en asuetos
        $centro->save();
        Alert::success('Registrar Centro', $centro->centro . ' de forma existosa')->autoClose(2000);
        return redirect()->action('Empleados\CentrosController@listarCentros');
    }

    public function listarCentros(){

        //consulta que tipo de rol tiene el usuario que esta ingresando al sistema
   if(Auth::user()->hasRole('Gestor de Recursos Humanos') || Auth::user()->hasRole('Gestor de Recursos Humanos Regional')){
      $id = Auth::id();//obtenemso el id del usuario autentificado

    $centro = DB::table('admin_usuario as u')->leftJoin('emp_empleado as e', 'e.id_empleado', '=', 'u.empleado_id')->leftJoin('emp_oficina as o', 'o.id_oficina', '=', 'e.oficina_id')->leftJoin('emp_centro as c', 'c.id_centro', '=', 'o.centro_id')->select('c.id_centro', 'c.centro')->where('id_usuario', '=', $id)->first(); //obtenemos el centro de la oficina de ese usuario
        if($centro->id_centro==1 || $centro->id_centro==3){//consulta si el centro es OFICINA CENTRAL u CENTRO II
            $centros = Centro::where('estado', '=', 1)->whereIntegerInRaw('id_centro', [1,3])->get(); //LISTAR CENTROS DE OFICINAS CENTRAL Y/O OFICINA HABILITADOS
            $centroDeshabilitado=Centro::where('estado', '=', 0)->whereIntegerInRaw('id_centro', [1,3])->get(); //ÑISTAR ESOS CENTROS QUE ESTAN DESHABILITADOS
            return view('empleados.gestionCentros.listar', compact('centros','centroDeshabilitado'));  
        }else{ //SI ES CUALQUIERA OTRO CENTRO DE ESE USUARIO AUTENTIFICADO
            $centros = Centro::where('estado', '=', 1)->where('id_centro','=', $centro->id_centro)->get();//LISTAR CENTRO DE ESE USUARIO SOLAMENTE HABILITADO
            $centroDeshabilitado=Centro::where('estado', '=', 0)->where('id_centro','=', $centro->id_centro)->get(); //LISTAR CENTRO DE ESE USUARIO SOLAMENTE DESHABILITADOS
            return view('empleados.gestionCentros.listar', compact('centros','centroDeshabilitado'));
        }//fin del if de identificacion de centros
    }else{
        $centros = Centro::where('estado', '=', 1)->get(); //LISTA TODOS LOS CENTROS PARA EL USUARIO ADMINISTRADOR
        $centroDeshabilitado=Centro::where('estado', '=', 0)->get();
        return view('empleados.gestionCentros.listar', compact('centros','centroDeshabilitado'));
    }



    }

    public function editarCentros($id){
        //carga la vista para editar el centro seleccionado
        $centro = Centro::find($id);
        return view('empleados.gestionCentros.editar', compact('centro'));
    }

    public function actualizarCentros(EditarCentroRequest $request, $id){
        //actualiza los datos del centro seleccionado
        $centro = Centro::find($id);
        $centro->centro = $request->centro;
        $centro->abreviatura = $request->abreviatura;
        $centro->encargado = $request->encargado;
        $centro->update();
        Alert::warning('Actualizar Centro', $centro->centro . ' de forma existosa')->autoClose(2000)->iconHtml('<i class="fa fa-pencil"></i>');

        return redirect()->action('Empleados\CentrosController@listarCentros');
    }


    public function eliminarCentros($id){
        //desactiva el centro seleccionado
    $centro = Centro::find($id); //se busca el Centro que se desea eliminar
    $centro->estado=0;
    $centro->update();
    Alert::error('Eliminar Centro ', $centro->centro . ' de forma existosa')->autoClose(2000)->iconHtml('<i class="fa fa-trash-o"></i>');
     return redirect()->action('Empleados\CentrosController@listarCentros');
    }


    public function habilitarCentros($id){
        //activa nuevamente el centro seleccionado
        $centro = Centro::find($id);
        $centro->estado=1;
        $centro->update();
        Alert::success('Habilitar Centro', $centro->centro . ' de forma existosa')->autoClose(2000);
        return redirect()->action('Empleados\CentrosController@listarCentros');
    }


    public function asignarCentros(){

       
        $centros = Centro::where('estado', '=', 1)->get(); //LISTA TODOS LOS CENTROS PARA EL USUARIO ADMINISTRADOR
        
         return view('viaticos.gestionHorario.listar', compact('centros'));
   
}

    public function programarCentros($id){
        //muesta la vista de programacion de centro
        $centro = Centro::find($id);

        return view('viaticos.gestionHorario.editar', compact('centro'));
    }

    public function horariosCentros(Request $request, $id){
        //guarda el horario para el centro
        $centro = Centro::find($id);
        $centro->fecha_inicio = $request->dia_inicial;
        $centro->hora_inicio = $request->hora_inicio;
        $centro->fecha_fin = $request->dia_final;
        $centro->hora_fin = $request->hora_final;
        $centro->activo = 1;
        $centro->update();

        Alert::warning('Actualizar Centro', $centro->centro . ' de forma existosa')->autoClose(2000)->iconHtml('<i class="fa fa-pencil"></i>');

        return redirect()->action('Empleados\CentrosController@asignarCentros');

    }

    public function activarCentros($id){
        //activar la validacion del centro
        $centro = Centro::find($id);
         $centro->activo = 1;
        $centro->update();

   Alert::success('Ha activado el Centro', $centro->centro . ' de forma existosa')->autoClose(2000);
     return redirect()->action('Empleados\CentrosController@asignarCentros');
    }

    public function desactivarCentros($id){ 
        //desactiva la validacion del centro
         $centro = Centro::find($id);
         $centro->activo = 0;
         $centro->update();
        Alert::error('Ha desactivado el Centro ', $centro->centro . ' de forma existosa')->autoClose(2000)->iconHtml('<i class="fa fa-trash-o"></i>');

         return redirect()->action('Empleados\CentrosController@asignarCentros');

    }


    public function activarFinSemana($id){
        //activa la validacion de fin de semana
        $centro = Centro::find($id);
        $centro->fin_semana=1;
        $centro->update();
        Alert::success('Ha activado la validacion de Fin de Semana del Centro ', $centro->centro . ' de forma existosa')->autoClose(2000);

         return redirect()->action('Empleados\CentrosController@asignarCentros');
    }

    public function desactivarFinSemana($id){
        //desactiva la validacion de fin de semana
        $centro = Centro::find($id);
        $centro->fin_semana=0;
        $centro->update();
        Alert::error('Ha desactivado la validación de Fin de Semana del Centro ', $centro->centro . ' de forma existosa')->autoClose(2000)->iconHtml('<i class="fa fa-trash-o"></i>');

         return redirect()->action('Empleados\CentrosController@asignarCentros');

    }


    public function activarAsueto($id){ 
        //activa la validacion de los dias de asueto
        $centro = Centro::find($id);
        $centro->asueto=1;
        $centro->update();
        Alert::success('Ha activado la validacion de Asueto del Centro ', $centro->centro . ' de forma existosa')->autoClose(2000);

         return redirect()->action('Empleados\CentrosController@asignarCentros');

    }

    public function desactivarAsueto($id){
        //desactiva la validacion de los dias de asueto
        $centro = Centro::find($id);
        $centro->asueto=0;
        $centro->update();
        Alert::error('Ha desactivado la validación de Asueto del Centro ', $centro->centro . ' de forma existosa')->autoClose(2000)->iconHtml('<i class="fa fa-trash-o"></i>');

         return redirect()->action('Empleados\CentrosController@asignarCentros');
    }
}
