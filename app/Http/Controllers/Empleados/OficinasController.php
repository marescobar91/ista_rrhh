<?php

namespace App\Http\Controllers\Empleados;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Laracasts\Flash\Flash;
use Validator;
use App\Modelos\Empleado\Oficina;
use App\Modelos\Empleado\UnidadAdministrativa;
use App\Modelos\Empleado\Centro;
use App\Http\Requests\OficinaRequest;
use App\Http\Requests\EditarOficinaRequest; 
use RealRashid\SweetAlert\Facades\Alert;

class OficinasController extends Controller
{
    //funcion para los permisos de oficina
    public function __construct(){

    $this->middleware(['permission:crear.catalogos'], ['only'    => ['crearOficinas', 'guardarOficinas']]);
    $this->middleware(['permission:editar.catalogos'], ['only'   => ['editarOficinas', 'actualizarOficinas']]);
    $this->middleware(['permission:eliminar.catalogos'], ['only' => 'eliminarOficinas']);
    $this->middleware(['permission:consultar.catalogos'], ['only'   => 'listarOficinas']);
    $this->middleware(['permission:habilitar.catalogos'], ['only'   => 'habilitarOficinas']);

    }
    
    public function crearOficinas(Request $request){
        //muestra la vista para crear oficina
    	$unidadAdministrativa = UnidadAdministrativa::where('estado', '=', 1)->orderBy('id_unidad_administrativa', 'DESC')->get();
        $centro = Centro::where('estado', '=', 1)->orderBy('id_centro', 'DESC')->get();
    	return view('empleados.gestionOficinas.crear', compact('unidadAdministrativa','centro')); //llama a la vista de crear Oficinas

    }

    public function guardarOficinas(OficinaRequest $request){
        //guarda los datos de la oficina
    	$oficina = new Oficina;
    	$oficina->unidad_administrativa_id = $request->unidad_administrativa;
    	$oficina->oficina = $request->oficina;
        $oficina->telefono_oficina = $request->telefono_oficina;
        $oficina->centro_id = $request->centro;
    	$oficina->estado=1;
    	$oficina->save();
        Alert::success('Registrar Oficina', $oficina->oficina . ' de forma existosa')->autoClose(2000);
    	return redirect()->action('Empleados\OficinasController@listarOficinas');
    }

    public function listarOficinas(){
        //muestra lista las oficinas 
    	$oficina = Oficina::where('estado', '=', 1)->orderBy('id_oficina', 'DESC')->get();
        $oficinaDeshabilitado = Oficina::where('estado', '=', 0)->orderBy('id_oficina', 'DESC')->get();
    	return view('empleados.gestionOficinas.listar', compact('oficina','oficinaDeshabilitado'));    	
    }

    public function editarOficinas($id){
        //carga la vista para editar las oficinas
    	$oficina = Oficina::find($id);
    	$unidadAdministrativa = UnidadAdministrativa::where('estado', '=', 1)->orderBy('id_unidad_administrativa', 'DESC')->get();
        $centro = Centro::where('estado', '=', 1)->orderBy('id_centro', 'DESC')->get();
    	return view('empleados.gestionOficinas.editar', compact('oficina', 'unidadAdministrativa','centro'));
    }

    public function actualizarOficinas(EditarOficinaRequest $request, $id){
        //actualiza los datos de las oficinas
    	$oficina = Oficina::find($id);
    	$oficina->unidad_administrativa_id = $request->unidad_administrativa;
    	$oficina->oficina = $request->oficina;
        $oficina->telefono_oficina = $request->telefono_oficina;
        $oficina->centro_id = $request->centro;
        $oficina->telefono_oficina = $request->telefono_oficina;
    	$oficina->update();
        Alert::warning('Actualizar Oficina', $oficina->oficina . ' de forma existosa')->autoClose(2000)->iconHtml('<i class="fa fa-pencil"></i>');

    	return redirect()->action('Empleados\OficinasController@listarOficinas');
    }


    public function eliminarOficinas($id){
        //desactiva la oficina seleccionada
        $oficina = Oficina::find($id); //se busca el oficina que se desea eliminar
    	$oficina->estado=0;
    	$oficina->update();
        Alert::error('Eliminar Oficina', $oficina->oficina . ' de forma existosa')->autoClose(2000)->iconHtml('<i class="fa fa-trash-o"></i>');

    	 return redirect()->action('Empleados\OficinasController@listarOficinas');
    }


    public function habilitarOficinas($id){
        //activa nuevamente la oficina seleccionada
    	$oficina = Oficina::find($id);
    	$oficina->estado=1;
    	$oficina->update();
        Alert::success('Habilitar Oficina', $oficina->oficina . ' de forma existosa')->autoClose(2000);
    	return redirect()->action('Empleados\OficinasController@listarOficinas');
    }
    
}
