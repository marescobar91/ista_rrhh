<?php

namespace App\Http\Controllers\Empleados;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Laracasts\Flash\Flash;
use Validator;
use App\Modelos\Empleado\TipoPlaza;
use App\Http\Requests\TipoPlazaRequest;
use App\Http\Requests\EditarTipoPlazaRequest; 
use RealRashid\SweetAlert\Facades\Alert;

class TiposPlazaController extends Controller
{
    //funcion para los permisos de tipos de plaza
    public function __construct(){

    $this->middleware(['permission:crear.catalogos'], ['only'    => ['crearTiposPlaza', 'guardarTiposPlaza']]);
    $this->middleware(['permission:editar.catalogos'], ['only'   => ['editarTiposPlaza', 'actualizarTiposPlaza']]);
    $this->middleware(['permission:eliminar.catalogos'], ['only' => 'eliminarTiposPlaza']);
    $this->middleware(['permission:consultar.catalogos'], ['only'   => 'listarTiposPlaza']);
    $this->middleware(['permission:habilitar.catalogos'], ['only'   => 'habilitarTiposPlaza']);

    }
    //
    public function crearTiposPlaza(Request $request){
        //carga la vista de crear tipos de plaza
    	return view('empleados.gestionTiposPlaza.crear'); //llama a la vista de crear TiposPlaza

    }

    public function guardarTiposPlaza(TipoPlazaRequest $request){
        //guardar los datos de tipos de plaza
    	$tipoPlaza = new TipoPlaza();
    	$tipoPlaza->tipo_plaza = $request->tipo_plaza;
    	$tipoPlaza->estado=1;
    	$tipoPlaza->save();
        Alert::success('Registrar Tipo de Plaza', $tipoPlaza->tipo_plaza . ' de forma existosa')->autoClose(2000);
    	return redirect()->action('Empleados\TiposPlazaController@listarTiposPlaza');
    }

    public function listarTiposPlaza(){
        //muestra el listado de tipo de plaza
    	$tipoPlaza = TipoPlaza::where('estado', '=', 1)->orderBy('id_tipo_plaza', 'DESC')->get();
        $tipoPlazaDeshabilitado = TipoPlaza::where('estado', '=', 0)->orderBy('id_tipo_plaza', 'DESC')->get();
    	return view('empleados.gestionTiposPlaza.listar', compact('tipoPlaza','tipoPlazaDeshabilitado'));    	
    }

    public function editarTiposPlaza($id){
        //muestra la vista de editar tipos de plaza
    	$tipoPlaza = TipoPlaza::find($id); 
    	return view('empleados.gestionTiposPlaza.editar', compact('tipoPlaza'));
    }

    public function actualizarTiposPlaza(EditarTipoPlazaRequest $request, $id){
        //actualiza los datos de los tipos de plaza
    	$tipoPlaza = TipoPlaza::find($id);
    	$tipoPlaza->tipo_plaza = $request->tipo_plaza;
    	$tipoPlaza->update();
        Alert::warning('Actualizar Tipo de Plaza', $tipoPlaza->tipo_plaza . ' de forma existosa')->autoClose(2000)->iconHtml('<i class="fa fa-pencil"></i>');

    	return redirect()->action('Empleados\TiposPlazaController@listarTiposPlaza');
    }


    public function eliminarTiposPlaza($id){
        //desactiva el tipo de plaza seleccionado
    $tipoPlaza = TipoPlaza::find($id); //se busca el tipoPlaza que se desea eliminar
	$tipoPlaza->estado=0;
	$tipoPlaza->update();
    Alert::error('Eliminar Tipo de Plaza', $tipoPlaza->tipo_plaza . ' de forma existosa')->autoClose(2000)->iconHtml('<i class="fa fa-trash-o"></i>');
	 return redirect()->action('Empleados\TiposPlazaController@listarTiposPlaza');
    }


    public function habilitarTiposPlaza($id){
        //activa nuevamente el tipo de plaza seleccionado
    	$tipoPlaza = tipoPlaza::find($id);
    	$tipoPlaza->estado=1;
    	$tipoPlaza->update();
        Alert::success('Habilitar Tipo de Plaza', $tipoPlaza->tipo_plaza . ' de forma existosa')->autoClose(2000);
    	return redirect()->action('Empleados\TiposPlazaController@listarTiposPlaza');
    }

}
