<?php

namespace App\Http\Controllers\Empleados;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Laracasts\Flash\Flash;
use Validator;
use App\Modelos\Empleado\CargoHacienda;
use App\Http\Requests\CargoHaciendaRequest;
use App\Http\Requests\EditarCargoHaciendaRequest;
use RealRashid\SweetAlert\Facades\Alert;

class CargosHaciendaController extends Controller
{
    //funcion para los permisos para los cargos de hacienda
    public function __construct(){

    $this->middleware(['permission:crear.catalogos'], ['only'    => ['crearCargosHacienda', 'guardarCargosHacienda']]);
    $this->middleware(['permission:editar.catalogos'], ['only'   => ['editarCargosHacienda', 'actualizarCargosHacienda']]);
    $this->middleware(['permission:eliminar.catalogos'], ['only' => 'eliminarCargosHacienda']);
    $this->middleware(['permission:consultar.catalogos'], ['only'   => 'listarCargosHacienda']); 
    $this->middleware(['permission:habilitar.catalogos'], ['only'   => 'habilitarCargosHacienda']);

    }
    public function crearCargosHacienda(Request $request){

    	return view('empleados.gestionCargosHacienda.crear'); //llama a la vista de crear CargosHacienda

    }

    public function guardarCargosHacienda(CargoHaciendaRequest $request){

    	$cargoHacienda = new CargoHacienda();
    	$cargoHacienda->cargo_hacienda = $request->cargo_hacienda;
    	$cargoHacienda->estado=1;
    	$cargoHacienda->save();
        Alert::success('Registrar cargo hacienda', $cargoHacienda->cargo_hacienda . ' de forma existosa')->autoClose(2000);
    	return redirect()->action('Empleados\CargosHaciendaController@listarCargosHacienda');
    }

    public function listarCargosHacienda(){

    	$cargoHacienda = CargoHacienda::where('estado', '=', 1)->orderBy('id_cargo_hacienda', 'DESC')->get();
        $cargoHaciendaDeshabilitado = CargoHacienda::where('estado', '=', 0)->orderBy('id_cargo_hacienda', 'DESC')->get();
    	return view('empleados.gestionCargosHacienda.listar', compact('cargoHacienda','cargoHaciendaDeshabilitado'));    	
    }

    public function editarCargosHacienda($id){

    	$cargo_hacienda = CargoHacienda::find($id);
    	return view('empleados.gestionCargosHacienda.editar', compact('cargo_hacienda'));
    }

    public function actualizarCargosHacienda(EditarCargoHaciendaRequest $request, $id){

    	$cargoHacienda = CargoHacienda::find($id);
    	$cargoHacienda->cargo_hacienda = $request->cargo_hacienda;
    	$cargoHacienda->update();
        Alert::warning('Actualizar Cargo Hacienda', $cargoHacienda->cargo_hacienda . ' de forma existosa')->autoClose(2000)->iconHtml('<i class="fa fa-pencil"></i>');
    	return redirect()->action('Empleados\CargosHaciendaController@listarCargosHacienda');
    }


    public function eliminarCargosHacienda($id){

    $cargoHacienda = CargoHacienda::find($id); //se busca el cargosHacienda que se desea eliminar
	$cargoHacienda->estado=0;
	$cargoHacienda->update();
    Alert::error('Eliminar Cargo Hacienda', $cargoHacienda->cargo_hacienda  . ' de forma existosa')->autoClose(2000)->iconHtml('<i class="fa fa-trash-o"></i>');
	 return redirect()->action('Empleados\CargosHaciendaController@listarCargosHacienda');
    }

    public function habilitarCargosHacienda($id){

    	$cargoHacienda = CargoHacienda::find($id);
    	$cargoHacienda->estado=1;
    	$cargoHacienda->update();
        Alert::success('Habilitar Cargo Haienda', $cargoHacienda->cargo_hacienda . ' de forma existosa')->autoClose(2000);
    	return redirect()->action('Empleados\CargosHaciendaController@listarCargosHacienda');
    }
}
