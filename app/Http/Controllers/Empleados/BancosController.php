<?php

namespace App\Http\Controllers\Empleados;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Laracasts\Flash\Flash;
use Validator;
use App\Modelos\Empleado\Banco;
use App\Http\Requests\BancoRequest;
use App\Http\Requests\EditarBancoRequest;
use RealRashid\SweetAlert\Facades\Alert;



class BancosController extends Controller
{
    //funcion para los permisos de bancos
    public function __construct(){

    $this->middleware(['permission:crear.catalogos'], ['only'    => ['crearBancos', 'guardarBancos']]);
    $this->middleware(['permission:editar.catalogos'], ['only'   => ['editarBancos', 'actualizarBancos']]);
    $this->middleware(['permission:eliminar.catalogos'], ['only' => 'eliminarBancos']);
    $this->middleware(['permission:consultar.catalogos'], ['only'   => 'listarBancos']);
    $this->middleware(['permission:habilitar.catalogos'], ['only'   => 'habilitarBancos']);

    }

    public function crearBancos(Request $request){

    	return view('empleados.gestionBancos.crear'); //llama a la vista de crear bancos

    }

    public function guardarBancos(BancoRequest $request){
        //guarda los datos del nuevo banco
    	$banco = new Banco();
    	$banco->nombre_banco = $request->nombre_banco;
    	$banco->estado=1;
    	$banco->save();
        Alert::success('Registrar Banco', $banco->nombre_banco . ' de forma existosa')->autoClose(2000);
    	return redirect()->action('Empleados\BancosController@listarBancos');
    }

    public function listarBancos(){
        //muestra el listado de bancos
    	$banco = Banco::where('estado', '=', 1)->orderBy('id_banco', 'DESC')->get();
        $bancoDeshabilitado = Banco::where('estado', '=', 0)->orderBy('id_banco', 'DESC')->get();
    	return view('empleados.gestionBancos.listar', compact('banco','bancoDeshabilitado'));    	
    }

    public function editarBancos($id){
        //carga la vista de la edicion de bancos
    	$banco = Banco::find($id);
    	return view('empleados.gestionBancos.editar', compact('banco'));
    }

    public function actualizarBancos(EditarBancoRequest $request, $id){
        //editar los datos del banco obtenido
    	$banco = Banco::find($id);
    	$banco->nombre_banco = $request->nombre_banco;
    	$banco->update();
        Alert::warning('Actualizar Banco', $banco->nombre_banco . ' de forma existosa')->autoClose(2000)->iconHtml('<i class="fa fa-pencil"></i>');
    	return redirect()->action('Empleados\BancosController@listarBancos');
    }


    public function eliminarBancos($id){
        //desactiva el banco seleccionado
    $banco = Banco::find($id); //se busca el banco que se desea eliminar
	$banco->estado=0;
	$banco->update();
    Alert::error('Eliminar Banco', $banco->nombre_banco . ' de forma existosa')->autoClose(2000)->iconHtml('<i class="fa fa-trash-o"></i>');
	 return redirect()->action('Empleados\BancosController@listarBancos');
    }


    public function habilitarBancos($id){
        //activa nuevamente el banco seleccionado
    	$banco = Banco::find($id);
    	$banco->estado=1;
    	$banco->update();
        Alert::success('Habilitar Banco', $banco->nombre_banco . ' de forma existosa')->autoClose(2000);
    	return redirect()->action('Empleados\BancosController@listarBancos');
    }
}
