<?php

namespace App\Http\Controllers\Empleados;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Laracasts\Flash\Flash;
use Validator;
use App\Modelos\Empleado\NivelAcademico;
use App\Http\Requests\NivelAcademicoRequest;
use App\Http\Requests\EditarNivelAcademicoRequest;
use RealRashid\SweetAlert\Facades\Alert;

class NivelesAcademicosController extends Controller
{   
    //funcion para permisos de niveles academicos
    public function __construct(){

    $this->middleware(['permission:crear.catalogos'], ['only'    => ['crearNivelesAcademicos', 'guardarNivelesAcademicos']]);
    $this->middleware(['permission:editar.catalogos'], ['only'   => ['editarNivelesAcademicos', 'actualizarNivelesAcademicos']]);
    $this->middleware(['permission:eliminar.catalogos'], ['only' => 'eliminarNivelesAcademicos']);
    $this->middleware(['permission:consultar.catalogos'], ['only'   => 'listarNivelesAcademicos']);
    $this->middleware(['permission:habilitar.catalogos'], ['only'   => 'habilitarNivelesAcademicos']);

    }

    public function crearNivelesAcademicos(Request $request){

    	return view('empleados.gestionNivelesAcademicos.crear'); //llama a la vista de crear NivelesAcademicos

    }

    public function guardarNivelesAcademicos(NivelAcademicoRequest $request){
        //guarda los niveles academicos
    	$nivel_academico = new NivelAcademico();
    	$nivel_academico->nivel_academico = $request->nivel_academico;
    	$nivel_academico->estado=1;
    	$nivel_academico->save();

        Alert::success('Registrar Nivel Academico', $nivel_academico->nivel_academico . ' de forma existosa')->autoClose(2000);
    	return redirect()->action('Empleados\NivelesAcademicosController@listarNivelesAcademicos');
    }

    public function listarNivelesAcademicos(){
        //muestra el listado de niveles academicos
    	$nivel_academico = NivelAcademico::where('estado', '=', 1)->orderBy('id_nivel_academico', 'DESC')->get();
        $nivel_academicoDeshabilitado = NivelAcademico::where('estado', '=', 0)->orderBy('id_nivel_academico', 'DESC')->get();
    	return view('empleados.gestionNivelesAcademicos.listar', compact('nivel_academico','nivel_academicoDeshabilitado'));    	
    }

    public function editarNivelesAcademicos($id){
        // muestra la vista para editar los niveles academicos
    	$nivel_academico = NivelAcademico::find($id);
    	return view('empleados.gestionNivelesAcademicos.editar', compact('nivel_academico'));
    }

    public function actualizarNivelesAcademicos(EditarNivelAcademicoRequest $request, $id){
        //actualiza los datos de niveles academicos
    	$nivel_academico = NivelAcademico::find($id);
    	$nivel_academico->nivel_academico = $request->nivel_academico;
    	$nivel_academico->update();
        Alert::warning('Actualizar Nivel Academico', $nivel_academico->nivel_academico . ' de forma existosa')->autoClose(2000)->iconHtml('<i class="fa fa-pencil"></i>');

    	return redirect()->action('Empleados\NivelesAcademicosController@listarNivelesAcademicos');
    }


    public function eliminarNivelesAcademicos($id){

    $nivel_academico = NivelAcademico::find($id); //se busca el nivel_academico que se desea eliminar
	$nivel_academico->estado=0;
	$nivel_academico->update();
    Alert::error('Eliminar Nivel Academico', $nivel_academico->nivel_academico . ' de forma existosa')->autoClose(2000)->iconHtml('<i class="fa fa-trash-o"></i>');

	 return redirect()->action('Empleados\NivelesAcademicosController@listarNivelesAcademicos');
    }

    public function habilitarNivelesAcademicos($id){

    	$nivel_academico = NivelAcademico::find($id);
    	$nivel_academico->estado=1;
    	$nivel_academico->update();
        Alert::success('Habilitar Nivel Académico', $nivel_academico->nivel_academico . ' de forma existosa')->autoClose(2000);
    	return redirect()->action('Empleados\NivelesAcademicosController@listarNivelesAcademicos');
    }
}
