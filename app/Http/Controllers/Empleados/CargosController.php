<?php

namespace App\Http\Controllers\Empleados;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Laracasts\Flash\Flash;
use Validator;
use App\Modelos\Empleado\Cargo;
use App\Http\Requests\CargoRequest;
use App\Http\Requests\EditarCargoRequest;
use RealRashid\SweetAlert\Facades\Alert;

class CargosController extends Controller
{   
    //funcion para los permisos para los cargos
    public function __construct(){

    $this->middleware(['permission:crear.catalogos'], ['only'    => ['crearCargos', 'guardarCargos']]);
    $this->middleware(['permission:editar.catalogos'], ['only'   => ['editarCargos', 'actualizarCargos']]);
    $this->middleware(['permission:eliminar.catalogos'], ['only' => 'eliminarCargos']);
    $this->middleware(['permission:consultar.catalogos'], ['only'   => 'listarCargos']);
    $this->middleware(['permission:habilitar.catalogos'], ['only'   => 'habilitarCargos']);

    }

    public function crearCargos(Request $request)
    {
    	return view('empleados.gestionCargos.crear'); //llama a la vista de crear cargos
    }

    public function guardarCargos(CargoRequest $request){
        //guarda los datos del cargo
    	$cargo = new Cargo();
    	$cargo->cargo = $request->nombre_cargo;
    	$cargo->estado=1;
    	$cargo->save();
        Alert::success('Registrar Cargo', $cargo->cargo . ' de forma existosa')->autoClose(2000);
    	return redirect()->action('Empleados\CargosController@listarCargos');
    }

    public function listarCargos(){
        //muestra el listado de cargos
    	$cargo = Cargo::where('estado', '=', 1)->orderBy('id_cargo', 'DESC')->get();
        $cargoDeshabilitado = Cargo::where('estado', '=', 0)->orderBy('id_cargo', 'DESC')->get();
    	return view('empleados.gestionCargos.listar', compact('cargo','cargoDeshabilitado'));    	
    }

    public function editarCargos($id){
        //carga la vista para la edicion del cargo
    	$cargo = Cargo::find($id);
    	return view('empleados.gestionCargos.editar', compact('cargo'));
    }

    public function actualizarCargos(EditarCargoRequest $request, $id){
        //edita el cargo obtenido
    	$cargo = Cargo::find($id);
    	$cargo->cargo = $request->nombre_cargo;
    	$cargo->update();
        Alert::warning('Actualizar Cargo', $cargo->cargo . ' de forma existosa')->autoClose(2000)->iconHtml('<i class="fa fa-pencil"></i>');
    	return redirect()->action('Empleados\CargosController@listarCargos');
    }

    public function eliminarCargos($id){
        //desactiva el cargo seleccionado
    $cargo = Cargo::find($id); //se busca el cargo que se desea eliminar
	$cargo->estado=0;
	$cargo->update();
    Alert::error('Eliminar Cargo', $cargo->cargo  . ' de forma existosa')->autoClose(2000)->iconHtml('<i class="fa fa-trash-o"></i>');
	 return redirect()->action('Empleados\CargosController@listarCargos');
    }


    public function habilitarCargos($id){
        //activa nuevamente el cargo seleccionado
    	$cargo = Cargo::find($id);
    	$cargo->estado=1;
    	$cargo->update();
        Alert::success('Habilitar Cargo', $cargo->cargo . ' de forma existosa')->autoClose(2000);
    	return redirect()->action('Empleados\CargosController@listarCargos');
    }

}
