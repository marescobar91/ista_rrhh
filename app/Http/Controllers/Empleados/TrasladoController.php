<?php

namespace App\Http\Controllers\Empleados;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Modelos\Empleado\Empleado;
use App\Modelos\Empleado\Oficina;
use App\Modelos\Empleado\Cargo;
use App\Modelos\Empleado\CargoMP;
use App\Modelos\Empleado\CargoHacienda;
use App\Modelos\Empleado\UnidadAdministrativa;
use App\Modelos\Empleado\Centro;
use Carbon\Carbon;
use Laracasts\Flash\Flash;
use App\Modelos\Empleado\Historial;
use Auth;
use DB;
use RealRashid\SweetAlert\Facades\Alert;


class TrasladoController extends Controller
{
    //funcion para los permisos de traslatado controller
    public function __construct(){

   $this->middleware(['permission:crear.traslados'], ['only'    => ['crearTraslados', 'guardarTraslados']]);
    $this->middleware(['permission:consultar.traslados'], ['only'   => 'listarTraslados']);


    }
    public function crearTraslados($id){
      //vista para crear traslados por el empleado seleccionado
    	$empleado = Empleado::find($id);
    	$cargoFuncional = Cargo::where('estado', '=', 1)->orderBy('id_cargo', 'DESC')->get();
        $cargoNominal = Cargo::where('estado', '=', 1)->orderBy('id_cargo', 'DESC')->get();
        $cargoMP = CargoMP::where('estado', '=', 1)->orderBy('id_cargo_mp', 'DESC')->get();
        $unidad = UnidadAdministrativa::where('estado', '=', 1)->orderBy('id_unidad_administrativa', 'DESC')->get();
        $cargoHacienda = CargoHacienda::where('estado', '=', 1)->orderBy('id_cargo_hacienda', 'DESC')->get();
         $oficinaActual = $empleado->oficina_id;    //se obtiene la oficina actual del empleado
       	$unidadActual = $empleado->oficina->unidad_administrativa_id;   //se obtiene la unidad administrativa actual del empleado
        $centro = Centro::where('estado', '=', 1)->orderBy('id_centro', 'DESC')->get();
        $centroActual = $empleado->oficina->centro_id;    //se obtiene el centro actual del empleado
    	return view('empleados.gestionTraslados.crear', compact('empleado', 'cargoFuncional', 'cargoNominal', 'cargoMP', 'unidad', 'cargoHacienda','oficinaActual','unidadActual','centro','centroActual'));

    }

    public function obtenerOficinas(Request $request)
      {
        //obtiene las oficinas de los centro y unidades correspondientes
       if ($request->ajax()) {
          $oficinas = DB::table('emp_oficina')
          ->where('unidad_administrativa_id', $request->unidad_administrativa)
          ->where('centro_id',$request->centro)
          ->get();

          foreach ($oficinas as $oficina) {
            $oficinaArray[$oficina->id_oficina] = $oficina->oficina;
          }
          return response()->json($oficinaArray);
        }
      }

      public function guardarTraslados(Request $request, $id){
        //guarda la informacion de Traslados
      

      $empleado = Empleado::find($id);
      $empleado->salario_actual=$request->salario_actual;
      $empleado->fecha_traslado=Carbon::parse($request->fecha_traslado)->format('Y-m-d');
      $empleado->oficina_id=$request->oficina;
      $empleado->cargo_mp_id = $request->cargoMP;
      $empleado->cargo_hacienda_id = $request->cargoH;  
      $empleado->cargo_nominal_id = $request->cargoN;
      $empleado->cargo_funcional_id = $request->cargoF;
     
	    $empleado->update();

	    $idEmpleado = $empleado->id_empleado;
      $accion = "Traslado";


       return redirect()->action('Empleados\TrasladoController@guardarHistorial', ['id' => $idEmpleado, 'accion'=>$accion]);
   }

     public function listarTraslados($id){
     	//aqui seria listar los de un empleado. 
     	$empleado = Empleado::find($id);
     	$traslados = Historial::where('empleado_id', '=', $empleado->id_empleado)->whereNotNull('traslado')->get();
      //dd($traslados);
     	return view('empleados.gestionTraslados.listar')
      ->with('traslados', $traslados)
      ->with('empleado', $empleado);  
     }



     public function guardarHistorial($id, $accion){
      //guarda el historial del empleado

     	$empleado = Empleado::find($id);
       //obtiene el empleado
     	$usuario_id = Auth::id();   // obtenemos el usuario autentificado
      $fecha = Carbon::now(new \DateTimeZone('America/El_Salvador')); 
      $fechaActual =  $fecha->format('Y-m-d');  

      if($accion== 'Crear'){
         $historial= [$empleado->id_empleado, $usuario_id, $empleado->municipio_id,$empleado->nivel_academico_id,$empleado->cargo_nominal_id,$empleado->tipo_plaza_id,$empleado->estado_civil_id, $empleado->banco_id,$empleado->cargo_funcional_id,$empleado->oficina_id,$empleado->cargo_mp_id,$empleado->cargo_hacienda_id,$empleado->codigo_empleado,$empleado->nombre,$empleado->apellido,$empleado->fecha_traslado,$empleado->pin_empleado,$empleado->numero_expediente,$empleado->tipo_documento,$empleado->numero_documento,$empleado->fecha_nacimiento,$empleado->lugar_nacimiento,$empleado->nit,$empleado->isss,$empleado->prevision,$empleado->nup,$empleado->direccion,$empleado->telefono,$empleado->correo,$empleado->nombre_contacto,$empleado->telefono_contacto,$empleado->sexo,$empleado->fecha_ingreso,$empleado->fecha_reingreso,$empleado->salario_inicial,$empleado->salario_actual,$empleado->cuenta_banco,$empleado->cifra_presupuestaria,$empleado->numero_acuerdo,$empleado->numero_partida,$empleado->numero_subpartida,$empleado->afiliado_sindicato,$empleado->rendir_probidad,$empleado->es_jefe,$empleado->marca,$empleado->autorizado_estudio,$empleado->pensionado,$empleado->enviar_jefe,$empleado->pasivo,$empleado->activo,$empleado->gifcard,null,null,null,$empleado->created_at,$empleado->updated_at, $empleado->guardado,$empleado->created_at];
      
      }elseif ($accion == 'Traslado') {
         $historial= [$empleado->id_empleado, $usuario_id, $empleado->municipio_id,$empleado->nivel_academico_id,$empleado->cargo_nominal_id,$empleado->tipo_plaza_id,$empleado->estado_civil_id, $empleado->banco_id,$empleado->cargo_funcional_id,$empleado->oficina_id,$empleado->cargo_mp_id,$empleado->cargo_hacienda_id,$empleado->codigo_empleado,$empleado->nombre,$empleado->apellido,$empleado->fecha_traslado,$empleado->pin_empleado,$empleado->numero_expediente,$empleado->tipo_documento,$empleado->numero_documento,$empleado->fecha_nacimiento,$empleado->lugar_nacimiento,$empleado->nit,$empleado->isss,$empleado->prevision,$empleado->nup,$empleado->direccion,$empleado->telefono,$empleado->correo,$empleado->nombre_contacto,$empleado->telefono_contacto,$empleado->sexo,$empleado->fecha_ingreso,$empleado->fecha_reingreso,$empleado->salario_inicial,$empleado->salario_actual,$empleado->cuenta_banco,$empleado->cifra_presupuestaria,$empleado->numero_acuerdo,$empleado->numero_partida,$empleado->numero_subpartida,$empleado->afiliado_sindicato,$empleado->rendir_probidad,$empleado->es_jefe,$empleado->marca,$empleado->autorizado_estudio,$empleado->pensionado,$empleado->enviar_jefe,$empleado->pasivo,$empleado->activo,$empleado->gifcard,null,null,null,$empleado->created_at,$empleado->updated_at, $empleado->guardado, $fechaActual];
      }elseif ($accion != 'Traslado' || $accion!= 'Crear') {
        $historial= [$empleado->id_empleado, $usuario_id, $empleado->municipio_id,$empleado->nivel_academico_id,$empleado->cargo_nominal_id,$empleado->tipo_plaza_id,$empleado->estado_civil_id, $empleado->banco_id,$empleado->cargo_funcional_id,$empleado->oficina_id,$empleado->cargo_mp_id,$empleado->cargo_hacienda_id,$empleado->codigo_empleado,$empleado->nombre,$empleado->apellido,$empleado->fecha_traslado,$empleado->pin_empleado,$empleado->numero_expediente,$empleado->tipo_documento,$empleado->numero_documento,$empleado->fecha_nacimiento,$empleado->lugar_nacimiento,$empleado->nit,$empleado->isss,$empleado->prevision,$empleado->nup,$empleado->direccion,$empleado->telefono,$empleado->correo,$empleado->nombre_contacto,$empleado->telefono_contacto,$empleado->sexo,$empleado->fecha_ingreso,$empleado->fecha_reingreso,$empleado->salario_inicial,$empleado->salario_actual,$empleado->cuenta_banco,$empleado->cifra_presupuestaria,$empleado->numero_acuerdo,$empleado->numero_partida,$empleado->numero_subpartida,$empleado->afiliado_sindicato,$empleado->rendir_probidad,$empleado->es_jefe,$empleado->marca,$empleado->autorizado_estudio,$empleado->pensionado,$empleado->enviar_jefe,$empleado->pasivo,$empleado->activo,$empleado->gifcard,null,null,null,$empleado->created_at,$empleado->updated_at, $empleado->guardado,null];
      } 
       DB::select('CALL historial_sp (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', $historial);

     	 if($accion=="Crear"){ 

        Alert::success('Registrar Empleado', $empleado->nombre . ' ' .$empleado->apellido  . ' de forma existosa')->autoClose(2000);
     	 	return redirect()->action('Empleados\EmpleadosController@listarEmpleados');

     	 }elseif ($accion =="Editar") {
        Alert::warning('Actualizar Empleado', $empleado->nombre . ' ' .$empleado->apellido . ' de forma existosa')->autoClose(2000)->iconHtml('<i class="fa fa-pencil"></i>');
     	 	 return redirect()->action('Empleados\EmpleadosController@listarEmpleados');

     	 }elseif ($accion=="Desactivar con usuario") {
        Alert::error('Desactivar Empleado', $empleado->nombre . ' ' .$empleado->apellido  . ' con su respectivo usuario, de forma existosa')->autoClose(2000)->iconHtml('<i class="fa fa-trash-o"></i>'); 
        return redirect()->action('Empleados\EmpleadosController@listarEmpleados');

     	 }elseif ($accion=="Desactivar") {
         Alert::error('Desactivar Empleado', $empleado->nombre . ' ' .$empleado->apellido  . ' de forma existosa')->autoClose(2000)->iconHtml('<i class="fa fa-trash-o"></i>'); 
            return redirect()->action('Empleados\EmpleadosController@listarEmpleados');

     	 }elseif ($accion=="Activar con usuario") {
        Alert::success('Activar Empleado', $empleado->nombre . ' ' .$empleado->apellido. 'con su respectivo usuario de forma existosa')->autoClose(2000);
        return redirect()->action('Empleados\EmpleadosController@listarEmpleados');

     	 }elseif ($accion=="Activar") {
        Alert::success('Activar Empleado', $empleado->nombre . ' ' .$empleado->apellido. 'de forma existosa')->autoClose(2000);
        return redirect()->action('Empleados\EmpleadosController@listarEmpleados');

     	 }elseif ($accion=="Traslado") {
     	 	  $idEmpleado = $empleado->id_empleado;
          Alert::success('Traslado de Empleado', $empleado->nombre . ' ' .$empleado->apellido. ' de forma existosa')->autoClose(2000);
     	 	 //
        	return redirect()->action('Empleados\TrasladoController@listarTraslados', ['id' => $idEmpleado]);
     	 }

  
    
     }


}
