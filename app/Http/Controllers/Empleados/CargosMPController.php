<?php

namespace App\Http\Controllers\Empleados;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Laracasts\Flash\Flash;
use Validator;
use App\Modelos\Empleado\CargoMP;
use App\Http\Requests\CargoMPRequest;
use App\Http\Requests\EditarCargoMPRequest;
use RealRashid\SweetAlert\Facades\Alert;

class CargosMPController extends Controller
{
    //funcion para los permisos de cargos de manual de puestos
     public function __construct(){

    $this->middleware(['permission:crear.catalogos'], ['only'    => ['crearCargosMP', 'guardarCargosMP']]);
    $this->middleware(['permission:editar.catalogos'], ['only'   => ['editarCargosMP', 'actualizarCargosMP']]);
    $this->middleware(['permission:eliminar.catalogos'], ['only' => 'eliminarCargosMP']);
    $this->middleware(['permission:consultar.catalogos'], ['only'   => 'listarCargosMP']);
    $this->middleware(['permission:habilitar.catalogos'], ['only'   => 'habilitarCargosMP']);

    }

    public function crearCargosMP(Request $request){

    	return view('empleados.gestionCargosMP.crear'); //llama a la vista de crear CargosMP

    }

    public function guardarCargosMP(CargoMPRequest $request){
        //guarda los datos de los cargos MP
    	$cargoMP = new CargoMP();
    	$cargoMP->cargo_mp = $request->cargo_mp;
    	$cargoMP->estado=1;
    	$cargoMP->save();
        Alert::success('Registrar Cargo MP', $cargoMP->cargo_mp . ' de forma existosa')->autoClose(2000);
    	return redirect()->action('Empleados\CargosMPController@listarCargosMP');
    }

    public function listarCargosMP(){
        //lista los cargos MP
    	$cargoMP = CargoMP::where('estado', '=', 1)->orderBy('id_cargo_mp', 'DESC')->get();
        $cargoMPDeshabilitado = CargoMP::where('estado', '=', 0)->orderBy('id_cargo_mp', 'DESC')->get();
    	return view('empleados.gestionCargosMP.listar', compact('cargoMP','cargoMPDeshabilitado'));    	
    }

    public function editarCargosMP($id){
        //carga la vista para editar los datos de los cargos MP
    	$cargoMP = CargoMP::find($id);
    	return view('empleados.gestionCargosMP.editar', compact('cargoMP'));
    }

    public function actualizarCargosMP(EditarCargoMPRequest $request, $id){
        //actualiza los datos de los cargos MP
    	$cargoMP = CargoMP::find($id);
    	$cargoMP->cargo_mp = $request->cargo_mp;
    	$cargoMP->update();
        Alert::warning('Actualizar Cargo MP', $cargoMP->cargo_mp . ' de forma existosa')->autoClose(2000)->iconHtml('<i class="fa fa-pencil"></i>');
    	return redirect()->action('Empleados\CargosMPController@listarCargosMP');
    }


    public function eliminarCargosMP($id){
        //desactiva el cargo seleccionado
	    $cargoMP = CargoMP::find($id); //se busca el cargo mp que se desea eliminar
		$cargoMP->estado=0;
		$cargoMP->update();
        Alert::error('Eliminar Cargo MP', $cargoMP->cargo_mp . ' de forma existosa')->autoClose(2000)->iconHtml('<i class="fa fa-trash-o"></i>');
		 return redirect()->action('Empleados\CargosMPController@listarCargosMP');
    }


    public function habilitarCargosMP($id){
        //activa nuevamente el cargo MP
    	$cargoMP = CargoMP::find($id);
    	$cargoMP->estado=1;
    	$cargoMP->update();
        Alert::success('Habilitar Cargo MP', $cargoMP->cargo_mp  . ' de forma existosa')->autoClose(2000);
    	return redirect()->action('Empleados\CargosMPController@listarCargosMP');
    }
}
