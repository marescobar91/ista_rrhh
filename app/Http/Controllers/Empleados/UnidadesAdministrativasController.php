<?php

namespace App\Http\Controllers\Empleados;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Laracasts\Flash\Flash;
use Validator;
use App\Modelos\Empleado\UnidadAdministrativa;
use App\Http\Requests\UnidadAdministrativaRequest;
use App\Http\Requests\EditarUnidadAdministrativaRequest;
use RealRashid\SweetAlert\Facades\Alert;

class UnidadesAdministrativasController extends Controller
{
    //funcion para los permisos de las unidades administrativas
     public function __construct(){

    $this->middleware(['permission:crear.catalogos'], ['only'    => ['crearUnidadesAdministrativas', 'guardarUnidadesAdministrativas']]);
    $this->middleware(['permission:editar.catalogos'], ['only'   => ['editarUnidadesAdministrativas', 'actualizarUnidadesAdministrativas']]);
    $this->middleware(['permission:eliminar.catalogos'], ['only' => 'eliminarUnidadesAdministrativas']);
    $this->middleware(['permission:consultar.catalogos'], ['only'   => 'listarUnidadesAdministrativas']);
    $this->middleware(['permission:habilitar.catalogos'], ['only'   => 'habilitarUnidadesAdministrativas']);

    }
    //
    public function crearUnidadesAdministrativas(Request $request){

    	return view('empleados.gestionUnidadesAdministrativas.crear'); //llama a la vista de crear UnidadesAdministrativas

    }

    public function guardarUnidadesAdministrativas(UnidadAdministrativaRequest $request){
        //guarda las unidades administrativas
    	$unidadAdministrativa = new UnidadAdministrativa();
    	$unidadAdministrativa->unidad_administrativa = $request->unidad_administrativa;
    	
    	$unidadAdministrativa->estado=1;
    	$unidadAdministrativa->save();
        Alert::success('Registrar Unidad Administrativa', $unidadAdministrativa->unidad_administrativa . ' de forma existosa')->autoClose(2000);
    	return redirect()->action('Empleados\UnidadesAdministrativasController@listarUnidadesAdministrativas');
    }

    public function listarUnidadesAdministrativas(){    
        //muestra los listados de unidades administrativas
    	$unidadAdministrativa = UnidadAdministrativa::where('estado', '=', 1)->orderBy('id_unidad_administrativa', 'DESC')->get();
        $unidadAdministrativaDeshabilitado = UnidadAdministrativa::where('estado', '=', 0)->orderBy('id_unidad_administrativa', 'DESC')->get();
    	return view('empleados.gestionUnidadesAdministrativas.listar', compact('unidadAdministrativa','unidadAdministrativaDeshabilitado'));    	
    }

    public function editarUnidadesAdministrativas($id){
        //carga la vista de editar de unidades administrativas

    	$unidadAdministrativa = UnidadAdministrativa::find($id);
    	return view('empleados.gestionUnidadesAdministrativas.editar', compact('unidadAdministrativa'));
    }

    public function actualizarUnidadesAdministrativas(EditarUnidadAdministrativaRequest $request, $id){
        //actualiza los datos de la unidades administrativas

    	$unidadAdministrativa = UnidadAdministrativa::find($id);
    	$unidadAdministrativa->unidad_administrativa = $request->unidad_administrativa;
    	$unidadAdministrativa->update();
        Alert::warning('Actualizar Unidad Administrativa', $unidadAdministrativa->unidad_administrativa . ' de forma existosa')->autoClose(2000)->iconHtml('<i class="fa fa-pencil"></i>');

    	return redirect()->action('Empleados\UnidadesAdministrativasController@listarUnidadesAdministrativas');
    }


    public function eliminarUnidadesAdministrativas($id){
        //desactivar la unidad administrativa seleccionado
    $unidadAdministrativa = UnidadAdministrativa::find($id); //se busca el UnidadAdministrativa que se desea eliminar
	$unidadAdministrativa->estado=0;
	$unidadAdministrativa->update();
    Alert::error('Eliminar Unidad Administrativa ', $unidadAdministrativa->unidad_administrativa . ' de forma existosa')->autoClose(2000)->iconHtml('<i class="fa fa-trash-o"></i>');
	 return redirect()->action('Empleados\UnidadesAdministrativasController@listarUnidadesAdministrativas');
    }


    public function habilitarUnidadesAdministrativas($id){
        //activa la unidad administrativa seleccionado
    	$unidadAdministrativa = UnidadAdministrativa::find($id);
    	$unidadAdministrativa->estado=1;
    	$unidadAdministrativa->update();
        Alert::success('Habilitar Unidad Administrativa', $unidadAdministrativa->unidad_administrativa . ' de forma existosa')->autoClose(2000);
    	return redirect()->action('Empleados\UnidadesAdministrativasController@listarUnidadesAdministrativas');
    }
}
