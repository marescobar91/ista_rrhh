<?php

namespace App\Http\Controllers\Empleados;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Laracasts\Flash\Flash;
use Validator;
use App\Modelos\Empleado\Empleado; 
use App\Modelos\Empleado\EmpleadoTmp; 
use App\Http\Requests\EmpleadoRequest;
//*****Nuevos Request para guardar empleado con AJAX*****//
use App\Http\Requests\EmpleadoInformacionPersonalRequest;
use App\Http\Requests\EmpleadoEditarInformacionPersonalRequest;
use App\Http\Requests\EmpleadoDocumentoPersonalRequest;
use App\Http\Requests\EmpleadoEditarDocumentoPersonalRequest;
use App\Http\Requests\EmpleadoInformacionLaboralRequest;
use App\Http\Requests\EmpleadoInformacionFinancieraRequest;
use App\Http\Requests\EmpleadoEditarInformacionFinancieraRequest;
//*******************************************************//
use App\Modelos\Empleado\Municipio;
use App\Modelos\Empleado\EstadoCivil;
use App\Modelos\Empleado\Departamento;
use App\Modelos\Empleado\Cargo;
use App\Modelos\Empleado\NivelAcademico;
use App\Modelos\Empleado\TipoPlaza;
use App\Modelos\Empleado\Banco;
use App\Modelos\Empleado\CargoMP;
use App\Modelos\Empleado\CargoHacienda;
use App\Modelos\Empleado\UnidadAdministrativa;
use App\Modelos\Empleado\Centro;
use App\Modelos\Empleado\Oficina;
use App\Modelos\Administracion\Usuario;
use Carbon\Carbon;
use App\Modelos\Marcacion\Calendario;
use App\Http\Requests\EditarEmpleadoRequest;
use App\Http\Requests\InactivoEmpleadoRequest;
use App\Http\Requests\ActivoEmpleadoRequest;
use App\Http\Requests\InformacionPersonalRequest;
use App\Http\Requests\DocumentosPersonalesRequest;
use App\Http\Requests\InformacionFinancieraRequest;
use RealRashid\SweetAlert\Facades\Alert;
use PhpOffice\PhpWord\TemplateProcessor;

use Barryvdh\DomPDF\PDF;
use Session;
use DB;
use Auth;

class EmpleadosController extends Controller
{
    //funcion para los permisos del empleado
    public function __construct(){

    $this->middleware(['permission:crear.empleados'], ['only'    => ['crearEmpleados', 'guardarEmpleados']]);
    $this->middleware(['permission:editar.empleados'], ['only'   => ['editarEmpleados', 'actualizarEmpleados']]);
    $this->middleware(['permission:eliminar.empleados'], ['only' => 'deshabilitarEmpleados', 'desactivarEmpleados']);
    $this->middleware(['permission:consultar.empleados'], ['only'   => 'listarEmpleados', 'mostrarEmpleados']);
    $this->middleware(['permission:habilitar.empleados'], ['only'   => 'habilitarEmpleados', 'activarEmpleados']);

    }

   public function crearEmpleados(){
    //Esta funcion se encarga de mostrar la vista con sus 
    //respectivos catalogos en forma de lista

   $municipio = Municipio::get();
   $estadoCivil = EstadoCivil::get();
   $departamento = Departamento::get();
   $nivelAcademico = NivelAcademico::where('estado', '=', 1)->orderBy('id_nivel_academico', 'DESC')->get();
   $cargoFuncional = Cargo::where('estado', '=', 1)->orderBy('id_cargo', 'DESC')->get();
   $cargoNominal = Cargo::where('estado', '=', 1)->orderBy('id_cargo', 'DESC')->get();
   $tipoPlaza = TipoPlaza::where('estado', '=', 1)->orderBy('id_tipo_plaza', 'DESC')->get();
   $banco = Banco::where('estado', '=', 1)->orderBy('id_banco', 'DESC')->get();
   $cargoMP = CargoMP::where('estado', '=', 1)->orderBy('id_cargo_mp', 'DESC')->get();
   $oficina = Oficina::where('estado', '=', 1)->orderBy('id_oficina', 'DESC')->get();
   $unidad = UnidadAdministrativa::where('estado', '=', 1)->orderBy('id_unidad_administrativa', 'DESC')->get();
   $centro = Centro::where('estado', '=', 1)->orderBy('id_centro', 'DESC')->get();
   $cargoHacienda = CargoHacienda::where('estado', '=', 1)->orderBy('id_cargo_hacienda', 'DESC')->get();

   return view('empleados.gestionEmpleados.crear', compact('municipio','estadoCivil','departamento','nivelAcademico','cargoFuncional','cargoNominal','tipoPlaza','banco','cargoMP','oficina','unidad','cargoHacienda','centro'));

 }

 public function guardarInformacionPersonal(EmpleadoInformacionPersonalRequest $request){
  //muesta la vista para la informacion personal del empleado
  if ($request->ajax()) {
      $empleado = new Empleado();
      $fechaNacimiento=Carbon::parse($request->fecha_nacimiento);
      $empleado->pin_empleado = $request->pin_empleado; 
      $empleado->numero_expediente = $request->numero_expediente;
      $empleado->codigo_empleado = $request->codigo_empleado; 
      $empleado->nombre = $request->nombre;
      $empleado->apellido = $request->apellido;
      $empleado->estado_civil_id = $request->estadoCivil;
      $empleado->fecha_nacimiento = $request->fecha_nacimiento;
      $empleado->sexo = $request->sexo;
      $empleado->lugar_nacimiento = $request->lugar_nacimiento;
      $empleado->correo = $request->correo;
      $empleado->telefono = $request->telefono;
      $empleado->direccion = $request->direccion;
      $empleado->municipio_id = $request->municipio_id;
      $empleado->guardado=0;
      $empleado->activo=1;
      $empleado->save();
      return response()->json([
        "id" => $empleado->id_empleado
      ]);
  }
 }

 public function editarInformacionPersonal(EmpleadoEditarInformacionPersonalRequest $request,$id){

  if ($request->ajax()) {
      $empleado = Empleado::find($id);
      $fechaNacimiento=Carbon::parse($request->fecha_nacimiento);
      $empleado->pin_empleado = $request->pin_empleado; 
      $empleado->numero_expediente = $request->numero_expediente;
      $empleado->codigo_empleado = $request->codigo_empleado; 
      $empleado->nombre = $request->nombre;
      $empleado->apellido = $request->apellido;
      $empleado->estado_civil_id = $request->estadoCivil;
      $empleado->fecha_nacimiento = $request->fecha_nacimiento;
      $empleado->sexo = $request->sexo;
      $empleado->lugar_nacimiento = $request->lugar_nacimiento;
      $empleado->correo = $request->correo;
      $empleado->telefono = $request->telefono;
      $empleado->direccion = $request->direccion;
      $empleado->municipio_id = $request->municipio_id;
      $empleado->guardado=0;
      $empleado->update();
      return response()->json([
        "id" => $empleado->id_empleado
      ]);
  }
 }

 public function guardarDocumentosPersonales(EmpleadoDocumentoPersonalRequest $request,$id){
  if($request->ajax()){
    $empleado = Empleado::find($id);
    $empleado->tipo_documento=$request->tipo_documento;
    $empleado->numero_documento=$request->numero_documento;
    $empleado->nit=$request->nit;
    $empleado->isss=$request->isss;
    $empleado->prevision=$request->prevision;
    $empleado->nup=$request->nup;
    $empleado->nombre_contacto=$request->nombre_contacto;
    $empleado->telefono_contacto=$request->telefono_contacto;
    $empleado->nivel_academico_id=$request->nivelAcademico;
    $empleado->update();
    return response()->json();
  }
 }

 public function editarDocumentosPersonales(EmpleadoEditarDocumentoPersonalRequest $request,$id){
  if($request->ajax()){
    $empleado = Empleado::find($id);
    $empleado->tipo_documento=$request->tipo_documento;
    $empleado->numero_documento=$request->numero_documento;
    $empleado->nit=$request->nit;
    $empleado->isss=$request->isss;
    $empleado->prevision=$request->prevision;
    $empleado->nup=$request->nup;
    $empleado->nombre_contacto=$request->nombre_contacto;
    $empleado->telefono_contacto=$request->telefono_contacto;
    $empleado->nivel_academico_id=$request->nivelAcademico;
    $empleado->update();
    return response()->json();
  }
 }

 public function guardarInformacionLaboral(EmpleadoInformacionLaboralRequest $request,$id){
  if($request->ajax()){
    $empleado = Empleado::find($id);
    $empleado->oficina_id=$request->oficina_id;
    $empleado->fecha_ingreso=$request->fecha_ingreso;
    $empleado->salario_actual=$request->salario_actual;
    $empleado->salario_inicial=$request->salario_actual;
    $empleado->cargo_nominal_id=$request->cargoN;
    $empleado->cargo_funcional_id=$request->cargoF;
    $empleado->cargo_mp_id=$request->cargoMP;
    $empleado->cargo_hacienda_id=$request->cargoHacienda;
    $empleado->update();
    return response()->json();
  }
 }

 public function editarInformacionLaboral(EmpleadoInformacionLaboralRequest $request,$id){
  if($request->ajax()){
    $empleado = Empleado::find($id);
    $empleado->oficina_id=$request->oficina_id;
    $empleado->fecha_ingreso=$request->fecha_ingreso;
    $empleado->salario_actual=$request->salario_actual;
    $empleado->salario_inicial=$request->salario_actual;
    $empleado->cargo_nominal_id=$request->cargoN;
    $empleado->cargo_funcional_id=$request->cargoF;
    $empleado->cargo_mp_id=$request->cargoMP;
    $empleado->cargo_hacienda_id=$request->cargoHacienda;
    $empleado->update();
    return response()->json();
  }
 }

 public function guardarInformacionFinanciera(EmpleadoInformacionFinancieraRequest $request,$id){
  if($request->ajax()){
    $empleado = Empleado::find($id);
    $empleado->banco_id=$request->banco;
    $empleado->cuenta_banco=$request->cuenta_banco;
    $empleado->tipo_plaza_id=$request->tipoPlaza;
    $empleado->pin_empleado=$request->pin_empleado;
    $empleado->numero_expediente=$request->numero_expediente;
    $empleado->numero_partida=$request->numero_partida;
    $empleado->numero_acuerdo=$request->numero_acuerdo;
    $empleado->numero_subpartida=$request->numero_subpartida;
    $empleado->cifra_presupuestaria=$request->cifra_presupuestaria;
    $empleado->gifcard=$request->gifcard;
    $empleado->update();
    return response()->json([
      "id" => $empleado->id_empleado
    ]);
  }
 }

 public function editarInformacionFinanciera(EmpleadoEditarInformacionFinancieraRequest $request,$id){
  if($request->ajax()){
    $empleado = Empleado::find($id);
    $empleado->banco_id=$request->banco;
    $empleado->cuenta_banco=$request->cuenta_banco;
    $empleado->pin_empleado=$request->pin_empleado;
    $empleado->numero_expediente=$request->numero_expediente;
    $empleado->numero_partida=$request->numero_partida;
    $empleado->numero_acuerdo=$request->numero_acuerdo;
    $empleado->numero_subpartida=$request->numero_subpartida;
    $empleado->cifra_presupuestaria=$request->cifra_presupuestaria;
    $empleado->gifcard=$request->gifcard;
    $empleado->update();
    return response()->json([
      "id" => $empleado->id_empleado
    ]);
  }
 }

 public function guardarEstadoEmpleado(Request $request){
    //dd($request->id_empl);
    $empleado = Empleado::find($request->id_emp);
    $empleado->afiliado_sindicato = $request->afiliado_sindicato;
    $empleado->rendir_probidad = $request->rendir_probidad;
    $empleado->es_jefe = $request->es_jefe;
    $empleado->marca = $request->marca;
    $empleado->autorizado_estudio = $request->autorizado_estudio;
    $empleado->pensionado = $request->pensionado;
    $empleado->enviar_jefe = $request->enviar_jefe;
    $empleado->pasivo = $request->pasivo;
    $empleado->activo = $request->activo;
    $empleado->gifcard = $request->gifcard;
    //Valor de los checkbox
    if($request->input('afiliado_sindicato')=='on'){
      $empleado->afiliado_sindicato=1;
    }else{
      $empleado->afiliado_sindicato=0;
    }
    if($request->input('rendir_probidad')=='on'){
      $empleado->rendir_probidad=1;
    }else{
      $empleado->rendir_probidad=0;
    }
    if($request->input('es_jefe')=='on'){
      $empleado->es_jefe=1;
    }else{
      $empleado->es_jefe=0;
    }
    if($request->input('marca')=='on'){
      $empleado->marca=1;
    }else{
      $empleado->marca=0;
    }
    if($request->input('autorizado_estudio')=='on'){
      $empleado->autorizado_estudio=1;
    }else{
      $empleado->autorizado_estudio=0;
    }
    if($request->input('pensionado')=='on'){
      $empleado->pensionado=1;
    }else{
      $empleado->pensionado=0;
    }
    if($request->input('enviar_jefe')=='on'){
      $empleado->enviar_jefe=1;
    }else{
      $empleado->enviar_jefe=0;
    }
    $empleado->pasivo=0;
    $empleado->activo=1;
    $empleado->guardado=1;

    $empleado->update();
    $fecha = $empleado->fecha_ingreso;
    $calendarioActual = Calendario::where('fecha', '>=', $fecha)->get();
    foreach($calendarioActual as $calendario){
        $arrayCalendario[] = $calendario->id_calendario;
        }

    $empleado->empleadoCalendario()->attach($arrayCalendario);
    $empleado->update();
    $idEmpleado = $empleado->id_empleado;
    //envia la accion de crear al historial del empleado
    $accion = "Crear";
    return redirect()->action('Empleados\TrasladoController@guardarHistorial', ['id' => $idEmpleado, 'accion'=>$accion]);
 }

      public function editarEmpleados($id){
        //muestra vista para editar a los empleados
        $empleado = Empleado::find($id);

        $municipio = Municipio::get();
        $estadoCivil = EstadoCivil::get(); 
        $departamento = Departamento::get();
        $nivelAcademico = NivelAcademico::where('estado', '=', 1)->orderBy('id_nivel_academico', 'DESC')->get();
        $cargoFuncional = Cargo::where('estado', '=', 1)->orderBy('id_cargo', 'DESC')->get();
        $cargoNominal = Cargo::where('estado', '=', 1)->orderBy('id_cargo', 'DESC')->get();
        $tipoPlaza = TipoPlaza::where('estado', '=', 1)->orderBy('id_tipo_plaza', 'DESC')->get();
        $banco = Banco::where('estado', '=', 1)->orderBy('id_banco', 'DESC')->get();
        $cargoMP = CargoMP::where('estado', '=', 1)->orderBy('id_cargo_mp', 'DESC')->get();
        $oficina = Oficina::where('estado', '=', 1)->orderBy('id_oficina', 'DESC')->get();
        $centro = Centro::where('estado', '=', 1)->orderBy('id_centro', 'DESC')->get();
        $unidad = UnidadAdministrativa::where('estado', '=', 1)->orderBy('id_unidad_administrativa', 'DESC')->get();
        $cargoHacienda = CargoHacienda::where('estado', '=', 1)->orderBy('id_cargo_hacienda', 'DESC')->get();

        //validaciones si el empleado no se termino de llenar y pueda terminarlo en el editar
        if($empleado->municipio_id!= null){
          $municipioActual= $empleado->municipio_id;
          $departamentoActual = $empleado->municipio->departamento_id;
        }else{
        $municipioActual= "";
        $departamentoActual = "";
        }

        if($empleado->oficina_id != null){
        $oficinaActual=$empleado->oficina_id;
        $centroActual=$empleado->oficina->centro_id;
        $unidadActual=$empleado->oficina->unidad_administrativa_id;
        }else{
        $oficinaActual="";
        $centroActual="";
        $unidadActual="";
        }
       

        $edad = Carbon::parse($empleado->fecha_nacimiento)->age; // calcula la edad

        return view('empleados.gestionEmpleados.editar')
        ->with('empleado', $empleado)
        ->with('estadoCivil', $estadoCivil)
        ->with('departamento', $departamento)
        ->with('departamentoActual',$departamentoActual)
        ->with('municipio', $municipio)
        ->with('municipioActual',$municipioActual) 
        ->with('nivelAcademico', $nivelAcademico)
        ->with('cargoFuncional', $cargoFuncional)
        ->with('cargoNominal', $cargoNominal)
        ->with('tipoPlaza', $tipoPlaza)
        ->with('banco', $banco)
        ->with('cargoMP', $cargoMP)
        ->with('oficina', $oficina)
        ->with('unidad', $unidad)
         ->with('centro', $centro)
        ->with('cargoHacienda', $cargoHacienda)
        ->with('edad',$edad)
        ->with('oficinaActual',$oficinaActual)
        ->with('centroActual',$centroActual)
        ->with('unidadActual',$unidadActual);

      }

      public function actualizarEmpleados(EditarEmpleadoRequest $request, $id){
        //funcion para actualizar a los empleados
        $empleado = Empleado::find($id);
        if($empleado->guardado==0){ //valida que los empleados terminen de ser ingresados
           $usuario_id = Auth::id(); 
        $fecha = Carbon::now(new \DateTimeZone('America/El_Salvador'));
        $date = $fecha->format('Y-m-d');
        $empleado->municipio_id = $request->municipio;
        $empleado->nivel_academico_id = $request->nivel_academico;
        $empleado->tipo_plaza_id = $request->tipo_plaza;
        $empleado->estado_civil_id = $request->estadoCivil;
        $empleado->banco_id = $request->banco;
        $empleado->codigo_empleado = $request->codigo_empleado;
        $empleado->nombre = $request->nombre;
        $empleado->apellido = $request->apellido;
        $empleado->pin_empleado = $request->pin_empleado;
        $empleado->numero_expediente = $request->numero_expediente;
        $empleado->tipo_documento = $request->tipo_documento;
        $empleado->numero_documento = $request->numero_documento;
        $empleado->fecha_nacimiento = $request->fecha_nacimiento; 
        $empleado->lugar_nacimiento = $request->lugar_nacimiento;
         $empleado->oficina_id = $request->oficina;
        $empleado->nit = $request->nit;
        $empleado->isss = $request->isss;
        $empleado->prevision = $request->prevision;
        $empleado->nup = $request->nup;
        $empleado->direccion = $request->direccion;
        $empleado->telefono = $request->telefono;
        $empleado->correo = $request->correo;
        $empleado->nombre_contacto = $request->nombre_contacto;
        $empleado->telefono_contacto = $request->telefono_contacto;
        $empleado->sexo = $request->sexo;
        $empleado->fecha_ingreso = $request->fecha_ingreso;
        $empleado->salario_actual = $request->salario_actual;
         $empleado->salario_inicial = $request->salario_actual;
        $empleado->cuenta_banco = $request->cuenta_banco;
        $empleado->cifra_presupuestaria = $request->cifra_presupuestaria;
        $empleado->numero_acuerdo = $request->numero_acuerdo;
        $empleado->numero_partida = $request->numero_partida;
        $empleado->numero_subpartida = $request->numero_subpartida;
        $empleado->afiliado_sindicato = $request->afiliado_sindicato;
        $empleado->rendir_probidad = $request->rendir_probidad;
        $empleado->es_jefe = $request->es_jefe;
        $empleado->marca = $request->marca;
        $empleado->autorizado_estudio = $request->autorizado_estudio;
        $empleado->pensionado = $request->pensionado;
        $empleado->enviar_jefe = $request->enviar_jefe;
        $empleado->pasivo = $request->pasivo;
        $empleado->activo = $request->activo;
        $empleado->gifcard = $request->gifcard;
        $empleado->cargo_nominal_id = $request->cargoN;
        $empleado->cargo_funcional_id = $request->cargoF;
        $empleado->cargo_mp_id = $request->cargoMP;
        $empleado->cargo_hacienda_id = $request->cargoH;
        


    //Obtenemos valores de los checkbox 
        if($request->input('afiliado_sindicato')=='on'){
         $empleado->afiliado_sindicato=1;
       }else{
         $empleado->afiliado_sindicato=0;
       }
       if($request->input('rendir_probidad')=='on'){
         $empleado->rendir_probidad=1;
       }else{
         $empleado->rendir_probidad=0;
       }
       if($request->input('es_jefe')=='on'){
         $empleado->es_jefe=1;
       }else{
         $empleado->es_jefe=0;
       }
       if($request->input('marca')=='on'){
         $empleado->marca=1;
       }else{
         $empleado->marca=0;
       }
       if($request->input('autorizado_estudio')=='on'){
         $empleado->autorizado_estudio=1;
       }else{
         $empleado->autorizado_estudio=0;
       }
       if($request->input('pensionado')=='on'){
         $empleado->pensionado=1;
       }else{
         $empleado->pensionado=0;
       }
       if($request->input('enviar_jefe')=='on'){
         $empleado->enviar_jefe=1;
       }else{
         $empleado->enviar_jefe=0;
       }
       if($request->input('pasivo')=='on'){
         $empleado->pasivo=1;
       }else{
         $empleado->pasivo=0;
       }
       if($request->input('activo')=='on'){
     $empleado->activo=1;
   }else{
     $empleado->activo=0;
   }
        $empleado->guardado=1;
        $empleado->update(); 
        $fecha = $empleado->fecha_ingreso;

        $calendarioActual = Calendario::where('fecha', '>=', $fecha)->get();
        foreach($calendarioActual as $calendario){
            $arrayCalendario[] = $calendario->id_calendario;
        }

        $empleado->empleadoCalendario()->attach($arrayCalendario);
        $empleado->update();
        $idEmpleado = $empleado->id_empleado;
        $accion = "Crear";
        //envia la accion de editar el empleado al guardarHistorial
      return redirect()->action('Empleados\TrasladoController@guardarHistorial', ['id' => $idEmpleado, 'accion'=>$accion]);

        }elseif ($empleado->guardado==1) {  //validacion para editar un empleado que se ingreso completamente
            $usuario_id = Auth::id(); 
        $fecha = Carbon::now(new \DateTimeZone('America/El_Salvador'));
        $date = $fecha->format('Y-m-d');
        $empleado->municipio_id = $request->municipio;
        $empleado->nivel_academico_id = $request->nivel_academico;
        $empleado->tipo_plaza_id = $request->tipo_plaza;
        $empleado->estado_civil_id = $request->estadoCivil;
        $empleado->banco_id = $request->banco;
        $empleado->codigo_empleado = $request->codigo_empleado;
        $empleado->nombre = $request->nombre;
        $empleado->apellido = $request->apellido;
        $empleado->pin_empleado = $request->pin_empleado;
        $empleado->numero_expediente = $request->numero_expediente;
        $empleado->tipo_documento = $request->tipo_documento;
        $empleado->numero_documento = $request->numero_documento;
        $empleado->fecha_nacimiento = $request->fecha_nacimiento; 
        $empleado->lugar_nacimiento = $request->lugar_nacimiento;
        $empleado->nit = $request->nit;
        $empleado->isss = $request->isss;
        $empleado->prevision = $request->prevision;
        $empleado->nup = $request->nup;
        $empleado->direccion = $request->direccion;
        $empleado->telefono = $request->telefono;
        $empleado->correo = $request->correo;
        $empleado->nombre_contacto = $request->nombre_contacto;
        $empleado->telefono_contacto = $request->telefono_contacto;
        $empleado->sexo = $request->sexo;
        $empleado->fecha_ingreso = $request->fecha_ingreso;
        $empleado->salario_actual = $request->salario_actual;
        $empleado->cuenta_banco = $request->cuenta_banco;
        $empleado->oficina_id = $request->oficina;
        $empleado->cifra_presupuestaria = $request->cifra_presupuestaria;
        $empleado->numero_acuerdo = $request->numero_acuerdo;
        $empleado->numero_partida = $request->numero_partida;
        $empleado->numero_subpartida = $request->numero_subpartida;
        $empleado->afiliado_sindicato = $request->afiliado_sindicato;
        $empleado->rendir_probidad = $request->rendir_probidad;
        $empleado->es_jefe = $request->es_jefe;
        $empleado->marca = $request->marca;
        $empleado->autorizado_estudio = $request->autorizado_estudio;
        $empleado->pensionado = $request->pensionado;
        $empleado->enviar_jefe = $request->enviar_jefe;
        $empleado->pasivo = $request->pasivo;
        $empleado->activo = $request->activo;
        $empleado->gifcard = $request->gifcard;
        $empleado->cargo_nominal_id = $request->cargoN;
        $empleado->cargo_funcional_id = $request->cargoF;
        $empleado->cargo_mp_id = $request->cargoMP;
        $empleado->cargo_hacienda_id = $request->cargoH;
        
    //Obtenemos valores de los checkbox 
        if($request->input('afiliado_sindicato')=='on'){
         $empleado->afiliado_sindicato=1;
       }else{
         $empleado->afiliado_sindicato=0;
       }
       if($request->input('rendir_probidad')=='on'){
         $empleado->rendir_probidad=1;
       }else{
         $empleado->rendir_probidad=0;
       }
       if($request->input('es_jefe')=='on'){
         $empleado->es_jefe=1;
       }else{
         $empleado->es_jefe=0;
       }
       if($request->input('marca')=='on'){
         $empleado->marca=1;
       }else{
         $empleado->marca=0;
       }
       if($request->input('autorizado_estudio')=='on'){
         $empleado->autorizado_estudio=1;
       }else{
         $empleado->autorizado_estudio=0;
       }
       if($request->input('pensionado')=='on'){
         $empleado->pensionado=1;
       }else{
         $empleado->pensionado=0;
       }
       if($request->input('enviar_jefe')=='on'){
         $empleado->enviar_jefe=1;
       }else{
         $empleado->enviar_jefe=0;
       }
       if($request->input('pasivo')=='on'){
         $empleado->pasivo=1;
       }else{
         $empleado->pasivo=0;
       }
       if($request->input('activo')=='on'){
     $empleado->activo=1;
   }else{
     $empleado->activo=0;
   }
        $empleado->update(); 
   
        $idEmpleado = $empleado->id_empleado;
        $accion = "Editar";
        //agrega la accion al historial de empleado
        return redirect()->action('Empleados\TrasladoController@guardarHistorial', ['id' => $idEmpleado, 'accion'=>$accion]);
    }

      
    }

      public function listarEmpleados()
      {
       //muestra el listado de Empleados
        $empleado = Empleado::where('activo', '=', 1)->orderBy('id_empleado', 'DESC')->get();
        $empleadoDeshabilitados = Empleado::where('activo', '=', 0)->orderBy('id_empleado', 'DESC')->get();
        $idUsuario = Auth::id(); 
        $usuario = Usuario::where('id_usuario', '=', $idUsuario)->first();
        $empleadoId= $usuario->empleado_id;
        return view('empleados.gestionEmpleados.listar', compact('empleado', 'empleadoDeshabilitados', 'empleadoId'));
      } 

      public function getMunicipios(Request $request)
      { 
        //obtiene los municipios dependiendo del departamento
        if ($request->ajax()) {
          $municipios = Municipio::where('departamento_id', $request->departamento)->get();
          foreach ($municipios as $municipio) {
            $municipioArray[$municipio->id_municipio] = $municipio->municipio;
          }
          return response()->json($municipioArray);
        }
      }


      public function getOficinas(Request $request)
      {
        //obtiene las oficinas dependiendo de las unidad administrativa y el centro
          $oficinas = DB::table('emp_oficina')
          ->where('unidad_administrativa_id', $request->unidad_administrativa)
          ->where('centro_id',$request->centro)
          ->get();

          foreach ($oficinas as $oficina) {
            $oficinaArray[$oficina->id_oficina] = $oficina->oficina;
          }
          return response()->json($oficinaArray);
        
      }

      public function mostrarEmpleados($id){
        //muesta los datos del empleado 
        $empleado = Empleado::find($id);
        $edad = Carbon::parse($empleado->fecha_nacimiento)->age; // calcula la edad

        return view('empleados.gestionEmpleados.ver', compact('empleado','edad'));
      }

      public function deshabilitarEmpleados($id){
        //deshabilitar el empleado seleccionado
        $empleado = Empleado::find($id);
        return view('empleados.gestionEmpleados.eliminar', compact('empleado'));
      }

      public function habilitarEmpleados($id){
        //habilita el empleado seleccionado
        $empleado = Empleado::find($id);
        $cargoFuncional = Cargo::where('estado', '=', 1)->orderBy('id_cargo', 'DESC')->get();
        $cargoNominal = Cargo::where('estado', '=', 1)->orderBy('id_cargo', 'DESC')->get();
        $cargoMP = CargoMP::where('estado', '=', 1)->orderBy('id_cargo_mp', 'DESC')->get();
        $cargoHacienda = CargoHacienda::where('estado', '=', 1)->orderBy('id_cargo_hacienda', 'DESC')->get();
        $oficina = Oficina::where('estado', '=', 1)->orderBy('id_oficina', 'DESC')->get();
        $unidad = UnidadAdministrativa::where('estado', '=', 1)->orderBy('id_unidad_administrativa', 'DESC')->get();
        $centro = Centro::where('estado','=',1)->get();
        $oficinaActual = $empleado->oficina_id;
        $unidadActual = $empleado->oficina->unidad_administrativa_id;
        $centroActual = $empleado->oficina->centro_id;
        

         return view('empleados.gestionEmpleados.activar', compact('empleado','oficinaActual','unidadActual','centroActual'))
         ->with('cargoFuncional', $cargoFuncional)
         ->with('cargoNominal', $cargoNominal)
         ->with('cargoMP', $cargoMP)
         ->with('cargoHacienda', $cargoHacienda)
         ->with('oficina', $oficina)
         ->with('unidad', $unidad)
         ->with('centro',$centro);
      }

      public function desactivarEmpleados(InactivoEmpleadoRequest $request, $id){
        //desactiva al empleado seleccionado
        $empleado = Empleado::find($id);
        $usuario_id = Auth::id(); 
        $fecha = Carbon::now(new \DateTimeZone('America/El_Salvador'));
        $date = $fecha->format('Y-m-d');
        $empleado->numero_acuerdo_retiro = $request->numero_acuerdo_retiro;
        $empleado->motivo_retiro = $request->motivo_retiro;
        $empleado->fecha_retiro = $request->fecha_retiro;
        $empleado->activo = 0;
     
        $empleado->update();

        $usuario = Usuario::where('empleado_id', '=', $empleado->id_empleado)->first();
          if($usuario){
              $usuario->delete();
              $idEmpleado = $empleado->id_empleado;
              //envia la accion al historial de empleados
              $accion = "Desactivar con usuario";
            return redirect()->action('Empleados\TrasladoController@guardarHistorial', ['id' => $idEmpleado, 'accion'=>$accion]);
            
            }else{
              $idEmpleado = $empleado->id_empleado;
              //envia la accion al historial de empleados
              $accion = "Desactivar";
            return redirect()->action('Empleados\TrasladoController@guardarHistorial', ['id' => $idEmpleado, 'accion'=>$accion]);

            }
    }

      public function activarEmpleados(ActivoEmpleadoRequest $request, $id){
        //activa el empleado seleccionado
        $empleado = Empleado::find($id);
        $usuario_id = Auth::id();
        $empleado->numero_acuerdo_retiro = null;//$request->numero_acuerdo_retiro;
        $empleado->motivo_retiro = null;//$request->motivo_retiro;
        $empleado->fecha_retiro = null;//$request->fecha_retiro;
        $empleado->fecha_reingreso = $request->fecha_reingreso;
        $empleado->cargo_mp_id = $request->cargoMP;
        $empleado->oficina_id = $request->oficina;
        $empleado->cargo_hacienda_id = $request->cargoH;  
        $empleado->cargo_nominal_id = $request->cargoN;
        $empleado->cargo_funcional_id = $request->cargoF;
        $empleado->salario_actual = $request->salario_actual;
        
        $empleado->activo=1;
        $empleado->update();

        $usuario = Usuario::onlyTrashed()->where('empleado_id',$empleado->id_empleado)->first();
        if($usuario){
            $usuario->restore();
             $idEmpleado = $empleado->id_empleado;
             //envia la accion al historial de empleados
              $accion = "Activar con usuario";
            return redirect()->action('Empleados\TrasladoController@guardarHistorial', ['id' => $idEmpleado, 'accion'=>$accion]);
        }else{
           $idEmpleado = $empleado->id_empleado;
           //envia la accion al historial de empleados
              $accion = "Activar";
            return redirect()->action('Empleados\TrasladoController@guardarHistorial', ['id' => $idEmpleado, 'accion'=>$accion]);
          }
      }


      public function exportarContratoEmpleado($id){
        $empleado = Empleado::find($id);
        $fecha = Carbon::now(new \DateTimeZone('America/El_Salvador'));
        $date = $fecha->format('Ymd');
        $nombreCompleto = (strtoupper($empleado->nombre).' '.strtoupper($empleado->apellido));
        $cargo=$empleado->cargoMP->cargo_mp;
        $dui=$empleado->numero_documento;
        $salario=$empleado->salario_actual;
        $templateProcessor = new TemplateProcessor('contratos/formato-contrato.docx');
        $templateProcessor->setValue('nombre', $nombreCompleto);
        $templateProcessor->setValue('cargo', $cargo);
        $templateProcessor->setValue('dui', $dui);
        $templateProcessor->setValue('salario', $salario);
        $fileName = $empleado->codigo_empleado.'_'.$date;
        $templateProcessor->saveAs($fileName . '.docx');
        return response()->download($fileName . '.docx')->deleteFileAfterSend(true);
        
        
      }


}