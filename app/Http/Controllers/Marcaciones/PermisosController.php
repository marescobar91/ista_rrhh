<?php

namespace App\Http\Controllers\Marcaciones;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon; 
use App\Modelos\Empleado\Empleado;
use App\Modelos\Marcacion\TipoPermiso;
use App\Modelos\Marcacion\Permiso;
use App\Modelos\Marcacion\Calendario;
use App\Modelos\Marcacion\PermisoDetalle;
use RealRashid\SweetAlert\Facades\Alert;
use App\Modelos\Empleado\Oficina;
use App\Modelos\Empleado\UnidadAdministrativa;
use App\Modelos\Empleado\Centro;
use Laracasts\Flash\Flash;
use DatePeriod;
use DateInterval;
use DB;

class PermisosController extends Controller 
{
    //funcion para los permisos de los permisos de los empleados
 public function __construct(){

  $this->middleware(['permission:crear.permisos.empleado'], ['only'    => ['buscarPermisoEmpleado', 'crearPermisoEmpleado', 'guardarPermisoEmpleado']]);
  $this->middleware(['permission:consultar.permisos.empleado'], ['only'    => ['listarPermisoEmpleado']]);
  $this->middleware(['permission:editar.permisos.empleado'], ['only'    => ['editarPermisoEmpleado','actualizarPermisoEmpleado','actualizarPermisoEmpleado']]);
  $this->middleware(['permission:eliminar.permisos.empleado'], ['only'    => ['eliminarPermiso']]);
  $this->middleware(['permission:habilitar.permisos.empleado'], ['only'    => ['habilitarPermiso']]);
  $this->middleware(['permission:buscar.permisos.empleado'], ['only'    => ['buscarConsultaPermisoEmpleado', 'consultarPermisoEmpleado']]);
   $this->middleware(['permission:reporte.permiso.empleado'], ['only'    => ['buscarReportePermisoSinGoceSueldo', 'reportePermisoSinGoceSueldo', 'permisoSinGoceSueldoPDF']]);
  

}


public function buscarPermisoEmpleado(){
  //buscar el permiso del empleado
  $empleado = Empleado::all();
  return view('marcacion.gestionPermisos.crear', compact('empleado'));
}

public function crearPermisoEmpleado(Request $request){
  //carga la vista para crear el permiso del empleado
  Carbon::setLocale('es_SV');
  $fecha = Carbon::now(new \DateTimeZone('America/El_Salvador'));
  $hoy = $fecha->format('Y-m-d');
  $empleado = Empleado::where('id_empleado', $request->codigo_id)->get();
  $dias = $fecha->format('d'); 

  foreach ($empleado as $empleado){
    $permiso = DB::table('detalle_permiso_empleado_vw')->where('empleado_id','=', $empleado->id_empleado)->get();
    $i=0;
    $detalle=[];
    foreach($permiso as $permiso){
      $det['codigo']=$permiso->codigo_permiso;
      $det['descripcion']=$permiso->descripcion;
      $det['horas']=$permiso->numero_horas;
      $det['horas_acumuladas']=$permiso->horas_acumuladas;
      $det['resta_horas']=$permiso->resta_hora;
      $detalle[$i]=$det;
      $i++;
    }

    $fechaIngreso= $empleado->fecha_ingreso;
     //consultar el sexo del empleado
    if ($empleado->sexo == 'Mujer') {
      $tipoPermiso = TipoPermiso::where('descripcion','<>', 'LICENCIA POR PATERNIDAD')->get();
    }elseif($empleado->sexo == 'Hombre'){
      $tipoPermiso = TipoPermiso::where('descripcion','<>', 'LICENCIA POR MATERNIDAD')->get();
    }
    
    $anios = $fecha->diff($fechaIngreso);
    $dias = $anios->format('%a');
    if (date('L', strtotime($fecha))) {
     $anios_trabajados = round(($dias/366),4);
   } else {
    $anios_trabajados = round(($dias/365),4);
  } 

  if ($anios_trabajados >=6){
      //Validacion que muestra el permiso de:PERMISOS O LICENCIAS POR ENFERMEDAD si el empleado tiene 6 o mas años de servicios activos en el ISTA
    if ($empleado->sexo == 'Hombre') {

      $tipoPermiso = TipoPermiso::where('codigo_permiso','<>', 02)->where('codigo_permiso','<>', 03)->where('codigo_permiso','<>', 04)->where('codigo_permiso','<>', 05)->where('codigo_permiso','<>', 10)->where('codigo_permiso','<>', 61)->get();
    }else{
      $tipoPermiso = TipoPermiso::where('codigo_permiso','<>', 02)->where('codigo_permiso','<>', 03)->where('codigo_permiso','<>', 04)->where('codigo_permiso','<>', 05)->where('codigo_permiso','<>', 62)->get();
    }
  }elseif($anios_trabajados >=4 &&  $anios_trabajados <6){

    //Validacion que muestra el permiso de:PERMISOS O LICENCIAS POR ENFERMEDAD si el empleado tiene de 4 años a 6 años de servicios activos en el ISTA
    if ($empleado->sexo == 'Hombre') {

      $tipoPermiso = TipoPermiso::where('codigo_permiso','<>', 01)->where('codigo_permiso','<>', 03)->where('codigo_permiso','<>', 04)->where('codigo_permiso','<>', 05)->where('codigo_permiso','<>', 10)->where('codigo_permiso','<>', 61)->get();
    }else{
      $tipoPermiso = TipoPermiso::where('codigo_permiso','<>', 01)->where('codigo_permiso','<>', 03)->where('codigo_permiso','<>', 04)->where('codigo_permiso','<>', 05)->where('codigo_permiso','<>', 62)->get();
    }

  }elseif($anios_trabajados >=1 &&  $anios_trabajados <4){

    //Validacion que muestra el permiso de:PERMISOS O LICENCIAS POR ENFERMEDAD si el empleado tiene de 1 año a 4 años de servicios activos en el ISTA
    //
   if ($empleado->sexo == 'Hombre') {

    $tipoPermiso = TipoPermiso::where('codigo_permiso','<>', 01)->where('codigo_permiso','<>', 02)->where('codigo_permiso','<>', 04)->where('codigo_permiso','<>', 05)->where('codigo_permiso','<>', 10)->where('codigo_permiso','<>', 61)->get();
  }else{
    $tipoPermiso = TipoPermiso::where('codigo_permiso','<>', 01)->where('codigo_permiso','<>', 02)->where('codigo_permiso','<>', 04)->where('codigo_permiso','<>', 05)->where('codigo_permiso','<>', 62)->get();
  }

}elseif($anios_trabajados < 1 && $empleado->sexo == 'Hombre'){

    //Validacion que muestra el permiso de:PERMISOS O LICENCIAS POR ENFERMEDAD si el empleado es HOMBRE y tiene menos de un año en la empresa

  $tipoPermiso = TipoPermiso::where('codigo_permiso','<>', 01)->where('codigo_permiso','<>', 02)->where('codigo_permiso','<>', 03)->where('codigo_permiso','<>', 05)->where('descripcion','<>', 'LICENCIA POR MATERNIDAD')->where('codigo_permiso','<>', 61)->get();

}elseif ($anios_trabajados < 1 && $empleado->sexo == 'Mujer' ) {

    //Validacion que muestra el permiso de:PERMISOS O LICENCIAS POR ENFERMEDAD si el empleado es Mujer y tiene menos de un año en la empresa

  $tipoPermiso = TipoPermiso::where('codigo_permiso','<>', 01)->where('codigo_permiso','<>', 02)->where('codigo_permiso','<>', 03)->where('codigo_permiso','<>', 04)->where('descripcion','<>', 'LICENCIA POR PATERNIDAD')->get();
}
 //obtenemos el tipo de permiso
foreach($tipoPermiso as $tPermiso){
  $arrayPermiso[$tPermiso->id_tipo_permiso]=$tPermiso->codigo_permiso;
}


$A1['id']= $empleado->id_empleado;
$A1['pin']= $empleado->pin_empleado;
$A1['fecha_actual']= $hoy;
$A1['anios']= $anios_trabajados;
$A1['nombre']= $empleado->nombre.' '.$empleado->apellido;
$A1['oficina']= $empleado->oficina->oficina;
$Array['empleado']= $A1;
$Array['permisos']= $arrayPermiso;
$Array['detalle']= $detalle;
}
return response()->json($Array); 
}

public function getCodigoTipoPermiso(Request $request){
  //obtenemos el codigo de tipo permiso
  $tipoPermiso = TipoPermiso::where('id_tipo_permiso', $request->tipo_permiso)->get();
  foreach($tipoPermiso as $tPermiso){
    $arrayPermiso['id']=$tPermiso->id_tipo_permiso;
    $arrayPermiso['descripcion']=$tPermiso->descripcion;
  }
  return response()->json($arrayPermiso);
}

public function guardarPermisoEmpleado(Request $request){
//guardamos los permisos del empleado
 Carbon::setLocale('es_SV');
 $fecha = Carbon::now(new \DateTimeZone('America/El_Salvador'));
 $date = $fecha->format('Y-m-d');
 $permiso = new Permiso();
 $permiso->estado=1;
 $permiso->empleado_id = $request->id_empleado;
 $permiso->motivo_permiso = $request->motivo_permiso;
 $permiso->fecha_inicio = $request->fecha_inicio;
 $permiso->fecha_fin = $request->fecha_final;
 $permiso->hora_inicio = $request->hora_inicio; 
 $permiso->hora_fin = $request->hora_final;
 $permiso->fecha_registro = $request->fecha_registro;
 $permiso->tipo_permiso_id = $request->tipo_permiso;
     $fecha1= Carbon::parse($request->fecha_inicio); //Traemos fechas de inicio en la vista, transformar a carbon
     $fecha2= Carbon::parse($request->fecha_final); //Traemos fechas de final en la vista, transformar a carbon
     $tipoPermiso = TipoPermiso::find($request->tipo_permiso);

     //consulta si el permiso que se esta ingresando ya existe
     $permisoExiste = DB::table('mar_permiso as p')->where('p.empleado_id', '=', $request->id_empleado)->where('p.fecha_inicio', '=', $request->fecha_inicio)->where('p.fecha_fin', '=', $request->fecha_final)->where('p.hora_inicio', '=', $request->hora_inicio)->where('p.hora_fin', '=', $request->hora_final)->where('p.tipo_permiso_id', '=', $request->tipo_permiso)->get();

    //valida si el permiso es diferente de vacio le mostrara un mensaje de error. 

     if(!$permisoExiste->isEmpty()){

      flash("¡ERROR! ¡El permiso ya ha sido registrada!")->error()->important();
      return redirect()->action('Marcaciones\PermisosController@buscarPermisoEmpleado');
    }//fin del if de permiso existe

    if($fecha1!=null && $fecha2!=null){

      if($fecha2 < $fecha1){ 

        flash("¡ERROR! ¡La fecha final no puede ser menor que la fecha de inicio!")->error()->important();
        return redirect()->action('Marcaciones\PermisosController@buscarPermisoEmpleado');
      }
    }
    //validacion de fin de semana
    $fechaUno = $fecha1->format('D');
    $fechaDos = $fecha2->format('D');


    if ($fechaUno == 'Sun'||  $fechaUno == 'Sat') {
      flash("¡ERROR! ¡La fecha inicial no puede ser fin de semana!")->error()->important();
      return redirect()->action('Marcaciones\PermisosController@buscarPermisoEmpleado');
    }elseif($fechaDos == 'Sun'||  $fechaDos == 'Sat'){
      flash("¡ERROR! ¡La fecha final no puede ser fin de semana!")->error()->important();
      return redirect()->action('Marcaciones\PermisosController@buscarPermisoEmpleado');
    }
    //buscar los asuetos
    $asuetoUno = DB::table('mar_asueto')->select(DB::raw('COUNT(id_asueto) as diaAsueto'))->where('fecha', '=',$fecha1)->first();
    $asuetoDos = DB::table('mar_asueto')->select(DB::raw('COUNT(id_asueto) as diaAsueto'))->where('fecha', '=',$fecha2)->first();

    if ($asuetoUno->diaAsueto != 0){
      flash("¡ERROR! ¡La fecha inicial no puede ser asueto!")->error()->important();
      return redirect()->action('Marcaciones\PermisosController@buscarPermisoEmpleado');
    }elseif($asuetoDos->diaAsueto != 0){
      flash("¡ERROR! ¡La fecha final no puede ser asueto!")->error()->important();
      return redirect()->action('Marcaciones\PermisosController@buscarPermisoEmpleado');
    }


    $horasIntermedias = 0; //acumulador para las horas intermedias
    $horaI = $request->hora_inicio; //guardamos las horas de inicio de la vista
    $horaF = $request->hora_final; //hora final de la vista
    $intervalo = new DateInterval('P1D'); //intervalo de recorrer dentro del foreach de fechas
    $rango = new DatePeriod($fecha1, $intervalo ,$fecha2); //rango entre fechas de inicio y final

    $fechaHoI = $fecha1.' '.$horaI; //unimos fecha y hora de inicio
    $fechaHoF = $fecha2.' '.$horaF; //unimos fecha y hora final
    
    if($fechaHoF < $fechaHoI){

      flash("¡ERROR! ¡La hora final no puede ser menor a la hora inicial!")->error()->important();
      return redirect()->action('Marcaciones\PermisosController@buscarPermisoEmpleado');
    }



    if ($tipoPermiso->codigo_permiso == 10) {


       $diasDiferencia = $fecha2->DiffInDays($fecha1); //dias de diferencia entre fecha inicio y fecha final
    //se guarda cuantos dias hay entre fechas

       if($diasDiferencia > 1 ){
        for ($i=1; $i < $diasDiferencia ; $i++) { 
          $horasIntermedias = $horasIntermedias +8;
        }  
        $hora1= round(((strtotime('15:30')-strtotime($horaI))/3600),3);
        $hora2= round(((strtotime($horaF) - strtotime('07:30'))/3600),3);
        $horasAcumuladas = $hora1 + $horasIntermedias + $hora2; 
      }
      $permiso->horas_acumuladas = $horasAcumuladas;
      //dd($horasAcumuladas);
      if ($horasAcumuladas > $tipoPermiso->numero_horas) {
        flash("¡ERROR! ¡Esta superando el total de horas permitidas!")->error()->important();
        return redirect()->action('Marcaciones\PermisosController@buscarPermisoEmpleado');
      }
      $permiso->horas_acumuladas = $horasAcumuladas;
      $permiso->save();
      
      $permiso->save();
      
    $fechaRango=[]; //array de fechas en blancoç
    for($date = $fecha1; $date->lte($fecha2); $date->addDay()) { //recorremos en un for las fechas de inicio y final por dias
    $fechaRango[] = $date->format('Y-m-d'); //guardamos en el array las fechas
  }
     if($request->fecha_inicio != $request->fecha_final){// si las fecha de inicio y final son diferentes
     foreach($fechaRango as $fechas){ //entramos al arreglo de fecha
      $detallePermiso = new PermisoDetalle(); //creamos el objeto para guardar esa fechas con sus horas
      $empleado = Empleado::find($request->id_empleado);
      $detallePermiso->permiso_id = $permiso->id_permiso;
      $detallePermiso->fecha_permiso = $fechas;
      $calendario = DB::table('mar_calendario as c')->leftJoin('mar_calendario_emp as ce', 'ce.calendario_id', '=', 'c.id_calendario')->select('ce.id_calendario_emp')->where('fecha', '=', $fechas)->where('ce.empleado_id', '=', $empleado->id_empleado)->first();
      $detallePermiso->emp_calendario_id= $calendario->id_calendario_emp;
      if($fechas==$request->fecha_inicio){// se pregunta si la fecha del array es la del inicio
        $detallePermiso->hora_inicio= $horaI;
        $detallePermiso->hora_fin='15:30:00';
        $detallePermiso->horas_acumuladas= round(((strtotime('15:30')-strtotime($horaI))/3600),3);
      $detallePermiso->save(); //se guarda las horas de esa fecha
      }elseif ($fechas==$request->fecha_final) { //se pregunta si la fecha del array es la de final para poder calcular las horas finales
        $detallePermiso->hora_inicio= '07:30:00';
        $detallePermiso->hora_fin=$horaF;
        $detallePermiso->horas_acumuladas= round(((strtotime($horaF) - strtotime('07:30'))/3600),3);
      $detallePermiso->save(); //se guarda las horas de esa fecha
      }else{// las otras fechas intermedias se hace con 8 horas automaticamente
        $detallePermiso->hora_inicio='07:30:00';
        $detallePermiso->hora_fin= '15:30:00';
        $detallePermiso->horas_acumuladas=8.000;
        $detallePermiso->save();
      }
     }//fin del foreach
      }//fin del if que valida las fechas

    }else{
    //valida los fines de semana

    $sabado = 0; //acumulador de fin de semana
    $domingo = 0; //acumulador de fin de semana
    foreach($rango as $date){ //entramos al rango entre fecha inicio y fecha final
      $days = $date->format('D'); //traemos el nombre del dia de cada fecha
      if ($days == 'Sat') { //consultamos si es sabado
        $sabado++;  //aumenta en uno si es sabado
      }
      if ($days == 'Sun') { //consultamos si es domingo
        $domingo++;  //aumenta en uno si es domingo
      }
    }//fin del foreach
    //valida asuetos
    $asueto = DB::table('mar_asueto')->select(DB::raw('COUNT(id_asueto) as diaAsueto'))->whereBetween('fecha', [$fecha1, $fecha2])->first(); //cuenta el numero de dias de asueto que hay entre fecha de inicio y fecha final
    foreach ($asueto as $asuetos) {

      $cantidadAsuetos = $asueto->diaAsueto; // se recorre el objeto de asuetos para traer la cantidad de dias de asueto
    }
    $diasDiferencia = $fecha2->DiffInDays($fecha1); //dias de diferencia entre fecha inicio y fecha final

    $dias=($diasDiferencia) - ($sabado + $domingo) - $cantidadAsuetos;
    //se guarda cuantos dias hay entre fechas, quitando los asuetos y los fines de semana.

    if($dias > 1 ){
      for ($i=1; $i < $dias ; $i++) { 
        $horasIntermedias = $horasIntermedias +8;
      }  
      $hora1= round(((strtotime('15:30')-strtotime($horaI))/3600),3);
      $hora2= round(((strtotime($horaF) - strtotime('07:30'))/3600),3);
      $horasAcumuladas = $hora1 + $horasIntermedias + $hora2; 
    }elseif ($dias==1) {
      $hora1= round(((strtotime('15:30')-strtotime($horaI))/3600),3);
      $hora2= round(((strtotime($horaF) - strtotime('07:30'))/3600),3);
      $horasAcumuladas = $hora1 +$hora2; 
    }elseif($dias==0){
      $horasAcumuladas = round(((strtotime($horaF)-strtotime($horaI))/3600),3);
    }

    $permiso->horas_acumuladas = $horasAcumuladas;

    //validacion de resta de horas disponilbles 
    $tipoPermiso = TipoPermiso::find($request->tipo_permiso);
    $permisoH = DB::table('mar_permiso')->select(DB::raw('SUM(horas_acumuladas) as total_hora'))->where('empleado_id', '=',$request->id_empleado)->where('tipo_permiso_id', '=', $request->tipo_permiso)->first();

    if(!$permisoH == null){
    //suma las horas que tiene ya acumuladas mas las que se estan solicitando en la creacion del permiso.
      $suma = $permisoH->total_hora + $horasAcumuladas;
    //valida que si la suma es mayor a las horas disponibles mostrara un mensaje informado que ha terminado las horas .
    } 
    $resta1 = 0;
    if($suma > $tipoPermiso->numero_horas){
      $resta1 =  $suma - $tipoPermiso->numero_horas;
      if ($resta1 < 1) {
        $resta1 =  round(($resta1 *60),1);
        flash("¡ERROR! ¡Has terminado las horas disponibles del permiso ". $tipoPermiso->descripcion. " no puedes ingresar más! Se esta ingresando ".$resta1. " minutos más de las disponibles.  ")->error()->important();
      }else{
        flash("¡ERROR! ¡Has terminado las horas disponibles del permiso ". $tipoPermiso->descripcion. " no puedes ingresar más! Se esta ingresando ".$resta1. " horas más de las disponibles.  ")->error()->important();
      }
      return redirect()->action('Marcaciones\PermisosController@buscarPermisoEmpleado');

    }
    $permiso->save();
   }//fin del ese de validar si el permiso ingresado es por licencia de maternidad

    $empleado = Empleado::find($request->id_empleado); //Buscamos el empleado que hizo el permiso
    $fechaAsueto = DB::table('mar_asueto')->whereBetween('fecha', [$fecha1, $fecha2])->get(); //encontramos los dias de asueto que hay entre ambas fechas
    $asuetos=[]; // array de asuetos en blanco
    $fechaRango=[]; //array de fechas en blanco


       for($date = $fecha1; $date->lte($fecha2); $date->addDay()) { //recorremos en un for las fechas de inicio y final por dias
        $days = $date->format('D'); //obtenemos el nombre de esos dias
      if ($days != 'Sat' && $days!= 'Sun') { //validamos que los dias no sea sabado y domingo
           $fechaRango[] = $date->format('Y-m-d'); //guardamos en el array las fechas dentro del rango de fecha inicio y fecha final
         }
       }

     if($fechaAsueto!= null){ //si hay algun asueto
      foreach ($fechaAsueto as $asueto) {
        $asuetos[]= $asueto->fecha; //guardamos los dias de asueto encontrados
      }
    }

     $rangoFecha= array_diff($fechaRango, $asuetos);    //buscamos y guardamos los dias que son diferentes a los dias de asueto
     if($request->fecha_inicio != $request->fecha_final){// si las fecha de inicio y final son diferentes


     foreach($rangoFecha as $fechas){ //entramos al arreglo de fecha
      $detallePermiso = new PermisoDetalle(); //creamos el objeto para guardar esa fechas con sus horas
      $detallePermiso->permiso_id = $permiso->id_permiso;
      $detallePermiso->fecha_permiso = $fechas;
      $calendario = DB::table('mar_calendario as c')->leftJoin('mar_calendario_emp as ce', 'ce.calendario_id', '=', 'c.id_calendario')->select('ce.id_calendario_emp')->where('fecha', '=', $fechas)->where('ce.empleado_id', '=', $empleado->id_empleado)->first();
      $detallePermiso->emp_calendario_id= $calendario->id_calendario_emp;
      if($fechas==$request->fecha_inicio){// se pregunta si la fecha del array es la del inicio
        $detallePermiso->hora_inicio= $horaI;
        $detallePermiso->hora_fin='15:30:00';
        $detallePermiso->horas_acumuladas= round(((strtotime('15:30')-strtotime($horaI))/3600),3);
      $detallePermiso->save(); //se guarda las horas de esa fecha
      }elseif ($fechas==$request->fecha_final) { //se pregunta si la fecha del array es la de final para poder calcular las horas finales
        $detallePermiso->hora_inicio= '07:30:00';
        $detallePermiso->hora_fin=$horaF;
        $detallePermiso->horas_acumuladas= round(((strtotime($horaF) - strtotime('07:30'))/3600),3);
      $detallePermiso->save(); //se guarda las horas de esa fecha
      }else{// las otras fechas intermedias se hace con 8 horas automaticamente
        $detallePermiso->hora_inicio='07:30:00';
        $detallePermiso->hora_fin= '15:30:00';
        $detallePermiso->horas_acumuladas=8.000;
        $detallePermiso->save();
      }

      
    }

      }elseif ($request->fecha_inicio == $request->fecha_final) { //en el caso que sea un solo dia simplemente se hace el calcula de horas de ese mismo dia.
        foreach($fechaRango as $fechas){
          //guardar el permiso detalle
          $detallePermiso = new PermisoDetalle();
          $detallePermiso->permiso_id = $permiso->id_permiso;
          $detallePermiso->fecha_permiso = $fechas;
          $calendario = DB::table('mar_calendario as c')->leftJoin('mar_calendario_emp as ce', 'ce.calendario_id', '=', 'c.id_calendario')->select('ce.id_calendario_emp')->where('fecha', '=', $fechas)->where('ce.empleado_id', '=', $empleado->id_empleado)->first();
          $detallePermiso->emp_calendario_id= $calendario->id_calendario_emp;
          $detallePermiso->hora_inicio=$horaI;
          $detallePermiso->hora_fin=$horaF;
          $detallePermiso->horas_acumuladas=round(((strtotime($horaF) - strtotime($horaI))/3600),3);
          $detallePermiso->save();

        }

      }

      Alert::success('Registrar Permiso de Empleado', $empleado->nombre.' '.$empleado->apellido  . ' de forma existosa')->autoClose(1200);
      return redirect()->action('Marcaciones\PermisosController@buscarPermisoEmpleado');

    }
    public function listarPermisoEmpleado(){
      //muesta el listado de permisos de empleados

      $permiso = Permiso::where('estado', '=', 1)->orderBy('id_permiso', 'DESC')->get();
      $permisoDeshabilitados = Permiso::where('estado', '=', 0)->orderBy('id_permiso', 'DESC')->get();   

      return view('marcacion.gestionPermisos.listar' , compact('permiso', 'permisoDeshabilitados')); 
    }

    public function editarPermisoEmpleado($id){
      //carga la vista de editar permiso empleado

      $permiso = Permiso::find($id);
      $tipoPermiso = TipoPermiso::all();
      $empleado_id = $permiso->empleado_id;
      $empleado = Empleado::find($empleado_id);
      $permisos = DB::table('detalle_permiso_empleado_vw')->where('empleado_id','=', $empleado->id_empleado)->get();
      $fechaIngreso= $empleado->fecha_ingreso;
      $fecha = Carbon::now(new \DateTimeZone('America/El_Salvador'));
        $anios = $fecha->diff($fechaIngreso); // resta la fecha actual con la fecha de ingreso
        $dias = $anios->format('%a');
        if (date('L', strtotime($fecha))) { 
          $anios_trabajados = round(($dias/366),4);

        } else {
         $anios_trabajados = round(($dias/365),4);
       }

       if ($anios_trabajados >=6){
      //Validacion que muestra el permiso de:PERMISOS O LICENCIAS POR ENFERMEDAD si el empleado tiene 6 o mas años de servicios activos en el ISTA
        if ($empleado->sexo == 'Hombre') {

          $tipoPermiso = TipoPermiso::where('codigo_permiso','<>', 02)->where('codigo_permiso','<>', 03)->where('codigo_permiso','<>', 04)->where('codigo_permiso','<>', 05)->where('codigo_permiso','<>', 10)->get();
        }else{
          $tipoPermiso = TipoPermiso::where('codigo_permiso','<>', 02)->where('codigo_permiso','<>', 03)->where('codigo_permiso','<>', 04)->where('codigo_permiso','<>', 05)->where('codigo_permiso','<>', 62)->get();
        }
      }elseif($anios_trabajados >=4 &&  $anios_trabajados <6){

    //Validacion que muestra el permiso de:PERMISOS O LICENCIAS POR ENFERMEDAD si el empleado tiene de 4 años a 6 años de servicios activos en el ISTA
        if ($empleado->sexo == 'Hombre') {

          $tipoPermiso = TipoPermiso::where('codigo_permiso','<>', 01)->where('codigo_permiso','<>', 03)->where('codigo_permiso','<>', 04)->where('codigo_permiso','<>', 05)->where('codigo_permiso','<>', 10)->get();
        }else{
          $tipoPermiso = TipoPermiso::where('codigo_permiso','<>', 01)->where('codigo_permiso','<>', 03)->where('codigo_permiso','<>', 04)->where('codigo_permiso','<>', 05)->where('codigo_permiso','<>', 62)->get();
        }

      }elseif($anios_trabajados >=1 &&  $anios_trabajados <4){

    //Validacion que muestra el permiso de:PERMISOS O LICENCIAS POR ENFERMEDAD si el empleado tiene de 1 año a 4 años de servicios activos en el ISTA
    //
       if ($empleado->sexo == 'Hombre') {

        $tipoPermiso = TipoPermiso::where('codigo_permiso','<>', 01)->where('codigo_permiso','<>', 02)->where('codigo_permiso','<>', 04)->where('codigo_permiso','<>', 05)->where('codigo_permiso','<>', 10)->get();
      }else{
        $tipoPermiso = TipoPermiso::where('codigo_permiso','<>', 01)->where('codigo_permiso','<>', 02)->where('codigo_permiso','<>', 04)->where('codigo_permiso','<>', 05)->where('codigo_permiso','<>', 62)->get();
      }

    }elseif($anios_trabajados < 1 && $empleado->sexo == 'Hombre'){

    //Validacion que muestra el permiso de:PERMISOS O LICENCIAS POR ENFERMEDAD si el empleado es HOMBRE y tiene menos de un año en la empresa

      $tipoPermiso = TipoPermiso::where('codigo_permiso','<>', 01)->where('codigo_permiso','<>', 02)->where('codigo_permiso','<>', 03)->where('codigo_permiso','<>', 05)->where('descripcion','<>', 'LICENCIA POR MATERNIDAD')->get();

    }elseif ($anios_trabajados < 1 && $empleado->sexo == 'Mujer' ) {

    //Validacion que muestra el permiso de:PERMISOS O LICENCIAS POR ENFERMEDAD si el empleado es Mujer y tiene menos de un año en la empresa

      $tipoPermiso = TipoPermiso::where('codigo_permiso','<>', 01)->where('codigo_permiso','<>', 02)->where('codigo_permiso','<>', 03)->where('codigo_permiso','<>', 04)->where('descripcion','<>', 'LICENCIA POR PATERNIDAD')->get();
    }
    return view ('marcacion.gestionPermisos.editar', compact('permiso', 'tipoPermiso', 'anios_trabajados','permisos'));   

  }

  public function actualizarPermisoEmpleado(Request $request, $id){
    //actualiza los datos del permiso por empleado
    Carbon::setLocale('es_SV');
    $fecha = Carbon::now(new \DateTimeZone('America/El_Salvador'));

    $date = $fecha->format('Y-m-d');
    $permiso = Permiso::find($id);
    $permiso->estado=1;
    $permiso->motivo_permiso = $request->motivo_permiso;
    $permiso->fecha_inicio = $request->fecha_inicio;
    $permiso->fecha_fin = $request->fecha_final;
    $permiso->hora_inicio = $request->hora_inicio;
    $permiso->hora_fin = $request->hora_final;
    $permiso->tipo_permiso_id = $request->tipo_permiso;
    $fecha1= Carbon::parse($request->fecha_inicio);
    $fecha2= Carbon::parse($request->fecha_final);
    $dife = $fecha1->diff($fecha2);

    $permisoExiste = DB::table('mar_permiso as p')->where('p.empleado_id', '=', $request->id_empleado)->where('p.fecha_inicio', '=', $request->fecha_inicio)->where('p.fecha_fin', '=', $request->fecha_final)->where('p.hora_inicio', '=', $request->hora_inicio)->where('p.hora_fin', '=', $request->hora_final)->where('p.tipo_permiso_id', '=', $request->tipo_permiso)->where('p.id_permiso', '<>', $permiso->id_permiso)->get();

    //dd($permisoExiste);

    if(!$permisoExiste->isEmpty()){

      flash("¡ERROR! ¡El permiso ya ha sido registrado!")->error()->important();
      return redirect()->action('Marcaciones\PermisosController@buscarPermisoEmpleado');
    }

    if($fecha1!=null && $fecha2!=null){

      if($fecha2 < $fecha1){

        flash("¡ERROR! ¡La fecha final no puede ser menor que la fecha de inicio!")->error()->important();
        return redirect()->action('Marcaciones\PermisosController@buscarPermisoEmpleado');
      }

    }

    $horasIntermedias = 0;
    $horaI = $request->hora_inicio;
    $horaF = $request->hora_final;

    $intervalo = new DateInterval('P1D');
    $rango = new DatePeriod($fecha1, $intervalo ,$fecha2);


    $fechaHoI = $fecha1.' '.$horaI;
    $fechaHoF = $fecha2.' '.$horaF;

    if($fechaHoF < $fechaHoI){

      flash("¡ERROR! ¡La hora final no puede ser menor a la hora inicial!")->error()->important();
      return redirect()->action('Marcaciones\PermisosController@buscarPermisoEmpleado');
    }

    $sabado = 0;
    $domingo = 0;
    foreach($rango as $date){
      $days = $date->format('D');
      if ($days == 'Sat') {
        $sabado++;
      }
      if ($days == 'Sun') {
        $domingo++;
      }
    }
    $asueto = DB::table('mar_asueto')->select(DB::raw('COUNT(id_asueto) as diaAsueto'))->whereBetween('fecha', [$fecha1, $fecha2])->first();
    foreach ($asueto as $asuetos) {

      $cantidadAsuetos = $asueto->diaAsueto;
    }

    $diasDiferencia = $fecha2->DiffInDays($fecha1);

    $dias=($diasDiferencia) - ($sabado + $domingo) - $cantidadAsuetos;

    if($dias > 1 ){
      for ($i=1; $i < $dias ; $i++) { 
        $horasIntermedias = $horasIntermedias +8;
      }  
      $hora1= round(((strtotime('15:30')-strtotime($horaI))/3600),3);
      $hora2= round(((strtotime($horaF) - strtotime('07:30'))/3600),3);
      $horasAcumuladas = $hora1 + $horasIntermedias + $hora2; 
    }else{
      $horasAcumuladas = round(((strtotime($horaF)-strtotime($horaI))/3600),3);
    }

    $permiso->horas_acumuladas = $horasAcumuladas;
    $permiso->update();
    $empleado_id = $permiso->empleado_id;
    $empleado = Empleado::find($empleado_id);
    DB::table('mar_permiso_detalle')->where('permiso_id', '=', $id)->delete();
    $fechaAsueto = DB::table('mar_asueto')->whereBetween('fecha', [$fecha1, $fecha2])->get();
    $asuetos=[];
    $fechaRango=[];


    for($date = $fecha1; $date->lte($fecha2); $date->addDay()) {
      $days = $date->format('D');
      if ($days != 'Sat' && $days!= 'Sun') {
       $fechaRango[] = $date->format('Y-m-d');
     }
   }

   if($fechaAsueto!= null){
    foreach ($fechaAsueto as $asueto) {
      $asuetos[]= $asueto->fecha;
    }
  }

  $rangoFecha= array_diff($fechaRango, $asuetos);    
  if($request->fecha_inicio != $request->fecha_final){


   foreach($rangoFecha as $fechas){
    $detallePermiso = new PermisoDetalle();
    $detallePermiso->permiso_id = $permiso->id_permiso;
    $detallePermiso->fecha_permiso = $fechas;
    $calendario = DB::table('mar_calendario as c')->leftJoin('mar_calendario_emp as ce', 'ce.calendario_id', '=', 'c.id_calendario')->select('ce.id_calendario_emp')->where('fecha', '=', $fechas)->where('ce.empleado_id', '=', $empleado->id_empleado)->first();
    $detallePermiso->emp_calendario_id= $calendario->id_calendario_emp;
    if($fechas==$request->fecha_inicio){
      $detallePermiso->hora_inicio= $horaI;
      $detallePermiso->hora_fin='15:30:00';
      $detallePermiso->horas_acumuladas= round(((strtotime('15:30')-strtotime($horaI))/3600),3);
      $detallePermiso->save();
    }elseif ($fechas==$request->fecha_final) {
      $detallePermiso->hora_inicio= '07:30:00';
      $detallePermiso->hora_fin=$horaF;
      $detallePermiso->horas_acumuladas= round(((strtotime($horaF) - strtotime('07:30'))/3600),3);
      $detallePermiso->save();
    }else{
      $detallePermiso->hora_inicio='07:30:00';
      $detallePermiso->hora_fin= '15:30:00';
      $detallePermiso->horas_acumuladas=8.000;
      $detallePermiso->save();
    }


  }

}elseif ($request->fecha_inicio == $request->fecha_final) {
  foreach($fechaRango as $fechas){
    $detallePermiso = new PermisoDetalle();
    $detallePermiso->permiso_id = $permiso->id_permiso;
    $detallePermiso->fecha_permiso = $fechas;
    $calendario = DB::table('mar_calendario as c')->leftJoin('mar_calendario_emp as ce', 'ce.calendario_id', '=', 'c.id_calendario')->select('ce.id_calendario_emp')->where('fecha', '=', $fechas)->where('ce.empleado_id', '=', $empleado->id_empleado)->first();
    $detallePermiso->emp_calendario_id= $calendario->id_calendario_emp;
    $detallePermiso->hora_inicio=$horaI;
    $detallePermiso->hora_fin=$horaF;
    $detallePermiso->horas_acumuladas=round(((strtotime($horaF) - strtotime($horaI))/3600),3);
    $detallePermiso->save();


  }

}

Alert::warning('Permiso Editado de Empleado', $empleado->nombre.' '.$empleado->apellido  . ' de forma existosa')->autoClose(2000)->iconHtml('<i class="fa fa-pencil"></i>');
return redirect()->action('Marcaciones\PermisosController@listarPermisoEmpleado');

}

public function eliminarPermiso($id){
  //desactiva el permiso seleccionado
  $permiso = Permiso::find($id);  
  $permiso->estado=0;
  $permiso->update();
  $empleado_id = $permiso->empleado_id;
  $empleado = Empleado::find($empleado_id);
  Alert::error('Eliminar Permiso', $empleado->nombre.' '.$empleado->apellido  . ' de forma existosa')->autoClose(2000)->iconHtml('<i class="fa fa-trash-o"></i>');
  return redirect()->action('Marcaciones\PermisosController@listarPermisoEmpleado');

}

public function habilitarPermiso($id){
  //activa nuevamente el permiso seleccionado 
  $permiso = Permiso::find($id);
  $permiso->estado=1;
  $permiso->update();
  $empleado_id = $permiso->empleado_id;
  $empleado = Empleado::find($empleado_id);
  Alert::success('Habilitar Tipo de Permiso', $empleado->nombre.' '.$empleado->apellido  . ' de forma existosa')->autoClose(2000);
  return redirect()->action('Marcaciones\PermisosController@listarPermisoEmpleado');

}

    public function buscarConsultaPermisoEmpleado(){

       $empleado = Empleado::all();
        return view('marcacion.gestionPermisos.consulta', compact('empleado')); //llama a la vista de consulta
      }

      public function getOficinas(Request $request)
      {

        $oficinas = DB::table('emp_oficina')
        ->where('unidad_administrativa_id', $request->unidad_administrativa)
        ->where('centro_id',$request->centro)
        ->get();

        foreach ($oficinas as $oficina) {
          $oficinaArray[$oficina->id_oficina] = $oficina->oficina;
        }
        return response()->json($oficinaArray);
        
      }

      public function getEmpleado(Request $request){

        if ($request->ajax()) {
          $empleados = Empleado::where('oficina_id', $request->oficina)->get();
          foreach ($empleados as $empleado) {
            $empleadoArray[$empleado->id_empleado] = $empleado->nombre.' '.$empleado->apellido;
          }
          return response()->json($empleadoArray);
        }
      }

      public function consultarPermisoEmpleado(Request $request){

        Carbon::setLocale('es_SV');
        $fecha = Carbon::now(new \DateTimeZone('America/El_Salvador'));
        $mes = $fecha->monthName;
        $anios = $fecha->year;
        settype($anios, 'string');
        $empleado = Empleado::where('id_empleado', $request->codigo_id)->get(); 

        foreach($empleado as $empleado){
          $arrayEmpleado['fecha_actual']= $anios;
          $arrayEmpleado['codigo']= $empleado->codigo_empleado;
          $arrayEmpleado['nombre']= $empleado->nombre.' '.$empleado->apellido;
          $arrayEmpleado['centro']= $empleado->oficina->centro->centro;
          $arrayEmpleado['unidad']= $empleado->oficina->unidadAdministrativa->unidad_administrativa;
          $arrayEmpleado['oficina']= $empleado->oficina->oficina;

          $permiso = DB::table('detalle_permiso_empleado_vw')->where('empleado_id','=', $empleado->id_empleado)->orderBy('codigo_permiso', 'ASC')->get();
          $i=0;
          $detalle=[];
          foreach($permiso as $permiso){
            $det['codigo']=$permiso->codigo_permiso;
            $det['descripcion']=$permiso->descripcion;
            $det['horas']=$permiso->numero_horas;
            $det['horas_acumuladas']=$permiso->horas_acumuladas;
            $det['resta_horas']=$permiso->resta_hora;
            $detalle[$i]=$det;
            $i++;
          }

          $permisoDetalle = Permiso::where('empleado_id', '=', $empleado->id_empleado)->orderBy('tipo_permiso_id', 'ASC')->get();
          $j=0;
          $detallePer=[];
          foreach($permisoDetalle as $permiso){
            $dPermiso['codigo']=$permiso->tipoPermiso->codigo_permiso;
            $dPermiso['descripcion']=$permiso->tipoPermiso->descripcion;
            $dPermiso['fecha_emision']=Carbon::parse($permiso->fecha_registro)->format('d-m-Y');
            $dPermiso['fecha_desde']=Carbon::parse($permiso->fecha_inicio)->format('d-m-Y');
            $dPermiso['hora_inicio']=$permiso->hora_inicio;
            $dPermiso['fecha_hasta']=Carbon::parse($permiso->fecha_fin)->format('d-m-Y');
            $dPermiso['hora_hasta']=$permiso->hora_fin;
            $dPermiso['horas_permiso']=$permiso->horas_acumuladas;
            $detallePer[$j]=$dPermiso;
            $j++;
          }

          $permisoTotal = DB::table('mar_permiso')->select(DB::raw('SUM(horas_acumuladas) as total_hora'))->where('empleado_id', '=',$empleado->id_empleado)->first();
          $total['acumulado']=$permisoTotal->total_hora;
          //$arrayTotal['total']=$total;

          $array['empleado']=$arrayEmpleado;
          $array['detalle']=$detalle;
          $array['detalleP']=$detallePer;
          $array['total']=$total;
        }

        return response()->json($array);
      }

      public function buscarReportePermisoSinGoceSueldo(){
        $vacio = "";
        return view('marcacion.gestionPermisos.verPermisoSinGoceSueldo', compact('vacio'));
      } 

      public function reportePermisoSinGoceSueldo(Request $request){
        //Obtiene Fecha Actual
        Carbon::setLocale('es_SV');
        $fecha = Carbon::now(new \DateTimeZone('America/El_Salvador'));
        $hora= $fecha->format('h:i:s A'); 
        $date = $fecha->format('Y-m-d');
        $day = $fecha->format('d-m-Y');
        $mes = $fecha->monthName;
        $anio = $fecha->year;
         //fin de Fecha Actual
        $fecha_inicio = $request->input('fecha_inicio');
        $fecha_final = $request->input('fecha_final');

        $fechaFin = Carbon::parse($fecha_final); //convierte la fecha final
        $mesFin = $fechaFin->monthName; //obtengo el mes de la fecha fin

        $fechaIni = Carbon::parse($fecha_inicio); //convierte la fecha inicial
        $mesIni = $fechaIni->monthName; //obtengo el mes de la fecha inicial


        if($fecha_inicio!=null && $fecha_final!=null){

          if($fecha_final < $fecha_inicio){

            flash("¡ERROR! ¡La fecha final no puede ser menor que la fecha de inicio!")->error()->important();
            return redirect()->action('Marcaciones\PermisosController@buscarReportePermisoSinGoceSueldo');
          }
          if($fecha_inicio > $date){

           flash("¡ERROR! ¡La fecha inicio no puede ser mayor que la fecha de actual!")->error()->important();
           return redirect()->action('Marcaciones\PermisosController@buscarReportePermisoSinGoceSueldo');
         }
         if($fecha_final > $date){

           flash("¡ERROR! ¡La fecha final no puede ser mayor que la fecha actual!")->error()->important();
           return redirect()->action('Marcaciones\PermisosController@buscarReportePermisoSinGoceSueldo');
         }
       }
       $permisoSinGoce = DB::table('permiso_sin_goce_sueldo_vw as ps')->select('ps.codigo_empleado','ps.nombre','ps.apellido', 'ps.salario_actual','ps.tipo_plaza','ps.mes','ps.anio',DB::raw('SUM(horas_acumuladas) as horas'))->whereBetween('ps.fecha_permiso', [$fecha_inicio, $fecha_final])->groupBy('ps.codigo_empleado','ps.nombre','ps.apellido','ps.salario_actual','ps.tipo_plaza', 'ps.mes', 'ps.anio')->whereIntegerInRaw('ps.codigo_permiso', ['12', '27'])->get();

       $arrayDescuento= [];

       foreach ($permisoSinGoce as $permiso) {
        $mesD = $permiso->mes;
        $salario = $permiso->salario_actual;
        $horas = $permiso->horas;

        $tipo = $permiso->tipo_plaza;
        $codigo = $permiso->codigo_empleado;
        $nombre = $permiso->nombre.' '.$permiso->apellido;
        $year=$permiso->anio;
        if($mesD == 1 || $mesD == 3 || $mesD == 5 || $mesD == 7 || $mesD == 8 || $mesD == 10 || $mesD ==12){
          $dia = 31;
          $descHoras = ($salario/$dia/8)*$horas;
          $des = round($descHoras,2);
          $arrayDescuento[]=[$codigo,$nombre, $tipo, $salario, $horas,$des];
        }
        elseif ($mesD == 4 || $mesD == 6 || $mesD == 9 || $mesD == 11) {
         $dia = 30;
         $descHoras = ($salario/$dia/8)*$horas;
         $des = round($descHoras,2);
         $arrayDescuento[]=[$codigo,$nombre, $tipo, $salario, $horas,$des];
       }
       else{

        if (($year%4 == 0 && $year%100 != 0) || $year%400 == 0) {
          $dia = 29;
          $descHoras = ($salario/$dia/8)*$horas;
          $des = round($descHoras,2);
          $arrayDescuento[]=[$codigo,$nombre, $tipo, $salario, $horas,$des];

          $descuentoArray[]=[$codigo,$nombre, $tipo, $salario, $horas,$minutos,$des];

        }
        else{
          $dia = 28;
          $descHoras = ($salario/$dia/8)*$horas;
          $des = round($descHoras,2);
          $arrayDescuento[]=[$codigo,$nombre, $tipo, $salario, $horas,$des];

        }
      }

       }//fin de foreach
       $vacio="Existe";

       $noPermiso= $permisoSinGoce->isEmpty();

       if($noPermiso == true)
       {
        flash('¡No se tiene dicha información en el sistema!')->error()->important();
        return redirect()->action('Marcaciones\PermisosController@buscarReportePermisoSinGoceSueldo');
      }

      return view('marcacion.gestionPermisos.verPermisoSinGoceSueldo', compact('arrayDescuento', 'vacio', 'fecha_inicio', 'fecha_final', 'day', 'mesFin', 'mesIni', 'hora', 'date')); 
    }

    public function permisoSinGoceSueldoPDF(Request $request){

      Carbon::setLocale('es_SV');
      $fecha = Carbon::now(new \DateTimeZone('America/El_Salvador'));
      $hora= $fecha->format('h:i:s A'); 
      $date = $fecha->format('Y-m-d');
      $day = $fecha->format('d-m-Y');
      $mes = $fecha->monthName;
      $anio = $fecha->year;

         //fin de Fecha Actual
      $fecha_inicio = $request->input('fecha_ini');
      $fecha_final = $request->input('fecha_fin');

        $fechaFin = Carbon::parse($fecha_final); //convierte la fecha final
        $mesFin = $fechaFin->monthName; //obtengo el mes de la fecha fin

        $fechaIni = Carbon::parse($fecha_inicio); //convierte la fecha inicial
        $mesIni = $fechaIni->monthName; 

       $permisoSinGoce = DB::table('permiso_sin_goce_sueldo_vw as ps')->select('ps.codigo_empleado','ps.nombre','ps.apellido', 'ps.salario_actual','ps.tipo_plaza','ps.mes','ps.anio',DB::raw('SUM(horas_acumuladas) as horas'))->whereBetween('ps.fecha_permiso', [$fecha_inicio, $fecha_final])->groupBy('ps.codigo_empleado','ps.nombre','ps.apellido','ps.salario_actual','ps.tipo_plaza', 'ps.mes', 'ps.anio')->whereIntegerInRaw('ps.codigo_permiso', ['12', '27'])->get();

        $arrayDescuentoC= [];
        $arrayDescuentoL= [];

        foreach ($permisoSinGoce as $permiso) {
          $mesD = $permiso->mes;
          $salario = $permiso->salario_actual;
          $horas = $permiso->horas;

          $tipo = $permiso->tipo_plaza;
          $codigo = $permiso->codigo_empleado;
          $nombre = $permiso->nombre.' '.$permiso->apellido;
          $year=$permiso->anio;
          if($mesD == 1 || $mesD == 3 || $mesD == 5 || $mesD == 7 || $mesD == 8 || $mesD == 10 || $mesD ==12){
            $dia = 31;
            $descHoras = ($salario/$dia/8)*$horas;
            $des = round($descHoras,2);
                //$arrayDescuento[]=[$codigo,$nombre, $tipo, $salario, $horas,$des];

            if($tipo == 'Contrato'){
              $arrayDescuentoC[]=[$codigo,$nombre, $tipo, $salario, $horas,$des];
            }elseif ($tipo == 'Ley de Salarios') {
              $arrayDescuentoL[]=[$codigo,$nombre, $tipo, $salario, $horas,$des];
            }
          }
          elseif ($mesD == 4 || $mesD == 6 || $mesD == 9 || $mesD == 11) {
           $dia = 30;
           $descHoras = ($salario/$dia/8)*$horas;
           $des = round($descHoras,2);
                //$arrayDescuento[]=[$codigo,$nombre, $tipo, $salario, $horas,$des];
                //
           if($tipo == 'Contrato'){
            $arrayDescuentoC[]=[$codigo,$nombre, $tipo, $salario, $horas,$des];
          }elseif ($tipo == 'Ley de Salarios') {
            $arrayDescuentoL[]=[$codigo,$nombre, $tipo, $salario, $horas,$des];
          }
        }
        else{

          if (($year%4 == 0 && $year%100 != 0) || $year%400 == 0) {
            $dia = 29;
            $descHoras = ($salario/$dia/8)*$horas;
            $des = round($descHoras,2);
                //$arrayDescuento[]=[$codigo,$nombre, $tipo, $salario, $horas,$des];

                //$descuentoArray[]=[$codigo,$nombre, $tipo, $salario, $horas,$minutos,$des];
            if($tipo == 'Contrato'){
              $arrayDescuentoC[]=[$codigo,$nombre, $tipo, $salario, $horas,$des];
            }elseif ($tipo == 'Ley de Salarios') {
              $arrayDescuentoL[]=[$codigo,$nombre, $tipo, $salario, $horas,$des];
            }

          }
          else{
            $dia = 28;
            $descHoras = ($salario/$dia/8)*$horas;
            $des = round($descHoras,2);
               // $arrayDescuento[]=[$codigo,$nombre, $tipo, $salario, $horas,$des];

            if($tipo == 'Contrato'){
              $arrayDescuentoC[]=[$codigo,$nombre, $tipo, $salario, $horas,$des];
            }elseif ($tipo == 'Ley de Salarios') {
              $arrayDescuentoL[]=[$codigo,$nombre, $tipo, $salario, $horas,$des];
            }


          }
        }

       }//fin de foreach


       $pdf = \PDF::loadView('marcacion.gestionPermisos.imprimirPermisoSinGoceSueldo', ['permisoSinGoce' => $permisoSinGoce, 'fecha_inicio' => $fecha_inicio, 'fecha_final' => $fecha_final], compact('dia', 'hora','mesIni', 'mesFin', 'arrayDescuentoL','arrayDescuentoC', 'day'))->setPaper('A4', 'landscape')->setWarnings(false);
       return $pdf->stream('permisoSinGoceSueldo.pdf');


     }




   }
