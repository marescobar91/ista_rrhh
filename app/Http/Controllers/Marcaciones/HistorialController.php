<?php

namespace App\Http\Controllers\Marcaciones;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Modelos\Marcacion\Historial;
use DB;
use Carbon\Carbon;


class HistorialController extends Controller
{
	public function __construct(){
		//funcion de los permisos para el historial
    $this->middleware(['permission:listar.historial'], ['only'    => ['listarHistorial']]);
 
    }

   public function listarHistorial(){
   		//muestra el listado de historial 
   		$historial = Historial::orderBy('fecha_importar','ASC')->get();
	  	return view('marcacion.historialMarcacion.listar' , compact('historial')); 	
	}




}
