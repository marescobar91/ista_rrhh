<?php

namespace App\Http\Controllers\Marcaciones;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Modelos\Marcacion\TipoPermiso;
use RealRashid\SweetAlert\Facades\Alert;

class TipoPermisosController extends Controller
{
     public function __construct(){

    $this->middleware(['permission:editar.catalogos'], ['only'   => ['editarTipoPermiso', 'actualizarTipoPermiso']]);
    $this->middleware(['permission:eliminar.catalogos'], ['only'   => ['eliminarTipoPermiso']]);
    $this->middleware(['permission:habilitar.catalogos'], ['only'   => ['habilitarTipoPermiso']]);
    $this->middleware(['permission:crear.catalogos'], ['only'   => ['crearTipoPermiso', 'guardarTipoPermiso']]);
    $this->middleware(['permission:consultar.catalogos'], ['only'   => 'listarTipoPermiso']); 
     }

    public function listarTipoPermiso(){

    $tipoPermiso = TipoPermiso::where('estado', '=', 1)->orderBy('id_tipo_permiso', 'DESC')->get();
     $tipoPermisoDeshabilitados = TipoPermiso::where('estado', '=', 0)->orderBy('id_tipo_permiso', 'DESC')->get();

    	return view('marcacion.gestionTipoPermiso.listar', compact('tipoPermiso', 'tipoPermisoDeshabilitados'));
    }


    public function crearTipoPermiso(){

    	return view('marcacion.gestionTipoPermiso.crear');

    }


    public function guardarTipoPermiso(Request $request){

    	$tipoPermiso = new TipoPermiso();
    	$tipoPermiso->codigo_permiso = $request->codigo;
    	$tipoPermiso->estado=1;
    	$tipoPermiso->descripcion = $request->descripcion;
    	$tipoPermiso->numero_horas = $request->numero_horas;
    	$tipoPermiso->save();

    	Alert::success('Registrar Tipo de Permiso', $tipoPermiso->descripcion. ' de forma existosa')->autoClose(2000);
    	return redirect()->action('Marcaciones\TipoPermisosController@listarTipoPermiso');
    }

    public function editarTipoPermiso($id){

    	$tipoPermiso = TipoPermiso::find($id);

    	return view('marcacion.gestionTipoPermiso.editar', compact('tipoPermiso'));
    	
    }

    public function actualizarTipoPermiso(Request $request, $id){

    	$tipoPermiso = TipoPermiso::find($id);
    	$tipoPermiso->codigo_permiso = $request->codigo;
    	$tipoPermiso->estado=1;
    	$tipoPermiso->descripcion = $request->descripcion;
    	$tipoPermiso->numero_horas = $request->numero_horas;
    	$tipoPermiso->update();

    Alert::warning('Actualizar Tipo de Permiso', $tipoPermiso->descripcion . ' de forma existosa')->autoClose(2000)->iconHtml('<i class="fa fa-pencil"></i>');
            return redirect()->action('Marcaciones\TipoPermisosController@listarTipoPermiso');
    }


    public function eliminarTipoPermiso($id){

    	$tipoPermiso = TipoPermiso::find($id);
		$tipoPermiso->estado=0;
		$tipoPermiso->update();
        Alert::error('Eliminar Permiso', $tipoPermiso->descripcion . ' de forma existosa')->autoClose(2000)->iconHtml('<i class="fa fa-trash-o"></i>');
        return redirect()->action('Marcaciones\TipoPermisosController@listarTipoPermiso');

    }


    public function habilitarTipoPermiso($id){
    	$tipoPermiso = TipoPermiso::find($id);
		$tipoPermiso->estado=1;
		$tipoPermiso->update();
		 Alert::success('Habilitar Tipo de Permiso', $tipoPermiso->descripcion . ' de forma existosa')->autoClose(2000);
		 return redirect()->action('Marcaciones\TipoPermisosController@listarTipoPermiso');

    }
}
