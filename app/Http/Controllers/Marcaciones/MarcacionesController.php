<?php

namespace App\Http\Controllers\Marcaciones;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Modelos\Marcacion\Marcacion;
use App\Modelos\Marcacion\Asueto;
use App\Modelos\Marcacion\Calendario;
use App\Modelos\Empleado\Empleado;
use RealRashid\SweetAlert\Facades\Alert;
use App\Modelos\Empleado\Oficina;
use App\Modelos\Empleado\UnidadAdministrativa;
use App\Modelos\Empleado\Centro;
use DatePeriod;
use DateInterval;
use DB;
use Carbon\Carbon; 

class MarcacionesController extends Controller
{
    //funcion para los permisos de marcaciones
    public function __construct(){

    $this->middleware(['permission:consultar.marcaciones'], ['only'    => ['buscarEmpleadoMarcacion', 'consultarEmpleadoMarcacion']]);
 
    }

    public function buscarEmpleadoMarcacion(){
      //busca la marcacion por empleado
    	$vacio = "";
      $empleado = Empleado::all();
    	return view('marcacion.gestionMarcacion.listar', compact( 'vacio','empleado'));
    }


     public function obtenerOficinas(Request $request)
      {
        //obtiene las oficinas 
        
          $oficinas = DB::table('emp_oficina')
          ->where('unidad_administrativa_id', $request->unidad_administrativa)
          ->where('centro_id',$request->centro)
          ->get();

          foreach ($oficinas as $oficina) {
            $oficinaArray[$oficina->id_oficina] = $oficina->oficina;
          }
          return response()->json($oficinaArray);
        
      }


    public function obtenerEmpleado(Request $request){
      //obtiene los empleados

        if ($request->ajax()) {
          $empleados = Empleado::where('oficina_id', $request->oficina)->get();
          foreach ($empleados as $empleado) {
            $empleadoArray[$empleado->id_empleado] = $empleado->nombre.' '.$empleado->apellido;
          }
          return response()->json($empleadoArray);
        }
    }

    public function consultarEmpleadoMarcacion(Request $request){
      //muestra los datos de la consulta
    	  Carbon::setLocale('es_SV');
        $fecha = Carbon::now(new \DateTimeZone('America/El_Salvador'));
        $hoy = $fecha->format('Y-m-d');
        $dias = $fecha->format('d'); 
        
        $empleado = Empleado::find($request->codigo_id);
        $fechaInicio = $request->fecha_inicio;
        $fechaFin = $request->fecha_fin;

        if($empleado==null){
        	flash('¡No se tiene dicha información en el sistema!')->error()->important();
              return redirect()->action('Marcaciones\MarcacionesController@buscarEmpleadoMarcacion');

        }else{

        $marcacionCalendario = DB::table('calendario_marcacion_vw')->select('fecha', DB::raw('MIN(hora) as entrada'),DB::raw('MAX(hora) as salida'), 'permisos', 'asueto', 'id_empleado')->whereBetween('fecha', [$fechaInicio,$fechaFin])->where('id_empleado', '=', $empleado->id_empleado)->groupBy('fecha', 'permisos','asueto', 'id_empleado')->get();



        foreach($marcacionCalendario as $marcacion){
          $fecha=$marcacion->fecha;
          $entrada=$marcacion->entrada;
          $salida=$marcacion->salida;
          $permisos=$marcacion->permisos;
          $asueto=$marcacion->asueto;
          $id_empleado=$marcacion->id_empleado;
          if($entrada ==null && $salida==null){
            $horaCorto="";
            $horaTrabajada="";

          }elseif(date('H:i:s',strtotime($entrada))<'07:30:00' && date('H:i:s',strtotime($salida))<='15:30:00'){
            $horaCorto=date('H:i:s',strtotime('07:30:00') - strtotime($marcacion->entrada));
            $horaTrabajada=date('H:i:s',strtotime($horaCorto) + strtotime('08:00:00'));
          
          }elseif (date('H:i:s',strtotime($entrada))>='07:30:00' && date('H:i:s',strtotime($salida))> '15:30:00'){
            $horaCorto=date('H:i:s',strtotime($marcacion->salida) - strtotime('15:30:00'));
            $horaTrabajada=date('H:i:s',strtotime($horaCorto) + strtotime('08:00:00'));
         
          }elseif (date('H:i:s',strtotime($entrada))<'07:30:00' && date('H:i:s',strtotime($salida))>'15:30:00') {
            $horaEntrada=date('H:i:s',strtotime('07:30:00') - strtotime($marcacion->entrada));
            $horaSalida=$horaTrabajada=date('H:i:s',strtotime($marcacion->salida) - strtotime('15:30:00'));
            $horaCorto=date('H:i:s',strtotime($horaEntrada) + strtotime($horaSalida));
            $horaTrabajada=date('H:i:s',strtotime($horaCorto) + strtotime('08:00:00'));
          
          }elseif (date('H:i:s',strtotime($entrada))>='07:30:00' && date('H:i:s',strtotime($salida))<='15:30:00'){
            $horaCorto="00:00:00";
            $horaEntrada=date('H:i:s',strtotime($marcacion->entrada) - strtotime('07:30:00'));
            $horaSalida=$horaTrabajada=date('H:i:s',strtotime('15:30:00') - strtotime($marcacion->salida));
            $minutos=date('H:i:s',strtotime($horaSalida) + strtotime($horaEntrada));
             $horaTrabajada=date('H:i:s',strtotime('08:00:00') - strtotime($minutos));
       

          }

          $marcacionEmpleado[]=[$fecha, $entrada, $salida,$permisos,$asueto,$id_empleado,$horaTrabajada, $horaCorto];
        }

        $vacio="Existe";

       $noMarcacion = $marcacionCalendario->isEmpty();
       if($noMarcacion==true){
       	flash('¡No hay marcaciones en ese rango de fechas!')->error()->important();
        return redirect()->action('Marcaciones\MarcacionesController@buscarEmpleadoMarcacion');
       }      
       return view('marcacion.gestionMarcacion.listar', compact('marcacionEmpleado', 'vacio', 'empleado', 'fechaInicio', 'fechaFin'));
      }

    }


}
