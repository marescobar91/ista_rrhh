<?php

namespace App\Http\Controllers\Marcaciones;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Modelos\Marcacion\HoraExtra;
use App\Modelos\Marcacion\MesHora;
use App\Modelos\Empleado\Empleado;
use App\Modelos\Empleado\Oficina;
use App\Modelos\Empleado\UnidadAdministrativa;
use App\Modelos\Empleado\Centro;
use Carbon\Carbon;
use DB;
use RealRashid\SweetAlert\Facades\Alert;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\HorasExtrasExport;
use Barryvdh\DomPDF\Facade as PDF;



class HorasExtrasController extends Controller
{
  //funcion de permisos para horas extras
 public function __construct(){

  $this->middleware(['permission:consultar.horas.extras'], ['only'    => ['listarHorasExtras',]]);
  $this->middleware(['permission:editar.horas.extras'], ['only'   => ['editarHorasExtras', 'actualizarHorasExtras']]);
  $this->middleware(['permission:eliminar.horas.extras'], ['only'   => ['eliminarHorasExtras']]);
  $this->middleware(['permission:crear.horas.extras'], ['only'   => ['buscarEmpleadoHorasExtras', 'crearHorasExtras', 'guardarHorasExtras']]);
  $this->middleware(['permission:reporte.horas.extras'], ['only'   => ['reporteHorasExtras', '', 'verReporteHorasExtras','excelReporteHorasExtras','imprimirReporteHorasExtras']]);
} 

public function listarHorasExtras(){
  //muestra el listado de horas extras
  $horaExtra= HoraExtra::all(); 
  return view('marcacion.gestionHorasExtras.listar', compact('horaExtra')); 
}

public function buscarEmpleadoHorasExtras(){
//funcion para buscar el empleado 
    $fecha = Carbon::now(new \DateTimeZone('America/El_Salvador'));
    $anio = $fecha->year;
 $empleado = Empleado::all();

 return view('marcacion.gestionHorasExtras.crear', compact('empleado','anio'));
}

public function getOficinas(Request $request)
{

  //funcion para obtener las oficinas dependientes de la unidad o de centro

  $oficinas = DB::table('emp_oficina')
  ->where('unidad_administrativa_id', $request->unidad_administrativa)
  ->where('centro_id',$request->centro)
  ->get();

  foreach ($oficinas as $oficina) {
    $oficinaArray[$oficina->id_oficina] = $oficina->oficina;
  }
  return response()->json($oficinaArray);

} 


public function getEmpleado(Request $request){
    	//funcion que se usa en la seccion de javascript para obtener el select dependiente de unidad, oficina y centro.
 if ($request->ajax()) {
  $empleados = Empleado::where('oficina_id', $request->oficina)->get();
  foreach ($empleados as $empleado) {
    $empleadoArray[$empleado->id_empleado] = $empleado->nombre.' '.$empleado->apellido;
  }
  return response()->json($empleadoArray);
}
}

public function crearHorasExtras(Request $request){
  //funcion para crear las horas extras
        $empleado = Empleado::where('id_empleado', $request->codigo_id)->get();
        
        foreach ($empleado as $empleado) {
            $A1['id']= $empleado->id_empleado;
            $A1['pin']= $empleado->pin_empleado;
            $A1['codigo_empleado']=$empleado->codigo_empleado;
            $A1['nombre']= $empleado->nombre.' '.$empleado->apellido;
            $A1['oficina']= $empleado->oficina->oficina;
            $A1['centro']= $empleado->oficina->centro->centro;
            $A1['unidad']= $empleado->oficina->unidadAdministrativa->unidad_administrativa;
        }
  
      return response()->json($A1); //redirecciona a la vista con los campos cargados.

     }

     public function guardarHorasExtras(Request $request){
      //funcion para guardar horas extras 
      Carbon::setLocale('es_SV'); //obtiene la localidad
      $fecha = Carbon::now(new \DateTimeZone('America/El_Salvador'));    //obtiene la fecha y hora actual
      $date = $fecha->format('Y-m-d');  //formatea la feche del tipo Y-m-d
      $horasExtras = new HoraExtra(); // crea un nuevo objeto de hora extra
      $horasExtras->empleado_id = $request->id_empleado; 
      $horasExtras->fecha_desde = $request->fecha_inicio;
      $horasExtras->fecha_hasta = $request->fecha_final;
      $horasExtras->horas = $request->hora;
      $horasExtras->minutos = $request->minutos;
      $horasExtras->fecha_registro = $fecha;
      $horasCalculadas = ($request->minutos/60)+$request->hora;
      $fechaI = Carbon::parse($request->fecha_inicio); //obtiene la fecha inicio de tipo carbon
      $fechaF = Carbon::parse($request->fecha_final);  //obtiene la fecha final de tipo carbon
      $mes = $request->mes;
      $empleado = Empleado::find($request->id_empleado);

      if($fechaI!=null && $fechaF!=null){

        if($fechaF < $fechaI){

          flash("¡ERROR! ¡La fecha final no puede ser menor que la fecha de inicio!")->error()->important();
          return redirect()->action('Marcaciones\HorasExtrasController@buscarEmpleadoHorasExtras');
        }
        if($fechaI > $date){

         flash("¡ERROR! ¡La fecha inicio no puede ser mayor que la fecha de actual!")->error()->important();
         return redirect()->action('Marcaciones\HorasExtrasController@buscarEmpleadoHorasExtras');
       }
       if($fechaF > $date){

         flash("¡ERROR! ¡La fecha final no puede ser mayor que la fecha actual!")->error()->important();
         return redirect()->action('Marcaciones\HorasExtrasController@buscarEmpleadoHorasExtras');
       }
     }

     $horasAnterior = HoraExtra::where('empleado_id',$request->id_empleado)->orderBy('id_hora_extra','desc')->first();
     //dd($request->fecha_inicio);
  
     if($request->fecha_inicio == $horasAnterior->fecha_desde && $request->fecha_final == $horasAnterior->fecha_hasta && $request->id_empleado == $horasAnterior->empleado_id){
        flash("¡ERROR! ¡Ya se tiene ingresado horas extras de ". $empleado->nombre.' '.$empleado->apellido. " en este periodo de tiempo!")->error()->important();
         return redirect()->action('Marcaciones\HorasExtrasController@buscarEmpleadoHorasExtras');
     }


     $horasExtras->mes = $mes;  //variable para mes
     $mesHoras = MesHora::all();
     foreach ($mesHoras as $mesH) {
       if($mesH->meses == $mes){
        $horaMes=$mesH->horas;
      }
    }

     //proceso para obtener el valor de devengo
   
    $salario = $empleado->salario_actual;
    $devengo = round((($salario/$horaMes)*$horasCalculadas),4);
    $horasExtras->devengo = $devengo;

     //proceso para obtener el valor el isss
    if($empleado->prevision != null){
      if($salario>=1000){
        $isss=0;
        $horasExtras->isss=$isss;
      }
      elseif(($salario + $devengo)>=1000){
        $isss=round((1000-$salario)*0.075,4);
        $horasExtras->isss = $isss;
      }
      else{
        $isss = round($devengo*0.075,4);
        $horasExtras->isss = $isss;
      }

    }
    else{
      $isss=0;
      $horasExtras->isss=$isss;
    }
     //proceso para obtener el valor de insaforp
    if($empleado->prevision != null){
      if($salario>=1000){
        $insaforp= 0;
        $horasExtras->insaforp=$insaforp;
      }
      elseif(($salario + $devengo)>=1000){
        $insaforp=round((1000-$salario)*0.01,4);
        $horasExtras->insaforp = $insaforp;
      }
      else{
        $insaforp = round($devengo*0.01,4);
        $horasExtras->insaforp = $insaforp;
      }

    }
    else{
      $insaforp= 0;
      $horasExtras->insaforp=$insaforp;
    }
     //proceso para obtener el valor de inpep e ipsfa
    if($devengo != null){
      if($empleado->prevision == 'INPEP'){
        $inpep= round($devengo*0.07,4);  //se utiliza la misma variable de inpep para ambos valores inpep e ipsfa
        $horasExtras->inpep = $inpep;
      }
      elseif($empleado->prevision == 'IPSFA'){
        $inpep= round($devengo*0.06,4);
        $horasExtras->inpep = $inpep;
      }
      else{
        $inpep=0;
        $horasExtras->inpep=$inpep;
      }

    }
    else{
      $inpep=0;
      $horasExtras->inpep=$inpep;
    }
     //proceso para obtener el valor de AFP
    if($devengo != null){
      if($empleado->prevision == 'AFP CONFIA' || $empleado->prevision == 'AFP CRECER'){
        $afp= round($devengo*0.0675,4);
        $horasExtras->afp = $afp;
      }
      else{
        $afp = 0;
        $horasExtras->afp=$afp;
      }

    }
    else{
      $afp = 0;
      $horasExtras->afp=$afp;
    }

    $total = round($devengo + $isss + $insaforp + $inpep + $afp,4); // obtiene el valor total para descontar de las horas extras.
    $horasExtras->total = $total;
    $horasExtras->save();

    Alert::success('Registrar Horas Extras de Empleado', $empleado->nombre.' '.$empleado->apellido  . ' de forma existosa')->autoClose(1200);
    return redirect()->action('Marcaciones\HorasExtrasController@buscarEmpleadoHorasExtras');

  }

  public function editarHorasExtras($id){
    //carga la vista de edicion de horas extras
   $horasExtra = HoraExtra::find($id);
   $empleado_id = $horasExtra->empleado_id;
   $empleado = Empleado::find($empleado_id);
   $fecha = Carbon::now(new \DateTimeZone('America/El_Salvador'));
   $mesExtra=$horasExtra->mes;
    if($horasExtra->mes == 1)
      $mes = "Enero";
      elseif($horasExtra->mes == 2)
        $mes = "Febrero";
      elseif($horasExtra->mes == 3)
        $mes = "Marzo";
      elseif($horasExtra->mes == 4)
        $mes = "Abril";
      elseif($horasExtra->mes == 5)
        $mes = "Mayo";
      elseif($horasExtra->mes == 6)
        $mes = "Junio";
      elseif($horasExtra->mes == 7)
        $mes = "Julio";
      elseif($horasExtra->mes == 8)
        $mes = "Agosto";
      elseif($horasExtra->mes == 9)
        $mes = "Septiembre";
      elseif($horasExtra->mes == 10)
        $mes = "Octubre";
      elseif($horasExtra->mes == 11)
        $mes = "Noviembre";
      elseif($horasExtra->mes == 12)
        $mes = "Diciembre";

      $anio = Carbon::parse($horasExtra->fecha_desde)->format('Y');
   return view('marcacion.gestionHorasExtras.editar', compact('horasExtra', 'empleado','mes' , 'anio', 'mesExtra'));

 }

 public function actualizarHorasExtras(Request $request, $id){
  //actualiza los datos de las horas extra
   Carbon::setLocale('es_SV');
   $fecha = Carbon::now(new \DateTimeZone('America/El_Salvador'));
   $date = $fecha->format('Y-m-d');
   $horasExtras = HoraExtra::find($id);
   $horasExtras->fecha_desde = $request->fecha_inicio;
   $horasExtras->fecha_hasta = $request->fecha_final;
   $horasExtras->horas = $request->hora;
   $horasExtras->minutos = $request->minutos;
   $horasExtras->fecha_registro = $fecha;
   $horasCalculadas = ($request->minutos/60)+$request->hora;
   $fechaI = Carbon::parse($request->fecha_inicio);
   $fechaF = Carbon::parse($request->fecha_final);
   $mes = $request->mesExtra;


   if($fechaI!=null && $fechaF!=null){

    if($fechaF < $fechaI){

      flash("¡ERROR! ¡La fecha final no puede ser menor que la fecha de inicio!")->error()->important();
      return redirect()->action('Marcaciones\HorasExtrasController@buscarEmpleadoHorasExtras');
    }
    if($fechaI > $date){

     flash("¡ERROR! ¡La fecha inicio no puede ser mayor que la fecha de actual!")->error()->important();
     return redirect()->action('Marcaciones\HorasExtrasController@buscarEmpleadoHorasExtras');
   }
   if($fechaF > $date){

     flash("¡ERROR! ¡La fecha final no puede ser mayor que la fecha actual!")->error()->important();
     return redirect()->action('Marcaciones\HorasExtrasController@buscarEmpleadoHorasExtras');
   }
 }
     $horasExtras->mes = $mes;  //variable para mes
     $mesHoras = MesHora::all();

     foreach ($mesHoras as $mesH) {
       if($mesH->meses == $mes){
        $horaMes=$mesH->horas;
      }
    }

   
     //devengo
    $empleado_id = $horasExtras->empleado_id;
    $empleado = Empleado::find($empleado_id);
    $salario = $empleado->salario_actual;
    $devengo = round((($salario/$horaMes)*$horasCalculadas),4);
    $horasExtras->devengo = $devengo;
     //isss
    if($empleado->prevision != null){
      if($salario>=1000){
        $isss=0;
        $horasExtras->isss=$isss;
      }
      elseif(($salario + $devengo)>=1000){
        $isss=round((1000-$salario)*0.075,4);
        $horasExtras->isss = $isss;
      }
      else{
        $isss = round($devengo*0.075,4);
        $horasExtras->isss = $isss;
      }

    }
    else{
      $isss=0;
      $horasExtras->isss=$isss;
    }
     //insaforp
    if($empleado->prevision != null){
      if($salario>=1000){
        $insaforp= 0;
        $horasExtras->insaforp=$insaforp;
      }
      elseif(($salario + $devengo)>=1000){
        $insaforp=round((1000-$salario)*0.01,4);
        $horasExtras->insaforp = $insaforp;
      }
      else{
        $insaforp = round($devengo*0.01,4);
        $horasExtras->insaforp = $insaforp;
      }

    }
    else{
      $insaforp= 0;
      $horasExtras->insaforp=$insaforp;
    }
     //inpep e ipsfa
    if($devengo != null){
      if($empleado->prevision == 'INPEP'){
        $inpep= round($devengo*0.07,4);
        $horasExtras->inpep = $inpep;
      }
      elseif($empleado->prevision == 'IPSFA'){
        $inpep= round($devengo*0.06,4);
        $horasExtras->inpep = $inpep;
      }
      else{
        $inpep=0;
        $horasExtras->inpep=$inpep;
      }

    }
    else{
      $inpep=0;
      $horasExtras->inpep=$inpep;
    }
     //AFP
    if($devengo != null){
      if($empleado->prevision == 'AFP CONFIA' || $empleado->prevision == 'AFP CRECER'){
        $afp= round($devengo*0.0675,4);
        $horasExtras->afp = $afp;
      }
      else{
        $afp = 0;
        $horasExtras->afp=$afp;
      }

    }
    else{
      $afp = 0;
      $horasExtras->afp=$afp;
    }

    $total = round($devengo + $isss + $insaforp + $inpep + $afp,4);
    $horasExtras->total = $total;

    $horasExtras->update();

    Alert::warning('Registro de Horas Extras Editado del Empleado', $empleado->nombre.' '.$empleado->apellido  . ' de forma existosa')->autoClose(2000)->iconHtml('<i class="fa fa-pencil"></i>');
    return redirect()->action('Marcaciones\HorasExtrasController@listarHorasExtras');
  }

  public function eliminarHorasExtras($id){
    //elimina las horas extras
   $horaExtra = HoraExtra::find($id);
   $horaExtra->delete();
   $empleado_id = $horaExtra->empleado_id;
   $empleado = Empleado::find($empleado_id);
   Alert::error('Registro de Horas Extras Eliminado', $empleado->nombre.' '.$empleado->apellido  . ' de forma existosa')->autoClose(2000)->iconHtml('<i class="fa fa-trash-o"></i>');
   return redirect()->action('Marcaciones\HorasExtrasController@listarHorasExtras');

 }

 public function reporteHorasExtras(){
  //buscador para el reporte de horas extras
  $vacio = '';
  $hoy = Carbon::now(new \DateTimeZone('America/El_Salvador'));
  $mesHoy = $hoy->monthName;      //nombre del mes actual
  $anioHoy = $hoy->year;          //numero del año actual
  return view('marcacion.gestionHorasExtras.reporte', compact('mesHoy','anioHoy','vacio'));
}

  public function verReporteHorasExtras(Request $request){
    //muestra el contenido del reporte
      $vacio= "Existe";
      $hoy = Carbon::now(new \DateTimeZone('America/El_Salvador'));
      $hora= $hoy->format('h:i:s A'); 
      $dia = $hoy->format('d-m-Y');
      $mesHoy = $hoy->monthName;    //mes del año actual
      $mes = $request->mes;
      $anioHoy = $request->anioHoy;
      $fechaFin = Carbon::parse($request->fecha_final); //convierte la fecha final
      $mesFin = $fechaFin->monthName; //obtengo el mes de la fecha fin

      $fechaIni = Carbon::parse($request->fecha_inicio); //convierte la fecha inicial
      $mesIni = $fechaIni->monthName;

      $mesI = $fechaIni->month;     //nombre del mes de inicio
      $mesF= $fechaFin->month;      //nombre del mes final
      $fechaF = $request->fecha_final;
      $contrato = 'Contrato';  
      $ley = "Ley de Salarios";

      if($mes == null && $anioHoy != null && $fechaIni == null && $fechaFin == null){
        flash('¡No ha seleccionado ninguna opcion!')->error()->important();
        return redirect()->action('Marcaciones\HorasExtrasController@reporteHorasExtras');
      }

      if($mes != null && $anioHoy != null){
        $horasExtrasC = DB::table('generar_horas_extras_vw')->where('mes', $mes)->whereBetween('fecha_registro',[$fechaIni, $fechaFin])->where('tipo_plaza',$contrato)->orderBy('unidad_administrativa','ASC')->get();
        $devengoC = DB::table('generar_horas_extras_vw')->select( DB::raw('ROUND(SUM(devengo),2) as Devengo'))->where('mes', $mes)->whereBetween('fecha_registro',[$fechaIni, $fechaFin])->where('tipo_plaza',$contrato)->first();
        $totalC = DB::table('generar_horas_extras_vw')->select( DB::raw('ROUND(SUM(total),2) as Total'))->where('mes', $mes)->whereBetween('fecha_registro',[$fechaIni, $fechaFin])->where('tipo_plaza',$contrato)->first();

        $horasExtrasL = DB::table('generar_horas_extras_vw')->where('mes', $mes)->whereBetween('fecha_registro',[$fechaIni, $fechaFin])->where('tipo_plaza',$ley)->orderBy('unidad_administrativa','ASC')->get();
        $devengoL = DB::table('generar_horas_extras_vw')->select( DB::raw('ROUND(SUM(devengo),2) as Devengo'))->where('mes', $mes)->whereBetween('fecha_registro',[$fechaIni, $fechaFin])->where('tipo_plaza',$ley)->first();
        $totalL = DB::table('generar_horas_extras_vw')->select( DB::raw('ROUND(SUM(total),2) as Total'))->where('mes', $mes)->whereBetween('fecha_registro',[$fechaIni, $fechaFin])->where('tipo_plaza',$ley)->first();
      }
      elseif($fechaIni != null && $fechaFin != null && $anioHoy != null){
        $horasExtrasC = DB::table('generar_horas_extras_vw')->where('mes', $mes)->whereBetween('fecha_registro',[$fechaIni, $fechaFin])->where('tipo_plaza',$contrato)->orderBy('unidad_administrativa','ASC')->get();
        $devengoC = DB::table('generar_horas_extras_vw')->select( DB::raw('ROUND(SUM(devengo),2) as Devengo'))->where('mes', $mes)->whereBetween('fecha_registro',[$fechaIni, $fechaFin])->where('tipo_plaza',$contrato)->first();
        $totalC = DB::table('generar_horas_extras_vw')->select( DB::raw('ROUND(SUM(total),2) as Total'))->where('mes', $mes)->whereBetween('fecha_registro',[$fechaIni, $fechaFin])->where('tipo_plaza',$contrato)->first();

        $horasExtrasL = DB::table('generar_horas_extras_vw')->where('mes', $mes)->whereBetween('fecha_registro',[$fechaIni, $fechaFin])->where('tipo_plaza',$ley)->orderBy('unidad_administrativa','ASC')->get();
        $devengoL = DB::table('generar_horas_extras_vw')->select( DB::raw('ROUND(SUM(devengo),2) as Devengo'))->where('mes', $mes)->whereBetween('fecha_registro',[$fechaIni, $fechaFin])->where('tipo_plaza',$ley)->first();
        $totalL = DB::table('generar_horas_extras_vw')->select( DB::raw('ROUND(SUM(total),2) as Total'))->where('mes', $mes)->whereBetween('fecha_registro',[$fechaIni, $fechaFin])->where('tipo_plaza',$ley)->first();
      }
      if($mes != null && $anioHoy != null && $fechaIni != null && $fechaFin != null){
       $horasExtrasC = DB::table('generar_horas_extras_vw')->where('mes', $mes)->whereBetween('fecha_registro',[$fechaIni, $fechaFin])->where('tipo_plaza',$contrato)->orderBy('unidad_administrativa','ASC')->get();
       $devengoC = DB::table('generar_horas_extras_vw')->select( DB::raw('ROUND(SUM(devengo),2) as Devengo'))->where('mes', $mes)->whereBetween('fecha_registro',[$fechaIni, $fechaFin])->where('tipo_plaza',$contrato)->first();
       $totalC = DB::table('generar_horas_extras_vw')->select( DB::raw('ROUND(SUM(total),2) as Total'))->where('mes', $mes)->whereBetween('fecha_registro',[$fechaIni, $fechaFin])->where('tipo_plaza',$contrato)->first();

       $horasExtrasL = DB::table('generar_horas_extras_vw')->where('mes', $mes)->whereBetween('fecha_registro',[$fechaIni, $fechaFin])->where('tipo_plaza',$ley)->orderBy('unidad_administrativa','ASC')->get();
       $devengoL = DB::table('generar_horas_extras_vw')->select( DB::raw('ROUND(SUM(devengo),2) as Devengo'))->where('mes', $mes)->whereBetween('fecha_registro',[$fechaIni, $fechaFin])->where('tipo_plaza',$ley)->first();
       $totalL = DB::table('generar_horas_extras_vw')->select( DB::raw('ROUND(SUM(total),2) as Total'))->where('mes', $mes)->whereBetween('fecha_registro',[$fechaIni, $fechaFin])->where('tipo_plaza',$ley)->first();
     }


     return view('marcacion.gestionHorasExtras.reporte', compact('mesHoy','anioHoy','horasExtrasC','fechaIni','fechaFin','dia','hora','mesFin','mesIni','totalC','devengoC','horasExtrasL','devengoL','totalL', 'vacio', 'mes'));

   }

   public function excelReporteHorasExtras(Request $request){

    //funcion para el archivo excel de horas extras
     $fechaInicio = Carbon::parse($request->fecha_inicio);
     $fechaFinal = Carbon::parse($request->fecha_final);
     $fechaIni =$fechaInicio->format('Y-m-d');
     $fechaFin =$fechaFinal->format('Y-m-d');
     $now = Carbon::now(new \DateTimeZone('America/El_Salvador'));
     $mes =$request->mesExtra;
 
     //exporta los datos al Export para el excel
     return Excel::download(new HorasExtrasExport($mes,$fechaIni,$fechaFin), 'horasextras'. $now .'.xlsx');
   }

   public function imprimirReporteHorasExtras(Request $request){
    //funcion PDF 
    $hoy = Carbon::now(new \DateTimeZone('America/El_Salvador'));
    $hora= $hoy->format('h:i:s A'); 
    $dia = $hoy->format('d-m-Y');
    $mesHoy = $hoy->monthName;
    $anioHoy = $hoy->year;
    $fechaInicio = Carbon::parse($request->fecha_inicio); //convierte la fecha inicial
    $mesIni = $fechaInicio->monthName;
    $fechaFin = Carbon::parse($request->fecha_final);
    $mesFin = $fechaFin->monthName;
    $fechaIni=$fechaInicio->format('Y-m-d');
    $fechaFinal =$fechaFin->format('Y-m-d');
    $mes = $request->mesExtra;
    $contrato = 'Contrato';
    $ley = "Ley de Salarios";
    $horasExtrasC = DB::table('generar_horas_extras_vw')->where('mes', $mes)->whereBetween('fecha_registro',[$fechaIni, $fechaFinal])->where('tipo_plaza',$contrato)->orderBy('unidad_administrativa','ASC')->get();

    $devengoC = DB::table('generar_horas_extras_vw')->select( DB::raw('ROUND(SUM(devengo),2) as Devengo'))->where('mes', $mes)->whereBetween('fecha_registro',[$fechaIni, $fechaFinal])->where('tipo_plaza',$contrato)->first();
    $totalC = DB::table('generar_horas_extras_vw')->select( DB::raw('ROUND(SUM(total),2) as Total'))->where('mes', $mes)->whereBetween('fecha_registro',[$fechaIni, $fechaFinal])->where('tipo_plaza',$contrato)->first();

    $horasExtrasL = DB::table('generar_horas_extras_vw')->where('mes', $mes)->whereBetween('fecha_registro',[$fechaIni, $fechaFinal])->where('tipo_plaza',$ley)->orderBy('unidad_administrativa','ASC')->get();
    $devengoL = DB::table('generar_horas_extras_vw')->select( DB::raw('ROUND(SUM(devengo),2) as Devengo'))->where('mes', $mes)->whereBetween('fecha_registro',[$fechaIni, $fechaFinal])->where('tipo_plaza',$ley)->first();
    $totalL = DB::table('generar_horas_extras_vw')->select( DB::raw('ROUND(SUM(total),2) as Total'))->where('mes', $mes)->whereBetween('fecha_registro',[$fechaIni, $fechaFinal])->where('tipo_plaza',$ley)->first();

        $pdf = \PDF::loadView('marcacion.gestionHorasExtras.imprimirReporte', ['horasExtrasC' => $horasExtrasC , 'horasExtrasL' => $horasExtrasL], compact('mesHoy','anioHoy','fechaIni','fechaFinal','dia','hora','mesFin','mesIni','devengoC','totalC','devengoL','totalL'))->setPaper('A4', 'landscape')->setWarnings(false);
        return $pdf->stream('HorasExtras.pdf');

      }


    }
