<?php

namespace App\Http\Controllers\Marcaciones;

use App\Http\Controllers\Controller;
use App\Modelos\Marcacion\Asueto;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Laracasts\Flash\Flash;
use App\Modelos\Marcacion\Calendario;
use DB;
use RealRashid\SweetAlert\Facades\Alert;
use App\Http\Requests\AsuetoRequest;
use App\Http\Requests\EditarAsuetoRequest;

class AsuetosController extends Controller
{
    //funcion para los permisos de asuetos
    public function __construct(){

    $this->middleware(['permission:crear.asuetos'], ['only'    => ['crearAsuetos', 'guardarAsuetos']]);
    $this->middleware(['permission:editar.asuetos'], ['only'   => ['editarAsuetos', 'actualizarAsuetos']]);
    $this->middleware(['permission:consultar.asuetos'], ['only'   => ['listarAsuetos']]);
    $this->middleware(['permission:eliminar.asuetos'], ['only'   => ['eliminarAsuetos']]);
    $this->middleware(['permission:cargar.asuetos'], ['only'   => ['cargarAsuetos']]);
    }


    public function crearAsuetos(Request $request){
        //carga la vista de crear asueto
    	return view('marcacion.gestionAsuetos.crear'); //llama a la vista de crear asueto
    }
    public function guardarAsuetos(AsuetoRequest $request){
        //guarda los datos de los asuetos
    	$asueto = new Asueto();
    	$asueto->fecha = Carbon::parse($request->fecha);
        $calendario = Calendario::where('fecha', '=', $request->fecha)->first();
        $calendario_id = $calendario->id_calendario;
    	$asueto->asueto = $request->asueto;
    	$fecha = Carbon::parse($request->fecha);
        //obtenemos el dia 
    	$dia = $fecha->day;
        if($fecha->day == "1" || $fecha->day == "2" || $fecha->day == "3" || $fecha->day == "4" || $fecha->day == "5" || $fecha->day == "6" || $fecha->day == "7" || $fecha->day == "8" || $fecha->day == "9"){
            $asueto->dia = "0".$dia;  //agrega el 0 al numero de dia
        }
        else{
            $asueto->dia = $dia;  
        }
        //obtenemos el mes
    	$mes = $fecha->month;
        if($fecha->month == "10" || $fecha->month == "11" || $fecha->month == "12"){
            //guarda la fecha con formato de dd-mm-aa
            $anio = $fecha->year;
            $asueto->mes = $mes;
            $asueto->anio = substr($anio, -2);
            $formato_fecha = $asueto->dia.$asueto->mes.$asueto->anio;
            $asueto->formato_fecha = $formato_fecha;
            $asueto->calendario_id = $calendario_id;

            $asueto->save();
            Alert::success('Registrar Asueto', $asueto->asueto . ' de forma existosa')->autoClose(2000);
            return redirect()->action('Marcaciones\AsuetosController@listarAsuetos');
        }
        else{
            //guarda la fecha con formato de dd-mm-aa
            $anio = $fecha->year;
            $asueto->mes = "0".$mes;    //agrega el 0 al numero de mes
            $asueto->anio = substr($anio, -2);
            $formato_fecha = $asueto->dia.$asueto->mes.$asueto->anio;
            $asueto->formato_fecha = $formato_fecha;
            $asueto->calendario_id = $calendario_id;
            $asueto->save();
            Alert::success('Registrar Asueto', $asueto->asueto . ' de forma existosa')->autoClose(2000);
            return redirect()->action('Marcaciones\AsuetosController@listarAsuetos');
        }
    	
    }
    public function listarAsuetos(){
    	//muestra el listad de los asuetos
    	$asueto = DB::table('mar_asueto')->orderBy('anio','DESC')->get();
    
    	
    	return view('marcacion.gestionAsuetos.listar' , compact('asueto')); 	
    }

    public function editarAsuetos($id){
        //carga la vista para editar el asueto
        $asueto = Asueto::find($id);
        return view('marcacion.gestionAsuetos.editar', compact('asueto'));
    }

    public function actualizarAsuetos(EditarAsuetoRequest $request, $id){ 
        //actualizar el asueto seleccionado 
        $asueto = Asueto::find($id);
        $asueto->fecha = $request->fecha;
        $calendario = Calendario::where('fecha', '=', $request->fecha)->first();
        $calendario_id = $calendario->id_calendario;
        $asueto->asueto = $request->asueto;
        $fecha = Carbon::parse($request->fecha);
        $dia = $fecha->day;
        if($fecha->day == "1" || $fecha->day == "2" || $fecha->day == "3" || $fecha->day == "4" || $fecha->day == "5" || $fecha->day == "6" || $fecha->day == "7" || $fecha->day == "8" || $fecha->day == "9"){
            $asueto->dia = "0".$dia;
        }
        else{
            $asueto->dia = $dia;
        }
        $mes = $fecha->month;
        if($fecha->month == "10" || $fecha->month == "11" || $fecha->month == "12"){
            $anio = $fecha->year;
            $asueto->mes = $mes;
            $asueto->anio = substr($anio, -2);
            $formato_fecha = $asueto->dia.$asueto->mes.$asueto->anio;
            $asueto->formato_fecha = $formato_fecha;
             $asueto->calendario_id = $calendario_id;
            $asueto->save();
            Alert::warning('Actualizar Asueto', $asueto->asueto . ' de forma existosa')->autoClose(2000)->iconHtml('<i class="fa fa-pencil"></i>');
            return redirect()->action('Marcaciones\AsuetosController@listarAsuetos');
        }
        else{
            
            $anio = $fecha->year;
            $asueto->mes = "0".$mes;
            $asueto->anio = substr($anio, -2);
            $formato_fecha = $asueto->dia.$asueto->mes.$asueto->anio;
            $asueto->formato_fecha = $formato_fecha;
             $asueto->calendario_id = $calendario_id;
            $asueto->save();
            Alert::warning('Actualizar Asueto', $asueto->asueto . ' de forma existosa')->autoClose(2000)->iconHtml('<i class="fa fa-pencil"></i>');
            return redirect()->action('Marcaciones\AsuetosController@listarAsuetos');
        }
    }

    public function eliminarAsuetos($id){
        $asueto = Asueto::find($id);
        $asueto->delete();
        Alert::error('Eliminar Asuetos', $asueto->asueto . ' de forma existosa')->autoClose(2000)->iconHtml('<i class="fa fa-trash-o"></i>');
        return redirect()->action('Marcaciones\AsuetosController@listarAsuetos');
    }

    public function cargarAsuetos(Request $request){
        //carga los asuetos del año
    	$fechaHoy = Carbon::now(new \DateTimeZone('America/El_Salvador'));
        $year = $fechaHoy->year;
        $anio = substr($year, -2);
        $fechaEnero = $year."-01-01";   //obtener 1 de enero
        $asuetos = Asueto::all();
        if(!$asuetos->isEmpty()){
            foreach($asuetos as $asueto){
                if(Asueto::where('fecha', '=', $fechaEnero)->exists()){
                    //valida si los asuetos ya fueron agregados
                    Alert::error('Error','Los Asuetos ya fueron agregados')->autoClose(2000);
                    return redirect()->action('Marcaciones\AsuetosController@listarAsuetos');
                }
                else{
                    //por cada fecha se llama a un procedimiento para agregarlos a la tabla de asuetos
                    $calendario = $calendario = Calendario::where('fecha', '=', $year."-01-01")->first();
                    $calendario_id = $calendario->id_calendario;
                    $unoEnero = [$year."-01-01","Uno de Enero","01","01","$anio","0101$anio",$calendario_id ];
                    DB::select('CALL generarAsueto_sp(?,?,?,?,?,?,?)',$unoEnero); 
                    $calendario = $calendario = Calendario::where('fecha', '=', $year."-05-01")->first();
                    $calendario_id = $calendario->id_calendario;
                    $diaTrabajo = [$year."-05-01","Dia del Trabajo","01","05","$anio","0105$anio", $calendario_id];
                    DB::select('CALL generarAsueto_sp(?,?,?,?,?,?,?)',$diaTrabajo);
                     $calendario = $calendario = Calendario::where('fecha', '=', $year."-05-10")->first();
                    $calendario_id = $calendario->id_calendario;
                    $diaMadre = [$year."-05-10","Dia de la Madre","10","05","$anio","1005$anio",$calendario_id];
                    DB::select('CALL generarAsueto_sp(?,?,?,?,?,?,?)',$diaMadre); 
                    $calendario = $calendario = Calendario::where('fecha', '=', $year."-05-27")->first();
                    $calendario_id = $calendario->id_calendario;
                    $diaAniversario = [$year."-05-27","Dia de Aniversario de constitucion del Sindicato","27","05","$anio","2705$anio", $calendario_id];
                    DB::select('CALL generarAsueto_sp(?,?,?,?,?,?,?)',$diaAniversario);
                    $calendario = $calendario = Calendario::where('fecha', '=', $year."-06-17")->first();
                    $calendario_id = $calendario->id_calendario;
                    $diaPadre = [$year."-06-17","Dia del Padre","17","06","$anio","1706$anio",$calendario_id];
                    DB::select('CALL generarAsueto_sp(?,?,?,?,?,?,?)',$diaPadre);
                    $calendario = $calendario = Calendario::where('fecha', '=', $year."-06-22")->first();
                    $calendario_id = $calendario->id_calendario; 
                    $diaMaestro = [$year."-06-22","Dia del Maestro","22","06","$anio","2206$anio", $calendario_id];
                    DB::select('CALL generarAsueto_sp(?,?,?,?,?,?,?)',$diaMaestro);
                    $calendario = $calendario = Calendario::where('fecha', '=', $year."-08-03")->first();
                    $calendario_id = $calendario->id_calendario;  
                    $agosto3 = [$year."-08-03","Fiestas Agosto","03","08","$anio","0308$anio", $calendario_id];
                    DB::select('CALL generarAsueto_sp(?,?,?,?,?,?,?)',$agosto3);
                    $calendario = $calendario = Calendario::where('fecha', '=', $year."-08-04")->first();
                    $calendario_id = $calendario->id_calendario;  
                    $agosto4 = [$year."-08-04","Fiestas Agosto","04","08","$anio","0408$anio", $calendario_id];
                    DB::select('CALL generarAsueto_sp(?,?,?,?,?,?,?)',$agosto4);
                     $calendario = $calendario = Calendario::where('fecha', '=', $year."-08-05")->first();
                    $calendario_id = $calendario->id_calendario;  
                    $agosto5 = [$year."-08-05","Fiestas Agosto","05","08","$anio","0508$anio", $calendario_id];
                    DB::select('CALL generarAsueto_sp(?,?,?,?,?,?,?)',$agosto5);
                    $calendario = $calendario = Calendario::where('fecha', '=', $year."-08-06")->first();
                    $calendario_id = $calendario->id_calendario;
                    $agosto6 = [$year."-08-06","Fiestas Agosto","06","08","$anio","0608$anio", $calendario_id];

                    DB::select('CALL generarAsueto_sp(?,?,?,?,?,?,?)',$agosto6); 
                    $calendario = $calendario = Calendario::where('fecha', '=', $year."-09-15")->first();
                    $calendario_id = $calendario->id_calendario;
                    $diaIndependencia = [$year."-09-15","Dia de la Independencia","15","09","$anio","1509$anio",$calendario_id];
                    DB::select('CALL generarAsueto_sp(?,?,?,?,?,?,?)',$diaIndependencia);
                    $calendario = $calendario = Calendario::where('fecha', '=', $year."-10-12")->first();
                    $calendario_id = $calendario->id_calendario; 
                    $diaRaza = [$year."-10-12","Dia de la Raza","12","10","$anio","1210$anio", $calendario_id];
                    DB::select('CALL generarAsueto_sp(?,?,?,?,?,?,?)',$diaRaza);
                    $calendario = $calendario = Calendario::where('fecha', '=', $year."-11-02")->first();
                    $calendario_id = $calendario->id_calendario; 
                    $diaDifuntos = [$year."-11-02","Dia de los Difuntos","02","11","$anio","0211$anio", $calendario_id]; 
                    DB::select('CALL generarAsueto_sp(?,?,?,?,?,?,?)',$diaDifuntos);
                    $calendario = $calendario = Calendario::where('fecha', '=', $year."-11-05")->first();
                    $calendario_id = $calendario->id_calendario; 
                    $grito = [$year."-11-05","Aniversario del primer grito de Independencia","05","11","$anio","0511$anio",$calendario_id]; 
                    DB::select('CALL generarAsueto_sp(?,?,?,?,?,?,?)',$grito);
                      $calendario = $calendario = Calendario::where('fecha', '=', $year."-12-25")->first();
                    $calendario_id = $calendario->id_calendario;  
                    $navidad = [$year."-12-25","Navidad","25","12","$anio","2512$anio", $calendario_id];
                    DB::select('CALL generarAsueto_sp(?,?,?,?,?,?,?)',$navidad); 
                    Alert::success('Asuetos del año '. $year.' ya fueron agregados')->autoClose(2000);
                    return redirect()->action('Marcaciones\AsuetosController@listarAsuetos');
                }
            }
        }
        else{
            if(Asueto::where('fecha', '=', $fechaEnero)->exists()){
                    Alert::error('Error','Los Asuetos ya fueron agregados')->autoClose(2000);
                    return redirect()->action('Marcaciones\AsuetosController@listarAsuetos');
                }
                else{
                      $calendario = $calendario = Calendario::where('fecha', '=', $year."-01-01")->first();
                    $calendario_id = $calendario->id_calendario;
                    $unoEnero = [$year."-01-01","Uno de Enero","01","01","$anio","0101$anio",$calendario_id ];
                    DB::select('CALL generarAsueto_sp(?,?,?,?,?,?,?)',$unoEnero); 
                    $calendario = $calendario = Calendario::where('fecha', '=', $year."-05-01")->first();
                    $calendario_id = $calendario->id_calendario;
                    $diaTrabajo = [$year."-05-01","Dia del Trabajo","01","05","$anio","0105$anio", $calendario_id];
                    DB::select('CALL generarAsueto_sp(?,?,?,?,?,?,?)',$diaTrabajo);
                     $calendario = $calendario = Calendario::where('fecha', '=', $year."-05-10")->first();
                    $calendario_id = $calendario->id_calendario;
                    $diaMadre = [$year."-05-10","Dia de la Madre","10","05","$anio","1005$anio",$calendario_id];
                    DB::select('CALL generarAsueto_sp(?,?,?,?,?,?,?)',$diaMadre); 
                    $calendario = $calendario = Calendario::where('fecha', '=', $year."-05-27")->first();
                    $calendario_id = $calendario->id_calendario;
                    $diaAniversario = [$year."-05-27","Dia de Aniversario de constitucion del Sindicato","27","05","$anio","2705$anio", $calendario_id];
                    DB::select('CALL generarAsueto_sp(?,?,?,?,?,?,?)',$diaAniversario);
                    $calendario = $calendario = Calendario::where('fecha', '=', $year."-06-17")->first();
                    $calendario_id = $calendario->id_calendario;
                    $diaPadre = [$year."-06-17","Dia del Padre","17","06","$anio","1706$anio",$calendario_id];
                    DB::select('CALL generarAsueto_sp(?,?,?,?,?,?,?)',$diaPadre);
                    $calendario = $calendario = Calendario::where('fecha', '=', $year."-06-22")->first();
                    $calendario_id = $calendario->id_calendario; 
                    $diaMaestro = [$year."-06-22","Dia del Maestro","22","06","$anio","2206$anio", $calendario_id];
                    DB::select('CALL generarAsueto_sp(?,?,?,?,?,?,?)',$diaMaestro);
                    $calendario = $calendario = Calendario::where('fecha', '=', $year."-08-03")->first();
                    $calendario_id = $calendario->id_calendario;  
                    $agosto3 = [$year."-08-03","Fiestas Agosto","03","08","$anio","0308$anio", $calendario_id];
                    DB::select('CALL generarAsueto_sp(?,?,?,?,?,?,?)',$agosto3);
                    $calendario = $calendario = Calendario::where('fecha', '=', $year."-08-04")->first();
                    $calendario_id = $calendario->id_calendario;  
                    $agosto4 = [$year."-08-04","Fiestas Agosto","04","08","$anio","0408$anio", $calendario_id];
                    DB::select('CALL generarAsueto_sp(?,?,?,?,?,?,?)',$agosto4);
                     $calendario = $calendario = Calendario::where('fecha', '=', $year."-08-05")->first();
                    $calendario_id = $calendario->id_calendario;  
                    $agosto5 = [$year."-08-05","Fiestas Agosto","05","08","$anio","0508$anio", $calendario_id];
                    DB::select('CALL generarAsueto_sp(?,?,?,?,?,?,?)',$agosto5);
                    $calendario = $calendario = Calendario::where('fecha', '=', $year."-08-06")->first();
                    $calendario_id = $calendario->id_calendario;
                    $agosto6 = [$year."-08-06","Fiestas Agosto","06","08","$anio","0608$anio", $calendario_id];

                    DB::select('CALL generarAsueto_sp(?,?,?,?,?,?,?)',$agosto6); 
                    $calendario = $calendario = Calendario::where('fecha', '=', $year."-09-15")->first();
                    $calendario_id = $calendario->id_calendario;
                    $diaIndependencia = [$year."-09-15","Dia de la Independencia","15","09","$anio","1509$anio", $calendario_id];
                    DB::select('CALL generarAsueto_sp(?,?,?,?,?,?,?)',$diaIndependencia);
                    $calendario = $calendario = Calendario::where('fecha', '=', $year."-10-12")->first();
                    $calendario_id = $calendario->id_calendario; 
                    $diaRaza = [$year."-10-12","Dia de la Raza","12","10","$anio","1210$anio", $calendario_id];
                    DB::select('CALL generarAsueto_sp(?,?,?,?,?,?,?)',$diaRaza);
                    $calendario = $calendario = Calendario::where('fecha', '=', $year."-11-02")->first();
                    $calendario_id = $calendario->id_calendario; 
                    $diaDifuntos = [$year."-11-02","Dia de los Difuntos","02","11","$anio","0211$anio", $calendario_id]; 
                    DB::select('CALL generarAsueto_sp(?,?,?,?,?,?,?)',$diaDifuntos);
                    $calendario = $calendario = Calendario::where('fecha', '=', $year."-11-05")->first();
                    $calendario_id = $calendario->id_calendario; 
                    $grito = [$year."-11-05","Aniversario del primer grito de Independencia","05","11","$anio","0511$anio",$calendario_id]; 
                    DB::select('CALL generarAsueto_sp(?,?,?,?,?,?,?)',$grito);
                      $calendario = $calendario = Calendario::where('fecha', '=', $year."-12-25")->first();
                    $calendario_id = $calendario->id_calendario;  
                    $navidad = [$year."-12-25","Navidad","25","12","$anio","2512$anio", $calendario_id];
                    DB::select('CALL generarAsueto_sp(?,?,?,?,?,?,?)',$navidad);  

                    Alert::success('Asuetos del año '. $year.' ya fueron agregados')->autoClose(2000);
                    return redirect()->action('Marcaciones\AsuetosController@listarAsuetos');
                }
        }

    	return redirect()->action('Marcaciones\AsuetosController@listarAsuetos');

    }
    
}

