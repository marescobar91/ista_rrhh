<?php

namespace App\Http\Controllers\Marcaciones;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Str;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use Illuminate\Database\Query\Builder;
use App\Modelos\Empleado\Empleado;
use App\Modelos\Marcacion\Calendario;
use App\Modelos\Marcacion\Marcacion;
use App\Modelos\Marcacion\Asueto;
use Barryvdh\DomPDF\Facade as PDF;
use App\Modelos\Marcacion\Permiso;
use Maatwebsite\Excel\Facades\Excel;

class ReportesDescuentosController extends Controller
{
  public function salidaTempranoDescuentoPDF(Request $request){
    	//Obtiene Fecha Actual
    Carbon::setLocale('es_SV');
    $fecha = Carbon::now(new \DateTimeZone('America/El_Salvador'));
    $hora= $fecha->format('h:i:s A'); 
    $date = $fecha->format('Y-m-d');
    $dias = $fecha->format('d-m-Y');
    $mes = $fecha->monthName;
    $anio = $fecha->year;
    $descuentoArrayC= [];
    $descuentoArrayL= [];
         //fin de Fecha Actual
    $fecha_inicio = $request->input('fecha_ini');
    $fecha_final = $request->input('fecha_fin');

        $fechaFin = Carbon::parse($fecha_final); //convierte la fecha final
        $mesFin = $fechaFin->monthName; //obtengo el mes de la fecha fin

        $fechaIni = Carbon::parse($fecha_inicio); //convierte la fecha inicial
        $mesIni = $fechaIni->monthName; //obtengo el mes de la fecha inicial

        $marcacionMax = DB::table('marcacion_salida_temprano_vw as m')->select('m.nombre','m.apellido','m.tipo','m.salario','m.fecha_marcacion','m.minutos', 'm.hora_marcacion','m.mes','m.codigo_empleado')->whereBetween('m.fecha_marcacion',[$fecha_inicio,$fecha_final])->whereNull('permiso_fecha')->where('m.minutos','>', '00:01:00')->get();
      //tipo de contrato : contratado
        foreach ($marcacionMax as $marcacion) {
          $codigo = $marcacion->codigo_empleado;
          $nombre = $marcacion->nombre . ' ' . $marcacion->apellido;
          $salario = $marcacion->salario;
          $tipo = $marcacion->tipo;
          $mesD = $marcacion->mes;
          $fecha = $marcacion->fecha_marcacion;
          $horas = Carbon::parse($marcacion->minutos)->format('H');
          $minutos =Carbon::parse($marcacion->minutos)->format('i');
          if($mesD == 1 || $mesD == 3 || $mesD == 5 || $mesD == 7 || $mesD == 8 || $mesD == 10 || $mesD ==12){
            $dia = 31;
            $descHoras = ($salario/$dia/8)*$horas;
            $descMinutos= ($salario/$dia/8/60)*$minutos; 
            $des = round(($descHoras+$descMinutos),2);
            if($tipo == 'Contrato'){
              $descuentoArrayC[]=[$codigo,$nombre, $tipo, $salario, $horas,$minutos,$des];
            }elseif ($tipo == 'Ley de Salarios') {
              $descuentoArrayL[]=[$codigo,$nombre, $tipo, $salario, $horas,$minutos,$des];
            }


          }
          elseif ($mesD == 4 || $mesD == 6 || $mesD == 9 || $mesD == 11) {
           $dia = 30;
           $descHoras = ($salario/$dia/8)*$horas;
           $descMinutos= ($salario/$dia/8/60)*$minutos; 
           $des = round(($descHoras+$descMinutos),2);

           if($tipo == 'Contrato'){
            $descuentoArrayC[]=[$codigo,$nombre, $tipo, $salario, $horas,$minutos,$des];
          }elseif ($tipo == 'Ley de Salarios') {
            $descuentoArrayL[]=[$codigo,$nombre, $tipo, $salario, $horas,$minutos,$des];
          }
        }
        else{

          if (date('L', strtotime($fecha))) {
            $dia = 29;
            $descHoras = ($salario/$dia/8)*$horas;
            $descMinutos= ($salario/$dia/8/60)*$minutos; 
            $des = round(($descHoras+$descMinutos),2);
            if($tipo == 'Contrato'){
              $descuentoArrayC[]=[$codigo,$nombre, $tipo, $salario, $horas,$minutos,$des];
            }elseif ($tipo == 'Ley de Salarios') {
              $descuentoArrayL[]=[$codigo,$nombre, $tipo, $salario, $horas,$minutos,$des];
            }
          }
          else{
            $dia = 28;
            $descHoras = ($salario/$dia/8)*$horas;
            $descMinutos= ($salario/$dia/8/60)*$minutos; 
            $des = round(($descHoras+$descMinutos),2);
            if($tipo == 'Contrato'){
              $descuentoArrayC[]=[$codigo,$nombre, $tipo, $salario, $horas,$minutos,$des];
            }elseif ($tipo == 'Ley de Salarios') {
              $descuentoArrayL[]=[$codigo,$nombre, $tipo, $salario, $horas,$minutos,$des];
            }
          }
        }
      }
      
      $descuento = '15:30:00'; 

      $pdf = \PDF::loadView('marcacion.reporteMarcacionDescuentos.imprimirDescuentosSalidaTemprano', ['descuentoArrayC' => $descuentoArrayC, 'descuentoArrayL' => $descuentoArrayL, 'descuento' => $descuento, 'fecha_inicio' => $fecha_inicio, 'fecha_final' => $fecha_final], compact('dia', 'dias', 'hora','mesIni', 'mesFin'))->setPaper('A4', 'landscape')->setWarnings(false);
      return $pdf->stream('salidaTemprana.pdf');
    }

    public function llegadaTardeDescuentoPDF(Request $request){
        //reporte de llegada tarde en PDF
         //Obtiene Fecha Actual

      Carbon::setLocale('es_SV');
      $fecha = Carbon::now(new \DateTimeZone('America/El_Salvador'));
      $hora= $fecha->format('h:i:s A'); 
      $date = $fecha->format('Y-m-d');
      $dias = $fecha->format('d-m-Y');
      $mes = $fecha->monthName;
      $anio = $fecha->year;
       $descuentoArrayC= [];
        $descuentoArrayL= [];
         //fin de Fecha Actual
      $fecha_inicio = $request->input('fecha_ini');
      $fecha_final = $request->input('fecha_fin');

        $fechaFin = Carbon::parse($fecha_final); //convierte la fecha final
        $mesFin = $fechaFin->monthName; //obtengo el mes de la fecha fin

        $fechaIni = Carbon::parse($fecha_inicio); //convierte la fecha inicial
        $mesIni = $fechaIni->monthName; //obtengo el mes de la fecha inicial

        $fechaMaxima = DB::table('llegada_tarde_fecha_vw')->whereBetween('fecha_marcacion', [$fecha_inicio, $fecha_final])->get();
        $marcacionTarde = DB::table('marcacion_llegada_tarde_vw as m')->select('m.id_empleado','m.nombre','m.apellido','m.codigo_empleado','m.semana','m.oficina','m.mes','m.unidad_administrativa', DB::raw('SEC_TO_TIME(SUM(TIME_TO_SEC(m.minutos))) as minutos'),  'm.tipo_plaza', 'm.salario_actual')->whereNull('fecha_asueto')->whereNull('permiso_fecha')->whereBetween('m.fecha_marcacion',[$fecha_inicio,$fecha_final])->groupBy('m.id_empleado','m.nombre','m.apellido','m.codigo_empleado','m.semana','m.oficina','m.unidad_administrativa','m.mes', 'm.tipo_plaza', 'm.salario_actual')->where('m.minutos','>','00:01:00')->get();


        $vacio="Existe";

       foreach ($fechaMaxima as $maxima) {
            foreach ($marcacionTarde as $marcacion) {
                if($maxima->semana==$marcacion->semana && $maxima->mes ==$marcacion->mes && $maxima->id_empleado == $marcacion->id_empleado){
                    if($marcacion->minutos > '00:25:00'){
                        $llegadaTarde[]=[
                        $marcacion->codigo_empleado,
                        $marcacion->nombre, 
                        $marcacion->apellido,
                        $marcacion->unidad_administrativa, 
                        $marcacion->oficina, 
                        $marcacion->tipo_plaza, 
                        $marcacion->salario_actual, 
                        $maxima->fecha_marcacion, 
                        date('H:i:s',strtotime($marcacion->minutos) - strtotime('00:25:00')), 
                        $marcacion->mes];
                      }
                    }
                  }
                }
                    
                    
                foreach ($llegadaTarde as $marcacion) {
                  $codigo = $marcacion[0];
                  $nombre = $marcacion[1] . ' ' . $marcacion[2];
                  $salario = $marcacion[6];
                  $tipo = $marcacion[5];
                  $mesD = Carbon::parse($marcacion[7])->format('m');
                  $fecha = $marcacion[7];
                  $horas = Carbon::parse($marcacion[8])->format('H');
                  $minutos =Carbon::parse($marcacion[8])->format('i');
                  if($mesD == 1 || $mesD == 3 || $mesD == 5 || $mesD == 7 || $mesD == 8 || $mesD == 10 || $mesD ==12){
                    $dia = 31;
                    $descHoras = ($salario/$dia/8)*$horas;
                    $descMinutos= ($salario/$dia/8/60)*$minutos; 
                    $des = round(($descHoras+$descMinutos),2);
                    if($tipo == 'Contrato'){
                      $descuentoArrayC[]=[$codigo,$nombre, $tipo, $salario, $horas,$minutos,$des];
                    }elseif ($tipo == 'Ley de Salarios') {
                      $descuentoArrayL[]=[$codigo,$nombre, $tipo, $salario, $horas,$minutos,$des];
                    }


                  }
                  elseif ($mesD == 4 || $mesD == 6 || $mesD == 9 || $mesD == 11) {
                   $dia = 30;
                   $descHoras = ($salario/$dia/8)*$horas;
                   $descMinutos= ($salario/$dia/8/60)*$minutos; 
                   $des = round(($descHoras+$descMinutos),2);

                   if($tipo == 'Contrato'){
                    $descuentoArrayC[]=[$codigo,$nombre, $tipo, $salario, $horas,$minutos,$des];
                  }elseif ($tipo == 'Ley de Salarios') {
                    $descuentoArrayL[]=[$codigo,$nombre, $tipo, $salario, $horas,$minutos,$des];
                  }
                }
                else{

                  if (date('L', strtotime($fecha))) {
                    $dia = 29;
                    $descHoras = ($salario/$dia/8)*$horas;
                    $descMinutos= ($salario/$dia/8/60)*$minutos; 
                    $des = round(($descHoras+$descMinutos),2);
                    if($tipo == 'Contrato'){
                      $descuentoArrayC[]=[$codigo,$nombre, $tipo, $salario, $horas,$minutos,$des];
                    }elseif ($tipo == 'Ley de Salarios') {
                      $descuentoArrayL[]=[$codigo,$nombre, $tipo, $salario, $horas,$minutos,$des];
                    }
                  }
                  else{
                    $dia = 28;
                    $descHoras = ($salario/$dia/8)*$horas;
                    $descMinutos= ($salario/$dia/8/60)*$minutos; 
                    $des = round(($descHoras+$descMinutos),2);
                    if($tipo == 'Contrato'){
                      $descuentoArrayC[]=[$codigo,$nombre, $tipo, $salario, $horas,$minutos,$des];
                    }elseif ($tipo == 'Ley de Salarios') {
                      $descuentoArrayL[]=[$codigo,$nombre, $tipo, $salario, $horas,$minutos,$des];
                    }
                  }
                }
              }

        $pdf = \PDF::loadView('marcacion.reporteMarcacionDescuentos.imprimirDescuentosLlegadaTarde', ['descuentoArrayC' => $descuentoArrayC, 'descuentoArrayL' => $descuentoArrayL, 'fecha_inicio' => $fecha_inicio, 'fecha_final' => $fecha_final], compact('dias', 'hora','mesIni', 'mesFin'))->setPaper('A4', 'landscape')->setWarnings(false);
        return $pdf->stream('llegadaTarde.pdf');
      }
      public function faltaEntradaDescuentoPDF(Request $request){
      //Obtiene Fecha Actual
        Carbon::setLocale('es_SV');
        $fecha = Carbon::now(new \DateTimeZone('America/El_Salvador'));
        $hora= $fecha->format('h:i:s A'); 
        $date = $fecha->format('Y-m-d');
        $dias = $fecha->format('d-m-Y');
        $mes = $fecha->monthName;
        $anio = $fecha->year;
         $descuentoArrayC= [];
        $descuentoArrayL= [];
         //fin de Fecha Actual
        $fecha_inicio = $request->input('fecha_ini');
        $fecha_final = $request->input('fecha_fin');

        $fechaFin = Carbon::parse($fecha_final); //convierte la fecha final
        $mesFin = $fechaFin->monthName; //obtengo el mes de la fecha fin

        $fechaIni = Carbon::parse($fecha_inicio); //convierte la fecha inicial
        $mesIni = $fechaIni->monthName; 


        $marcacionFaltaEnt = DB::table('marcacion_falta_entrada_vw as m')->select('m.id_empleado','m.nombre','m.apellido','m.codigo_empleado','m.oficina','m.fecha_marcacion', 'm.hora_marcacion','m.mes','m.salario','m.tipo')->whereBetween('m.fecha_marcacion',[$fecha_inicio,$fecha_final])->get();

        $marcacionFalta = DB::table('mar_marcacion as m')->select(DB::raw('COUNT(m.empleado_id) as total'), 'm.empleado_id as empleado', 'm.fecha')->whereBetween('m.fecha', [$fecha_inicio, $fecha_final])->groupBy('m.fecha', 'm.empleado_id')->havingRaw('COUNT(m.empleado_id) = ?', [1])->get();

        foreach ($marcacionFaltaEnt as $marcacion) {
          $id_empleado = $marcacion->id_empleado;
          $codigo = $marcacion->codigo_empleado;
          $nombre = $marcacion->nombre . ' ' . $marcacion->apellido;
          $salario = $marcacion->salario;
          $tipo = $marcacion->tipo;
          $mesD = $marcacion->mes;
          $fecha = $marcacion->fecha_marcacion;
          $horas = 4;
          $minutos =0;
          if($mesD == 1 || $mesD == 3 || $mesD == 5 || $mesD == 7 || $mesD == 8 || $mesD == 10 || $mesD ==12){
            $dia = 31;
            $descHoras = ($salario/$dia/8)*$horas;
            $descMinutos= ($salario/$dia/8/60)*$minutos; 
            $des = round(($descHoras+$descMinutos),2);
            if($tipo == 'Contrato'){
              $descuentoArrayC[]=[$id_empleado,$nombre, $tipo, $salario, $horas,$minutos,$des,$fecha];
            }elseif ($tipo == 'Ley de Salarios') {
              $descuentoArrayL[]=[$id_empleado,$nombre, $tipo, $salario, $horas,$minutos,$des,$fecha];
            }


          }
          elseif ($mesD == 4 || $mesD == 6 || $mesD == 9 || $mesD == 11) {
           $dia = 30;
           $descHoras = ($salario/$dia/8)*$horas;
           $descMinutos= ($salario/$dia/8/60)*$minutos; 
           $des = round(($descHoras+$descMinutos),2);

           if($tipo == 'Contrato'){
            $descuentoArrayC[]=[$id_empleado,$nombre, $tipo, $salario, $horas,$minutos,$des,$fecha];
          }elseif ($tipo == 'Ley de Salarios') {
            $descuentoArrayL[]=[$id_empleado,$nombre, $tipo, $salario, $horas,$minutos,$des,$fecha];
          }
        }
        else{

          if (date('L', strtotime($fecha))) {
            $dia = 29;
            $descHoras = ($salario/$dia/8)*$horas;
            $descMinutos= ($salario/$dia/8/60)*$minutos; 
            $des = round(($descHoras+$descMinutos),2);
            if($tipo == 'Contrato'){
              $descuentoArrayC[]=[$id_empleado,$nombre, $tipo, $salario, $horas,$minutos,$des,$fecha];
            }elseif ($tipo == 'Ley de Salarios') {
              $descuentoArrayL[]=[$id_empleado,$nombre, $tipo, $salario, $horas,$minutos,$des,$fecha];
            }
          }
          else{
            $dia = 28;
            $descHoras = ($salario/$dia/8)*$horas;
            $descMinutos= ($salario/$dia/8/60)*$minutos; 
            $des = round(($descHoras+$descMinutos),2);
            if($tipo == 'Contrato'){
              $descuentoArrayC[]=[$id_empleado,$nombre, $tipo, $salario, $horas,$minutos,$des,$fecha];
            }elseif ($tipo == 'Ley de Salarios') {
              $descuentoArrayL[]=[$id_empleado,$nombre, $tipo, $salario, $horas,$minutos,$des,$fecha];
            }
          }
        }
      }

      $pdf = \PDF::loadView('marcacion.reporteMarcacionDescuentos.imprimirDescuentosFaltaEntrada', ['descuentoArrayC' => $descuentoArrayC, 'descuentoArrayL' => $descuentoArrayL, 'marcacionFalta' => $marcacionFalta, 'fecha_inicio' => $fecha_inicio, 'fecha_final' => $fecha_final], compact('dias', 'hora','mesIni', 'mesFin'))->setPaper('A4', 'landscape')->setWarnings(false);
      return $pdf->stream('faltaMarcacionEntrada.pdf');
    }

    public function faltaSalidaDescuentoPDF(Request $request){
        //Obtiene Fecha Actual
      Carbon::setLocale('es_SV');
      $fecha = Carbon::now(new \DateTimeZone('America/El_Salvador'));
      $hora= $fecha->format('h:i:s A'); 
      $date = $fecha->format('Y-m-d');
      $dias = $fecha->format('d-m-Y');
      $mes = $fecha->monthName;
      $anio = $fecha->year;
       $descuentoArrayC= [];
        $descuentoArrayL= [];
         //fin de Fecha Actual
      $fecha_inicio = $request->input('fecha_ini');
      $fecha_final = $request->input('fecha_fin');

        $fechaFin = Carbon::parse($fecha_final); //convierte la fecha final
        $mesFin = $fechaFin->monthName; //obtengo el mes de la fecha fin

        $fechaIni = Carbon::parse($fecha_inicio); //convierte la fecha inicial
        $mesIni = $fechaIni->monthName; 

        $marcacionFaltaEnt =DB::table('marcacion_falta_salida_vw as m')->select('m.id_empleado','m.nombre','m.apellido','m.codigo_empleado','m.oficina','m.fecha_marcacion', 'm.hora_marcacion','m.mes','m.salario','m.tipo')->whereBetween('m.fecha_marcacion',[$fecha_inicio,$fecha_final])->get();
        $marcacionFalta = DB::table('mar_marcacion as m')->select(DB::raw('COUNT(m.empleado_id) as total'), 'm.empleado_id as empleado', 'm.fecha')->whereBetween('m.fecha', [$fecha_inicio, $fecha_final])->groupBy('m.fecha', 'm.empleado_id')->havingRaw('COUNT(m.empleado_id) = ?', [1])->get();

        foreach ($marcacionFaltaEnt as $marcacion) {
          $id_empleado = $marcacion->id_empleado;
          $codigo = $marcacion->codigo_empleado;
          $nombre = $marcacion->nombre . ' ' . $marcacion->apellido;
          $salario = $marcacion->salario;
          $tipo = $marcacion->tipo;
          $mesD = $marcacion->mes;
          $fecha = $marcacion->fecha_marcacion;
          $horas = 4;
          $minutos =0;
          if($mesD == 1 || $mesD == 3 || $mesD == 5 || $mesD == 7 || $mesD == 8 || $mesD == 10 || $mesD ==12){
            $dia = 31;
            $descHoras = ($salario/$dia/8)*$horas;
            $descMinutos= ($salario/$dia/8/60)*$minutos; 
            $des = round(($descHoras+$descMinutos),2);
            if($tipo == 'Contrato'){
              $descuentoArrayC[]=[$id_empleado,$nombre, $tipo, $salario, $horas,$minutos,$des,$fecha];
            }elseif ($tipo == 'Ley de Salarios') {
              $descuentoArrayL[]=[$id_empleado,$nombre, $tipo, $salario, $horas,$minutos,$des,$fecha];
            }


          }
          elseif ($mesD == 4 || $mesD == 6 || $mesD == 9 || $mesD == 11) {
           $dia = 30;
           $descHoras = ($salario/$dia/8)*$horas;
           $descMinutos= ($salario/$dia/8/60)*$minutos; 
           $des = round(($descHoras+$descMinutos),2);

           if($tipo == 'Contrato'){
            $descuentoArrayC[]=[$id_empleado,$nombre, $tipo, $salario, $horas,$minutos,$des,$fecha];
          }elseif ($tipo == 'Ley de Salarios') {
            $descuentoArrayL[]=[$id_empleado,$nombre, $tipo, $salario, $horas,$minutos,$des,$fecha];
          }
        }
        else{

          if (date('L', strtotime($fecha))) {
            $dia = 29;
            $descHoras = ($salario/$dia/8)*$horas;
            $descMinutos= ($salario/$dia/8/60)*$minutos; 
            $des = round(($descHoras+$descMinutos),2);
            if($tipo == 'Contrato'){
              $descuentoArrayC[]=[$id_empleado,$nombre, $tipo, $salario, $horas,$minutos,$des,$fecha];
            }elseif ($tipo == 'Ley de Salarios') {
              $descuentoArrayL[]=[$id_empleado,$nombre, $tipo, $salario, $horas,$minutos,$des,$fecha];
            }
          }
          else{
            $dia = 28;
            $descHoras = ($salario/$dia/8)*$horas;
            $descMinutos= ($salario/$dia/8/60)*$minutos; 
            $des = round(($descHoras+$descMinutos),2);
            if($tipo == 'Contrato'){
              $descuentoArrayC[]=[$id_empleado,$nombre, $tipo, $salario, $horas,$minutos,$des,$fecha];
            }elseif ($tipo == 'Ley de Salarios') {
              $descuentoArrayL[]=[$id_empleado,$nombre, $tipo, $salario, $horas,$minutos,$des,$fecha];
            }
          }
        }
      }

      $pdf = \PDF::loadView('marcacion.reporteMarcacionDescuentos.imprimirDescuentosFaltaSalida',['descuentoArrayC' => $descuentoArrayC, 'descuentoArrayL' => $descuentoArrayL, 'marcacionFalta' => $marcacionFalta, 'fecha_inicio' => $fecha_inicio, 'fecha_final' => $fecha_final], compact('dias', 'hora','mesIni', 'mesFin'))->setPaper('A4', 'landscape')->setWarnings(false);
      return $pdf->stream('faltaMarcacionSalida.pdf');

    }
    public function ausenciaDescuentoPDF(Request $request){
      Carbon::setLocale('es_SV');
      $fecha = Carbon::now(new \DateTimeZone('America/El_Salvador'));
      $hora= $fecha->format('h:i:s A'); 
      $date = $fecha->format('Y-m-d');
      $dias = $fecha->format('d-m-Y');
      $mes = $fecha->monthName;
      $anio = $fecha->year;
       $descuentoArrayC= [];
        $descuentoArrayL= [];
         //fin de Fecha Actual
      $fecha_inicio = $request->input('fecha_ini');
      $fecha_final = $request->input('fecha_fin');

        $fechaFin = Carbon::parse($fecha_final); //convierte la fecha final
        $mesFin = $fechaFin->monthName; //obtengo el mes de la fecha fin

        $fechaIni = Carbon::parse($fecha_inicio); //convierte la fecha inicial
        $mesIni = $fechaIni->monthName; 

        $marcacion = DB::table('marcaciones_ausencia_vw')->select('codigo_empleado', 'nombre','apellido', 'oficina','fecha','mes', 'salario','oficina','tipo_plaza')->whereBetween('fecha', [$fecha_inicio, $fecha_final])->whereNull('fecha_marcacion')->whereNull('fecha_asueto')->whereNull('fecha_permiso')->whereRaw('WEEKDAY(fecha) <> 5')->whereRaw('WEEKDAY(fecha) <> 6')->get();

        foreach ($marcacion as $marcacion) {
          $codigo = $marcacion->codigo_empleado;
          $nombre = $marcacion->nombre . ' ' . $marcacion->apellido;
          $salario = $marcacion->salario;
          $tipo = $marcacion->tipo_plaza;
          $mesD = $marcacion->mes;
          $fecha = $marcacion->fecha;
          $horas = 8;
          $minutos =0;
          if($mesD == 1 || $mesD == 3 || $mesD == 5 || $mesD == 7 || $mesD == 8 || $mesD == 10 || $mesD ==12){
            $dia = 31;
            $descHoras = ($salario/$dia/8)*$horas;
            $descMinutos= ($salario/$dia/8/60)*$minutos; 
            $des = round(($descHoras+$descMinutos),2);
            if($tipo == 'Contrato'){
              $descuentoArrayC[]=[$codigo,$nombre, $tipo, $salario, $horas,$minutos,$des];
            }elseif ($tipo == 'Ley de Salarios') {
              $descuentoArrayL[]=[$codigo,$nombre, $tipo, $salario, $horas,$minutos,$des];
            }


          }
          elseif ($mesD == 4 || $mesD == 6 || $mesD == 9 || $mesD == 11) {
           $dia = 30;
           $descHoras = ($salario/$dia/8)*$horas;
           $descMinutos= ($salario/$dia/8/60)*$minutos; 
           $des = round(($descHoras+$descMinutos),2);

           if($tipo == 'Contrato'){
            $descuentoArrayC[]=[$codigo,$nombre, $tipo, $salario, $horas,$minutos,$des];
          }elseif ($tipo == 'Ley de Salarios') {
            $descuentoArrayL[]=[$codigo,$nombre, $tipo, $salario, $horas,$minutos,$des];
          }
        }
        else{

          if (date('L', strtotime($fecha))) {
            $dia = 29;
            $descHoras = ($salario/$dia/8)*$horas;
            $descMinutos= ($salario/$dia/8/60)*$minutos; 
            $des = round(($descHoras+$descMinutos),2);
            if($tipo == 'Contrato'){
              $descuentoArrayC[]=[$codigo,$nombre, $tipo, $salario, $horas,$minutos,$des];
            }elseif ($tipo == 'Ley de Salarios') {
              $descuentoArrayL[]=[$codigo,$nombre, $tipo, $salario, $horas,$minutos,$des];
            }
          }
          else{
            $dia = 28;
            $descHoras = ($salario/$dia/8)*$horas;
            $descMinutos= ($salario/$dia/8/60)*$minutos; 
            $des = round(($descHoras+$descMinutos),2);
            if($tipo == 'Contrato'){
              $descuentoArrayC[]=[$codigo,$nombre, $tipo, $salario, $horas,$minutos,$des];
            }elseif ($tipo == 'Ley de Salarios') {
              $descuentoArrayL[]=[$codigo,$nombre, $tipo, $salario, $horas,$minutos,$des];
            }
          }
        }
      }

        $pdf = \PDF::loadView('marcacion.reporteMarcacionDescuentos.imprimirDescuentosAusencias',['descuentoArrayC' => $descuentoArrayC, 'descuentoArrayL' => $descuentoArrayL,'fecha_inicio' => $fecha_inicio, 'fecha_final' => $fecha_final], compact('dias', 'hora','mesIni', 'mesFin'))->setPaper('A4', 'landscape')->setWarnings(false);
        return $pdf->stream('ausenciaMarcacion.pdf');
      }


       public function intermediasDescuentoPDF(Request $request){
        //reporte de intermedias PDF
         //Obtiene Fecha Actual
      Carbon::setLocale('es_SV');
      $fecha = Carbon::now(new \DateTimeZone('America/El_Salvador'));
      $hora= $fecha->format('h:i:s A'); 
      $date = $fecha->format('Y-m-d');
      $dias = $fecha->format('d-m-Y');
      $mes = $fecha->monthName;
      $anio = $fecha->year;
      $descuentoArrayC= [];
      $descuentoArrayL= [];
         //fin de Fecha Actual
      $fecha_inicio = $request->input('fecha_ini');
      $fecha_final = $request->input('fecha_fin');

      $fechaFin = Carbon::parse($fecha_final); //convierte la fecha final
      $mesFin = $fechaFin->monthName; //obtengo el mes de la fecha fin

      $fechaIni = Carbon::parse($fecha_inicio); //convierte la fecha inicial
      $mesIni = $fechaIni->monthName; //obtengo el mes de la fecha inicial

      $marcacionInter = DB::table('marcacion_intermedias_vw as m')->whereBetween('m.fecha_marcacion', [$fecha_inicio, $fecha_final])->get();
      $marcacionIntermedia = DB::table('mar_marcacion as m')->select(DB::raw('COUNT(m.empleado_id) as total'), DB::raw('MAX(m.hora) as max'),DB::raw('MIN(m.hora) as min'),'m.empleado_id as empleado', 'm.fecha')->whereBetween('m.fecha', [$fecha_inicio, $fecha_final])->groupBy('m.fecha', 'm.empleado_id')->havingRaw('COUNT(m.empleado_id) = ?', [4])->get();
        foreach ($marcacionInter as $marc) {
            foreach ($marcacionIntermedia as $marcaciones) {
                if($marc->id_empleado ==$marcaciones->empleado && $marc->fecha_marcacion ==$marcaciones->fecha){
                    if($marc->hora_marcacion != $marcaciones->max && $marc->hora_marcacion != $marcaciones->min){
                       $intermedias[] = [$marc->codigo_empleado, $marc->nombre, $marc->apellido, $marc->oficina, $marc->fecha_marcacion, $marc->tipo, $marc->salario, $marc->mes, $marc->hora_marcacion]; 
                    }
                }
            }
        }

       foreach($intermedias as $key => $marc){
        foreach ($intermedias as $key => $marcacion) {
            if($marc[0]==$marcacion[0] && $marc[4]==$marcacion[4]){
            if($marc[8]<$marcacion[8]){
                $codigo_empleado = $marc[0];
                $nombre = $marc[1];
                $apellido=$marc[2];
                $oficina=$marc[3];
                $fecha_marcacion=$marc[4];
                $tipo=$marc[5];
                $salario=$marc[6];
                $mes=$marc[7];
                $hora_marcacion=$marc[8];
                $intermediaMin[] = [$codigo_empleado,$nombre,$apellido,$oficina,$fecha_marcacion,$tipo,$salario,$mes,$hora_marcacion];
            }
        }
        } 

}

  foreach($intermedias as $key => $marc){
        foreach ($intermedias as $key => $marcacion) {
            if($marc[0]==$marcacion[0] && $marc[4]==$marcacion[4]){
            if($marc[8]<$marcacion[8]){
                $codigo_empleado = $marcacion[0];
                $nombre = $marcacion[1];
                $apellido=$marcacion[2];
                $oficina=$marcacion[3];
                $fecha_marcacion=$marcacion[4];
                $tipo=$marcacion[5];
                $salario=$marcacion[6];
                $mes=$marcacion[7];
                $hora_marcacion=$marcacion[8];
                $intermediaMax[] = [$codigo_empleado,$nombre,$apellido,$oficina,$fecha_marcacion,$tipo,$salario,$mes,$hora_marcacion];
            }
        }
        } 

}
  foreach ($intermediaMin as $key => $min) {
    foreach ($intermediaMax as $key => $max) {
      if($min[0] == $max[0] && $min[4] == $max[4]){
        $codigo=$min[0];
        $nombre=$min[1];
        $apellido=$min[2];
        $oficina=$min[3];
        $fecha_marcacion= date('d-m-Y', strtotime($min[4]));
        $tipo=$min[5];
        $salario=$min[6];
        $mes=$min[7];
        $horas=date('H',strtotime($max[8]) - strtotime($min[8]));
        $minutos=date('i',strtotime($max[8]) - strtotime($min[8]));
        $marcIntemedia[]=[$codigo,$nombre,$apellido,$oficina,$fecha_marcacion,$tipo,$salario,$mes,$horas,$minutos];

      }
    }
  }


                    
                    
                foreach ($marcIntemedia as $key=>$marcacion) {
                  $codigo = $marcacion[0];
                  $nombre = $marcacion[1] . ' ' . $marcacion[2];
                  $salario = $marcacion[6];
                  $tipo = $marcacion[5];
                  $mesD = Carbon::parse($marcacion[4])->format('m');
                  $fecha = $marcacion[4];
                  $horas = $marcacion[8];
                  $minutos =$marcacion[9];
                  if($mesD == 1 || $mesD == 3 || $mesD == 5 || $mesD == 7 || $mesD == 8 || $mesD == 10 || $mesD ==12){
                    $dia = 31;
                    $descHoras = ($salario/$dia/8)*$horas;
                    $descMinutos= ($salario/$dia/8/60)*$minutos; 
                    $des = round(($descHoras+$descMinutos),2);
                    if($tipo == 'Contrato'){
                      $descuentoArrayC[]=[$codigo,$nombre, $tipo, $salario, $horas,$minutos,$des];
                    }elseif ($tipo == 'Ley de Salarios') {
                      $descuentoArrayL[]=[$codigo,$nombre, $tipo, $salario, $horas,$minutos,$des];
                    }


                  }
                  elseif ($mesD == 4 || $mesD == 6 || $mesD == 9 || $mesD == 11) {
                   $dia = 30;
                   $descHoras = ($salario/$dia/8)*$horas;
                   $descMinutos= ($salario/$dia/8/60)*$minutos; 
                   $des = round(($descHoras+$descMinutos),2);

                   if($tipo == 'Contrato'){
                    $descuentoArrayC[]=[$codigo,$nombre, $tipo, $salario, $horas,$minutos,$des];
                  }elseif ($tipo == 'Ley de Salarios') {
                    $descuentoArrayL[]=[$codigo,$nombre, $tipo, $salario, $horas,$minutos,$des];
                  }
                }
                else{

                  if (date('L', strtotime($fecha))) {
                    $dia = 29;
                    $descHoras = ($salario/$dia/8)*$horas;
                    $descMinutos= ($salario/$dia/8/60)*$minutos; 
                    $des = round(($descHoras+$descMinutos),2);
                    if($tipo == 'Contrato'){
                      $descuentoArrayC[]=[$codigo,$nombre, $tipo, $salario, $horas,$minutos,$des];
                    }elseif ($tipo == 'Ley de Salarios') {
                      $descuentoArrayL[]=[$codigo,$nombre, $tipo, $salario, $horas,$minutos,$des];
                    }
                  }
                  else{
                    $dia = 28;
                    $descHoras = ($salario/$dia/8)*$horas;
                    $descMinutos= ($salario/$dia/8/60)*$minutos; 
                    $des = round(($descHoras+$descMinutos),2);
                    if($tipo == 'Contrato'){
                      $descuentoArrayC[]=[$codigo,$nombre, $tipo, $salario, $horas,$minutos,$des];
                    }elseif ($tipo == 'Ley de Salarios') {
                      $descuentoArrayL[]=[$codigo,$nombre, $tipo, $salario, $horas,$minutos,$des];
                    }
                  }
                }
              }

        $pdf = \PDF::loadView('marcacion.reporteMarcacionDescuentos.imprimirDescuentosIntermedias', ['descuentoArrayC' => $descuentoArrayC, 'descuentoArrayL' => $descuentoArrayL, 'fecha_inicio' => $fecha_inicio, 'fecha_final' => $fecha_final], compact('dias', 'hora','mesIni', 'mesFin'))->setPaper('A4', 'landscape')->setWarnings(false);
        return $pdf->stream('intermedias.pdf');
      }



    }
