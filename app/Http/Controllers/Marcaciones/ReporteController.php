<?php

namespace App\Http\Controllers\Marcaciones;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Str;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use Illuminate\Database\Query\Builder;
use App\Modelos\Empleado\Empleado;
use App\Modelos\Marcacion\Calendario;
use App\Modelos\Marcacion\Marcacion;
use App\Modelos\Marcacion\Asueto;
use Barryvdh\DomPDF\Facade as PDF;
use App\Modelos\Marcacion\Permiso;
use App\Exports\MarcacionesExport;
use App\Exports\MarcacionSTExport;
use App\Exports\MarcacionFaltaEntradaExport;
use App\Exports\MarcacionFaltaSalidaExport;
use App\Exports\MarcacionLlegadaTardeExport;
use App\Exports\MarcacionIntermediasExport;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\DescuentoImport;

class ReporteController extends Controller
{   
    //funcion para los permisos de los reportes de marcaciones
    public function __construct(){

    $this->middleware(['permission:reporte.marcaciones'], ['only'    => ['buscarLlegadaTarde', 'reporteLlegadaTarde', 'llegadaTardePDF', 'buscarReporteSalidaTemprana', 'reporteSalidaTemprana', 'salidaTempranaPDF', 'buscarReporteFaltaMarcacionEntrada', 'reporteFaltaMarcacionEntrada', 'faltaMarcacionEntradaPDF', 'buscarReporteFaltaMarcacionSalida', 'reporteFaltaMarcacionSalida', 'faltaMarcacionSalidaPDF', 'buscarReporteAusencia', 'reporteMarcacionAusencia', 'faltaMarcacionAusenciaPDF', 'buscarMarcacionIntermedia', 'reporteMarcacionIntermedia', 'marcacionIntermediaPDF', 'verReportes', 'faltaMarcacionAusenciaExcel', 'llegadaTardeExcel', 'salidaTempranaExcel', 'buscarDescuentos', 'descuentoImp', 'faltaEntradaExcel', 'faltaSalidaExcel', 'marcacionIntermediaExcel']]);
 
    }
    //
    public function buscarLlegadaTarde()
    {   
        //buscar reporte de llegada tarde
        $vacio = "";
    	return view('marcacion.reporteMarcacion.verLlegadasTardes', compact('vacio'));
    }
    //llegada tarde
    public function reporteLlegadaTarde(Request $request){ 
        //carga el reporte de llegada tarde
        //Obtiene Fecha Actual
        Carbon::setLocale('es_SV');
        $fecha = Carbon::now(new \DateTimeZone('America/El_Salvador'));
        $hora= $fecha->format('h:i:s A'); 
        $date = $fecha->format('Y-m-d');
        $dias = $fecha->format('d-m-Y');
        $dia = $fecha->day;
        $mes = $fecha->monthName;
        $anio = $fecha->year;
         //fin de Fecha Actual
    	$fecha_inicio = $request->input('fecha_inicio');
        $fecha_final = $request->input('fecha_final');
        $codigo_empleado = $request->codigo_empleado;

        $fechaFin = Carbon::parse($fecha_final); //convierte la fecha final
        $mesFin = $fechaFin->monthName; //obtengo el mes de la fecha fin

        $fechaIni = Carbon::parse($fecha_inicio); //convierte la fecha inicial
        $mesIni = $fechaIni->monthName; //obtengo el mes de la fecha inicial
        

        if($fecha_inicio!=null && $fecha_final!=null){

        if($fecha_final < $fecha_inicio){

            flash("¡ERROR! ¡La fecha final no puede ser menor que la fecha de inicio!")->error()->important();
            return redirect()->action('Marcaciones\ReporteController@buscarLlegadaTarde');
        }
        if($fecha_inicio > $date){

             flash("¡ERROR! ¡La fecha inicio no puede ser mayor que la fecha de actual!")->error()->important();
            return redirect()->action('Marcaciones\ReporteController@buscarLlegadaTarde');
        }
        if($fecha_final > $date){

             flash("¡ERROR! ¡La fecha final no puede ser mayor que la fecha actual!")->error()->important();
            return redirect()->action('Marcaciones\ReporteController@buscarLlegadaTarde');
        }
    }
        $minutos = '00:05:00';

        $fechaMaxima = DB::table('llegada_tarde_fecha_vw')->whereBetween('fecha_marcacion', [$fecha_inicio, $fecha_final])->get();
      

        $marcacionTarde = DB::table('marcacion_llegada_tarde_vw as m')->select('m.id_empleado','m.nombre','m.apellido','m.codigo_empleado','m.semana','m.oficina','m.mes','m.unidad_administrativa', DB::raw('SEC_TO_TIME(SUM(TIME_TO_SEC(m.minutos))) as minutos'),  'm.tipo_plaza', 'm.salario_actual')->whereNull('fecha_asueto')->whereNull('permiso_fecha')->whereBetween('m.fecha_marcacion',[$fecha_inicio,$fecha_final])->groupBy('m.id_empleado','m.nombre','m.apellido','m.codigo_empleado','m.semana','m.oficina','m.unidad_administrativa','m.mes', 'm.tipo_plaza', 'm.salario_actual')->get();




        $vacio="Existe";

        foreach ($fechaMaxima as $maxima) {
            foreach ($marcacionTarde as $marcacion) {
                if($maxima->semana==$marcacion->semana && $maxima->mes ==$marcacion->mes && $maxima->id_empleado == $marcacion->id_empleado){
                    if($marcacion->minutos > '00:25:00'){
                    $llegadaTarde[]=[$marcacion->codigo_empleado,
                    $marcacion->nombre, $marcacion->apellido,$marcacion->unidad_administrativa, $marcacion->oficina, $marcacion->tipo_plaza, $marcacion->salario_actual, $maxima->fecha_marcacion, date('H:i:s',strtotime($marcacion->minutos) - strtotime('00:25:00'))];
                }
                  

                }
            }
        }


        $noMarcacion= $marcacionTarde->isEmpty();               
        if($noMarcacion == true)
                {
                flash('¡No se tiene dicha información en el sistema!')->error()->important();
                return redirect()->action('Marcaciones\ReporteController@buscarLlegadaTarde');
                }

        return view('marcacion.reporteMarcacion.verLlegadasTardes', compact('llegadaTarde', 'vacio', 'fecha_inicio', 'fecha_final', 'dia', 'mesFin', 'mesIni', 'hora','dias'));
    
     }

     public function llegadaTardePDF(Request $request){
        //reporte de llegada tarde en PDF
         //Obtiene Fecha Actual
        Carbon::setLocale('es_SV');
        $fecha = Carbon::now(new \DateTimeZone('America/El_Salvador'));
        $hora= $fecha->format('h:i:s A'); 
        $date = $fecha->format('Y-m-d');
        $dia = $fecha->format('d-m-Y');
         $mes = $fecha->monthName;
        $anio = $fecha->year;
         //fin de Fecha Actual
        $fecha_inicio = $request->input('fecha_ini');
        $fecha_final = $request->input('fecha_fin');

        $fechaFin = Carbon::parse($fecha_final); //convierte la fecha final
        $mesFin = $fechaFin->monthName; //obtengo el mes de la fecha fin

        $fechaIni = Carbon::parse($fecha_inicio); //convierte la fecha inicial
        $mesIni = $fechaIni->monthName; //obtengo el mes de la fecha inicial

        $fechaMaxima = DB::table('llegada_tarde_fecha_vw')->whereBetween('fecha_marcacion', [$fecha_inicio, $fecha_final])->get();
        $marcacionTarde = DB::table('marcacion_llegada_tarde_vw as m')->select('m.id_empleado','m.nombre','m.apellido','m.codigo_empleado','m.semana','m.oficina','m.mes','m.unidad_administrativa', DB::raw('SEC_TO_TIME(SUM(TIME_TO_SEC(m.minutos))) as minutos'),  'm.tipo_plaza', 'm.salario_actual')->whereNull('fecha_asueto')->whereNull('permiso_fecha')->whereBetween('m.fecha_marcacion',[$fecha_inicio,$fecha_final])->groupBy('m.id_empleado','m.nombre','m.apellido','m.codigo_empleado','m.semana','m.oficina','m.unidad_administrativa','m.mes', 'm.tipo_plaza', 'm.salario_actual')->get();


        $vacio="Existe";

        foreach ($fechaMaxima as $maxima) {
            foreach ($marcacionTarde as $marcacion) {
                if($maxima->semana==$marcacion->semana && $maxima->mes ==$marcacion->mes && $maxima->id_empleado == $marcacion->id_empleado){
                    if($marcacion->minutos > '00:25:00'){
                    $llegadaTarde[]=[$marcacion->codigo_empleado,
                    $marcacion->nombre, $marcacion->apellido,$marcacion->unidad_administrativa, $marcacion->oficina, $marcacion->tipo_plaza, $marcacion->salario_actual, $maxima->fecha_marcacion, date('H:i:s',strtotime($marcacion->minutos) - strtotime('00:25:00'))];
                }
                  

                }
            }
        }

        $pdf = \PDF::loadView('marcacion.reporteMarcacion.imprimirLlegadaTarde', ['llegadaTarde' => $llegadaTarde, 'fecha_inicio' => $fecha_inicio, 'fecha_final' => $fecha_final], compact('dia', 'hora','mesIni', 'mesFin'))->setPaper('A4', 'landscape')->setWarnings(false);
         return $pdf->stream('llegadaTarde.pdf');

     } 

     public function llegadaTardeExcel(Request $request){
        //funcion para reporte de excel
        $now = Carbon::now(new \DateTimeZone('America/El_Salvador')); 
        $fecha_inicio = Carbon::parse($request->fecha_ini);
        $fecha_final = Carbon::parse($request->fecha_fin);

        return Excel::download(new MarcacionLlegadaTardeExport($fecha_inicio, $fecha_final), 'llegadaTarde'. $now .'.xlsx'); 

     }


       public function buscarReporteSalidaTemprana()
    {
        $vacio = "";
        return view('marcacion.reporteMarcacion.verSalidaTemprana', compact('vacio'));
    }

    public function reporteSalidaTemprana(Request $request){ 
          //Obtiene Fecha Actual
        Carbon::setLocale('es_SV');
        $fecha = Carbon::now(new \DateTimeZone('America/El_Salvador'));
        $hora= $fecha->format('h:i:s A'); 
        $date = $fecha->format('Y-m-d');
        $dia = $fecha->format('d-m-Y');
        $mes = $fecha->monthName;
        $anio = $fecha->year;
         //fin de Fecha Actual
        $fecha_inicio = $request->input('fecha_inicio');
        $fecha_final = $request->input('fecha_final');
        $codigo_empleado = $request->codigo_empleado;

        $fechaFin = Carbon::parse($fecha_final); //convierte la fecha final
        $mesFin = $fechaFin->monthName; //obtengo el mes de la fecha fin

        $fechaIni = Carbon::parse($fecha_inicio); //convierte la fecha inicial
        $mesIni = $fechaIni->monthName; //obtengo el mes de la fecha inicial


        if($fecha_inicio!=null && $fecha_final!=null){

        if($fecha_final < $fecha_inicio){

            flash("¡ERROR! ¡La fecha final no puede ser menor que la fecha de inicio!")->error()->important();
            return redirect()->action('Marcaciones\ReporteController@buscarReporteSalidaTemprana');
        }
        if($fecha_inicio > $date){

             flash("¡ERROR! ¡La fecha inicio no puede ser mayor que la fecha de actual!")->error()->important();
            return redirect()->action('Marcaciones\ReporteController@buscarReporteSalidaTemprana');
        }
        if($fecha_final > $date){

             flash("¡ERROR! ¡La fecha final no puede ser mayor que la fecha actual!")->error()->important();
            return redirect()->action('Marcaciones\ReporteController@buscarReporteSalidaTemprana');
        }
    }

        $marcacionMax = DB::table('marcacion_salida_temprano_vw as m')->select('m.nombre','m.apellido','m.codigo_empleado','m.fecha_marcacion','m.oficina','m.fecha_marcacion','m.minutos', 'm.hora_marcacion')->whereBetween('m.fecha_marcacion',[$fecha_inicio,$fecha_final])->whereNull('permiso_fecha')->where('m.minutos','>', '00:01:00')->get();

        $vacio="Existe";
     
        $noMarcacion= $marcacionMax->isEmpty();               
        if($noMarcacion == true)
                {
                flash('¡No se tiene dicha información en el sistema!')->error()->important();
                return redirect()->action('Marcaciones\ReporteController@buscarReporteSalidaTemprana');
                }
            $descuento = '15:30:00';

        return view('marcacion.reporteMarcacion.verSalidaTemprana', compact('marcacionMax', 'vacio', 'descuento', 'fecha_inicio', 'fecha_final', 'dia', 'mesFin', 'mesIni', 'hora'));
    
     }


      public function salidaTempranaPDF(Request $request){ 

         //Obtiene Fecha Actual
        Carbon::setLocale('es_SV');
        $fecha = Carbon::now(new \DateTimeZone('America/El_Salvador'));
        $hora= $fecha->format('h:i:s A'); 
        $date = $fecha->format('Y-m-d');
        $dia = $fecha->format('d-m-Y');
        $mes = $fecha->monthName;
        $anio = $fecha->year;
         //fin de Fecha Actual
        $fecha_inicio = $request->input('fecha_ini');
        $fecha_final = $request->input('fecha_fin');

        $fechaFin = Carbon::parse($fecha_final); //convierte la fecha final
        $mesFin = $fechaFin->monthName; //obtengo el mes de la fecha fin

        $fechaIni = Carbon::parse($fecha_inicio); //convierte la fecha inicial
        $mesIni = $fechaIni->monthName; //obtengo el mes de la fecha inicial

      $marcacionMax = DB::table('marcacion_salida_temprano_vw as m')->select('m.nombre','m.apellido','m.codigo_empleado','m.fecha_marcacion','m.oficina','m.fecha_marcacion','m.minutos', 'm.hora_marcacion')->whereBetween('m.fecha_marcacion',[$fecha_inicio,$fecha_final])->whereNull('permiso_fecha')->where('m.minutos','>', '00:01:00')->get();
        $descuento = '15:30:00'; 
        

        $pdf = \PDF::loadView('marcacion.reporteMarcacion.imprimirSalidaTemprana', ['marcacionMax' => $marcacionMax, 'descuento' => $descuento, 'fecha_inicio' => $fecha_inicio, 'fecha_final' => $fecha_final], compact('dia', 'hora','mesIni', 'mesFin'))->setPaper('A4', 'landscape')->setWarnings(false);
         return $pdf->stream('salidaTemprana.pdf');

     }  
     public function salidaTempranaExcel(Request $request){
        $now = Carbon::now(new \DateTimeZone('America/El_Salvador'));
        $fecha_inicio = Carbon::parse($request->fecha_ini);
        $fecha_final = Carbon::parse($request->fecha_fin);

        return Excel::download(new MarcacionSTExport($fecha_inicio, $fecha_final), 'salidaTemprano'. $now .'.xlsx');
     }

    public function buscarDescuentos(){
         $vacio = "";
         return view('marcacion.reporteMarcacion.verReporteDescuento',compact('vacio'));

    }

     public function descuentoImp(Request $request){

        $archivoExcel = $request->file('reporteImp');
        $fecha_final = Carbon::parse($request->fecha_final); //convierte la fecha final
        $mesFin = $fecha_final->monthName; //obtengo el mes de la fecha fin

        $fecha_inicio = Carbon::parse($request->fecha_inicio); //convierte la fecha inicial
        $mesIni = $fecha_inicio->monthName; //obtengo el mes de la fecha inicial
        $tipoReporte = $request->tipoReporte;
        $fecha = Carbon::now(new \DateTimeZone('America/El_Salvador'));
        $hora= $fecha->format('h:i:s A'); 
        $date = $fecha->format('Y-m-d');
        $day = $fecha->format('d-m-Y');
        $import = Excel::toArray(new DescuentoImport, $archivoExcel);
       foreach ($import as $import) {
           unset($import[0]);
           $descuentos = array_values($import);
           
       }
      //dd($descuentos);
        $descuentoArrayC= [];
        $descuentoArrayL= [];
       foreach ($descuentos as $descuentos) {
            $codigo = $descuentos[0];  
            $nombre = $descuentos[1];  
            $oficina = $descuentos[2];
            $fecha = $descuentos[3];
            $tipo = $descuentos[4];
            $salario =$descuentos[5];
            $mesD = $descuentos[6];
            $horas = $descuentos[7];
            $minutos = $descuentos[8];
           if($mesD == 1 || $mesD == 3 || $mesD == 5 || $mesD == 7 || $mesD == 8 || $mesD == 10 || $mesD ==12){
                $dia = 31;
                $descHoras = ($salario/$dia/8)*$horas;
                $descMinutos= ($salario/$dia/8/60)*$minutos; 
                $des = round(($descHoras+$descMinutos),2);
                if($tipo == 'Contrato'){
                    $descuentoArrayC[]=[$codigo,$nombre, $tipo, $salario, $horas,$minutos,$des];
                }elseif ($tipo == 'Ley de Salarios') {
                    $descuentoArrayL[]=[$codigo,$nombre, $tipo, $salario, $horas,$minutos,$des];
                }

                
           }
           elseif ($mesD == 4 || $mesD == 6 || $mesD == 9 || $mesD == 11) {
               $dia = 30;
                $descHoras = ($salario/$dia/8)*$horas;
                $descMinutos= ($salario/$dia/8/60)*$minutos; 
                $des = round(($descHoras+$descMinutos),2);
               
                if($tipo == 'Contrato'){
                    $descuentoArrayC[]=[$codigo,$nombre, $tipo, $salario, $horas,$minutos,$des];
                }elseif ($tipo == 'Ley de Salarios') {
                    $descuentoArrayL[]=[$codigo,$nombre, $tipo, $salario, $horas,$minutos,$des];
                }
           }
           else{

                if (date('L', strtotime($fecha))) {
                    $dia = 29;
                     $descHoras = ($salario/$dia/8)*$horas;
                    $descMinutos= ($salario/$dia/8/60)*$minutos; 
                    $des = round(($descHoras+$descMinutos),2);
                    if($tipo == 'Contrato'){
                        $descuentoArrayC[]=[$codigo,$nombre, $tipo, $salario, $horas,$minutos,$des];
                    }elseif ($tipo == 'Ley de Salarios') {
                        $descuentoArrayL[]=[$codigo,$nombre, $tipo, $salario, $horas,$minutos,$des];
                    }
                }
                else{
                    $dia = 28;
                     $descHoras = ($salario/$dia/8)*$horas;
                    $descMinutos= ($salario/$dia/8/60)*$minutos; 
                    $des = round(($descHoras+$descMinutos),2);
                    if($tipo == 'Contrato'){
                        $descuentoArrayC[]=[$codigo,$nombre, $tipo, $salario, $horas,$minutos,$des];
                    }elseif ($tipo == 'Ley de Salarios') {
                        $descuentoArrayL[]=[$codigo,$nombre, $tipo, $salario, $horas,$minutos,$des];
                    }
                }
           }
            
       }

       $vacio="Existe";
    $pdf = \PDF::loadView('marcacion.reporteMarcacion.imprimirDescuentos', compact('vacio','descuentoArrayC','descuentoArrayL','fecha_final','fecha_inicio','tipoReporte','mesIni','mesFin','day','hora'))->setPaper('A4', 'landscape')->setWarnings(false);
    return $pdf->stream('reporteDescuentos.pdf');
       //return view('marcacion.reporteMarcacion.verReporteDescuento' ,compact('vacio','descuentoArray','fecha_final','fecha_inicio','tipoReporte','mesIni','mesFin','day','hora'));

     }

    

     public function buscarReporteFaltaMarcacionEntrada(){

         $vacio = "";
        return view('marcacion.reporteMarcacion.verFaltaEntrada', compact('vacio'));

      
     }

     public function reporteFaltaMarcacionEntrada(Request $request){

         //Obtiene Fecha Actual
        Carbon::setLocale('es_SV');
        $fecha = Carbon::now(new \DateTimeZone('America/El_Salvador'));
        $hora= $fecha->format('h:i:s A'); 
        $date = $fecha->format('Y-m-d');
        $dia = $fecha->format('d-m-Y');
        $mes = $fecha->monthName;
        $anio = $fecha->year;
         //fin de Fecha Actual
        $fecha_inicio = $request->input('fecha_inicio');
        $fecha_final = $request->input('fecha_final');
        $codigo_empleado = $request->codigo_empleado;

        $fechaFin = Carbon::parse($fecha_final); //convierte la fecha final
        $mesFin = $fechaFin->monthName; //obtengo el mes de la fecha fin

        $fechaIni = Carbon::parse($fecha_inicio); //convierte la fecha inicial
        $mesIni = $fechaIni->monthName; //obtengo el mes de la fecha inicial


        if($fecha_inicio!=null && $fecha_final!=null){

        if($fecha_final < $fecha_inicio){

            flash("¡ERROR! ¡La fecha final no puede ser menor que la fecha de inicio!")->error()->important();
            return redirect()->action('Marcaciones\ReporteController@buscarReporteFaltaMarcacionEntrada');
        }
        if($fecha_inicio > $date){

             flash("¡ERROR! ¡La fecha inicio no puede ser mayor que la fecha de actual!")->error()->important();
            return redirect()->action('Marcaciones\ReporteController@buscarReporteFaltaMarcacionEntrada');
        }
        if($fecha_final > $date){

             flash("¡ERROR! ¡La fecha final no puede ser mayor que la fecha actual!")->error()->important();
            return redirect()->action('Marcaciones\ReporteController@buscarReporteFaltaMarcacionEntrada');
        }
    }
       

       $marcacionFaltaEnt = DB::table('marcacion_falta_entrada_vw as m')->select('m.id_empleado','m.nombre','m.apellido','m.codigo_empleado','m.oficina','m.fecha_marcacion', 'm.hora_marcacion')->whereBetween('m.fecha_marcacion',[$fecha_inicio,$fecha_final])->get();

       $marcacionFalta = DB::table('mar_marcacion as m')->select(DB::raw('COUNT(m.empleado_id) as total'), 'm.empleado_id as empleado', 'm.fecha')->whereBetween('m.fecha', [$fecha_inicio, $fecha_final])->groupBy('m.fecha', 'm.empleado_id')->havingRaw('COUNT(m.empleado_id) = ?', [1])->get();


        $vacio="Exis";

        $noMarcacion= $marcacionFaltaEnt->isEmpty();   
        $noFalta = $marcacionFalta->isEmpty();            
        if($noMarcacion == true && $noFalta== true)
                {
                flash('¡No se tiene dicha información en el sistema!')->error()->important();
                return redirect()->action('Marcaciones\ReporteController@buscarReporteFaltaMarcacionEntrada');
                }
            
      //  dd($marcacionFaltaEnt);
        return view('marcacion.reporteMarcacion.verFaltaEntrada', compact('marcacionFaltaEnt', 'vacio', 'fecha_inicio', 'fecha_final', 'dia', 'mesFin', 'mesIni', 'hora', 'marcacionFalta'));
    
     }

     /* public function faltaMarcacionEntradaPDF(Request $request){

        //Obtiene Fecha Actual
        Carbon::setLocale('es_SV');
        $fecha = Carbon::now(new \DateTimeZone('America/El_Salvador'));
        $hora= $fecha->format('h:i:s A'); 
        $date = $fecha->format('Y-m-d');
        $dia = $fecha->format('d-m-Y');
         $mes = $fecha->monthName;
        $anio = $fecha->year;
         //fin de Fecha Actual
        $fecha_inicio = $request->input('fecha_ini');
        $fecha_final = $request->input('fecha_fin');

        $fechaFin = Carbon::parse($fecha_final); //convierte la fecha final
        $mesFin = $fechaFin->monthName; //obtengo el mes de la fecha fin

        $fechaIni = Carbon::parse($fecha_inicio); //convierte la fecha inicial
        $mesIni = $fechaIni->monthName; 


        $marcacionFaltaEnt = DB::table('marcacion_falta_entrada_vw as m')->select('m.id_empleado','m.nombre','m.apellido','m.codigo_empleado','m.oficina','m.fecha_marcacion', 'm.hora_marcacion')->whereBetween('m.fecha_marcacion',[$fecha_inicio,$fecha_final])->get();

        $marcacionFalta = DB::table('mar_marcacion as m')->select(DB::raw('COUNT(m.empleado_id) as total'), 'm.empleado_id as empleado', 'm.fecha')->whereBetween('m.fecha', [$fecha_inicio, $fecha_final])->groupBy('m.fecha', 'm.empleado_id')->havingRaw('COUNT(m.empleado_id) = ?', [1])->get();

        $pdf = \PDF::loadView('marcacion.reporteMarcacion.imprimirFaltaEntrada', ['marcacionFaltaEnt' => $marcacionFaltaEnt, 'marcacionFalta' => $marcacionFalta, 'fecha_inicio' => $fecha_inicio, 'fecha_final' => $fecha_final], compact('dia', 'hora','mesIni', 'mesFin'))->setPaper('A4', 'landscape')->setWarnings(false);
         return $pdf->stream('faltaMarcacionEntrada.pdf');

      
     } */

   

     public function buscarReporteFaltaMarcacionSalida(){

         $vacio = "";
        return view('marcacion.reporteMarcacion.verFaltaSalida', compact('vacio'));

      
     }

     public function reporteFaltaMarcacionSalida(Request $request){

         //Obtiene Fecha Actual
        Carbon::setLocale('es_SV');
        $fecha = Carbon::now(new \DateTimeZone('America/El_Salvador'));
        $hora= $fecha->format('h:i:s A'); 
        $date = $fecha->format('Y-m-d');
        $dia = $fecha->format('d-m-Y');
        $mes = $fecha->monthName;
        $anio = $fecha->year;
         //fin de Fecha Actual
        $fecha_inicio = $request->input('fecha_inicio');
        $fecha_final = $request->input('fecha_final');
        $codigo_empleado = $request->codigo_empleado;

        $fechaFin = Carbon::parse($fecha_final); //convierte la fecha final
        $mesFin = $fechaFin->monthName; //obtengo el mes de la fecha fin

        $fechaIni = Carbon::parse($fecha_inicio); //convierte la fecha inicial
        $mesIni = $fechaIni->monthName; //obtengo el mes de la fecha inicial


        if($fecha_inicio!=null && $fecha_final!=null){

        if($fecha_final < $fecha_inicio){

            flash("¡ERROR! ¡La fecha final no puede ser menor que la fecha de inicio!")->error()->important();
            return redirect()->action('Marcaciones\ReporteController@buscarReporteFaltaMarcacionSalida');
        }
        if($fecha_inicio > $date){

             flash("¡ERROR! ¡La fecha inicio no puede ser mayor que la fecha de actual!")->error()->important();
            return redirect()->action('Marcaciones\ReporteController@buscarReporteFaltaMarcacionSalida');
        }
        if($fecha_final > $date){

             flash("¡ERROR! ¡La fecha final no puede ser mayor que la fecha actual!")->error()->important();
            return redirect()->action('Marcaciones\ReporteController@buscarReporteFaltaMarcacionSalida');
        }
    }
       


       $marcacionFaltaEnt =DB::table('marcacion_falta_salida_vw as m')->select('m.id_empleado','m.nombre','m.apellido','m.codigo_empleado','m.oficina','m.fecha_marcacion', 'm.hora_marcacion')->whereBetween('m.fecha_marcacion',[$fecha_inicio,$fecha_final])->get();
       $marcacionFalta = DB::table('mar_marcacion as m')->select(DB::raw('COUNT(m.empleado_id) as total'), 'm.empleado_id as empleado', 'm.fecha')->whereBetween('m.fecha', [$fecha_inicio, $fecha_final])->groupBy('m.fecha', 'm.empleado_id')->havingRaw('COUNT(m.empleado_id) = ?', [1])->get();


        $vacio="Existe";

        $noMarcacion= $marcacionFaltaEnt->isEmpty();   
        $noFalta = $marcacionFalta->isEmpty();            
        if($noMarcacion == true && $noFalta== true)
                {
                flash('¡No se tiene dicha información en el sistema!')->error()->important();
                return redirect()->action('Marcaciones\ReporteController@buscarReporteFaltaMarcacionSalida');
                }
            

        return view('marcacion.reporteMarcacion.verFaltaSalida', compact('marcacionFaltaEnt', 'vacio', 'fecha_inicio', 'fecha_final', 'dia', 'mesFin', 'mesIni', 'hora', 'marcacionFalta'));
    
     }

   /*  public function faltaMarcacionSalidaPDF(Request $request){ 

        //Obtiene Fecha Actual
        Carbon::setLocale('es_SV');
        $fecha = Carbon::now(new \DateTimeZone('America/El_Salvador'));
        $hora= $fecha->format('h:i:s A'); 
        $date = $fecha->format('Y-m-d');
        $dia = $fecha->format('d-m-Y');
         $mes = $fecha->monthName;
        $anio = $fecha->year;
         //fin de Fecha Actual
        $fecha_inicio = $request->input('fecha_ini');
        $fecha_final = $request->input('fecha_fin');

        $fechaFin = Carbon::parse($fecha_final); //convierte la fecha final
        $mesFin = $fechaFin->monthName; //obtengo el mes de la fecha fin

        $fechaIni = Carbon::parse($fecha_inicio); //convierte la fecha inicial
        $mesIni = $fechaIni->monthName; 



       $marcacionFaltaEnt =DB::table('marcacion_falta_salida_vw as m')->select('m.id_empleado','m.nombre','m.apellido','m.codigo_empleado','m.oficina','m.fecha_marcacion', 'm.hora_marcacion')->whereBetween('m.fecha_marcacion',[$fecha_inicio,$fecha_final])->get();
       $marcacionFalta = DB::table('mar_marcacion as m')->select(DB::raw('COUNT(m.empleado_id) as total'), 'm.empleado_id as empleado', 'm.fecha')->whereBetween('m.fecha', [$fecha_inicio, $fecha_final])->groupBy('m.fecha', 'm.empleado_id')->havingRaw('COUNT(m.empleado_id) = ?', [1])->get();

        $pdf = \PDF::loadView('marcacion.reporteMarcacion.imprimirFaltaSalida', ['marcacionFaltaEnt' => $marcacionFaltaEnt, 'marcacionFalta' => $marcacionFalta, 'fecha_inicio' => $fecha_inicio, 'fecha_final' => $fecha_final], compact('dia', 'hora','mesIni', 'mesFin'))->setPaper('A4', 'landscape')->setWarnings(false);
         return $pdf->stream('faltaMarcacionSalida.pdf');

      
     } */

     

     public function buscarReporteAusencia(){
        $vacio = "";
        return view('marcacion.reporteMarcacion.verAusencias', compact('vacio'));


     } 


     public function reporteMarcacionAusencia(Request $request){
        //Obtiene Fecha Actual
        Carbon::setLocale('es_SV');
        $fecha = Carbon::now(new \DateTimeZone('America/El_Salvador'));
        $hora= $fecha->format('h:i:s A'); 
        $date = $fecha->format('Y-m-d');
        $dia = $fecha->format('d-m-Y');
        $mes = $fecha->monthName;
        $anio = $fecha->year;
         //fin de Fecha Actual
        $fecha_inicio = $request->input('fecha_inicio');
        $fecha_final = $request->input('fecha_final');

        $fechaFin = Carbon::parse($fecha_final); //convierte la fecha final
        $mesFin = $fechaFin->monthName; //obtengo el mes de la fecha fin

        $fechaIni = Carbon::parse($fecha_inicio); //convierte la fecha inicial
        $mesIni = $fechaIni->monthName; //obtengo el mes de la fecha inicial


        if($fecha_inicio!=null && $fecha_final!=null){

        if($fecha_final < $fecha_inicio){

            flash("¡ERROR! ¡La fecha final no puede ser menor que la fecha de inicio!")->error()->important();
            return redirect()->action('Marcaciones\ReporteController@buscarReporteAusencia');
        }
        if($fecha_inicio > $date){

             flash("¡ERROR! ¡La fecha inicio no puede ser mayor que la fecha de actual!")->error()->important();
            return redirect()->action('Marcaciones\ReporteController@buscarReporteAusencia');
        }
        if($fecha_final > $date){

             flash("¡ERROR! ¡La fecha final no puede ser mayor que la fecha actual!")->error()->important();
            return redirect()->action('Marcaciones\ReporteController@buscarReporteAusencia');
        }
    }

       $marcacion = DB::table('marcaciones_ausencia_vw')->select('codigo_empleado', 'nombre','apellido', 'oficina','fecha','mes', 'salario','oficina','tipo_plaza')->whereBetween('fecha', [$fecha_inicio, $fecha_final])->whereNull('fecha_marcacion')->whereNull('fecha_asueto')->whereNull('fecha_permiso')->whereRaw('WEEKDAY(fecha) <> 5')->whereRaw('WEEKDAY(fecha) <> 6')->get();



       $vacio="Existe";
    

    
        $noMarcacion= $marcacion->isEmpty();
                  
        if($noMarcacion == true)
                {
                flash('¡No se tiene dicha información en el sistema!')->error()->important();
                return redirect()->action('Marcaciones\ReporteController@buscarReporteAusencia');
                }


        return view('marcacion.reporteMarcacion.verAusencias', compact('marcacion', 'vacio', 'fecha_inicio', 'fecha_final', 'dia', 'mesFin', 'mesIni', 'hora', 'date')); 
       


}

    public function faltaMarcacionAusenciaPDF(Request $request){

        Carbon::setLocale('es_SV');
        $fecha = Carbon::now(new \DateTimeZone('America/El_Salvador'));
        $hora= $fecha->format('h:i:s A'); 
        $date = $fecha->format('Y-m-d');
        $dia = $fecha->format('d-m-Y');
         $mes = $fecha->monthName;
        $anio = $fecha->year;
         //fin de Fecha Actual
        $fecha_inicio = $request->input('fecha_ini');
        $fecha_final = $request->input('fecha_fin');

        $fechaFin = Carbon::parse($fecha_final); //convierte la fecha final
        $mesFin = $fechaFin->monthName; //obtengo el mes de la fecha fin

        $fechaIni = Carbon::parse($fecha_inicio); //convierte la fecha inicial
        $mesIni = $fechaIni->monthName; 

     $marcacion = DB::table('marcaciones_ausencia_vw')->select('codigo_empleado', 'nombre','apellido', 'oficina','fecha','mes', 'salario','oficina','tipo_plaza')->whereBetween('fecha', [$fecha_inicio, $fecha_final])->whereNull('fecha_marcacion')->whereNull('fecha_asueto')->whereNull('fecha_permiso')->whereRaw('WEEKDAY(fecha) <> 5')->whereRaw('WEEKDAY(fecha) <> 6')->get();



        $pdf = \PDF::loadView('marcacion.reporteMarcacion.imprimirAusencias', ['marcacion' => $marcacion, 'fecha_inicio' => $fecha_inicio, 'fecha_final' => $fecha_final], compact('dia', 'hora','mesIni', 'mesFin'))->setPaper('A4', 'landscape')->setWarnings(false);
         return $pdf->stream('ausenciaMarcacion.pdf');


    }


    public function faltaMarcacionAusenciaExcel(Request $request){

        $fecha_inicio = Carbon::parse($request->fecha_ini);
        $fecha_final = Carbon::parse($request->fecha_fin);
       $now = Carbon::now(new \DateTimeZone('America/El_Salvador'));
        return Excel::download(new MarcacionesExport($fecha_inicio,$fecha_final), 'marcaciones_ausencia'. $now .'.xlsx');

    }

    public function buscarMarcacionIntermedia(){
          $vacio = "";
        return view('marcacion.reporteMarcacion.verIntermedias', compact('vacio'));
    }

    public function reporteMarcacionIntermedia(Request $request){
         //Obtiene Fecha Actual
        Carbon::setLocale('es_SV');
        $fecha = Carbon::now(new \DateTimeZone('America/El_Salvador'));
        $hora= $fecha->format('h:i:s A'); 
        $date = $fecha->format('Y-m-d');
        $dia = $fecha->format('d-m-Y');
        $mes = $fecha->monthName;
        $anio = $fecha->year;
         //fin de Fecha Actual
        $fecha_inicio = $request->input('fecha_inicio');
        $fecha_final = $request->input('fecha_final');

        $fechaFin = Carbon::parse($fecha_final); //convierte la fecha final
        $mesFin = $fechaFin->monthName; //obtengo el mes de la fecha fin

        $fechaIni = Carbon::parse($fecha_inicio); //convierte la fecha inicial
        $mesIni = $fechaIni->monthName; //obtengo el mes de la fecha inicial


        if($fecha_inicio!=null && $fecha_final!=null){

        if($fecha_final < $fecha_inicio){

            flash("¡ERROR! ¡La fecha final no puede ser menor que la fecha de inicio!")->error()->important();
            return redirect()->action('Marcaciones\ReporteController@buscarMarcacionIntermedia');
        }
        if($fecha_inicio > $date){

             flash("¡ERROR! ¡La fecha inicio no puede ser mayor que la fecha de actual!")->error()->important();
            return redirect()->action('Marcaciones\ReporteController@buscarMarcacionIntermedia');
        }
        if($fecha_final > $date){

             flash("¡ERROR! ¡La fecha final no puede ser mayor que la fecha actual!")->error()->important();
            return redirect()->action('Marcaciones\ReporteController@buscarMarcacionIntermedia');
        }
    }//fin del if principal

    $marcacionInter = DB::table('marcacion_intermedias_vw as m')->whereBetween('m.fecha_marcacion', [$fecha_inicio, $fecha_final])->get();


       $marcacionIntermedia = DB::table('mar_marcacion as m')->select(DB::raw('COUNT(m.empleado_id) as total'), DB::raw('MAX(m.hora) as max'),DB::raw('MIN(m.hora) as min'),'m.empleado_id as empleado', 'm.fecha')->whereBetween('m.fecha', [$fecha_inicio, $fecha_final])->groupBy('m.fecha', 'm.empleado_id')->havingRaw('COUNT(m.empleado_id) = ?', [4])->get();


        $vacio="Existe";

        $noMarcacion= $marcacionInter->isEmpty();   
        $noFalta = $marcacionIntermedia->isEmpty();            
        if($noMarcacion == true && $noFalta== true)
                {
                flash('¡No se tiene dicha información en el sistema!')->error()->important();
                return redirect()->action('Marcaciones\ReporteController@buscarMarcacionIntermedia');
                }

        foreach ($marcacionInter as $marc) {
            foreach ($marcacionIntermedia as $marcaciones) {
                if($marc->id_empleado ==$marcaciones->empleado && $marc->fecha_marcacion ==$marcaciones->fecha){
                    if($marc->hora_marcacion != $marcaciones->max && $marc->hora_marcacion != $marcaciones->min){
                       $intermedias[] = [$marc->codigo_empleado, $marc->nombre, $marc->apellido, $marc->oficina, $marc->fecha_marcacion, $marc->tipo, $marc->salario, $marc->mes, $marc->hora_marcacion]; 
                    }
                }
            }
        }

       foreach($intermedias as $key => $marc){
        foreach ($intermedias as $key => $marcacion) {
            if($marc[0]==$marcacion[0] && $marc[4]==$marcacion[4]){
            if($marc[8]<$marcacion[8]){
                $codigo_empleado = $marc[0];
                $nombre = $marc[1];
                $apellido=$marc[2];
                $oficina=$marc[3];
                $fecha_marcacion=$marc[4];
                $tipo=$marc[5];
                $salario=$marc[6];
                $mes=$marc[7];
                $hora_marcacion=$marc[8];
                $intermediaMin[] = [$codigo_empleado,$nombre,$apellido,$oficina,$fecha_marcacion,$tipo,$salario,$mes,$hora_marcacion];
            }
        }
        } 

}

  foreach($intermedias as $key => $marc){
        foreach ($intermedias as $key => $marcacion) {
            if($marc[0]==$marcacion[0] && $marc[4]==$marcacion[4]){
            if($marc[8]<$marcacion[8]){
                $codigo_empleado = $marcacion[0];
                $nombre = $marcacion[1];
                $apellido=$marcacion[2];
                $oficina=$marcacion[3];
                $fecha_marcacion=$marcacion[4];
                $tipo=$marcacion[5];
                $salario=$marcacion[6];
                $mes=$marcacion[7];
                $hora_marcacion=$marcacion[8];
                $intermediaMax[] = [$codigo_empleado,$nombre,$apellido,$oficina,$fecha_marcacion,$tipo,$salario,$mes,$hora_marcacion];
            }
        }
        } 

}
              
        return view('marcacion.reporteMarcacion.verIntermedias',compact('intermediaMin','intermediaMax' ,'vacio', 'fecha_inicio', 'fecha_final', 'dia', 'mesFin', 'mesIni', 'hora','date')); 
       
    }


    public function marcacionIntermediaPDF(Request $request){
          //Obtiene Fecha Actual
        Carbon::setLocale('es_SV');
        $fecha = Carbon::now(new \DateTimeZone('America/El_Salvador'));
        $hora= $fecha->format('h:i:s A'); 
        $date = $fecha->format('Y-m-d');
        $dia = $fecha->format('d-m-Y');
        $mes = $fecha->monthName;
        $anio = $fecha->year;
         //fin de Fecha Actual
        $fecha_inicio = $request->input('fecha_ini');
        $fecha_final = $request->input('fecha_fin');

        $fechaFin = Carbon::parse($fecha_final); //convierte la fecha final
        $mesFin = $fechaFin->monthName; //obtengo el mes de la fecha fin

        $fechaIni = Carbon::parse($fecha_inicio); //convierte la fecha inicial
        $mesIni = $fechaIni->monthName; //obtengo el mes de la fecha inicial

    $marcacionInter = DB::table('marcacion_intermedias_vw as m')->whereBetween('m.fecha_marcacion', [$fecha_inicio, $fecha_final])->get();


       $marcacionIntermedia = DB::table('mar_marcacion as m')->select(DB::raw('COUNT(m.empleado_id) as total'), DB::raw('MAX(m.hora) as max'),DB::raw('MIN(m.hora) as min'),'m.empleado_id as empleado', 'm.fecha')->whereBetween('m.fecha', [$fecha_inicio, $fecha_final])->groupBy('m.fecha', 'm.empleado_id')->havingRaw('COUNT(m.empleado_id) = ?', [4])->get();
         foreach ($marcacionInter as $marc) {
            foreach ($marcacionIntermedia as $marcaciones) {
                if($marc->id_empleado ==$marcaciones->empleado && $marc->fecha_marcacion ==$marcaciones->fecha){
                    if($marc->hora_marcacion != $marcaciones->max && $marc->hora_marcacion != $marcaciones->min){
                       $intermedias[] = [$marc->codigo_empleado, $marc->nombre, $marc->apellido, $marc->oficina, $marc->fecha_marcacion, $marc->tipo, $marc->salario, $marc->mes, $marc->hora_marcacion]; 
                    }
                }
            }
        }

       foreach($intermedias as $key => $marc){
        foreach ($intermedias as $key => $marcacion) {
            if($marc[0]==$marcacion[0] && $marc[4]==$marcacion[4]){
            if($marc[8]<$marcacion[8]){
                $codigo_empleado = $marc[0];
                $nombre = $marc[1];
                $apellido=$marc[2];
                $oficina=$marc[3];
                $fecha_marcacion=$marc[4];
                $tipo=$marc[5];
                $salario=$marc[6];
                $mes=$marc[7];
                $hora_marcacion=$marc[8];
                $intermediaMin[] = [$codigo_empleado,$nombre,$apellido,$oficina,$fecha_marcacion,$tipo,$salario,$mes,$hora_marcacion];
            }
        }
        } 

}

  foreach($intermedias as $key => $marc){
        foreach ($intermedias as $key => $marcacion) {
            if($marc[0]==$marcacion[0] && $marc[4]==$marcacion[4]){
            if($marc[8]<$marcacion[8]){
                $codigo_empleado = $marcacion[0];
                $nombre = $marcacion[1];
                $apellido=$marcacion[2];
                $oficina=$marcacion[3];
                $fecha_marcacion=$marcacion[4];
                $tipo=$marcacion[5];
                $salario=$marcacion[6];
                $mes=$marcacion[7];
                $hora_marcacion=$marcacion[8];
                $intermediaMax[] = [$codigo_empleado,$nombre,$apellido,$oficina,$fecha_marcacion,$tipo,$salario,$mes,$hora_marcacion];
            }
        }
        } 

}

      $pdf = \PDF::loadView('marcacion.reporteMarcacion.imprimirIntermedias', ['intermediaMax' => $intermediaMax, 'intermediaMin' => $intermediaMin, 'fecha_inicio' => $fecha_inicio, 'fecha_final' => $fecha_final], compact('dia', 'hora','mesIni', 'mesFin'))->setPaper('A4', 'landscape')->setWarnings(false);

         return $pdf->stream('intermedias.pdf');

    }


    public function marcacionIntermediaExcel(Request $request){
    $fecha_inicio = Carbon::parse($request->fecha_ini);
    $fecha_final = Carbon::parse($request->fecha_fin);
    $fechaFin =$fecha_final->format('Y-m-d');
    $fechaInicio=$fecha_inicio->format('Y-m-d');
    $now = Carbon::now(new \DateTimeZone('America/El_Salvador'));
        return Excel::download(new MarcacionIntermediasExport($fechaInicio,$fechaFin), 'marcaciones_intermedias'. $now .'.xlsx');

    }


    public function verReportes(){
        return view('marcacion.reporteMarcacion.verReportes');
    }



}
    


