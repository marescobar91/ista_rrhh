<?php

namespace App\Http\Controllers\Viaticos;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Modelos\Viatico\Viatico;
use App\Modelos\Viatico\Periodo;
use App\Modelos\Viatico\ProgramacionDetalle;
use App\Modelos\Viatico\Programacion;
use App\Modelos\Empleado\Centro;
use DB;
use Auth;
use Carbon\Carbon;
use RealRashid\SweetAlert\Facades\Alert;


class LiquidacionesController extends Controller
{
    public function __construct(){

    
    $this->middleware(['permission:crear.liquidacion'], ['only'   => ['crearLiquidacion', 'guardarLiquidacion']]);
    $this->middleware(['permission:consultar.liquidacion'], ['only'   => 'listarLiquidacion']); 
    $this->middleware(['permission:buscar.liquidacion'], ['only'   => 'buscarLiquidacion']); 

     }


  public function listarLiquidacion(){
    if(Auth::user()->hasRole('Gestor de Caja Chica')){   
      $viatico = DB::table('via_viatico as v')->leftJoin('via_programacion as p', 'p.viatico_id', '=', 'v.id_viatico')->leftJoin('emp_centro as c', 'c.id_centro', 'v.centro_id')->select('v.id_viatico','c.centro','v.referencia', 'v.referencia_ufi', 'v.fecha_inicio', 'v.fecha_final', DB::raw('SUM(p.pago_viatico) as pago'),'v.estado')->whereBetween('v.estado', [2,3])->groupBy('v.id_viatico','c.centro','v.referencia', 'v.referencia_ufi','v.estado', 'v.fecha_inicio', 'v.fecha_final')->whereNotIn('c.id_centro',[1,3])->get(); //obtiene todos los viaticos con referencia distinta
    }elseif (Auth::user()->hasRole('Gestor de Fondo Circulante')) {
     $viatico = DB::table('via_viatico as v')->leftJoin('via_programacion as p', 'p.viatico_id', '=', 'v.id_viatico')->leftJoin('emp_centro as c', 'c.id_centro', 'v.centro_id')->select('v.id_viatico','c.centro','v.referencia', 'v.referencia_ufi', 'v.fecha_inicio', 'v.fecha_final', DB::raw('SUM(p.pago_viatico) as pago'),'v.estado')->whereBetween('v.estado', [2,3])->groupBy('v.id_viatico','c.centro','v.referencia', 'v.referencia_ufi','v.estado', 'v.fecha_inicio', 'v.fecha_final')->whereIn('c.id_centro',[1,3])->get();
    }else{
      $viatico = DB::table('via_viatico as v')->leftJoin('via_programacion as p', 'p.viatico_id', '=', 'v.id_viatico')->leftJoin('emp_centro as c', 'c.id_centro', 'v.centro_id')->select('v.id_viatico','c.centro','v.referencia', 'v.referencia_ufi', 'v.fecha_inicio', 'v.fecha_final', DB::raw('SUM(p.pago_viatico) as pago'),'v.estado')->whereBetween('v.estado', [2,3])->groupBy('v.id_viatico','c.centro','v.referencia', 'v.referencia_ufi','v.estado', 'v.fecha_inicio', 'v.fecha_final')->get();
    }


    return view('viaticos.gestionLiquidacion.listar', compact('viatico'));
  }

  public function crearLiquidacion(){

    if(Auth::user()->hasRole('Gestor de Caja Chica')){   
      $viatico = DB::table('via_viatico')->select(DB::raw('DISTINCT(referencia) as referencia'))->where('estado', 2)->whereIntegerInRaw('centro_id',[2,4,5,6])->whereNull('referencia_ufi')->get(); //obtiene todos los viaticos con referencia distinta
    }elseif (Auth::user()->hasRole('Gestor de Fondo Circulante')) {
      $viatico = DB::table('via_viatico')->select(DB::raw('DISTINCT(referencia) as referencia'))->where('estado', 2)->whereIntegerInRaw('centro_id',[1,3])->whereNull('referencia_ufi')->get();
    }else{
     $viatico = DB::table('via_viatico')->select(DB::raw('DISTINCT(referencia) as referencia'))->where('estado', 2)->whereNull('referencia_ufi')->get();
   }
   

   return view('viaticos.gestionLiquidacion.crear', compact('viatico'));
 }

 public function getReferencia(Request $request){
    if ($request->ajax()){//ajax para para consultar referencia
     $viatico = Viatico::where('referencia', $request->referencia)->first();
     $centro = $viatico->centro->centro;
     $referencia = $viatico->referencia;
     $programaciones = DB::table('informe_liquidaciones_vw')->select('id_programacion_detalle','referencia','codigo_empleado', 'nombre', 'apellido', 'fecha_programacion', 'pago_viatico', 'estado_programacion', 'centro')->where('referencia', $referencia)->where('estado', 2)->get(); //obtiene el detalle de tdoas las programaciones consultadas
      $total = DB::table('informe_liquidaciones_vw')->select(DB::raw('SUM(pago_viatico) as monto'))->where('referencia', $referencia)->where('estado', 2)->where('estado_programacion',1)->first();
      $anulado = DB::table('informe_liquidaciones_vw')->select(DB::raw('SUM(pago_viatico) as monto'))->where('referencia', $referencia)->where('estado', 2)->where('estado_programacion',0)->first();

      if($centro=='OFICINA CENTRAL' || $centro=='CETIA II'){
      $pagadoCentral = DB::table('informe_liquidaciones_vw')->select(DB::raw('SUM(pago_viatico) as monto'))->where('referencia', $referencia)->where('estado', 2)->where('estado_programacion',1)->where('id_centro',1)->first();
      $pagadoCetiaDos = DB::table('informe_liquidaciones_vw')->select(DB::raw('SUM(pago_viatico) as monto'))->where('referencia', $referencia)->where('estado', 2)->where('estado_programacion',1)->where('id_centro',3)->first();
        $anuladoCentral = DB::table('informe_liquidaciones_vw')->select(DB::raw('SUM(pago_viatico) as monto'))->where('referencia', $referencia)->where('estado', 2)->where('estado_programacion',0)->where('id_centro',1)->first();
        $anuladoCetiaDos = DB::table('informe_liquidaciones_vw')->select(DB::raw('SUM(pago_viatico) as monto'))->where('referencia', $referencia)->where('estado', 2)->where('estado_programacion',0)->where('id_centro',3)->first();
        $listadoCentros=Centro::where('estado',1)->whereIn('id_centro',[1,3])->get();

      }else{
        $subtotalPagado = DB::table('informe_liquidaciones_vw')->select(DB::raw('SUM(pago_viatico) as monto'))->where('referencia', $referencia)->where('estado', 2)->where('estado_programacion',1)->where('centro', $centro)->first();
        $subtotalAnulado = DB::table('informe_liquidaciones_vw')->select(DB::raw('SUM(pago_viatico) as monto'))->where('referencia', $referencia)->where('estado', 2)->where('estado_programacion',0)->where('centro', $centro)->first();
         $listadoCentros=Centro::where('estado',1)->where('centro',$centro)->get();
      }

     $referencia = DB::table('via_viatico as v')->leftJoin('via_programacion as p', 'p.viatico_id', '=', 'v.id_viatico')->leftJoin('via_forma_pago as f', 'f.id_forma_pago', '=', 'v.forma_pago_id')->select('v.referencia', 'v.estado', 'f.forma_pago', DB::raw('SUBSTRING(f.forma_pago,1,2) as inicial'), 'v.fecha_inicio', 'v.fecha_final')->groupBy('v.referencia', 'v.estado', 'f.forma_pago', 'v.fecha_inicio', 'v.fecha_final')->where('referencia', $request->referencia)->where('v.estado', 2)->get();
     if($referencia->isEmpty()){
      $arrayViatico=null;
    }
    else
    {
      foreach($referencia as $viatico){

        $arrayViatico['referencia'] = $viatico->referencia;
        if($viatico->estado==2){
         $arrayViatico['estado'] = "Cerrado"; 
       }

       $arrayViatico['forma_pago'] = $viatico->forma_pago;
       $arrayViatico['iniciales'] = $viatico->inicial;
       $arrayViatico['fecha_desde'] = $viatico->fecha_inicio;
       $arrayViatico['fecha_hasta'] = $viatico->fecha_final;
       if($centro=='CETIA II' || $centro=='OFICINA CENTRAL'){
         $arrayViatico['centro'] = 'OFICINA CENTRAL Y CETIA II';
       }else{
         $arrayViatico['centro'] = $centro;
       }

     }
     $i=0;
     $detalleProgramacion=[];

     foreach($programaciones as $programacion){
      $det['id'] = $programacion->id_programacion_detalle;
      $det['referencia']=$programacion->referencia;
      $det['codigo_empleado']=$programacion->codigo_empleado;
      $det['nombre']=$programacion->nombre . ' '. $programacion->apellido;
      $det['fecha_programacion']=date('d-m-Y', strtotime($programacion->fecha_programacion));
      if($programacion->estado_programacion==1){
      $det['pago_viatico']=$programacion->pago_viatico;
      }elseif ($programacion->estado_programacion==0) {
      $det['pago_viatico']=0.00;
      }
      if($programacion->estado_programacion==1){
       $det['anulado']=0.00;
      }elseif ($programacion->estado_programacion==0) {
       $det['anulado']=$programacion->pago_viatico;
      }
     
      $det['estado']=$programacion->estado_programacion;
      $detalleProgramacion[$i]=$det;
      $i++;
    }

    $p=0;
    $detalleSubtotal=[];
    foreach($listadoCentros as $listado){

      if($listado->centro=='OFICINA CENTRAL'){
      $det['centro']= 'OFICINA CENTRAL';
      if($pagadoCentral->monto!=null){
        $det['pagado']=  $pagadoCentral->monto;
      }else{
        $det['pagado']= 0.0;
      }
      if($anuladoCentral->monto!=null){
         $det['anulado']= $anuladoCentral->monto;
      }else{
         $det['anulado']= 0.0;
      }
      $detalleSubtotal[$p]=$det;
      $p++;
    }elseif($listado->centro=='CETIA II'){
        $det['centro']= 'CETIA II';
      if($pagadoCetiaDos->monto!=null){
        $det['pagado']=  $pagadoCetiaDos->monto;
      }else{
         $det['pagado']= 0.0;
      }
      if($anuladoCetiaDos->monto!=null){
         $det['anulado']= $anuladoCetiaDos->monto;
      }else{
         $det['anulado']= 0.0;
      }
      $detalleSubtotal[$p]=$det;
      $p++;
    }else{
      $det['centro']=$centro;
      if($subtotalPagado->monto!=null){
        $det['pagado']= $subtotalPagado->monto;
      }else{
        $det['pagado']= 0.0;
      }
      if($subtotalAnulado->monto!=null){
      $det['anulado']= $subtotalAnulado->monto;
      }else{
        $det['anulado']= 0.0;
      }
     $detalleSubtotal[$p]=$det;
     $p++;
  }//fin del if

    }//fin del foreach del centro
  
  
    
    $totales['total']=$total->monto;
    $anulados['anulado']=$anulado->monto;

    $arrayLiquidacion['viatico']=$arrayViatico;
    $arrayLiquidacion['programacion']=$detalleProgramacion;
    $arrayLiquidacion['totales']=$totales;
    $arrayLiquidacion['anulados']=$anulados;
    $arrayLiquidacion['pagado']=$detalleSubtotal;
   
  }
  return response()->json($arrayLiquidacion);
}       
}

public function liquidadoCerrado(Request $request, $id){
    $viaticos = Viatico::find($id);
    $viaticos->estado=3;
    $viaticos->update();
    if(Auth::user()->hasRole('Gestor de Caja Chica')){   
      $viatico = DB::table('via_viatico as v')->leftJoin('via_programacion as p', 'p.viatico_id', '=', 'v.id_viatico')->leftJoin('emp_centro as c', 'c.id_centro', 'v.centro_id')->select('v.id_viatico','c.centro','v.referencia', 'v.referencia_ufi', 'v.fecha_inicio', 'v.fecha_final', DB::raw('SUM(p.pago_viatico) as pago'),'v.estado')->whereBetween('v.estado', [2,3])->groupBy('v.id_viatico','c.centro','v.referencia', 'v.referencia_ufi','v.estado', 'v.fecha_inicio', 'v.fecha_final')->whereNotIn('c.id_centro',[1,3])->get(); //obtiene todos los viaticos con referencia distinta
    }elseif (Auth::user()->hasRole('Gestor de Fondo Circulante')) {
     $viatico = DB::table('via_viatico as v')->leftJoin('via_programacion as p', 'p.viatico_id', '=', 'v.id_viatico')->leftJoin('emp_centro as c', 'c.id_centro', 'v.centro_id')->select('v.id_viatico','c.centro','v.referencia', 'v.referencia_ufi', 'v.fecha_inicio', 'v.fecha_final', DB::raw('SUM(p.pago_viatico) as pago'),'v.estado')->whereBetween('v.estado', [2,3])->groupBy('v.id_viatico','c.centro','v.referencia', 'v.referencia_ufi','v.estado', 'v.fecha_inicio', 'v.fecha_final')->whereIn('c.id_centro',[1,3])->get();
    }else{
      $viatico = DB::table('via_viatico as v')->leftJoin('via_programacion as p', 'p.viatico_id', '=', 'v.id_viatico')->leftJoin('emp_centro as c', 'c.id_centro', 'v.centro_id')->select('v.id_viatico','c.centro','v.referencia', 'v.referencia_ufi', 'v.fecha_inicio', 'v.fecha_final', DB::raw('SUM(p.pago_viatico) as pago'),'v.estado')->whereBetween('v.estado', [2,3])->groupBy('v.id_viatico','c.centro','v.referencia', 'v.referencia_ufi','v.estado', 'v.fecha_inicio', 'v.fecha_final')->get();
    }


    return view('viaticos.gestionLiquidacion.listar', compact('viatico'));

}

public function cerradoLiquidado(Request $request , $id){
$viaticos = Viatico::find($id);
    $viaticos->estado=2;
    $viaticos->update();

   if(Auth::user()->hasRole('Gestor de Caja Chica')){   
      $viatico = DB::table('via_viatico as v')->leftJoin('via_programacion as p', 'p.viatico_id', '=', 'v.id_viatico')->leftJoin('emp_centro as c', 'c.id_centro', 'v.centro_id')->select('v.id_viatico','c.centro','v.referencia', 'v.referencia_ufi', 'v.fecha_inicio', 'v.fecha_final', DB::raw('SUM(p.pago_viatico) as pago'),'v.estado')->whereBetween('v.estado', [2,3])->groupBy('v.id_viatico','c.centro','v.referencia', 'v.referencia_ufi','v.estado', 'v.fecha_inicio', 'v.fecha_final')->whereNotIn('c.id_centro',[1,3])->get(); //obtiene todos los viaticos con referencia distinta
    }elseif (Auth::user()->hasRole('Gestor de Fondo Circulante')) {
     $viatico = DB::table('via_viatico as v')->leftJoin('via_programacion as p', 'p.viatico_id', '=', 'v.id_viatico')->leftJoin('emp_centro as c', 'c.id_centro', 'v.centro_id')->select('v.id_viatico','c.centro','v.referencia', 'v.referencia_ufi', 'v.fecha_inicio', 'v.fecha_final', DB::raw('SUM(p.pago_viatico) as pago'),'v.estado')->whereBetween('v.estado', [2,3])->groupBy('v.id_viatico','c.centro','v.referencia', 'v.referencia_ufi','v.estado', 'v.fecha_inicio', 'v.fecha_final')->whereIn('c.id_centro',[1,3])->get();
    }else{
      $viatico = DB::table('via_viatico as v')->leftJoin('via_programacion as p', 'p.viatico_id', '=', 'v.id_viatico')->leftJoin('emp_centro as c', 'c.id_centro', 'v.centro_id')->select('v.id_viatico','c.centro','v.referencia', 'v.referencia_ufi', 'v.fecha_inicio', 'v.fecha_final', DB::raw('SUM(p.pago_viatico) as pago'),'v.estado')->whereBetween('v.estado', [2,3])->groupBy('v.id_viatico','c.centro','v.referencia', 'v.referencia_ufi','v.estado', 'v.fecha_inicio', 'v.fecha_final')->get();
    }


    return view('viaticos.gestionLiquidacion.listar', compact('viatico'));
}


public function buscarProgramaciones(Request $request){

 	$referencia = $request->referencia; //buscar programaciones
 	$fecha_inicio = $request->fecha_desde;
 	$fecha_final = $request->fecha_hasta;
  $formaPago = $request->forma_pago;
  $estado = $request->estado;
  $centros = $request->centro;
  $hoy = Carbon::now(new \DateTimeZone('America/El_Salvador'));
  $fechaActual = $hoy->format('Y-m-d');
  $anio = $hoy->year;
  $listadoViaticos =Viatico::where('referencia', $referencia)->first();
  $centro=$listadoViaticos->centro->centro;


  $viatico = DB::table('via_viatico as v')->leftJoin('via_programacion as p', 'p.viatico_id', '=', 'v.id_viatico')->leftJoin('via_forma_pago as f', 'f.id_forma_pago', '=', 'v.forma_pago_id')->select('v.referencia', 'v.estado', 'f.forma_pago', DB::raw('SUBSTRING(f.forma_pago,1,2) as inicial'), 'v.fecha_inicio', 'v.fecha_final')->groupBy('v.referencia', 'v.estado', 'f.forma_pago', 'v.fecha_inicio', 'v.fecha_final')->where('referencia', $referencia)->where('v.estado', 2)->get();

  $programaciones = DB::table('informe_liquidaciones_vw')->select('referencia','codigo_empleado', 'nombre', 'apellido', 'fecha_programacion', 'pago_viatico', 'estado_programacion')->where('referencia', $referencia)->whereBetween('fecha_programacion', [$fecha_inicio, $fecha_final])->get();

  $totalPago = DB::table('informe_liquidaciones_vw')->select(DB::raw('SUM(pago_viatico) as pago'))->where('referencia', $referencia)->whereBetween('fecha_programacion', [$fecha_inicio, $fecha_final])->where('estado_programacion', 1)->first();
  $totalRemesa = DB::table('informe_liquidaciones_vw')->select(DB::raw('SUM(pago_viatico) as remesa'))->where('referencia', $referencia)->whereBetween('fecha_programacion', [$fecha_inicio, $fecha_final])->where('estado_programacion', 0)->first();
  
  if($totalRemesa->remesa==null){
    $remesa=0.0;
  }else{
    $remesa=$totalRemesa->remesa;
  }

  if($centro=='OFICINA CENTRAL' || $centro=='CETIA II'){
      $pagadoCentral = DB::table('informe_liquidaciones_vw')->select(DB::raw('SUM(pago_viatico) as monto'))->where('referencia', $referencia)->where('estado', 2)->where('estado_programacion',1)->where('id_centro',1)->first();
      $pagadoCetiaDos = DB::table('informe_liquidaciones_vw')->select(DB::raw('SUM(pago_viatico) as monto'))->where('referencia', $referencia)->where('estado', 2)->where('estado_programacion',1)->where('id_centro',3)->first();
        $anuladoCentral = DB::table('informe_liquidaciones_vw')->select(DB::raw('SUM(pago_viatico) as monto'))->where('referencia', $referencia)->where('estado', 2)->where('estado_programacion',0)->where('id_centro',1)->first();
        $anuladoCetiaDos = DB::table('informe_liquidaciones_vw')->select(DB::raw('SUM(pago_viatico) as monto'))->where('referencia', $referencia)->where('estado', 2)->where('estado_programacion',0)->where('id_centro',3)->first();
        $listadoCentros=Centro::where('estado',1)->whereIn('id_centro',[1,3])->get();

      }else{
        $subtotalPagado = DB::table('informe_liquidaciones_vw')->select(DB::raw('SUM(pago_viatico) as monto'))->where('referencia', $referencia)->where('estado', 2)->where('estado_programacion',1)->where('centro', $centro)->first();
        $subtotalAnulado = DB::table('informe_liquidaciones_vw')->select(DB::raw('SUM(pago_viatico) as monto'))->where('referencia', $referencia)->where('estado', 2)->where('estado_programacion',0)->where('centro', $centro)->first();
         $listadoCentros=Centro::where('estado',1)->where('centro',$centro)->get();
      }


    $p=0;
    $detalleSubtotal=[];
    foreach($listadoCentros as $listado){

      if($listado->centro=='OFICINA CENTRAL'){
      $det['centro']= 'OFICINA CENTRAL';
      if($pagadoCentral->monto!=null){
        $det['pagado']=  $pagadoCentral->monto;
      }else{
        $det['pagado']= 0.0;
      }
      if($anuladoCentral->monto!=null){
         $det['anulado']= $anuladoCentral->monto;
      }else{
         $det['anulado']= 0.0;
      }
      $detalleSubtotal[$p]=$det;
      $p++;
    }elseif($listado->centro=='CETIA II'){
        $det['centro']= 'CETIA II';
      if($pagadoCetiaDos->monto!=null){
        $det['pagado']=  $pagadoCetiaDos->monto;
      }else{
         $det['pagado']= 0.0;
      }
      if($anuladoCetiaDos->monto!=null){
         $det['anulado']= $anuladoCetiaDos->monto;
      }else{
         $det['anulado']= 0.0;
      }
      $detalleSubtotal[$p]=$det;
      $p++;
    }else{
      $det['centro']=$centro;
      if($subtotalPagado->monto!=null){
        $det['pagado']= $subtotalPagado->monto;
      }else{
        $det['pagado']= 0.0;
      }
      if($subtotalAnulado->monto!=null){
      $det['anulado']= $subtotalAnulado->monto;
      }else{
        $det['anulado']= 0.0;
      }
     $detalleSubtotal[$p]=$det;
     $p++;
  }//fin del if

    }//fin del foreach del centro




  $viaticos = DB::table('via_viatico')->where('referencia', $referencia)->get();
  foreach ($viaticos as $viatico) {
    $arrayCentro[] = $viatico->centro_id;
  }


  $periodo = DB::table('via_periodo')->select(DB::raw('DISTINCT(mes) as mes'), 'anio')->whereIntegerInRaw('centro_id', $arrayCentro)->where('estado', 1)->first();
  $vacio="Existe";
  
  
  return view('viaticos.gestionLiquidacion.liquidar', compact('programaciones', 'periodo', 'anio', 'referencia', 'fecha_inicio', 'fecha_final', 'formaPago', 'estado', 'centro', 'totalPago', 'remesa', 'detalleSubtotal'));



}


public function anularProgramaciones(request $request, $id){
  if($request->ajax()){
    $programacionDetalle=ProgramacionDetalle::find($id);
    $referencia=$request->referencia;
    if($programacionDetalle->estado==1){
        $programacionDetalle->estado=0;
        $programacionDetalle->update();
        $programacion = Programacion::find($programacionDetalle->programacion_id);
        $programacion->pago_viatico =$programacion->pago_viatico - $programacionDetalle->pago_viatico;
        $programacion->save();
        $mensaje="Anulado con exito";
        $titulo="Viatico";
        
    }else{
      $programacionDetalle->estado=1;
      $programacionDetalle->update();
      $programacion = Programacion::find($programacionDetalle->programacion_id);
      $programacion->pago_viatico =$programacion->pago_viatico + $programacionDetalle->pago_viatico;
      $programacion->save();
      $mensaje="Habilitado con exito";
      $titulo="Viatico";
    }

    $viatico = Viatico::where('referencia', $referencia)->first();
    $centro = $viatico->centro->centro;
    $programaciones = DB::table('informe_liquidaciones_vw')->select('id_programacion_detalle','referencia','codigo_empleado', 'nombre', 'apellido', 'fecha_programacion', 'pago_viatico', 'estado_programacion')->where('referencia', $referencia)->where('estado', 2)->get(); //obtiene el detalle de tdoas las programaciones consultadas
    $total = DB::table('informe_liquidaciones_vw')->select(DB::raw('SUM(pago_viatico) as monto'))->where('referencia', $referencia)->where('estado', 2)->where('estado_programacion',1)->first();
    $anulado = DB::table('informe_liquidaciones_vw')->select(DB::raw('SUM(pago_viatico) as monto'))->where('referencia', $referencia)->where('estado', 2)->where('estado_programacion',0)->first();
   
      if($centro=='OFICINA CENTRAL' || $centro=='CETIA II'){
      $pagadoCentral = DB::table('informe_liquidaciones_vw')->select(DB::raw('SUM(pago_viatico) as monto'))->where('referencia', $referencia)->where('estado', 2)->where('estado_programacion',1)->where('id_centro',1)->first();
      $pagadoCetiaDos = DB::table('informe_liquidaciones_vw')->select(DB::raw('SUM(pago_viatico) as monto'))->where('referencia', $referencia)->where('estado', 2)->where('estado_programacion',1)->where('id_centro',3)->first();
        $anuladoCentral = DB::table('informe_liquidaciones_vw')->select(DB::raw('SUM(pago_viatico) as monto'))->where('referencia', $referencia)->where('estado', 2)->where('estado_programacion',0)->where('id_centro',1)->first();
        $anuladoCetiaDos = DB::table('informe_liquidaciones_vw')->select(DB::raw('SUM(pago_viatico) as monto'))->where('referencia', $referencia)->where('estado', 2)->where('estado_programacion',0)->where('id_centro',3)->first();
        $listadoCentros=Centro::where('estado',1)->whereIn('id_centro',[1,3])->get();

      }else{
        $subtotalPagado = DB::table('informe_liquidaciones_vw')->select(DB::raw('SUM(pago_viatico) as monto'))->where('referencia', $referencia)->where('estado', 2)->where('estado_programacion',1)->where('centro', $centro)->first();
        $subtotalAnulado = DB::table('informe_liquidaciones_vw')->select(DB::raw('SUM(pago_viatico) as monto'))->where('referencia', $referencia)->where('estado', 2)->where('estado_programacion',0)->where('centro', $centro)->first();
         $listadoCentros=Centro::where('estado',1)->where('centro',$centro)->get();
      }
   
        $i=0;
        $detalleProgramacion=[];
        foreach($programaciones as $programacion){
          $det['id'] = $programacion->id_programacion_detalle;
          $det['referencia']=$programacion->referencia;
          $det['codigo_empleado']=$programacion->codigo_empleado;
          $det['nombre']=$programacion->nombre . ' '. $programacion->apellido;

          $det['fecha_programacion']= date('d-m-Y', strtotime($programacion->fecha_programacion));
          if($programacion->estado_programacion==1){
          $det['pago_viatico']=$programacion->pago_viatico;
          }elseif ($programacion->estado_programacion==0) {
          $det['pago_viatico']=0.00;
          }
          if($programacion->estado_programacion==1){
          $det['anulado']=0.00;
          }elseif ($programacion->estado_programacion==0) {
          $det['anulado']=$programacion->pago_viatico;
          }
        
          $det['estado']=$programacion->estado_programacion;
          $detalleProgramacion[$i]=$det;
          $i++;
        }

         $p=0;
    $detalleSubtotal=[];
    foreach($listadoCentros as $listado){

      if($listado->centro=='OFICINA CENTRAL'){
      $det['centro']= 'OFICINA CENTRAL';
      if($pagadoCentral->monto!=null){
        $det['pagado']=  $pagadoCentral->monto;
      }else{
        $det['pagado']= 0.0;
      }
      if($anuladoCentral->monto!=null){
         $det['anulado']= $anuladoCentral->monto;
      }else{
         $det['anulado']= 0.0;
      }
      $detalleSubtotal[$p]=$det;
      $p++;
    }elseif($listado->centro=='CETIA II'){
        $det['centro']= 'CETIA II';
      if($pagadoCetiaDos->monto!=null){
        $det['pagado']=  $pagadoCetiaDos->monto;
      }else{
         $det['pagado']= 0.0;
      }
      if($anuladoCetiaDos->monto!=null){
         $det['anulado']= $anuladoCetiaDos->monto;
      }else{
         $det['anulado']= 0.0;
      }
      $detalleSubtotal[$p]=$det;
      $p++;
    }else{
      $det['centro']=$centro;
      if($subtotalPagado->monto!=null){
        $det['pagado']= $subtotalPagado->monto;
      }else{
        $det['pagado']= 0.0;
      }
      if($subtotalAnulado->monto!=null){
      $det['anulado']= $subtotalAnulado->monto;
      }else{
        $det['anulado']= 0.0;
      }
     $detalleSubtotal[$p]=$det;
     $p++;
  }//fin del if

    }//fin del foreach del centro
  


        $totales['total']=$total->monto;
        $anulados['anulado']=$anulado->monto;
        $arrayRef['referencia']=$referencia;

        $arrayMensaje['mensaje']=$mensaje;
        $arrayMensaje['titulo']=$titulo;

        $arrayLiquidacion['programacion']=$detalleProgramacion;
        $arrayLiquidacion['totales']=$totales;
        $arrayLiquidacion['ref']=$arrayRef;
        $arrayLiquidacion['mensajes']=$arrayMensaje;
        $arrayLiquidacion['anulados']=$anulados;
        $arrayLiquidacion['pagado']=$detalleSubtotal;
       
        return response()->json($arrayLiquidacion);
    
  }
}


public function guardarLiquidacion(Request $request){

  if($request->mes_periodo ==null){
   Alert::error('¡ERROR! No puede liquidar en periodo no establecido')->autoClose(1500)->iconHtml('<i class="fa fa-trash-o"></i>');
   return redirect()->action('Viaticos\LiquidacionesController@crearLiquidacion');
 }
 $referencia=$request->referencia;
 $hoy = Carbon::now(new \DateTimeZone('America/El_Salvador'));
 $fecha = $hoy->format('Y-m-d');
 $viatico = Viatico::where('referencia', $referencia)->get();
 foreach($viatico as $viatico){
  $viatico->referencia_ufi=$request->referencia_ufi;
  $viatico->cheque = $request->cheque;
  $viatico->fecha_cheque = $request->fecha_cheque;
  $viatico->cantidad_remesa = $request->cantidad_remesa_uno;
  $viatico->fecha_remesa = $request->fecha_remesa_uno;
  $viatico->cantidad_remesa_segundo = $request->cantidad_remesa_dos;
  $viatico->fecha_remesa_segundo = $request->fecha_remesa_dos;
  $viatico->folio = $request->folio_uno;
  $viatico->folio_segundo = $request->folio_dos;
  $viatico->fecha_liquidar = $fecha;
  $viatico->mes_liquidar = $request->mes_periodo;
  $viatico->anio_liquidar = $request->anio_periodo;
  $viatico->estado = 3;
  $viatico->update();
}

Alert::success('¡La programacion con', $referencia. ' fue liquidado exitosamente!')->autoClose(1000);
return redirect()->action('Viaticos\LiquidacionesController@crearLiquidacion');


}


public function mostrarLiquidacion($referencia){

  $viatico = Viatico::where('referencia', $referencia)->first();
  $centro = $viatico->centro->centro;
  $formaPago = $viatico->formaPago->forma_pago;
  if($viatico->estado==3){
   $estado="Pagado"; 
 }elseif($viatico->estado==2){
  $estado='Cerrado';
 }
 if($centro=='CETIA II' || $centro=='OFICINA CENTRAL'){
  $centros= 'OFICINA CENTRAL Y CETIA II';
}else{
  $centros = $centro;
}

$fechas = Viatico::where('referencia', $referencia)->whereIn('estado',[2,3])->first();

$listadoViatico = DB::table('informe_liquidaciones_vw')->select('referencia','codigo_empleado', 'nombre', 'apellido', 'fecha_programacion', 'pago_viatico', 'estado_programacion')->where('referencia', $referencia)->whereIn('estado',[2,3])->get();

$totalPago = DB::table('informe_liquidaciones_vw')->select(DB::raw('SUM(pago_viatico) as pago'))->where('referencia', $referencia)->whereIn('estado',[2,3])->where('estado_programacion', 1)->first();
$totalRemesa = DB::table('informe_liquidaciones_vw')->select(DB::raw('SUM(pago_viatico) as remesa'))->where('referencia', $referencia)->whereIn('estado',[2,3])->where('estado_programacion', 0)->first();

return view('viaticos.gestionLiquidacion.consultar', compact('listadoViatico', 'totalPago', 'totalRemesa', 'formaPago', 'estado', 'centros', 'viatico', 'fechas'));
}




public function buscarLiquidacion(){
  if(Auth::user()->hasRole('Gestor de Caja Chica')){   
      $viatico = DB::table('via_viatico')->select(DB::raw('DISTINCT(referencia) as referencia'))->where('estado', 3)->whereIntegerInRaw('centro_id',[2,4,5,6])->get(); //obtiene todos los viaticos con referencia distinta
    }elseif (Auth::user()->hasRole('Gestor de Fondo Circulante')) {
      $viatico = DB::table('via_viatico')->select(DB::raw('DISTINCT(referencia) as referencia'))->where('estado', 3)->whereIntegerInRaw('centro_id',[1,3])->get();
    }else{
     $viatico = DB::table('via_viatico')->select(DB::raw('DISTINCT(referencia) as referencia'))->where('estado', 3)->get();
   }
   return view('viaticos.gestionLiquidacion.buscar',compact('viatico'));
 }


 public function obtenerReferencia(Request $request){
    if ($request->ajax()){//ajax para para consultar referencia
      $viaticoActual = Viatico::where('referencia', $request->referencia)->first();
      $centro = $viaticoActual->centro->centro;
      $viatico = DB::table('via_viatico as v')->leftJoin('via_programacion as p', 'p.viatico_id', '=', 'v.id_viatico')->leftJoin('via_forma_pago as f', 'f.id_forma_pago', '=', 'v.forma_pago_id')->leftJoin('emp_centro as c', 'c.id_centro', '=', 'v.centro_id')->select('v.referencia', 'v.estado', 'f.forma_pago', 'v.fecha_inicio', 'v.fecha_final', 'c.centro', 'v.cheque', 'v.cantidad_remesa', 'v.fecha_cheque', 'v.folio')->groupBy('v.referencia', 'v.estado', 'f.forma_pago', 'c.centro', 'v.cheque', 'v.cantidad_remesa', 'v.fecha_cheque', 'v.folio', 'v.fecha_inicio', 'v.fecha_final')->where('referencia', $request->referencia)->where('v.estado', 3)->get();
      if($viatico->isEmpty()){
        $arrayViatico=null;
      }
      else
      {
        foreach($viatico as $viatico){

          $arrayViatico['referencia'] = $viatico->referencia;
          if($viatico->estado==3){
           $arrayViatico['estado'] = "Pagado"; 
         }
         $arrayViatico['forma_pago'] = $viatico->forma_pago;
         $arrayViatico['periodo'] = $viatico->fecha_inicio.' al '.$viatico->fecha_final;
         if($centro=='CETIA II' || $centro=='OFICINA CENTRAL'){
          $arrayViatico['centro'] = 'OFICINA CENTRAL Y CETIA II';
        }else{
          $arrayViatico['centro'] = $centro;
        }
        $arrayViatico['cheque'] = $viatico->cheque;
        $arrayViatico['cantidad_remesa'] = $viatico->cantidad_remesa;
        $arrayViatico['fecha_cheque'] = $viatico->fecha_cheque;
        $arrayViatico['folio'] = $viatico->folio;

      }
    }
    return response()->json($arrayViatico);
  }       
}

public function reporte(Request $request){
  $referencia = $request->referencia;
  $referencia_ufi = $request->numero_referencia;
  $fecha = Carbon::now(new \DateTimeZone('America/El_Salvador'));
  $estado = $request->estado;
  $hora= $fecha->format('h:i:s A'); 
  $date = $fecha->format('Y-m-d');
  $dia = $fecha->format('d-m-Y');
  $viaticos = Viatico::where('referencia', $referencia)->first();

      if($request->filp80 == 1){
        
        $filp80 = DB::table('filp80_liquidacion_vw')->select('codigo','nombre','apellido','fecha','valor')->where('referencia','=',$referencia)->where('estado_v','=',3)->where('estado_pd','=',$estado)->get();
    
        $totalPagados = DB::table('filp80_liquidacion_vw')->select(DB::raw('SUM(valor) as monto'))->where('referencia','=',$referencia)->where('estado_v','=',3)->where('estado_pd',1)->first();

        $totalAnulados = DB::table('filp80_liquidacion_vw')->select(DB::raw('SUM(valor) as monto'))->where('referencia','=',$referencia)->where('estado_v','=',3)->where('estado_pd',0)->first();
         $fecha_inicio = $viaticos->fecha_inicio;
         $fecha_final = $viaticos->fecha_final;
         $forma_pago =$viaticos->formaPago->forma_pago;

        if($estado==1){
       
        $pdf = \PDF::loadView('viaticos.gestionLiquidacion.filp80',compact('filp80','dia','hora','fecha_final','fecha_inicio','totalPagados','referencia','referencia_ufi','estado','forma_pago', 'totalAnulados'));   
        return $pdf->stream('filp80.pdf');
        }
        elseif ($estado==0) {
        $pdf = \PDF::loadView('viaticos.gestionLiquidacion.filp80',compact('filp80','dia','hora','fecha_final','fecha_inicio','referencia','referencia_ufi','estado','forma_pago', 'totalAnulados'));   
        return $pdf->stream('filp80.pdf');
        }
        

      }
      elseif ($request->filp27 == 2) {
        $pagado = DB::table('filp27_liquidacion_vw')->select(DB::raw('SUM(pago) as pago'))->where('referencia','=',$referencia)->where('estado_v','=',3)->where('estado_pd','=',1)->first();

        $nulo= DB::table('filp27_liquidacion_vw')->select(DB::raw('SUM(pago) as pago'))->where('referencia','=',$referencia)->where('estado_v','=',3)->where('estado_pd','=',0)->first();
        if($nulo->pago == null){
          $nulo->pago = 0;
        }
        $fecha_inicio = $viaticos->fecha_inicio;

        $fecha_final = $viaticos->fecha_final;

        $centro = DB::table('filp27_liquidacion_vw')->select('centro')->where('referencia','=',$referencia)->where('estado_v','=', 3)->first();


        $cheque = DB::table('filp27_liquidacion_vw')->select('cheque')->where('referencia','=',$referencia)->where('estado_v','=', 3)->first();

        $pdf = \PDF::loadView('viaticos.gestionLiquidacion.filp27',compact('pagado','nulo','referencia_ufi','fecha_inicio','fecha_final','centro','cheque'));   
        return $pdf->stream('filp27.pdf');
      }

      elseif ($request->filp28 == 3) {
      

      $filp28 = DB::table('filp80_liquidacion_vw')->select('codigo','nombre','apellido','fecha','valor')->where('referencia','=',$referencia)->where('estado_v','=',3)->where('estado_pd','=',1)->get();


        $totalPagados = DB::table('filp80_liquidacion_vw')->select(DB::raw('SUM(valor) as monto'))->where('referencia','=',$referencia)->where('estado_v','=',3)->where('estado_pd','=',1)->first();

        $totalAnulados = DB::table('filp80_liquidacion_vw')->select(DB::raw('SUM(valor) as monto'))->where('referencia','=',$referencia)->where('estado_v','=',3)->where('estado_pd','=',0)->first();

       
        $fecha_inicio = $viaticos->fecha_inicio;

        $fecha_final = $viaticos->fecha_final;
        $forma_pago =$viaticos->formaPago->forma_pago;


        $pdf = \PDF::loadView('viaticos.gestionLiquidacion.filp28',compact('filp28','dia','hora','fecha_final','fecha_inicio','totalPagados','referencia','referencia_ufi','totalAnulados', 'forma_pago'));   
        return $pdf->stream('filp80.pdf');
      }

}





}
