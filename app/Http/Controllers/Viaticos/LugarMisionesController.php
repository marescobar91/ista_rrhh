<?php

namespace App\Http\Controllers\Viaticos;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Modelos\Viatico\LugarMision;
use App\Modelos\Empleado\Departamento;
use RealRashid\SweetAlert\Facades\Alert;

class LugarMisionesController extends Controller
{

    public function __construct(){

    $this->middleware(['permission:editar.catalogos'], ['only'   => ['editarLugarMision', 'actualizarLugarMision']]);
    $this->middleware(['permission:eliminar.catalogos'], ['only'   => ['eliminarLugarMision']]);
    $this->middleware(['permission:habilitar.catalogos'], ['only'   => ['habilitarLugarMision']]);
    $this->middleware(['permission:crear.catalogos'], ['only'   => ['crearLugarMision', 'guardarLugarMision']]);
    $this->middleware(['permission:consultar.catalogos'], ['only'   => 'listarLugarMision']); 
     }
     
    
    public function crearLugarMision(){

    	$departamento = Departamento::all();

    	return view('viaticos.gestionLugarMision.crear', compact('departamento'));


    }

    public function guardarLugarMision(Request $request){
    	$lugarMision = new LugarMision();
    	$lugarMision->departamento_id=$request->departamento;
    	$lugarMision->lugar_mision=$request->lugar_mision;
    	$lugarMision->kilometros=$request->kilometros;
    	$lugarMision->estado=1;
    	$lugarMision->save();
    	Alert::success('Registrar Lugar de Misión ', $lugarMision->lugar_mision . ' de forma existosa')->autoClose(2000);
    	return redirect()->action('Viaticos\LugarMisionesController@listarLugarMision');


    }

    public function listarLugarMision(){
    	$lugarMision = LugarMision::where('estado', '=',1)->get();
    	$lugarMisionDeshabilitado = LugarMision::where('estado', '=', 0)->get();

    	return view('viaticos.gestionLugarMision.listar', compact('lugarMision', 'lugarMisionDeshabilitado'));
    }

    public function editarLugarMision($id){
    	$departamento =Departamento::all();
    	$lugarMision = LugarMision::find($id);

    	return view('viaticos.gestionLugarMision.editar', compact('lugarMision', 'departamento'));
    }

    public function actualizarLugarMision(Request $request, $id){
    	$lugarMision=LugarMision::find($id);
    	$lugarMision->departamento_id=$request->departamento;
    	$lugarMision->lugar_mision=$request->lugar_mision;
    	$lugarMision->kilometros=$request->kilometros;
    	$lugarMision->estado=1;
    	$lugarMision->update();
    	Alert::warning('Actualizar Lugar de Mision ', $lugarMision->lugar_mision . ' de forma existosa')->autoClose(2000)->iconHtml('<i class="fa fa-pencil"></i>');
    	return redirect()->action('Viaticos\LugarMisionesController@listarLugarMision');
    }

    public function eliminarLugarMision($id){
    	$lugarMision=LugarMision::find($id);
    	$lugarMision->estado=0;
    	$lugarMision->update();
    	Alert::error('Eliminar Lugar de Misión ', $lugarMision->lugar_mision . ' de forma existosa')->autoClose(2000);
    	return redirect()->action('Viaticos\LugarMisionesController@listarLugarMision');

    }

    public function habilitarLugarMision($id){
    	$lugarMision=LugarMision::find($id);
    	$lugarMision->estado=1;
    	$lugarMision->update();
    	Alert::success('Registrar Lugar de Misión ', $lugarMision->lugar_mision . ' de forma existosa')->autoClose(2000);
    	return redirect()->action('Viaticos\LugarMisionesController@listarLugarMision');
    }



}

