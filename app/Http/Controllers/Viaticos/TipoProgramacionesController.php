<?php

namespace App\Http\Controllers\Viaticos;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Modelos\Viatico\TipoProgramacion;
use RealRashid\SweetAlert\Facades\Alert;

class TipoProgramacionesController extends Controller
{

  public function __construct(){

    $this->middleware(['permission:editar.catalogos'], ['only'   => ['editarTipoProgramacion', 'actualizarTipoProgramacion']]);
    $this->middleware(['permission:eliminar.catalogos'], ['only'   => ['eliminarTipoProgramacion']]);
    $this->middleware(['permission:habilitar.catalogos'], ['only'   => ['habilitarTipoProgramacion']]);
    $this->middleware(['permission:crear.catalogos'], ['only'   => ['crearTipoProgramacion', 'guardarTipoProgramacion']]);
    $this->middleware(['permission:consultar.catalogos'], ['only'   => 'listarTipoProgramacion']); 
     }
  
     public function crearTipoProgramacion(){

    	return view('viaticos.gestionTipoProgramacion.crear');
    }

    public function guardarTipoProgramacion(Request $request){

    	$tipoProgramacion = new TipoProgramacion();
    	$tipoProgramacion->tipo_programacion=$request->tipo_programacion;
    	$tipoProgramacion->estado=1;
    	$tipoProgramacion->save();
    	Alert::success('Registrar Tipo de Programacion ', $tipoProgramacion->tipo_programacion . ' de forma existosa')->autoClose(2000);
    	return redirect()->action('Viaticos\TipoProgramacionesController@listarTipoProgramacion');
    }

   public function listarTipoProgramacion(){

   	$tipoProgramacion = TipoProgramacion::where('estado', '=', 1)->get();
   	$tipoProgramacionDeshabilitado = TipoProgramacion::where('estado', '=', 0)->get();

   	return view('viaticos.gestionTipoProgramacion.listar', compact('tipoProgramacion', 'tipoProgramacionDeshabilitado'));
   }

   public function editarTipoProgramacion($id){

   	$tipoProgramacion = TipoProgramacion::find($id);
  

   return view('viaticos.gestionTipoProgramacion.editar', compact('tipoProgramacion'));
   }


   public function actualizarTipoProgramacion(Request $request, $id){

   	$tipoProgramacion = TipoProgramacion::find($id);
   	$tipoProgramacion->tipo_programacion=$request->tipo_programacion;
   	$tipoProgramacion->estado=1;
   	$tipoProgramacion->update();
   	Alert::warning('Actualizar Tipo de Programacion', $tipoProgramacion->tipo_programacion . ' de forma existosa')->autoClose(2000)->iconHtml('<i class="fa fa-pencil"></i>');
    	return redirect()->action('Viaticos\TipoProgramacionesController@listarTipoProgramacion');
   }

   public function eliminarTipoProgramacion($id){
   	$tipoProgramacion= TipoProgramacion::find($id);
   	$tipoProgramacion->estado =0;
   	$tipoProgramacion->update();
   	Alert::error('Eliminar Tipo de Programacion', $tipoProgramacion->tipo_programacion . ' de forma existosa')->autoClose(2000)->iconHtml('<i class="fa fa-trash-o"></i>');
	 return redirect()->action('Viaticos\TipoProgramacionesController@listarTipoProgramacion');

   }

   public function habilitarTipoProgramacion($id){

   	$tipoProgramacion= TipoProgramacion::find($id);
   	$tipoProgramacion->estado =1;
   	$tipoProgramacion->update();
   	Alert::success('Habilitar Tipo de Programacion', $tipoProgramacion->tipo_programacion . ' de forma existosa')->autoClose(2000);
	 return redirect()->action('Viaticos\TipoProgramacionesController@listarTipoProgramacion');
   }
}
