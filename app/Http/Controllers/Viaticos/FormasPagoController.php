<?php

namespace App\Http\Controllers\Viaticos;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Modelos\Viatico\FormaPago;
use RealRashid\SweetAlert\Facades\Alert;

class FormasPagoController extends Controller
{
    public function __construct(){

    $this->middleware(['permission:editar.catalogos'], ['only'   => ['editarFormaPago', 'actualizarFormaPago']]);
    $this->middleware(['permission:eliminar.catalogos'], ['only'   => ['eliminarFormaPago']]);
    $this->middleware(['permission:habilitar.catalogos'], ['only'   => ['habilitarFormaPago']]);
    $this->middleware(['permission:crear.catalogos'], ['only'   => ['crearFormaPago', 'guardarFormaPago']]);
    $this->middleware(['permission:consultar.catalogos'], ['only'   => 'listarFormaPago']); 
     }

    public function crearFormaPago(){

    	return view('viaticos.gestionFormaPago.crear');
    }

    public function guardarFormaPago(Request $request){
    	$formaPago = new FormaPago();
    	$formaPago->forma_pago=$request->forma_pago;
    	$formaPago->estado=1;
    	$formaPago->save();

    	Alert::success('Registrar Forma de Pago ', $formaPago->forma_pago . ' de forma existosa')->autoClose(2000);
    	return redirect()->action('Viaticos\FormasPagoController@listarFormaPago');

    }

    public function listarFormaPago(){

    	$formaPago = FormaPago::where('estado', '=', 1)->get();
    	$formaDeshabilitado = FormaPago::where('estado', '=', 0)->get();

    	return view('viaticos.gestionFormaPago.listar', compact('formaPago', 'formaDeshabilitado'));
    }

    public function editarFormaPago($id){
    	$formaPago = FormaPago::find($id);

    	return view('viaticos.gestionFormaPago.editar', compact('formaPago'));

    }

    public function actualizarFormaPago(Request $request, $id){

    	$formaPago= FormaPago::find($id);
    	$formaPago->forma_pago=$request->forma_pago;
    	$formaPago->estado = 1;
    	$formaPago->update();

    	Alert::warning('Actualizar Forma de Pago ', $formaPago->forma_pago . ' de forma existosa')->autoClose(2000)->iconHtml('<i class="fa fa-pencil"></i>');
    	return redirect()->action('Viaticos\FormasPagoController@listarFormaPago');
    }

    public function eliminarFormaPago($id){

    	$formaPago = FormaPago::find($id);
    	$formaPago->estado=0;
    	$formaPago->update();
    	Alert::error('Eliminar Forma de Pago ', $formaPago->forma_pago . ' de forma existosa')->autoClose(2000);
    	return redirect()->action('Viaticos\FormasPagoController@listarFormaPago');

    }

    public function habilitarFormaPago($id){

    	$formaPago= FormaPago::find($id);
    	$formaPago->estado=1;
    	$formaPago->update();
    	Alert::success('Habilitar Forma de Pago ', $formaPago->forma_pago . ' de forma existosa')->autoClose(2000);
    	return redirect()->action('Viaticos\FormasPagoController@listarFormaPago');
    }
}
