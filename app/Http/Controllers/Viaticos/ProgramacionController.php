<?php

namespace App\Http\Controllers\Viaticos;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\ProgramacionRequest;
use App\Modelos\Viatico\Programacion;
use App\Modelos\Empleado\Oficina;
use App\Modelos\Empleado\Centro;
use App\Modelos\Empleado\Empleado;
use App\Modelos\Viatico\Actividad;
use App\Modelos\Viatico\LugarMision;
use App\Modelos\Viatico\ProgramacionDetalle;
use App\Modelos\Viatico\Viatico;
use App\Modelos\Marcacion\MesHora;
use App\Modelos\Administracion\Usuario;
use Barryvdh\DomPDF\Facade as PDF;
use Carbon\Carbon; 
use DatePeriod;
use DateInterval;
use DB;
use Auth;
use RealRashid\SweetAlert\Facades\Alert;
use Input;

class ProgramacionController extends Controller
{     
  public function __construct(){

    $this->middleware(['permission:editar.programacion'], ['only'   => ['editarProgramacion', 'actualizarProgramacion']]);
    $this->middleware(['permission:eliminar.programacion'], ['only'   => ['eliminarProgramacion']]);
    $this->middleware(['permission:habilitar.programacion'], ['only'   => ['habilitarProgramacion']]);
    $this->middleware(['permission:crear.programacion'], ['only'   => ['crearProgramacion', 'guardarProgramacion']]);
    $this->middleware(['permission:consultar.programacion'], ['only'   => 'listarProgramacion']); 
    $this->middleware(['permission:generar.cco'], ['only'   => 'generarCco']); 

    $this->middleware(['permission:buscar.programacion'], ['only'   => 'buscarProgramacion']); 
     }

  public function listarProgramacion(){

      $id = Auth::id();
     if(Auth::user()->hasRole('Secretaria')){     
    

      $oficina = DB::table('admin_usuario as u')->leftJoin('admin_usuario_oficina as uo', 'u.id_usuario', '=', 'usuario_id')->leftJoin('emp_oficina', 'id_oficina', '=', 'oficina_id')->select('id_oficina')->where('id_usuario', '=', $id)->get();

      foreach ($oficina as $key =>$ofi) {
        $id_oficina[] = $ofi->id_oficina; 
      }

      $programacion = DB::table('listado_programaciones_vw')->where('estado','<>',4)->where('usuario_id', '=', $id)->orderBy('id_programacion', 'desc')->get();
      $programacionDeshabilitado= DB::table('listado_programaciones_vw')->whereIntegerInRaw('id_oficina', $id_oficina)->where('estado','=',4)->where('usuario_id', '=', $id)->orderBy('id_programacion', 'desc')->get();

      return view('viaticos.gestionProgramacion.listar', compact('programacion','programacionDeshabilitado'));


    }elseif(Auth::user()->hasRole('Administrador Regional')){
       

      $usuario = Usuario::find($id);//obtenemos el objeto usuario autentificado
      $centroUsuario = $usuario->empleado->oficina->centro->id_centro; //obtenemos el centro de ese objeto usuario autentificado

  $programacion = DB::table('listado_programaciones_vw')->where('id_centro', $centroUsuario)->where('estado','<>',4)->where('usuario_id', '=', $id)->orderBy('id_programacion', 'desc')->get();
  $programacionDeshabilitado=DB::table('listado_programaciones_vw')->where('id_centro', $centroUsuario)->where('estado','=',4)->where('usuario_id', '=', $id)->orderBy('id_programacion', 'desc')->get();
     return view('viaticos.gestionProgramacion.listar', compact('programacion','programacionDeshabilitado'));
   
    
    }elseif(Auth::user()->hasRole('Administrador')) {
   
     
   $programacion = DB::table('listado_programaciones_vw')->where('estado','<>',4)->orderBy('id_programacion', 'desc')->get();
    $programacionDeshabilitado=DB::table('listado_programaciones_vw')->where('estado','=',4)->orderBy('id_programacion', 'desc')->get();
    return view('viaticos.gestionProgramacion.listar', compact('programacion','programacionDeshabilitado'));

    }else{
       $programacion = DB::table('listado_programaciones_vw')->where('estado','<>',4)->where('usuario_id', '=', $id)->orderBy('id_programacion', 'desc')->get();
    $programacionDeshabilitado=DB::table('listado_programaciones_vw')->where('estado','=',4)->where('usuario_id', '=', $id)->orderBy('id_programacion', 'desc')->get();
    return view('viaticos.gestionProgramacion.listar', compact('programacion','programacionDeshabilitado'));
    }


  }

  public function crearProgramacion(){

    Carbon::setLocale('es_SV'); //obtiene la localidad
    $fechaUltima = DB::table('via_programacion')->orderBy('id_programacion','desc')->where('estado','=', 1)->first();
    if($fechaUltima == null){
        $fechaActual = Carbon::now(new \DateTimeZone('America/El_Salvador'));
        $fecha = $fechaActual->format('Y-m-d');
    }
    else{
        $fechaLast = DB::table('via_programacion')->orderBy('id_programacion','desc')->where('estado','=', 1)->first();
        $fecha = $fechaLast->fecha_desde;
    }
    
    $id = Auth::id(); 

    if(Auth::user()->hasRole('Secretaria')){     

    $oficina = DB::table('admin_usuario as u')->leftJoin('admin_usuario_oficina as uo', 'u.id_usuario', '=', 'usuario_id')->leftJoin('emp_oficina', 'id_oficina', '=', 'oficina_id')->select('id_oficina')->where('id_usuario', '=', $id)->get();

      foreach ($oficina as $key =>$ofi) {
        $id_oficina[] = $ofi->id_oficina; 
      }

    $empleados = DB::table('emp_empleado as e')->leftJoin('emp_oficina as o', 'o.id_oficina', '=', 'e.oficina_id')->select('e.id_empleado', 'e.nombre', 'e.apellido', 'e.codigo_empleado', 'o.oficina')->whereIntegerInRaw('o.id_oficina', $id_oficina)->get();
    $actividad = Actividad::where('estado', '=', 1)->get();
    $lugarMision = lugarMision::where('estado', '=', 1)->get();
    $programacion = Programacion::where('estado', '=', 1)->where('usuario_id', $id)->orderBy('id_programacion','desc')->get();
    return view('viaticos.gestionProgramacion.crear', compact('programacion','empleados','actividad','lugarMision','fecha'));


    }elseif(Auth::user()->hasRole('Administrador Regional')){
      

      $usuario = Usuario::find($id);//obtenemos el objeto usuario autentificado
      $centroUsuario = $usuario->empleado->oficina->centro->id_centro; //obtenemos el centro de ese objeto usuario autentificado

    $empleados = DB::table('emp_empleado as e')->leftJoin('emp_oficina as o', 'o.id_oficina', '=', 'e.oficina_id')->leftJoin('emp_centro as c', 'c.id_centro', '=', 'o.centro_id')->select('e.id_empleado', 'e.nombre', 'e.apellido', 'e.codigo_empleado', 'o.oficina')->where('c.id_centro', $centroUsuario)->get();
     $actividad = Actividad::where('estado', '=', 1)->get();
      $lugarMision = lugarMision::where('estado', '=', 1)->get();
      $programacion = Programacion::where('estado', '=', 1)->where('usuario_id', $id)->orderBy('id_programacion','desc')->get();
      return view('viaticos.gestionProgramacion.crear', compact('programacion','empleados','actividad','lugarMision', 'fecha'));   
   
    }else{
      $empleados=Empleado::where('activo',1)->get();
      $actividad = Actividad::where('estado', '=', 1)->get();
      $lugarMision = lugarMision::where('estado', '=', 1)->get();
      $programacion = Programacion::where('estado', '=', 1)->where('usuario_id', $id)->orderBy('id_programacion','desc')->get();
      return view('viaticos.gestionProgramacion.crear', compact('programacion','empleados','actividad','lugarMision', 'fecha'));

    }//fin del if



  } //fin de la funcion crear

  public function getEmpleado(Request $request){
    if ($request->ajax()){
      $empleado = Empleado::where('id_empleado', $request->empleado)->get();
      if($empleado->isEmpty()){
        $arrayEmpleado=null;
      }
      else
      {
        foreach($empleado as $empleado){
          $arrayEmpleado['oficina'] = $empleado->oficina->oficina;
          $arrayEmpleado['nombre'] = $empleado->nombre." ".$empleado->apellido;
          $arrayEmpleado['id_oficina'] = $empleado->oficina_id;
          $arrayEmpleado['centro']=$empleado->oficina->centro->centro;
          $arrayEmpleado['id_centro']=$empleado->oficina->centro->id_centro;

        }
      }
      return response()->json($arrayEmpleado);
    }       
  }

   

  public function guardarProgramacion(ProgramacionRequest $request){
    $fecha1= Carbon::parse($request->fecha_inicio);
    $fecha2= Carbon::parse($request->fecha_final);

    Carbon::setLocale('es_SV'); //obtiene la localidad
    $fechaActual = Carbon::now(new \DateTimeZone('America/El_Salvador'));    //obtiene la fecha y hora actual


    $dia = date('N', strtotime($fechaActual));

    $hora = $fechaActual->format('H:i:s'); 
    $centro = DB::table('emp_centro as c')->leftJoin('emp_oficina as o', 'o.centro_id', '=', 'c.id_centro')->select('c.centro', 'c.fecha_inicio', 'c.fecha_fin', 'c.hora_inicio', 'c.hora_fin')->where('o.id_oficina', '=', $request->id_oficina)->where('c.activo', '=', 1)->first(); //Obtener el centro de la programacion que se quiere obtener para ver si tiene un periodo para ingresar activado

    if($centro != null){ //Consulta si hay algun centro activo
      $diaInicio = $centro->fecha_inicio; //obtiene los datps de ese centro, el periodo fecha y horas.
      $diaFin =$centro->fecha_fin;
      $horaInicio = $centro->hora_inicio;
      $horaFin = $centro->hora_fin;


      if($diaInicio!=null || $diaFin!=null || $horaFin!=null || $horaInicio != null){ //consulta si hay un periodo activado
        if($dia != $diaInicio || $hora < $horaInicio){
          if($dia != $diaFin || $hora < $horaFin){
           Alert::error('¡ERROR! No puede ingresar programaciones en este periodo')->autoClose(3000)->iconHtml('<i class="fa fa-trash-o"></i>'); //si no esta permitido ingresar en este periodo de tiempo-
           return redirect()->action('Viaticos\ProgramacionController@crearProgramacion')->withInput($request->all());
         }
     
       }
     }
 }//fin de pregunta si es vacio 
 //consulta si existe programacion en el rango de fecha
 $programacionExiste = DB::table('via_programacion_detalle as pd')->leftJoin('via_programacion as p', 'p.id_programacion', '=', 'pd.programacion_id')->where('p.empleado_id', '=', $request->empleado)->where('pd.fecha_programacion', '=', [$fecha1, $fecha2])->get();

    //valida si la programacion es diferente de vacio le mostrara un mensaje de error. 
 if(!$programacionExiste->isEmpty()){
  flash("¡ERROR! ¡La programacion ya ha sido registrado!")->error()->important();
  return redirect()->action('Viaticos\ProgramacionController@crearProgramacion')->withInput($request->all());
    }//fin del if de programacion existe
    
    //Valida que la fecha final no sea menor que la fecha de inicio
    if($fecha1!=null && $fecha2!=null){

      if($fecha2 < $fecha1){ 

        flash("¡ERROR! ¡La fecha final no puede ser menor que la fecha de inicio!")->error()->important();
        return redirect()->action('Viaticos\ProgramacionController@crearProgramacion')->withInput($request->all());
      }
    }

    $fechaUno = $fecha1->format('D');
    $fechaDos = $fecha2->format('D');

    $activarFinSemana = Centro::where('id_centro','=',$request->id_centro)->where('fin_semana', '=', 1)->first();
   

    if($activarFinSemana!=null){ //consultamos si han validado que no se pueda ingresar fin de semanas

    if ($fechaUno == 'Sun'||  $fechaUno == 'Sat') {
      flash("¡ERROR! ¡La fecha inicial no puede ser fin de semana!")->error()->important();
      return redirect()->action('Viaticos\ProgramacionController@crearProgramacion')->withInput($request->all());
    }elseif($fechaDos == 'Sun'||  $fechaDos == 'Sat'){
      flash("¡ERROR! ¡La fecha final no puede ser fin de semana!")->error()->important();
      return redirect()->action('Viaticos\ProgramacionController@crearProgramacion')->withInput($request->all());
    }
    }//fin del if de activado fin de semana

  
    $asuetoUno = DB::table('mar_asueto')->select(DB::raw('COUNT(id_asueto) as diaAsueto'))->where('fecha', '=',$fecha1)->first();
    $asuetoDos = DB::table('mar_asueto')->select(DB::raw('COUNT(id_asueto) as diaAsueto'))->where('fecha', '=',$fecha2)->first();

    
    $activarAsueto = Centro::where('id_centro','=',$request->id_centro)->where('asueto', '=', 1)->first();
   if($activarAsueto != null){
     if ($asuetoUno->diaAsueto != 0){
      flash("¡ERROR! ¡La fecha inicial no puede ser asueto!")->error()->important();
      return redirect()->action('Viaticos\ProgramacionController@crearProgramacion')->withInput($request->all());;
    }elseif($asuetoDos->diaAsueto != 0){
      flash("¡ERROR! ¡La fecha final no puede ser asueto!")->error()->important();
      return redirect()->action('Viaticos\ProgramacionController@crearProgramacion')->withInput($request->all());;
    }

   }

   

    $tipoViatico = $request->tipo_viatico;
    if($fecha1 == $fecha2 && $tipoViatico==2){
      flash("¡ERROR! ¡No puede guardar un viatico de tipo corrido para un solo día!")->error()->important();
      return redirect()->action('Viaticos\ProgramacionController@crearProgramacion')->withInput($request->all());;
    }

    $programacion = New Programacion(); 
    $programacion->empleado_id=$request->empleado;
    $programacion->oficina_id=$request->id_oficina;
    $programacion->lugar_mision_id=$request->lugarMision;
    $programacion->actividad_id=$request->actividad;
    $programacion->tipo_viatico_id=$request->tipo_viatico;
    $programacion->fecha_desde=$request->fecha_inicio;
    $programacion->fecha_hasta=$request->fecha_final;
    $programacion->impreso=0;
     $programacion->estado=1; //estado 1: para la programacion de viatico
     $programacion->pago_viatico=0; //definimos cero para poder guardar la programacion y hacer referencia a su id
     $id = Auth::id();
    $programacion->usuario_id= $id; //id del usuario que guardo la programacion
      $programacion->fecha_registro = $fechaActual; //se agrega la fecha de hoy
     $programacion->save(); //Guardamos para que se le asigne un id

    $pagoAcumulado=0;  //Variable que acumula el total a pagar por la programacion ingresada
    $fechaRango=[]; //array de fechas en blanco
    for($date = $fecha1; $date->lte($fecha2); $date->addDay()) { //recorremos en un for las fechas de inicio y final por dias
        $fechaRango[] = $date->format('Y-m-d'); //guardamos en el array las fechas
      }

        if($fecha1 != $fecha2){// si las fecha de inicio y final son diferentes
            foreach($fechaRango as $fechas){ //entramos al arreglo de fecha
                $fechaCarbon=Carbon::parse($fechas);//convertimos la fecha a formato Carbon
                $day = $fechaCarbon->format('D'); //obtenemos el dia de esa fecha
               
                $detalleProgramacion = new ProgramacionDetalle(); //creamos el objeto para guardar el detalle de programacion                
                    $detalleProgramacion->programacion_id = $programacion->id_programacion;
                    $detalleProgramacion->fecha_programacion = $fechas;
                    $detalleProgramacion->estado = $programacion->estado;
                    if($tipoViatico==1){//Viatico de tipo diario
                      $detalleProgramacion->pago_viatico = 8;
                      $pagoAcumulado=$pagoAcumulado+8;
                    }
                    elseif($tipoViatico==2){//Viatico de tipo corrido
                        if($fechaCarbon==$fecha2){//Validamos si es el ultimo dia
                          $detalleProgramacion->pago_viatico = 8;
                          $pagoAcumulado=$pagoAcumulado+8;
                        }
                        else
                        {
                          $detalleProgramacion->pago_viatico = 10;
                          $pagoAcumulado=$pagoAcumulado+10;
                        }
                      }  
                      $detalleProgramacion->save();
                  
            }//fin del foreach
        }//fin del if que valida las fechas
        elseif($fecha1 == $fecha2) //Programacion para un solo dia
        {
            $detalleProgramacion = new ProgramacionDetalle(); //creamos el objeto para guardar el detalle de programacion                
            $detalleProgramacion->programacion_id = $programacion->id_programacion;
            $detalleProgramacion->fecha_programacion = $request->fecha_inicio;
            $detalleProgramacion->estado = $programacion->estado;
            if($tipoViatico==1){
              $detalleProgramacion->pago_viatico = 8;
            }
            $detalleProgramacion->save();
          }
        $programacion->pago_viatico=$pagoAcumulado;//Actualizamos el valor del pago total
        $programacion->update();//Actualizamos la programacion

        Alert::success('Registro Programacion de forma existosa')->autoClose(1200);
        return redirect()->action('Viaticos\ProgramacionController@crearProgramacion')->withInput($request->all());;
      }
      public function editarProgramacion($id){
     

         $actividad = Actividad::where('estado', '=', 1)->get();
         $lugarMision = lugarMision::where('estado', '=', 1)->get();
         $programacion = Programacion::where('estado', '=', 1)->where('id_programacion','=', $id)->orderBy('id_programacion','desc')->first();

         return view('viaticos.gestionProgramacion.editar', compact('actividad','lugarMision','programacion'));
   

     }

     public function actualizarProgramacion(Request $request, $id){

      $programacion = Programacion::find($id);
      $fecha1= Carbon::parse($request->fecha_inicio);
      $fecha2= Carbon::parse($request->fecha_final);
       Carbon::setLocale('es_SV'); //obtiene la localidad
      $fechaActual = Carbon::now(new \DateTimeZone('America/El_Salvador'));    //obtiene la fecha y hora actual
      $dia = date('N', strtotime($fechaActual));
      $hora = $fechaActual->format('H:i:s'); 
      $centro = DB::table('emp_centro as c')->leftJoin('emp_oficina as o', 'o.centro_id', '=', 'c.id_centro')->select('c.centro', 'c.fecha_inicio', 'c.fecha_fin', 'c.hora_inicio', 'c.hora_fin')->where('o.id_oficina', '=', $request->id_oficina)->where('c.activo', '=', 1)->first(); //Obtener el centro de la programacion que se quiere obtener para ver si tiene un periodo para ingresar activado

    if($centro != null){ //Consulta si hay algun centro activo
      $diaInicio = $centro->fecha_inicio; //obtiene los datps de ese centro, el periodo fecha y horas.
      $diaFin =$centro->fecha_fin;
      $horaInicio = $centro->hora_inicio;
      $horaFin = $centro->hora_fin;


      if($diaInicio!=null || $diaFin!=null || $horaFin!=null || $horaInicio != null){ //consulta si hay un periodo activado
        if($dia != $diaInicio || $hora < $horaInicio){
          if($dia != $diaFin || $hora < $horaFin){
           Alert::error('¡ERROR! No puede ingresar programaciones en este periodo')->autoClose(3000)->iconHtml('<i class="fa fa-trash-o"></i>'); //si no esta permitido ingresar en este periodo de tiempo-
           return redirect()->action('Viaticos\ProgramacionController@crearProgramacion')->withInput($request->all());
         }
     
       }
     }
 }//fin de pregunta si es vacio 
     //consulta si la programacion existe ya
     $programacionExiste = DB::table('via_programacion_detalle as pd')->leftJoin('via_programacion as p', 'p.id_programacion', '=', 'pd.programacion_id' )->where('p.empleado_id', '=', $programacion->empleado_id)->whereBetween('pd.fecha_programacion', [$fecha1,$fecha2])->where('p.id_programacion', '<>', $id)->get();

    
    //valida si la programacion es diferente de vacio le mostrara un mensaje de error. 
     if(!$programacionExiste->isEmpty()){
      flash("¡ERROR! ¡La programacion ya ha sido registrado!")->error()->important();
      return redirect()->action('Viaticos\ProgramacionController@listarProgramacion')->withInput($request->all());
    }//fin del if de programacion existe
    
    //Valida que la fecha final no sea menor que la fecha de inicio
    if($fecha1!=null && $fecha2!=null){

      if($fecha2 < $fecha1){ 

        flash("¡ERROR! ¡La fecha final no puede ser menor que la fecha de inicio!")->error()->important();
        return redirect()->action('Viaticos\ProgramacionController@listarProgramacion')->withInput($request->all());
      }
    }

    $fechaUno = $fecha1->format('D');
    $fechaDos = $fecha2->format('D');

     $activarFinSemana = Centro::where('id_centro','=',$request->id_centro)->where('fin_semana', '=', 1)->first();
    if($activarFinSemana!=null){ //consultamos si han validado que no se pueda ingresar fin de semanas

    if ($fechaUno == 'Sun'||  $fechaUno == 'Sat') {
      flash("¡ERROR! ¡La fecha inicial no puede ser fin de semana!")->error()->important();
      return redirect()->action('Viaticos\ProgramacionController@crearProgramacion')->withInput($request->all());
    }elseif($fechaDos == 'Sun'||  $fechaDos == 'Sat'){
      flash("¡ERROR! ¡La fecha final no puede ser fin de semana!")->error()->important();
      return redirect()->action('Viaticos\ProgramacionController@crearProgramacion')->withInput($request->all());
    }
    }//fin del if de activado fin de semana

  
    $asuetoUno = DB::table('mar_asueto')->select(DB::raw('COUNT(id_asueto) as diaAsueto'))->where('fecha', '=',$fecha1)->first();
    $asuetoDos = DB::table('mar_asueto')->select(DB::raw('COUNT(id_asueto) as diaAsueto'))->where('fecha', '=',$fecha2)->first();

    
    $activarAsueto = Centro::where('id_centro','=',$request->id_centro)->where('asueto', '=', 1)->first();
   if($activarAsueto != null){
     if ($asuetoUno->diaAsueto != 0){
      flash("¡ERROR! ¡La fecha inicial no puede ser asueto!")->error()->important();
      return redirect()->action('Viaticos\ProgramacionController@crearProgramacion')->withInput($request->all());;
    }elseif($asuetoDos->diaAsueto != 0){
      flash("¡ERROR! ¡La fecha final no puede ser asueto!")->error()->important();
      return redirect()->action('Viaticos\ProgramacionController@crearProgramacion')->withInput($request->all());;
    }

   }


    $tipoViatico = $request->tipo_viatico;
    if($fecha1 == $fecha2 && $tipoViatico==2){
      flash("¡ERROR! ¡No puede guardar un viatico de tipo corrido para un solo día!")->error()->important();
      return redirect()->action('Viaticos\ProgramacionController@listarProgramacion')->withInput($request->all());
    }


    $programacion->lugar_mision_id=$request->lugarMision;
    $programacion->actividad_id=$request->actividad;
    $programacion->tipo_viatico_id=$request->tipo_viatico;
    $programacion->fecha_desde=$request->fecha_inicio;
    $programacion->fecha_hasta=$request->fecha_final;

    $programacion->update();

    $pagoAcumulado=0;  //Variable que acumula el total a pagar por la programacion ingresada
    $fechaRango=[]; //array de fechas en blanco
    DB::table('via_programacion_detalle')->where('programacion_id', '=', $id)->delete();
    for($date = $fecha1; $date->lte($fecha2); $date->addDay()) { //recorremos en un for las fechas de inicio y final por dias
        $fechaRango[] = $date->format('Y-m-d'); //guardamos en el array las fechas
      }

        if($fecha1 != $fecha2){// si las fecha de inicio y final son diferentes
            foreach($fechaRango as $fechas){ //entramos al arreglo de fecha
                $fechaCarbon=Carbon::parse($fechas);//convertimos la fecha a formato Carbon
               
               
                  $detalleProgramacion = new ProgramacionDetalle(); //creamos el objeto para guardar el detalle de programacion                
                    $detalleProgramacion->programacion_id = $programacion->id_programacion;
                    $detalleProgramacion->fecha_programacion = $fechas;
                    $detalleProgramacion->estado = $programacion->estado;
                    if($tipoViatico==1){//Viatico de tipo diario
                      $detalleProgramacion->pago_viatico = 8;
                      $pagoAcumulado=$pagoAcumulado+8;
                    }
                    elseif($tipoViatico==2){//Viatico de tipo corrido
                        if($fechaCarbon==$fecha2){//Validamos si es el ultimo dia
                          $detalleProgramacion->pago_viatico = 8;
                          $pagoAcumulado=$pagoAcumulado+8;
                        }
                        else
                        {
                          $detalleProgramacion->pago_viatico = 10;
                          $pagoAcumulado=$pagoAcumulado+10;
                        }
                      }  
                      $detalleProgramacion->save();
                    
            }//fin del foreach
        }//fin del if que valida las fechas
        elseif($fecha1 == $fecha2) //Programacion para un solo dia
        {
            
            $detalleProgramacion = new ProgramacionDetalle(); //creamos el objeto para guardar el detalle de programacion                
            $detalleProgramacion->programacion_id = $programacion->id_programacion;
            $detalleProgramacion->fecha_programacion = $request->fecha_inicio;
            $detalleProgramacion->estado = $programacion->estado;
            if($tipoViatico==1){
              $detalleProgramacion->pago_viatico = 8;
            }
            $detalleProgramacion->save();
          }
        $programacion->pago_viatico=$pagoAcumulado;//Actualizamos el valor del pago total
        $programacion->update();//Actualizamos la programacion

        Alert::warning('Actualizar Programacion', $programacion->programacionEmpleado->nombre . ' de forma existosa')->autoClose(2000)->iconHtml('<i class="fa fa-pencil"></i>');
        return redirect()->action('Viaticos\ProgramacionController@listarProgramacion');


      }
      public function eliminarProgramacion($id){
        $programacion = Programacion::find($id);
        $programacion->estado = 4; //estado 4: eliminado
        $programacion->update();
 		Alert::error('Eliminar Programacion de forma existosa')->autoClose(2000);
        return redirect()->action('Viaticos\ProgramacionController@listarProgramacion');
      }

      public function habilitarProgramacion($id){
        $programacion = Programacion::find($id);
        $programacion->estado = 1; //estado 1: ingresar
        $programacion->update();

        return redirect()->action('Viaticos\ProgramacionController@listarProgramacion');
      }

      public function imprimirProgramacion($id){
        $programacion = Programacion::find($id);
        $fecha_inicio = $programacion->fecha_desde;
        $fecha_final = $programacion->fecha_hasta;
         
        $fecha = Carbon::now(new \DateTimeZone('America/El_Salvador'));
        $hora= $fecha->format('h:i:s A'); 
        $date = $fecha->format('Y-m-d');
        $dia = $fecha->format('d-m-Y');
        $codigo_empleado = $programacion->programacionEmpleado->codigo_empleado;
        $cco = DB::table('generar_cco_vw')->select('nombre', 'codigo','apellido','salario','dias','lugar','departamento','tipo_plaza','tipo_viatico','unidad','sede_oficial','actividad','desde','hasta','cargo','pago_viatico','codigo_viatico','valor')->where('desde','=', $fecha_inicio)->where('hasta','=', $fecha_final)->where('codigo', '=', $codigo_empleado)->get();
        $total=DB::table('generar_cco_vw')->select(DB::raw('SUM(pago_viatico) as monto'))->where('desde','=', $fecha_inicio)->where('hasta','=', $fecha_final)->where('codigo', '=', $codigo_empleado)->first();
        $pdf = \PDF::loadView('viaticos.gestionCco.imprimir',compact('cco','total','dia','hora'))->setPaper('A4', 'landscape');   
        return $pdf->stream('programacion.pdf');
      }

    public function generarCco(){ 
    $vacio="";

    Carbon::setLocale('es_SV'); //obtiene la localidad
    $fechaActual = Carbon::now(new \DateTimeZone('America/El_Salvador'));
    $fecha = $fechaActual->format('Y-m-d');
    $id = Auth::id();
    $usuario = Usuario::find($id);

   if(Auth::user()->hasRole('Administrador Regional')){
      //obtenemos el objeto usuario autentificado
      $centroUsuario = $usuario->empleado->oficina->centro->id_centro;
      //$nombreCentro= $usuario->empleado->oficina->centro->centro; //obtenemos el centro de ese objeto usuario autentificado
    $empleados = DB::table('emp_empleado as e')->leftJoin('emp_oficina as o', 'o.id_oficina', '=', 'e.oficina_id')->leftJoin('emp_centro as c', 'c.id_centro', '=', 'o.centro_id')->select('e.id_empleado', 'e.nombre', 'e.apellido', 'e.codigo_empleado', 'o.oficina')->where('c.id_centro', $centroUsuario)->get();
    $arrayCentro=[$centroUsuario]; //el centro del usuario autentificado
    
    return view('viaticos.gestionCco.cerrar',compact('empleados', 'vacio', 'arrayCentro', 'nombreCentro'));
  
   
    }elseif(Auth::user()->hasRole('Secretaria')) {
      $oficina = DB::table('admin_usuario as u')->leftJoin('admin_usuario_oficina as uo', 'u.id_usuario', '=', 'usuario_id')->leftJoin('emp_oficina as o', 'o.id_oficina', '=', 'uo.oficina_id')->select('u.id_oficina', 'u.oficina')->where('id_usuario', '=', $id)->get();

      foreach ($oficina as $key =>$ofi) {
        $id_oficina[] = $ofi->id_oficina; 
        //$nombreCentro= $ofi->oficina;
      }


    $empleados = DB::table('emp_empleado as e')->leftJoin('emp_oficina as o', 'o.id_oficina', '=', 'e.oficina_id')->select('e.id_empleado', 'e.nombre', 'e.apellido', 'e.codigo_empleado', 'o.oficina')->whereIntegerInRaw('o.id_oficina', $id_oficina)->get();
     $arrayCentro=[]; //ningun centro para la secretaria
    return view('viaticos.gestionCco.cerrar',compact('empleados', 'vacio', 'arrayCentro', 'nombreCentro'));

    }else{
      $empleados=Empleado::where('activo', 1)->get();
      $arrayCentro=[]; //ningun centro los otros usuarios
     // $nombreCentro= $usuario->empleado->oficina->centro->centro;

  return view('viaticos.gestionCco.cerrar',compact('empleados', 'vacio', 'arrayCentro'));


    }//fin del if de consultar el usuario
}

      public function generarCcoPdf(Request $request){ 
        $id = Auth::id(); //Id del usuario autentificado
        if($request->verCco==1){
          $fecha = Carbon::now(new \DateTimeZone('America/El_Salvador'));
          $hora= $fecha->format('h:i:s A');  //hora actual
          $date = $fecha->format('Y-m-d'); //fecha actual
          $dia = $fecha->format('d-m-Y'); //dia actual
          if($request->empleado !=0){ //si es un empleado en especifico
          $fechaInicio = $request->fecha_inicio; //fecha de inicio
          $fechaFinal = $request->fecha_final; //fecha final
          $vacio ="Existe";
          $empleado = Empleado::find($request->empleado); //edl empleaddo seleccionado

          if(Auth::user()->hasRole('Administrador Regional') || Auth::user()->hasRole('Secretaria') || Auth::user()->hasRole('Gestor de Recursos Humanos')){
        $cco = DB::table('generar_cco_vw')->select('nombre', 'codigo','apellido','salario','dias','lugar','departamento','tipo_plaza','tipo_viatico','unidad','sede_oficial','actividad','desde','hasta','cargo','diario')->whereBetween('fecha_registro', [$fechaInicio, $fechaFinal])->where('codigo', '=', $empleado->codigo_empleado)->where('estado', '=',1)->where('usuario_id', '=',$id)->whereNull('id')->get();

          }else{
        $cco = DB::table('generar_cco_vw')->select('nombre', 'codigo','apellido','salario','dias','lugar','departamento','tipo_plaza','tipo_viatico','unidad','sede_oficial','actividad','desde','hasta','cargo','diario')->whereBetween('fecha_registro', [$fechaInicio, $fechaFinal])->where('codigo', '=', $empleado->codigo_empleado)->where('estado', '=',1)->whereNull('id')->get();

          }//fin del if programaciones segun usuario


          }elseif($request->empleado==0){
          $fechaInicio = $request->fecha_inicio;
          $fechaFinal = $request->fecha_final;
          $centro = $request->centros;
         
      
          $vacio ="Existe";
          
          if(!empty($centro)){ //consulta cuales son los centros del usuario autentificado

          $cco = DB::table('generar_cco_vw')->select('nombre', 'codigo','apellido','salario','dias','lugar','departamento','tipo_plaza','tipo_viatico','unidad','sede_oficial','actividad','desde','hasta','cargo','diario')->whereBetween('fecha_registro', [$fechaInicio, $fechaFinal])->whereIntegerInRaw('id_centro', $centro)->where('estado', '=',1)->where('usuario_id', '=',$id)->whereNull('id')->get();
        
         
          }elseif (is_null($centro)) {//ver que usuario el que esta entrando, a que centro es
            if(Auth::user()->hasRole('Secretaria') || Auth::user()->hasRole('Gestor de Recursos Humanos')){ //si es secretaria solo trae a los de secretaria
                $cco = DB::table('generar_cco_vw')->select('nombre', 'codigo','apellido','salario','dias','lugar','departamento','tipo_plaza','tipo_viatico','unidad','sede_oficial','actividad','desde','hasta','cargo','diario')->whereBetween('fecha_registro', [$fechaInicio, $fechaFinal])->where('estado', '=',1)->where('usuario_id', '=',$id)->whereNull('id')->get();
            }else{ //de no ser el caso trae todos
              $cco = DB::table('generar_cco_vw')->select('nombre', 'codigo','apellido','salario','dias','lugar','departamento','tipo_plaza','tipo_viatico','unidad','sede_oficial','actividad','desde','hasta','cargo','diario')->whereBetween('fecha_registro', [$fechaInicio, $fechaFinal])->where('estado', '=',1)->whereNull('id')->get();
            }

          }//fin del if de comprobar si no hay centros
        }//cerrar if de la condicion del select empleados,como todos los empleados



        
          if($cco->isEmpty()){
            flash('¡No hay programaciones ingresadas en ese rango de fechas!')->error()->important();
            return redirect()->action('Viaticos\ProgramacionController@generarCco');
          }

          $pdf = \PDF::loadView('viaticos.gestionInformes.ccos',compact('cco','dia','hora'));   
          return $pdf->stream('cco.pdf');
        }
        elseif ($request->imprimirCco==3) { //consulta si lo que se hará es imprimir el CCO
        //if($request->empleado=!0){ 
        $idEmpleado = $request->empleado;
        $empleado = Empleado::find($idEmpleado);
        if($idEmpleado!=0){
       //obtiene el empleado seleccionado
         $fechaInicio = $request->fecha_inicio; //obtiene la fecha de inicio
         $fechaFinal = $request->fecha_final; //obtiene la fecha final
         if(Auth::user()->hasRole('Administrador Regional') || Auth::user()->hasRole('Secretaria') || Auth::user()->hasRole('Gestor de Recursos Humanos')){
            $idProgramacion = DB::table('via_programacion')->select('id_programacion')->whereBetween('fecha_registro', [$fechaInicio,$fechaFinal])->where('empleado_id','=', $empleado->id_empleado)->where('usuario_id', '=',$id)->whereNull('viatico_id')->first();
          }else{
            $idProgramacion = DB::table('via_programacion')->select('id_programacion')->whereBetween('fecha_registro', [$fechaInicio,$fechaFinal])->where('empleado_id','=', $empleado->id_empleado)->whereNull('viatico_id')->first();
          }
       //obtiene el id del registro de la programacion buscada del empleado en especifico
         $programacion = Programacion::find($idProgramacion->id_programacion); //obtenemos el objeto programacion según el id encontrado
         $programacion->estado=3; //estado 3: Impreso de CCO
         $programacion->impreso = 1; //impreso 1:Impreso
         $programacion->update();
         $fecha = Carbon::now(new \DateTimeZone('America/El_Salvador')); //obyiene la fecha actual
         $hora= $fecha->format('h:i:s A'); //obtenemos la hora
         $date = $fecha->format('Y-m-d');//obtenemos formato de fechaa año, mes y dia
         $dia = $fecha->format('d-m-Y'); //obtenemos formato fecha dia, mes y año.
         if(Auth::user()->hasRole('Administrador Regional') || Auth::user()->hasRole('Secretaria') || Auth::user()->hasRole('Gestor de Recursos Humanos')){ //obtener todas las programaciones de ese usuario
          $programaciones = DB::table('informe_programacion_vw')->whereBetween('fecha_registro', [$fechaInicio, $fechaFinal])->where('codigo_empleado', '=', $empleado->codigo_empleado)->where('estado_programacion','=', 3)->where('impreso','=',1)->where('usuario_id', '=',$id)->whereNull('viatico_id')->get(); //obtenemos el formato de cco

        $total=DB::table('informe_programacion_vw')->select(DB::raw('SUM(pago_viatico) as monto'))->whereBetween('fecha_registro', [$fechaInicio, $fechaFinal])->where('codigo_empleado', '=', $empleado->codigo_empleado)->where('estado_programacion','=', 3)->where('impreso','=',1)->where('usuario_id', '=',$id)->whereNull('viatico_id')->first(); 
      }else{ //otener todas las programaciones
        $programaciones = DB::table('informe_programacion_vw')->whereBetween('fecha_registro', [$fechaInicio, $fechaFinal])->where('codigo_empleado', '=', $empleado->codigo_empleado)->where('estado_programacion','=', 3)->where('impreso','=',1)->whereNull('viatico_id')->get(); //obtenemos el formato de cco

        $total=DB::table('informe_programacion_vw')->select(DB::raw('SUM(pago_viatico) as monto'))->whereBetween('fecha_registro', [$fechaInicio, $fechaFinal])->where('codigo_empleado', '=', $empleado->codigo_empleado)->where('estado_programacion','=', 3)->where('impreso','=',1)->whereNull('viatico_id')->first(); 
      }
        //obtenemos el total de todas las programaciones buscadas
        if($programaciones->isEmpty()){//consulta si hay algo encontrado en la busqueda
            flash('¡No hay programaciones ingresadas en ese rango de fechas!')->error()->important();
            return redirect()->action('Viaticos\ProgramacionController@generarCco'); //error, regresa a donde estaba 
          } 

        $pdf = \PDF::loadView('viaticos.gestionInformes.programacion',compact('programaciones','total','dia','hora'))->setPaper('A4', 'landscape');   
        return $pdf->stream('programacion.pdf'); //genera el pdf en una nueva hoja

        }elseif ($request->empleado==0) {
          $fechaInicio = $request->fecha_inicio;
          $fechaFinal = $request->fecha_final;
          $centro = $request->centros;
         
         $fecha = Carbon::now(new \DateTimeZone('America/El_Salvador')); //obyiene la fecha actual
         $hora= $fecha->format('h:i:s A'); //obtenemos la hora
         $date = $fecha->format('Y-m-d');//obtenemos formato de fechaa año, mes y dia
         $dia = $fecha->format('d-m-Y');
          
          if(!empty($centro)){
           //consulta cuales son los centros del usuario autentificado
           //
        $idProgramacion = DB::table('via_programacion as p')->leftJoin('emp_empleado as e', 'e.id_empleado', '=', 'p.empleado_id')->leftJoin('emp_oficina as o', 'o.id_oficina', '=', 'e.oficina_id')->leftJoin('emp_centro as c', 'c.id_centro', '=', 'o.centro_id')->select('p.id_programacion')->whereBetween('p.fecha_registro',[$fechaInicio, $fechaFinal])->whereIntegerInRaw('c.id_centro', $centro)->whereNull('p.viatico_id')->where('p.estado', '=',2)->where('usuario_id', '=',$id)->get();
          foreach ($idProgramacion as $idProgramacion) {
            //obtiene el id del registro de la programacion buscada del empleado en especifico
         $programacion = Programacion::find($idProgramacion->id_programacion); //obtenemos el objeto programacion según el id encontrado
         $programacion->estado=3; //estado 3: Impreso de CCO
         $programacion->impreso = 1; //impreso 1:Impreso
         $programacion->update();
          }
       
          $programaciones = DB::table('informe_programacion_vw')->whereBetween('fecha_registro', [$fechaInicio, $fechaFinal])->where('estado_programacion','=', 3)->where('impreso','=',1)->whereIntegerInRaw('id_centro', $centro)->whereNull('viatico_id')->where('usuario_id', '=',$id)->get(); 

          $total=DB::table('informe_programacion_vw')->select(DB::raw('SUM(pago_viatico) as monto'))->whereBetween('fecha_registro', [$fechaInicio, $fechaFinal])->where('estado_programacion','=', 3)->where('impreso','=',1)->whereIntegerInRaw('id_centro', $centro)->whereNull('viatico_id')->where('usuario_id', '=',$id)->first();
        
         
          }elseif (is_null($centro)) {//si no hay centros
            if(Auth::user()->hasRole('Secretaria')|| Auth::user()->hasRole('Gestor de Recursos Humanos')){
          $idProgramacion = DB::table('via_programacion as p')->leftJoin('emp_empleado as e', 'e.id_empleado', '=', 'p.empleado_id')->leftJoin('emp_oficina as o', 'o.id_oficina', '=', 'e.oficina_id')->leftJoin('emp_centro as c', 'c.id_centro', '=', 'o.centro_id')->select('p.id_programacion')->whereBetween('p.fecha_registro',[$fechaInicio, $fechaFinal])->where('p.estado', '=',2)->whereNull('p.viatico_id')->where('usuario_id', '=',$id)->get();
          }else{
          $idProgramacion = DB::table('via_programacion as p')->leftJoin('emp_empleado as e', 'e.id_empleado', '=', 'p.empleado_id')->leftJoin('emp_oficina as o', 'o.id_oficina', '=', 'e.oficina_id')->leftJoin('emp_centro as c', 'c.id_centro', '=', 'o.centro_id')->select('p.id_programacion')->whereBetween('p.fecha_registro',[$fechaInicio, $fechaFinal])->where('p.estado', '=',2)->whereNull('p.viatico_id')->get();
          }

        
          foreach ($idProgramacion as $idProgramacion) {
            //obtiene el id del registro de la programacion buscada del empleado en especifico
         $programacion = Programacion::find($idProgramacion->id_programacion); //obtenemos el objeto programacion según el id encontrado
         $programacion->estado=3; //estado 3: Impreso de CCO
         $programacion->impreso = 1; //impreso 1:Impreso
         $programacion->update();
          }
       
     if(Auth::user()->hasRole('Secretaria')|| Auth::user()->hasRole('Gestor de Recursos Humanos')){
       $programaciones = DB::table('informe_programacion_vw')->whereBetween('fecha_registro', [$fechaInicio, $fechaFinal])->where('estado_programacion','=', 3)->where('impreso','=',1)->whereNull('viatico_id')->where('usuario_id', '=',$id)->get(); //obtenemos el formato de cco
        $total=DB::table('informe_programacion_vw')->select(DB::raw('SUM(pago_viatico) as monto'))->whereBetween('fecha_registro', [$fechaInicio, $fechaFinal])->where('estado_programacion','=', 3)->where('impreso','=',1)->whereNull('viatico_id')->where('usuario_id', '=',$id)->first();
      }else{
        $programaciones = DB::table('informe_programacion_vw')->whereBetween('fecha_registro', [$fechaInicio, $fechaFinal])->where('estado_programacion','=', 3)->where('impreso','=',1)->whereNull('viatico_id')->get(); //obtenemos el formato de cco
        $total=DB::table('informe_programacion_vw')->select(DB::raw('SUM(pago_viatico) as monto'))->whereBetween('fecha_registro', [$fechaInicio, $fechaFinal])->where('estado_programacion','=', 3)->where('impreso','=',1)->whereNull('viatico_id')->first();
      }//fin del if de preguntar que roles son
       
  
    }//fin del if de comprobar si no hay centros
        if($programaciones->isEmpty()){//consulta si hay algo encontrado en la busqueda
            flash('¡No hay programaciones ingresadas en ese rango de fechas!')->error()->important();
            return redirect()->action('Viaticos\ProgramacionController@generarCco'); //error, regresa a donde estaba 
          } 
        $pdf = \PDF::loadView('viaticos.gestionInformes.programacion',compact('programaciones','total','dia','hora'))->setPaper('A4', 'landscape');   
        return $pdf->stream('programacion.pdf'); //genera el pdf en una nueva hoja

          
        }//fin del if de consulta si son todos los empleados
         
              } //fin del if que pregunta si es el boton de imprimir


        }// fin de la funcion de generar CCO
            public function cerrarCco(Request $request){ 
            //$vacio="Existe";
            $id = Auth::id();  //el usuario autenficado
            $fechaInicio = $request->fecha_inicio;
            $fechaFinal = $request->fecha_final;
            if($request->empleado!=0){ //encontramos si es un empleado en especifico el que quiere
              $empleado = Empleado::find($request->empleado);
            //obtenemos el id de la programacion a cerrar 
            if(Auth::user()->hasRole('Administrador Regional') || Auth::user()->hasRole('Secretaria') || Auth::user()->hasRole('Gestor de Recursos Humanos')){ // de ser cualquiera de estos roles, busca programaciones solo de esos usuarios
              $idProgramacion = DB::table('via_programacion')->select('id_programacion')->whereBetween('fecha_registro',[$fechaInicio,$fechaFinal])->where('empleado_id','=', $empleado->id_empleado)->where('estado', '=',1)->where('usuario_id', $id)->first();

            }else{
              $idProgramacion = DB::table('via_programacion')->select('id_programacion')->whereBetween('fecha_registro',[$fechaInicio,$fechaFinal])->where('empleado_id','=', $empleado->id_empleado)->where('estado', '=',1)->first();
            }
            
            $programacion = Programacion::find($idProgramacion->id_programacion);
            $programacion->estado=2; //estado 2: Cerrado de CCO
            $programacion->update();
            return response()->json(["mensaje"=>'Cerrado con exito']);

            }elseif ($request->empleado==0){ //consultamos si son todos los empleados
                //obtenemos el id de la programacion a cerrar 
            if(Auth::user()->hasRole('Administrador Regional')){
        
              $usuario = Usuario::find($id);
              $centroUsuario = $usuario->empleado->oficina->centro->id_centro;//obtenemos el id del centro de ese usuario
             
              $centro=[$centroUsuario];// de no ser ninguno de estos, especificamos que el usuario con cualquier rol de administrador regional, se guarda el centro de ese usuario en un array 

            }else{
              $centro=[];// de no ser el rol administrador regional, no guardamos nada en los centros. Se ira vacio
            }//fin del if que consulta el rol de usuario

            if(!empty($centro)){//consulta si el array centro es diferente de vacio
            $idProgramacion = DB::table('via_programacion as p')->select('p.id_programacion')->whereBetween('p.fecha_registro',[$fechaInicio, $fechaFinal])->where('p.estado', '=',1)->where('usuario_id',$id)->whereNull('viatico_id')->get(); //obtenemos los id de programacion de esos centros
              foreach ($idProgramacion as $idProgramacion) { //recorremos el objeto de esas programaciones
                  $programacion = Programacion::find($idProgramacion->id_programacion);
                  $programacion->estado=2; //estado 2: Cerrado de CCO
                  $programacion->update();
                }
            return response()->json(["mensaje"=>'Cerrado con exito']);
              }elseif(empty($centro)){//consultamos si es nulo
                if(Auth::user()->hasRole('Secretaria') || Auth::user()->hasRole('Gestor de Recursos Humanos')){
                   $idProgramacion = DB::table('via_programacion')->select('id_programacion')->whereBetween('fecha_registro',[$fechaInicio, $fechaFinal])->where('estado', '=',1)->where('usuario_id',$id)->whereNull('viatico_id')->get(); //obtenemos todas las programaciones segun el rango de fecha seleccionado del usuario autentificado
                }else{
                   $idProgramacion = DB::table('via_programacion')->select('id_programacion')->whereBetween('fecha_registro',[$fechaInicio, $fechaFinal])->where('estado', '=',1)->whereNull('viatico_id')->get();
                }
               
              foreach ($idProgramacion as $idProgramacion) {//recorremos las programaciones de ese estado
                  $programacion = Programacion::find($idProgramacion->id_programacion);
                  $programacion->estado=2; //estado 2: Cerrado de CCO
                  $programacion->update();
                }//fin del foreach
            return response()->json(["mensaje"=>'Cerrado con exito']);

                } //fin del if de consultar que centros pertenece o si lo hay, el usuario autentificado
              }// fin del if de consultar si son todos los empleados
          }

          public function reiniciar(Request $request, $id){ 
            $programacion = Programacion::where('viatico_id','=', $id)->get();
            foreach ($programacion as $programacion) {
              $programacion->estado =1; //regresa a estado 1: ingresado
              $programacion->impreso=0;
              $programacion->viatico_id = null; 
              $programacion->update();
            }

            $viaticos = Viatico::find($id);
            if($viaticos == null){
             $vacio='';
             return view('viaticos.gestionInformes.listar',compact('vacio')); 
           }
           
          $viaticos->delete();
          $vacio="Existe";
          $fecha_inicio = $request->input('fecha_inicio');
          $fecha_final = $request->input('fecha_final');

          $informe = DB::table('informes_viaticos_vw')->where('fecha_inicio','>=',$fecha_inicio)->where('fecha_fin','<=',$fecha_final)->get();

          if($informe->isEmpty()){
            flash("Actualmente, no se poseen datos")->error()->important();
            $vacio='';
            return view('viaticos.gestionInformes.listar',compact('vacio'));
          }else{
            $total = DB::table('informes_viaticos_vw')->select(DB::raw('SUM(monto) AS total'))->where('fecha_inicio','>=',$fecha_inicio)->where('fecha_fin','<=',$fecha_final)->first();

            Alert::warning('Se ha reiniciado',' de forma existosa')->autoClose(2000)->iconHtml('<i class="fa fa-undo"></i>');  

            return view('viaticos.gestionInformes.listar', compact('vacio','informe','fecha_final','fecha_inicio','total'));
          }



        }
        public function buscarProgramacion(){ 

          $vacio = '';
          return view('viaticos.gestionProgramacion.busqueda',compact('vacio'));
        }

        public function cargarProgramacion(Request $request){ 
          $vacio="Existe";
          $fecha_inicio = Carbon::parse($request->fecha_inicio)->format('Y-m-d');
          $fecha_final = Carbon::parse($request->fecha_final)->format('Y-m-d');


        $programacion = DB::table('informe_programacion_vw')->select('id_unidad_administrativa', 'estado_programacion', 'impreso', 'unidad_administrativa',DB::raw('MIN(fecha_programacion) as minimo'),DB::raw('MAX(fecha_programacion) as maximo'),DB::raw('SUM(pago_viatico) as monto'),'nombre_usuario')->whereBetween('fecha_programacion',[$fecha_inicio, $fecha_final])->whereNull('viatico_id')->groupBy('id_unidad_administrativa','estado_programacion','impreso','unidad_administrativa','nombre_usuario')->get();
        
        $total=DB::table('informe_programacion_vw')->select(DB::raw('SUM(pago_viatico) as monto'))->whereBetween('fecha_programacion', [$fecha_inicio, $fecha_final])->whereNull('viatico_id')->first();
       

          return view('viaticos.gestionProgramacion.busqueda', compact('vacio','programacion','total','fecha_final','fecha_inicio'));

        }
        public function pdfProgramacion(Request $request, $id){ 
          $fecha_inicio = $request->fecha_inicio;
          $fecha_final = $request->fecha_final;

          $fecha = Carbon::now(new \DateTimeZone('America/El_Salvador'));
          $hora= $fecha->format('h:i:s A'); 
          $date = $fecha->format('Y-m-d');
          $dia = $fecha->format('d-m-Y');
          $programaciones = DB::table('informe_programacion_vw')->whereBetween('fecha_programacion',[$fecha_inicio, $fecha_final])->where('id_unidad_administrativa','=',$id)->whereNull('viatico_id')->get();



          $total=DB::table('informe_programacion_vw')->select(DB::raw('SUM(pago_viatico) as monto'))->whereBetween('fecha_programacion', [$fecha_inicio, $fecha_final])->whereNull('viatico_id')->where('id_unidad_administrativa','=',$id)->first();

          $pdf = \PDF::loadView('viaticos.gestionInformes.programacion',compact('programaciones','dia','hora','total'))->setPaper('A4', 'landscape');   
          return $pdf->stream('programacion.pdf');
        }



      }
