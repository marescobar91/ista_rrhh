<?php

namespace App\Http\Controllers\Viaticos;

use App\Http\Controllers\Controller;
use App\Modelos\Empleado\Centro;
use App\Modelos\Viatico\TipoViatico;
use App\Modelos\Viatico\FormaPago;
use App\Modelos\Viatico\Programacion;
use App\Modelos\Viatico\Viatico;
use App\Modelos\Viatico\TipoProgramacion;
use Illuminate\Http\Request;
use Carbon\Carbon;
use DB;
use Auth;
use RealRashid\SweetAlert\Facades\Alert;
use App\Http\Requests\ViaticoRequest;
use Barryvdh\DomPDF\Facade as PDF;

class ViaticosController extends Controller
{

	public function __construct(){

		$this->middleware(['permission:buscar.informe'], ['only'   => ['buscarInformes']]);
		$this->middleware(['permission:listar.informe'], ['only'   => ['listarInformes']]);
		$this->middleware(['permission:habilitar.viatico'], ['only'   => ['habilitarTipoProgramacion']]);
		$this->middleware(['permission:crear.viatico'], ['only'   => ['crearViaticos', 'guardarNota']]);
		$this->middleware(['permission:consultar.viatico'], ['only'   => 'listarTipoProgramacion']); 
	}

	public function crearViaticos(){

		$hoy = Carbon::now(new \DateTimeZone('America/El_Salvador'));
		$anio = $hoy->year;
		$anio2 = substr($anio, -2);
		$id = Auth::id();
		if(Auth::user()->hasRole('Administrador Regional')){
		$centros = DB::table('admin_usuario as u')->leftJoin('emp_empleado as e', 'e.id_empleado', '=', 'u.empleado_id')->leftJoin('emp_oficina as o', 'o.id_oficina', '=', 'e.oficina_id')->leftJoin('emp_centro as c', 'c.id_centro', '=', 'o.centro_id')->select('c.id_centro', 'c.centro')->where('id_usuario', '=', $id)->first();

		$centro = Centro::where('id_centro',$centros->id_centro)->get();
		$tipo = TipoProgramacion::where('estado','=',1)->get();
		$pago = FormaPago::where('estado','=',1)->get();

		return view('viaticos.gestionNotas.crear',compact('centro','tipo','pago', 'anio2'));

  }else{
  	$centro = Centro::where('estado','=', 1)->get();
  	$tipo = TipoProgramacion::where('estado','=',1)->get();
  	$pago = FormaPago::where('estado','=',1)->get();

  return view('viaticos.gestionNotas.crear',compact('centro','tipo','pago', 'anio2'));

    }//fin del if
}

public function getCentro(Request $request){
	//obtiene el centro
	if ($request->ajax()){
	$centro = Centro::where('id_centro', $request->centro)->get();  //se obtiene los datos del centro seleccionado
	foreach($centro as $c){
			//se recorre el centro obtenido
	$a1['abv']=$c->abreviatura;  //obtiene la abreviatura del centro obtenido
			//validacion si es oficina central y cetia II 
	if($c->id_centro == 1 || $c->id_centro == 3){
				//obtiene la fecha minima y maxima de las programaciones 
	$fechas = DB::table('via_programacion as p')->leftJoin('emp_empleado as e','e.id_empleado','=','p.empleado_id')->leftJoin('emp_oficina as o','o.id_oficina','=','e.oficina_id')->leftJoin('emp_centro as c','c.id_centro','=','o.centro_id')->select(DB::raw('MIN(fecha_desde) as desde'),DB::raw('MAX(fecha_hasta) as hasta'))->whereIntegerInRaw('c.id_centro', [1,3])->whereNull('p.viatico_id')->whereIn('p.estado',[2,3])->first();
		}//si es cualquiera de los dos
		else{
				//obtiene la fecha minima y maxima de las programaciones 
	$fechas = DB::table('via_programacion as p')->leftJoin('emp_empleado as e','e.id_empleado','=','p.empleado_id')->leftJoin('emp_oficina as o','o.id_oficina','=','e.oficina_id')->leftJoin('emp_centro as c','c.id_centro','=','o.centro_id')->select(DB::raw('MIN(fecha_desde) as desde'),DB::raw('MAX(fecha_hasta) as hasta'))->where('c.id_centro','=',$request->centro)->whereNull('p.viatico_id')->whereIn('p.estado',[2,3])->first();
			} //fin del if
		}//fin del foreach
		//envia los datos a la vista
		$fecha['fechaMinima']= $fechas->desde;
		$fecha['fechaMaxima']= $fechas->hasta;
		$arrayNota['abreviatura'] = $a1;
		$arrayNota['fechas'] = $fecha;

		return response()->json($arrayNota);
	}//fin del if del ajax
}

public function guardarNota(Request $request){
		//funcion para guardar nota de viatico para asignar una referencia a una programacion
	$hoy = Carbon::now(new \DateTimeZone('America/El_Salvador'))->format('Y-m-d');
	$fecha1 = $request->fecha_inicio;
	$fecha2 = $request->fecha_final;
	$viatico = new Viatico();//creamos dos objetos de la clase viáticos
	$viaticoDos = new Viatico();
	//Validamos las fecha, fecha Uno no puede ser mayor que la fecha dos.
	if($fecha1!=null && $fecha2!=null){
		if($fecha2<$fecha1){
		flash("¡ERROR! ¡La fecha final no puede ser menor que la fecha de inicio!")->error()->important();
		return redirect()->action('Viaticos\ViaticosController@crearViaticos')->withInput($request->all());
		}
	}else{
	flash("¡ERROR! ¡No hay programaciones para esta CETIA!")->error()->important();
	return redirect()->action('Viaticos\ViaticosController@crearViaticos')->withInput($request->all());
	}

	//Consulta para ver si la referencia ya existe, validacion que la referencia
	//sea del mismo centro.
	$referencia=Viatico::where('referencia', '=', $request->referencia)->whereNotIn('centro_id', [1,3])->get();

	if(!$referencia->isEmpty()){ //diferente de vacio es porque ya existe una referencia del centro seleccionado
		flash("¡ERROR! ¡La referencia ya ha sido registrada!")->error()->important();
		return redirect()->action('Viaticos\ViaticosController@crearViaticos')->withInput($request->all());	
	}

	if($request->centro==1){//consulta si es OFICINA CENTRAL o CETIA II
	$programacionOficinaCentral =DB::table('informe_programacion_vw')->select(DB::raw('DISTINCT(id_programacion)'))->whereBetween('fecha_programacion', [$fecha1,$fecha2])->where('id_centro', 1)->whereBetween('estado_programacion', [2,3])->whereNull('viatico_id')->get();
	$programacionCetiaDos= DB::table('informe_programacion_vw')->select(DB::raw('DISTINCT(id_programacion)'))->whereBetween('fecha_programacion', [$fecha1,$fecha2])->where('id_centro', 3)->whereBetween('estado_programacion', [2,3])->whereNull('viatico_id')->get();

		if(!$programacionOficinaCentral->isEmpty() && !$programacionCetiaDos->isEmpty()){ //valida que hay programaciones de los centros OFICINA CENTRAL Y CETIA II
			if($request->pago==2){
				$programacionAbonoCuenta = DB::table('informe_programacion_vw')->select(DB::raw('DISTINCT(id_programacion)'))->whereBetween('fecha_programacion', [$fecha1,$fecha2])->whereIntegerInRaw('id_centro',[1,3])->whereNull('viatico_id')->where('banco', '=', 'Banco Agricola')->whereIntegerInRaw('estado_programacion', [2,3])->get();
				if($programacionAbonoCuenta->isEmpty()){
				flash("¡ERROR! ¡Las programaciones no se pueden pagar por abono a cuenta!")->error()->important();
				return redirect()->action('Viaticos\ViaticosController@crearViaticos')->withInput($request->all());
					}//fin del if si hay programaciones de abono a cuenta
				} //fin del if del tipo de pago
			//Asigna datos de la oficina central
			$viatico->referencia = $request->referencia;
			$viatico->centro_id = 1;
			$viatico->tipo_programacion_id = $request->tipo;
			$viatico->forma_pago_id = $request->pago;
			$viatico->fecha = $hoy;
			$viatico->estado = 1;
			$viatico->cantidad_remesa = 0;
			$viatico->cantidad_remesa_segundo = 0;
			$viatico->fecha_inicio=$fecha1;
			$viatico->fecha_final=$fecha2;
			//asigna datos de la CETIA II
			$viaticoDos->referencia = $request->referencia;
			$viaticoDos->centro_id = 3;
			$viaticoDos->tipo_programacion_id = $request->tipo;
			$viaticoDos->forma_pago_id = $request->pago;
			$viaticoDos->fecha = $hoy;
			$viaticoDos->estado = 1;
			$viaticoDos->cantidad_remesa = 0;
			$viaticoDos->cantidad_remesa_segundo = 0;
			$viaticoDos->fecha_inicio=$fecha1;
			$viaticoDos->fecha_final=$fecha2;
			$viatico->save(); //guarda viatico 1 
			$viaticoDos->save(); // guarda viatico 2
	$consultaProgramaciones =DB::table('informe_programacion_vw')->select(DB::raw('DISTINCT(id_programacion)'))->whereBetween('fecha_programacion', [$fecha1,$fecha2])->where('id_centro', 1)->whereBetween('estado_programacion', [2,3])->whereNull('viatico_id')->get();
	$consultaProgramacionesCetiaDos= DB::table('informe_programacion_vw')->select(DB::raw('DISTINCT(id_programacion)'))->whereBetween('fecha_programacion', [$fecha1,$fecha2])->where('id_centro', 3)->whereBetween('estado_programacion', [2,3])->whereNull('viatico_id')->get();
	$id_viatico = $viatico->id_viatico;
	$id_viaticoDos = $viaticoDos->id_viatico;
	foreach ($consultaProgramaciones  as $con) {
			//actualizar la tabla programacion con el id de viatico que se ha creado
	$programacion = Programacion::find($con->id_programacion);
	$programacion->viatico_id = $id_viatico;
	$programacion->update();
	}//fin del foreach
	foreach($consultaProgramacionesCetiaDos as $con){
		$programacion = Programacion::find($con->id_programacion);
		$programacion->viatico_id =$id_viaticoDos;
		$programacion->update();
	}//fin del foreach
	$referencia = $viatico->referencia;
	return redirect()->action('Viaticos\ViaticosController@notaViatico',['referencia' => $referencia]);

}// fin de if de comprueba que hay programaciones en CETIA DOS Y OFICINA CENTRAL
elseif ($programacionOficinaCentral->isEmpty() && !$programacionCetiaDos->isEmpty()) { //Si no hay programaciones en la oficina central
	if($request->pago==2){
		$programacionAbonoCuenta = DB::table('informe_programacion_vw')->select(DB::raw('DISTINCT(id_programacion)'))->whereBetween('fecha_programacion', [$fecha1,$fecha2])->where('id_centro',3)->whereNull('viatico_id')->where('banco', '=', 'Banco Agricola')->whereIntegerInRaw('estado_programacion', [2,3])->get();
				if($programacionAbonoCuenta->isEmpty()){
				flash("¡ERROR! ¡Las programaciones no se pueden pagar por abono a cuenta!")->error()->important();
				return redirect()->action('Viaticos\ViaticosController@crearViaticos')->withInput($request->all());
					}//fin del if si hay programaciones de abono a cuenta
				} //fin del if del tipo de pago
	$viatico->referencia = $request->referencia;
	$viatico->centro_id = 1;
	$viatico->tipo_programacion_id = $request->tipo;
	$viatico->forma_pago_id = $request->pago;
	$viatico->fecha = $hoy;
	$viatico->estado = 1;
	$viatico->cantidad_remesa = 0;
	$viatico->cantidad_remesa_segundo = 0;
	$viatico->fecha_inicio=$fecha1;
	$viatico->fecha_final=$fecha2;
	$viatico->save(); //guarda viatico
	$consultaProgramaciones =DB::table('informe_programacion_vw')->select(DB::raw('DISTINCT(id_programacion)'))->whereBetween('fecha_programacion', [$fecha1,$fecha2])->where('id_centro', 3)->whereBetween('estado_programacion', [2,3])->whereNull('viatico_id')->get();
	$id_viatico = $viatico->id_viatico;
	foreach ($consultaProgramaciones  as $con) {
			//actualizar la tabla programacion con el id de viatico que se ha creado
	$programacion = Programacion::find($con->id_programacion);
	$programacion->viatico_id = $id_viatico;
	$programacion->update();
	}//fin del foreach
	$referencia = $viatico->referencia;
	return redirect()->action('Viaticos\ViaticosController@notaViatico',['referencia' => $referencia]);

}//fin del validacion si oficina central esta vacia solamente
elseif (!$programacionOficinaCentral->isEmpty() && $programacionCetiaDos->isEmpty()) {// valida si no hay programaciones del CETIA II
	if($request->pago==2){
		$programacionAbonoCuenta = DB::table('informe_programacion_vw')->select(DB::raw('DISTINCT(id_programacion)'))->whereBetween('fecha_programacion', [$fecha1,$fecha2])->where('id_centro',1)->whereNull('viatico_id')->where('banco', '=', 'Banco Agricola')->whereIntegerInRaw('estado_programacion', [2,3])->get();
				if($programacionAbonoCuenta->isEmpty()){
				flash("¡ERROR! ¡Las programaciones no se pueden pagar por abono a cuenta!")->error()->important();
				return redirect()->action('Viaticos\ViaticosController@crearViaticos')->withInput($request->all());
					}//fin del if si hay programaciones de abono a cuenta
				} //fin del if del tipo de pago
	$viatico->referencia = $request->referencia;
	$viatico->centro_id = 1;
	$viatico->tipo_programacion_id = $request->tipo;
	$viatico->forma_pago_id = $request->pago;
	$viatico->fecha = $hoy;
	$viatico->estado = 1;
	$viatico->cantidad_remesa = 0;
	$viatico->cantidad_remesa_segundo = 0;
	$viatico->fecha_inicio=$fecha1;
	$viatico->fecha_final=$fecha2;
	$viatico->save(); //guarda viatico
	$consultaProgramaciones =DB::table('informe_programacion_vw')->select(DB::raw('DISTINCT(id_programacion)'))->whereBetween('fecha_programacion', [$fecha1,$fecha2])->where('id_centro', 1)->whereBetween('estado_programacion', [2,3])->whereNull('viatico_id')->get();
	$id_viatico = $viatico->id_viatico;
	foreach ($consultaProgramaciones  as $con) {
			//actualizar la tabla programacion con el id de viatico que se ha creado
	$programacion = Programacion::find($con->id_programacion);
	$programacion->viatico_id = $id_viatico;
	$programacion->update();
	}//fin del foreach
	$referencia = $viatico->referencia;
	return redirect()->action('Viaticos\ViaticosController@notaViatico',['referencia' => $referencia]);


}//fin del if que comprueba de la CEITA II
//para todos los demas centros

} //fin del if comprueba si es CETIA II y OFICINA CENTRAL}
	elseif($request->centro!=1){
		//valida si hay registro de programaciones
	$programacionExiste = DB::table('informe_programacion_vw')->whereBetween('fecha_programacion', [$fecha1, $fecha2])->where('id_centro', '=', $request->centro)->whereNull('viatico_id')->whereIntegerInRaw('estado_programacion', [2,3])->get();

	if ($programacionExiste->isEmpty()) {
			flash("¡ERROR! ¡No hay programaciones registradas!")->error()->important();
			return redirect()->action('Viaticos\ViaticosController@crearViaticos')->withInput($request->all());
	}//fin del if existe
	if($request->pago == 2){  //abono a cuenta
		$programacionAbonoCuenta = DB::table('informe_programacion_vw')->select(DB::raw('DISTINCT(id_programacion)'))->whereBetween('fecha_programacion', [$fecha1, $fecha2])->where('id_centro', '=', $request->centro)->where('banco', '=', 'Banco Agricola')->whereIntegerInRaw('estado_programacion', [2,3])->whereNull('viatico_id')->get();
	if($programacionAbonoCuenta->isEmpty()){
			flash("¡ERROR! ¡No posee empleados para realizar abono a cuenta!")->error()->important();
			return redirect()->action('Viaticos\ViaticosController@crearViaticos')->withInput($request->all());
		}
	}//fin de la validacion del tipo de pago
	$viatico->referencia = $request->referencia;
	$viatico->centro_id = $request->centro;
	$viatico->tipo_programacion_id = $request->tipo;
	$viatico->forma_pago_id = $request->pago;
	$viatico->fecha = $hoy;
	$viatico->estado = 1;
	$viatico->cantidad_remesa = 0;
	$viatico->cantidad_remesa_segundo = 0;
	$viatico->fecha_inicio=$fecha1;
	$viatico->fecha_final=$fecha2;
	$viatico->save(); //guarda viatico
	$consultaProgramaciones = DB::table('informe_programacion_vw')->select(DB::raw('DISTINCT(id_programacion)'))->whereBetween('fecha_programacion', [$fecha1, $fecha2])->where('id_centro', '=', $request->centro)->whereBetween('estado_programacion',[2,3])->whereNull('viatico_id')->get();
	$id_viatico = $viatico->id_viatico;
	foreach ($consultaProgramaciones  as $con) {
			//actualizar la tabla programacion con el id de viatico que se ha creado
	$programacion = Programacion::find($con->id_programacion);
	$programacion->viatico_id = $id_viatico;
	$programacion->update();
	}//fin del foreach
	$referencia = $viatico->referencia;
	return redirect()->action('Viaticos\ViaticosController@notaViatico',['referencia' => $referencia]);

	}


	
		
}
	public function notaViatico($referencia){
		$viatico = Viatico::where('referencia', '=', $referencia)->first();
		$hoy = Carbon::now(new \DateTimeZone('America/El_Salvador'))->format('d-m-Y');	
		$fecha = Carbon::now(new \DateTimeZone('America/El_Salvador'));	
		$hora= $fecha->format('h:i:s A'); 
		$date = $fecha->format('Y-m-d');
		$dia = $fecha->format('d-m-Y');
		$mes = $fecha->monthName;
		$anio = $fecha->year;
		$pago = $viatico->formaPago->forma_pago;
		$centro = $viatico->centro->centro;
		$abreviatura = $viatico->centro->abreviatura;

		 if($centro=='OFICINA CENTRAL' || $centro=='CETIA II'){
      $montoCentral = DB::table('informes_viaticos_vw')->select(DB::raw('SUM(monto) as monto'))->where('referencia', $referencia)->where('id_centro',1)->first();
      $montoCetiaDos = DB::table('informes_viaticos_vw')->select(DB::raw('SUM(monto) as monto'))->where('referencia', $referencia)->where('id_centro',3)->first();
        $listadoCentros=Centro::where('estado',1)->whereIn('id_centro',[1,3])->get();

      }else{
      $monto= DB::table('informes_viaticos_vw')->select(DB::raw('SUM(monto) as monto'))->where('referencia', $referencia)->where('centro',$centro)->first();
         $listadoCentros=Centro::where('estado',1)->where('centro',$centro)->get();
      }

     $p=0;
    $consulta=[];
    foreach($listadoCentros as $listado){

      if($listado->centro=='OFICINA CENTRAL'){
      $det['centro']= 'OFICINA CENTRAL';
      if($montoCentral->monto!=null){
        $det['efectivo']=$montoCentral->monto;
      }else{
        $det['efectivo']= 0.0;
      }
      $consulta[$p]=$det;
      $p++;
    }elseif($listado->centro=='CETIA II'){
        $det['centro']= 'CETIA II';
      if($montoCetiaDos->monto!=null){
        $det['efectivo']= $montoCetiaDos->monto;
      }else{
         $det['efectivo']= 0.0;
      }
      $consulta[$p]=$det;
      $p++;
    }else{
      $det['centro']=$centro;
      if($monto->monto!=null){
        $det['efectivo']= $monto->monto;
      }else{
        $det['efectivo']= 0.0;
      }
     $consulta[$p]=$det;
     $p++;
  }//fin del if

    }//fin del foreach del centro

$empleadoJefe = DB::table('emp_empleado as e')->leftJoin('emp_cargo_mp as mp', 'e.cargo_mp_id', '=', 'mp.id_cargo_mp')->select('e.nombre', 'e.apellido')->where('mp.id_cargo_mp', '=', 16)->first();
$empleadoJefeFinanciero = DB::table('emp_empleado as e')->leftJoin('emp_cargo_mp as mp', 'e.cargo_mp_id', '=', 'mp.id_cargo_mp')->leftJoin('emp_oficina as o', 'e.oficina_id', '=', 'o.id_oficina')->leftJoin('emp_centro as c', 'c.id_centro', '=', 'o.centro_id')->select('e.nombre', 'e.apellido')->where('mp.id_cargo_mp', '=',17)->where('c.abreviatura', '=', 'GRH')->first();

$sumaViatico = DB::table('informes_viaticos_vw')->select(DB::raw('SUM(monto) as Efectivo'))->where('referencia', '=', $referencia)->first();

$fechaFin = Carbon::parse($viatico->fecha_final); //convierte la fecha final
        $mesFin = $fechaFin->monthName; //obtengo el mes de la fecha fin

  $fechaIni = Carbon::parse($viatico->fecha_inicio); //convierte la fecha inicial
   $mesIni = $fechaIni->monthName; //obtengo el mes de la fecha inicial

  $pdf = \PDF::loadView('viaticos.gestionNotas.imprimir',compact('dia','hora','fechaFin','fechaIni', 'referencia','pago', 'empleadoJefe', 'empleadoJefeFinanciero', 'consulta', 'hoy', 'sumaViatico', 'mes', 'anio', 'mesIni', 'mesFin'));   
        return $pdf->stream('notaViatico.pdf');
    }

    public function notaViaticoPDF(Request $request, $id){
    	$vacio="Existe";
    	$hoy = Carbon::now(new \DateTimeZone('America/El_Salvador'))->format('d-m-Y');
    	$fecha = Carbon::now(new \DateTimeZone('America/El_Salvador'));	
    	$hora= $fecha->format('h:i:s A'); 
    	$date = $fecha->format('Y-m-d');
    	$dia = $fecha->format('d-m-Y');
    	$mes = $fecha->monthName;
    	$anio = $fecha->year;
        //obtengo el mes de la fecha inicial
        $informe = DB::table('informes_viaticos_vw')->where('fecha_inicio','>=',$request->fecha_inicio)->where('fecha_fin','<=',$request->fecha_final)->where('id', $id)->first();
        $referencia = $informe->referencia;

        $viatico = Viatico::where('referencia', $referencia)->first();
        $pago = $viatico->formaPago->forma_pago;
        $centro = $viatico->centro->centro;
        $abreviatura = $viatico->centro->abreviatura;
        $fecha_final= $viatico->fecha_final;
        $fecha_inicio= $viatico->fecha_inicio;
        $fechaFin = Carbon::parse($fecha_final); //convierte la fecha final
        $mesFin = $fechaFin->monthName; //obtengo el mes de la fecha fin

        $fechaIni = Carbon::parse($fecha_inicio); //convierte la fecha inicial
        $mesIni = $fechaIni->monthName; 


	if($centro=='OFICINA CENTRAL' || $centro=='CETIA II'){
      $montoCentral = DB::table('informes_viaticos_vw')->select(DB::raw('SUM(monto) as monto'))->where('referencia', $referencia)->where('id_centro',1)->first();
      $montoCetiaDos = DB::table('informes_viaticos_vw')->select(DB::raw('SUM(monto) as monto'))->where('referencia', $referencia)->where('id_centro',3)->first();
        $listadoCentros=Centro::where('estado',1)->whereIn('id_centro',[1,3])->get();

      }else{
      $monto= DB::table('informes_viaticos_vw')->select(DB::raw('SUM(monto) as monto'))->where('referencia', $referencia)->where('centro',$centro)->first();
         $listadoCentros=Centro::where('estado',1)->where('centro',$centro)->get();
      }

     $p=0;
    $consulta=[];
    foreach($listadoCentros as $listado){

      if($listado->centro=='OFICINA CENTRAL'){
      $det['centro']= 'OFICINA CENTRAL';
      if($montoCentral->monto!=null){
        $det['efectivo']=$montoCentral->monto;
      }else{
        $det['efectivo']= 0.0;
      }
      $consulta[$p]=$det;
      $p++;
    }elseif($listado->centro=='CETIA II'){
        $det['centro']= 'CETIA II';
      if($montoCetiaDos->monto!=null){
        $det['efectivo']= $montoCetiaDos->monto;
      }else{
         $det['efectivo']= 0.0;
      }
      $consulta[$p]=$det;
      $p++;
    }else{
      $det['centro']=$centro;
      if($monto->monto!=null){
        $det['efectivo']= $monto->monto;
      }else{
        $det['efectivo']= 0.0;
      }
     $consulta[$p]=$det;
     $p++;
  }//fin del if

    }//fin del foreach del centro

     
    $empleadoJefe = DB::table('emp_empleado as e')->leftJoin('emp_cargo_mp as mp', 'e.cargo_mp_id', '=', 'mp.id_cargo_mp')->select('e.nombre', 'e.apellido')->where('mp.id_cargo_mp', '=', 16)->first();

    $empleadoJefeFinanciero = DB::table('emp_empleado as e')->leftJoin('emp_cargo_mp as mp', 'e.cargo_mp_id', '=', 'mp.id_cargo_mp')->leftJoin('emp_oficina as o', 'e.oficina_id', '=', 'o.id_oficina')->leftJoin('emp_centro as c', 'c.id_centro', '=', 'o.centro_id')->select('e.nombre', 'e.apellido')->where('mp.id_cargo_mp', '=',17)->where('c.abreviatura', '=', 'GRH')->first();

     $sumaViatico = DB::table('informes_viaticos_vw')->select(DB::raw('SUM(monto) as Efectivo'))->where('referencia', '=', $referencia)->first();

        $pdf = \PDF::loadView('viaticos.gestionNotas.imprimir',compact('informe','dia','hora','fechaFin','fechaIni', 'referencia','pago', 'empleadoJefe', 'empleadoJefeFinanciero', 'consulta', 'hoy', 'sumaViatico', 'mes', 'anio', 'mesIni', 'mesFin'));   
        return $pdf->stream('notaViatico.pdf');


	}//fin de la funcion notaViaticoPDF

	public function buscarInformes(){
		$vacio='';
		return view('viaticos.gestionInformes.listar', compact('vacio'));
	}

	public function listarInformes(Request $request){
		$vacio="Existe"; 
		$fecha_inicio = $request->input('fecha_inicio');
		$fecha_final = $request->input('fecha_final');

		if($fecha_inicio !=null && $fecha_final!=null){

			if($fecha_final < $fecha_inicio){ 

				flash("¡ERROR! ¡La fecha final no puede ser menor que la fecha de inicio!")->error()->important();

			}
		}

		$informe = DB::table('informes_viaticos_vw')->where('fecha_inicio','>=',$fecha_inicio)->where('fecha_fin','<=',$fecha_final)->orderBy('centro', 'asc')->orderBy('fecha_inicio', 'asc')->get();

		if($informe->isEmpty()){
			flash("El informe no contiene información, en las fechas establecidas")->error()->important();
			$vacio='';
		}

		$total = DB::table('informes_viaticos_vw')->select(DB::raw('SUM(monto) AS total'))->where('fecha_inicio','>=',$fecha_inicio)->where('fecha_fin','<=',$fecha_final)->first();

		return view('viaticos.gestionInformes.listar', compact('vacio','informe','fecha_final','fecha_inicio','total'));
	}

	public function notaAbiertoCerrado(Request $request,$id){
		$vacio="Existe";
		$fecha_inicio = $request->input('fecha_inicio');
		$fecha_final = $request->input('fecha_final');
		$viaticos = Viatico::find($id);
		$viaticos->estado=2;
		$viaticos->update();

		$informe = DB::table('informes_viaticos_vw')->where('fecha_inicio','>=',$fecha_inicio)->where('fecha_fin','<=',$fecha_final)->get();

		$total = DB::table('informes_viaticos_vw')->select(DB::raw('SUM(monto) AS total'))->where('fecha_inicio','>=',$fecha_inicio)->where('fecha_fin','<=',$fecha_final)->first();

		Alert::error('Se ha cerrado',' de forma existosa')->autoClose(2000)->iconHtml('<i class="fa fa-circle"></i>');

		return view('viaticos.gestionInformes.listar', compact('vacio','informe','fecha_final','fecha_inicio','total'));

	}

	public function notaCerradoAbierto(Request $request,$id){
		$vacio="Existe";
		$fecha_inicio = $request->input('fecha_inicio');
		$fecha_final = $request->input('fecha_final');
		$viaticos = Viatico::find($id);
		$viaticos->estado=1;
		$viaticos->update();

		$informe = DB::table('informes_viaticos_vw')->where('fecha_inicio','>=',$fecha_inicio)->where('fecha_fin','<=',$fecha_final)->get();

		$total = DB::table('informes_viaticos_vw')->select(DB::raw('SUM(monto) AS total'))->where('fecha_inicio','>=',$fecha_inicio)->where('fecha_fin','<=',$fecha_final)->first();


		Alert::warning('Se ha abierto',' de forma existosa')->autoClose(2000)->iconHtml('<i class="fa fa-circle-o"></i>');	

		return view('viaticos.gestionInformes.listar', compact('vacio','informe','fecha_final','fecha_inicio','total'));
	}

	public function informePDF(Request $request,$id){
		$vacio="Existe";
		$fecha_inicio = $request->input('fecha_inicio');
		$fecha_final = $request->input('fecha_final');
		$fecha = Carbon::now(new \DateTimeZone('America/El_Salvador'));
		$hora= $fecha->format('h:i:s A'); 
		$date = $fecha->format('Y-m-d');
		$dia = $fecha->format('d-m-Y');
		$firma = "";

		$viatico = Viatico::find($id);
		if($viatico->id_viatico==null){
			flash("¡ERROR! ¡La fecha final no puede ser menor que la fecha de inicio!")->error()->important();
		}

		if($viatico->TipoProgramacion->tipo_programacion=='Anticipado'){
		$tipoReporte ='FIPL-26 R_1';
		$informe = DB::table('informe_empleado_vw')->select('empleado','fecha',DB::raw('SUM(pago) AS total'))->whereBetween('fecha',[$fecha_inicio,$fecha_final])->where('id','=',$id)->where('tipo_programacion', 'Anticipado')->groupBy('empleado',DB::raw('fecha WITH ROLLUP'))->get();


		$total = DB::table('informe_empleado_vw')->select(DB::raw('SUM(pago) AS total'))->whereBetween('fecha',[$fecha_inicio,$fecha_final])->where('id','=',$id)->where('tipo_programacion', 'Anticipado')->first();

		$centro = DB::table('informe_empleado_vw')->select('centro')->whereBetween('fecha',[$fecha_inicio,$fecha_final])->where('id','=',$id)->where('tipo_programacion', 'Anticipado')->first();

		$pdf = \PDF::loadView('viaticos.gestionInformes.informe',compact('informe','dia','hora','fecha_final','fecha_inicio','total','centro','tipoReporte'));   
		return $pdf->stream('informe.pdf');

		}else{
		$tipoReporte ='FIPL-87 R_1';
		$informe = DB::table('informe_empleado_vw')->select('empleado','fecha',DB::raw('SUM(pago) AS total'))->whereBetween('fecha',[$fecha_inicio,$fecha_final])->where('id','=',$id)->where('tipo_programacion', '<>','Anticipado')->groupBy('empleado',DB::raw('fecha WITH ROLLUP'))->get();


		$total = DB::table('informe_empleado_vw')->select(DB::raw('SUM(pago) AS total'))->whereBetween('fecha',[$fecha_inicio,$fecha_final])->where('id','=',$id)->where('tipo_programacion', '<>','Anticipado')->first();

		$centro = DB::table('informe_empleado_vw')->select('centro')->whereBetween('fecha',[$fecha_inicio,$fecha_final])->where('id','=',$id)->where('tipo_programacion', '<>','Anticipado')->first();

		$pdf = \PDF::loadView('viaticos.gestionInformes.informe',compact('informe','dia','hora','fecha_final','fecha_inicio','total','centro', 'tipoReporte'));   
		return $pdf->stream('informe.pdf');
		}

		
	}

	public function ccosPDF(Request $request,$id){
		$vacio="Existe";
		$fecha_inicio = $request->input('fecha_inicio');
		$fecha_final = $request->input('fecha_final');
		$fecha = Carbon::now(new \DateTimeZone('America/El_Salvador'));
		$hora= $fecha->format('h:i:s A'); 
		$date = $fecha->format('Y-m-d');
		$dia = $fecha->format('d-m-Y');
		$dias= date('t', strtotime($fecha));

		$cco = DB::table('generar_cco_vw')->select('nombre', 'codigo','apellido','salario','dias','lugar','departamento','tipo_plaza','tipo_viatico','unidad','sede_oficial','actividad','desde','hasta','cargo','diario')->where('id','=', $id)->get();


		$pdf = \PDF::loadView('viaticos.gestionInformes.ccos',compact('cco','dia','hora','fecha_final','fecha_inicio'));   
		return $pdf->stream('cco.pdf');
	}
	public function programacionesPDF(Request $request,$id){
		$fecha_inicio = $request->fecha_inicio;
		$fecha_final = $request->fecha_final;

		$fecha = Carbon::now(new \DateTimeZone('America/El_Salvador'));
		$hora= $fecha->format('h:i:s A'); 
		$date = $fecha->format('Y-m-d');
		$dia = $fecha->format('d-m-Y');
		$programaciones = DB::table('informe_programacion_vw')->where('viatico_id', '=', $id)->get();

		$total=DB::table('informe_programacion_vw')->select(DB::raw('SUM(pago_viatico) as monto'))->where('viatico_id', '=', $id)->first();

		$pdf = \PDF::loadView('viaticos.gestionInformes.programacion',compact('programaciones','dia','hora','total'))->setPaper('A4', 'landscape');   
		return $pdf->stream('programacion.pdf');
	}

	public function actualizarReferencia(Request $request,$id){

		$viatico = Viatico::find($id);
		if($request->referencia == ""){
			$programacion = Programacion::where('viatico_id',$id)->get();
			foreach ($programacion as $pro) {
				$pro->viatico_id = null;
				$pro->update();
			}
			$viatico->delete();
			flash('Se ha eliminado de forma existosa')->error()->important();
		}
		else{
			$viaticoExiste = Viatico::where('referencia','=', $request->referencia)->where('id_viatico','<>',$id)->get();
			if(!$viaticoExiste->isEmpty()){
				flash('¡ERROR! ¡La referencia ya existe!')->error()->important();
			}else{
				$viatico->referencia= $request->referencia;
				$viatico->update();
				Alert::warning('Se ha actualizado', $viatico->referencia .' de forma existosa')->autoClose(2000)->iconHtml('<i class="fa fa-pencil"></i>');
			}
			
		}
		$vacio="Existe";
		$fecha_inicio = $request->input('fecha_inicio');
		$fecha_final = $request->input('fecha_final');
		$informe = DB::table('informes_viaticos_vw')->where('fecha_inicio','>=',$fecha_inicio)->where('fecha_fin','<=',$fecha_final)->get();

		$total = DB::table('informes_viaticos_vw')->select(DB::raw('SUM(monto) AS total'))->where('fecha_inicio','>=',$fecha_inicio)->where('fecha_fin','<=',$fecha_final)->first();

		return view('viaticos.gestionInformes.listar', compact('vacio','informe','fecha_final','fecha_inicio','total'));
	}
}
