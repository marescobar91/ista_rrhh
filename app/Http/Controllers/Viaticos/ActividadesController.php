<?php

namespace App\Http\Controllers\Viaticos;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Modelos\Viatico\Actividad;
use RealRashid\SweetAlert\Facades\Alert;

class ActividadesController extends Controller
{
    public function __construct(){

    $this->middleware(['permission:editar.catalogos'], ['only'   => ['editarActividad', 'actualizarActividad']]);
    $this->middleware(['permission:eliminar.catalogos'], ['only'   => ['eliminarActividad']]);
    $this->middleware(['permission:habilitar.catalogos'], ['only'   => ['habilitarActividad']]);
    $this->middleware(['permission:crear.catalogos'], ['only'   => ['crearActividad', 'guardarActividad']]);
    $this->middleware(['permission:consultar.catalogos'], ['only'   => 'listarActividad']); 
     }

    public function crearActividad(){

    	return view('viaticos.gestionActividad.crear');
    }

    public function guardarActividad(Request $request){

    	$actividad = new Actividad();
    	$actividad->actividad=$request->nombre_actividad;
    	$actividad->estado=1;
    	$actividad->save();
    	Alert::success('Registrar Actividad ', $actividad->actividad . ' de forma existosa')->autoClose(2000);
    	return redirect()->action('Viaticos\ActividadesController@listarActividad');
    }

   public function listarActividad(){

   	$actividad = Actividad::where('estado', '=', 1)->get();
   	$actividadDeshabilitado = Actividad::where('estado', '=', 0)->get();

   	return view('viaticos.gestionActividad.listar', compact('actividad', 'actividadDeshabilitado'));
   }

   public function editarActividad($id){

   	$actividad = Actividad::find($id);
  

   return view('viaticos.gestionActividad.editar', compact('actividad'));
   }


   public function actualizarActividad(Request $request, $id){

   	$actividad = Actividad::find($id);
   	$actividad->actividad=$request->nombre_actividad;
   	$actividad->estado=1;
   	$actividad->update();
   	Alert::warning('Actualizar Actividad', $actividad->actividad . ' de forma existosa')->autoClose(2000)->iconHtml('<i class="fa fa-pencil"></i>');
    	return redirect()->action('Viaticos\ActividadesController@listarActividad');
   }

   public function eliminarActividad($id){
   	$actividad= Actividad::find($id);
   	$actividad->estado =0;
   	$actividad->update();
   	Alert::error('Eliminar Actividad', $actividad->actividad . ' de forma existosa')->autoClose(2000)->iconHtml('<i class="fa fa-trash-o"></i>');
	 return redirect()->action('Viaticos\ActividadesController@listarActividad');

   }

   public function habilitarActividad($id){

   	$actividad= Actividad::find($id);
   	$actividad->estado =1;
   	$actividad->update();
   	Alert::success('Habilitar Actividad', $actividad->actividad . ' de forma existosa')->autoClose(2000);
	 return redirect()->action('Viaticos\ActividadesController@listarActividad');
   }


}
