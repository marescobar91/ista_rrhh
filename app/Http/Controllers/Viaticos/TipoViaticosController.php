<?php

namespace App\Http\Controllers\Viaticos;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Modelos\Viatico\TipoViatico;
use RealRashid\SweetAlert\Facades\Alert;


class TipoViaticosController extends Controller
{
    public function __construct(){

    $this->middleware(['permission:editar.catalogos'], ['only'   => ['editarTipoViatico', 'actualizarTipoViatico']]);
    $this->middleware(['permission:eliminar.catalogos'], ['only'   => ['eliminarTipoViatico']]);
    $this->middleware(['permission:habilitar.catalogos'], ['only'   => ['habilitarTipoViatico']]);
    $this->middleware(['permission:crear.catalogos'], ['only'   => ['crearTipoViatico', 'guardarTipoViatico']]);
    $this->middleware(['permission:consultar.catalogos'], ['only'   => 'listarTipoViatico']); 
     }
    

    public function crearTipoViatico(){

    	return view('viaticos.gestionTipoViatico.crear');
    }

    public function guardarTipoViatico(Request $request){

    	$tipoViatico = new TipoViatico();
    	$tipoViatico->tipo_viatico = $request->tipo_viatico;
    	$tipoViatico->codigo = $request->codigo;
    	$tipoViatico->valor = $request->valor;
    	$tipoViatico->estado=1;
    	$tipoViatico->save();
    	Alert::success('Registrar Tipo de Viáticos ', $tipoViatico->tipo_viatico . ' de forma existosa')->autoClose(2000);
    	return redirect()->action('Viaticos\TipoViaticosController@listarTipoViatico');

    }


    public function listarTipoViatico(){
    	$tipoViatico = TipoViatico::where('estado', '=', 1)->get();
    	$tipoViaticoDeshabilitado = TipoViatico::where('estado', '=', 0)->get();

    	return view('viaticos.gestionTipoViatico.listar', compact('tipoViatico', 'tipoViaticoDeshabilitado'));

    }

    public function editarTipoViatico($id){

    	$tipoViatico= TipoViatico::find($id);

    	return view('viaticos.gestionTipoViatico.editar', compact('tipoViatico'));
    	
    }

    public function actualizarTipoViatico(Request $request, $id){

    	$tipoViatico = TipoViatico::find($id);
    	$tipoViatico->tipo_viatico = $request->tipo_viatico;
    	$tipoViatico->codigo = $request->codigo;
    	$tipoViatico->valor = $request->valor;
    	$tipoViatico->estado=1;
    	$tipoViatico->update();

    	Alert::warning('Actualizar Tipo de Viático', $tipoViatico->tipo_viatico . ' de forma existosa')->autoClose(2000)->iconHtml('<i class="fa fa-pencil"></i>');
    	return redirect()->action('Viaticos\TipoViaticosController@listarTipoViatico');


    }

    public function eliminarTipoViatico($id){

    	$tipoViatico = TipoViatico::find($id);
    	$tipoViatico->estado =0;
    	$tipoViatico->update();

    	Alert::error('Eliminar Tipo de Viático', $tipoViatico->tipo_viatico  . ' de forma existosa')->autoClose(2000)->iconHtml('<i class="fa fa-trash-o"></i>');
	 return redirect()->action('Viaticos\TipoViaticosController@listarTipoViatico');

    }


    public function habilitarTipoViatico($id){

    	$tipoViatico = TipoViatico::find($id);
    	$tipoViatico->estado=1;
    	$tipoViatico->update();

    Alert::success('Habilitar Tipo de Viáticos ', $tipoViatico->tipo_viatico . ' de forma existosa')->autoClose(2000);
    	return redirect()->action('Viaticos\TipoViaticosController@listarTipoViatico');

    }
}
