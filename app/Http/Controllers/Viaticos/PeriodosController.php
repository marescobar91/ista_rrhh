<?php

namespace App\Http\Controllers\Viaticos;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Modelos\Viatico\Periodo;
use App\Modelos\Empleado\Centro;
use Carbon\Carbon;
use DB;
use RealRashid\SweetAlert\Facades\Alert;

class PeriodosController extends Controller 
{
     public function __construct(){

    
    $this->middleware(['permission:eliminar.periodo'], ['only'   => ['desactivarPeriodo']]);
    $this->middleware(['permission:habilitar.periodo'], ['only'   => ['activarPeriodo']]);
    $this->middleware(['permission:crear.periodo'], ['only'   => ['crearPeriodo', 'guardarPeriodo']]);
    $this->middleware(['permission:consultar.periodo'], ['only'   => 'listarPeriodo']); 
     }
   
    	public function listarPeriodo(){

    		$periodo = Periodo::all();

    		return view('viaticos.gestionPeriodo.listar', compact('periodo'));
    	}


   public function crearPeriodo(){

 	$centro = Centro::where('estado', 1)->get();
 	$hoy = Carbon::now(new \DateTimeZone('America/El_Salvador'));
	$anio = $hoy->year;

   	return view('viaticos.gestionPeriodo.crear', compact('centro', 'anio'));
   }

   public function guardarPeriodo(Request $request){

     $centros = $_POST['centros'];
   	 $centro = DB::table('emp_centro')->whereIntegerInRaw('id_centro', $centros)->get();

   	foreach($centro as $centro){
   		$periodo = new Periodo();
   		$periodo->mes = $request->mes_periodo;

   		if($request->mes_periodo =='Enero'){

   			$periodo->numero_mes=1;
   		}elseif ($request->mes_periodo=='Febrero') {
   			$periodo->numero_mes=2;
   		}elseif ($request->mes_periodo=='Marzo') {
   			$periodo->numero_mes=3;
   		}elseif ($request->mes_periodo=='Abril') {
   			$periodo->numero_mes=4;
   		}elseif ($request->mes_periodo=='Mayo') {
   			$periodo->numero_mes=5;
   		}elseif ($request->mes_periodo=='Junio') {
   			$periodo->numero_mes=6;
   		}elseif ($request->mes_periodo=='Julio') {
   			$periodo->numero_mes=7;
   		}elseif ($request->mes_periodo=='Agosto') {
   			$periodo->numero_mes=8;
   		}elseif ($request->mes_periodo=='Septiembre') {
   			$periodo->numero_mes=9;
   		}elseif ($request->mes_periodo=='Octubre') {
   			$periodo->numero_mes=10;
   		}elseif ($request->mes_periodo=='Noviembre') {
   			$periodo->numero_mes=11;
   		}elseif ($request->mes_periodo=='Diciembre') {
   			$periodo->numero_mes=12;
   		}

   		$periodo->anio = $request->anio_periodo;
   		$periodo->centro_id =$centro->id_centro;
   		$periodo->estado=1;
   		$periodo->save();

   	}

     Alert::success('Registrar Periodos de forma existosa')->autoClose(2000);
     return redirect()->action('Viaticos\PeriodosController@listarPeriodo');
   }


    public function activarPeriodo($id){
        $periodo = Periodo::find($id);
        $periodo->estado = 1;
        $periodo->update();

   Alert::success('Ha activado el Periodo', $periodo->mes.' '.$periodo->anio . $periodo->centro->centro. ' de forma existosa')->autoClose(1500);
     return redirect()->action('Viaticos\PeriodosController@listarPeriodo');
    }

    public function desactivarPeriodo($id){
          $periodo = Periodo::find($id);
        $periodo->estado = 0;
        $periodo->update();
        Alert::error('Ha desactivado el Periodo ', $periodo->mes.' '.$periodo->anio.' '.$periodo->centro->centro . ' de forma existosa')->autoClose(1500)->iconHtml('<i class="fa fa-trash-o"></i>');

         return redirect()->action('Viaticos\PeriodosController@listarPeriodo');

    }


    
}
