<?php

namespace App\Http\Controllers\Administracion;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Laracasts\Flash\Flash;
use Validator;
use App\Http\Requests\PermisoRequest;
use App\Http\Requests\EditarPermisoRequest;
use DB;
use Carbon\Carbon;
use RealRashid\SweetAlert\Facades\Alert;


class PermisosController extends Controller
{
    //Constructor para proteger funciones con permisos
    
    public function __construct(){

    $this->middleware(['permission:crear.permisos'], ['only' 	=> ['crearPermisos', 'guardarPermisos']]);
    $this->middleware(['permission:editar.permisos'], ['only' 	=> ['editarPermisos', 'actualizarPermisos']]);
    $this->middleware(['permission:eliminar.permisos'], ['only' => 'eliminarPermisos']);
    $this->middleware(['permission:consultar.permisos'], ['only' 	=> 'listarPermisos']);

    }


    /*Funcion para ver formulario de crear */

    public function crearPermisos(Request $request){
    	return view('administracion.gestionPermisos.crear');

	}

	/*Funcion para guardar permisos*/

	public function guardarPermisos(PermisoRequest $request){
		/*Se ha creado un Request para validar el formulario. 
		El request se identifica como PermisoRequest*/
		$permiso = new Permission();
		
		//No permite letras como ñ
		$permiso->name = $request->nombre_permiso;
		$permiso->save();
    	
 		Alert::success('Registrar Permiso', $permiso->name . ' de forma existosa')->autoClose(2000);
    	return redirect()->action('Administracion\PermisosController@listarPermisos');



	}

	public function editarPermisos($id)
	{
		$permiso = Permission::find($id); //obtiene el id del permiso
		//convierte el nonmbre del permiso obtenido en el formato normal Primera letra mayuscula y espacio reemplaza punto.
		return view('administracion.gestionPermisos.editar', compact('permiso'));
	}


	public function actualizarPermisos(EditarPermisoRequest $request, $id){

		$permiso = Permission::find($id); //obtiene el id del permiso
		$permiso->name =$request->nombre_permiso;	//guardamos el nombre de ese permiso en una 
    	$permiso->update();
    	Alert::warning('Actualizar Permiso', $permiso->name . ' de forma existosa')->autoClose(2000)->iconHtml('<i class="fa fa-pencil"></i>');
    	return redirect()->action('Administracion\PermisosController@listarPermisos');

	}

	/*Funcion para listar los permisos*/

	public function listarPermisos(){

		$permiso = Permission::orderBy('id', 'DESC')->get(); //Lista los permisos por el id de forma descendente

		return view('administracion.gestionPermisos.listar', compact('permiso')); //envia a la vista el array de permisos.
		

	}

	/* Función para eliminar permisos */
	public function eliminarPermisos($id){

		//$rol=Role::all();
		$permiso = Permission::find($id);

		$rol = DB::table('roles as r')->join('role_has_permissions as rp', 'r.id', '=', 'rp.role_id')->join('permissions as p', 'rp.permission_id', '=', 'p.id')->select('p.name as name')->where('p.name','=',$permiso->name)->get();  //se busca si el permiso esta en un rol
		
        	if (!$rol->isEmpty()) {  //verifica si hay un rol con ese permiso.
        		
        	    Alert::warning('Eliminar Privilegio ', $permiso->name . ' no se puede borrar porque ha sido asignado!')->autoClose(5000);
		        return redirect()->route('permisos.listar'); //muestra la vista de listado de permisos
        	}
        	else
        	{
                $permiso->delete();//elimina el permiso seleccionado
	            Alert::error('Eliminar Privilegio', $permiso->name . ' de forma existosa')->autoClose(2000)->iconHtml('<i class="fa fa-trash-o"></i>');
 //muestra mensaje del permiso que ha sido eliminado
		        return redirect()->route('permisos.listar'); //muestra la vista de listado de permisos
        	}
        
		
	}
}
