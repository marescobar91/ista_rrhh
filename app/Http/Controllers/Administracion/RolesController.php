<?php

namespace App\Http\Controllers\Administracion;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use App\Modelos\Administracion\Usuario;
use Spatie\Permission\Models\Permission;
use Laracasts\Flash\Flash;
use Validator;
use App\Http\Requests\RolRequest;
use App\Http\Requests\EditarRolRequest;
use DB;
use Illuminate\Database\Query\Builder;
use RealRashid\SweetAlert\Facades\Alert;

class RolesController extends Controller
{

    //Funcion para crear los permisos para los roles
   public function __construct(){

   $this->middleware(['permission:crear.roles'], ['only'    => ['crearRoles', 'guardarRoles']]);
    $this->middleware(['permission:editar.roles'], ['only'   => ['editarRoles', 'actualizarRoles']]);
    $this->middleware(['permission:eliminar.roles'], ['only' => 'eliminarRoles']);
    $this->middleware(['permission:consultar.roles'], ['only'   => 'listarRoles']);

    }
    //
    /*Funcion para mostrar el formulario de crear rol*/
    public function crearRoles(Request $request){
    	$permiso = Permission::get();  //Conseguir todos los permisos
    	return view('administracion.gestionRoles.crear', compact('permiso')); //Mandar a la vista Crear el array de Permisos.
    }

    public function guardarRoles(RolRequest $request){
        /*Se ha creado un Request para validar el formulario. 
        El request se identifica como RolRequest*/

    	$rol = new Role();
    	$rol->name = $request->nombre_rol;
    	$rol->save(); 

    	$rol->syncPermissions($request->get('permisos')); //sincroniza y guarda los permisos seleccionados al rol creado y guardado.

        //Mensaje de exito que se muestra en la vista Listar, mostrando el nombre del rol creado y guardado.
        Alert::success('Registrar Rol', $rol->name . ' de forma existosa')->autoClose(2000);
    	return redirect()->action('Administracion\RolesController@listarRoles');

    }

    public function listarRoles(){
    	$rol = Role::orderBy('id', 'DESC')->get(); //Se lista los roles por el id ordenandolos descendentemente

    	return view('administracion.gestionRoles.listar', compact('rol')); //Se envia a la vista listar el array de roles.
    }

    /* Función para editar Rol*/
    public function editarRoles($id){
        $rol = Role::find($id); // busca el rol que se va a editar.
        $permiso = Permission::all(); // muestra el listado de permisos
        $permission = $rol->permissions->pluck('id')->ToArray(); //Trae los permisos asignados
        
        return view('administracion.gestionRoles.editar')
        ->with('rol', $rol)
        ->with('permiso', $permiso)
        ->with('permission', $permission);

    }

    /* Función para actualizar datos de Rol*/
    public function actualizarRoles(EditarRolRequest $request, $id){
        $rol = Role::find($id);//busca el rol que se va a editar
        $rol->name = $request->nombre_rol; //muestra el nombre del rol
        $rol->syncPermissions($request->get('permisos'));
       // actualiza los permisos que han sido seleccionados o removidos.

        //dd($rol);
        $rol->update(); //actualiza el nombre del rol
        Alert::warning('Actualizar Rol', $rol->name . ' de forma existosa')->autoClose(2000)->iconHtml('<i class="fa fa-pencil"></i>'); //muestra mensaje que el rol ha sido editado con exito
        return redirect()->route('roles.listar'); //muestra el listado de roles
    }

    /* Función para eliminar datos de Rol*/
    public function eliminarRoles($id)
    {
       $rol = Role::find($id);  //Recupera el rol seleccionado mediante el id
       $usuarios = DB::table('admin_usuario as u')->join('model_has_roles as m', 'u.id_usuario', '=', 'm.model_id')->join('roles as r', 'm.role_id', '=', 'r.id')->select('u.nombre_usuario as nombre')->where('r.name','=',$rol->name)->get(); //busca en la base si hay un usuario con el rol asignado
        if (!$usuarios->isEmpty()) {  //verifica si el rol esta asignado
            Alert::warning('Eliminar Rol', $rol->name . ' ya ha sido asignado!')->autoClose(5000);
           return redirect()->action('Administracion\RolesController@listarRoles');  //redirige al listado de roles
       }
       else{
        $rol->delete();  //elimina el rol
        Alert::error('Eliminar Rol', $rol->name . ' de forma existosa')->autoClose(2000)->iconHtml('<i class="fa fa-trash-o"></i>');
        return redirect()->action('Administracion\RolesController@listarRoles');  //redirige al listado de roles
       }
       
       
    }
}
