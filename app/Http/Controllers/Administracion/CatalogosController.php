<?php

namespace App\Http\Controllers\Administracion;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CatalogosController extends Controller
{
    public function verCatalogos(){
    	//muestra el listado de catalogos
    	return view('administracion.gestionCatalogos.verCatalogos');
    }
}
