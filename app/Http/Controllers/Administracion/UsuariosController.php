<?php

namespace App\Http\Controllers\Administracion;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Modelos\Administracion\Usuario;
use App\Modelos\Empleado\Empleado;
use App\Modelos\Empleado\Oficina;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use App\Http\Requests\EditarUsuarioRequest;
use App\Http\Requests\PasswordRequest;
use App\Http\Requests\UsuarioRequest;
use Auth;
use DB;
use RealRashid\SweetAlert\Facades\Alert;

class UsuariosController extends Controller
{

  //Funcion para los permisos de usuarios
  public function __construct(){

   $this->middleware(['permission:crear.usuarios'], ['only'    => ['crearUsuarios', 'guardarUsuarios']]);
    $this->middleware(['permission:editar.usuarios'], ['only'   => ['editarUsuarios', 'actualizarUsuarios']]);
    $this->middleware(['permission:eliminar.usuarios'], ['only' => 'eliminarUsuarios']);
    $this->middleware(['permission:consultar.usuarios'], ['only'   => 'listarUsuarios']);
    $this->middleware(['permission:resetear.password'], ['only'   => ['cambiarContraseña', 'guardarNuevaContraseña']]);

    }

   public function crearUsuarios($id)
   {
    
         
     if(Auth::user()->hasRole('Gestor de Usuarios')){  //comparamos si el usuario autenfiticado es Gestor de Usuarios 
      $empleado = Empleado::find($id);  //obtenemos los datos de los empleados

      $usuarioExiste = Usuario::where('empleado_id', $empleado->id_empleado)->get(); //obtenemos cuantos usuarios tiene el empleado
        if(count($usuarioExiste) >= 1){ //validamos que el usuario solo este asignado a un empleado
          Alert::warning('Crear Usuario', $empleado->nombre .' '. $empleado->apellido .' ya tiene un usuario creado')->autoClose(2000);
        return redirect()->action('Administracion\UsuariosController@listarUsuarios');
        }else{
          //obtienen todos los roles a excepcion del Gestor de Usuario y Administrador
          $rol = Role::where('name', '<>', 'Gestor de Usuarios')->where('name', '<>', 'Administrador')->get();
          return view('administracion.gestionUsuarios.crear', compact('rol','empleado')); //Envia el arreglo de roles a la vista crear  
            }
     }
     else{

     $empleado = Empleado::find($id); //obtenemos los datos de los empleados
       $usuarioExiste = Usuario::where('empleado_id', $empleado->id_empleado)->get(); //obtenemos cuantos usuarios tiene el empleado
        if(count($usuarioExiste) >= 1){ //validamos que el usuario solo este asignado a un empleado
          Alert::warning('Crear Usuario', $empleado->nombre .' '. $empleado->apellido .' ya tiene un usuario creado')->autoClose(5000);
         
        return redirect()->action('Administracion\UsuariosController@listarUsuarios');
        }else{
          //obtenemos todos los roles
          $rol = Role::all();
          return view('administracion.gestionUsuarios.crear', compact('rol','empleado')); //Envia el arreglo de roles a la vista crear  
            }
     }
        
      
   }

      public function guardarUsuarios(UsuarioRequest $request, $id)
   {
    //se otienen los datos de empleado que se asignara un usuario
    $empleado = Empleado::find($id);
     $usuario = new Usuario();
     $usuario->empleado_id = $empleado->id_empleado;
     $usuario->nombre_usuario= $request->nombre_usuario;
     $usuario->email=$request->email;
     $usuario->password=bcrypt($request->password);
     $usuario->save();

     
     $usuario->syncRoles($request->get('rol'));
     if($usuario->hasRole('Secretaria')){ 
        //valida si se asigna un rol de secretaria para asignarle sus oficinas a cargo. 
        $idUsuario = $usuario->id_usuario;
        $accion = 'Crear';
      
       return redirect()->action('Administracion\UsuariosController@asignarOficinas', ['id' => $idUsuario, 'accion'=>$accion]);
     }else{
      //si no es secretaria regresa al listado de usuarios
     Alert::success('Registrar Usuario', $usuario->nombre_usuario . ' de forma existosa')->autoClose(2000);
     return redirect()->action('Administracion\UsuariosController@listarUsuarios');
   }
   }

    //
   /* Función para mostrar formulario de edit*/

   public function editarUsuarios($id){
    
    if(Auth::user()->hasRole('Gestor de Usuarios')){
         
         $rol = Role::where('name', '<>', 'Gestor de Usuarios')->where('name', '<>', 'Administrador')->get();
         $usuario = Usuario::find($id); //obtiene el id de un usuario en especifico.

         /*Manda a la vista el array de rol y el registro del usuario del id obtenido*/
         return view('administracion.gestionUsuarios.editar')->with('usuario', $usuario)->with('rol', $rol); 
      }
      else{
         $rol = Role::all(); //Obtiene un listado de roles por nombre y id
         $usuario = Usuario::find($id);
         $roles = Role::where('name', '<>', 'Gestor de Usuarios')->get();

         /*Manda a la vista el array de rol y el registro del usuario del id obtenido*/
         return view('administracion.gestionUsuarios.editar')->with('usuario', $usuario)->with('rol', $rol)->with('roles', $roles); 
      }

   }

   public function actualizarUsuarios(EditarUsuarioRequest $request, $id){

   	$usuario = Usuario::find($id); //obtiene el id del usuario especificado
   	$usuario->nombre_usuario = $request->nombre_usuario;
    $usuario->email = $request->email; 
   	$usuario->syncRoles($request->rol); 
    /*sincroniza el array de rol, de elegirse un rol nuevo, elimina el anterior de la base y registra el nuevo seleccionado*/
   	$usuario->update();
    /*Manda un mensaje de éxito al haber editado correctamente el usuario, mostrando el nombre del usuario modificado*/

    if($usuario->hasRole('Secretaria')){
      //valida si se asigna un rol de secretaria para asignarle sus oficinas a cargo. 
        $idUsuario = $usuario->id_usuario;
        $accion = 'Editar';
        return redirect()->action('Administracion\UsuariosController@asignarOficinas', ['id' => $idUsuario, 'accion'=>$accion]);
      }else{
         //si no es secretaria regresa al listado de usuarios
         Alert::warning('Actualizar Usuario', $usuario->nombre_usuario . ' de forma existosa')->autoClose(2000)->iconHtml('<i class="fa fa-pencil"></i>');
    return redirect()->action('Administracion\UsuariosController@listarUsuarios'); //Redirecciona a la vista de Lista
      }
   


   }

   public function listarUsuarios(){
    if(Auth::user()->hasRole(['Gestor de Claves', 'Gestor de Usuarios'])){

   	//$usuario=Usuario::whereHas("roles", function($q){ $q->where("name","<>", "Administrador"); })->get(); //Obtiene un array de usuarios a excepcion del administrador. 
    $usuarios = Usuario::with('roles')->get();
    $usuario = $usuarios->reject(function ($user, $key) {
            return $user->hasAnyRole('Administrador');
        });
    //dd($usuario);
    $usuarioDeshabilitado = Usuario::onlyTrashed()->get();
    $id = Auth::id();
   	return view('administracion.gestionUsuarios.listar', compact('usuario','id','usuarioDeshabilitado' )); /*Envia el array de usuario a la vista de Listar*/
   }
   else{
    $usuario=Usuario::all();//Obtiene un array de todos los usuarios
    $usuarioDeshabilitado = Usuario::onlyTrashed()->get();
    $id = Auth::id();
    return view('administracion.gestionUsuarios.listar', compact('usuario','id','usuarioDeshabilitado' )); /*Envia el array de usuario a la vista de Listar*/
   }
   }

   public function eliminarUsuarios($id){
      //SofDeletes
    $usuario = Usuario::find($id);
    $usuario->delete();
    Alert::error('Eliminar Usuario', $usuario->nombre_usuario. ' de forma existosa')->autoClose(2000)->iconHtml('<i class="fa fa-trash-o"></i>');
    //dd($id);
    return redirect()->action('Administracion\UsuariosController@listarUsuarios');
   }

   public function cambiarContraseña($id)
   {
     $usuario = Usuario::find($id);
     return view('administracion.gestionUsuarios.password')->with('usuario', $usuario);
   }

   public function guardarNuevaContraseña(PasswordRequest $request, $id)
   {
    //guarda la nueva contraseña
        $usuario = Usuario::find($id);
        $usuario->password = bcrypt($request->password);
        $usuario->update();
        Alert::success('Cambiar Contraseña', $usuario->nombre_usuario . ' de forma existosa')->autoClose(2000);
        return redirect()->action('Administracion\UsuariosController@listarUsuarios');
   }

   public function asignarEmpleado()
   {
    //listado para asignar usuario a los empleados
     $empleado = Empleado::orderBy('id_empleado', 'DESC')->get();
     return view('administracion.gestionUsuarios.asignarEmpleado', compact('empleado'));
   }

   public function activarUsuarios($id){
    //Habilita los usuarios inactivos
    $usuario = Usuario::onlyTrashed()->where('id_usuario', $id)->restore();
  
    $usuario = Usuario::find($id);
    Alert::success('Habilitar Usuario', $usuario->nombre_usuario . ' de forma existosa')->autoClose(2000);
    return redirect()->action('Administracion\UsuariosController@listarUsuarios');
      
    }


    public function asignarOficinas($id, $accion){
      //asigna las oficinas a la usuario con rol secretaria
      if($accion=='Crear'){
      $usuario = Usuario::find($id);
      //obtenermos la oficina del empleado con el usuario asignado
      $oficina = DB::table('emp_empleado as e')->leftJoin('emp_oficina as o', 'o.id_oficina', '=', 'e.oficina_id')->select('o.oficina', 'id_oficina')->where('e.id_empleado', '=', $usuario->empleado_id)->first();
      //se muestra la oficina actual 
      $oficinaActual = $oficina->id_oficina;
      $oficinas = Oficina::where('estado', '=', 1)->get();

      return view('administracion.gestionUsuarios.asignarSecretaria', compact('usuario', 'oficinaActual', 'oficinas', 'accion'));

      }elseif ($accion=='Editar') {
        $usuario = Usuario::find($id);
        //mostramos las oficinas asignadas al secretaria
        $oficinasActuales= $usuario->usuarioOficina->pluck('id_oficina')->ToArray();
        $oficinas = Oficina::where('estado', '=', 1)->get();
        return view('administracion.gestionUsuarios.asignarSecretaria', compact('usuario', 'oficinasActuales', 'oficinas', 'accion'));
      }
      
    }

    public function guardarUsuarioOficina(Request $request, $id){
      $usuario = Usuario::find($id);
      //asigna las oficinas para los empleados que seran usuario de rol secretaria
      $usuario->usuarioOficina()->sync($request->oficinas);
      $usuario->update();
      $accion = $request->accion;

      if($accion=='Crear'){
        Alert::success('Registrar Usuario', $usuario->nombre_usuario . ' de forma existosa')->autoClose(2000);
     return redirect()->action('Administracion\UsuariosController@listarUsuarios');

      }elseif ($accion=='Editar') {
      Alert::warning('Actualizar Usuario', $usuario->nombre_usuario . ' de forma existosa')->autoClose(2000)->iconHtml('<i class="fa fa-pencil"></i>');
    return redirect()->action('Administracion\UsuariosController@listarUsuarios');
      }
      

    }


}
