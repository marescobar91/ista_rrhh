<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Laracasts\Flash\Flash;
use Validator;

class AuthController extends Controller
{
    


        public function inicioSesion() {
                return view( 'auth.login' );
            }

        public function iniciarSesion( Request $request ) {
                $this->validate($request, [

                'nombre_usuario' => 'required|max:255',
                'password'    => 'required'
                ]);
                

            if ( Auth::attempt( [ 'nombre_usuario' => $request->nombre_usuario, 'password' => $request->password ]) ) {

                       // return response()->json( [ 'success' => true, 'message' => 'Acceso satisfactorio' ] );
                        return view('home');

                } else {
                        return back()->withErrors(['Estas credenciales no coinciden con nuestros registros','Este usuario esta deshabilitado']);
            }
        }

            //Cerrar session
        public function cerrarSesion() {

                Auth::logout();

                return redirect( '/' );
            }

            //Cerrar session
        public function salirSesion() {

                Auth::logout();

                return redirect( '/' );
            }
}
