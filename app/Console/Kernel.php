<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
       
     

    'App\Console\Commands\TareaMarcaciones',
    'App\Console\Commands\UpdateCalendario',
    'App\Console\Commands\SegundoCalendario',

        
    ];
 
    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        //Frecuencia de ejecucion de tarea
       $schedule->command('marcaciones:actualizar')->twiceDaily(8 , 17);
       $schedule->command('marcaciones:calendario')->yearly();
       $schedule->command('calendarios:actualizar')->yearly();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
