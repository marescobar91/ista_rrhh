<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
use Illuminate\Support\Str;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use Illuminate\Database\Query\Builder;
use App\Modelos\Marcacion\Calendario;
use App\Modelos\Empleado\Empleado;

class SegundoCalendario extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'calendarios:actualizar';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Se crea un segundo calendario del año siguiente';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $fecha = Carbon::now(new \DateTimeZone('America/El_Salvador'));
        $year=$fecha->year;
        $yearDos = $year+1;
        $anioDos = substr($yearDos, -2);
        //meses del año siguiente
        //
        $eneroDos = $yearDos.'-01-01';
        $diaEneroDos= date('t', strtotime($eneroDos));
        $febreroDos = $yearDos.'-02-01';
        $diaFebreroDos= date('t', strtotime($febreroDos));
        $marzoDos = $yearDos.'-03-01';
        $diaMarzoDos= date('t', strtotime($marzoDos));
        $abrilDos = $yearDos.'-04-01';
        $diaAbrilDos= date('t', strtotime($abrilDos));
        $mayoDos = $yearDos.'-05-01';
        $diaMayoDos= date('t', strtotime($mayoDos));
        $junioDos = $yearDos.'-06-01';
        $diaJunioDos= date('t', strtotime($junioDos));
        $julioDos = $yearDos.'-07-01';
        $diaJulioDos= date('t', strtotime($julioDos));
        $agostoDos = $yearDos.'-08-01';
        $diaAgostoDos= date('t', strtotime($agostoDos));
        $septiembreDos = $yearDos.'-09-01';
        $diaSeptiembreDos= date('t', strtotime($septiembreDos));
        $octubreDos = $yearDos.'-10-01';
        $diaOctubreDos= date('t', strtotime($octubreDos));
        $noviembreDos = $yearDos.'-11-01';
        $diaNoviembreDos= date('t', strtotime($noviembreDos));
        $diciembreDos = $yearDos.'-12-01';
        $diaDiciembreDos= date('t', strtotime($diciembreDos));


         $listadoCalendario = Calendario::all();
        if(!$listadoCalendario->isEmpty()){
       if(Calendario::where('anio', '=', $yearDos)->exists()){
        
       }else {



for ($i=1; $i <=$diaEneroDos ; $i++) {

        if($i<10){
            $fechaEnero[$i]=$yearDos.'-01-0'.$i;
            $calendario = new Calendario();
            $calendario->fecha = $fechaEnero[$i];
            $calendario->dia ='0'.$i;
            $calendario->mes = '01';
            $calendario->anio = $yearDos;
            $calendario->formato_fecha = '0'.$i.'01'.$anioDos;
            $calendario->save();
        } else{
            $fechaEnero[$i]=$yearDos.'-01-'.$i;
            $calendario = new Calendario();
            $calendario->fecha = $fechaEnero[$i];
            $calendario->dia =$i;
            $calendario->mes = '01';
            $calendario->anio = $yearDos;
            $calendario->formato_fecha = $i.'01'.$anioDos;
            $calendario->save();

        }
       }
       for ($i=1; $i <=$diaFebreroDos ; $i++) {

        if($i<10){
            $fechaFebrero[$i]=$yearDos.'-02-0'.$i;
            $calendario = new Calendario();
            $calendario->fecha = $fechaFebrero[$i];
            $calendario->dia ='0'.$i;
            $calendario->mes = '02';
            $calendario->anio = $yearDos;
            $calendario->formato_fecha = '0'.$i.'02'.$anioDos;
            $calendario->save();
        } else{
            $fechaFebrero[$i]=$yearDos.'-02-'.$i;
            $calendario = new Calendario();
            $calendario->fecha = $fechaFebrero[$i];
            $calendario->dia =$i;
            $calendario->mes = '02';
            $calendario->anio = $yearDos;
            $calendario->formato_fecha = $i.'02'.$anioDos;
            $calendario->save();

        }
       }

       for ($i=1; $i <=$diaMarzoDos ; $i++) {

        if($i<10){
            $fechaMarzo[$i]=$yearDos.'-03-0'.$i;
            $calendario = new Calendario();
            $calendario->fecha = $fechaMarzo[$i];
            $calendario->dia ='0'.$i;
            $calendario->mes = '03';
            $calendario->anio = $yearDos;
            $calendario->formato_fecha = '0'.$i.'03'.$anioDos;
            $calendario->save();
        } else{
            $fechaMarzo[$i]=$yearDos.'-03-'.$i;
            $calendario = new Calendario();
            $calendario->fecha = $fechaMarzo[$i];
            $calendario->dia =$i;
            $calendario->mes = '03';
            $calendario->anio = $yearDos;
            $calendario->formato_fecha = $i.'03'.$anioDos;
            $calendario->save();

        }
       }

        for ($i=1; $i <=$diaAbrilDos ; $i++) {

        if($i<10){
            $fechaAbril[$i]=$yearDos.'-04-0'.$i;
            $calendario = new Calendario();
            $calendario->fecha = $fechaAbril[$i];
            $calendario->dia ='0'.$i;
            $calendario->mes = '04';
            $calendario->anio = $yearDos;
            $calendario->formato_fecha = '0'.$i.'04'.$anioDos;
            $calendario->save();
        } else{
            $fechaAbril[$i]=$yearDos.'-04-'.$i;
            $calendario = new Calendario();
            $calendario->fecha = $fechaAbril[$i];
            $calendario->dia =$i;
            $calendario->mes = '04';
            $calendario->anio = $yearDos;
            $calendario->formato_fecha = $i.'04'.$anioDos;
            $calendario->save();

        }
       }

       for ($i=1; $i <=$diaMayoDos ; $i++) {

        if($i<10){
            $fechaMayo[$i]=$yearDos.'-05-0'.$i;
            $calendario = new Calendario();
            $calendario->fecha = $fechaMayo[$i];
            $calendario->dia ='0'.$i;
            $calendario->mes = '05';
            $calendario->anio = $yearDos;
            $calendario->formato_fecha = '0'.$i.'05'.$anioDos;
            $calendario->save();
        } else{
            $fechaMayo[$i]=$yearDos.'-05-'.$i;
            $calendario = new Calendario();
            $calendario->fecha = $fechaMayo[$i];
            $calendario->dia =$i;
            $calendario->mes = '05';
            $calendario->anio = $yearDos;
            $calendario->formato_fecha = $i.'05'.$anioDos;
            $calendario->save();

        }
       }

       for ($i=1; $i <=$diaJunioDos; $i++) {

        if($i<10){
            $fechaJunio[$i]=$yearDos.'-06-0'.$i;
            $calendario = new Calendario();
            $calendario->fecha = $fechaJunio[$i];
            $calendario->dia ='0'.$i;
            $calendario->mes = '06';
            $calendario->anio = $yearDos;
            $calendario->formato_fecha = '0'.$i.'06'.$anioDos;
            $calendario->save();
        } else{
            $fechaJunio[$i]=$yearDos.'-06-'.$i;
            $calendario = new Calendario();
            $calendario->fecha = $fechaJunio[$i];
            $calendario->dia =$i;
            $calendario->mes = '06';
            $calendario->anio = $yearDos;
            $calendario->formato_fecha = $i.'06'.$anioDos;
            $calendario->save();

        }
       }
       for ($i=1; $i <=$diaJulioDos ; $i++) {

        if($i<10){
            $fechaJulio[$i]=$yearDos.'-07-0'.$i;
            $calendario = new Calendario();
            $calendario->fecha = $fechaJulio[$i];
            $calendario->dia ='0'.$i;
            $calendario->mes = '07';
            $calendario->anio = $yearDos;
            $calendario->formato_fecha = '0'.$i.'07'.$anioDos;
            $calendario->save();
        } else{
            $fechaJulio[$i]=$yearDos.'-07-'.$i;
            $calendario = new Calendario();
            $calendario->fecha = $fechaJulio[$i];
            $calendario->dia =$i;
            $calendario->mes = '07';
            $calendario->anio = $yearDos;
            $calendario->formato_fecha = $i.'07'.$anioDos;
            $calendario->save();

        }
       }
       for ($i=1; $i <=$diaAgostoDos; $i++) {

        if($i<10){
            $fechaAgosto[$i]=$yearDos.'-08-0'.$i;
            $calendario = new Calendario();
            $calendario->fecha = $fechaAgosto[$i];
            $calendario->dia ='0'.$i;
            $calendario->mes = '08';
            $calendario->anio = $yearDos;
            $calendario->formato_fecha = '0'.$i.'08'.$anioDos;
            $calendario->save();
        } else{
            $fechaAgosto[$i]=$yearDos.'-08-'.$i;
            $calendario = new Calendario();
            $calendario->fecha = $fechaAgosto[$i];
            $calendario->dia =$i;
            $calendario->mes = '08';
            $calendario->anio = $yearDos;
            $calendario->formato_fecha = $i.'08'.$anioDos;
            $calendario->save();

        }
       }
       for ($i=1; $i <=$diaSeptiembreDos; $i++) {

        if($i<10){
            $fechaSeptiembre[$i]=$yearDos.'-09-0'.$i;
            $calendario = new Calendario();
            $calendario->fecha = $fechaSeptiembre[$i];
            $calendario->dia ='0'.$i;
            $calendario->mes = '09';
            $calendario->anio = $yearDos;
            $calendario->formato_fecha = '0'.$i.'09'.$anioDos;
            $calendario->save();
        } else{
            $fechaSeptiembre[$i]=$yearDos.'-09-'.$i;
            $calendario = new Calendario();
            $calendario->fecha = $fechaSeptiembre[$i];
            $calendario->dia =$i;
            $calendario->mes = '09';
            $calendario->anio = $yearDos;
            $calendario->formato_fecha = $i.'09'.$anioDos;
            $calendario->save();

        }
       }
        for ($i=1; $i <=$diaOctubreDos; $i++) {

        if($i<10){
            $fechaOctubre[$i]=$yearDos.'-10-0'.$i;
            $calendario = new Calendario();
            $calendario->fecha = $fechaOctubre[$i];
            $calendario->dia ='0'.$i;
            $calendario->mes = '10';
            $calendario->anio = $yearDos;
            $calendario->formato_fecha = '0'.$i.'10'.$anioDos;
            $calendario->save();
        } else{
            $fechaOctubre[$i]=$yearDos.'-10-'.$i;
            $calendario = new Calendario();
            $calendario->fecha = $fechaOctubre[$i];
            $calendario->dia =$i;
            $calendario->mes = '10';
            $calendario->anio = $yearDos;
            $calendario->formato_fecha = $i.'10'.$anioDos;
            $calendario->save();

        }
       }
        for ($i=1; $i <=$diaNoviembreDos; $i++) {

        if($i<10){
            $fechaNoviembre[$i]=$yearDos.'-11-0'.$i;
            $calendario = new Calendario();
            $calendario->fecha = $fechaNoviembre[$i];
            $calendario->dia ='0'.$i;
            $calendario->mes = '11';
            $calendario->anio = $yearDos;
            $calendario->formato_fecha = '0'.$i.'11'.$anioDos;
            $calendario->save();
        } else{
            $fechaNoviembre[$i]=$yearDos.'-11-'.$i;
            $calendario = new Calendario();
            $calendario->fecha = $fechaNoviembre[$i];
            $calendario->dia =$i;
            $calendario->mes = '11';
            $calendario->anio = $yearDos;
            $calendario->formato_fecha = $i.'11'.$anioDos;
            $calendario->save();

        }
       }

         for ($i=1; $i <=$diaDiciembreDos; $i++) {

        if($i<10){
            $fechaDiciembre[$i]=$yearDos.'-12-0'.$i;
            $calendario = new Calendario();
            $calendario->fecha = $fechaDiciembre[$i];
            $calendario->dia ='0'.$i;
            $calendario->mes = '12';
            $calendario->anio = $yearDos;
            $calendario->formato_fecha = '0'.$i.'12'.$anioDos;
            $calendario->save();
        } else{
            $fechaDiciembre[$i]=$yearDos.'-12-'.$i;
            $calendario = new Calendario();
            $calendario->fecha = $fechaDiciembre[$i];
            $calendario->dia =$i;
            $calendario->mes = '12';
            $calendario->anio = $yearDos;
            $calendario->formato_fecha = $i.'12'.$anioDos;
            $calendario->save();

        }
       }
       $calendarioActual = Calendario::where('anio', '=', $yearDos)->get();
        $empleado = Empleado::all(); 
        foreach($calendarioActual as $calendario){
            $arrayCalendario[] = $calendario->id_calendario;
        }

       foreach($empleado as $empleado){

        $empleado->empleadoCalendario()->attach($arrayCalendario);
        $empleado->update();
       }

   } //finaliza el if del calendario dos


      
} // finaliza el calendario que no existe
else{

     for ($i=1; $i <=$diaEneroDos ; $i++) {

        if($i<10){
            $fechaEnero[$i]=$yearDos.'-01-0'.$i;
            $calendario = new Calendario();
            $calendario->fecha = $fechaEnero[$i];
            $calendario->dia ='0'.$i;
            $calendario->mes = '01';
            $calendario->anio = $yearDos;
            $calendario->formato_fecha = '0'.$i.'01'.$anioDos;
            $calendario->save();
        } else{
            $fechaEnero[$i]=$yearDos.'-01-'.$i;
            $calendario = new Calendario();
            $calendario->fecha = $fechaEnero[$i];
            $calendario->dia =$i;
            $calendario->mes = '01';
            $calendario->anio = $yearDos;
            $calendario->formato_fecha = $i.'01'.$anioDos;
            $calendario->save();

        }
       }
       for ($i=1; $i <=$diaFebreroDos ; $i++) {

        if($i<10){
            $fechaFebrero[$i]=$yearDos.'-02-0'.$i;
            $calendario = new Calendario();
            $calendario->fecha = $fechaFebrero[$i];
            $calendario->dia ='0'.$i;
            $calendario->mes = '02';
            $calendario->anio = $yearDos;
            $calendario->formato_fecha = '0'.$i.'02'.$anioDos;
            $calendario->save();
        } else{
            $fechaFebrero[$i]=$yearDos.'-02-'.$i;
            $calendario = new Calendario();
            $calendario->fecha = $fechaFebrero[$i];
            $calendario->dia =$i;
            $calendario->mes = '02';
            $calendario->anio = $yearDos;
            $calendario->formato_fecha = $i.'02'.$anioDos;
            $calendario->save();

        }
       }

       for ($i=1; $i <=$diaMarzoDos ; $i++) {

        if($i<10){
            $fechaMarzo[$i]=$yearDos.'-03-0'.$i;
            $calendario = new Calendario();
            $calendario->fecha = $fechaMarzo[$i];
            $calendario->dia ='0'.$i;
            $calendario->mes = '03';
            $calendario->anio = $yearDos;
            $calendario->formato_fecha = '0'.$i.'03'.$anioDos;
            $calendario->save();
        } else{
            $fechaMarzo[$i]=$yearDos.'-03-'.$i;
            $calendario = new Calendario();
            $calendario->fecha = $fechaMarzo[$i];
            $calendario->dia =$i;
            $calendario->mes = '03';
            $calendario->anio = $yearDos;
            $calendario->formato_fecha = $i.'03'.$anioDos;
            $calendario->save();

        }
       }

        for ($i=1; $i <=$diaAbrilDos ; $i++) {

        if($i<10){
            $fechaAbril[$i]=$yearDos.'-04-0'.$i;
            $calendario = new Calendario();
            $calendario->fecha = $fechaAbril[$i];
            $calendario->dia ='0'.$i;
            $calendario->mes = '04';
            $calendario->anio = $yearDos;
            $calendario->formato_fecha = '0'.$i.'04'.$anioDos;
            $calendario->save();
        } else{
            $fechaAbril[$i]=$yearDos.'-04-'.$i;
            $calendario = new Calendario();
            $calendario->fecha = $fechaAbril[$i];
            $calendario->dia =$i;
            $calendario->mes = '04';
            $calendario->anio = $yearDos;
            $calendario->formato_fecha = $i.'04'.$anioDos;
            $calendario->save();

        }
       }

       for ($i=1; $i <=$diaMayoDos ; $i++) {

        if($i<10){
            $fechaMayo[$i]=$yearDos.'-05-0'.$i;
            $calendario = new Calendario();
            $calendario->fecha = $fechaMayo[$i];
            $calendario->dia ='0'.$i;
            $calendario->mes = '05';
            $calendario->anio = $yearDos;
            $calendario->formato_fecha = '0'.$i.'05'.$anioDos;
            $calendario->save();
        } else{
            $fechaMayo[$i]=$yearDos.'-05-'.$i;
            $calendario = new Calendario();
            $calendario->fecha = $fechaMayo[$i];
            $calendario->dia =$i;
            $calendario->mes = '05';
            $calendario->anio = $yearDos;
            $calendario->formato_fecha = $i.'05'.$anioDos;
            $calendario->save();

        }
       }

       for ($i=1; $i <=$diaJunioDos; $i++) {

        if($i<10){
            $fechaJunio[$i]=$yearDos.'-06-0'.$i;
            $calendario = new Calendario();
            $calendario->fecha = $fechaJunio[$i];
            $calendario->dia ='0'.$i;
            $calendario->mes = '06';
            $calendario->anio = $yearDos;
            $calendario->formato_fecha = '0'.$i.'06'.$anioDos;
            $calendario->save();
        } else{
            $fechaJunio[$i]=$yearDos.'-06-'.$i;
            $calendario = new Calendario();
            $calendario->fecha = $fechaJunio[$i];
            $calendario->dia =$i;
            $calendario->mes = '06';
            $calendario->anio = $yearDos;
            $calendario->formato_fecha = $i.'06'.$anioDos;
            $calendario->save();

        }
       }
       for ($i=1; $i <=$diaJulioDos ; $i++) {

        if($i<10){
            $fechaJulio[$i]=$yearDos.'-07-0'.$i;
            $calendario = new Calendario();
            $calendario->fecha = $fechaJulio[$i];
            $calendario->dia ='0'.$i;
            $calendario->mes = '07';
            $calendario->anio = $yearDos;
            $calendario->formato_fecha = '0'.$i.'07'.$anioDos;
            $calendario->save();
        } else{
            $fechaJulio[$i]=$yearDos.'-07-'.$i;
            $calendario = new Calendario();
            $calendario->fecha = $fechaJulio[$i];
            $calendario->dia =$i;
            $calendario->mes = '07';
            $calendario->anio = $yearDos;
            $calendario->formato_fecha = $i.'07'.$anioDos;
            $calendario->save();

        }
       }
       for ($i=1; $i <=$diaAgostoDos; $i++) {

        if($i<10){
            $fechaAgosto[$i]=$yearDos.'-08-0'.$i;
            $calendario = new Calendario();
            $calendario->fecha = $fechaAgosto[$i];
            $calendario->dia ='0'.$i;
            $calendario->mes = '08';
            $calendario->anio = $yearDos;
            $calendario->formato_fecha = '0'.$i.'08'.$anioDos;
            $calendario->save();
        } else{
            $fechaAgosto[$i]=$yearDos.'-08-'.$i;
            $calendario = new Calendario();
            $calendario->fecha = $fechaAgosto[$i];
            $calendario->dia =$i;
            $calendario->mes = '08';
            $calendario->anio = $yearDos;
            $calendario->formato_fecha = $i.'08'.$anioDos;
            $calendario->save();

        }
       }
       for ($i=1; $i <=$diaSeptiembreDos; $i++) {

        if($i<10){
            $fechaSeptiembre[$i]=$yearDos.'-09-0'.$i;
            $calendario = new Calendario();
            $calendario->fecha = $fechaSeptiembre[$i];
            $calendario->dia ='0'.$i;
            $calendario->mes = '09';
            $calendario->anio = $yearDos;
            $calendario->formato_fecha = '0'.$i.'09'.$anioDos;
            $calendario->save();
        } else{
            $fechaSeptiembre[$i]=$yearDos.'-09-'.$i;
            $calendario = new Calendario();
            $calendario->fecha = $fechaSeptiembre[$i];
            $calendario->dia =$i;
            $calendario->mes = '09';
            $calendario->anio = $yearDos;
            $calendario->formato_fecha = $i.'09'.$anioDos;
            $calendario->save();

        }
       }
        for ($i=1; $i <=$diaOctubreDos; $i++) {

        if($i<10){
            $fechaOctubre[$i]=$yearDos.'-10-0'.$i;
            $calendario = new Calendario();
            $calendario->fecha = $fechaOctubre[$i];
            $calendario->dia ='0'.$i;
            $calendario->mes = '10';
            $calendario->anio = $yearDos;
            $calendario->formato_fecha = '0'.$i.'10'.$anioDos;
            $calendario->save();
        } else{
            $fechaOctubre[$i]=$yearDos.'-10-'.$i;
            $calendario = new Calendario();
            $calendario->fecha = $fechaOctubre[$i];
            $calendario->dia =$i;
            $calendario->mes = '10';
            $calendario->anio = $yearDos;
            $calendario->formato_fecha = $i.'10'.$anioDos;
            $calendario->save();

        }
       }
        for ($i=1; $i <=$diaNoviembreDos; $i++) {

        if($i<10){
            $fechaNoviembre[$i]=$yearDos.'-11-0'.$i;
            $calendario = new Calendario();
            $calendario->fecha = $fechaNoviembre[$i];
            $calendario->dia ='0'.$i;
            $calendario->mes = '11';
            $calendario->anio = $yearDos;
            $calendario->formato_fecha = '0'.$i.'11'.$anioDos;
            $calendario->save();
        } else{
            $fechaNoviembre[$i]=$yearDos.'-11-'.$i;
            $calendario = new Calendario();
            $calendario->fecha = $fechaNoviembre[$i];
            $calendario->dia =$i;
            $calendario->mes = '11';
            $calendario->anio = $yearDos;
            $calendario->formato_fecha = $i.'11'.$anioDos;
            $calendario->save();

        }
       }

         for ($i=1; $i <=$diaDiciembreDos; $i++) {

        if($i<10){
            $fechaDiciembre[$i]=$yearDos.'-12-0'.$i;
            $calendario = new Calendario();
            $calendario->fecha = $fechaDiciembre[$i];
            $calendario->dia ='0'.$i;
            $calendario->mes = '12';
            $calendario->anio = $yearDos;
            $calendario->formato_fecha = '0'.$i.'12'.$anioDos;
            $calendario->save();
        } else{
            $fechaDiciembre[$i]=$yearDos.'-12-'.$i;
            $calendario = new Calendario();
            $calendario->fecha = $fechaDiciembre[$i];
            $calendario->dia =$i;
            $calendario->mes = '12';
            $calendario->anio = $yearDos;
            $calendario->formato_fecha = $i.'12'.$anioDos;
            $calendario->save();

        }
       }
       $calendarioActual = Calendario::where('anio', '=', $yearDos)->get();
        $empleado = Empleado::all(); 
        foreach($calendarioActual as $calendario){
            $arrayCalendario[] = $calendario->id_calendario;
        }

       foreach($empleado as $empleado){

        $empleado->empleadoCalendario()->attach($arrayCalendario);
        $empleado->update();
       }



   
} //finaliza else





    } //finaliza la funcion handle

} //finaliza la clase
