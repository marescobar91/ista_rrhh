<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
use Illuminate\Support\Str;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use Illuminate\Database\Query\Builder;
use App\Modelos\Empleado\Empleado;
use App\Modelos\Marcacion\Calendario;
class TareaMarcaciones extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    
    //se coloca el nombre de como se llamara el comando
    protected $signature = 'marcaciones:actualizar';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Se cargaron las marcaciones';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    //en los parantesis se inyecta la dependencia
    public function __construct()
    {
        
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $fecha = Carbon::now(new \DateTimeZone('America/El_Salvador'));
        $year=$fecha->year;
        
        $path = "storage/app/public/documentos".$year;
        if (!file_exists($path)) {
           mkdir($path, 0777, true);
           }
        //Apertura de archivos
        $archivo1=fopen("public/dispositivov3/TIMING.20", 'rb') or die ("se produjo un error al abrir archivo"); //fopen recibe la ruta del archivo, a partir de la carpeta public
        $archivo2=fopen("public/dispositivov2/_etcom.20", 'rb') or die ("se produjo un error al abrir archivo");
        $archivoMerge=fopen("$path/archivoMerge$year.txt", 'w') or die ("se produjo un error al abrir archivo");
        //Une archivos en uno nuevo
        while (!feof($archivo1)) {
            $linea = fgets($archivo1);
            $primer=substr($linea,0,1);
            if ($primer=="0") {
                $distintivo=substr($linea,0,5);
                $anyo=substr($linea,7,2);
                $mes=substr($linea,9,2);
                $dia=substr($linea,11,2);
                $fecha=$dia.$mes.$anyo;

                $fecha_formato='20'.$anyo.'-'.$mes.'-'.$dia;

                $hora=substr($linea,13,6);

                $horas =substr($linea,13,2);
                $minutos=substr($linea,15,2);
                $segundos=substr($linea,17,2);

                $formato_hora= $horas.':'.$minutos.':'.$segundos;
                //$fecha_hora = $fecha_formato.' '.$formato_hora;

                $pin=substr($linea,23,5);
                fwrite($archivoMerge,$distintivo.",".$fecha.",".$hora.",".$pin.",".$fecha_formato.",".$formato_hora."\n");       
            }
        }
        while (!feof($archivo2)) {
            $linea = fgets($archivo2);
            $primer=substr($linea,0,1);
            if ($primer=="0") {
                $distintivo=substr($linea,0,5);
                $fecha=substr($linea,5,6);

                $anyo=substr($linea,9,2);
                $mes=substr($linea,7,2);
                $dia=substr($linea,5,2);

                $fecha_formato='20'.$anyo.'-'.$mes.'-'.$dia;

                $hora=substr($linea,11,6);

                $horas =substr($linea,11,2);
                $minutos=substr($linea,13,2);
                $segundos=substr($linea,15,2);

                $formato_hora= $horas.':'.$minutos.':'.$segundos;
                //$fecha_hora=$fecha_formato.' '.$formato_hora;

                $pin=substr($linea,21,5);
                if (feof($archivo2)) {
                    fwrite($archivoMerge,$distintivo.",".$fecha.",".$hora.",".$pin.",".$fecha_formato.",".$formato_hora);
                }
                else
                {
                    fwrite($archivoMerge,$distintivo.",".$fecha.",".$hora.",".$pin.",".$fecha_formato.",".$formato_hora."\n");
                }
                }
            }

        //Cierra los archivos
        fclose($archivo1);
        fclose($archivo2);
        fclose($archivoMerge);

        $conteoR=0; //variable para el conteo de registros
        //Importacion de archivo a la Base
        $archivoMerge=fopen("$path/archivoMerge$year.txt", 'rb') or die ("se produjo un error al abrir archivo");
        while (!feof($archivoMerge)) {
            $linea = fgets($archivoMerge);
            $distintivo=substr($linea,0,5);
            $fecha=substr($linea,6,6);

            $hora=substr($linea,13,6);
            $pin=substr($linea,20,5);
            $pin_empleado = (int)$pin;

            $formato_fecha = substr($linea,26,10);
            $formato_hora =substr($linea,37,8);
            $fecha_hora =$formato_fecha.' '.$formato_hora;
            $empleado = Empleado::where('pin_empleado', '=', $pin_empleado)->get();
            foreach ($empleado as $empleado) {
                $oficina_id = $empleado->oficina_id;
                $empleado_id = $empleado->id_empleado;
               
            }
            $calendario = DB::table('mar_calendario as c')->leftJoin('mar_calendario_emp as ce', 'ce.calendario_id', '=', 'c.id_calendario')->select('ce.id_calendario_emp')->where('fecha', '=', $formato_fecha)->where('ce.empleado_id', '=', $empleado_id)->get();
            foreach ($calendario as $calendario) {
                $calendario_id = $calendario->id_calendario_emp;
            }
            
           
            $marcacion = [$distintivo,$fecha,$hora,$pin_empleado,$formato_fecha,$formato_hora,$fecha_hora,$oficina_id,$empleado_id,$calendario_id];
            //valida la existencia de registros ya existen en la base de datos
            $mar = DB::select('SELECT distintivo FROM mar_marcacion WHERE formato_fecha=? AND formato_hora=? AND pin_empleado=?',[$fecha,$hora,$pin_empleado]);              
            if($mar==null){
                DB::select('CALL marcaciones_sp(?,?,?,?,?,?,?,?,?,?)',$marcacion);    //procedimiento para importar las marcaciones
                $conteoR=$conteoR+1;
            }

        }//Fin while archivoMerge
        
        //agregar al historial de las marcaciones
        $nombre_archivo = filemtime('storage/app/public/documentos2021/archivoMerge2021.txt');
        date_default_timezone_set('America/El_Salvador');
        if (file_exists($nombre_archivo)) {

            $fechaH = date('Y-m-d');
            $horaH =  date('H:i:s');
            $historialMar = [$fechaH, $horaH, $conteoR];
            DB::select('CALL historialMarcaciones_sp(?,?,?)',$historialMar);
            }
            else {
                $fechaH = date('Y-m-d');
                $horaH =  date('H:i:s');
                $historialMar = [$fechaH, $horaH, $conteoR];
                DB::select('CALL historialMarcaciones_sp(?,?,?)',$historialMar);
            }

        fclose($archivoMerge); //Cierra archivoMerge 
    } // Fin handle
}
