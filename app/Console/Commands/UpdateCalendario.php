<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
use Illuminate\Support\Str;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use Illuminate\Database\Query\Builder;
use App\Modelos\Marcacion\Calendario;
use App\Modelos\Empleado\Empleado;

class UpdateCalendario extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    //se coloca el nombre de como se llamara el comando
    protected $signature = 'marcaciones:calendario';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Se crea calendario del año actual';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
       $fecha = Carbon::now(new \DateTimeZone('America/El_Salvador'));
        $year=$fecha->year;
        $anio = substr($year, -2);
        //meses del año actual
        $enero = $year.'-01-01';
        $diaEnero= date('t', strtotime($enero));
        $febrero = $year.'-02-01';
        $diaFebrero= date('t', strtotime($febrero));
        $marzo = $year.'-03-01';
        $diaMarzo= date('t', strtotime($marzo));
        $abril = $year.'-04-01';
        $diaAbril= date('t', strtotime($abril));
        $mayo = $year.'-05-01';
        $diaMayo= date('t', strtotime($mayo));
        $junio = $year.'-06-01';
        $diaJunio= date('t', strtotime($junio));
        $julio = $year.'-07-01';
        $diaJulio= date('t', strtotime($julio));
        $agosto = $year.'-08-01';
        $diaAgosto= date('t', strtotime($agosto));
        $septiembre = $year.'-09-01';
        $diaSeptiembre= date('t', strtotime($septiembre));
        $octubre = $year.'-10-01';
        $diaOctubre= date('t', strtotime($octubre));
        $noviembre = $year.'-11-01';
        $diaNoviembre= date('t', strtotime($noviembre));
        $diciembre = $year.'-12-01';
        $diaDiciembre= date('t', strtotime($diciembre));
        
        $listadoCalendario = Calendario::all();
        if(!$listadoCalendario->isEmpty()){
       if(Calendario::where('anio', '=', $year)->exists()){
        
       }else {
       for ($i=1; $i <=$diaEnero ; $i++) {

        if($i<10){
            $fechaEnero[$i]=$year.'-01-0'.$i;
            $calendario = new Calendario();
            $calendario->fecha = $fechaEnero[$i];
            $calendario->dia ='0'.$i;
            $calendario->mes = '01';
            $calendario->anio = $year;
            $calendario->formato_fecha = '0'.$i.'01'.$anio;
            $calendario->save();
        } else{
            $fechaEnero[$i]=$year.'-01-'.$i;
            $calendario = new Calendario();
            $calendario->fecha = $fechaEnero[$i];
            $calendario->dia =$i;
            $calendario->mes = '01';
            $calendario->anio = $year;
            $calendario->formato_fecha = $i.'01'.$anio;
            $calendario->save();

        }
       }
       for ($i=1; $i <=$diaFebrero ; $i++) {

        if($i<10){
            $fechaFebrero[$i]=$year.'-02-0'.$i;
            $calendario = new Calendario();
            $calendario->fecha = $fechaFebrero[$i];
            $calendario->dia ='0'.$i;
            $calendario->mes = '02';
            $calendario->anio = $year;
            $calendario->formato_fecha = '0'.$i.'02'.$anio;
            $calendario->save();
        } else{
            $fechaFebrero[$i]=$year.'-02-'.$i;
            $calendario = new Calendario();
            $calendario->fecha = $fechaFebrero[$i];
            $calendario->dia =$i;
            $calendario->mes = '02';
            $calendario->anio = $year;
            $calendario->formato_fecha = $i.'02'.$anio;
            $calendario->save();

        }
       }

       for ($i=1; $i <=$diaMarzo ; $i++) {

        if($i<10){
            $fechaMarzo[$i]=$year.'-03-0'.$i;
            $calendario = new Calendario();
            $calendario->fecha = $fechaMarzo[$i];
            $calendario->dia ='0'.$i;
            $calendario->mes = '03';
            $calendario->anio = $year;
            $calendario->formato_fecha = '0'.$i.'03'.$anio;
            $calendario->save();
        } else{
            $fechaMarzo[$i]=$year.'-03-'.$i;
            $calendario = new Calendario();
            $calendario->fecha = $fechaMarzo[$i];
            $calendario->dia =$i;
            $calendario->mes = '03';
            $calendario->anio = $year;
            $calendario->formato_fecha = $i.'03'.$anio;
            $calendario->save();

        }
       }

        for ($i=1; $i <=$diaAbril ; $i++) {

        if($i<10){
            $fechaAbril[$i]=$year.'-04-0'.$i;
            $calendario = new Calendario();
            $calendario->fecha = $fechaAbril[$i];
            $calendario->dia ='0'.$i;
            $calendario->mes = '04';
            $calendario->anio = $year;
            $calendario->formato_fecha = '0'.$i.'04'.$anio;
            $calendario->save();
        } else{
            $fechaAbril[$i]=$year.'-04-'.$i;
            $calendario = new Calendario();
            $calendario->fecha = $fechaAbril[$i];
            $calendario->dia =$i;
            $calendario->mes = '04';
            $calendario->anio = $year;
            $calendario->formato_fecha = $i.'04'.$anio;
            $calendario->save();

        }
       }

       for ($i=1; $i <=$diaMayo ; $i++) {

        if($i<10){
            $fechaMayo[$i]=$year.'-05-0'.$i;
            $calendario = new Calendario();
            $calendario->fecha = $fechaMayo[$i];
            $calendario->dia ='0'.$i;
            $calendario->mes = '05';
            $calendario->anio = $year;
            $calendario->formato_fecha = '0'.$i.'05'.$anio;
            $calendario->save();
        } else{
            $fechaMayo[$i]=$year.'-05-'.$i;
            $calendario = new Calendario();
            $calendario->fecha = $fechaMayo[$i];
            $calendario->dia =$i;
            $calendario->mes = '05';
            $calendario->anio = $year;
            $calendario->formato_fecha = $i.'05'.$anio;
            $calendario->save();

        }
       }

       for ($i=1; $i <=$diaJunio ; $i++) {

        if($i<10){
            $fechaJunio[$i]=$year.'-06-0'.$i;
            $calendario = new Calendario();
            $calendario->fecha = $fechaJunio[$i];
            $calendario->dia ='0'.$i;
            $calendario->mes = '06';
            $calendario->anio = $year;
            $calendario->formato_fecha = '0'.$i.'06'.$anio;
            $calendario->save();
        } else{
            $fechaJunio[$i]=$year.'-06-'.$i;
            $calendario = new Calendario();
            $calendario->fecha = $fechaJunio[$i];
            $calendario->dia =$i;
            $calendario->mes = '06';
            $calendario->anio = $year;
            $calendario->formato_fecha = $i.'06'.$anio;
            $calendario->save();

        }
       }
       for ($i=1; $i <=$diaJulio ; $i++) {

        if($i<10){
            $fechaJulio[$i]=$year.'-07-0'.$i;
            $calendario = new Calendario();
            $calendario->fecha = $fechaJulio[$i];
            $calendario->dia ='0'.$i;
            $calendario->mes = '07';
            $calendario->anio = $year;
            $calendario->formato_fecha = '0'.$i.'07'.$anio;
            $calendario->save();
        } else{
            $fechaJulio[$i]=$year.'-07-'.$i;
            $calendario = new Calendario();
            $calendario->fecha = $fechaJulio[$i];
            $calendario->dia =$i;
            $calendario->mes = '07';
            $calendario->anio = $year;
            $calendario->formato_fecha = $i.'07'.$anio;
            $calendario->save();

        }
       }
       for ($i=1; $i <=$diaAgosto; $i++) {

        if($i<10){
            $fechaAgosto[$i]=$year.'-08-0'.$i;
            $calendario = new Calendario();
            $calendario->fecha = $fechaAgosto[$i];
            $calendario->dia ='0'.$i;
            $calendario->mes = '08';
            $calendario->anio = $year;
            $calendario->formato_fecha = '0'.$i.'08'.$anio;
            $calendario->save();
        } else{
            $fechaAgosto[$i]=$year.'-08-'.$i;
            $calendario = new Calendario();
            $calendario->fecha = $fechaAgosto[$i];
            $calendario->dia =$i;
            $calendario->mes = '08';
            $calendario->anio = $year;
            $calendario->formato_fecha = $i.'08'.$anio;
            $calendario->save();

        }
       }
       for ($i=1; $i <=$diaSeptiembre; $i++) {

        if($i<10){
            $fechaSeptiembre[$i]=$year.'-09-0'.$i;
            $calendario = new Calendario();
            $calendario->fecha = $fechaSeptiembre[$i];
            $calendario->dia ='0'.$i;
            $calendario->mes = '09';
            $calendario->anio = $year;
            $calendario->formato_fecha = '0'.$i.'09'.$anio;
            $calendario->save();
        } else{
            $fechaSeptiembre[$i]=$year.'-09-'.$i;
            $calendario = new Calendario();
            $calendario->fecha = $fechaSeptiembre[$i];
            $calendario->dia =$i;
            $calendario->mes = '09';
            $calendario->anio = $year;
            $calendario->formato_fecha = $i.'09'.$anio;
            $calendario->save();

        }
       }
        for ($i=1; $i <=$diaOctubre; $i++) {

        if($i<10){
            $fechaOctubre[$i]=$year.'-10-0'.$i;
            $calendario = new Calendario();
            $calendario->fecha = $fechaOctubre[$i];
            $calendario->dia ='0'.$i;
            $calendario->mes = '10';
            $calendario->anio = $year;
            $calendario->formato_fecha = '0'.$i.'10'.$anio;
            $calendario->save();
        } else{
            $fechaOctubre[$i]=$year.'-10-'.$i;
            $calendario = new Calendario();
            $calendario->fecha = $fechaOctubre[$i];
            $calendario->dia =$i;
            $calendario->mes = '10';
            $calendario->anio = $year;
            $calendario->formato_fecha = $i.'10'.$anio;
            $calendario->save();

        }
       }
        for ($i=1; $i <=$diaNoviembre; $i++) {

        if($i<10){
            $fechaNoviembre[$i]=$year.'-11-0'.$i;
            $calendario = new Calendario();
            $calendario->fecha = $fechaNoviembre[$i];
            $calendario->dia ='0'.$i;
            $calendario->mes = '11';
            $calendario->anio = $year;
            $calendario->formato_fecha = '0'.$i.'11'.$anio;
            $calendario->save();
        } else{
            $fechaNoviembre[$i]=$year.'-11-'.$i;
            $calendario = new Calendario();
            $calendario->fecha = $fechaNoviembre[$i];
            $calendario->dia =$i;
            $calendario->mes = '11';
            $calendario->anio = $year;
            $calendario->formato_fecha = $i.'11'.$anio;
            $calendario->save();

        }
       }

         for ($i=1; $i <=$diaDiciembre; $i++) {

        if($i<10){
            $fechaDiciembre[$i]=$year.'-12-0'.$i;
            $calendario = new Calendario();
            $calendario->fecha = $fechaDiciembre[$i];
            $calendario->dia ='0'.$i;
            $calendario->mes = '12';
            $calendario->anio = $year;
            $calendario->formato_fecha = '0'.$i.'12'.$anio;
            $calendario->save();
        } else{
            $fechaDiciembre[$i]=$year.'-12-'.$i;
            $calendario = new Calendario();
            $calendario->fecha = $fechaDiciembre[$i];
            $calendario->dia =$i;
            $calendario->mes = '12';
            $calendario->anio = $year;
            $calendario->formato_fecha = $i.'12'.$anio;
            $calendario->save();

        }
       }
       $calendarioActual = Calendario::where('anio', '=', $year)->get();
        $empleado = Empleado::all(); 
        foreach($calendarioActual as $calendario){
            $arrayCalendario[] = $calendario->id_calendario;
        }

       foreach($empleado as $empleado){

        $empleado->empleadoCalendario()->attach($arrayCalendario);
        $empleado->update();
       }
      
   } //finalice el if de si no existe ese calendario de este año

    
}else //si no hay ningun calendario
    {
    for ($i=1; $i <=$diaEnero ; $i++) {

        if($i<10){
            $fechaEnero[$i]=$year.'-01-0'.$i;
            $calendario = new Calendario();
            $calendario->fecha = $fechaEnero[$i];
            $calendario->dia ='0'.$i;
            $calendario->mes = '01';
            $calendario->anio = $year;
            $calendario->formato_fecha = '0'.$i.'01'.$anio;
            $calendario->save();
        } else{
            $fechaEnero[$i]=$year.'-01-'.$i;
            $calendario = new Calendario();
            $calendario->fecha = $fechaEnero[$i];
            $calendario->dia =$i;
            $calendario->mes = '01';
            $calendario->anio = $year;
            $calendario->formato_fecha = $i.'01'.$anio;
            $calendario->save();

        }
       }
       for ($i=1; $i <=$diaFebrero ; $i++) {

        if($i<10){
            $fechaFebrero[$i]=$year.'-02-0'.$i;
            $calendario = new Calendario();
            $calendario->fecha = $fechaFebrero[$i];
            $calendario->dia ='0'.$i;
            $calendario->mes = '02';
            $calendario->anio = $year;
            $calendario->formato_fecha = '0'.$i.'02'.$anio;
            $calendario->save();
        } else{
            $fechaFebrero[$i]=$year.'-02-'.$i;
            $calendario = new Calendario();
            $calendario->fecha = $fechaFebrero[$i];
            $calendario->dia =$i;
            $calendario->mes = '02';
            $calendario->anio = $year;
            $calendario->formato_fecha = $i.'02'.$anio;
            $calendario->save();

        }
       }

       for ($i=1; $i <=$diaMarzo ; $i++) {

        if($i<10){
            $fechaMarzo[$i]=$year.'-03-0'.$i;
            $calendario = new Calendario();
            $calendario->fecha = $fechaMarzo[$i];
            $calendario->dia ='0'.$i;
            $calendario->mes = '03';
            $calendario->anio = $year;
            $calendario->formato_fecha = '0'.$i.'03'.$anio;
            $calendario->save();
        } else{
            $fechaMarzo[$i]=$year.'-03-'.$i;
            $calendario = new Calendario();
            $calendario->fecha = $fechaMarzo[$i];
            $calendario->dia =$i;
            $calendario->mes = '03';
            $calendario->anio = $year;
            $calendario->formato_fecha = $i.'03'.$anio;
            $calendario->save();

        }
       }

        for ($i=1; $i <=$diaAbril ; $i++) {

        if($i<10){
            $fechaAbril[$i]=$year.'-04-0'.$i;
            $calendario = new Calendario();
            $calendario->fecha = $fechaAbril[$i];
            $calendario->dia ='0'.$i;
            $calendario->mes = '04';
            $calendario->anio = $year;
            $calendario->formato_fecha = '0'.$i.'04'.$anio;
            $calendario->save();
        } else{
            $fechaAbril[$i]=$year.'-04-'.$i;
            $calendario = new Calendario();
            $calendario->fecha = $fechaAbril[$i];
            $calendario->dia =$i;
            $calendario->mes = '04';
            $calendario->anio = $year;
            $calendario->formato_fecha = $i.'04'.$anio;
            $calendario->save();

        }
       }

       for ($i=1; $i <=$diaMayo ; $i++) {

        if($i<10){
            $fechaMayo[$i]=$year.'-05-0'.$i;
            $calendario = new Calendario();
            $calendario->fecha = $fechaMayo[$i];
            $calendario->dia ='0'.$i;
            $calendario->mes = '05';
            $calendario->anio = $year;
            $calendario->formato_fecha = '0'.$i.'05'.$anio;
            $calendario->save();
        } else{
            $fechaMayo[$i]=$year.'-05-'.$i;
            $calendario = new Calendario();
            $calendario->fecha = $fechaMayo[$i];
            $calendario->dia =$i;
            $calendario->mes = '05';
            $calendario->anio = $year;
            $calendario->formato_fecha = $i.'05'.$anio;
            $calendario->save();

        }
       }

       for ($i=1; $i <=$diaJunio ; $i++) {

        if($i<10){
            $fechaJunio[$i]=$year.'-06-0'.$i;
            $calendario = new Calendario();
            $calendario->fecha = $fechaJunio[$i];
            $calendario->dia ='0'.$i;
            $calendario->mes = '06';
            $calendario->anio = $year;
            $calendario->formato_fecha = '0'.$i.'06'.$anio;
            $calendario->save();
        } else{
            $fechaJunio[$i]=$year.'-06-'.$i;
            $calendario = new Calendario();
            $calendario->fecha = $fechaJunio[$i];
            $calendario->dia =$i;
            $calendario->mes = '06';
            $calendario->anio = $year;
            $calendario->formato_fecha = $i.'06'.$anio;
            $calendario->save();

        }
       }
       for ($i=1; $i <=$diaJulio ; $i++) {

        if($i<10){
            $fechaJulio[$i]=$year.'-07-0'.$i;
            $calendario = new Calendario();
            $calendario->fecha = $fechaJulio[$i];
            $calendario->dia ='0'.$i;
            $calendario->mes = '07';
            $calendario->anio = $year;
            $calendario->formato_fecha = '0'.$i.'07'.$anio;
            $calendario->save();
        } else{
            $fechaJulio[$i]=$year.'-07-'.$i;
            $calendario = new Calendario();
            $calendario->fecha = $fechaJulio[$i];
            $calendario->dia =$i;
            $calendario->mes = '07';
            $calendario->anio = $year;
            $calendario->formato_fecha = $i.'07'.$anio;
            $calendario->save();

        }
       }
       for ($i=1; $i <=$diaAgosto; $i++) {

        if($i<10){
            $fechaAgosto[$i]=$year.'-08-0'.$i;
            $calendario = new Calendario();
            $calendario->fecha = $fechaAgosto[$i];
            $calendario->dia ='0'.$i;
            $calendario->mes = '08';
            $calendario->anio = $year;
            $calendario->formato_fecha = '0'.$i.'08'.$anio;
            $calendario->save();
        } else{
            $fechaAgosto[$i]=$year.'-08-'.$i;
            $calendario = new Calendario();
            $calendario->fecha = $fechaAgosto[$i];
            $calendario->dia =$i;
            $calendario->mes = '08';
            $calendario->anio = $year;
            $calendario->formato_fecha = $i.'08'.$anio;
            $calendario->save();

        }
       }
       for ($i=1; $i <=$diaSeptiembre; $i++) {

        if($i<10){
            $fechaSeptiembre[$i]=$year.'-09-0'.$i;
            $calendario = new Calendario();
            $calendario->fecha = $fechaSeptiembre[$i];
            $calendario->dia ='0'.$i;
            $calendario->mes = '09';
            $calendario->anio = $year;
            $calendario->formato_fecha = '0'.$i.'09'.$anio;
            $calendario->save();
        } else{
            $fechaSeptiembre[$i]=$year.'-09-'.$i;
            $calendario = new Calendario();
            $calendario->fecha = $fechaSeptiembre[$i];
            $calendario->dia =$i;
            $calendario->mes = '09';
            $calendario->anio = $year;
            $calendario->formato_fecha = $i.'09'.$anio;
            $calendario->save();

        }
       }
        for ($i=1; $i <=$diaOctubre; $i++) {

        if($i<10){
            $fechaOctubre[$i]=$year.'-10-0'.$i;
            $calendario = new Calendario();
            $calendario->fecha = $fechaOctubre[$i];
            $calendario->dia ='0'.$i;
            $calendario->mes = '10';
            $calendario->anio = $year;
            $calendario->formato_fecha = '0'.$i.'10'.$anio;
            $calendario->save();
        } else{
            $fechaOctubre[$i]=$year.'-10-'.$i;
            $calendario = new Calendario();
            $calendario->fecha = $fechaOctubre[$i];
            $calendario->dia =$i;
            $calendario->mes = '10';
            $calendario->anio = $year;
            $calendario->formato_fecha = $i.'10'.$anio;
            $calendario->save();

        }
       }
        for ($i=1; $i <=$diaNoviembre; $i++) {

        if($i<10){
            $fechaNoviembre[$i]=$year.'-11-0'.$i;
            $calendario = new Calendario();
            $calendario->fecha = $fechaNoviembre[$i];
            $calendario->dia ='0'.$i;
            $calendario->mes = '11';
            $calendario->anio = $year;
            $calendario->formato_fecha = '0'.$i.'11'.$anio;
            $calendario->save();
        } else{
            $fechaNoviembre[$i]=$year.'-11-'.$i;
            $calendario = new Calendario();
            $calendario->fecha = $fechaNoviembre[$i];
            $calendario->dia =$i;
            $calendario->mes = '11';
            $calendario->anio = $year;
            $calendario->formato_fecha = $i.'11'.$anio;
            $calendario->save();

        }
       }

         for ($i=1; $i <=$diaDiciembre; $i++) {

        if($i<10){
            $fechaDiciembre[$i]=$year.'-12-0'.$i;
            $calendario = new Calendario();
            $calendario->fecha = $fechaDiciembre[$i];
            $calendario->dia ='0'.$i;
            $calendario->mes = '12';
            $calendario->anio = $year;
            $calendario->formato_fecha = '0'.$i.'12'.$anio;
            $calendario->save();
        } else{
            $fechaDiciembre[$i]=$year.'-12-'.$i;
            $calendario = new Calendario();
            $calendario->fecha = $fechaDiciembre[$i];
            $calendario->dia =$i;
            $calendario->mes = '12';
            $calendario->anio = $year;
            $calendario->formato_fecha = $i.'12'.$anio;
            $calendario->save();

        }
       }
        $calendarioActual = Calendario::where('anio', '=', $year)->get();
        $empleado = Empleado::all(); 
        foreach($calendarioActual as $calendario){
            $arrayCalendario[] = $calendario->id_calendario;
        }

       foreach($empleado as $empleado){

        $empleado->empleadoCalendario()->attach($arrayCalendario);
        $empleado->update();
       }


} //finaliza else
 
  } //final del handle
} // final del la clase