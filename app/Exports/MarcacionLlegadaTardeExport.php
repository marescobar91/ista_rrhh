<?php

namespace App\Exports;

use App\Marcacion;
use Illuminate\Http\Request;
use DB;
use Maatwebsite\Excel\Concerns\Exportable;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class MarcacionLlegadaTardeExport implements FromView
{
    /**
    * @return \Illuminate\Support\Collection
    */
    use Exportable;


	public function __construct($fecha_inicio = null , $fecha_final = null){
		$this->fecha_inicio = $fecha_inicio;
        $this->fecha_final = $fecha_final;
	}
    /**
    * @return \Illuminate\Support\Collection
    */
    public function view(): View
    {
        $fecha_inicio = $this->fecha_inicio;
        $fecha_final= $this->fecha_final;


        $fechaMaxima = DB::table('llegada_tarde_fecha_vw')->whereBetween('fecha_marcacion', [$fecha_inicio, $fecha_final])->get();
      

        $marcacionTarde = DB::table('marcacion_llegada_tarde_vw as m')->select('m.id_empleado','m.nombre','m.apellido','m.codigo_empleado','m.semana','m.oficina','m.mes','m.unidad_administrativa', DB::raw('SEC_TO_TIME(SUM(TIME_TO_SEC(m.minutos))) as minutos'),  'm.tipo_plaza', 'm.salario_actual')->whereNull('fecha_asueto')->whereNull('permiso_fecha')->whereBetween('m.fecha_marcacion',[$fecha_inicio,$fecha_final])->groupBy('m.id_empleado','m.nombre','m.apellido','m.codigo_empleado','m.semana','m.oficina','m.unidad_administrativa','m.mes', 'm.tipo_plaza', 'm.salario_actual')->get();


        $vacio="Existe";

        foreach ($fechaMaxima as $maxima) {
            foreach ($marcacionTarde as $marcacion) {
                if($maxima->semana==$marcacion->semana && $maxima->mes ==$marcacion->mes && $maxima->id_empleado == $marcacion->id_empleado){
                    if($marcacion->minutos > '00:25:00'){
                        $llegadaTarde[]=[
                        $marcacion->codigo_empleado,
                        $marcacion->nombre, 
                        $marcacion->apellido,
                        $marcacion->unidad_administrativa, 
                        $marcacion->oficina, 
                        $marcacion->tipo_plaza, 
                        $marcacion->salario_actual, 
                        $maxima->fecha_marcacion, 
                        date('H:i:s',strtotime($marcacion->minutos) - strtotime('00:25:00')), 
                        $marcacion->mes];
                    }
                    
                  

                }
            }
        }
        

        return view("marcacion.reporteMarcacion.excelLlegadaTarde", compact('llegadaTarde','fecha_inicio','fecha_final'));
    }
}
    