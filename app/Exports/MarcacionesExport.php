<?php

namespace App\Exports;

use App\Marcacion;
use Illuminate\Http\Request;
use DB;
use Maatwebsite\Excel\Concerns\Exportable;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class MarcacionesExport implements FromView
{
    /**
    * @return \Illuminate\Support\Collection
    */
    use Exportable;


	public function __construct($fecha_inicio = null , $fecha_final = null){
		$this->fecha_inicio = $fecha_inicio;
        $this->fecha_final = $fecha_final;
	}
    /**
    * @return \Illuminate\Support\Collection
    */
    public function view(): View
    {
        $fecha_inicio = $this->fecha_inicio;
        $fecha_final= $this->fecha_final;
         $marcacion = DB::table('marcaciones_ausencia_vw')->select('codigo_empleado', 'nombre','apellido', 'oficina','fecha','mes', 'salario','oficina','tipo_plaza')->whereBetween('fecha', [$fecha_inicio, $fecha_final])->whereNull('fecha_marcacion')->whereNull('fecha_asueto')->whereNull('fecha_permiso')->whereRaw('WEEKDAY(fecha) <> 5')->whereRaw('WEEKDAY(fecha) <> 6')->get();

        return view("marcacion.reporteMarcacion.excelAusencia", compact('marcacion','fecha_inicio','fecha_final'));
    }
}
    