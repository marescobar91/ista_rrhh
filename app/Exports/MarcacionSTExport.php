<?php

namespace App\Exports;

use App\Modelos\Marcacion\Marcacion;
use Illuminate\Http\Request;
use DB;
use Maatwebsite\Excel\Concerns\Exportable;
//use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class MarcacionSTExport implements FromView
{
     /**
    * @return \Illuminate\Support\Collection
    */
    use Exportable;


	public function __construct($fecha_inicio = null , $fecha_final = null){
		$this->fecha_inicio = $fecha_inicio;
        $this->fecha_final = $fecha_final;
	}
    /**
    * @return \Illuminate\Support\Collection
    */
    public function view(): View{
    	$fecha_inicio = $this->fecha_inicio;

        $fecha_final= $this->fecha_final;
        

    	 $marcacionMax = DB::table('marcacion_salida_temprano_vw as m')->select('m.nombre','m.apellido','m.codigo_empleado','m.fecha_marcacion','m.oficina','m.mes','m.salario','m.tipo','m.minutos')->whereBetween('m.fecha_marcacion',[$fecha_inicio,$fecha_final])->whereNull('permiso_fecha')->get();

        	$descuento = '15:30:00'; 

        return view("marcacion.reporteMarcacion.excelSalidaTemprano", compact('marcacionMax','descuento','fecha_inicio','fecha_final'));

    }
}
    