<?php

namespace App\Exports;

use App\Modelos\Marcacion\HoraExtra;
//use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Http\Request;
use DB;
use Maatwebsite\Excel\Concerns\Exportable;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class HorasExtrasExport implements FromView
{
	use Exportable;
	protected $mes;
  protected $fechaIni;
  protected $fechaFin;

	public function __construct($mes = null,$fechaIni = null, $fechaFin=null){
		    $this->mes = $mes;
        $this->fechaIni = $fechaIni;
        $this->fechaFin = $fechaFin;
	}
    /**
    * @return \Illuminate\Support\Collection
    */
    public function view(): View
    {
        $mes = $this->mes;
        $fechaIni= $this->fechaIni;
        $fechaFin= $this->fechaFin;
  
        $contrato = 'Contrato';
      $ley = "Ley de Salarios";
        $horasExtrasC = DB::table('generar_horas_extras_vw')->where('mes', $mes)->whereBetween('fecha_registro',[$fechaIni, $fechaFin])->where('tipo_plaza',$contrato)->orderBy('unidad_administrativa','ASC')->get();

       $devengoC = DB::table('generar_horas_extras_vw')->select( DB::raw('ROUND(SUM(devengo),2) as Devengo'))->where('mes', $mes)->whereBetween('fecha_registro',[$fechaIni, $fechaFin])->where('tipo_plaza',$contrato)->first();
       $totalC = DB::table('generar_horas_extras_vw')->select( DB::raw('ROUND(SUM(total),2) as Total'))->where('mes', $mes)->whereBetween('fecha_registro',[$fechaIni, $fechaFin])->where('tipo_plaza',$contrato)->first();

        $horasExtrasL = DB::table('generar_horas_extras_vw')->where('mes', $mes)->whereBetween('fecha_registro',[$fechaIni, $fechaFin])->where('tipo_plaza',$ley)->orderBy('unidad_administrativa','ASC')->get();
       $devengoL = DB::table('generar_horas_extras_vw')->select( DB::raw('ROUND(SUM(devengo),2) as Devengo'))->where('mes', $mes)->whereBetween('fecha_registro',[$fechaIni, $fechaFin])->where('tipo_plaza',$ley)->first();
       $totalL = DB::table('generar_horas_extras_vw')->select( DB::raw('ROUND(SUM(total),2) as Total'))->where('mes', $mes)->whereBetween('fecha_registro',[$fechaIni, $fechaFin])->where('tipo_plaza',$ley)->first();
     

        return view("marcacion.gestionHorasExtras.excelReporte", compact('horasExtrasC','devengoC','totalC','horasExtrasL','devengoL','totalL'));
    }

    
}
