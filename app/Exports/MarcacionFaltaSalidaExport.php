<?php

namespace App\Exports;

use App\Modelos\Marcacion\Marcacion;
use DB;
use Maatwebsite\Excel\Concerns\Exportable;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;


class MarcacionFaltaSalidaExport implements FromView
{
    /**
    * @return \Illuminate\Support\Collection
    */
     /**
    * @return \Illuminate\Support\Collection
    */
    use Exportable;


	public function __construct($fecha_inicio = null , $fecha_final = null){
		$this->fecha_inicio = $fecha_inicio;
        $this->fecha_final = $fecha_final;
	}
    /**
    * @return \Illuminate\Support\Collection
    */
    public function view(): View{

    	$fecha_inicio = $this->fecha_inicio;
        $fecha_final= $this->fecha_final;

        $marcacionFaltaEnt =DB::table('marcacion_falta_salida_vw as m')->select('m.id_empleado','m.nombre','m.apellido','m.codigo_empleado','m.oficina','m.fecha_marcacion', 'm.hora_marcacion','m.tipo','m.mes','m.salario')->whereBetween('m.fecha_marcacion',[$fecha_inicio,$fecha_final])->get();
       $marcacionFalta = DB::table('mar_marcacion as m')->select(DB::raw('COUNT(m.empleado_id) as total'), 'm.empleado_id as empleado', 'm.fecha')->whereBetween('m.fecha', [$fecha_inicio, $fecha_final])->groupBy('m.fecha', 'm.empleado_id')->havingRaw('COUNT(m.empleado_id) = ?', [1])->get();


        return view("marcacion.reporteMarcacion.excelFaltaSalida", compact('marcacionFaltaEnt','marcacionFalta','fecha_inicio','fecha_final'));

    }
}
