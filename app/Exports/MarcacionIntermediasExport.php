<?php

namespace App\Exports;

use App\Marcacion;
use Illuminate\Http\Request;
use DB;
use Maatwebsite\Excel\Concerns\Exportable;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class MarcacionIntermediasExport implements FromView
{
    /**
    * @return \Illuminate\Support\Collection
    */
 use Exportable;


	public function __construct($fechaInicio = null , $fechaFin = null){
		$this->fechaInicio = $fechaInicio;
        $this->fechaFin = $fechaFin;
	}
    /**
    * @return \Illuminate\Support\Collection
    */
    public function view(): View
    {
        $fechaInicio = $this->fechaInicio;
        $fechaFin= $this->fechaFin;
         $marcacionInter = DB::table('marcacion_intermedias_vw as m')->whereBetween('m.fecha_marcacion', [$fechaInicio, $fechaFin])->get();


       $marcacionIntermedia = DB::table('mar_marcacion as m')->select(DB::raw('COUNT(m.empleado_id) as total'), DB::raw('MAX(m.hora) as max'),DB::raw('MIN(m.hora) as min'),'m.empleado_id as empleado', 'm.fecha')->whereBetween('m.fecha', [$fechaInicio, $fechaFin])->groupBy('m.fecha', 'm.empleado_id')->havingRaw('COUNT(m.empleado_id) = ?', [4])->get();

         foreach ($marcacionInter as $marc) {
            foreach ($marcacionIntermedia as $marcaciones) {
                if($marc->id_empleado ==$marcaciones->empleado && $marc->fecha_marcacion ==$marcaciones->fecha){
                    if($marc->hora_marcacion != $marcaciones->max && $marc->hora_marcacion != $marcaciones->min){
                       $intermedias[] = [$marc->codigo_empleado, $marc->nombre, $marc->apellido, $marc->oficina, $marc->fecha_marcacion, $marc->tipo, $marc->salario, $marc->mes, $marc->hora_marcacion]; 
                    }
                }
            }
        }

       foreach($intermedias as $key => $marc){
        foreach ($intermedias as $key => $marcacion) {
            if($marc[0]==$marcacion[0] && $marc[4]==$marcacion[4]){
            if($marc[8]<$marcacion[8]){
                $codigo_empleado = $marc[0];
                $nombre = $marc[1];
                $apellido=$marc[2];
                $oficina=$marc[3];
                $fecha_marcacion=$marc[4];
                $tipo=$marc[5];
                $salario=$marc[6];
                $mes=$marc[7];
                $hora_marcacion=$marc[8];
                $intermediaMin[] = [$codigo_empleado,$nombre,$apellido,$oficina,$fecha_marcacion,$tipo,$salario,$mes,$hora_marcacion];
            }
        }
        } 

}

  foreach($intermedias as $key => $marc){
        foreach ($intermedias as $key => $marcacion) {
            if($marc[0]==$marcacion[0] && $marc[4]==$marcacion[4]){
            if($marc[8]<$marcacion[8]){
                $codigo_empleado = $marcacion[0];
                $nombre = $marcacion[1];
                $apellido=$marcacion[2];
                $oficina=$marcacion[3];
                $fecha_marcacion=$marcacion[4];
                $tipo=$marcacion[5];
                $salario=$marcacion[6];
                $mes=$marcacion[7];
                $hora_marcacion=$marcacion[8];
                $intermediaMax[] = [$codigo_empleado,$nombre,$apellido,$oficina,$fecha_marcacion,$tipo,$salario,$mes,$hora_marcacion];
            }
        }
        } 

}

        return view("marcacion.reporteMarcacion.excelIntermedias", compact('intermediaMax', 'intermediaMin','fechaInicio','fechaFin'));
    }
}
    