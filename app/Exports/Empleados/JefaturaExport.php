<?php

namespace App\Exports\Empleados;

use App\Modelos\Empleado\Empleado;
//use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Illuminate\Http\Request;
use DB;


class JefaturaExport implements FromView
{
     use Exportable;
    public function __construct(){
		    
	}
    /**
    * @return \Illuminate\Support\Collection
    */
    public function view(): View
    {
  
        $jefatura = DB::table('jefaturas_vw')->get();
        return view("empleados.gestionReportes.excelJefaturas", compact('jefatura'));
    }
}
