<?php

namespace App\Exports\Empleados;

use App\Modelos\Empleado\Empleado;
//use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Illuminate\Http\Request;
use DB;

class UnidadesExport implements FromView
{
    use Exportable;
    public function __construct(){
		    
	}
    /**
    * @return \Illuminate\Support\Collection
    */
    public function view(): View
    {
        
        $unidades = DB::table('reporte_unidad_vw')->OrderBy('unidad_administrativa')->get();
        $unidad = DB::table('unidades_vw')->OrderBy('unidad_administrativa')->get();
        return view("empleados.gestionReportes.excelUnidad", compact('unidades','unidad'));
    }
}
