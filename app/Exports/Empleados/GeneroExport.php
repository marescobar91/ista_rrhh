<?php

namespace App\Exports\Empleados;

use App\Modelos\Empleado\Empleado;
//use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Illuminate\Http\Request;
use DB;

class GeneroExport implements FromView
{
    use Exportable;
    public function __construct(){
		    
	}
    /**
    * @return \Illuminate\Support\Collection
    */
    public function view(): View
    {
        
        $genero = DB::table('genero_vw')->select('Oficina',  DB::raw('COUNT(Hombre) as hombre'), DB::raw('COUNT(Mujer) as mujer'), DB::raw('COUNT(ID) as total'))->groupBy('Oficina')->get();
        return view("empleados.gestionReportes.excelGenero", compact('genero'));
    }
}
