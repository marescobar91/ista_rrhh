<?php

namespace App\Exports\Empleados;

use App\Modelos\Empleado\Empleado;
//use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Illuminate\Http\Request;
use DB;


class ReporteUnoExport implements FromView
{
    use Exportable;
  	protected $departamento;
  	protected $direccion;
 

	public function __construct($departamento = null,$direccion = null){
        $this->departamento = $departamento;
        $this->direccion = $direccion;
	}
    /**
    * @return \Illuminate\Support\Collection
    */
    public function view(): View
    {
        $departamento = $this->departamento;
        $direccion = $this->direccion;
  
       $empleado = DB::table('empleado_vw')->select('codigo_empleado', 'nombre', 'apellido', 'sexo', 'unidad_administrativa', 'oficina', 'direccion', 'cargo_mp','departamento')->get();
      // dd($departamento);
       return view("empleados.gestionReportes.excelReporteUno", compact('empleado','departamento','direccion'));

    }
}
