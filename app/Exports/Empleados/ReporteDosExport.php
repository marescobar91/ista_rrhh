<?php

namespace App\Exports\Empleados;

use App\Modelos\Empleado\Empleado;
//use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Illuminate\Http\Request;
use DB;


class ReporteDosExport implements FromView
{
     use Exportable;
  	protected $fechaNacimiento;
  	protected $edad;
 

	public function __construct($fechaNacimiento = null,$edad = null){
        $this->fechaNacimiento = $fechaNacimiento;
        $this->edad = $edad;
	}
    /**
    * @return \Illuminate\Support\Collection
    */
    public function view(): View
    {
        $fechaNacimiento = $this->fechaNacimiento;
        $edad = $this->edad;
  
       $empleado = DB::table('empleado_vw')->select('codigo_empleado', 'nombre', 'apellido', 'sexo', 'unidad_administrativa', 'oficina', 'fecha_nacimiento', 'cargo_mp')->get();
      // dd($departamento);
       return view("empleados.gestionReportes.excelReporteDos", compact('empleado','fechaNacimiento','edad'));

    }
}
