<?php

namespace App\Exports\Empleados;

use App\Modelos\Empleado\Empleado;
//use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Illuminate\Http\Request;
use DB;


class ReporteTresExport implements FromView
{
    use Exportable;
    protected $salario;
  	protected $fechaIngreso;
  	protected $tipoPlaza;
 

	public function __construct($salario = null,$fechaIngreso = null, $tipoPlaza = null){
        $this->salario = $salario;
        $this->fechaIngreso = $fechaIngreso;
        $this->tipoPlaza = $tipoPlaza;
	}
    /**
    * @return \Illuminate\Support\Collection
    */
    public function view(): View
    {
        $salario = $this->salario;
        $fechaIngreso = $this->fechaIngreso;
        $tipoPlaza = $this->tipoPlaza;
  
       $empleado = DB::table('empleado_vw')->select('codigo_empleado', 'nombre', 'apellido', 'sexo', 'unidad_administrativa', 'oficina', 'tipo_plaza','fecha_ingreso', 'salario_actual', 'cargo_mp')->get();
      // dd($departamento);
       return view("empleados.gestionReportes.excelReporteTres", compact('empleado','fechaIngreso','tipoPlaza','salario'));

    }
}
