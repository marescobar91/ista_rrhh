<?php

namespace App\Modelos\Administracion;


use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Permission\Traits\HasRoles;
use App\Notifications\UsuarioResetarContrasena;

class Usuario extends Authenticatable
{
    use Notifiable;
    use HasRoles { restore as private restoreA; }
    use SoftDeletes { restore as private restoreB; }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
       public function restore()
    {
        $this->restoreA();
        $this->restoreB();
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     *
     */
    protected $table = 'admin_usuario';
    protected $primaryKey = 'id_usuario';


    protected $dates = ['deleted_at'];

    protected $fillable = [
        'empleado_id','nombre_usuario', 'email', 'password',
    ];


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function empleado(){

        return $this->belongsTo('App\Modelos\Empleado\Empleado', 'empleado_id', 'id_empleado');
    }

    public function historial(){

        return $this->hasMany('App\Modelos\Empleado\Historial', 'usuario_id', 'id_usuario');

    }


     public function sendPasswordResetNotification($token)
    {
        $this->notify(new UsuarioResetarContrasena($token));
    }


    public function usuarioOficina(){
        return $this->belongsToMany('App\Modelos\Empleado\Oficina','admin_usuario_oficina','usuario_id','oficina_id');
    }
}
