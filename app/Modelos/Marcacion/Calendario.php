<?php

namespace App\Modelos\Marcacion;

use Illuminate\Database\Eloquent\Model;

class Calendario extends Model
{
    protected $table = 'mar_calendario';
    protected $primaryKey = 'id_calendario';

    protected $fillable = [
    	'fecha','dia','mes','anio','formato_fecha'
    ];

    public $timestamps = true;


     public function calendarioEmpleado(){
        return $this->belongsToMany('App\Modelos\Empleado\Empleado','mar_calendario_emp','empleado_id','calendario_id');
    }

    
}
