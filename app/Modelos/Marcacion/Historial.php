<?php

namespace App\Modelos\Marcacion;

use Illuminate\Database\Eloquent\Model;

class Historial extends Model
{
    protected $table = 'mar_historial';
    protected $primaryKey = 'id_historial';

    protected $fillable = [
    	'fecha_importar','hora_importar','cantidad_registros'
    ];

    public $timestamps = true;
}
