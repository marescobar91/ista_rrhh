<?php

namespace App\Modelos\Marcacion;

use Illuminate\Database\Eloquent\Model;

class HoraExtra extends Model
{
    protected $table = 'mar_hora_extra';
    protected $primaryKey = 'id_hora_extra';

    protected $fillable = [
    	'empleado_id','fecha_desde','fecha_hasta','horas','minutos','fecha_registro','devengo','isss','insaforp','inpep','afp','total','mes'
    ];


    public function empleado(){
        return $this->belongsTo('App\Modelos\Empleado\Empleado', 'empleado_id', 'id_empleado');
        }
}
