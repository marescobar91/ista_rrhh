<?php

namespace App\Modelos\Marcacion;

use Illuminate\Database\Eloquent\Model;

class Asueto extends Model
{
    protected $table = 'mar_asueto';
    protected $primaryKey = 'id_asueto';

    protected $fillable = [
    	'fecha','asueto','dia','mes','anio','formato_fecha', 'emp_calendario_id'
    ];

    public $timestamps = true;
}
