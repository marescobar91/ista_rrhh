<?php

namespace App\Modelos\Marcacion;

use Illuminate\Database\Eloquent\Model;

class Marcacion extends Model
{
    protected $table = 'mar_marcacion';
    protected $primaryKey = 'id_marcacion';

    protected $fillable = [
    	'id_marcacion',	'distintivo',	'formato_fecha', 'formato_hora', 'pin_empleado', 'fecha',	'hora',	'fecha_hora','oficina_id',	'empleado_id', 'emp_calendario_id'
    ];

    public $timestamps = true;

     public function oficina(){
        return $this->belongsTo('App\Modelos\Empleado\Oficina', 'oficina_id', 'id_oficina');
        }

 public function empleado(){
        return $this->belongsTo('App\Modelos\Empleado\Empleado', 'empleado_id', 'id_empleado');
        }
    
}


