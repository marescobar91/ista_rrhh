<?php

namespace App\Modelos\Marcacion;

use Illuminate\Database\Eloquent\Model;

class MesHora extends Model
{
     protected $table = 'mar_meses_horas';
    protected $primaryKey = 'id_meses_horas';

    protected $fillable = [
    	'meses','horas'
    ];
}
