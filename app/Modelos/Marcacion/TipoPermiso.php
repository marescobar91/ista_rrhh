<?php

namespace App\Modelos\Marcacion;

use Illuminate\Database\Eloquent\Model;

class TipoPermiso extends Model
{
    protected $table = 'mar_tipo_permiso';
    protected $primaryKey = 'id_tipo_permiso';

    protected $fillable = [
    	'codigo_permiso','descripcion','numero_horas', 'estado', 'hora_inicio', 'hora_fin', 'horas_acumuladas'
    ];

    public $timestamps = true;

     public function tipoPermiso()
    {
        return $this->hasMany('App\Modelos\Marcacion\Permiso','tipo_permiso_id', 'id_tipo_permiso');
    }

    
}
