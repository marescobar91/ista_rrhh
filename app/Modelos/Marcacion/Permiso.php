<?php

namespace App\Modelos\Marcacion;

use Illuminate\Database\Eloquent\Model;

class Permiso extends Model
{
    protected $table = 'mar_permiso';
    protected $primaryKey = 'id_permiso';

    protected $fillable = [
    	'motivo_permiso','horas_acumuladas','fecha_registro','fecha_inicio','fecha_fin','hora_inicio','hora_fin', 'tipo_permiso_id', 'empleado_id', 'estado'
    ];

    public $timestamps = true;

    public function permisoEmpleado(){
        return $this->belongsTo('App\Modelos\Empleado\Empleado','empleado_id','id_empleado'); 
    }

    public function tipoPermiso()
    {
        return $this->belongsTo('App\Modelos\Marcacion\TipoPermiso','tipo_permiso_id', 'id_tipo_permiso');
    }

    public function permisoDetalle(){
        return $this->hasMany('App\Modelos\Marcacion\PermisoDetalle','permiso_id','id_permiso'); 
    }



}
