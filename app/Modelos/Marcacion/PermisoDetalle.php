<?php

namespace App\Modelos\Marcacion;

use Illuminate\Database\Eloquent\Model;

class PermisoDetalle extends Model
{
   

    protected $table = 'mar_permiso_detalle';
    protected $primaryKey = 'id_permiso_detalle';

    protected $fillable = [
    	'permiso_id', 'fecha_permiso', 'emp_calendario_id'
    ];

    public $timestamps = true;

    public function permiso(){
        return $this->belongsTo('App\Modelos\Marcacion\Permiso','permiso_id','id_permiso'); 
    }

}
