<?php

namespace App\Modelos\Empleado;

use Illuminate\Database\Eloquent\Model;

class EstadoCivil extends Model
{
    protected $table="emp_estado_civil";
    protected $primaryKey="id_estado_civil";

    protected $fillable = ['estado_civil'];

    public $timestamps=true;

    public function empleados(){
    	return $this->hasMany('App\Modelos\Empleado\Empleado','estado_civil_id', 'id_estado_civil');
    }

}
