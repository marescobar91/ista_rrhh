<?php

namespace App\Modelos\Empleado;

use Illuminate\Database\Eloquent\Model;

class Historial extends Model
{
     protected $table="emp_historial";
    protected $primaryKey="id_historial"; 

    protected $fillable = ['empleado_id','usuario_id','municipio_id','nivel_academico_id','cargo_nominal_id','tipo_plaza_id','estado_civil_id','banco_id','cargo_funcional_id','oficina_id','cargo_mp_id','cargo_hacienda_id','codigo_empleado','nombre', 'apellido','pin_empleado','numero_expediente','numero_documento','fecha_nacimiento','lugar_nacimiento','nit','isss','prevision','nup','direccion','telefono','nombre_contacto','telefono_contacto','sexo','fecha_ingreso','fecha_reingreso','salario_actual','salario_inicial','cuenta_banco','cifra_presupuestaria','numero_acuerdo','numero_partida','numero_subpartida','afiliado_sindicato','rendir_probidad','es_jefe','marca','autorizado_estudio','pensionado','enviar_jefe','pasivo','activo','gifcard','numero_acuerdo_retiro','motivo_retiro','fecha_retiro','tipo_documento', 'fecha_traslado', 'correo', 'traslado'];

    public $timestamps=true;

    public function banco(){
    	return $this->belongsTo('App\Modelos\Empleado\Banco', 'banco_id', 'id_banco');
        }
   
    public function cargoHacienda(){
        return $this->belongsTo('App\Modelos\Empleado\CargoHacienda', 'cargo_hacienda_id', 'id_cargo_hacienda');
    }
    public function cargoMP(){
        return $this->belongsTo('App\Modelos\Empleado\CargoMP','cargo_mp_id', 'id_cargo_mp');
    }
    public function nivelAcademico(){
        return $this->belongsTo('App\Modelos\Empleado\NivelAcademico', 'nivel_academico_id', 'id_nivel_academico');
        }
    public function oficina(){
        return $this->belongsTo('App\Modelos\Empleado\Oficina', 'oficina_id', 'id_oficina');
        }
    public function tipoPlaza(){
        return $this->belongsTo('App\Modelos\Empleado\TipoPlaza','tipo_plaza_id', 'id_tipo_plaza');
    }
    public function municipio(){
        return $this->belongsTo('App\Modelos\Empleado\Municipio','municipio_id', 'id_municipio');
    }
    public function estadoCivil(){
        return $this->belongsTo('App\Modelos\Empleado\EstadoCivil','estado_civil_id', 'id_estado_civil');
    }

    public function empleado(){
    	return $this->belongsTo('App\Modelos\Empleado\Empleado', 'empleado_id', 'id_empleado');
    }

    public function usuario(){

    	return $this->belongsTo('App\Modelos\Administracion\Usuario', 'usuario_id', 'id_usuario');
    }


   
}
