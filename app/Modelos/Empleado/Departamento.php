<?php

namespace App\Modelos\Empleado;

use Illuminate\Database\Eloquent\Model;

class Departamento extends Model
{
    protected $table = 'emp_departamento';
    protected $primaryKey = 'id_departamento';

    protected $fillabel = ['departamento'];

    public $timestamps = true;

    public function municipio(){
    	return $this->belongsTo('App\Modelos\Empleado\Municipio','departamento_id', 'id_departamento');
    }

     public function lugarMision(){

    	return $this->hasMany('App\Modelos\Viatico\LugarMision','departamento_id','id_departamento');
    }
}
