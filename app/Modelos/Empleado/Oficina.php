<?php

namespace App\Modelos\Empleado;

use Illuminate\Database\Eloquent\Model;

class Oficina extends Model
{
    
    protected $table = 'emp_oficina';
    protected $primaryKey = 'id_oficina';

    protected $fillable = [
        'unidad_administrativa_id', 'oficina', 'estado', 'telefono_oficina', 'centro_id'
    ];

    public $timestamps=true;

    public function empleados(){
    	
    	return $this->hasMany('App\Modelos\Empleado\Empleado', 'oficina_id', 'id_oficina');
	}

	public function unidadAdministrativa(){
        return $this->belongsTo('App\Modelos\Empleado\UnidadAdministrativa', 'unidad_administrativa_id', 'id_unidad_administrativa');
        }

    public function marcaciones(){
        
        return $this->hasMany('App\Modelos\Marcacion\Marcacion', 'oficina_id', 'id_oficina');
    }
    public function centro(){
        return $this->belongsTo('App\Modelos\Empleado\Centro', 'centro_id', 'id_centro');
        }

    public function oficinaUsuario(){
       return $this->belongsToMany('App\Modelos\Administracion\Usuario','admin_usuario_oficina','usuario_id','oficina_id');
    }
    
    public function programacion(){
    	return $this->hasMany('App\Modelos\Viatico\Programacion', 'oficina_id', 'id_oficina');
	}
    public function historial(){
        return $this->hasMany('App\Modelos\Empleado\Historial','oficina_id','id_oficina');  
    }

}
