<?php

namespace App\Modelos\Empleado;

use Illuminate\Database\Eloquent\Model;

class CargoMP extends Model
{
    //
    protected $table = 'emp_cargo_mp';
    protected $primaryKey = 'id_cargo_mp';

    protected $fillable = [
        'cargo_mp','estado',
    ];

    public $timestamps=true;

    public function empleados(){
    	
    	return $this->hasMany('App\Modelos\Empleado\Empleado', 'cargo_mp_id', 'id_cargo_mp');
	}

}
