<?php

namespace App\Modelos\Empleado;

use Illuminate\Database\Eloquent\Model;

class CargoHacienda extends Model
{
    protected $table = 'emp_cargo_hacienda';
    protected $primaryKey = 'id_cargo_hacienda';

    protected $fillable = [
        'cargo_hacienda','estado',
    ];

    public $timestamps=true;

    public function empleados(){
    	
    	return $this->hasMany('App\Modelos\Empleado\Empleado', 'cargo_hacienda_id', 'id_cargo_hacienda');
	

	}
}
