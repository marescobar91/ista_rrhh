<?php

namespace App\Modelos\Empleado;

use Illuminate\Database\Eloquent\Model;

class Municipio extends Model
{
    protected $table = 'emp_municipio';
    protected $primaryKey = 'id_municipio';

    protected $fillable = [
    	'departamento_id','municipio'];

    public $timestamps=true;

    public function empleados(){
    	return $this->hasMany('App\Modelos\Empleado\Empleado','municipio_id', 'id_municipio');
    }
    public function departamento(){
    	return $this->belongsTo('App\Modelos\Empleado\Departamento','departamento_id', 'id_departamento');
    }
}
