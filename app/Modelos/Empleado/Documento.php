<?php

namespace App\Modelos\Empleado;

use Illuminate\Database\Eloquent\Model;

class Documento extends Model
{
    //
     protected $table = 'emp_documento';
    protected $primaryKey = 'id_documento';

    protected $fillable = [
        'empleado_id','nombre_documento', 'descripcion', 'ruta', 'extension'
    ];

    public $timestamps=true;

    public function empleado(){

    	return $this->belongsTo('App\Modelos\Empleado\Empleado', 'empleado_id', 'id_empleado');
    }
}
