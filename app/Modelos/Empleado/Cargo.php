<?php

namespace App\Modelos\Empleado;

use Illuminate\Database\Eloquent\Model;

class Cargo extends Model
{
    protected $table = 'emp_cargo';
    protected $primaryKey = 'id_cargo';

    protected $fillable = [
        'cargo','estado'
    ];

    public $timestamps=true;

    
}
