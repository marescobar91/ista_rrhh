<?php

namespace App\Modelos\Empleado;

use Illuminate\Database\Eloquent\Model;

class Banco extends Model
{
    //
    protected $table = 'emp_banco';
    protected $primaryKey = 'id_banco';

    protected $fillable = [
        'nombre_banco','estado',
    ];

    public $timestamps=true;

    public function empleados(){
    	
    	return $this->hasMany('App\Modelos\Empleado\Empleado', 'banco_id', 'id_banco');
	

	}


}
