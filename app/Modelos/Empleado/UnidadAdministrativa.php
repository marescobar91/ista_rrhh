<?php

namespace App\Modelos\Empleado;

use Illuminate\Database\Eloquent\Model;

class UnidadAdministrativa extends Model
{
    //
    protected $table = 'emp_unidad_administrativa';
    protected $primaryKey = 'id_unidad_administrativa';

    protected $fillable = [
        'unidad_administrativa', 'estado'
    ];

    public $timestamps=true;

    public function oficinas(){
    	
    	return $this->hasMany('App\Modelos\Empleado\Oficina', 'unidad_administrativa_id', 'id_unidad_administrativa');
	

	}
}
