<?php

namespace App\Modelos\Empleado;

use Illuminate\Database\Eloquent\Model;

class Centro extends Model
{
    protected $table = 'emp_centro';
    protected $primaryKey = 'id_centro';

    protected $fillable = [
        'abreviatura','centro', 'estado', 'encargado', 'fecha_inicio', 'hora_inicio', 'fecha_fin', 'hora_fin', 'activo', 'fin_semana', 'asueto'
    ];

    public $timestamps=true;

    public function oficinas(){
    	
    	return $this->hasMany('App\Modelos\Empleado\Centro', 'centro_id', 'id_centro'); 
	}


    public function viatico(){
        return $this->hasMany('App\Modelos\Viatico\Viatico','centro_id','id_centro');
    }
}
