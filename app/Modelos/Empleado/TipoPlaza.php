<?php

namespace App\Modelos\Empleado;

use Illuminate\Database\Eloquent\Model;

class TipoPlaza extends Model
{
    //
    protected $table = 'emp_tipo_plaza';
    protected $primaryKey = 'id_tipo_plaza';

    protected $fillable = [
        'tipo_plaza','estado',
    ];

    public $timestamps=true;

    public function empleados(){
    	
    	return $this->hasMany('App\Modelos\Empleado\Empleado', 'tipo_plaza_id', 'id_tipo_plaza');
	

	}

}
