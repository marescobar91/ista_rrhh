<?php

namespace App\Modelos\Empleado;

use Illuminate\Database\Eloquent\Model;

class NivelAcademico extends Model
{
    //
    protected $table = 'emp_nivel_academico';
    protected $primaryKey = 'id_nivel_academico';

    protected $fillable = [
        'nivel_academico','estado',
    ];

    public $timestamps=true;

    public function empleados(){
    	
    	return $this->hasMany('App\Modelos\Empleado\Empleado', 'nivel_academico_id', 'id_nivel_academico');
	

	}

}
