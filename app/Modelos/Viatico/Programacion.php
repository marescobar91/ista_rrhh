<?php

namespace App\Modelos\Viatico;

use Illuminate\Database\Eloquent\Model;

class Programacion extends Model
{
    protected $table = 'via_programacion';
    protected $primaryKey = 'id_programacion';

    protected $fillable = [
    	'empleado_id','oficina_id','lugar_mision_id','actividad_id','tipo_viatico_id','fecha_desde','fecha_hasta','impreso','estado', 'pago_viatico', 'viatico_id', 'usuario_id'
    ];

    public $timestamps = true;

    public function programacionEmpleado(){
        return $this->belongsTo('App\Modelos\Empleado\Empleado','empleado_id','id_empleado'); 
    }

    public function oficina(){
        return $this->belongsTo('App\Modelos\Empleado\Oficina', 'oficina_id', 'id_oficina');
    }

    public function lugarMision(){
        return $this->belongsTo('App\Modelos\Viatico\LugarMision', 'lugar_mision_id', 'id_lugar_mision');
        }
    
    public function actividad(){
        return $this->belongsTo('App\Modelos\Viatico\Actividad', 'actividad_id', 'id_actividad');
        }

    public function tipoViatico(){
        return $this->belongsTo('App\Modelos\Viatico\TipoViatico', 'tipo_viatico_id', 'id_tipo_viatico');
        }

    public function programacionDetalle(){
        return $this->belongsTo('App\Modelos\Viatico\ProgramacionDetalle','id_programacion','programacion_id'); 
    }

    public function viatico(){
        return $this->belongsTo('App\Modelos\Viatico\Viatico', 'viatico_id', 'id_viatico');
    }
}
