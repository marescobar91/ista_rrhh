<?php

namespace App\Modelos\Viatico;

use Illuminate\Database\Eloquent\Model;

class TipoProgramacion extends Model
{
    protected $table = 'via_tipo_programacion';
    protected $primaryKey = 'id_tipo_programacion';

    protected $fillable = [
    	'tipo_programacion','estado'
    ];

    public $timestamps = true;

   public function viatico(){
        return $this->hasMany('App\Modelos\Viatico\Viatico', 'tipo_programacion_id','id_tipo_programacion');
    }
    
}
