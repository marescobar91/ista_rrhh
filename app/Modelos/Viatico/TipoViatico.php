<?php

namespace App\Modelos\Viatico;

use Illuminate\Database\Eloquent\Model;

class TipoViatico extends Model
{
    protected $table = 'via_tipo_viatico';
    protected $primaryKey = 'id_tipo_viatico';

    protected $fillable = [
    	'tipo_viatico','codigo', 'valor','estado'
    ];

    public $timestamps = true;

    public function programacion(){
    	return $this->belongsTo('App\Modelos\Viatico\Programacion','id_tipo_viatico','tipo_viatico_id');
    }
}
