<?php

namespace App\Modelos\Viatico;

use Illuminate\Database\Eloquent\Model;

class FormaPago extends Model
{
    protected $table = 'via_forma_pago';
    protected $primaryKey = 'id_forma_pago';

    protected $fillable = [
    	'forma_pago','estado'
    ];

    public $timestamps = true;

    public function viatico(){
        return $this->hasMany('App\Modelos\Viatico\Viatico','forma_pago_id','id_forma_pago');
    }
}
