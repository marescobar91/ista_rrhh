<?php

namespace App\Modelos\Viatico;

use Illuminate\Database\Eloquent\Model;

class Periodo extends Model
{
    
    protected $table = 'via_periodo';
    protected $primaryKey = 'id_periodo';
    protected $fillable = [
    	'mes','anio', 'centro_id', 'estado'
    ];

    public $timestamps = true;

    public function centro(){

    	return $this->belongsTo('App\Modelos\Empleado\Centro','centro_id','id_centro');
    }
}
