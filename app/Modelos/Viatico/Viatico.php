<?php

namespace App\Modelos\Viatico;

use Illuminate\Database\Eloquent\Model;

class Viatico extends Model
{
    protected $table = 'via_viatico';
    protected $primaryKey = 'id_viatico';

    protected $fillable = [
    	'referencia','referencia_ufi', 'fecha','centro_id', 'cheque', 'cantidad_remesa', 'folio', 'estado', 'tipo_programacion_id', 'forma_pago_id', 'fecha_cheque', 'fecha_remesa', 'fecha_liquidar', 'mes_liquidar', 'anio_liquidar', 'cantidad_remesa_segundo', 'fecha_remesa_segundo', 'folio_segundo'
    ];

    public $timestamps = true;

    public function centro(){
    	return $this->belongsTo('App\Modelos\Empleado\Centro', 'centro_id','id_centro');
    }
    public function tipoProgramacion(){
    	return $this->belongsTo('App\Modelos\Viatico\TipoProgramacion', 'tipo_programacion_id','id_tipo_programacion');
    }
    public function formaPago(){
    	return $this->belongsTo('App\Modelos\Viatico\FormaPago','forma_pago_id','id_forma_pago');
    }

     public function programacion(){
        return $this->hasMany('App\Modelos\Viatico\Programacion', 'viatico_id', 'id_viatico');
    }

}
