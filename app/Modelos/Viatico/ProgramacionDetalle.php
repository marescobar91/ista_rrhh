<?php

namespace App\Modelos\Viatico;

use Illuminate\Database\Eloquent\Model;

class ProgramacionDetalle extends Model
{
    protected $table = 'via_programacion_detalle';
    protected $primaryKey = 'id_programacion_detalle';

    protected $fillable = [
    	'programacion_id','fecha_programacion','estado', 'pago_viatico'
    ];

    public $timestamps = true;

    public function programacion(){
        return $this->belongsTo('App\Modelos\Viatico\Programacion','programacion_id','id_programacion'); 
    }
}
