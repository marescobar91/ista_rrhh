<?php

namespace App\Modelos\Viatico;

use Illuminate\Database\Eloquent\Model;

class LugarMision extends Model
{
    protected $table = 'via_lugar_mision';
    protected $primaryKey = 'id_lugar_mision';

    protected $fillable = [
    	'lugar_mision','departamento_id', 'kilometros', 'estado'
    ];

    public $timestamps = true;

    public function departamento(){

    	return $this->belongsTo('App\Modelos\Empleado\Departamento','departamento_id','id_departamento');
    }

    public function programacion(){
    	return $this->hasMany('App\Modelos\Viatico\Programacion','id_lugar_mision','lugar_mision_id');
    }
}
