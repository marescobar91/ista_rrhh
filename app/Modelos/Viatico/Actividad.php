<?php

namespace App\Modelos\Viatico;

use Illuminate\Database\Eloquent\Model;

class Actividad extends Model
{
    protected $table = 'via_actividad';
    protected $primaryKey = 'id_actividad';

    protected $fillable = [
    	'actividad','estado'
    ];

    public $timestamps = true;

    public function programacion(){
    	return $this->belongsTo('App\Modelos\Viatico\Programacion','id_actividad','actividad_id');
    }
}
