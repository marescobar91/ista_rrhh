<?php

/*
|--------------------------------------------------------------------------
| Web Routes

|--------------------------------------------------------------------------
|--------------------------------------------------------------------------*/

 
require __DIR__ . '/web/public.php';			//Modulo Publicas
require __DIR__ . '/web/admin.php';				//Modulo Administracion
require __DIR__ . '/web/empleado.php';			//Modulo Personal
require __DIR__ . '/web/marcacion.php';			//Modulo Marcaciones
require __DIR__ . '/web/viatico.php';			//Modulo Viático

