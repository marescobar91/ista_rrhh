<?php
/*
|--------------------------------------------------------------------------
| Rutas de Administracion

|--------------------------------------------------------------------------
|--------------------------------------------------------------------------*/
Route::middleware(['auth'])->group(function (){
	/**************** Gestión de Permisos ****************/
	Route::get('/permisos/crear', [
		'uses'	=>	'Administracion\PermisosController@crearPermisos',
		'as'	=>	'permisos.crear']);
	Route::post('/permisos/', [
		'uses'	=>	'Administracion\PermisosController@guardarPermisos',
		'as'	=>	'permisos.guardar']);
	Route::get('/permisos/{id}/editar', [
		'uses'	=>	'Administracion\PermisosController@editarPermisos',
		'as'	=>	'permisos.editar']);
	Route::put('/{id}/permisos/', [
		'uses'	=>	'Administracion\PermisosController@actualizarPermisos',
		'as'	=>	'permisos.actualizar']);
	Route::get('/permisos', [
		'uses'	=>	'Administracion\PermisosController@listarPermisos',
		'as'	=>	'permisos.listar']);
	Route::get('permisos/{id}/eliminar',[
		'uses' => 'Administracion\PermisosController@eliminarPermisos',
		'as'   => 'permisos.eliminar']);
	/**************** Gestión de Permisos ****************/
	/**************** Gestión de Roles ****************/
		Route::get('/roles/crear', [
		'uses'	=>	'Administracion\RolesController@crearRoles',
		'as'	=>	'roles.crear']);
	Route::post('/roles/', [
		'uses'	=>	'Administracion\RolesController@guardarRoles',
		'as'	=>	'roles.guardar']);
	Route::get('/roles', [
		'uses'	=>	'Administracion\RolesController@listarRoles',
		'as'	=>	'roles.listar']);
	Route::get('roles/{id}/editar', [
		'uses'	=>	'Administracion\RolesController@editarRoles',
		'as'	=>	'roles.editar']);
	Route::put('/{id}/roles', [
		'uses'	=>	'Administracion\RolesController@actualizarRoles',
		'as'	=>	'roles.actualizar']);
	Route::get('roles/{id}/eliminar',[
		'uses' => 'Administracion\RolesController@eliminarRoles',
		'as'   => 'roles.eliminar']);
	/**************** Gestión de Roles ****************/
	/**************** Gestión de Usuarios****************/
    Route::get('/usuarios/{id}/crear', [
		'uses'	=>	'Administracion\UsuariosController@crearUsuarios',
		'as'	=>	'usuarios.crear']);
	Route::post('/{id}/usuarioNuevo', [
		'uses'	=>	'Administracion\UsuariosController@guardarUsuarios',
		'as'	=>	'usuarios.guardar']);
	Route::get('/usuarios/{id}/editar', [
		'uses'	=>	'Administracion\UsuariosController@editarUsuarios',
		'as'	=>	'usuarios.editar']);
	Route::put('/{id}/usuarios/', [
		'uses'	=>	'Administracion\UsuariosController@actualizarUsuarios',
		'as'	=>	'usuarios.actualizar']);
	Route::get('/usuarios', [
		'uses'	=>	'Administracion\UsuariosController@listarUsuarios',
		'as'	=>	'usuarios.listar']);
	Route::get('/usuarios/{id}/eliminar',[
		'uses'  =>	'Administracion\UsuariosController@eliminarUsuarios',
		'as'	=>	'usuarios.eliminar']);
	Route::get('/usuarios/{id}/cambiarContraseña', [
		'uses'	=>	'Administracion\UsuariosController@cambiarContraseña',
		'as'	=>	'usuarios.cambiarContraseña']);
	Route::put('/{id}/usuarios/password', [
		'uses'	=>	'Administracion\UsuariosController@guardarNuevaContraseña',
		'as'	=>	'usuarios.guardarContraseña']);
	Route::get('/usuarios/nuevo', [
		'uses'	=>	'Administracion\UsuariosController@asignarEmpleado',
		'as'	=>	'usuarios.asignarEmpleado']);
	Route::get('/usuarios/{id}/habilitar', [
		'uses'	=>	'Administracion\UsuariosController@activarUsuarios', 
		'as'	=>	'usuarios.activar']);
	Route::get('/usuario/asignar/oficina/{id}/{accion}', [
		'uses'	=>	'Administracion\UsuariosController@asignarOficinas',
		'as'	=>	'usuarios.asignar']);
	Route::put('/usuario/asignar/oficina/{id}/', [
		'uses'	=>	'Administracion\UsuariosController@guardarUsuarioOficina',
		'as'	=>	'usuario.oficina.guardar']);

	/**************** Gestión de Usuarios ****************/

	/**************** Gestion de Catalogos *****************/
	Route::get('/admin/catalogos', [
		'uses'	=>	'Administracion\CatalogosController@verCatalogos', 
		'as'	=>	'admin.catalogos']);
	/**************** Gestion de Catalogos *****************/

});