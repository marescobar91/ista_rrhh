<?php
/*
|--------------------------------------------------------------------------
| Rutas de Empleados

|--------------------------------------------------------------------------
|--------------------------------------------------------------------------*/
Route::middleware(['auth'])->group(function () {
	/**************** Gestión de Bancos ****************/
	Route::get('/empleados/bancos/crear',[
		'uses'	=>	'Empleados\BancosController@crearBancos',
		'as'	=>	'bancos.crear']);
	Route::post('/empleados/bancos/',[
		'uses'	=>	'Empleados\BancosController@guardarBancos',
		'as'	=>	'bancos.guardar']);
	Route::get('/empleados/bancos/{id}/editar',[
		'uses'	=>	'Empleados\BancosController@editarBancos',
		'as'	=>	'bancos.editar']);
	Route::put('/empleados/{id}/bancos',[
		'uses'	=>	'Empleados\BancosController@actualizarBancos', 
		'as'	=>	'bancos.actualizar']);
	Route::get('/empleados/bancos',[
		'uses'	=>	'Empleados\BancosController@listarBancos', 
		'as'	=>	'bancos.listar']);
	Route::get('/empleados/bancos/{id}/eliminar',[
		'uses'	=>	'Empleados\BancosController@eliminarBancos', 
		'as'	=>	'bancos.eliminar']);
	Route::get('empleados/bancos/{id}/habilitar', [
		'uses'	=>	'Empleados\BancosController@habilitarBancos', 
		'as'	=>	'bancos.habilitar']);
/**************** Gestión de Bancos ****************/
/**************** Gestión de Niveles Academicos ****************/
	Route::get('/empleados/academicos/crear',[
		'uses'	=>	'Empleados\NivelesAcademicosController@crearNivelesAcademicos',
		'as'	=>	'nivelesAcademicos.crear']);
	Route::post('/empleados/academicos/',[
		'uses'	=>	'Empleados\NivelesAcademicosController@guardarNivelesAcademicos',
		'as'	=>	'nivelesAcademicos.guardar']);
	Route::get('/empleados/academicos/{id}/editar',[
		'uses'	=>	'Empleados\NivelesAcademicosController@editarNivelesAcademicos',
		'as'	=>	'nivelesAcademicos.editar']);
	Route::put('/empleados/{id}/academicos',[
		'uses'	=>	'Empleados\NivelesAcademicosController@actualizarNivelesAcademicos', 
		'as'	=>	'nivelesAcademicos.actualizar']);
	Route::get('/empleados/academicos',[
		'uses'	=>	'Empleados\NivelesAcademicosController@listarNivelesAcademicos', 
		'as'	=>	'nivelesAcademicos.listar']);
	Route::get('/empleados/academicos/{id}/eliminar',[
		'uses'	=>	'Empleados\NivelesAcademicosController@eliminarNivelesAcademicos', 
		'as'	=>	'nivelesAcademicos.eliminar']);
	Route::get('empleados/academicos/{id}/habilitar', [
		'uses'	=>	'Empleados\NivelesAcademicosController@habilitarNivelesAcademicos', 
		'as'	=>	'nivelesAcademicos.habilitar']);
/**************** Gestión de Niveles Academicos ****************/
/*************** Gestión de Cargos Hacienda*********************/
	Route::get('/empleados/cargosHacienda/crear',[
		'uses'	=>	'Empleados\CargosHaciendaController@crearCargosHacienda',
		'as'	=>	'cargosHacienda.crear']);
	Route::post('/empleados/cargosHacienda/',[
		'uses'	=>	'Empleados\CargosHaciendaController@guardarCargosHacienda',
		'as'	=>	'cargosHacienda.guardar']);
	Route::get('/empleados/cargosHacienda/{id}/editar',[
		'uses'	=>	'Empleados\CargosHaciendaController@editarCargosHacienda',
		'as'	=>	'cargosHacienda.editar']);
	Route::put('/empleados/{id}/cargosHacienda',[
		'uses'	=>	'Empleados\CargosHaciendaController@actualizarCargosHacienda', 
		'as'	=>	'cargosHacienda.actualizar']);
	Route::get('/empleados/cargosHacienda',[
		'uses'	=>	'Empleados\CargosHaciendaController@listarCargosHacienda', 
		'as'	=>	'cargosHacienda.listar']);
	Route::get('/empleados/cargosHacienda/{id}/eliminar',[
		'uses'	=>	'Empleados\CargosHaciendaController@eliminarCargosHacienda', 
		'as'	=>	'cargosHacienda.eliminar']);
	Route::get('empleados/cargosHacienda/{id}/habilitar', [
		'uses'	=>	'Empleados\CargosHaciendaController@habilitarCargosHacienda', 
		'as'	=>	'cargosHacienda.habilitar']);
/**************** Gestión de Cargos Hacienda ****************/
/**************** Gestión de Tipo de Plaza ****************/
	Route::get('/empleados/tiposPlaza/crear',[
		'uses'	=>	'Empleados\TiposPlazaController@crearTiposPlaza',
		'as'	=>	'tiposPlaza.crear']);
	Route::post('/empleados/tiposPlaza/',[
		'uses'	=>	'Empleados\TiposPlazaController@guardarTiposPlaza',
		'as'	=>	'tiposPlaza.guardar']);
	Route::get('/empleados/tiposPlaza/{id}/editar',[
		'uses'	=>	'Empleados\TiposPlazaController@editarTiposPlaza',
		'as'	=>	'tiposPlaza.editar']);
	Route::put('/empleados/{id}/tiposPlaza',[
		'uses'	=>	'Empleados\TiposPlazaController@actualizarTiposPlaza', 
		'as'	=>	'tiposPlaza.actualizar']);
	Route::get('/empleados/tiposPlaza',[
		'uses'	=>	'Empleados\TiposPlazaController@listarTiposPlaza', 
		'as'	=>	'tiposPlaza.listar']);
	Route::get('/empleados/tiposPlaza/{id}/eliminar',[
		'uses'	=>	'Empleados\TiposPlazaController@eliminarTiposPlaza',  
		'as'	=>	'tiposPlaza.eliminar']);
	Route::get('empleados/tiposPlaza/{id}/habilitar', [
		'uses'	=>	'Empleados\TiposPlazaController@habilitarTiposPlaza', 
		'as'	=>	'tiposPlaza.habilitar']);
/**************** Gestión de Tipos de Plazas ****************/
/**************** Gestión de Cargos  ****************/
	Route::get('/empleados/cargos/crear',[
		'uses'	=>	'Empleados\CargosController@crearCargos',
		'as'	=>	'cargos.crear']);
	Route::post('/empleados/cargos/',[
		'uses'	=>	'Empleados\CargosController@guardarCargos',
		'as'	=>	'cargos.guardar']);
	Route::get('/empleados/cargos/{id}/editar',[
		'uses'	=>	'Empleados\CargosController@editarCargos',
		'as'	=>	'cargos.editar']);
	Route::put('/empleados/{id}/cargos',[
		'uses'	=>	'Empleados\CargosController@actualizarCargos', 
		'as'	=>	'cargos.actualizar']);
	Route::get('/empleados/cargos',[
		'uses'	=>	'Empleados\CargosController@listarCargos', 
		'as'	=>	'cargos.listar']);
	Route::get('/empleados/cargos/{id}/eliminar',[
		'uses'	=>	'Empleados\CargosController@eliminarCargos', 
		'as'	=>	'cargos.eliminar']);
	Route::get('empleados/cargos/{id}/habilitar', [
		'uses'	=>	'Empleados\CargosController@habilitarCargos', 
		'as'	=>	'cargos.habilitar']);
/**************** Gestión de Cargos ****************/
/**************** Gestión de Unidad Administrativa ****************/
	Route::get('/empleados/unidades/crear',[
		'uses'	=>	'Empleados\UnidadesAdministrativasController@crearUnidadesAdministrativas',
		'as'	=>	'unidadesAdministrativas.crear']);
	Route::post('/empleados/unidades/',[
		'uses'	=>	'Empleados\UnidadesAdministrativasController@guardarUnidadesAdministrativas',
		'as'	=>	'unidadesAdministrativas.guardar']);
	Route::get('/empleados/unidades/{id}/editar',[
		'uses'	=>	'Empleados\UnidadesAdministrativasController@editarUnidadesAdministrativas',
		'as'	=>	'unidadesAdministrativas.editar']);
	Route::put('/empleados/{id}/unidades',[
		'uses'	=>	'Empleados\UnidadesAdministrativasController@actualizarUnidadesAdministrativas', 
		'as'	=>	'unidadesAdministrativas.actualizar']);
	Route::get('/empleados/unidades',[
		'uses'	=>	'Empleados\UnidadesAdministrativasController@listarUnidadesAdministrativas', 
		'as'	=>	'unidadesAdministrativas.listar']);
	Route::get('/empleados/unidades/{id}/eliminar',[
		'uses'	=>	'Empleados\UnidadesAdministrativasController@eliminarUnidadesAdministrativas', 
		'as'	=>	'unidadesAdministrativas.eliminar']);
	Route::get('empleados/unidades/{id}/habilitar', [
		'uses'	=>	'Empleados\UnidadesAdministrativasController@habilitarUnidadesAdministrativas', 
		'as'	=>	'unidadesAdministrativas.habilitar']);
/**************** Gestión de Unidad Administrativa ****************/
/**************** Gestión de Oficinas ****************/
	Route::get('/empleados/oficinas/crear',[
		'uses'	=>	'Empleados\OficinasController@crearOficinas',
		'as'	=>	'oficinas.crear']);
	Route::post('/empleados/oficinas/',[
		'uses'	=>	'Empleados\OficinasController@guardarOficinas',
		'as'	=>	'oficinas.guardar']);
	Route::get('/empleados/oficinas/{id}/editar',[
		'uses'	=>	'Empleados\OficinasController@editarOficinas',
		'as'	=>	'oficinas.editar']);
	Route::put('/empleados/{id}/oficinas',[
		'uses'	=>	'Empleados\OficinasController@actualizarOficinas', 
		'as'	=>	'oficinas.actualizar']);
	Route::get('/empleados/oficinas',[
		'uses'	=>	'Empleados\OficinasController@listarOficinas', 
		'as'	=>	'oficinas.listar']);
	Route::get('/empleados/oficinas/{id}/eliminar',[
		'uses'	=>	'Empleados\OficinasController@eliminarOficinas', 
		'as'	=>	'oficinas.eliminar']);
	Route::get('empleados/oficinas/{id}/habilitar', [
		'uses'	=>	'Empleados\OficinasController@habilitarOficinas', 
		'as'	=>	'oficinas.habilitar']);
/**************** Gestión de Oficinas ****************/
/**************** Gestión de Cargos MP ****************/
	Route::get('/empleados/cargosMP/crear',[
		'uses'	=>	'Empleados\CargosMPController@crearCargosMP',
		'as'	=>	'cargosMP.crear']);
	Route::post('/empleados/cargosMP/',[
		'uses'	=>	'Empleados\CargosMPController@guardarCargosMP',
		'as'	=>	'cargosMP.guardar']);
	Route::get('/empleados/cargosMP/{id}/editar',[
		'uses'	=>	'Empleados\CargosMPController@editarCargosMP',
		'as'	=>	'cargosMP.editar']);
	Route::put('/empleados/{id}/cargosMP',[
		'uses'	=>	'Empleados\CargosMPController@actualizarCargosMP', 
		'as'	=>	'cargosMP.actualizar']);
	Route::get('/empleados/cargosMP',[
		'uses'	=>	'Empleados\CargosMPController@listarCargosMP', 
		'as'	=>	'cargosMP.listar']);
	Route::get('/empleados/cargosMP/{id}/eliminar',[
		'uses'	=>	'Empleados\CargosMPController@eliminarCargosMP', 
		'as'	=>	'cargosMP.eliminar']);
	Route::get('empleados/cargosMP/{id}/habilitar', [
		'uses'	=>	'Empleados\CargosMPController@habilitarCargosMP', 
		'as'	=>	'cargosMP.habilitar']);
/**************** Gestión de cargosMP ****************/  
/**************** Gestión de Centro ****************/
	Route::get('/empleados/centros/crear',[
		'uses'	=>	'Empleados\CentrosController@crearCentros',
		'as'	=>	'centros.crear']);
	Route::post('/empleados/centros/',[
		'uses'	=>	'Empleados\CentrosController@guardarCentros',
		'as'	=>	'centros.guardar']);
	Route::get('/empleados/centros/{id}/editar',[
		'uses'	=>	'Empleados\CentrosController@editarCentros',
		'as'	=>	'centros.editar']);
	Route::put('/empleados/{id}/centros',[
		'uses'	=>	'Empleados\CentrosController@actualizarCentros', 
		'as'	=>	'centros.actualizar']);
	Route::get('/empleados/centros',[
		'uses'	=>	'Empleados\CentrosController@listarCentros', 
		'as'	=>	'centros.listar']);
	Route::get('/empleados/centros/{id}/eliminar',[
		'uses'	=>	'Empleados\CentrosController@eliminarCentros', 
		'as'	=>	'centros.eliminar']);
	Route::get('empleados/centros/{id}/habilitar', [
		'uses'	=>	'Empleados\CentrosController@habilitarCentros', 
		'as'	=>	'centros.habilitar']);
/**************** Gestión de Centro ****************/
/*-------Asignacion de Horarios a Centros------*/

	Route::get('viaticos/asignar/centros', [
		'uses'	=>	'Empleados\CentrosController@asignarCentros',
		'as'	=>	'centros.asignar']);
	Route::get('viaticos/centros/{id}/programar', [
		'uses'	=>	'Empleados\CentrosController@programarCentros',
		'as'	=>	'centros.programar']);
	Route::put('viaticos/centros/{id}/horarios', [
		'uses'	=>	'Empleados\CentrosController@horariosCentros',
		'as'	=>	'centros.horarios']);

	Route::get('viaticos/centro/horario/{id}/activar', [
		'uses'	=>	'Empleados\CentrosController@activarCentros', 
		'as'	=>	'centros.activar']);
	Route::get('viaticos/centro/horario/{id}/desactivar', [
		'uses'	=>	'Empleados\CentrosController@desactivarCentros', 
		'as'	=>	'centros.desactivar']);
	Route::get('viaticos/centro/fin-de-semana/{id}/activar', [
		'uses'	=>	'Empleados\CentrosController@activarFinSemana', 
		'as'	=>	'centro.finsemana.activar']);
	Route::get('viaticos/centro/fin-de-semana/{id}/desactivar', [
		'uses'	=>	'Empleados\CentrosController@desactivarFinSemana', 
		'as'	=>	'centro.finsemana.desactivar']);
	Route::get('viaticos/centro/asueto/{id}/activar', [
		'uses'	=>	'Empleados\CentrosController@activarAsueto', 
		'as'	=>	'centro.asueto.activar']);
	Route::get('viaticos/centro/asueto/{id}/desactivar', [
		'uses'	=>	'Empleados\CentrosController@desactivarAsueto', 
		'as'	=>	'centro.asueto.desactivar']);

	/*-------Asignacion de Horarios a Centros------*/


/****************Gestion de Empleadoss************/
	Route::get('/empleados/crear', [
		'uses'	=>	'Empleados\EmpleadosController@crearEmpleados',
		'as'	=>	'empleados.crear']);

	Route::post('/empleados/guardar/personal', [
		'uses'	=>	'Empleados\EmpleadosController@guardarEmpleados',
		'as'	=>	'empleados.guardar']);

	Route::post('/empleados/guardar/informacionPersonal', [
		'uses'	=>	'Empleados\EmpleadosController@guardarInformacionPersonal',
		'as'	=>	'empleados.guardarInformacionPersonal']);
	Route::put('/empleados/editar/{id}/informacionPersonal', [
		'uses'	=>	'Empleados\EmpleadosController@editarInformacionPersonal',
		'as'	=>	'empleados.editarInformacionPersonal']);
	Route::put('/empleados/guardar/{id}/documentosPersonales', [
		'uses'	=>	'Empleados\EmpleadosController@guardarDocumentosPersonales',
		'as'	=>	'empleados.guardarDocumentosPersonales']);
	Route::put('/empleados/editar/{id}/documentosPersonales', [
		'uses'	=>	'Empleados\EmpleadosController@editarDocumentosPersonales',
		'as'	=>	'empleados.editarDocumentosPersonales']);
	Route::put('/empleados/guardar/{id}/informacionLaboral', [
		'uses'	=>	'Empleados\EmpleadosController@guardarInformacionLaboral',
		'as'	=>	'empleados.guardarInformacionLaboral']);
	Route::put('/empleados/guardar/{id}/informacionFinanciera', [
		'uses'	=>	'Empleados\EmpleadosController@guardarInformacionFinanciera',
		'as'	=>	'empleados.guardarInformacionFinanciera']);
	Route::put('/empleados/editar/{id}/informacionFinanciera', [
		'uses'	=>	'Empleados\EmpleadosController@editarInformacionFinanciera',
		'as'	=>	'empleados.editarInformacionFinanciera']);
	Route::post('/empleados/guardar/estadoEmpleado', [
		'uses'	=>	'Empleados\EmpleadosController@guardarEstadoEmpleado',
		'as'	=>	'empleados.guardarEstadoEmpleado']);

	Route::get('/empleados', [
		'uses'	=>	'Empleados\EmpleadosController@listarEmpleados',
		'as'	=>	'empleados.listar']);
	Route::get('/empleados/{id}/editar', [
		'uses'	=>	'Empleados\EmpleadosController@editarEmpleados',
		'as'	=>	'empleados.editar']);
	Route::put('/empleados/{id}', [
		'uses'	=>	'Empleados\EmpleadosController@actualizarEmpleados',
		'as'	=>	'empleados.actualizar']);
	Route::get('/municipios', [
		'uses'	=>	'Empleados\EmpleadosController@getMunicipios',
		'as'	=>	'empleados.municipios']);

	Route::get('/seleccionarOficina', [
		'uses'	=>	'Empleados\EmpleadosController@getOficinas',
		'as'	=>	'empleados.oficinas']);


	Route::get('/empleados/mostrar/{id}', [
		'uses'	=> 'Empleados\EmpleadosController@mostrarEmpleados',
		'as'	=>	'empleados.mostrar']);
	Route::get('/empleados/{id}/deshabilitar', [
		'uses'	=>	'Empleados\EmpleadosController@deshabilitarEmpleados',
		'as'	=>	'empleados.deshabilitar']);
	Route::put('/empleados/{id}/desactivar', [
		'uses'	=>	'Empleados\EmpleadosController@desactivarEmpleados', 
		'as'	=>	'empleados.desactivar']);
	Route::get('/empleados/{id}/habilitar', [
		'uses'	=>	'Empleados\EmpleadosController@habilitarEmpleados',
		'as'	=>	'empleados.habilitar']);
	Route::put('/empleados/{id}/activar', [
		'uses'	=>	'Empleados\EmpleadosController@activarEmpleados',
		'as'	=>	'empleados.activar']);
	
/**************** Gestión de empleados ****************/
/**************** Gestión de Documentos ****************/
	Route::get('/empleados/{id}/archivo',[
		'uses'	=>	'Empleados\DocumentosController@agregarArchivosEmpleado',
		'as'	=>	'documentos.agregar']);
	Route::post('/empleados/{id}/subir/', [
		'uses'	=>	'Empleados\DocumentosController@guardarArchivosEmpleado',
		'as'	=>	'documentos.guardar']);
	Route::get('/empleados/archivo/{id}/listar', [
		'uses'	=>	'Empleados\DocumentosController@listarArchivosEmpleado',
		'as'	=>	'documentos.listar']);
	Route::get('/empleados/archivo/editar/{id}', [
		'uses'	=>	'Empleados\DocumentosController@editarArchivosEmpleado',
		'as'	=>	'documentos.editar']);
	Route::put('/empleados/archivo/{id}/reemplazar', [
		'uses'	=>	'Empleados\DocumentosController@reemplazarArchivosEmpleado',
		'as'	=>	'documentos.reemplazar']);
	Route::get('/empleados/archivo/{id}/eliminar', [
		'uses'	=>	'Empleados\DocumentosController@eliminarArchivosEmpleado', 
		'as'	=>	'documentos.eliminar']);
	Route::get('/empleados/archivo/{id}/mostrar', [
		'uses'	=> 'Empleados\DocumentosController@mostrarArchivosEmpleado',
		'as'	=>	'documentos.mostrar']);
/**************** Gestión de Documentos ****************/

/**************** Gestión de Traslados ****************/

	Route::get('/empleados/trasladar/{id}', [
		'uses'	=>	'Empleados\TrasladoController@crearTraslados', 
		'as'	=>	'traslados.crear']);
	Route::get('/selecionOficinas', [
		'uses'	=>	'Empleados\TrasladoController@obtenerOficinas',
		'as'	=>	'traslados.oficinas']);
	Route::put('/empleados/{id}/traslados',[
		'uses'	=>	'Empleados\TrasladoController@guardarTraslados',
		'as'	=>	'traslados.guardar']);
	Route::get('/empleados/historial/{id}',[
		'uses'	=>	'Empleados\TrasladoController@listarTraslados',
		'as'	=>	'traslados.listar']);
	Route::get('/empleado/{id}/{accion}/historial/guardar', [
		'uses'	=>	'Empleados\TrasladoController@guardarHistorial',
		'as'	=>	'historial.guardar']);
/****************** Reportes de Empleados *************************/
	Route::get('/reportes',[
		'uses'	=>	'Empleados\ReporteController@verReportes',
		'as'	=>	'listado.reporte']);

	Route::get('/reporte/verUnidad',[
		'uses'	=>	'Empleados\ReporteController@verUnidad',
		'as'	=>	'reporte.unidad']);
	Route::get('/reporte/imprimirUnidad',[
		'uses'	=>	'Empleados\ReporteController@imprimirUnidad',
		'as'	=>	'reporte.unidadImprimir']);
	Route::get('/reporte/excelUnidad',[
		'uses'	=>	'Empleados\ReporteController@excelUnidad',
		'as'	=>	'excel.unidadImprimir']);

	Route::get('/reporte/verJefatura',[
		'uses'	=>	'Empleados\ReporteController@verJefatura', 
		'as'	=>	'reporte.jefatura']);
	Route::get('/reporte/imprimirJefatura',[
		'uses'	=>	'Empleados\ReporteController@imprimirJefatura',
		'as'	=>	'reporte.jefaturaImprimir']);
	Route::get('/reporte/excelJefatura',[
		'uses'	=>	'Empleados\ReporteController@excelJefatura',
		'as'	=>	'excel.jefaturaImprimir']);

	Route::get('reporte/empleados', [
		'uses'	=>	'Empleados\ReporteController@filtroReporteDinamico',
		'as'	=>	'reporte.filtroReporte']);
	
	Route::post('reporte/empleado/reporteUno', [
		'uses'	=>	'Empleados\ReporteController@verReporteUno',
		'as'	=>	'reporte.verReporteUno']);
	Route::post('reporte/imprimir/reporteUno',[
		'uses'	=>	'Empleados\ReporteController@imprimirReporteUno',
		'as'	=>	'reporte.imprimirReporteUno']);
	Route::post('reporte/excel/reporteUno',[
		'uses'	=>	'Empleados\ReporteController@excelReporteUno',
		'as'	=>	'excel.imprimirReporteUno']);
	Route::post('reporte/empleado/reporteDos', [
		'uses'	=>	'Empleados\ReporteController@verReporteDos',
		'as'	=>	'reporte.verReporteDos']);
	Route::post('reporte/imprimir/reporteDos',[
		'uses'	=>	'Empleados\ReporteController@imprimirReporteDos',
		'as'	=>	'reporte.imprimirReporteDos']);
	Route::post('reporte/excel/reporteDos',[
		'uses'	=>	'Empleados\ReporteController@excelReporteDos',
		'as'	=>	'excel.imprimirReporteDos']);
	Route::post('reporte/empleado/reporteTres', [
		'uses'	=>	'Empleados\ReporteController@verReporteTres',
		'as'	=>	'reporte.verReporteTres']);
	Route::post('reporte/imprimir/reporteTres',[
		'uses'	=>	'Empleados\ReporteController@imprimirReporteTres',
		'as'	=>	'reporte.imprimirReporteTres']);
	Route::post('reporte/excel/reporteTres',[
		'uses'	=>	'Empleados\ReporteController@excelReporteTres',
		'as'	=>	'excel.imprimirReporteTres']);
	Route::get('/reporte/verGenero',[
		'uses'	=>	'Empleados\ReporteController@verGenero', 
		'as'	=>	'reporte.genero']);
	Route::get('/reporte/imprimirGenero',[
		'uses'	=>	'Empleados\ReporteController@imprimirGenero',
		'as'	=>	'reporte.generoImprimir']);
	Route::get('/reporte/excelGenero',[
		'uses'	=>	'Empleados\ReporteController@excelGenero',
		'as'	=>	'excel.generoImprimir']);

/*****************Reportes de empleados********************************/

/*****************Contrato de Empleado********************************/
Route::get('empleado/exportar/{id}/contrato',[
		'uses'	=>	'Empleados\EmpleadosController@exportarContratoEmpleado',
		'as'	=>	'empleado.exportar.contrato']);

/*****************Reportes de empleados********************************/

});