<?php 

/*  Rutas de Marcaciones */

Route::middleware(['auth'])->group(function () {
	/* -----  Gestion de Asuetos ------ */ 
	Route::get('/marcaciones/asuetos/crear',[
		'uses'	=>	'Marcaciones\AsuetosController@crearAsuetos',
		'as'	=>	'asuetos.crear']);
	Route::post('/marcaciones/asuetos/',[
		'uses'	=>	'Marcaciones\AsuetosController@guardarAsuetos',
		'as'	=>	'asuetos.guardar']);
	Route::get('/marcaciones/asuetos/{id}/editar',[
		'uses'	=>	'Marcaciones\AsuetosController@editarAsuetos',
		'as'	=>	'asuetos.editar']);
	Route::put('/marcaciones/{id}/asuetos',[
		'uses'	=>	'Marcaciones\AsuetosController@actualizarAsuetos', 
		'as'	=>	'asuetos.actualizar']); 
	Route::get('/marcaciones/asuetos',[
		'uses'	=>	'Marcaciones\AsuetosController@listarAsuetos', 
		'as'	=>	'asuetos.listar']);
	Route::get('/marcaciones/asuetos/{id}/eliminar',[
		'uses'	=>	'Marcaciones\AsuetosController@eliminarAsuetos', 
		'as'	=>	'asuetos.eliminar']);   
	Route::get('/marcaciones/asuetos/generar',[
		'uses' 	=> 	'Marcaciones\AsuetosController@cargarAsuetos',
		'as'	=>	'asuetos.generar']);
	/* -----  Gestion de Asuetos ------ */ 
	/* ------- Historial de marcaciones ---- */
	Route::get('/marcaciones/historial',[
		'uses'	=>	'Marcaciones\HistorialController@listarHistorial', 
		'as'	=>	'historialM.listar']);

	/* ------- Reporte marcaciones ---- */
	Route::get('/marcaciones/buscar/llegada/tarde',[
		'uses'	=>	'Marcaciones\ReporteController@buscarLlegadaTarde', 
		'as'	=>	'marcaciones.buscar.llegadaTarde']);

	Route::post('/marcaciones/llegada/tarde',[
		'uses'	=>	'Marcaciones\ReporteController@reporteLlegadaTarde', 
		'as'	=>	'marcaciones.llegadaTarde']);

	Route::post('/reporte/llegada/tarde',[
		'uses'	=>	'Marcaciones\ReporteController@llegadaTardePDF', 
		'as'	=>	'marcaciones.reporte.llegadaTarde']);

	Route::post('/reporte/llegada/tarde/excel',[
		'uses'	=>	'Marcaciones\ReporteController@llegadaTardeExcel', 
		'as'	=>	'marcaciones.reporte.excel.llegadaTarde']);

	
	Route::get('/marcaciones/buscar/salida/temprana',[
		'uses'	=>	'Marcaciones\ReporteController@buscarReporteSalidaTemprana', 
		'as'	=>	'marcaciones.buscar.salidaTemprana']);

	Route::post('/marcaciones/salida/temprana',[
		'uses'	=>	'Marcaciones\ReporteController@reporteSalidaTemprana', 
		'as'	=>	'marcaciones.salidaTemprana']);

	Route::post('/reporte/salida/temprana',[
		'uses'	=>	'Marcaciones\ReporteController@salidaTempranaPDF', 
		'as'	=>	'marcaciones.reporte.salidaTemprana']);

	Route::post('/reporte/salida/temprana/excel',[
		'uses'	=>	'Marcaciones\ReporteController@salidaTempranaExcel', 
		'as'	=>	'marcaciones.reporte.excel.salidaTemprana']);

	Route::post('/reporte/descuento/excelImp',[
		'uses'	=>	'Marcaciones\ReporteController@descuentoImp', 
		'as'	=>	'marcaciones.reporte.excel.descuentoImp']);

	Route::get('/reporte/buscar/descuento/',[
		'uses' 	=> 	'Marcaciones\ReporteController@buscarDescuentos',
		'as'	=>	'marcaciones.reporte.descuento.buscar']);

	Route::post('/reporte/descuento/pdf',[
		'uses' 	=> 	'Marcaciones\ReporteController@reporteDescuentosPDF',
		'as'	=>	'marcaciones.reporte.descuento.pdf']);


	Route::get('/marcaciones/buscar/falta/entrada',[
		'uses'	=>	'Marcaciones\ReporteController@buscarReporteFaltaMarcacionEntrada', 
		'as'	=>	'marcaciones.buscar.faltaEntrada']);

	Route::post('/marcaciones/falta/entrada',[
		'uses'	=>	'Marcaciones\ReporteController@reporteFaltaMarcacionEntrada', 
		'as'	=>	'marcaciones.faltaEntrada']);

	Route::post('/reporte/falta/entrada',[
		'uses'	=>	'Marcaciones\ReporteController@faltaMarcacionEntradaPDF', 
		'as'	=>	'marcaciones.reporte.faltaEntrada']);

	Route::post('/reporte/falta/entrada/excel',[
		'uses'	=>	'Marcaciones\ReporteController@faltaEntradaExcel',  
		'as'	=>	'marcaciones.reporte.excel.faltaEntrada']);


	Route::get('/marcaciones/buscar/falta/salida',[
		'uses'	=>	'Marcaciones\ReporteController@buscarReporteFaltaMarcacionSalida', 
		'as'	=>	'marcaciones.buscar.faltaSalida']);

	Route::post('/marcaciones/falta/salida',[
		'uses'	=>	'Marcaciones\ReporteController@reporteFaltaMarcacionSalida', 
		'as'	=>	'marcaciones.faltaSalida']);

	Route::post('/reporte/falta/salida',[
		'uses'	=>	'Marcaciones\ReporteController@faltaMarcacionSalidaPDF', 
		'as'	=>	'marcaciones.reporte.faltaSalida']);

	Route::post('/reporte/falta/salida/excel',[
		'uses'	=>	'Marcaciones\ReporteController@faltaSalidaExcel', 
		'as'	=>	'marcaciones.reporte.excel.faltaSalida']);


	Route::get('/marcaciones/buscar/ausencia',[
		'uses'	=>	'Marcaciones\ReporteController@buscarReporteAusencia', 
		'as'	=>	'marcaciones.buscar.ausencia']);

	Route::post('/marcaciones/ausencia',[
		'uses'	=>	'Marcaciones\ReporteController@reporteMarcacionAusencia', 
		'as'	=>	'marcaciones.ausencia']);

	Route::post('/reporte/ausencia',[
		'uses'	=>	'Marcaciones\ReporteController@faltaMarcacionAusenciaPDF', 
		'as'	=>	'marcaciones.reporte.ausencia']);

		Route::post('/reporte/ausencia/excel',[
		'uses'	=>	'Marcaciones\ReporteController@faltaMarcacionAusenciaExcel', 
		'as'	=>	'marcaciones.excel.ausencia']);


	Route::get('/marcaciones/buscar/intermedia',[
		'uses'	=>	'Marcaciones\ReporteController@buscarMarcacionIntermedia', 
		'as'	=>	'marcaciones.buscar.intermedia']);

	Route::post('/marcaciones/intermedia',[
		'uses'	=>	'Marcaciones\ReporteController@reporteMarcacionIntermedia', 
		'as'	=>	'marcaciones.intermedia']);

	Route::post('/reporte/intermedia',[
		'uses'	=>	'Marcaciones\ReporteController@marcacionIntermediaPDF', 
		'as'	=>	'marcaciones.reporte.intermedia']);

	Route::post('/marcaciones/buscar/excel', [
		'uses'	=>	'Marcaciones\ReporteController@marcacionIntermediaExcel',
		'as'	=>	'marcaciones.excel.intermedias']);

//Reporte de descuentos de marcaciones 
	Route::post('/marcaciones/descuento/salidaTemprana',[
		'uses'	=>	'Marcaciones\ReportesDescuentosController@salidaTempranoDescuentoPDF', 
		'as'	=>	'marcaciones.descuento.salidaTemprana']);
	Route::post('/marcaciones/descuento/llegadaTarde',[
		'uses'	=>	'Marcaciones\ReportesDescuentosController@llegadaTardeDescuentoPDF', 
		'as'	=>	'marcaciones.descuento.llegadaTarde']);
	Route::post('/marcaciones/descuento/faltaEntrada',[
		'uses'	=>	'Marcaciones\ReportesDescuentosController@faltaEntradaDescuentoPDF', 
		'as'	=>	'marcaciones.descuento.faltaEntrada']);
	Route::post('/marcaciones/descuento/faltaSalida',[
		'uses'	=>	'Marcaciones\ReportesDescuentosController@faltaSalidaDescuentoPDF', 
		'as'	=>	'marcaciones.descuento.faltaSalida']);
	Route::post('/marcaciones/descuento/ausencia',[
		'uses'	=>	'Marcaciones\ReportesDescuentosController@ausenciaDescuentoPDF', 
		'as'	=>	'marcaciones.descuento.ausencia']);
	Route::post('/marcaciones/descuento/intermedias',[
		'uses'	=>	'Marcaciones\ReportesDescuentosController@intermediasDescuentoPDF', 
		'as'	=>	'marcaciones.descuento.intermedia']);
 
	/* ------- Reporte marcaciones ---- */

	/* ------- Consulta marcaciones ---- */
	Route::get('/marcaciones/buscar/empleado',[
		'uses'	=>	'Marcaciones\MarcacionesController@buscarEmpleadoMarcacion', 
		'as'	=>	'marcaciones.buscar']);

	Route::post('/marcaciones/empleado/consulta',[
		'uses'	=>	'Marcaciones\MarcacionesController@consultarEmpleadoMarcacion',
		'as'	=>	'marcaciones.consultar']);

	
	/* ------- Reporte marcaciones ---- */



	/********Gestion de Permiso************/
	Route::get('/permiso/empleado/buscar',[
		'uses'	=>	'Marcaciones\PermisosController@buscarPermisoEmpleado',
		'as'	=>	'permiso.empleado.buscar']); 

	Route::get('/permiso/empleado/crear',[
		'uses'	=>	'Marcaciones\PermisosController@crearPermisoEmpleado',
		'as'	=>	'permiso.empleado.crear']);

	Route::get('/getCodigoTipoPermiso',[
		'uses'	=>	'Marcaciones\PermisosController@getCodigoTipoPermiso', 
		'as'	=>	'permiso.empleado.getCodigoPermiso']);

	Route::post('/permiso/empleado/guardar',[
		'uses'	=>	'Marcaciones\PermisosController@guardarPermisoEmpleado',
		'as'	=>	'permiso.empleado.guardar']);
	
	Route::get('/permiso/empleado/listar',[
		'uses'	=>	'Marcaciones\PermisosController@listarPermisoEmpleado',
		'as'	=>	'permiso.empleado.listar']);

	Route::get('/permiso/empleado/editar/{id}',[ 
		'uses'	=>	'Marcaciones\PermisosController@editarPermisoEmpleado',
		'as'	=>	'permiso.empleado.editar']);

	Route::put('/permiso/empleado/actualizar/{id}',[ 
		'uses'	=>	'Marcaciones\PermisosController@actualizarPermisoEmpleado',
		'as'	=>	'permiso.empleado.actualizar']); 

	Route::get('/marcaciones/permiso/{id}/eliminar',[
		'uses'	=>	'Marcaciones\PermisosController@eliminarPermiso', 
		'as'	=>	'permiso.empleado.eliminar']); 
	Route::get('/marcaciones/permiso/{id}/habilitar',[
		'uses'	=>	'Marcaciones\PermisosController@habilitarPermiso', 
		'as'	=>	'permiso.empleado.habilitar']); 

	Route::get('/marcaciones/permiso/buscar/consulta',[
		'uses'	=> 'Marcaciones\PermisosController@buscarConsultaPermisoEmpleado',
		'as'	=>	'permiso.empleado.buscarPermiso']);
	Route::get('/seleccionOficina', [
		'uses'	=>	'Marcaciones\PermisosController@getOficinas',
		'as'	=>	'permiso.empleado.oficina']);
	Route::get('/seleccionarEmpleado', [
		'uses'	=>	'Marcaciones\PermisosController@getEmpleado',
		'as'	=>	'permiso.empleados']);
	Route::get('/marcaciones/consultar/permisos', [
		'uses'	=>	'Marcaciones\PermisosController@consultarPermisoEmpleado',
		'as'	=>	'permiso.empleado.consulta']);

	Route::get('/reporte/marcaciones', [
		'uses'	=>	'Marcaciones\ReporteController@verReportes', 
		'as'	=>	'marcaciones.verReportes']); 

	Route::get('/buscar/permiso/sin/goce/sueldo',[
		'uses'	=>	'Marcaciones\PermisosController@buscarReportePermisoSinGoceSueldo', 
		'as'	=>	'buscar.permiso.empleado']);

	Route::post('/permiso/sin/goce/sueldo',[
		'uses'	=>	'Marcaciones\PermisosController@reportePermisoSinGoceSueldo', 
		'as'	=>	'permisoempleado.reporte']);

	Route::post('/permiso/sin/goce/sueldo/PDF',[
		'uses'	=>	'Marcaciones\PermisosController@permisoSinGoceSueldoPDF', 
		'as'	=>	'permisoempleado.pdf']);

		

	/********Gestion de Permiso***********/


	/******* Gestión de Tipo Permiso ************/
	Route::get('/empleado/tipoPermiso/listar', [
		'uses'	=>	'Marcaciones\TipoPermisosController@listarTipoPermiso',
		'as'	=>	'tipo.permiso.listar']);

	Route::get('/empleado/tipoPermiso/crear', [
		'uses'	=>	'Marcaciones\TipoPermisosController@crearTipoPermiso',
		'as'	=>	'tipo.permiso.crear']);

	Route::post('/marcaciones/tipoPermiso/guardar', [
		'uses'	=>	'Marcaciones\TipoPermisosController@guardarTipoPermiso',
		'as'	=>	'tipo.permiso.guardar']);
	Route::get('/marcaciones/tipoPermiso/{id}/editar', [
		'uses'	=>	'Marcaciones\TipoPermisosController@editarTipoPermiso',
		'as'	=>	'tipo.permiso.editar']); 

	Route::put('/marcaciones/tipoPermiso/{id}/actualizar', [
		'uses'	=>	'Marcaciones\TipoPermisosController@actualizarTipoPermiso',
		'as'	=>	'tipo.permiso.actualizar']);
	Route::get('/marcaciones/tipoPermiso/{id}/eliminar',[
		'uses'	=>	'Marcaciones\TipoPermisosController@eliminarTipoPermiso', 
		'as'	=>	'tipo.permiso.eliminar']); 
	Route::get('/marcaciones/tipoPermiso/{id}/habilitar',[
		'uses'	=>	'Marcaciones\TipoPermisosController@habilitarTipoPermiso', 
		'as'	=>	'tipo.permiso.habilitar']);   


	/************Gestion de Horas Extras ******************************/
	
	Route::get('/empleado/horas/extras/listar', [
		'uses'	=>	'Marcaciones\HorasExtrasController@listarHorasExtras',
		'as'	=>	'horas.extras.listar']);

	Route::get('/empleado/horas/extras/buscar', [
		'uses'	=>	'Marcaciones\HorasExtrasController@buscarEmpleadoHorasExtras',
		'as'	=>	'horas.extras.buscar']);

	Route::get('/seleccionarOficina', [
		'uses'	=>	'Marcaciones\HorasExtrasController@getOficinas',
		'as'	=>	'horas.extras.oficina']);

	Route::get('/seleccionEmpleado', [
		'uses'	=>	'Marcaciones\HorasExtrasController@getEmpleado',
		'as'	=>	'horas.extras.empleados']);

	Route::get('/empleado/horas/extras/crear', [
		'uses'	=>	'Marcaciones\HorasExtrasController@crearHorasExtras',
		'as'	=>	'horas.extras.crear']);

	Route::post('/empleado/horas/extras/guardar', [
		'uses'	=>	'Marcaciones\HorasExtrasController@guardarHorasExtras',
		'as'	=>	'horas.extras.guardar']);

	Route::get('/empleado/horas/extras/editar/{id}',  [
		'uses'	=>	'Marcaciones\HorasExtrasController@editarHorasExtras',
		'as'	=>	'horas.extras.editar']);

	Route::put('/empleado/horas/extras/actualizar/{id}', [
		'uses'	=>	'Marcaciones\HorasExtrasController@actualizarHorasExtras',
		'as'	=>	'horas.extras.actualizar']);

	Route::get('/marcaciones/horas/extras/{id}/eliminar',[
		'uses'	=>	'Marcaciones\HorasExtrasController@eliminarHorasExtras', 
		'as'	=>	'horas.extras.eliminar']);

	Route::get('/marcaciones/horas/extras/reporte',[
		'uses'	=>	'Marcaciones\HorasExtrasController@reporteHorasExtras',
		'as'	=>	'horas.extras.reporte']);

	Route::post('/marcacaciones/horas/extras/verReporte',[
		'uses'	=>	'Marcaciones\HorasExtrasController@verReporteHorasExtras',
		'as'	=>	'horas.extras.verReporte']);

	Route::post('/marcaciones/horas/extras/excel',[
		'uses'	=>	'Marcaciones\HorasExtrasController@excelReporteHorasExtras',
		'as'	=>	'horas.extras.excel']);

	Route::post('/marcaciones/reporte/horas/extras/pdf',[
		'uses'	=>	'Marcaciones\HorasExtrasController@imprimirReporteHorasExtras', 
		'as'	=>	'horas.extras.pdf']);
	
	/************Gestion de Horas Extras ******************************/
	
	


	
});