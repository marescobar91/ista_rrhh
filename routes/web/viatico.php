<?php
/*
|--------------------------------------------------------------------------
| Rutas de Administracion

|--------------------------------------------------------------------------
|--------------------------------------------------------------------------*/
Route::middleware(['auth'])->group(function (){ 
	/* -----  Gestion de Actividades ------ */ 

	Route::get('viaticos/actividad/crear', [
		'uses'	=>	'Viaticos\ActividadesController@crearActividad',
		'as'	=>	'actividades.crear']);
	Route::get('viaticos/actividad/', [
		'uses'	=>	'Viaticos\ActividadesController@listarActividad',
		'as'	=>	'actividades.listar']);
	Route::post('viaticos/actividad', [
		'uses'	=>	'Viaticos\ActividadesController@guardarActividad',
		'as'	=>	'actividades.guardar']);
	Route::get('viaticos/actividad/{id}/editar', [
		'uses'	=>	'Viaticos\ActividadesController@editarActividad',
		'as'	=>	'actividades.editar']);
	Route::put('viaticos/{id}/actividad/', [
		'uses'	=>	'Viaticos\ActividadesController@actualizarActividad', 
		'as'	=>	'actividades.actualizar']);
	Route::get('viaticos/actividad/{id}/eliminar', [
		'uses'	=>	'Viaticos\ActividadesController@eliminarActividad', 
		'as'	=>	'actividades.eliminar']);

	Route::get('viaticos/actividad/{id}/habilitar', [
		'uses'	=>	'Viaticos\ActividadesController@habilitarActividad',
		'as'	=>	'actividades.habilitar']);
	/* -----  Gestion de Actividades ------ */ 

	/* -----  Gestion de Actividades ------ */ 

	Route::get('viaticos/forma/pago/crear', [
		'uses'	=>	'Viaticos\FormasPagoController@crearFormaPago',
		'as'	=>	'formas.pago.crear']);
	Route::get('viaticos/forma/pago', [
		'uses'	=>	'Viaticos\FormasPagoController@listarFormaPago',
		'as'	=>	'formas.pago.listar']);
	Route::post('viaticos/forma/pago', [
		'uses'	=>	'Viaticos\FormasPagoController@guardarFormaPago',
		'as'	=>	'formas.pago.guardar']);
	Route::get('viaticos/forma/pago/{id}/editar', [
		'uses'	=>	'Viaticos\FormasPagoController@editarFormaPago',
		'as'	=>	'formas.pago.editar']);
	Route::put('viaticos/{id}/forma/pago/', [
		'uses'	=>	'Viaticos\FormasPagoController@actualizarFormaPago', 
		'as'	=>	'formas.pago.actualizar']);
	Route::get('viaticos/forma/pago/{id}/eliminar', [
		'uses'	=>	'Viaticos\FormasPagoController@eliminarFormaPago', 
		'as'	=>	'formas.pago.eliminar']);

	Route::get('viaticos/forma/pago/{id}/habilitar', [
		'uses'	=>	'Viaticos\FormasPagoController@habilitarFormaPago',
		'as'	=>	'formas.pago.habilitar']);
	/* -----  Gestion de Actividades ------ */ 

	/* -----  Gestion de Lugar de Misión ------ */ 
	Route::get('viaticos/lugar/mision/crear', [
		'uses'	=>	'Viaticos\LugarMisionesController@crearLugarMision',
		'as'	=>	'lugar.misiones.crear']);
	Route::post('viaticos/lugar/mision', [
		'uses'	=>	'Viaticos\LugarMisionesController@guardarLugarMision',
		'as'	=>	'lugar.misiones.guardar']);

	Route::get('viaticos/lugar/mision/{id}/editar', [
		'uses'	=>	'Viaticos\LugarMisionesController@editarLugarMision',
		'as'	=>	'lugar.misiones.editar']);
	Route::put('viaticos/lugar/mision/{id}/actualizar', [
		'uses'	=>	'Viaticos\LugarMisionesController@actualizarLugarMision',
		'as'	=>	'lugar.misiones.actualizar']);
	Route::get('viaticos/lugar/mision', [
		'uses'	=>	'Viaticos\LugarMisionesController@listarLugarMision', 
		'as'	=>	'lugar.misiones.listar']);
	Route::get('viaticos/lugar/mision/{id}/eliminar', [
		'uses'	=>	'Viaticos\LugarMisionesController@eliminarLugarMision',
		'as'	=>	'lugar.misiones.eliminar']);
	Route::get('viaticos/lugar/mision/{id}/habilitar', [
		'uses'	=>	'Viaticos\LugarMisionesController@habilitarLugarMision',
		'as'	=>	'lugar.misiones.habilitar']);

	/* -----  Gestion de Lugar de Misión ------ */ 

	/* -----  Gestion de Tipo de Programacion ------ */ 

	Route::get('viaticos/tipo/programacion/crear', [
		'uses'	=>	'Viaticos\TipoProgramacionesController@crearTipoProgramacion',
		'as'	=>	'tipo.programaciones.crear']);
	Route::post('viaticos/tipo/programacion/', [
		'uses'	=>	'Viaticos\TipoProgramacionesController@guardarTipoProgramacion',
		'as'	=>	'tipo.programaciones.guardar']);
	Route::get('viaticos/tipo/programacion/', [
		'uses'	=>	'Viaticos\TipoProgramacionesController@listarTipoProgramacion',
		'as'	=>	'tipo.programaciones.listar']);

	Route::get('viaticos/tipo/programacion/{id}/editar', [
		'uses'	=>	'Viaticos\TipoProgramacionesController@editarTipoProgramacion',
		'as'	=>	'tipo.programaciones.editar']);
	Route::put('viaticos/tipo/programacion/{id}/actualizar', [
		'uses'	=>	'Viaticos\TipoProgramacionesController@actualizarTipoProgramacion',
		'as'	=>	'tipo.programaciones.actualizar']);
	Route::get('viaticos/tipo/programacion/{id}/eliminar', [
		'uses'	=>	'Viaticos\TipoProgramacionesController@eliminarTipoProgramacion',
		'as'	=>	'tipo.programaciones.eliminar']);
	Route::get('viaticos/tipo/programacion/{id}/habilitar', [
		'uses'	=>	'Viaticos\TipoProgramacionesController@habilitarTipoProgramacion',
		'as'	=>	'tipo.programaciones.habilitar']);
	/* -----  Gestion de Tipo de Programacion ------ */ 

	/* -----  Gestion de Tipo de Viáticos ------ */ 

	Route::get('viaticos/tipo/viatico/crear', [
		'uses'	=>	'Viaticos\TipoViaticosController@crearTipoViatico',
		'as'	=>	'tipo.viaticos.crear']);
	Route::post('viaticos/tipo/viatico/', [
		'uses'	=>	'Viaticos\TipoViaticosController@guardarTipoViatico',
		'as'	=>	'tipo.viaticos.guardar']);
	Route::get('viaticos/tipo/viatico', [
		'uses'	=>	'Viaticos\TipoViaticosController@listarTipoViatico',
		'as'	=> 	'tipo.viaticos.listar']);
	Route::get('viaticos/tipo/viatico/{id}/editar', [
		'uses'	=>	'Viaticos\TipoViaticosController@editarTipoViatico',
		'as'	=>	'tipo.viaticos.editar']);
	Route::put('viaticos/tipo/viatico/{id}/actualizar', [
		'uses'	=>	'Viaticos\TipoViaticosController@actualizarTipoViatico',
		'as'	=>	'tipo.viaticos.actualizar']);
	Route::get('viaticos/tipo/viatico/{id}/habilitar', [
		'uses'	=>	'Viaticos\TipoViaticosController@habilitarTipoViatico',
		'as'	=>	'tipo.viaticos.habilitar']);
	Route::get('viaticos/tipo/viatico/{id}/eliminar', [
		'uses'	=>	'Viaticos\TipoViaticosController@eliminarTipoViatico',
		'as'	=>	'tipo.viaticos.eliminar']);
	/* -----  Gestion de Tipo de Viáticos ------ */ 

	/* -----  Gestion de Programación  ------ */
	Route::get('viaticos/programacion', [
		'uses'	=>	'Viaticos\ProgramacionController@crearProgramacion',
		'as'	=> 	'programacion.crear']);
	Route::get('viaticos/listar',[
		'uses'	=>	'Viaticos\ProgramacionController@listarProgramacion',
		'as'	=>	'programacion.listar']);
	Route::post('viaticos/programacion/guardar', [
		'uses'	=>	'Viaticos\ProgramacionController@guardarProgramacion',
		'as'	=>	'programacion.guardar']);
	Route::get('viaticos/getEmpleado', [
		'uses'	=>	'Viaticos\ProgramacionController@getEmpleado',
		'as'	=>	'programacion.getEmpleado']);
	Route::get('viaticos/programacion/{id}/editar',[
		'uses'	=>	'Viaticos\ProgramacionController@editarProgramacion',
		'as'	=>	'viaticos.programacion.editar']);
	Route::put('viaticos/programacion/{id}/actualizar',[
		'uses'	=>	'Viaticos\ProgramacionController@actualizarProgramacion', 
		'as'	=>	'viaticos.programacion.actualizar']);
	Route::get('viaticos/eliminar/{id}/programacion',[
		'uses'	=>	'Viaticos\ProgramacionController@eliminarProgramacion',
		'as'	=> 'viaticos.eliminar.programacion']);
	Route::get('viaticos/habilitar/{id}/programacion',[
		'uses'	=>	'Viaticos\ProgramacionController@habilitarProgramacion',
		'as'	=> 'viaticos.habilitar.programacion']);
	Route::get('viaticos/imprimir/{id}/programacion',[
		'uses'	=>	'Viaticos\ProgramacionController@imprimirProgramacion',
		'as'	=> 'viaticos.imprimir.programacion']);
	/* -----  Gestion de Programacion ------ */

	/*----- Cerrar CCO -------**/
	Route::get('viaticos/generar/cco',[
		'uses'	=>	'Viaticos\ProgramacionController@generarCco',
		'as'	=>	'generar.cco']);
	Route::post('viaticos/generar/cco/pdf',[
		'uses' 	=> 'Viaticos\ProgramacionController@generarCcoPdf',
		'as'	=>	'generar.cco.pdf']);
	Route::post('viaticos/generar/cco/cerrar',[
		'uses' 	=> 'Viaticos\ProgramacionController@cerrarCco',
		'as'	=>	'cerrar.cco.pdf']);
	Route::post('viaticos/generar/cco/imprimir',[
		'uses' 	=> 'Viaticos\ProgramacionController@imprimirCco',
		'as'	=>	'imprimir.cco.pdf']);
	Route::get('viaticos/buscar/programacion',[
		'uses'	=>	'Viaticos\ProgramacionController@buscarProgramacion',
		'as'	=>	'busqueda.programacion']);
	Route::post('viaticos/programacion/buscar',[
		'uses'	=>	'Viaticos\ProgramacionController@cargarProgramacion',
		'as'	=>	'cargar.busqueda']);
	Route::post('viaticos/ver/programacion/{id}',[
		'uses'	=>	'Viaticos\ProgramacionController@pdfProgramacion',
		'as'	=>	'programacion.pdf.ver']);
	Route::post('viaticos/reiniciar/programacion/{id}',[
		'uses' 	=>	'Viaticos\ProgramacionController@reiniciar',
		'as'	=>	'reiniciar.programacion']);
	/*----- Cerrar CCO -------**/

	/*-----Nota de Viaticos ----*/ 
	Route::get('viaticos/nota/crear',[
		'uses'	=>	'Viaticos\ViaticosController@crearViaticos',
		'as'	=>	'nota.viaticos.crear']);
	Route::get('viaticos/nota/getCentro',[
		'uses'	=>	'Viaticos\ViaticosController@getCentro',
		'as'	=>	'nota.viaticos.getCentro']);
	Route::post('viaticos/nota/guardar',[
		'uses'	=>	'Viaticos\ViaticosController@guardarNota',
		'as'	=>	'nota.viaticos.guardar']);

	Route::post('viaticos/nota/{id}/cerrado',[
		'uses'	=>	'Viaticos\ViaticosController@notaAbiertoCerrado',
		'as'	=>	'nota.cerrado']);
	Route::post('viaticos/nota/{id}/abierto',[
		'uses'	=>	'Viaticos\ViaticosController@notaCerradoAbierto',
		'as'	=>	'nota.abierto']);
	Route::post('viaticos/nota/imprimir/{id}',[
		'uses'	=>	'Viaticos\ViaticosController@notaViaticoPDF',
		'as'	=>	'nota.viaticos.imprimir']);
	Route::get('viaticos/nota/viatico/{referencia}/',[
		'uses'	=> 'Viaticos\ViaticosController@notaViatico',
		'as'	=>	'nota.viatico.pdf']);

	/*-----Nota de Viaticos ----*/

	/*------Informes -------*/
	Route::get('viaticos/informes/buscar',[
		'uses'	=>	'Viaticos\ViaticosController@buscarInformes',
		'as'	=>	'viaticos.informes.buscar']);
	Route::post('viaticos/informes/listar',[
		'uses'	=>	'Viaticos\ViaticosController@listarInformes',
		'as'	=>	'viaticos.informes.listar']);
	Route::post('viaticos/informe/{id}/imprimir',[
		'uses' 	=> 'Viaticos\ViaticosController@informePDF',
		'as'	=>	'imprimir.pdf']);
	Route::post('viaticos/ccos/{id}/imprimir',[
		'uses' 	=> 'Viaticos\ViaticosController@ccosPDF',
		'as'	=>	'imprimir.ccos']);
	Route::post('viaticos/programacion/{id}/imprimir',[
		'uses' 	=> 'Viaticos\ViaticosController@programacionesPDF',
		'as'	=>	'imprimir.programacion']);
	Route::post('viaticos/cambiar/{id}/referencia',[
		'uses'	=>	'Viaticos\ViaticosController@actualizarReferencia',
		'as'	=>	'actualizar.referencia']);
	/*------Informes -------*/

	/*-------Gestion de Periodos ----*/

	Route::get('viaticos/periodo/listar', [
		'uses'	=>	'Viaticos\PeriodosController@listarPeriodo',
		'as'	=>	'periodos.listar']);

	Route::get('viaticos/periodo/crear', [
		'uses'	=>	'Viaticos\PeriodosController@crearPeriodo',
		'as'	=>	'periodos.crear']);

	Route::post('viaticos/periodo/guardar', [
		'uses'	=>	'Viaticos\PeriodosController@guardarPeriodo',
		'as'	=>	'periodos.guardar']);

	Route::get('viaticos/periodo/{id}/activar', [
		'uses'	=>	'Viaticos\PeriodosController@activarPeriodo',
		'as'	=>	'periodos.activar']);
	Route::get('viaticos/periodo/{id}/desactivar', [
		'uses'	=>	'Viaticos\PeriodosController@desactivarPeriodo',
		'as'	=>	'periodos.desactivar']);
	/*-------Gestion de Periodos ----*/

		/*-------Gestion de Liquidacion ----*/

	Route::get('viaticos/liquidacion/listar', [
		'uses'	=>	'Viaticos\LiquidacionesController@listarLiquidacion',
		'as'	=>	'liquidaciones.listar']);

	Route::get('viaticos/liquidacion/crear', [
		'uses'	=>	'Viaticos\LiquidacionesController@crearLiquidacion',
		'as'	=>	'liquidaciones.crear']);
	Route::get('viaticos/getReferencia', [
		'uses'	=>	'Viaticos\LiquidacionesController@getReferencia',
		'as'	=>	'liquidaciones.referencia']);
	Route::get('viaticos/obtenerReferencia', [
		'uses'	=>	'Viaticos\LiquidacionesController@obtenerReferencia',
		'as'	=>	'liquidaciones.obtener']);
	Route::post('viaticos/buscar/programaciones', [
		'uses'	=>	'Viaticos\LiquidacionesController@buscarProgramaciones',
		'as'	=>	'liquidaciones.buscar']);
	Route::post('viaticos/anular/{id}/programacion', [
		'uses'	=>	'Viaticos\LiquidacionesController@anularProgramaciones',
		'as'	=>	'liquidaciones.anular']);
	Route::post('viaticos/guardar/liquidacion', [
		'uses'	=>	'Viaticos\LiquidacionesController@guardarLiquidacion',
		'as'	=>	'liquidaciones.guardar']);
	Route::get('viaticos/liquidacion/buscar',[
		'uses'	=>	'Viaticos\LiquidacionesController@buscarLiquidacion',
		'as'	=>	'liquidacion.buscar']);
	Route::get('viaticos/liquidacion/{referencia}/mostrar', [
		'uses'	=> 'Viaticos\LiquidacionesController@mostrarLiquidacion',
		'as'	=>	'liquidaciones.mostrar']);
	Route::post('viaticos/liquidaciones/reporte',[
		'uses'	=>	'Viaticos\LiquidacionesController@reporte',
		'as'	=>	'reporte.liquidacion']);
	Route::post('viaticos/liquidacion/{id}/cerrado',[
		'uses'	=>	'Viaticos\LiquidacionesController@liquidadoCerrado',
		'as'	=>	'liquidado.cerrado']);
	Route::post('viaticos/liquidacion/{id}/liquidado',[
		'uses'	=>	'Viaticos\LiquidacionesController@cerradoLiquidado',
		'as'	=>	'liquidado.liquidado']);



	


});
