<?php

/*
|--------------------------------------------------------------------------
| Rutas Publicas

|--------------------------------------------------------------------------
|--------------------------------------------------------------------------*/

Route::get('password/reset',[
	'uses'	=> 	'Auth\ForgotPasswordController@showLinkRequestForm',
	'as'	=>	'password.request']);

Route::post('password/email',[
	'uses'	=> 	'Auth\ForgotPasswordController@sendResetLinkEmail',
	'as'	=>	'password.email']);

Route::get('password/reseteo/{token}',[
	'uses'	=> 	'Auth\ResetPasswordController@showResetForm',
	'as'	=>	'password.reseteo']);

Route::post('password/reset',[
	'uses'	=> 	'Auth\ResetPasswordController@reset']);


Route::post('password/reset', [
	'uses'	=> 'Auth\ResetPasswordController@reset',
	'as'	=>  'password.update']);

/**************** Inicio de Sesión y Pagina Principal ****************/
	Route::get('/', [
	'uses'	=>'HomeController@index',
	'as'	=>'home']);
	Route::get('inicio', [
		'uses'	=>	'AuthController@inicioSesion',
		'as'	=>	'inicioSesion']);
	Route::post('/inicio', [
		'uses'	=>	'AuthController@iniciarSesion',
		'as'	=>	'iniciarSesion']);
    Route::get('logout', [
		'uses'	=>	'AuthController@cerrarSesion',
		'as'	=>	'cerrarSesion']);
	Route::post('logout', [
		'uses'	=>	'AuthController@salirSesion',
		'as'	=>	'salirSesion']);