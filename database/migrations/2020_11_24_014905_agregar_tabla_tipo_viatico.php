<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AgregarTablaTipoViatico extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('via_tipo_viatico', function (Blueprint $table) {
            $table->bigIncrements('id_tipo_viatico');
            $table->string('tipo_viatico');
            $table->char('codigo',1);
            $table->integer('valor');
            $table->boolean('estado');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('via_tipo_viatico');
    }
}
