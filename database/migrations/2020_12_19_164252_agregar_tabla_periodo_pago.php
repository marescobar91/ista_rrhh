<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AgregarTablaPeriodoPago extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('via_periodo', function (Blueprint $table) {
            $table->bigIncrements('id_periodo');
            $table->unsignedBigInteger('centro_id');
            $table->string('mes');
            $table->integer('anio');
            $table->boolean('estado');
            $table->foreign("centro_id")->references("id_centro")->on("emp_centro")->onDelete("cascade")->onUpdate("cascade");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('via_periodo');
    }
}
