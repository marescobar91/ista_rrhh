<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AgregarColumnaFechas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('emp_centro', function (Blueprint $table) {
            $table->integer('fecha_inicio')->nullable()->after('encargado');
            $table->time('hora_inicio')->nullable()->after('fecha_inicio');
            $table->integer('fecha_fin')->nullable()->after('hora_inicio');
            $table->time('hora_fin')->nullable()->after('fecha_fin');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('emp_centro', function (Blueprint $table) {
            $table->dropColumn('fecha_inicio');
            $table->dropColumn('hora_inicio');
            $table->dropColumn('fecha_fin');
            $table->dropColumn('hora_fin');
        });
    }
}
