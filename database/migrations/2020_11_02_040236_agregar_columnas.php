<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AgregarColumnas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mar_hora_extra', function (Blueprint $table) {
            $table->integer('mes')->after('minutos');
            $table->decimal('devengo',8,4)->after('mes');
            $table->decimal('isss',8,4)->after('devengo');
            $table->decimal('insaforp',8,4)->after('isss');
            $table->decimal('inpep',8,4)->after('insaforp');
            $table->decimal('afp',8,4)->after('inpep');
            $table->decimal('total',8,4)->after('afp');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mar_hora_extra', function (Blueprint $table) {
            $table->dropColumn('mes');
            $table->dropColumn('devengo');
            $table->dropColumn('isss');
            $table->dropColumn('insaforp');
            $table->dropColumn('inpep');
            $table->dropColumn('afp');
            $table->dropColumn('total');
        });
    }
}
