<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AgregarTablaPivoteUsuarioOficina extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin_usuario_oficina', function (Blueprint $table) {
            $table->bigIncrements('id_usuario_oficina');
            $table->unsignedBigInteger('usuario_id');
            $table->unsignedBigInteger('oficina_id');
            $table->foreign("usuario_id")->references("id_usuario")->on("admin_usuario")->onDelete("cascade")->onUpdate("cascade");
            $table->foreign("oficina_id")->references("id_oficina")->on("emp_oficina")->onDelete("cascade")->onUpdate("cascade");

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admin_usuario_oficina');
    }
}
