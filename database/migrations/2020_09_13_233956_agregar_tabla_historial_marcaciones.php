<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AgregarTablaHistorialMarcaciones extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mar_historial', function (Blueprint $table) {
            $table->bigIncrements('id_historial');
            $table->date('fecha_importar');
            $table->time('hora_importar');
            $table->string('cantidad_registros')->nullable();
           
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mar_historial');
    }
}
