<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTableMarCalendario extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mar_calendario', function (Blueprint $table) {
            $table->bigIncrements('id_calendario');
            $table->date('fecha');
            $table->string('dia');
            $table->string('mes');
            $table->string('anio');
            $table->string('formato_fecha');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mar_calendario');
    }
}
