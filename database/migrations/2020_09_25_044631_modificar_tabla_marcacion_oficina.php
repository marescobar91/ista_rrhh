<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ModificarTablaMarcacionOficina extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mar_marcacion', function (Blueprint $table) {
             $table->unsignedBigInteger('oficina_id')->after('hora');
             $table->foreign("oficina_id")->references("id_oficina")->on("emp_oficina")->onDelete("cascade")->onUpdate("cascade");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mar_marcacion', function (Blueprint $table) {
             $table->dropForeign('mar_marcacion_oficina_id_foreign');
        });
    }
}
