<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ModificarTablaLugarMision extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('via_lugar_mision', function (Blueprint $table) {
            
        $table->foreign("departamento_id")->references("id_departamento")->on("emp_departamento")->onDelete("cascade")->onUpdate("cascade");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('via_lugar_mision', function (Blueprint $table) {
            $table->dropForeign('via_lugar_mision_departamento_id_foreign');
        });
    }
}
