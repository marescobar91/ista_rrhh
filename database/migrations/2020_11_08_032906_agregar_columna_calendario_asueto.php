<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AgregarColumnaCalendarioAsueto extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mar_asueto', function (Blueprint $table) {
            $table->unsignedBigInteger('calendario_id')->after('id_asueto');
            $table->foreign("calendario_id")->references("id_calendario")->on("mar_calendario")->onDelete("cascade")->onUpdate("cascade");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mar_asueto', function (Blueprint $table) {
            $table->dropColumn('calendario_id');
            $table->dropForeign('mar_asueto_calendario_id_foreign');
        });
    }
}
