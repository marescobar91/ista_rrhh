<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AgregarTablaProgramacion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('via_programacion', function (Blueprint $table) {
            $table->bigIncrements('id_programacion');
            $table->unsignedBigInteger('empleado_id');
            $table->unsignedBigInteger('oficina_id');
            $table->unsignedBigInteger('lugar_mision_id');
            $table->unsignedBigInteger('actividad_id');
            $table->unsignedBigInteger('tipo_viatico_id');
            $table->date('fecha_desde');
            $table->date('fecha_hasta');
            $table->boolean('impreso');
            $table->boolean('estado');
            $table->timestamps();
            $table->foreign("empleado_id")->references("id_empleado")->on("emp_empleado")->onDelete("cascade")->onUpdate("cascade");
            $table->foreign("oficina_id")->references("id_oficina")->on("emp_oficina")->onDelete("cascade")->onUpdate("cascade");
            $table->foreign("lugar_mision_id")->references("id_lugar_mision")->on("via_lugar_mision")->onDelete("cascade")->onUpdate("cascade");
            $table->foreign("actividad_id")->references("id_actividad")->on("via_actividad")->onDelete("cascade")->onUpdate("cascade");
            $table->foreign("tipo_viatico_id")->references("id_tipo_viatico")->on("via_tipo_viatico")->onDelete("cascade")->onUpdate("cascade");

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('via_programacion');
    }
}
