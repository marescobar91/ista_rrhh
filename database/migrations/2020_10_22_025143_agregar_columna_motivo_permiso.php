<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AgregarColumnaMotivoPermiso extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mar_permiso', function (Blueprint $table) {
            $table->string('motivo_permiso',50)->after('tipo_permiso_id');
        });
    }

    /**
     * Reverse the migrations.
     *     * @return void

     */
    public function down()
    {
        Schema::table('mar_permiso', function (Blueprint $table) {
            $table->dropColumn('motivo_permiso');
        });
    }
}
