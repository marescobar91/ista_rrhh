<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMarPermisoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mar_permiso', function (Blueprint $table) {
            $table->bigIncrements('id_permiso');
            $table->unsignedBigInteger('empleado_id');
            $table->unsignedBigInteger('tipo_permiso_id');
            $table->decimal('horas_acumuladas', 8,2);
            $table->date('fecha_registro');
            $table->date('fecha_inicio');
            $table->date('fecha_fin');
            $table->time('hora_inicio');
            $table->time('hora_fin');
            $table->timestamps();

            $table->foreign('empleado_id')->references('id_empleado')->on('emp_empleado')->onDelete('cascade');
            $table->foreign('tipo_permiso_id')->references('id_tipo_permiso')->on('mar_tipo_permiso')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mar_permiso');
    }
}
