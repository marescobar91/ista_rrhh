<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAsuetoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mar_asueto', function (Blueprint $table) {
            $table->bigIncrements('id_asueto');
            $table->date('fecha');
            $table->string('asueto');
            $table->string('dia');
            $table->string('mes');
            $table->string('anio');
            $table->string('formato_fecha');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mar_asueto');
    }
}
