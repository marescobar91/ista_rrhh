<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AgregarTablaUnidadAdministrativa extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('emp_unidad_administrativa', function (Blueprint $table) {
            //
            $table->boolean('estado')->after('unidad_administrativa');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('emp_unidad_administrativa', function (Blueprint $table) {
            //
            $table->dropColumn('estado');
        });
    }
}
