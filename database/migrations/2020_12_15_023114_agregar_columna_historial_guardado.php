<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AgregarColumnaHistorialGuardado extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('emp_historial', function (Blueprint $table) {
           $table->boolean('guardado')->after('fecha_retiro');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('emp_historial', function (Blueprint $table) {
             $table->dropColumn('guardado');
        });
    }
}
