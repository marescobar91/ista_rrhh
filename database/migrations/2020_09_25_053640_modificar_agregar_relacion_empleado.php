<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ModificarAgregarRelacionEmpleado extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mar_marcacion', function (Blueprint $table) {
             $table->unsignedBigInteger('empleado_id')->after('oficina_id');
             $table->foreign("empleado_id")->references("id_empleado")->on("emp_empleado")->onDelete("cascade")->onUpdate("cascade");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mar_marcacion', function (Blueprint $table) {
            $table->dropForeign('mar_marcacion_empleado_id_foreign');
        });
    }
}
