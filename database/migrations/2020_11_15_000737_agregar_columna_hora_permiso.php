<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AgregarColumnaHoraPermiso extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mar_permiso_detalle', function (Blueprint $table) {
            $table->time('hora_inicio')->after('fecha_permiso');
            $table->time('hora_fin')->after('hora_inicio');
            $table->decimal('horas_acumuladas', 8,4)->after('hora_fin');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mar_permiso_detalle', function (Blueprint $table) {
            $table->dropColumn('hora_inicio');
            $table->dropColumn('hora_fin');
            $table->dropColumn('horas_acumuladas');
        });
    }
}
