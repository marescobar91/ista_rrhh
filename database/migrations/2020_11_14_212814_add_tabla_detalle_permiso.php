<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTablaDetallePermiso extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mar_permiso_detalle', function (Blueprint $table) {
            $table->bigIncrements('id_permiso_detalle');
            $table->unsignedBigInteger('permiso_id');
            $table->foreign('permiso_id')->references('id_permiso')->on('mar_permiso')->onDelete("cascade")->onUpdate("cascade");
            $table->date('fecha_permiso');
            $table->unsignedBigInteger('emp_calendario_id');
            $table->foreign('emp_calendario_id')->references('id_calendario_emp')->on('mar_calendario_emp')->onDelete("cascade")->onUpdate("cascade");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mar_permiso_detalle');
    }
}
