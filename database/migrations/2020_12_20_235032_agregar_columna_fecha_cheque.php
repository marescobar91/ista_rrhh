<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AgregarColumnaFechaCheque extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('via_viatico', function (Blueprint $table) {
            $table->date('fecha_cheque')->nullable()->after('cheque');
            $table->date('fecha_remesa')->nullable()->after('cantidad_remesa');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('via_viatico', function (Blueprint $table) {
            $table->dropColumn('fecha_cheque');
            $table->dropColumn('fecha_remesa');
        });
    }
}
