<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AgregarTablaEmpleadoTipoDocumento extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('emp_empleado', function (Blueprint $table) {
            //
            $table->enum('tipo_documento', ['Dui', 'Pasaporte'])->after('numero_expediente')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('emp_empleado', function (Blueprint $table) {
            //
            $table->dropColumn('tipo_documento');
        });
    }
}
