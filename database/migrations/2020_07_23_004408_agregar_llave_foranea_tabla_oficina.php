<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AgregarLlaveForaneaTablaOficina extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('emp_oficina', function (Blueprint $table) {
            //
            $table->foreign("unidad_administrativa_id")->references("id_unidad_administrativa")->on("emp_unidad_administrativa")->onDelete("cascade")->onUpdate("cascade");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('emp_oficina', function (Blueprint $table) {
            //
        $table->dropForeign('emp_oficina_unidad_administrativa_id_foreign');
        });
    }
}
