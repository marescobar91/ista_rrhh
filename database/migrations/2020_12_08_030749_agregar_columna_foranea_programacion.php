<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AgregarColumnaForaneaProgramacion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('via_programacion', function (Blueprint $table) {
            $table->unsignedBigInteger('viatico_id')->nullable()->after('tipo_viatico_id');
            $table->foreign("viatico_id")->references("id_viatico")->on("via_viatico")->onDelete("cascade")->onUpdate("cascade");

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('via_programacion', function (Blueprint $table) {
            $table->dropForeign('via_programacion_viatico_id_foreign');
             $table->dropColumn('viatico_id');
        });
    }
}
