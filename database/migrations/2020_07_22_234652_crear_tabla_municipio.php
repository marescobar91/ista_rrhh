<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CrearTablaMunicipio extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('emp_municipio', function (Blueprint $table) {
            $table->bigIncrements('id_municipio');
            $table->unsignedBigInteger('departamento_id');
            $table->string('municipio', 50);
            $table->timestamps();

            $table->foreign('departamento_id')->references('id_departamento')->on('emp_departamento')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('emp_municipio');
    }
}
