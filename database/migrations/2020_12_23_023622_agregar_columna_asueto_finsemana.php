<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AgregarColumnaAsuetoFinsemana extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('emp_centro', function (Blueprint $table) {
            $table->boolean('fin_semana')->after('hora_fin');
            $table->boolean('asueto')->after('fin_semana');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('emp_centro', function (Blueprint $table) {
            $table->dropColumn('fin_semana');
            $table->dropColumn('asueto');
        });
    }
}
