<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AgregarColumnaIdCalendario extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mar_marcacion', function (Blueprint $table) {
            //
        $table->unsignedBigInteger('emp_calendario_id')->after('empleado_id');
        $table->foreign('emp_calendario_id')->references('id_calendario_emp')->on('mar_calendario_emp')->onDelete("cascade")->onUpdate("cascade");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mar_marcacion', function (Blueprint $table) {
            $table->dropColumn('emp_calendario_id');
            $table->dropForeign('mar_marcacion_emp_calendario_id_foreign');
        });
    }
}
