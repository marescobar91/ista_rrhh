<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTipoPermisoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mar_tipo_permiso', function (Blueprint $table) {
            $table->bigIncrements('id_tipo_permiso');
            $table->string('codigo_permiso', 4);
            $table->string('descripcion',50);
            $table->decimal('numero_horas',8,2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mar_tipo_permiso');
    }
}
