<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTableHorasExtras extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mar_hora_extra', function (Blueprint $table) {
            $table->bigIncrements('id_hora_extra');
            $table->unsignedBigInteger('empleado_id');
            $table->date('fecha_desde');
            $table->date('fecha_hasta');
            $table->integer('horas');
            $table->integer('minutos');
            $table->date('fecha_registro');
            $table->foreign("empleado_id")->references("id_empleado")->on("emp_empleado")->onDelete("cascade")->onUpdate("cascade");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mar_hora_extra');
    }
}
