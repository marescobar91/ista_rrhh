<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CrearTablaHistorial extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('emp_historial', function (Blueprint $table) {
            $table->bigIncrements('id_historial');
            $table->unsignedBigInteger('empleado_id')->nullable();
            $table->unsignedBigInteger('usuario_id')->nullable();
            $table->unsignedBigInteger('municipio_id')->nullable();
            $table->unsignedBigInteger('nivel_academico_id')->nullable();
            $table->unsignedBigInteger('cargo_nominal_id')->nullable();
            $table->unsignedBigInteger('tipo_plaza_id')->nullable();
            $table->unsignedBigInteger('estado_civil_id')->nullable();
            $table->unsignedBigInteger('banco_id')->nullable();
            $table->unsignedBigInteger('cargo_funcional_id')->nullable();
            $table->unsignedBigInteger('oficina_id')->nullable();
            $table->unsignedBigInteger('cargo_mp_id')->nullable();
            $table->unsignedBigInteger('cargo_hacienda_id')->nullable();
            $table->char('codigo_empleado', 7)->nullable();
            $table->string('nombre', 80)->nullable();
            $table->string('apellido',80)->nullable();
            $table->date('fecha_traslado')->nullable();
            $table->integer('pin_empleado')->nullable();
            $table->integer('numero_expediente')->nullable();
            $table->enum('tipo_documento', ['Dui', 'Pasaporte'])->nullable();
            $table->char('numero_documento', 15)->nullable();
            $table->date('fecha_nacimiento')->nullable();
            $table->string('lugar_nacimiento', 70)->nullable();
            $table->string('nit', 18)->nullable();
            $table->char('isss', 9)->nullable();
            $table->char('prevision', 30)->nullable();
            $table->char('nup', 15)->nullable();
            $table->string('direccion', 100)->nullable();
            $table->char('telefono',9)->nullable();
            $table->string('nombre_contacto', 100)->nullable();
            $table->char('telefono_contacto', 9)->nullable();
            $table->char('sexo', 6)->nullable();
            $table->date('fecha_ingreso')->nullable();
            $table->date('fecha_reingreso')->nullable();
            $table->decimal('salario_inicial',8,2)->nullable();
            $table->decimal('salario_actual',8,2)->nullable();
            $table->string('cuenta_banco',25)->nullable();
            $table->string('cifra_presupuestaria',30)->nullable();
            $table->string('numero_acuerdo', 30)->nullable();
            $table->string('numero_partida',30)->nullable();
            $table->string('numero_subpartida',30)->nullable();
            $table->boolean('afiliado_sindicato')->nullable();
            $table->boolean('rendir_probidad')->nullable();
            $table->boolean('es_jefe')->nullable();
            $table->boolean('marca')->nullable();
            $table->boolean('autorizado_estudio')->nullable();
            $table->boolean('pensionado')->nullable();
            $table->boolean('enviar_jefe')->nullable();
            $table->boolean('pasivo')->nullable();
            $table->boolean('activo')->nullable();
            $table->string('gifcard', 30)->nullable();
            $table->string('numero_acuerdo_retiro',30)->nullable();
            $table->string('motivo_retiro')->nullable();
            $table->date('fecha_retiro')->nullable();
            $table->timestamps();

            $table->foreign("municipio_id")->references("id_municipio")->on("emp_municipio")->onDelete("cascade")->onUpdate("cascade");

            $table->foreign("nivel_academico_id")->references("id_nivel_academico")->on("emp_nivel_academico")->onDelete("cascade")->onUpdate("cascade");

            $table->foreign("cargo_nominal_id")->references("id_cargo")->on("emp_cargo")->onDelete("cascade")->onUpdate("cascade");

            $table->foreign("tipo_plaza_id")->references("id_tipo_plaza")->on("emp_tipo_plaza")->onDelete("cascade")->onUpdate("cascade");

            $table->foreign("estado_civil_id")->references("id_estado_civil")->on("emp_estado_civil")->onDelete("cascade")->onUpdate("cascade");

            $table->foreign("banco_id")->references("id_banco")->on("emp_banco")->onDelete("cascade")->onUpdate("cascade");

            $table->foreign("cargo_funcional_id")->references("id_cargo")->on("emp_cargo")->onDelete("cascade")->onUpdate("cascade");

            $table->foreign("oficina_id")->references("id_oficina")->on("emp_oficina")->onDelete("cascade")->onUpdate("cascade");

            $table->foreign("cargo_mp_id")->references("id_cargo_mp")->on("emp_cargo_mp")->onDelete("cascade")->onUpdate("cascade");

            $table->foreign("cargo_hacienda_id")->references("id_cargo_hacienda")->on("emp_cargo_hacienda")->onDelete("cascade")->onUpdate("cascade");

            $table->foreign("empleado_id")->references("id_empleado")->on("emp_empleado")->onDelete("cascade")->onUpdate("cascade");

            $table->foreign("usuario_id")->references("id_usuario")->on("admin_usuario")->onDelete("cascade")->onUpdate("cascade");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('emp_historial');
    }
}
