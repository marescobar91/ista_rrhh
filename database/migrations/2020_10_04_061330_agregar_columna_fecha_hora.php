<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AgregarColumnaFechaHora extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mar_marcacion', function (Blueprint $table) {
            $table->dateTime('fecha_hora')->after('hora');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mar_marcacion', function (Blueprint $table) {
           $table->dropColumn('fecha_hora');
        });
    }
}
