<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AgregarEstadoTipopermiso extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mar_tipo_permiso', function (Blueprint $table) {
            $table->boolean('estado')->after('numero_horas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mar_tipo_permiso', function (Blueprint $table) {
             $table->dropColumn('estado');
        });
    }
}
