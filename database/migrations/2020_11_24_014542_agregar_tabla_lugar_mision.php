<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AgregarTablaLugarMision extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('via_lugar_mision', function (Blueprint $table) {
            $table->bigIncrements('id_lugar_mision');
            $table->string('lugar_mision');
            $table->unsignedBigInteger('departamento_id');
            $table->decimal('kilometros', 8,2);
            $table->boolean('estado');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('via_lugar_mision');
    }
}
