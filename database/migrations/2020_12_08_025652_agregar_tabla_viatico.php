<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AgregarTablaViatico extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('via_viatico', function (Blueprint $table) {
            $table->bigIncrements('id_viatico');
            $table->string('referencia');
            $table->string('referencia_ufi')->nullable();
            $table->date('fecha');
            $table->unsignedBigInteger('centro_id');
            $table->string('cheque')->nullable();
            $table->integer('cantidad_remesa');
            $table->string('folio')->nullable();
            $table->integer('estado');
            $table->unsignedBigInteger('tipo_programacion_id');
            $table->unsignedBigInteger('forma_pago_id');
            $table->foreign("centro_id")->references("id_centro")->on("emp_centro")->onDelete("cascade")->onUpdate("cascade");
            $table->foreign("tipo_programacion_id")->references("id_tipo_programacion")->on("via_tipo_programacion")->onDelete("cascade")->onUpdate("cascade");
            $table->foreign("forma_pago_id")->references("id_forma_pago")->on("via_forma_pago")->onDelete("cascade")->onUpdate("cascade");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('via_viatico');
    }
}
