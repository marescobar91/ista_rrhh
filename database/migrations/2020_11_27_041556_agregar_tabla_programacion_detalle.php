<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AgregarTablaProgramacionDetalle extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('via_programacion_detalle', function (Blueprint $table) {
            $table->bigIncrements('id_programacion_detalle');
            $table->unsignedBigInteger('programacion_id');
            $table->date('fecha_programacion');
            $table->integer('estado');
            $table->foreign("programacion_id")->references("id_programacion")->on("via_programacion")->onDelete("cascade")->onUpdate("cascade");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('via_programacion_detalle');
    }
}
