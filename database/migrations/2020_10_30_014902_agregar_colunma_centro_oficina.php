<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AgregarColunmaCentroOficina extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('emp_oficina', function (Blueprint $table) {
            $table->unsignedBigInteger('centro_id')->after('unidad_administrativa_id');
            $table->foreign("centro_id")->references("id_centro")->on("emp_centro")->onDelete("cascade")->onUpdate("cascade");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('emp_oficina', function (Blueprint $table) {
        $table->dropForeign('emp_oficina_centro_id_foreign');
        });
    }
}
