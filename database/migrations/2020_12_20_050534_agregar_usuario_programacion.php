<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AgregarUsuarioProgramacion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('via_programacion', function (Blueprint $table) {
            $table->unsignedBigInteger('usuario_id')->after('oficina_id');
             $table->foreign("usuario_id")->references("id_usuario")->on("admin_usuario")->onDelete("cascade")->onUpdate("cascade");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('via_programacion', function (Blueprint $table) {
           
            $table->dropForeign('via_programacion_usuario_id_foreign');
             $table->dropColumn('usuario_id');

        });
    }
}
