<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AgregarColumnaFechaLiquidar extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('via_viatico', function (Blueprint $table) {
            $table->date('fecha_liquidar')->nullable()->after('folio');
            $table->string('mes_liquidar')->nullable()->after('fecha_liquidar');
            $table->string('anio_liquidar')->nullable()->after('mes_liquidar');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('via_viatico', function (Blueprint $table) {
            $table->dropColumn('fecha_liquidar');
            $table->dropColumn('mes_liquidar');
            $table->dropColumn('anio_liquidar');
        });
    }
}
