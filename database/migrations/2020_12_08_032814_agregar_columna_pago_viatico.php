<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AgregarColumnaPagoViatico extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('via_programacion', function (Blueprint $table) {
        $table->double('pago_viatico', 8,2)->after('fecha_hasta');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('via_programacion', function (Blueprint $table) {
        $table->dropColumn('pago_viatico');
        });
    }
}
