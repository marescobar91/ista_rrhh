<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AgregarTablaMarcaciones extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mar_marcacion', function (Blueprint $table) {
            $table->bigIncrements('id_marcacion');
            $table->string('distintivo');
            $table->string('formato_fecha');
            $table->string('formato_hora');
            $table->integer('pin_empleado');
    
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mar_marcacion');
    }
}
