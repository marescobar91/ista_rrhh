<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AgregarColumnasRemesa extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('via_viatico', function (Blueprint $table) {
            $table->integer('cantidad_remesa_segundo')->after('folio');
            $table->date('fecha_remesa_segundo')->nullable()->after('cantidad_remesa_segundo');
            $table->string('folio_segundo')->nullable()->after('fecha_remesa_segundo');
           
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('via_viatico', function (Blueprint $table) {
            $table->dropColumnn('cantidad_remesa_segundo');
            $table->dropColumnn('fecha_remesa_segundo');
            $table->dropColumnn('folio_segundo');
        });
    }
}
