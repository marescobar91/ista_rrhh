<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AgregarTablaTipoPlaza extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('emp_tipo_plaza', function (Blueprint $table) {
            //
            $table->boolean('estado')->after('tipo_plaza');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('emp_tipo_plaza', function (Blueprint $table) {
            //
            $table->dropColumn('tipo_plaza');
        });
    }
}
