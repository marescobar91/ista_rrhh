<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CrearTableCalendarioMarc extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mar_calendario_emp', function (Blueprint $table) {
            $table->bigIncrements('id_calendario_emp')->nullable();
            $table->unsignedBigInteger('calendario_id')->nullable();
            $table->foreign("calendario_id")->references("id_calendario")->on("mar_calendario")->onDelete("cascade")->onUpdate("cascade");
            $table->unsignedBigInteger('empleado_id');
            $table->foreign("empleado_id")->references("id_empleado")->on("emp_empleado")->onDelete("cascade")->onUpdate("cascade");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mar_calendario_emp');
    }
}
