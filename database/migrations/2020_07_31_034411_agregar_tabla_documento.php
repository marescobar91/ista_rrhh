<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AgregarTablaDocumento extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('emp_documento', function (Blueprint $table) {
            $table->bigIncrements('id_documento');
            $table->unsignedBigInteger('empleado_id');
            $table->string('descripcion');
            $table->string('ruta');

            $table->foreign("empleado_id")->references("id_empleado")->on("emp_empleado")->onDelete("cascade")->onUpdate("cascade");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('emp_documento');
    }
}
