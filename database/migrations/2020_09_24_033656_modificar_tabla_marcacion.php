<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ModificarTablaMarcacion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mar_marcacion', function (Blueprint $table) {
            $table->date('fecha')->after('pin_empleado');
            $table->timeTz('hora')->after('fecha');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mar_marcacion', function (Blueprint $table) {
            //
            $table->dropColumn('fecha');
            $table->dropColumn('hora');
        });
    }
}
