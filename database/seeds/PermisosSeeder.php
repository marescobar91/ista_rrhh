<?php

use Illuminate\Database\Seeder;

class PermisosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()

    {
    	 app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();

    	Permission::create(['name'	=>	'crear.permisos']);
    	Permission::create(['name'	=>	'editar.permisos']);
    	Permission::create(['name'	=>	'consultar.permisos']);
    	Permission::create(['name'	=>	'eliminar.permisos']);
    	Permission::create(['name'	=>	'crear.roles']);
    	Permission::create(['name'	=>	'editar.roles']);
    	Permission::create(['name'	=>	'consultar.roles']);
    	Permission::create(['name'	=>	'eliminar.roles']);
    	Permission::create(['name'	=>	'crear.usuarios']);
    	Permission::create(['name'	=>	'editar.usuarios']);
    	Permission::create(['name'	=>	'consultar.usuarios']);
    	Permission::create(['name'	=>	'eliminar.usuarios']);
    	Permission::create(['name'	=>	'resetear.password']);
    	Permission::create(['name'	=>	'crear.empleados']);
    	Permission::create(['name'	=>	'editar.empleados']);
    	Permission::create(['name'	=>	'consultar.empleados']);
    	Permission::create(['name'	=>	'eliminar.empleados']);
    	Permission::create(['name'	=>	'habilitar.empleados']);
    	Permission::create(['name'	=>	'habilitar.usuarios']);
    	Permission::create(['name'	=>	'consultar.reportes']);
    	Permission::create(['name'	=>	'agregar.documentos']);
    	Permission::create(['name'	=>	'reemplazar.documentos']);
		Permission::create(['name'	=>	'consultar.documentos']);
		Permission::create(['name'	=>	'eliminar.documentos']);
		Permission::create(['name'	=>	'crear.catalogos']);
		Permission::create(['name'	=>	'editar.catalogos']);
		Permission::create(['name'	=>	'consultar.catalogos']);
		Permission::create(['name'	=>	'eliminar.catalogos']);
		Permission::create(['name'	=>	'habilitar.catalogos']);
		Permission::create(['name'	=>	'crear.asuetos']);
		Permission::create(['name'	=>	'editar.asuetos']);
		Permission::create(['name'	=>	'cargar.asuetos']);
		Permission::create(['name'	=>	'consultar.asuetos']);
		Permission::create(['name'	=>	'eliminar.asuetos']);
		Permission::create(['name'	=>	'listar.historial']);
		Permission::create(['name'	=>	'crear.permisos.empleado']);
		Permission::create(['name'	=>	'editar.permisos.empleado']);
		Permission::create(['name'	=>	'consultar.permisos.empleado']);
		Permission::create(['name'	=>	'eliminar.permisos.empleado']);
		Permission::create(['name'	=>	'habilitar.permisos.empleado']);
		Permission::create(['name'	=>	'buscar.permisos.empleado']);
		Permission::create(['name'	=>	'crear.horas.extras']);
		Permission::create(['name'	=>	'editar.horas.extras']);
		Permission::create(['name'	=>	'eliminar.horas.extras']);
		Permission::create(['name'	=>	'consultar.horas.extras']);
		Permission::create(['name'	=>	'reporte.horas.extras']);
		Permission::create(['name'	=>	'consultar.marcaciones']);
		Permission::create(['name'	=>	'reporte.marcaciones']);
		Permission::create(['name'	=>	'crear.viaticos']);
		Permission::create(['name'	=>	'editar.liquidacion']);
		Permission::create(['name'	=>	'reporte.liquidacion']);
		Permission::create(['name'	=>	'asignar.centro']);
		Permission::create(['name'	=>	'programar.centro']);
		Permission::create(['name'	=>	'activar.centro']);
		Permission::create(['name'	=>	'desactivar.centro']);
		Permission::create(['name'	=>	'crear.liquidacion']);
		Permission::create(['name'	=>	'consultar.liquidacion']);
		Permission::create(['name'	=>	'buscar.liquidacion']);
		Permission::create(['name'	=>	'eliminar.periodo']);
		Permission::create(['name'	=>	'habilitar.periodo']);
		Permission::create(['name'	=>	'crear.periodo']);
		Permission::create(['name'	=>	'consultar.periodo']);
		Permission::create(['name'	=>	'editar.programacion']);
		Permission::create(['name'	=>	'eliminar.programacion']);
		Permission::create(['name'	=>	'habilitar.programacion']);
		Permission::create(['name'	=>	'crear.programacion']);
		Permission::create(['name'	=>	'generar.cco']);
		Permission::create(['name'	=>	'buscar.programacion']);
		Permission::create(['name'	=>	'buscar.informe']);
		Permission::create(['name'	=>	'listar.informe']);
		Permission::create(['name'	=>	'habilitar.viatico']);
		Permission::create(['name'	=>	'crear.viatico']);
		Permission::create(['name'	=>	'consultar.viatico']);
		Permission::create(['name'	=>	'reporte.permiso.empleado']);

		/*$roleUno = Role::create(['name'	=>	'Administrador']);
		$roleDos = Role::create(['name'	=>	'Gestor de Usuarios']);
		$roleTres = Role::create(['name'	=>	'Gestor de Empleados']);
		$roleCuatro = Role::create(['name'	=>	'Gestor de Claves']);
		$roleCinco = Role::create(['name'	=>	'Consultor de Empleados']);
		$roleSeis = Role::create(['name'	=>	'Generar Reporte Empleados']);
		$roleSiete = Role::create(['name'	=>	'Gestor de Marcaciones']);
		$roleOcho = Role::create(['name'	=>	'Consultar de Marcaciones']);
		$roleNueve = Role::create(['name'	=>	'Generar Reporte Marcaciones']);
		$roleDiez = Role::create(['name'	=>	'Gestor de Permiso']);
		$roleOnce = Role::create(['name'	=>	'Consultor de Permisos']);
		$roleDoce = Role::create(['name'	=>	'Gestor de Horas Extras']);
		$roleTrece = Role::create(['name'	=>	'Consultor de Horas Extras']);
		$roleCatorce = Role::create(['name'	=>	'Generar Reporte Horas Extras']);
		$roleQuince = Role::create(['name'	=>	'Secretaria']);
		$roleDieciseis = Role::create(['name'	=>	'Gestor de Caja Chica']);
		$roleDiecisiete = Role::create(['name'	=>	'Gestor de Fondo Circulante']);
		$roleDieciocho = Role::create(['name'	=>	'Gestor de Recursos Humanos']);
		$roleDiecinueve = Role::create(['name'	=>	'Administrador Regional']);*/
		//asignar permisos
		//
		//Asignado a roles, permisos.
		/*$roleUno->givePermissionTo([
            'products.index',
            'products.edit',
            'products.show',
            'products.create',
            'products.destroy'
        ]);*/



    }
}
