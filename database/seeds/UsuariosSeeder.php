<?php

use Illuminate\Database\Seeder;

class UsuariosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        App\Usuario::create([
        	'nombre_usuario' 	=>	'ISTAAdmin',
        	'email'				=>	'istarrhh@gmail.com',
        	'password'			=>	bcrypt('IstaRRHHAdmin'),
        ]);
    }
}
