@extends('layouts.template')
@section('content')
<section role="main" class="content-body">
    <!-- start: page -->
    <div class="row">
        <div class="col-lg-12">
          
            <section role="main" class="content-body">
                    <header class="page-header">
                        <h2>404</h2>
                    </header>

                    <!-- start: page -->
                        <section class="body-error error-inside">
                            <div class="center-error">

                                <div class="row">
                                    <div class="col-md-8">
                                        <div class="main-error mb-xlg">
                                            <h2 class="error-code text-dark text-center text-semibold m-none"> 403 <i class="fa fa-file"></i></h2>
                                             
                                        </div>


                                    </div>

                                   
                        
                                
                                </div>
                                <a href="{{route('home')}}"><button class="btn btn-primary"> Regresar a Página Principal </button></a>
                                </div>
                            </div>
                        </section>
                    <!-- end: page -->
                </section>
        </div>
    </div>
    <!-- end: page -->
</section>

@endsection
