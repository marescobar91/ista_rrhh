@extends('layouts.template')
@section('content')
<section role="main" class="content-body">
    <header class="page-header">
        <h2>Gestión de Tipo de Programaciones</h2>
        <div class="right-wrapper pull-right">
            <ol class="breadcrumbs">
                <li style="margin-left: -25%"><a href="{{route('tipo.programaciones.listar')}}"><button class="btn btn-default"><span class="fa fa-chevron-left"> </span> Regresar </button></a></li>
            </ol>
        </div>
    </header>

    <!-- start: page -->
    <div class="row">
        <div class="col-lg-12">
            <div>
                @if ($errors->any())
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    
                    <div class="errors">
                        <ul>
                            @foreach ($errors->all() as $error)

                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif

                </div>
            </div>
            <section class="panel">
                <header class="panel-heading">
                    <div class="panel-actions">
                    </div>
                    <h2 class="panel-title">Editar Tipo de Programación</h2>
                </header>
                <div class="panel-body">
                  <form action="{{ route('tipo.programaciones.actualizar', $tipoProgramacion->id_tipo_programacion) }}" method="post" role="form" class="contactForm">
                    {{csrf_field()}}
                    {{ method_field('PUT') }}
                    <div class="form-group">
                            <div class="col-md-3"></div>
                            <div class="col-md-6">
                                 <label class="col-md-12 control-label text-right" style="text-align: left;">Tipo de Programación</label>
                                <div class="col-md-12">
                                    <input id="tipo_programacion" type="text" name="tipo_programacion" required autocomplete="tipo_programacion" autofocus placeholder="Tipo de Programación" class="form-control field-input {{ $errors->has('tipo_programacion') ? 'field-error' : '' }}" value="{{$tipoProgramacion->tipo_programacion}}">
                                    @if ($errors->has('tipo_programacion'))<span class="error-message">{{ $errors->first('tipo_programacion') }}</span>
                                    @endif
                                </div>
                            </div>
                        </div>  
                    <div class="col-lg-12 text-center" >    
                     <button type="submit" class="btn btn-warning">Editar</button>
                     <a href="{{route('home')}}" class="btn btn-default">Cancelar</a>
                 </div>                              
             </form>
         </div>
     </section>
     
 </div>
</div>
<!-- end: page -->
</section>

@endsection
