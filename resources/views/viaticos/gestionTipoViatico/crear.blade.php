@extends('layouts.template')
@section('content')
<section role="main" class="content-body">
    <header class="page-header">
        <h2>Gestión de Tipo de Viáticos</h2>
        <div class="right-wrapper pull-right">
            <ol class="breadcrumbs">
                <li style="margin-left: -25%"><a href="{{route('tipo.viaticos.listar')}}"><button class="btn btn-default"><span class="fa fa-chevron-left"> </span> Regresar </button></a></li>
            </ol>
        </div>
    </header>

    <!-- start: page -->
    <div class="row">
        <div class="col-lg-12">
            <div>
                @if ($errors->any())
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    
                    <div class="errors">
                        <ul>
                            @foreach ($errors->all() as $error)

                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif

                </div>
            </div>
            <section class="panel">
                <header class="panel-heading">
                    <div class="panel-actions">
                    </div>
                    <h2 class="panel-title">Crear Tipo de Viáticos</h2>
                </header>
                <div class="panel-body">
                    <form class="form-horizontal form-bordered" method="POST" action="{{ route('tipo.viaticos.guardar') }}">
                        @csrf                        
                        <div class="form-group">
                            <div class="col-md-2"></div>
                            <div class="col-md-8">
                                <label class="control-label col-md-12" style="text-align: left;">Tipo de Viáticos</label>
                            
                                <div class="col-md-12">
                                    <input id="tipo_viatico" type="text" value="{{ old('tipo_viatico') }}" name="tipo_viatico" required autocomplete="tipo_viatico" autofocus placeholder="Tipo de Viático" class="form-control field-input {{ $errors->has('tipo_viatico') ? 'field-error' : '' }}">
                                    @if ($errors->has('tipo_viatico'))<span class="error-message">{{ $errors->first('tipo_viatico') }}</span>
                                    @endif
                                </div>
                           
                            </div>
                            
                        </div>

                        <div class="form-group">
                            <div class="col-md-2"></div>
                            <div class="col-md-8">
                                <label class="col-md-12 control-label" style="text-align: left;">Código</label>
                                <div class="col-md-12">
                                    <input id="codigo" type="text" maxlength="1"value="{{ old('codigo') }}" name="codigo" required autocomplete="codigo" autofocus placeholder="Código" class="form-control field-input {{ $errors->has('codigo') ? 'field-error' : '' }}">
                                    @if ($errors->has('codigo'))<span class="error-message">{{ $errors->first('codigo') }}</span>
                                    @endif
                                </div>
                            </div>
                            
                        </div> 

                         <div class="form-group">
                            <div class="col-md-2"></div>
                            <div class="col-md-8">
                                <label class="col-md-12 control-label " style="text-align: left;">Valor</label>
                            <div class="col-md-12">
                                <input type="number"  min="1" name="valor" id="valor"  value="{{ old('valor') }}"  required autocomplete="valor" autofocus placeholder="Valor" class="form-control field-input {{ $errors->has('valor') ? 'field-error' : '' }}">
                                @if ($errors->has('valor'))<span class="error-message">{{ $errors->first('valor') }}</span>
                                @endif
                            </div>
                            </div>
                            
                        </div>
                        
                        <div class="col-lg-12 text-center" >    
                         <button type="submit" class="btn btn-primary">Guardar</button>
                         <a href="{{route('home')}}" class="btn btn-default">Cancelar</a>
                     </div>                              
                 </form>
             </div>
         </section>
         
     </div>
 </div>
 <!-- end: page -->
</section>

@endsection