@extends('layouts.template')
@section('content')
<section role="main" class="content-body">
  <header class="page-header">
    <h2>Generar Informes de Cierre</h2>
    <div class="right-wrapper pull-right">
            <ol class="breadcrumbs">
                <li style="margin-left: -25%"><a href="{{url('/')}}"><button class="btn btn-default"><span class="fa fa-chevron-left"> </span> Regresar </button></a></li>
            </ol>
        </div>
  </header>
  <!-- start: page -->
  <div class="row">
    <div class="col-lg-12">

      @if ($errors->any())
      <div class="alert alert-danger">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

        <div class="errors">
          <ul>
            @foreach ($errors->all() as $error) 
            <li>{{ $error }}</li>
            @endforeach
          </ul>
        </div>
        @endif

      </div>
    </div>

    <section class="panel">
      @include('flash::message') 
      <header class="panel-heading">
        <h2 class="panel-title">Generar Informes de Cierre</h2>
      </header>
      <div class="row">
        <div class="col-md-12">
         <div class="panel-body">

           <form class="form-horizontal" method="POST" action="{{ route('viaticos.informes.listar') }}" >
            {{ csrf_field() }} 


            <!-- fecha inicio-->
            <div class="form-group">
              <div class="col-md-1"></div>
              <label class="col-md-2 control-label text-right" for="w4-first-name">Fecha Inicio: </label>
              <div class="col-md-2">
                <div class="input-group">
                  <input type="date"  class="form-control field-input" name="fecha_inicio" id="fecha_inicio"   style="background: white" placeholder="dd/mm/yyyy" required> 
                </div>
              </div>
              <!-- fecha inicio-->
              <label class="col-md-2 control-label text-right" for="w4-first-name">Fecha Final: </label>
              <div class="col-md-2">
                <div class="input-group">
                  <input type="date"  class="form-control field-input" name="fecha_final" id="fecha_final"  style="background: white" placeholder="dd/mm/yyyy" required> 
                </div>
              </div>
            </div>
            <!--Boton de Generar Reporte-->
            <div class="form-group" align="center">
             <button type="submit" class="btn btn-success">
              Generar
            </button>
          </div>

        </form> 

      </div>
    </div>
  </div>

</section>

@if($vacio != null)
<section class="panel">
  <header class="panel-heading">
    <div class="panel-actions">
     <div class="col-lg-8"> 

     </div>
   </div>           

   <h2 class="panel-title">Listado de Informes de Cierre</h2>
 </header>

 <div class="row">
  <div class="col-md-12"> 
    <div class="panel-body">
      <table class="table table-bordered table-striped mb-none" style="overflow: scroll;" width="90%">

        <thead>
          <tr>
            <th width="5%">CETIA's</th>
            <th>Fecha Desde</th>
            <th>Fecha Hasta</th>
            <th>N° Referencia</th>
            <th>Monto</th>
            <th>Tipo</th>
            <th>Estatus</th>
            <th width="8%">Forma Pago</th>
            <th>Reiniciar</th>
            <th width="30%">Acciones</th>
          </tr>
        </thead>
        <tbody> 
          @foreach($informe as $informe)
          <tr class="gradeX" >

           <td>{{$informe->centro}}</td>
           <td>{{date('d-m-Y', strtotime($informe->fecha_inicio))}}</td>
           <td>{{date('d-m-Y', strtotime($informe->fecha_fin)) }}</td>
           <td>{{$informe->referencia}}</td>
           <td>${{$informe->monto}}.00</td>
           <td>{{$informe->tipo }}</td>
           @if($informe->estado == 1) 
           <td>
            <form action="{{route('nota.cerrado',$informe->id)}}" method="post">
              {{ csrf_field() }}
             @if($vacio != null)
             <input id="fecha_inicio" type="hidden" class="form-control" value="{{$fecha_inicio}}" name="fecha_inicio">
             <input id="fecha_final" type="hidden" class="form-control" value="{{$fecha_final}}" name="fecha_final"> 
             @endif
             <button class="btn btn-warning" title="Abierto"><span class="fa fa-circle-o"></span> </button>
           </form>
         </td>
         @elseif($informe->estado == 2)
         <td>
          <form action="{{route('nota.abierto',$informe->id)}}" method="post">
            {{ csrf_field() }}
           @if($vacio != null)
           <input id="fecha_inicio" type="hidden" class="form-control" value="{{$fecha_inicio}}" name="fecha_inicio">
           <input id="fecha_final" type="hidden" class="form-control" value="{{$fecha_final}}" name="fecha_final"> 
           @endif
           <button class="btn btn-danger" title="Abierto"><span class="fa fa-circle"></span> </button>
         </form>
       </td>
       @elseif($informe->estado == 3)
         <td>
          <form action="" method="get">
           @if($vacio != null)
           <input id="fecha_inicio" type="hidden" class="form-control" value="{{$fecha_inicio}}" name="fecha_inicio">
           <input id="fecha_final" type="hidden" class="form-control" value="{{$fecha_final}}" name="fecha_final"> 
           @endif
           <button class="btn btn-success" title="Abierto" disabled><span class="fa fa-dollar"></span> </button>
         </form>
       </td>
       @endif
       <td>{{$informe->pago}}</td>
       <td>
         @if($informe->estado == 1)
     <form action="{{route('reiniciar.programacion',$informe->id)}}" method="POST" class="col-md-2">
       @if($vacio != null)
       {{ csrf_field() }}
       <input id="fecha_inicio" type="hidden" class="form-control" value="{{$fecha_inicio}}" name="fecha_inicio">
       <input id="fecha_final" type="hidden" class="form-control" value="{{$fecha_final}}" name="fecha_final"> 
       @endif
       <button class="btn btn-default" title="Reiniciar"><span class="fa fa-undo"></span> </button>
     </form>       
      @elseif($informe->estado == 2 || $informe->estado == 3)
      <form action="" method="get" class="col-md-2">
       <button class="btn btn-default" title="Reiniciar" disabled><span class="fa fa-undo"></span> </button>
     </form>
     @endif
       </td>
       <td>
        <form action="{{route('imprimir.pdf',$informe->id)}}" method="POST" class="col-md-2" target="_blank">
         @if($vacio != null)
         {{ csrf_field() }}
         <input id="fecha_inicio" type="hidden" class="form-control" value="{{$fecha_inicio}}" name="fecha_inicio">
         <input id="fecha_final" type="hidden" class="form-control" value="{{$fecha_final}}" name="fecha_final"> 
         @endif
         <button class="btn btn-info" title="Informe"><span class="fa fa-file-text-o"></span> </button>
       </form>
       <!-- NOTA DE VIATICO -->
       <form action="{{route('nota.viaticos.imprimir',$informe->id)}}" method="POST" class="col-md-2" target="_blank">
         @if($vacio != null)
         {{ csrf_field() }}
         <input id="fecha_inicio" type="hidden" class="form-control" value="{{$fecha_inicio}}" name="fecha_inicio">
         <input id="fecha_final" type="hidden" class="form-control" value="{{$fecha_final}}" name="fecha_final"> 
         @endif
         <button class="btn btn-info" title="Nota"><span class="fa fa-file-text"></span> </button>
       </form>
       <form action="{{route('imprimir.ccos',$informe->id)}}" method="POST" class="col-md-2" target="_blank">
         @if($vacio != null)
         {{ csrf_field() }}
         <input id="fecha_inicio" type="hidden" class="form-control" value="{{$fecha_inicio}}" name="fecha_inicio">
         <input id="fecha_final" type="hidden" class="form-control" value="{{$fecha_final}}" name="fecha_final"> 
         @endif
         <button class="btn btn-info" title="CCO's"><span class="fa fa-file-archive-o"></span> </button>
       </form>
       @if($informe->estado == 1) 
       <div class="col-md-2">
         <a href="#modalForm" class="modal-with-form btn btn-success" onclick="cambiarRef('{{$informe->referencia}}',{{$informe->id}});" title="Cambiar Referencia"><span class="fa fa-key"></span></a>
       </div>      
      @elseif($informe->estado == 2 || $informe->estado==3)
      <form action="" method="" class="col-md-2">
       <button class="btn btn-success" title="Cambiar Referencia" disabled><span class="fa fa-key"></span> </button>
     </form>
     @endif
     <form action="{{route('imprimir.programacion', $informe->id)}}" method="POST" class="col-md-2" target="_blank">
       @if($vacio != null)
       {{ csrf_field() }}
       <input id="fecha_inicio" type="hidden" class="form-control" value="{{$fecha_inicio}}" name="fecha_inicio">
       <input id="fecha_final" type="hidden" class="form-control" value="{{$fecha_final}}" name="fecha_final"> 
       @endif
       <button class="btn btn-info" title="Programacion"><span class="fa fa-file-code-o"></span> </button>
     </form>
     
   </td>

 </tr>
 @endforeach
</tbody>
<tfoot>
 <td colspan="4" align="right">Total</td>
 <td>${{$total->total}}.00</td>
 <td colspan="5"></td>
</tfoot>
</table>
</div>
</div>
</div>
<div id="modalForm" class="modal-block modal-block-primary mfp-hide">
  <section class="panel">
    <header class="panel-heading">
      <h2 class="panel-title">Cambiar Referencia</h2>
    </header>
    <form id="form-ref" class="form-horizontal mb-lg" action="" method="POST">
    
        {{ csrf_field() }}
         <input id="fecha_inicio" type="hidden" class="form-control" value="{{$fecha_inicio}}" name="fecha_inicio">
       <input id="fecha_final" type="hidden" class="form-control" value="{{$fecha_final}}" name="fecha_final"> 
        <div class="panel-body">
        <div class="form-group mt-lg">
          <label class="col-sm-3 control-label">Referencia</label>
          <div class="col-sm-9">
            <input type="text" id="referencia" name="referencia" class="form-control" value="{{$informe->referencia}}"/>
          </div>
        </div>      
    </div>
    <footer class="panel-footer">
      <div class="row">
        <div class="col-md-12 text-right">
          <button class="btn btn-primary" type="submit">Aceptar</button>
          <button class="btn btn-default modal-dismiss">Cancel</button>
        </div>
      </div>
    </footer>
  </form>
  </section>
</div>
</section>

</div>
</div>
@endif
<!-- end: page -->
</section>

@endsection

@section('js')
<script type="text/javascript">
  function cambiarRef(referencia,id){
      $("#referencia").val(referencia);
      $("#form-ref").attr("action","/viaticos/cambiar/"+id+"/referencia");
    }
</script>
@endsection