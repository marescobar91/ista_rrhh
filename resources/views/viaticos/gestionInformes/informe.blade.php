<!DOCTYPE html>

<head>
  <title>ISTA - RRHH</title>
  <link href="css/style.css" rel="stylesheet">
  <style>
    .celda{
            border-bottom: lightgrey 0.1em solid;
        }
        .celdaa{
            border-bottom: lightgrey 0.1em solid;
        }
    .encabezado{
            border-bottom: grey 0.1em solid;
            background-color: grey;
            color: white;
        }
    .footer{
     position: fixed; left: 0px; bottom: -120px; right: 0px; height: 150px; 
   }
   .font{
    text-align: center;
    font-size: 11.3px;
    font-family: sans-serif;
  }
</style>
</head>
<body>
  <div id="wrapper">
    <!-- Page Content -->
    <div id="page-content-wrapper">
      <div class="container-fluid">
        <div class="container" style="margin-left: 13px">
          <div>
            <div class="table-title" style="background-color: #fff; margin-bottom: 1%">
              <div class="row">
                 <table>
                <tr>
                  <td>
                    <div style="width: 170px; margin-top: -25px">
                      <div class="row" align="center">
                        <img src="{{public_path().'/img/Logo-Institucional.svg'}}" width="50%" align="left" style="padding-top: -20px">
                      </div> 
                    </div>
                  </td>
                  <td >
                    <div style="width: 550px;margin-left: -150px;margin-top: 20px">
                      <h5 style="text-transform: uppercase; margin-bottom:-2%;margin-top: -20px" align="center">Instituto Salvadoreño de Transformación Agraria</h5>
                      <h5 style="text-transform: uppercase; margin-bottom:-2%;" align="center">Gerencia de Recursos Humanos</h5>
                      <h5 style="text-transform: uppercase; margin-bottom:-2%;padding-left: 70px" align="center">Reporte de Personal con Viáticos @if($tipoReporte=='FIPL-26 R_1') anticipados pagados @endif @if($tipoReporte=='FIPL-87 R_1') realizados a pagar @endif</h5>
                      <h5 style="text-transform: uppercase; margin-bottom:-2%;padding-left: 80px" align="center">periodo del {{$fecha_inicio}} al {{$fecha_final}}</h5>
                      <h5 style="text-transform: uppercase; margin-bottom:-2%; padding-left: 100px" align="center">{{$centro->centro}}</h5>
                    </div>
                  </td>
                  <td>
                    <div style="width: 120px;" align="center">
                        <h5 style="margin-bottom: -2%" >{{$tipoReporte}}</h5>
                        <h5 style="margin-bottom: -2%" >Fecha:{{$dia}}</h5> 
                        <h5 >Hora: {{$hora}}</h5>
                    </div>
                  </td>
                </tr>
              </table>
              </div>
            </div>
            <div class="row">
              <table class="table table-striped table-hover col-md-5" id="reporte" style="background: #fff;" width="97%"  >
                <thead class="font">
                  <tr class="encabezado">
                    <th>CODIGO</th>
                    <th>NOMBRE DEL EMPLEADO</th>
                    <th>PERIODO</th>
                    <th>VALOR</th>
                    <th></th>
                  </tr>
                </thead>
                @foreach($informe as $informe)
                <tbody class="font">
                  <tr>
             
                    <td colspan="2" width="40%">{{$informe->empleado}}</td>
                    @if($informe->fecha != null && $informe->empleado !=null)
                    <td >del  {{date('d-m-Y', strtotime($informe->fecha))}} al {{date('d-m-Y', strtotime($informe->fecha))}}</td>
                    <td  width="10%">${{$informe->total}}.00</td>
                    @elseif($informe->empleado != null)
                    <td ><strong>Subtotal</strong>  </td>
                    @else
                    <td><strong>Total</strong> </td>
                    @endif

                    @if($informe->fecha == null && $informe->empleado != null)
                     <td  width="10%"><strong>${{$informe->total}}.00</strong> </td>
                    <td  width="10%">F._________________</td>
                    @endif
                    @if($informe->fecha == null && $informe->empleado == null)
                     <td  width="10%" ><strong>${{$informe->total}}.00</strong> </td>
                    @endif
                  </tr>
                </tbody>
              @endforeach
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
<footer style="margin-left: 20px; padding-top: 5%" class="font">
  <table>
    <tr>
      <td style="text-align: left; width: 60%">Sistema Informatico para el manejo de Marcaciones y Viaticos de la Gerencia de Recursos Humanos del ISTA</td>
      <td></td>
      <td></td>
    </tr>
  </table>
</footer>
</body>
</html>