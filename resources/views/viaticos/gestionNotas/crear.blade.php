@extends('layouts.template')
@section('content')
<section role="main" class="content-body">
    <header class="page-header">
        <h2>Gestión de Nota de Viáticos</h2>
        <div class="right-wrapper pull-right">
            <ol class="breadcrumbs">
                <li style="margin-left: -25%"><a href="{{url('/')}}"><button class="btn btn-default"><span class="fa fa-chevron-left"> </span> Regresar </button></a></li>
            </ol>
        </div>
    </header>

    <!-- start: page -->
    <div class="row">
        <div class="col-lg-12">
            <div>
                @if ($errors->any())
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    
                    <div class="errors">
                        <ul>
                            @foreach ($errors->all() as $error)

                            <li>{{ $error }}</li>
                            @endforeach 
                        </ul>
                    </div>
                    @endif

                </div>
            </div>
             @include('flash::message')
            <section class="panel">
                <header class="panel-heading">
                    <div class="panel-actions">
                    </div>
                    <h2 class="panel-title">Generación de nota de viáticos por pagar</h2>
                </header>
                <div class="panel-body">
                    <form class="form-horizontal form-bordered" method="POST" action="{{route('nota.viaticos.guardar')}}" target="_blank">
                        @csrf                        
                        <div class="col-md-2"></div>
                            <div class="col-md-8">
                               <label class="col-md-12 control-label text-right" style="text-align: left;">CETIA:</label>
                               <div class="col-md-12">
                                <select data-plugin-selectTwo id="centro" class="form-control" name="centro" required>
                                    <option value="" selected>Seleccione uno</option>
                                    @foreach($centro as $centro)
                                    @if($centro->id_centro == 1)
                                    <option value="{{$centro->id_centro}}">{{$centro->centro}} --  CETIA II</option>
                                    @elseif($centro->id_centro != 3)
                                    <option value="{{$centro->id_centro}}">{{$centro->centro}}</option>
                                    @endif
                                    @endforeach
                                </select> 
                                @if ($errors->has('centro'))<span class="error-message">{{ $errors->first('centro') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-12"></div>
                        <div class="col-md-2"></div>
                            <div class="col-md-4">
                               <label class="col-md-12 control-label text-right" style="text-align: left;">Tipo:</label>
                               <div class="col-md-12">
                                <select data-plugin-selectTwo id="tipo" class="form-control" name="tipo" required>
                                    <option value="" selected>Seleccione uno</option>
                                    @foreach($tipo as $tipo)
                                    <option value="{{$tipo->id_tipo_programacion}}"  @if(old('tipo') == $tipo->id_tipo_programacion) selected="selected" @endif>{{$tipo->tipo_programacion}}</option>

                                    @endforeach
                                </select> 
                                @if ($errors->has('tipo'))<span class="error-message">{{ $errors->first('tipo') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-4">
                               <label class="col-md-12 control-label text-right" style="text-align: left;">Forma Pago:</label>
                               <div class="col-md-12">
                                <select data-plugin-selectTwo id="pago" class="form-control" name="pago" required> 
                                    @foreach($pago as $pago)
                                    <option value="{{$pago->id_forma_pago}}"  @if (old('pago') == $pago->id_forma_pago) selected="selected" @endif>{{$pago->forma_pago}}</option>

                                    @endforeach
                                </select> 
                                @if ($errors->has('pago'))<span class="error-message">{{ $errors->first('pago') }}</span>
                                @endif
                            </div>
                        </div>
                            

                        <div class="col-md-12"></div>
                        <div class="col-md-2"></div>
                        <div class="col-md-4">
                            <label class="col-md-12 control-label text-left" for="w4-first-name" style="text-align: left">Fecha Inicio: </label>
                                <div class="col-md-12">
                                  
                                        <input type="date"  name="fecha_inicio" id="fecha_inicio"  style="background: white"  class="form-control field-input {{ $errors->has('fecha_inicio') ? 'field-error' : '' }}" value="{{old('fecha_inicio')}}"> 
                                  
                                    @if ($errors->has('fecha_inicio'))<span class="error-message">{{ $errors->first('fecha_inicio') }}</span>
                                    @endif
                                </div>

                        </div>
                        <div class="col-md-4">
                            <label class="col-md-12 control-label text-right" for="w4-first-name" style="text-align: left">Fecha Final: </label>
                                <div class="col-md-12">
                                    
                                        <input type="date"  name="fecha_final" id="fecha_final"  style="background: white" placeholder="dd/mm/yyyy" class="form-control field-input {{ $errors->has('fecha_final') ? 'field-error' : '' }}" value="{{old('fecha_final')}}">
                                        @if ($errors->has('fecha_final'))<span class="error-message">{{ $errors->first('fecha_final') }}</span>
                                    @endif 
                                </div>
                            </div>
                            <div class="col-md-12"></div>
                            <div class="col-md-2"></div>
                            <div class="col-md-8">
                                <label class="col-md-12 control-label text-right" for="w4-first-name" style="text-align: left">Referencia: </label>
                            </div>
                            <div class="col-md-12"></div>
                            <div class="col-md-2"></div>
                            <div class="col-md-8">
                            <div class="col-md-2">
                                <input type="text" name="abreviatura" class="form-control field-input" id="abreviatura" value="{{old('abreviatura')}}">
                            </div>
                            <div class="col-md-2">
                                <input type="text" name="num1" class="form-control field-input" id="num1" value="00" required>
                            </div>
                            <div class="col-md-2">
                                <input type="number" name="num2" onchange="procesar();" class="form-control field-input" id="num2" required maxlength="4" minlength="4" min="0">
                            </div>
                            <div class="col-md-2">
                                <input type="text" name="anio" class="form-control field-input" id="anio" value="{{$anio2}}"  required> 
                            </div>
                            <div class="col-md-4">
                                <input type="text" name="referencia" class="form-control field-input" readonly id="referencia" required value="{{old('referencia')}}">
                            </div>
                            </div>
                            <div class="form-group"></div>

                        <div class="col-lg-12 text-center" >    
                         <button type="submit" class="btn btn-primary">Guardar</button>
                         <a href="{{route('home')}}" class="btn btn-default">Cancelar</a>
                     </div>                              
                 </form>
             </div>
         </section>
         
     </div>
 </div>
 <!-- end: page -->
</section>

@endsection

@section('js')
<script type="text/javascript">
    $(document).ready(function() {
        $('#centro').on('change',function(){
                var centro = $('#centro').val();
                if($.trim('centro') != ''){
                    $.get("{{route('nota.viaticos.getCentro')}}", {centro:  centro}, function (data){
                        $("#abreviatura").val(data.abreviatura.abv);
                        $("#fecha_inicio").val(data.fechas.fechaMinima);
                        $("#fecha_final").val(data.fechas.fechaMaxima);
                        
                });
            }   
        });
    });

</script>

<script type="text/javascript">
    function procesar() {

        abreviatura=document.getElementById('abreviatura').value;
        num1=document.getElementById('num1').value;
        num2=document.getElementById('num2').value;
        anio=document.getElementById('anio').value;
        
        referencia=abreviatura+'-'+num1+'-'+num2+'-'+anio;

        document.getElementById('referencia').value=referencia;
        if (referencia == null) {
             return false;
        }
 
        document.forms.ejemplo.submit();


    }
</script> 
@endsection