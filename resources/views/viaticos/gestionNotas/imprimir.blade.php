<!DOCTYPE html>

<head>
  <title>ISTA - RRHH</title>
  <link href="css/style.css" rel="stylesheet">
  <style>
    .celda{ 
            border-bottom: lightgrey 0.1em solid;
        }
        .encabezado{
            border-bottom: grey 0.1em solid;
            background-color: grey;
            color: white;
        }
    .footer{
     position: fixed; left: 0px; bottom: -120px; right: 0px; height: 150px; 
   }
   .font{
    text-transform: uppercase;
    font-size: 11.3px;
    font-family: sans-serif;
  }
</style>
</head>
<body>
  <div id="wrapper">
    <!-- Page Content -->
    <div id="page-content-wrapper">
      <div class="container-fluid">
        <div class="container" style="margin-left: 13px">
          <div>
            <div class="table-title" style="background-color: #fff; margin-bottom: 1%; margin-top: 3%">
              <table>
                <tr>
                  <td>
                    <div style="width: 200px; margin-top: -25px">
                      <div class="row" align="center">
                        <img src="{{public_path().'/img/Logo-Institucional.svg'}}" width="70%" align="left" style="padding-top: -20px">
                      </div> 
                    </div>
                  </td>
                  <td >
                    <div style="width: 550px;margin-left: -180px">
                      <h5 style="text-transform: uppercase; margin-bottom:-2%;margin-top: -20px" align="center">Instituto Salvadoreño de Transformación Agraria</h5>
                      <h5 style="text-transform: uppercase; margin-bottom:-2%;" align="center">Gerencia de Recursos Humanos</h5>
                      <h5 style="text-transform: uppercase; margin-bottom:-2.5%;" align="center">Resumen de Viáticos por Pagar</h5>
                    </div>
                  </td>
                <!--   <td>
                    <div style="width: 120px;" align="center">
                        <h5 style="margin-bottom: -2%" >FILP - 24</h5>
                        <h5 style="margin-bottom: -2%" >Fecha:</h5> 
                        <h5 >Hora: </h5>
                    </div>
                  </td> -->
                </tr>
              </table>
            </div>
            <br/>
            <br/>
            <table width="97%">
              <tr>
                <td></td>
                <td align="right">San Salvador, {{date('j', strtotime($dia))}} de {{$mes}} del {{$anio}}</td> 
              </tr> 
              <tr>
                <td></td>
                <td align="right">{{$referencia}}</td> 
              </tr>
              <tr>
                <td>Licendiado(a)</td>
                <td></td>
              </tr> 
              <tr>
                <td>{{$empleadoJefeFinanciero->nombre}} {{$empleadoJefeFinanciero->apellido}}</td>
                <td></td>
              </tr>
              <tr>
                <td>Jefe Unidad Financiera Institucional</td>
                <td></td>
              </tr>
              <tr>
                <td>Presente</td>
                <td></td>
              </tr>
            </table>
            <p style="padding-top: 2%">Le remito, el Reporte de Viáticos a Pagar correspondiente al periodo del {{date('j', strtotime($fechaIni))}} de {{ $mesIni }} del {{date('Y', strtotime($fechaIni))}}   al  {{date('j', strtotime($fechaFin))}} de {{ $mesFin }} del {{date('Y', strtotime($fechaFin))}}, segun programaciones de trabajo autorizadas.</p>
            <p style="padding-bottom: 1%;padding-top: 1%">Asi tambien se adjuntan las Constancias de Comision Oficial (C.C.O)*, debidamente firmadas.</p>
            <p style="padding-top: 1%;padding-bottom: 2%">Los montos a cancelar son:</p>
              
            <div class="row">
              <table class="table table-striped table-hover col-md-5" id="reporte" style="background: #fff;" width="97%">
                <thead class="font">
                  <tr class="encabezado">
                    <th>Descripción </th>
                    <th>{{$pago}}</th>
                    <th>Total</th>
                  </tr>
                </thead>
                
                <tbody class="font">
                  @foreach($consulta as $key => $consulta)
                  <tr >
                    <td class="celda">{{$consulta['centro']}}</td>
                    <td class="celda">${{$consulta['efectivo']}}.00</td>
                    <td class="celda">${{$consulta['efectivo']}}.00</td>
                  </tr>
                   @endforeach sumaViatico
                  <tr>
                    <td colspan="2" class="celda" align="center"><strong>Total</strong></td>
                    <td class="celda">${{$sumaViatico->Efectivo}}.00 </td>
                  </tr>

                 
                </tbody>
            
            </table>
            <p>Atentamente,</p>
            <br/>
            <br/>
            <br/>
            <div align="center">
              <p>{{$empleadoJefe->nombre}} {{$empleadoJefe->apellido}}</p>
              <p>GERENTE DE RECURSOS HUMANOS</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
<footer style="margin-left: 20px; padding-top: 5%" class="font">
  <table>
    <tr>
      <td style="text-align: left; width: 60%">Sistema Informatico para el manejo de Marcaciones y Viaticos de la Gerencia de Recursos Humanos del ISTA</td>
      <td></td>
      <td></td>
    </tr>
  </table>
</footer>
</body>
</html>