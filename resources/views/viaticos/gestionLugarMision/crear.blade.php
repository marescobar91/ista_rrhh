@extends('layouts.template')
@section('content')
<section role="main" class="content-body">
    <header class="page-header">
        <h2>Gestión de Lugar de Misiones</h2>
        <div class="right-wrapper pull-right">
            <ol class="breadcrumbs">
                <li style="margin-left: -25%"><a href="{{route('lugar.misiones.listar')}}"><button class="btn btn-default"><span class="fa fa-chevron-left"> </span> Regresar </button></a></li>
            </ol>
        </div>
    </header>

    <!-- start: page -->
    <div class="row">
        <div class="col-lg-12">
            <div>
                @if ($errors->any())
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    
                    <div class="errors">
                        <ul>
                            @foreach ($errors->all() as $error)

                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif

                </div>
            </div>
            <section class="panel">
                <header class="panel-heading">
                    <div class="panel-actions">
                    </div>
                    <h2 class="panel-title">Crear Lugar de Misión</h2>
                </header>
                <div class="panel-body">
                    <form class="form-horizontal form-bordered" method="POST" action="{{ route('lugar.misiones.guardar') }}">
                        @csrf                        
                        <div class="form-group">
                            <div class="col-md-2"></div>
                            <div class="col-md-8">
                                <label class="control-label col-md-12" style="text-align: left;">Departamento</label>
                            
                                <select data-plugin-selectTwo name="departamento" class="form-group col-md-12" required>
                                    <option value=""selected>Seleccione el Departamento</option>
                                    @foreach($departamento as $departamento)
                                    
                                    <option value="{{ $departamento->id_departamento }}" >{{ $departamento->departamento }}</option>
                                    
                                    @endforeach
                                </select>

                                @if ($errors->has('centro'))<span class="error-message">{{ $errors->first('centro') }}</span>
                                @endif
                           
                            </div>
                            
                        </div>

                        <div class="form-group">
                            <div class="col-md-2"></div>
                            <div class="col-md-8">
                                <label class="col-md-12 control-label" style="text-align: left;">Lugar de Misión</label>
                                <div class="col-md-12">
                                    <input id="lugar_mision" type="text" value="{{ old('lugar_mision') }}" name="lugar_mision" required autocomplete="lugar_mision" autofocus placeholder="Lugar de Misión" class="form-control field-input {{ $errors->has('lugar_mision') ? 'field-error' : '' }}">
                                    @if ($errors->has('lugar_mision'))<span class="error-message">{{ $errors->first('lugar_mision') }}</span>
                                    @endif
                                </div>
                            </div>
                            
                        </div> 

                         <div class="form-group">
                            <div class="col-md-2"></div>
                            <div class="col-md-8">
                                <label class="col-md-12 control-label " style="text-align: left;">Kilometros</label>
                            <div class="col-md-12">
                                <input type="number" step="any" min="0" name="kilometros" id="kilometros"  value="{{ old('kilometros') }}"  required autocomplete="kilometros" autofocus placeholder="Kilometros" class="form-control field-input {{ $errors->has('kilometros') ? 'field-error' : '' }}">
                                @if ($errors->has('kilometros'))<span class="error-message">{{ $errors->first('kilometros') }}</span>
                                @endif
                            </div>
                            </div>
                            
                        </div>
                        
                        <div class="col-lg-12 text-center" >    
                         <button type="submit" class="btn btn-primary">Guardar</button>
                         <a href="{{route('home')}}" class="btn btn-default">Cancelar</a>
                     </div>                              
                 </form>
             </div>
         </section>
         
     </div>
 </div>
 <!-- end: page -->
</section>

@endsection