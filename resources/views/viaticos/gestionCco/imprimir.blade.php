<!DOCTYPE html>

<head>
  <title>ISTA - RRHH</title>
  <link href="css/style.css" rel="stylesheet">
  <style>
    .celda{
            border-bottom: lightgrey 0.1em solid;
        }
        .encabezado{
            border-bottom: grey 0.1em solid;
            background-color: grey;
            color: white;
        }
    .footer{
     position: fixed; left: 0px; bottom: -120px; right: 0px; height: 150px; 
   }
   .font{
    text-transform: uppercase;
    font-size: 11.3px;
    font-family: sans-serif;
  }
</style>
</head>
<body>
  <div id="wrapper">
    <!-- Page Content -->
    <div id="page-content-wrapper">
      <div class="container-fluid">
        <div class="container" style="margin-left: 13px">
          <div>
            <div class="table-title" style="background-color: #fff; margin-bottom: 1%">
              <table>
                <tr>
                  <td>
                    <div style="width: 200px; margin-top: -25px">
                      <div class="row" align="center">
                        <img src="{{public_path().'/img/Logo-Institucional.svg'}}" width="70%" align="left" style="padding-top: -20px">
                      </div> 
                    </div>
                  </td>
                  <td >
                    <div style="width: 830px;margin-left: -180px">
                      <h5 style="text-transform: uppercase; margin-bottom:-2%;margin-top: -20px" align="center">Instituto Salvadoreño de Transformación Agraria</h5>
                      <h5 style="text-transform: uppercase; margin-bottom:-2%;" align="center">Gerencia de Recursos Humanos</h5>
                      <h5 style="text-transform: uppercase; margin-bottom:-2.5%;" align="center">Programación de Trabajo que requiere pago de Viáticos</h5>
                    </div>
                  </td>
                  <td>
                    <div style="width: 120px;" align="center">
                        <h5 style="margin-bottom: -2%" >FILP - 24</h5>
                        <h5 style="margin-bottom: -2%" >Fecha:{{$dia}}</h5> 
                        <h5 >Hora: {{$hora}}</h5>
                    </div>
                  </td>
                </tr>
              </table>
            </div>
            <div class="row">
              <table class="table table-striped table-hover col-md-5" id="reporte" style="background: #fff;" width="97%">
                <thead class="font">
                  <tr class="encabezado">
                    <th>codemp</th>
                    <th>Nombre del Empleado</th>
                    <th>Actividad</th>
                    <th>Desde</th>
                    <th>Hasta</th>
                    <th>Cod</th>
                    <th>Propiedad</th>
                    <th>departamento</th>
                    <th>tipo</th>
                    <th>valor</th>
                    <th>dias</th>
                    <th>monto</th>
                  </tr>
                </thead>
                @foreach($cco as $cco)
                <tbody class="font">
                  <tr >
                    <td class="celda">{{$cco->codigo}}</td>
                    <td class="celda">{{$cco->nombre}} {{$cco->apellido}}</td>
                    <td class="celda">{{$cco->actividad}}</td>
                    <td class="celda">{{$cco->desde}}</td>
                    <td class="celda">{{$cco->hasta}}</td>
                    <td class="celda">Cod</td>
                    <td class="celda">{{$cco->sede_oficial}}</td>
                    <td class="celda">{{$cco->departamento}}</td>
                    <td class="celda">{{$cco->codigo_viatico}}</td>
                    <td class="celda">{{$cco->valor}}</td>
                    <td class="celda">{{$cco->dias}}</td>
                    <td class="celda">${{$cco->pago_viatico}}.00</td>
                  </tr>
                </tbody>
              @endforeach
              <tfoot>
                <tr class="font">
                  <td colspan="11" class="celda" align="right"><strong>Total</strong> </td>
                  <td class="celda"><strong>${{$total->monto}}.00</strong> </td>
                </tr>
              </tfoot>
            </table>
          </div>
        </div>
        <div style="padding-top: 10%">
          <table>
            <tr>
              <td width="480px">F)_______________________________________</td>
              <td width="480px">F)_______________________________________</td>
            </tr>
            <tr>
              <td>ELABORO</td>
              <td>AUTORIZO</td>
            </tr>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
<footer style="margin-left: 20px; padding-top: 5%" class="font">
  <table>
    <tr>
      <td style="text-align: left; width: 100%">Sistema Informatico para el manejo de Marcaciones y Viaticos de la Gerencia de Recursos Humanos del ISTA</td>
      <td></td>
      <td></td>
    </tr>
  </table>
</footer>
</body>
</html>