@extends('layouts.template')
@section('content')
<section role="main" class="content-body">
    <header class="page-header">
        <h2>Gesti&oacute;n Constancia de Comision Oficial (C.C.O.)</h2>
        <div class="right-wrapper pull-right">
            <ol class="breadcrumbs">
                <li style="margin-left: -25%"><a href="{{route('programacion.listar')}}"><button class="btn btn-default"><span class="fa fa-chevron-left"> </span> Regresar </button></a></li>
            </ol>
        </div>
    </header>
    <!-- start: page -->
    <div class="row">
        <div class="col-lg-12">
            <div>
                @if ($errors->any())
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                    <div class="errors">
                        <ul>
                            @foreach ($errors->all() as $error)

                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                </div>
            </div>
            <section class="panel">
             @include('flash::message') 
             <header class="panel-heading">
               
                <h2 class="panel-title">Generación de CCO'S</h2>
            </header>
            <div class="panel-body">
                <form role="form" class="contactForm" method="POST" action="{{route('generar.cco.pdf')}}" target="_blank" id="formCco"> 
                    @csrf
                    <div class="col-md-2"></div>        
                    <div class="col-md-8">
                        <input type="hidden" id="id_departamento" value="" required>  
                        <div class="form-group">
                            <label class="control-label col-md-12">Empleado</label>
                            <div class="col-md-12">
                                <select data-plugin-selectTwo id="empleado" class="form-control populate" name="empleado" required class="form-control field-input {{ $errors->has('empleado') ? 'field-error' : '' }}">
                                    <option value="0" selected>Todos</option>
                                    @foreach($empleados as $empleado)
                                    <option value="{{$empleado->id_empleado}}">{{$empleado->codigo_empleado}} || {{$empleado->nombre}} {{$empleado->apellido}}</option>
                                    @endforeach
                                </select>
                                @if ($errors->has('empleado'))<span class="error-message">{{ $errors->first('empleado') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group"></div>
                        <div class="form-group">
                            <label class="col-md-2 control-label text-left" for="w4-first-name">Desde: </label>
                            <div class="col-md-4">
                                <input type="date"  class="form-control field-input" name="fecha_inicio" id="fecha_inicio"  style="background: white" placeholder="dd/mm/yyyy" required> 
                            </div>
                            <div class="col-md-1"></div>
                            <!-- fecha inicio-->
                            <label class="col-md-1 control-label text-right" for="w4-first-name">Hasta: </label>
                            <div class="col-md-4">
                                <input type="date"  class="form-control field-input" name="fecha_final" id="fecha_final"  style="background: white" placeholder="dd/mm/yyyy" required> 
                            </div>
                        </div>
                       
                            
                            <div class="col-md-4">
                                 @foreach($arrayCentro as $key=>$value)
                                <input type="hidden"  class="form-control field-input" name="centros[]" id="centros[]"  style="background: white" value="{{$value}}"> 
                                @endforeach
                            </div>
                            <div class="col-md-1"></div>
                           
                      
                    </div>
                    <div class="form-group"></div>
                    <div class="col-lg-12 text-center" >    
                     <button type="submit" class="btn btn-primary" value="1" name="verCco" id="verCco">Aceptar</button>
                     <a href="{{route('home')}}" class="btn btn-default">Cancelar</a>
                 </div> 
                 <div class="form-group"></div>
                 <div class="col-md-12 text-center">
                    <button type="submit" class="btn btn-danger" value="2" name="cerrarCco" id="cerrar" disabled>Cerrar CCO's</button> 
                </div>
                <div class="form-group"></div>
                <div class="col-md-12 text-center">
                 <button type="submit" class="btn btn-info" value="3" name="imprimirCco" id="imprimir" disabled>Imprimir Programación</button> 
             </div>                      
         </form>             

     </div>
 </section>

</div>
</div>
<!-- end: page -->
</section>

@endsection

@section('js')

<script type="text/javascript">
    $( "#verCco" ).on("click",function(){
        $("#cerrar").removeAttr("disabled"); 
    });

    $("#cerrar").on("click",function(e){
        e.preventDefault();
        $("#verCco").attr("disabled", true);
        $("#imprimir").removeAttr("disabled");
        e.preventDefault();
        var fecha_inicio = $('#fecha_inicio').val();
        var fecha_final = $('#fecha_final').val();
        var empleado = $('#empleado').val();
        var _token = $("input[name='_token']").val();
        
        swal({
            title: '¿Esta seguro de cerrarCco?',
            text: 'El registro sera cerrado',
            icon: 'warning',
            buttons: ["Cancelar","Aceptar"],
        }).then(function(value){
            if(value){
                $.ajax({
                    url:"{{route('cerrar.cco.pdf')}}",
                    type:"POST",
                    data:{
                        fecha_inicio:fecha_inicio,
                        empleado:empleado,
                        fecha_final:fecha_final,
                        _token:_token,
                    },
                    success:function(data){

                        swal({
                            title: "El CCO fue",
                            text: data.mensaje,
                            icon: "success",
                            timer: 2000,
                        }).then(function(value){
                                window.location.href = url;
                                
                        });
                        $("#cerrar").attr("disabled", true);

                    }
                })
            }
        })
        
    });
</script>

@endsection