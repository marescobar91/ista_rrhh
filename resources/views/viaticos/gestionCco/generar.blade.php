<!DOCTYPE html>

<head>
  <title>ISTA - RRHH</title>
  <link href="css/style.css" rel="stylesheet">
  <style>
    
    .footer{
     position: fixed; left: 0px; bottom: -120px; right: 0px; height: 150px; 
   }
   .font{
    text-transform: uppercase;
    font-size: 11.3px;
    font-family: sans-serif;
  }
</style>
</head>
<body>
  <div id="wrapper">
    <!-- Page Content -->
    <div id="page-content-wrapper">
      <div class="container-fluid">
        <div class="container" style="margin-left: 13px">
          <div>
            <div class="table-title" style="background-color: #fff; margin-bottom: 1%">
              <table>
                <tr>
                  <td>
                    <div style="width: 185px; margin-top: -25px">
                      <div class="row" align="center">
                        <img src="{{public_path().'/img/Logo-Institucional.svg'}}" width="70%" align="left" style="padding-top: -20px">
                      </div> 
                    </div>
                  </td>
                  <td >
                    <div style="width: 550px;margin-left: -180px">
                      <h5 style="text-transform: uppercase; margin-bottom:-2%;margin-top: -20px" align="center">Instituto Salvadoreño de Transformación Agraria</h5>
                      <h5 style="text-transform: uppercase; margin-bottom:-2%;" align="center">Gerencia de Recursos Humanos</h5>
                      <h5 style="text-transform: uppercase; margin-bottom:-2.5%;" align="center">Constancia de Comision Oficial (C.C.O.)</h5>
                    </div>
                  </td>
                  <td>
                    <div style="width: 120px;" align="center">
                        <h5 style="margin-bottom: -2%" >FIPL - 23</h5>
                        <h5 style="margin-bottom: -2%" >Fecha:{{$dia}}</h5> 
                        <h5 >Hora: {{$hora}}</h5>
                    </div>
                  </td>
                </tr>
              </table>
            </div>
            <div class="row">
              <table class="table table-striped table-hover col-md-5" id="reporte" style="background: #fff;" width="97%">
                @foreach($cco as $cco)
                <tbody class="font">
                 <tr>
                   <td colspan="4"> <strong>A) DATOS GENERALES</strong></td>
                 </tr>
                 <tr>
                   <td colspan="4">HACE CONSTAR QUE</td>
                 </tr>
                 <tr>
                  <td>SE HA COMISIONADO A:</td>
                  <td>{{$cco->nombre}} {{$cco->apellido}}</td>
                  <td>CODIGO:</td>
                  <td>{{$cco->codigo}}</td>
                </tr>
                <tr>
                  <td>PARA EFECTUAR TRABAJO DE:</td>
                  <td>{{$cco->actividad}}</td>
                  <td></td>
                  <td></td>
                </tr>
                <tr>
                  <td>EN:</td>
                  <td>{{$cco->lugar}}</td>
                  <td>DEL DEPARTAMENTO DE:</td>
                  <td>{{$cco->departamento}}</td>
                </tr>
                <tr>
                  <td>SIENDO SU FECHA DE SALIDA:</td>
                  <td>{{$cco->desde}}</td>
                  <td>DE LA SEDE OFICIAL:</td>
                  <td>{{$cco->sede_oficial}}</td>
                </tr>
                <tr>
                  <td>Y REGRESANDO EL:</td>
                  <td>{{$cco->hasta}}</td>
                  <td></td>
                  <td></td>
                </tr>
                <tr> 
                  <td colspan="4" style="padding-top: 20px"> <strong>B) ESPACIO RESERVADO PARA SECCION DE VIATICOS</strong> </td>
                </tr>
                <tr>
                  <td>SISTEMA DE PAGO</td>
                  <td>{{$cco->tipo_plaza}}</td>
                  <td></td>
                  <td></td>
                </tr>
                <tr>
                  <td>NOMBRAMIENTO:</td>
                  <td>{{$cco->cargo}}</td>
                  <td>SUELDO DIARIO:</td>
                  <td>${{$cco->diario}}</td>
                </tr>
                <tr>
                  <td>CARGO FUNCIONAL:</td>
                  <td>{{$cco->cargo}}</td>
                  <td>SUELDO MENSUAL:</td>
                  <td>${{$cco->salario}}</td>
                </tr>
               <!--  <tr>
                  <td>GERENCIA:</td>
                  <td></td>
                </tr>
                <tr>
                  <td>OFICINA:</td>
                  <td></td>
                </tr> -->
                <tr>
                  <td>OFICINA:</td>
                  <td></td>
                  <td>TIPO DE VIATICO:</td>
                  <td>{{$cco->tipo_viatico}}</td>
                </tr>
                <tr>
                  <td></td>
                  <td></td>
                  <td>NUMERO DE DIAS:</td>
                  <td>{{$cco->dias}}</td>
                </tr>
               <!--  <tr>
                  <td>SECCION:</td>
                  <td></td>
                  <td></td>
                  <td></td>
                </tr> -->
                <tr>
                  <td>UNIDAD:</td>
                  <td>{{$cco->unidad}}</td>
                  <td></td>
                  <td></td>
                </tr>
                <tr>
                  <td></td>
                  <td></td>
                  <td>VALOR DEL ALOJAMIENTO:</td>
                  <td>0</td>
                </tr>
                 <tr> 
                  <td colspan="4" style="padding-top: 30px;padding-bottom: 30px"><strong>C) PARA CONSTATAR EL CUMPLIMIENTO DE LA MISION FIRMA Y SELLA</strong> </td>
                </tr>
                <tr>
                  <td>COMISIONADO O ENCARGADO DE LA MISION</td>
                  <td>_________________________________</td>
                  <td></td>
                  <td></td>
                </tr>
                <tr>
                  <td></td>
                  <td style="padding-left: 85px">FIRMA</td>
                  <td></td>
                  <td></td>
                </tr>
                <tr>
                  <td colspan="4" style="padding-bottom: 50px"></td>
                </tr>
                <tr>
                  <td>JEFE DE OFICINA QUE AUTORIZA LA MISION OFICIAL ASIGNADA</td>
                  <td>__________________________________</td>
                  <td></td>
                  <td></td>
                </tr>
                <tr>
                  <td></td>
                  <td style="padding-left: 85px">FIRMA</td>
                  <td align="center">SELLO</td>
                  <td></td>
                </tr>
                 <tr>
                  <td colspan="4" style="padding-bottom: 50px"></td>
                </tr>
                <tr>
                  <td colspan="4" style="padding-bottom: 20px">OBSERVACIONES</td>
                </tr>
                <tr>
                  <td colspan="4" style="border-bottom: solid #000 0.75px 0.75px 0.75px 0.75px"></td>
                </tr>
                <tr>
                  <td colspan="4" style="border-bottom: solid #000 0.75px 0.75px 0.75px 0.75px; padding-bottom: 15px"></td>
                </tr>
                <tr>
                  <td colspan="4" style="border-bottom: solid #000 0.75px 0.75px 0.75px 0.75px; padding-bottom: 15px"></td>
                </tr>
                <tr>
                  <td colspan="4" style="border-bottom: solid #000 0.75px 0.75px 0.75px 0.75px;  padding-bottom: 15px"></td>
                </tr>
              </tbody>
              @endforeach
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
<footer style="margin-left: 20px; padding-top: 5%" class="font">
  <table>
    <tr>
      <td style="text-align: left; width: 60%">Sistema Informatico para el manejo de Marcaciones y Viaticos de la Gerencia de Recursos Humanos del ISTA</td>
      <td></td>
      <td></td>
    </tr>
  </table>
</footer>
</body>
</html>