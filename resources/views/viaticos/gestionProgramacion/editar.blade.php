@extends('layouts.template')
@section('content')
<section role="main" class="content-body">
    <header class="page-header">
        <h2>Gesti&oacute;n de Programación Mensual de Trabajo</h2>
        <div class="right-wrapper pull-right">
            <ol class="breadcrumbs">
                <li style="margin-left: -25%"><a href="{{route('programacion.listar')}}"><button class="btn btn-default"><span class="fa fa-chevron-left"> </span> Regresar </button></a></li>
            </ol>
        </div>
    </header>
    <!-- start: page -->
    <div class="row">
        <div class="col-lg-12">
            <div>
                @if ($errors->any())
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                    <div class="errors">
                        <ul>
                            @foreach ($errors->all() as $error)

                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                </div>
            </div>
            <section class="panel">
                <header class="panel-heading">
                    <div class="panel-actions">
                    </div>
                    <h2 class="panel-title">Editar de Programación de Viáticos</h2>
                </header>
                <div class="panel-body">
                    <form role="form" method="POST" action="{{ route('viaticos.programacion.actualizar', $programacion->id_programacion )}}" class="contactForm" >
                        @csrf
                    {{ method_field('PUT') }}
                        <div class="col-md-2"></div>        
                        <div class="col-md-8">
                             
                            <input type="hidden" id="id_oficina" name="id_oficina"value="{{$programacion->programacionEmpleado->oficina->id_oficina}}">

                              <div class="form-group">
                                <label class="col-md-12 control-label ">Centro</label>
                                <div class="col-md-12">
                                    <input id="centro" type="text" name="centro" required disabled autofocus value="{{$programacion->programacionEmpleado->oficina->centro->centro}}" class="form-control field-input {{ $errors->has('centro') ? 'field-error' : '' }}">
                                    @if ($errors->has('centro'))<span class="error-message">{{ $errors->first('centro') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-12 control-label ">Oficina</label>
                                <div class="col-md-12">
                                    <input id="oficina" type="text" name="oficina" required disabled autofocus value="{{$programacion->programacionEmpleado->oficina->oficina}}" class="form-control field-input {{ $errors->has('oficina') ? 'field-error' : '' }}">
                                    @if ($errors->has('oficina'))<span class="error-message">{{ $errors->first('oficina') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-12 control-label ">Nombre</label>
                                <div class="col-md-12">
                                    <input id="nombre" type="text" name="nombre" required disabled autofocus value="{{$programacion->programacionEmpleado->nombre}} {{$programacion->programacionEmpleado->apellido}}" class="form-control field-input {{ $errors->has('nombre') ? 'field-error' : '' }}">
                                    @if ($errors->has('nombre'))<span class="error-message">{{ $errors->first('nombre') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-12">Actividad</label>
                                <div class="col-md-12">
                                    <select data-plugin-selectTwo id="actividad" class="form-control populate" name="actividad" required class="form-control field-input {{ $errors->has('actividad') ? 'field-error' : '' }}">
                                        
                                        @foreach($actividad as $actividad)
                                        <option value="{{$actividad->id_actividad}}" @if($actividad->id_actividad==$programacion->actividad_id)
                                            selected='selected' @endif>{{$actividad->actividad}}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('actividad'))<span class="error-message">{{ $errors->first('actividad') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label text-left" for="w4-first-name">Fecha Inicio: </label>
                                <div class="col-md-4">
                                   
                                        <input type="date"  class="form-control field-input" name="fecha_inicio" id="fecha_inicio"  style="background: white" placeholder="dd/mm/yyyy" class="form-control field-input {{ $errors->has('fecha_inicio') ? 'field-error' : '' }}" value="{{$programacion->fecha_desde}}"> 
                                   
                                    @if ($errors->has('fecha_inicio'))<span class="error-message">{{ $errors->first('fecha_inicio') }}</span>
                                    @endif
                                </div>
                                
                                <!-- fecha inicio-->
                                <label class="col-md-2 control-label text-right" for="w4-first-name">Fecha Final: </label>
                                <div class="col-md-4">
                                    
                                        <input type="date"  class="form-control field-input" name="fecha_final" id="fecha_final"  style="background: white" placeholder="dd/mm/yyyy" class="form-control field-input {{ $errors->has('fecha_final') ? 'field-error' : '' }}" value="{{$programacion->fecha_hasta}}">
                                        @if ($errors->has('fecha_final'))<span class="error-message">{{ $errors->first('fecha_final') }}</span>
                                    @endif 
                                    
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-12">Lugar de Misión</label>
                                <div class="col-md-8">
                                    <select data-plugin-selectTwo id="lugarMision" class="form-control populate" name="lugarMision" required class="form-control field-input {{ $errors->has('lugarMision') ? 'field-error' : '' }}">
                                       
                                        @foreach($lugarMision as $lugar)
                                                <option value="{{$lugar->id_lugar_mision}}" @if($lugar->id_lugar_mision==$programacion->lugar_mision_id)
                                            selected='selected' @endif>{{$lugar->departamento->departamento}} || {{$lugar->lugar_mision}}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('lugarMision'))<span class="error-message">{{ $errors->first('lugarMision') }}</span>
                                    @endif
                                </div>
                                <div class="form-group" >
                                       <div class="col-md-2">
                                        <div class="radio-custom radio-primary">
                                            <input type="radio"  id="diario" name="tipo_viatico"  value="1" {{  ($programacion->tipo_viatico_id == '1' ? ' checked' : '') }}>
                                            <label for="w4-terms">Diario</label>                                     
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="radio-custom radio-primary">
                                            <input type="radio"  id="corrido" name="tipo_viatico" value="2" {{  ($programacion->tipo_viatico_id == '2' ? ' checked' : '') }}>
                                            <label for="w4-terms">Corrido</label>
                                        </div>
                                    </div>
                                    @if ($errors->has('tipo_viatico'))<span class="error-message">{{ $errors->first('tipo_viatico') }}</span>
                                    @endif 
                                </div>
                            </div>

                           
                        </div>
                        <div class="form-group"></div>
                        <div class="col-lg-12 text-center" >    
                           <button type="submit" id="editar_programacion" class="btn btn-warning">Editar</button>
                           <a href="{{route('home')}}" class="btn btn-default">Cancelar</a>
                       </div>  

                   </form>
               </div> 
           </section>

       </div>
   </div>
   <!-- end: page -->
</section>

@endsection


