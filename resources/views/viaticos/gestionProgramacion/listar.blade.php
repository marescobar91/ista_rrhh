@extends('layouts.template')
@section('content')
<section role="main" class="content-body">
    <header class="page-header">
        <h2>Gesti&oacute;n de Programación Mensual de Trabajo</h2>
        <div class="right-wrapper pull-right">
            <ol class="breadcrumbs">
                <li style="margin-left: -25%"><a href="{{url('/')}}"><button class="btn btn-default"><span class="fa fa-chevron-left"> </span> Regresar </button></a></li>
            </ol>
        </div>
    </header>

    <!-- start: page -->
    <div class="row">
        <div class="col-lg-12">
            <div>
                @if ($errors->any())
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    
                    <div class="errors">
                        <ul>
                            @foreach ($errors->all() as $error)

                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif

                </div>
            </div>
            <section class="panel">
             @include('flash::message') 
             <header class="panel-heading">

                <div class="panel-actions">
                    @can('asignar.centro')
                    <a href="{{route('centros.asignar')}}"><button class="btn btn-success"><span class="fa fa-plus"></span> Asignar horarios a Centros</button></a> 
                    @endcan
                    @can('crear.programacion')
                    <a href="{{route('programacion.crear')}}"><button class="btn btn-primary"><span class="fa fa-plus"></span> Agregar Programación </button></a>
                    @endcan
                    @can('generar.cco')
                    <a href="{{route('generar.cco')}}"><button class="btn btn-success"><span class="fa fa-plus"></span> Generar CCO's </button></a>
                    @endcan
                </div>                                
                                <h2 class="panel-title">Listado de Programaciones</h2>
                            </header>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="tabs">
                                        <ul class="nav nav-tabs nav-justified">
                                            <li class="active">
                                                <a href="#popular10" data-toggle="tab" class="text-center"><i class="fa fa-star"></i> Habilitados</a>
                                            </li>
                                            <li>
                                                <a href="#recent10" data-toggle="tab" class="text-center">Deshabilitados</a>
                                            </li>
                                        </ul>
                                        <div class="tab-content">
                                            <div id="popular10" class="tab-pane active">
                                             <div class="panel-body" style="overflow: scroll;">
                                                <table class="table table-bordered table-striped mb-none" id="datatable-default">
                                                    <thead>
                                                        <tr>
                                                            <th></th>
                                                            <th>Centro</th>
                                                            <th>Oficina</th>
                                                            <th>Codigo</th>
                                                            <th >Empleado</th>
                                                            <th> Desde</th>
                                                            <th> Hasta</th>
                                                            <th> Tipo </th>
                                                            <th>Pago Viatico</th>
                                                            <th class="col-md-2">Accion</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                       @foreach($programacion as $pro)
                                                       <tr class="gradeX" >
                                                        <td style="visibility:hidden;">{{$pro->id_programacion}}</td>
                                                        <td>{{ $pro->centro }}</td>
                                                        <td>{{ $pro->oficina }}</td>
                                                        <td>{{$pro->codigo_empleado}}</td>
                                                        <td>{{$pro->nombre}} {{$pro->apellido}}</td>
                                                        <td>{{date('d-m-Y', strtotime($pro->fecha_desde))}}</td>
                                                        <td>{{date('d-m-Y', strtotime($pro->fecha_hasta))}}</td>
                                                        <td>{{$pro->tipo_viatico}}</td>
                                                        <td>${{$pro->pago_viatico}}.00</td>
                                                        <td class="text-center">
                                                            @if($pro->estado==1)
                                                            @can('editar.programacion')
                                                            <a href="{{ route('viaticos.programacion.editar', $pro->id_programacion)}}" class="btn btn-warning" ><span class="fa fa-pencil"></span></a>
                                                            @endcan
                                                            @can('eliminar.programacion')
                                                            <a href="{{route('viaticos.eliminar.programacion',$pro->id_programacion)}}" id="delete"class="btn btn-danger eliminar"  ><span class="fa fa-trash-o"></span></a>
                                                            @endcan
                                                            @else
                                                            <button class="btn btn-warning" disabled><span class="fa fa-pencil"></span></button>
                                                            <button class="btn btn-danger" disabled=""><span class="fa fa-trash-o"></span></button>
                                                            @endif

                                                            <a href="{{route('viaticos.imprimir.programacion',$pro->id_programacion)}}" class="btn btn-info" target="_blank"><span class="fa fa-eye"></span></a>

                                                        </td>
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div id="recent10" class="tab-pane">
                                        <div class="panel-body" style="overflow: scroll;">
                                            <table class="table table-bordered table-striped mb-none" id="datatable-default">
                                                <thead>
                                                    <tr>
                                                        <th>Centro</th>
                                                        <th>Unidad</th>
                                                        <th>Oficina</th>
                                                        <th>Codigo</th>
                                                        <th >Empleado</th>
                                                        <th> Desde</th>
                                                        <th> Hasta</th>
                                                        <th> Tipo </th>
                                                        <th>Pago Viatico</th>
                                                        <th class="col-md-2">Accion</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                 @foreach($programacionDeshabilitado as $pro)
                                                 <tr class="gradeX" >
                                                    <td>{{ $pro->centro }}</td>
                                                        <td>{{ $pro->centro }}</td>
                                                        <td>{{ $pro->oficina }}</td>
                                                        <td>{{$pro->codigo_empleado}}</td>
                                                        <td>{{$pro->nombre}} {{$pro->apellido}}</td>
                                                        <td>{{date('d-m-Y', strtotime($pro->fecha_desde))}}</td>
                                                        <td>{{date('d-m-Y', strtotime($pro->fecha_hasta))}}</td>
                                                        <td>{{$pro->tipo_viatico}}</td>
                                                        <td>${{$pro->pago_viatico}}.00</td>
                                                    <td class="text-center">
                                                        @can('habilitar.programacion')
                                                        <a href="{{route('viaticos.habilitar.programacion',$pro->id_programacion)}}" id="delete"class="btn btn-primary habilitar"  ><span class="fa fa-caret-up"></span></a>
                                                        @endcan
                                                        <a href="{{route('viaticos.imprimir.programacion',$pro->id_programacion)}}" class="btn btn-info" target="_blank"><span class="fa fa-eye"></span></a>

                                                    </td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
    <!-- end: page -->
</section>

@endsection
