@extends('layouts.template')
@section('content')
<section role="main" class="content-body">
    <header class="page-header">
        <h2>Gesti&oacute;n de Programacion Mensual de Trabajo</h2>
        <div class="right-wrapper pull-right">
            <ol class="breadcrumbs">
                <li style="margin-left: -25%"><a href="{{route('programacion.listar')}}"><button class="btn btn-default"><span class="fa fa-chevron-left"> </span> Regresar </button></a></li>
            </ol>
        </div>
    </header>
    <!-- start: page -->
    <div class="row">
        <div class="col-lg-12">
            <div>
            
                @if ($errors->any())
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                    <div class="errors">
                        <ul>
                            @foreach ($errors->all() as $error)

                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                </div>
            </div>
            @include('flash::message')
            <section class="panel">
                <header class="panel-heading">
                    <div class="panel-actions">
                    </div>
                    <h2 class="panel-title">Ingreso de Programación de Viáticos</h2>
                </header>
                <div class="panel-body">
                    <form role="form" method="POST" action="{{ route('programacion.guardar') }}" class="contactForm" id="form_programacion">
                        @csrf  
                        <div class="col-md-2"></div>        
                        <div class="col-md-8">
                            <input type="hidden" id="id_departamento" value="">  
                            <input type="hidden" id="id_oficina" name="id_oficina" value="">
                            <input type="hidden" id="id_centro" name="id_centro" value="">
                            <div class="form-group">
                                <label class="control-label col-md-12">Empleado</label>
                                <div class="col-md-12">
                                    <select data-plugin-selectTwo id="empleado" class="form-control populate" name="empleado" required class="form-control field-input {{ $errors->has('empleado') ? 'field-error' : '' }}">
                                        <option value="" selected>Seleccione empleado</option>
                                        @foreach($empleados as $empleado)
                                        <option value="{{$empleado->id_empleado}}">{{$empleado->codigo_empleado}} || {{$empleado->nombre}} {{$empleado->apellido}}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('empleado'))<span class="error-message">{{ $errors->first('empleado') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-12 control-label ">Centro</label>
                                <div class="col-md-12">
                                    <input id="centro" type="text" name="centro" required disabled autofocus placeholder="" class="form-control field-input {{ $errors->has('centro') ? 'field-error' : '' }}" value="">
                                    @if ($errors->has('centro'))<span class="error-message">{{ $errors->first('centro') }}</span>
                                    @endif
                                </div>
                            </div>
                             <div class="form-group">
                                <label class="col-md-12 control-label ">Oficina</label>
                                <div class="col-md-12">
                                    <input id="oficina" type="text" name="oficina" required disabled autofocus placeholder="" class="form-control field-input {{ $errors->has('oficina') ? 'field-error' : '' }}" value="">
                                    @if ($errors->has('oficina'))<span class="error-message">{{ $errors->first('oficina') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-12 control-label ">Nombre</label>
                                <div class="col-md-12">
                                    <input id="nombre" type="text" name="nombre" required disabled autofocus placeholder="" class="form-control field-input {{ $errors->has('nombre') ? 'field-error' : '' }}" value="">
                                    @if ($errors->has('nombre'))<span class="error-message">{{ $errors->first('nombre') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-12">Actividad</label>
                                <div class="col-md-12">
                                    <select data-plugin-selectTwo id="actividad" class="form-control populate" name="actividad" required class="form-control field-input {{ $errors->has('actividad') ? 'field-error' : '' }}" >
                                        <option value="" selected>Seleccione actividad</option>
                                        @foreach($actividad as $actividad)
                                        <option value="{{$actividad->id_actividad}}" @if (old('actividad') == $actividad->id_actividad) selected="selected" @endif>{{$actividad->actividad}}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('actividad'))<span class="error-message">{{ $errors->first('actividad') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-5">
                                     <label class="col-md-12 control-label text-left" for="w4-first-name">Fecha Inicio: </label>
                                    <div class="col-md-12">
                                            <input type="date"  value="{{$fecha}}" class="form-control field-input" name="fecha_inicio" id="fecha_inicio"  style="background: white" placeholder="dd/mm/yyyy" class="form-control field-input {{ $errors->has('fecha_inicio') ? 'field-error' : '' }}">
                                        @if ($errors->has('fecha_inicio'))<span class="error-message">{{ $errors->first('fecha_inicio') }}</span>
                                        @endif
                                    </div>
                                </div>
                           <div class="col-md-2">
                                    <label class="col-md-12" style="color:transparent;">.</label>
                                <div class="col-md-12">
                                    <div class="checkbox-custom checkbox-default">
                                        <input type="checkbox"  id="repetir" name="repetir">
                                        <label for="w4-terms">Repetir fecha</label>
                                    </div>
                                </div> 
                               </div>
                                <div class="col-md-5">
                                    <!-- fecha inicio-->
                                    <label class="col-md-12 control-label" for="w4-first-name">Fecha Final: </label>
                                    <div class="col-md-12">
                                            <input type="date" value="{{old('fecha_final')}}" class="form-control field-input" name="fecha_final" id="fecha_final"  style="background: white" placeholder="dd/mm/yyyy" class="form-control field-input {{ $errors->has('fecha_final') ? 'field-error' : '' }}">
                                            @if ($errors->has('fecha_final'))<span class="error-message">{{ $errors->first('fecha_final') }}</span>
                                        @endif 
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-12">Lugar de Misión</label>
                                <div class="col-md-8">
                                    <select data-plugin-selectTwo id="lugarMision" class="form-control populate" name="lugarMision" required class="form-control field-input {{ $errors->has('lugarMision') ? 'field-error' : '' }}">
                                        <option value="" selected>Seleccione lugar de mision</option>
                                        @foreach($lugarMision as $lugar)
                                        <option value="{{$lugar->id_lugar_mision}}" @if (old('lugarMision') == $lugar->id_lugar_mision) selected="selected" @endif>{{$lugar->departamento->departamento}} || {{$lugar->lugar_mision}}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('lugarMision'))<span class="error-message">{{ $errors->first('lugarMision') }}</span>
                                    @endif
                                </div>
                                <div class="form-group" >
                                       <div class="col-md-2">
                                        <div class="radio-custom radio-primary">
                                            <input type="radio"  id="diario" name="tipo_viatico" value="1" required {{ old("tipo_viatico") == '1' ? 'checked' : '' }}>
                                            <label for="w4-terms">Diario</label>                                     
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="radio-custom radio-primary">
                                            <input type="radio"  id="corrido" name="tipo_viatico" value="2" required {{ old("tipo_viatico") == '2' ? 'checked' : '' }}>
                                            <label for="w4-terms">Corrido</label>
                                        </div>
                                    </div>
                                    @if ($errors->has('tipo_viatico'))<span class="error-message">{{ $errors->first('tipo_viatico') }}</span>
                                    @endif 
                                </div>
                            </div>
                        </div>
                        <div class="form-group"></div>
                        <div class="col-lg-12 text-center" >    
                           <button type="submit" id="guardar_programacion" class="btn btn-primary">Guardar</button>
                           <a href="{{route('home')}}" class="btn btn-default">Cancelar</a>
                       </div>  

                   </form>
               </div>
           </section>
           <section class="panel" >
            <header class="panel-heading">
                <h2 class="panel-title">Detalle de Viaticos</h2>
            </header>
               <div class="panel-body" style="overflow: scroll;">
                   <table class="table table-bordered table-striped mb-none" id="datatable-default" >
                       <thead>
                           <tr>
                               <th>Empleado</th>
                               <th>Oficina</th>
                               <th>Actividad</th>
                               <th>Lugar Mision</th>
                               <th>Departamento</th>
                               <th>Fecha inicio</th>
                               <th>Fecha fin</th>
                               <th>Tipo</th>
                               <th>Pago</th>
                           </tr>
                       </thead>
                       <tbody id="cuerpo">
                        @foreach($programacion as $progra)
                        <tr class="gradeX" >
                         <td>{{$progra->programacionEmpleado->nombre}} {{$progra->programacionEmpleado->apellido}}</td>
                         <td>{{$progra->oficina->oficina}}</td>
                         <td>{{$progra->actividad->actividad}}</td>
                         <td>{{$progra->lugarMision->lugar_mision}}</td>
                         <td>{{$progra->lugarMision->departamento->departamento}}</td>
                         <td>{{date('d-m-Y', strtotime($progra->fecha_desde))}}</td>  
                         <td>{{date('d-m-Y', strtotime($progra->fecha_hasta))}}</td> 
                         <td>{{$progra->tipoViatico->tipo_viatico}}</td>
                         <td>${{$progra->pago_viatico}}.00</td>
                        </tr>
                        @endforeach
                       </tbody>
                   </table>
               </div>
           </section>

       </div>
   </div>
   <!-- end: page -->
</section>

@endsection

@section('js')

<script type="text/javascript">

        $(document).ready(function() {

            function cargarEmpleado(){
                var empleado = $('#empleado').val();
                if($.trim('empleado') != ''){
                    $.get('/viaticos/getEmpleado', {empleado:  empleado}, function (data){
                        $('#nombre').val(data.nombre);
                        $('#oficina').val(data.oficina);
                        $('#id_oficina').val(data.id_oficina);
                        $('#centro').val(data.centro);
                        $('#id_centro').val(data.id_centro);
                    });
                }
            }
            $('#empleado').on('change', cargarEmpleado);
            

           $('#repetir').on('change', function(){
                var repetir = $("#repetir").is(":checked");
                if (repetir) {
                    var fecha_inicio = $('#fecha_inicio').val();
                    $('#fecha_final').val(fecha_inicio);
                }
            });

        

    });

</script>
@endsection