@extends('layouts.template')
@section('content')
<section role="main" class="content-body">
  <header class="page-header">
    <h2>Programación Mensual de Trabajo</h2>
    <div class="right-wrapper pull-right">
            <ol class="breadcrumbs">
                <li style="margin-left: -25%"><a href="{{url('/')}}"><button class="btn btn-default"><span class="fa fa-chevron-left"> </span> Regresar </button></a></li>
            </ol>
        </div>
  </header>
  <!-- start: page -->
  <div class="row">
    <div class="col-lg-12">

      @if ($errors->any())
      <div class="alert alert-danger">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

        <div class="errors">
          <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
          </ul>
        </div>
        @endif

      </div>
    </div>

    <section class="panel">
      @include('flash::message') 
      <header class="panel-heading">
        <h2 class="panel-title">Buscar Programación Mensual de Trabajo</h2>
      </header>
      <div class="row">
        <div class="col-md-12">
         <div class="panel-body">

           <form class="form-horizontal" method="POST" action="{{ route('cargar.busqueda') }}" >
            {{ csrf_field() }} 


            <!-- fecha inicio-->
            <div class="form-group">
              <div class="col-md-1"></div>
              <label class="col-md-2 control-label text-right" for="w4-first-name">Fecha Inicio: </label>
              <div class="col-md-2">
                <div class="input-group">
                  <input type="date"  class="form-control field-input" name="fecha_inicio" id="fecha_inicio"   style="background: white" placeholder="dd/mm/yyyy" required> 
                </div>
              </div>
              <!-- fecha inicio-->
              <label class="col-md-2 control-label text-right" for="w4-first-name">Fecha Final: </label>
              <div class="col-md-2">
                <div class="input-group">
                  <input type="date"  class="form-control field-input" name="fecha_final" id="fecha_final"  style="background: white" placeholder="dd/mm/yyyy" required> 
                </div>
              </div>
            </div>


            <!--Boton de Generar Reporte-->
            <div class="form-group" align="center">
             <button type="submit" class="btn btn-success">
              Generar
            </button>
          </div>

        </form> 

      </div>
    </div>
  </div>

</section>

@if($vacio != null)
<section class="panel">
  <header class="panel-heading">
    <div class="panel-actions">
     <div class="col-lg-8"> 

     </div>
   </div>           

   <h2 class="panel-title">Busqueda de programaciones </h2>
 </header>

 <div class="row">
  <div class="col-md-12"> 
    <div class="panel-body">
      <table class="table table-bordered table-striped mb-none" style="overflow: scroll;" width="90%">

        <thead>
          <tr>
            <th>Ubicación</th>
            <th>Desde</th>
            <th>Hasta</th>
            <th>Monto</th>
            <th>Usuario</th>
            <th>Programación</th>
            <th>Ingresada</th>
            <th>Impreso</th>
          </tr>
        </thead>
        <tbody>
          @foreach($programacion as $programacion)
          <tr class="gradeX"> 
            <td>{{$programacion->unidad_administrativa}}</td>
            <td>{{date('d-m-Y', strtotime($programacion->minimo))}}</td>
            <td>{{date('d-m-Y', strtotime($programacion->maximo))}}</td>
            <td>${{$programacion->monto}}.00</td>
            <td>{{$programacion->nombre_usuario}}</td>
            <td>
              <form action="{{route('programacion.pdf.ver',$programacion->id_unidad_administrativa)}}" method="POST" class="col-md-2" target="_blank">
               @if($vacio != null)
               {{ csrf_field() }}
               <input id="fecha_inicio" type="hidden" class="form-control" value="{{$fecha_inicio}}" name="fecha_inicio">
               <input id="fecha_final" type="hidden" class="form-control" value="{{$fecha_final}}" name="fecha_final"> 
               @endif
               <button class="btn btn-info" title="Programacion"><span class="fa fa-file-code-o"></span> </button>
             </form>
           </td>
           <td>
             <div class="checkbox-primary">                                  
                        <input type="checkbox" id="afiliado_sindicato" disabled {{  ($programacion->estado_programacion == 1 ? ' checked' : '') }} > 
                    </div>
           </td>
           <td>
             <div class="checkbox-primary">                                  
                        <input type="checkbox" id="afiliado_sindicato" disabled name="afiliado_sindicato" {{  ($programacion->impreso == 1 ? ' checked' : '') }} > 
                    </div>
           </td>
         </tr>
         @endforeach
       </tbody>

       <tfoot>
         <td colspan="3" align="right">Total</td>
         <td>${{$total->monto}}.00</td>
         <td colspan="4"></td>
       </tfoot>
     </table>
   </div>
 </div>
</div>
</section>

</div>
</div>
@endif
<!-- end: page -->
</section>

@endsection
