@extends('layouts.template')
@section('content')
<section role="main" class="content-body">
    <header class="page-header">
        <h2>Gestión de Periodo para CETIA's</h2>
        <div class="right-wrapper pull-right">
            <ol class="breadcrumbs">
                <li style="margin-left: -25%"><a href="{{route('programacion.listar')}}"><button class="btn btn-default"><span class="fa fa-chevron-left"> </span> Regresar </button></a></li>
            </ol>
        </div>
    </header>

    <!-- start: page -->
    <div class="row">
        <div class="col-lg-12">
            <div>
                @if ($errors->any())
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    
                    <div class="errors">
                        <ul>
                            @foreach ($errors->all() as $error)

                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif

                </div>
            </div>
            <section class="panel">

                <header class="panel-heading">
 
                                <h2 class="panel-title">Listado de Validación por CETIA's</h2>
                            </header>
                            <div class="row"> 
                                <div class="panel-body">
                                                <table class="table table-bordered table-striped mb-none" id="datatable-default">
                                                    <thead>
                                                        <tr>
                                                            <th >Nombre</th>
                                                            <th >Hora Inicial</th>
                                                            <th>Hora Final</th>
                                                            <th>Activo</th>
                                                            <th>Fin de Semana</th>
                                                            <th>Asueto</th>
                                                            <th class="col-md-2">Accion</th>
                                                            

                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @foreach($centros as $centro)
                                                     <tr class="gradeX" >
                                                        <td>{{$centro->centro}}</td>
                                                        <td>@php
                                                            if($centro->fecha_inicio==1)
                                                                 {echo 'Lunes';}
                                                             elseif ($centro->fecha_inicio==2) {
                                                                 {echo 'Martes';}
                                                             }
                                                             elseif ($centro->fecha_inicio==3) {
                                                                 {echo 'Miércoles';}
                                                             }
                                                             elseif ($centro->fecha_inicio==4) {
                                                                 {echo 'Jueves';}
                                                             }
                                                             elseif ($centro->fecha_inicio==5) {
                                                                 {echo 'Viernes';}
                                                             }
                                                             elseif ($centro->fecha_inicio==6) {
                                                                 {echo 'Sábado';}
                                                             }
                                                             elseif ($centro->fecha_inicio==7) {
                                                                 {echo 'Domingo';}
                                                             }elseif ($centro->fecha_inicio==0) {
                                                                 {echo ' ';}
                                                             }
                                                        @endphp

                                                          {{$centro->hora_inicio }} </td>
                                                        <td>@php
                                                            if($centro->fecha_fin==1)
                                                                 {echo 'Lunes';}
                                                             elseif ($centro->fecha_fin==2) {
                                                                 {echo 'Martes';}
                                                             }
                                                             elseif ($centro->fecha_fin==3) {
                                                                 {echo 'Miércoles';}
                                                             }
                                                             elseif ($centro->fecha_fin==4) {
                                                                 {echo 'Jueves';}
                                                             }
                                                             elseif ($centro->fecha_fin==5) {
                                                                 {echo 'Viernes';}
                                                             }
                                                             elseif ($centro->fecha_fin==6) {
                                                                 {echo 'Sábado';}
                                                             }
                                                             elseif ($centro->fecha_fin==7) {
                                                                 {echo 'Domingo';}
                                                             }elseif ($centro->fecha_fin==0) {
                                                                 {echo ' ';}
                                                             }
                                                        @endphp
                                                         {{$centro->hora_fin}}</td>
                                                           <td class="text-center">
                                                            @can('desactivar.centro')
                                                            @if($centro->activo==1)
                                                            <a href="{{ route('centros.desactivar', $centro->id_centro) }}" ><span class="fa fa-check fa-2x"></span></a>
                                                            @endif
                                                            @endcan
                                                            @can('activar.centro')
                                                            @if($centro->activo==0)
                                                            <a href="{{ route('centros.activar', $centro->id_centro) }}"><span class="fa fa-times fa-2x"></span></a>
                                                            @endif
                                                            @endcan
                                                        </td>
                                                         <td class="text-center">
                                                          
                                                            @if($centro->fin_semana==1)
                                                            <a href="{{ route('centro.finsemana.desactivar', $centro->id_centro) }}" ><span class="fa fa-check fa-2x"></span></a>
                                                            @endif
                                                          
                                                            @if($centro->fin_semana==0)
                                                            <a href="{{ route('centro.finsemana.activar', $centro->id_centro) }}"><span class="fa fa-times fa-2x"></span></a>
                                                            @endif
                                                            
                                                        </td>
                                                         <td class="text-center">
                                                         
                                                            @if($centro->asueto==1)
                                                            <a href="{{ route('centro.asueto.desactivar', $centro->id_centro) }}" ><span class="fa fa-check fa-2x"></span></a>
                                                            @endif
                                                          
                                                            @if($centro->asueto==0)
                                                            <a href="{{ route('centro.asueto.activar', $centro->id_centro) }}"><span class="fa fa-times fa-2x"></span></a>
                                                            @endif
                                                      
                                                        </td>
                                              
                                                        <td class="text-center"> 
                                                            @can('programar.centro')
                                                            <a href="{{ route('centros.programar', $centro->id_centro) }}" class="btn btn-warning" ><span class="fa fa-pencil"></span></a>
                                                            @endcan
                                                        </td>
                                                     
                                                    </tr>
                                                    @endforeach

                                                </tbody>
                                            </table>
                            
                       
                    </div>
                </div>
            </section>
        </div>
    </div>
    <!-- end: page -->
</section>

@endsection
