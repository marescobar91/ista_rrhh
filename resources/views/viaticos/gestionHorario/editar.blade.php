@extends('layouts.template')
@section('content')
<section role="main" class="content-body">
    <header class="page-header">
        <h2>Gestión de Periodo para CETIA's</h2>
        <div class="right-wrapper pull-right">
            <ol class="breadcrumbs">
                <li style="margin-left: -25%"><a href="{{route('centros.asignar')}}"><button class="btn btn-default"><span class="fa fa-chevron-left"> </span> Regresar </button></a></li>
            </ol>
        </div>
    </header>

    <!-- start: page -->
    <div class="row">
        <div class="col-lg-12">
            <div>
                @if ($errors->any())
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    
                    <div class="errors">
                        <ul>
                            @foreach ($errors->all() as $error)

                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif

                </div>
            </div>
            <section class="panel">
                <header class="panel-heading">
                    <div class="panel-actions">
                    </div>
                    <h2 class="panel-title">Asignar periodo a Centros</h2>
                </header>
                <div class="panel-body">
                    <form class="form-horizontal form-bordered" method="POST" action="{{route('centros.horarios', $centro->id_centro)}}">
                      {{csrf_field()}}
                    {{ method_field('PUT') }}                      
                            <div class="col-md-3"></div>
                            <div class="col-md-6">
                               <label class="col-md-12 control-label text-right" style="text-align: left;">CETIA:</label>
                               <div class="col-md-12">
                               <input id="centro" type="text" value="{{ $centro->centro}}" name="centro" required disabled autofocus placeholder="" class="form-control field-input {{ $errors->has('oficina') ? 'field-error' : '' }}">
                            </div>
                        </div>
                        <div class="col-md-12"></div>
                        <div class="col-md-3" ></div>
                        <div class="col-md-3">
                            <label class="col-md-12 control-label text-right" style="text-align: left;">Dia Inicial:</label>
                               <div class="col-md-12">
                                <select data-plugin-selectTwo id="dia_inicial" class="form-control" name="dia_inicial" required>
                                    <option value="">Seleccione Uno</option>
                                    <option value="1" @if($centro->fecha_inicio=== 1) selected='selected' @endif>Lunes</option>
                                    <option value="2" @if($centro->fecha_inicio=== 1) selected='selected' @endif>Martes</option>
                                    <option value="3" @if($centro->fecha_inicio=== 3) selected='selected' @endif>Miercoles</option>
                                    <option value="4" @if($centro->fecha_inicio=== 4) selected='selected' @endif>Jueves</option>
                                    <option value="5" @if($centro->fecha_inicio=== 5) selected='selected' @endif>Viernes</option>
                                    <option value="6" @if($centro->fecha_inicio=== 6) selected='selected' @endif>Sabado</option>
                                    <option value="0" @if($centro->fecha_inicio=== 7) selected='selected' @endif>Domingo</option>
                                </select> 
                                @if ($errors->has('dia_inicial'))<span class="error-message">{{ $errors->first('dia_inicial') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-3">
                            <label class="col-md-12 control-label text-right" style="text-align: left;">Hora:</label>
                            <input id="hora_inicio" type="time" value="{{ $centro->hora_inicio }}" name="hora_inicio" required autocomplete="hora_inicio" autofocus class="form-control field-input {{ $errors->has('hora_inicio') ? 'field-error' : '' }}">
                                    @if ($errors->has('hora_inicio'))<span class="error-message">{{ $errors->first('hora_inicio') }}</span>
                                    @endif
                        </div>
                        <div class="col-md-12"></div>
                        <div class="col-md-3"></div>
                        <div class="col-md-3">
                            <label class="col-md-12 control-label text-right" style="text-align: left;">Dia Final:</label>
                               <div class="col-md-12">
                                <select data-plugin-selectTwo id="dia_final" class="form-control" name="dia_final" required>
                                   <option value="">Seleccione Uno</option>
                                    <option value="1" @if($centro->fecha_fin===1) selected='selected' @endif>Lunes</option>
                                    <option value="2" @if($centro->fecha_fin===2) selected='selected' @endif>Martes</option>
                                    <option value="3" @if($centro->fecha_fin===3) selected='selected' @endif>Miercoles</option>
                                    <option value="4" @if($centro->fecha_fin===4) selected='selected' @endif>Jueves</option>
                                    <option value="5" @if($centro->fecha_fin===5) selected='selected' @endif>Viernes</option>
                                    <option value="6" @if($centro->fecha_fin===6) selected='selected' @endif>Sabado</option>
                                    <option value="0" @if($centro->fecha_fin===7) selected='selected' @endif>Domingo</option>
                                </select> 
                                @if ($errors->has('dia_final'))<span class="error-message">{{ $errors->first('dia_final') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-3">
                            <label class="col-md-12 control-label text-right" style="text-align: left;">Hora:</label>
                            <input id="hora_final" type="time"  value="{{ $centro->hora_fin }}" name="hora_final" required autocomplete="hora_final" autofocus class="form-control field-input {{ $errors->has('hora_final') ? 'field-error' : '' }}">
                                    @if ($errors->has('hora_final'))<span class="error-message">{{ $errors->first('hora_final') }}</span>
                                    @endif
                        </div>

                        <div class="col-md-12" style="padding-bottom: 1%"></div>
                        <div class="col-md-3"></div>
                        <div class="col-md-3">
                            <label class="control-label" style="text-align: right; padding-left: 15%"> Activo:</label>
                            <input type="checkbox" name="activo" id="activo" checked="checked" disabled class="checkbox-custom checkbox-default">
                        </div>


                        <div class="form-group"></div>
                    <div class="col-lg-12 text-center" >    
                       <button type="submit" class="btn btn-primary">Guardar</button>
                       <a href="{{route('home')}}" class="btn btn-default">Cancelar</a>
                   </div>                              
               </form>
           </div>
       </section>

   </div>
</div>
<!-- end: page -->
</section>

@endsection