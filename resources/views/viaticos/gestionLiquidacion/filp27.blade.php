<!DOCTYPE html>

<head>
  <title>ISTA - RRHH</title>
  <link href="css/style.css" rel="stylesheet">
  <style>
    .celda{
            border-bottom: lightgrey 0.1em solid;
        }
        .celdaa{
            border-bottom: lightgrey 0.1em solid;
        }
    .encabezado{
            border-bottom: grey 0.1em solid;
            background-color: grey;
            color: white;
        }
    .footer{
     position: fixed; left: 0px; bottom: -120px; right: 0px; height: 150px; 
   }
   .font{
    text-align: center;
    font-size: 11.3px;
    font-family: sans-serif;
  }
</style>
</head>
<body>
  <div id="wrapper">
    <!-- Page Content -->
    <div id="page-content-wrapper">
      <div class="container-fluid">
        <div class="container" style="margin-left: 13px">
          <div>
            <div class="table-title" style="background-color: #fff; margin-bottom: 1%">
              <div class="row">
                 <table width="97%">
                <tr align="center">
                  <td ><img src="{{public_path().'/img/logo_ista.png'}}" width="15%"></td>
                </tr>
                <tr>
                  <td><h5 style="text-transform: uppercase; font-size: 12px" align="center">Instituto Salvadoreño de Transformación Agraria</h5>
                    <h5 style="text-transform: uppercase; font-size: 12px; margin-bottom: -20px;margin-top: -20px" align="center">UFI-FCMF</h5>
                    <h5 style="text-transform: uppercase; font-size: 12px" align="center">Recibo de Liquidación</h5>
                  </td>
                </tr>
                <tr>
                  <td style="text-align: right;"><p style="font-size: 10px; margin-top: -20px">FIPL 27 R_0 </p> </td>
                </tr>
                <tr>
                  <td>
                    <h5 style="text-transform: uppercase; font-size: 12px; margin-bottom: -20px;margin-top: -20px" align="center">NIT: 0614-310131-003-0</h5>
                  </td>
                </tr>
                <tr>
                  <td>
                    <h5 style="text-transform: uppercase; font-size: 12px; margin-bottom: -20px;margin-top: -10px" align="center">Por: $ {{$pagado->pago}}.00</h5>
                  </td>
                </tr>
              </table>
              <p style="text-transform: uppercase; font-size: 12px; margin-bottom: 20px;margin-top: 10px">SEDE: <strong>{{$centro->centro}}</strong> </p>
              <p style="text-transform: uppercase; font-size: 12px; margin-bottom: 20px;margin-top: -15px">Analisis de Liquidación N°: <strong>{{$referencia_ufi}}</strong> </p>
              </div>
            </div>
            <div class="row">
              <table class="table table-striped table-hover col-md-5" id="reporte" style="background: #fff;" width="97%"  >
                <thead class="font">
                  <tr class="encabezado">
                    <th>TIPO DE VIATICOS</th>
                    <th>CEHEQUE/EFECTIVO</th>
                    <th>TOTAL</th>
                  </tr>
                </thead>
                <tbody class="font">
                  <tr>
                    <td class="celda">Programado</td>
                    <td class="celda">${{$pagado->pago}}.00</td>
                    <td class="celda">${{$pagado->pago}}.00</td>
                  </tr>
                  <tr>
                    <td class="celda">Pagado</td>
                    <td class="celda">${{$pagado->pago}}.00</td>
                    <td class="celda">${{$pagado->pago}}.00</td>
                  </tr>
                  <tr>
                    <td class="celda">Anulado</td>
                    <td class="celda">${{$nulo->pago}}.00</td>
                    <td class="celda">${{$nulo->pago}}.00</td>
                  </tr>
                  <tr>
                    <td class="celda"><strong>MONTO A REMESAR / LIQUIDAR</strong></td>
                    <td class="celda"><strong>${{$nulo->pago}}.00</strong></td>
                    <td class="celda"><strong>${{$nulo->pago}}.00</strong></td>
                  </tr>
                </tbody>
               
            </table>
            <br/>
            <br/> 
            <p style="font-size: 12px">La aplicación de gasto al recurso del Fondo Circulante se realizan segun lo dispuesto en los Procedimientos para el Manejo de Recursos del Fondo Circulante del Monto Fijo, autorizado por la DINAFI en fecha 27/05/2015</p>
            <p style="font-size: 12px">Esta liquidación corresponde al periodo del {{date('d-m-Y', strtotime($fecha_inicio))}} al {{date('d-m-Y', strtotime($fecha_final))}}, pagada mediante el FCMF  con el cheque N° {{$cheque->cheque}}, se adjuntan C.C.O. correspondientes y Reportes de personal con viáticos anticipados pagados y/o realizados a pagar</p>
            <br/>
            <p>____________________</p>
            <p style="font-size: 14px">Encargado(a) de FCMF</p>
            <p style="font-size: 14px; margin-top: -1%;margin-bottom: -1%">DUI:</p>
            <p style="font-size: 14px">NIT:</p>
            <br/>
            <p style="font-size: 14px">C.P. 54403</p>
            <br/>
            <br/> 
            <p style="border-top:black 0.05em solid; width: 40%; margin-left: 30%"></p>
            <br/>
            <p style="font-size: 11px" align="center" >
              Instituto Salvadoreño de Transformación Agraria
            </p>
            <p style="font-size: 10px" align="center">

              Kilómetro 5 1/2 Carretera a Santa Tecla, Final Calle y Colonia Las Mercedes, San Salvador, El Salvador.
              <br/>
              PBX: 2594-1000, E-mail: info@ista.gob.sv , Twitter: @ISTA_SV
              <br/>
              Página Web: www.ista.gob.sv
            </p>

          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
</body>
</html>