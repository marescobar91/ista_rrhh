@extends('layouts.template')
@section('content')
<section role="main" class="content-body">
    <header class="page-header">
        <h2>Gestión de Liquidación de Viáticos</h2>
        <div class="right-wrapper pull-right">
            <ol class="breadcrumbs">
                <li style="margin-left: -25%"><a href="{{route('liquidaciones.listar')}}"><button class="btn btn-default"><span class="fa fa-chevron-left"> </span> Regresar </button></a></li>
            </ol>
        </div>
    </header>

    <!-- start: page -->
    <div class="row">
        <div class="col-lg-12">
            <div>
                @if ($errors->any())
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    
                    <div class="errors">
                        <ul>
                            @foreach ($errors->all() as $error)

                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif

                </div>
            </div>
            <section class="panel">
                <header class="panel-heading">
                    <div class="panel-actions">
                        <a href="#" class="fa fa-caret-down"></a>
                    </div>
                    <h2 class="panel-title">Liquidación de Viáticos</h2>
                </header>
                <div class="panel-body">

                    <form class="form-horizontal form-bordered" method="POST" action="{{ route('liquidaciones.buscar') }}">
                        @csrf                        
                        <div class="col-md-2"></div>
                        <div class="col-md-4">
                         <label class="col-md-12 control-label text-right" style="text-align: left;">N° Referencia RH</label>
                         <div class="col-md-12">

                            <select data-plugin-selectTwo id="referencia" class="form-control populate" name="referencia" required class="form-control field-input {{ $errors->has('referencia') ? 'field-error' : '' }}">

                                <option value="" selected>Seleccione Referencia</option>
                                @foreach($viatico as $viatico)
                                <option value="{{$viatico->referencia}}">{{$viatico->referencia}}</option>
                                @endforeach

                            </select>

                            @if ($errors->has('referencia'))<span class="error-message">{{ $errors->first('referencia') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-4">
                        <label class="col-md-12 control-label text-right" style="text-align: left;">Cerrado en RH</label>
                        <div class="col-md-12">
                            <input id="estado" type="text" value="" name="estado" required readonly class="form-control field-input {{ $errors->has('estado') ? 'field-error' : '' }}">
                            @if ($errors->has('estado'))<span class="error-message">{{ $errors->first('estado') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-12"></div>
                    <div class="col-md-2"></div>
                    <div class="col-md-4">
                        <label class="col-md-12 control-label text-right" style="text-align: left;">Forma Pago</label>
                        <div class="col-md-12"  >
                            <input id="forma_pago" type="text" value="" name="forma_pago" required readonly class="form-control field-input {{ $errors->has('forma_pago') ? 'field-error' : '' }}">
                            @if ($errors->has('forma_pago'))<span class="error-message">{{ $errors->first('forma_pago') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-4">
                        <label class="col-md-12" style="color: transparent;">.</label>
                        <div class="col-md-12">
                            <input id="iniciales" type="text" value="" name="iniciales" required readonly class="form-control field-input {{ $errors->has('iniciales') ? 'field-error' : '' }}">
                            @if ($errors->has('iniciales'))<span class="error-message">{{ $errors->first('iniciales') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-12"></div>
                    <div class="col-md-2"></div>
                    <div class="col-md-8">
                        <label class="col-md-12 control-label text-right" style="text-align: left;">Región</label>
                        <div class="col-md-12"  >
                            <input id="centro" type="text" value="" name="centro" required readonly class="form-control field-input {{ $errors->has('centro') ? 'field-error' : '' }}">
                            @if ($errors->has('centro'))<span class="error-message">{{ $errors->first('centro') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-12"></div>
                    <div class="col-md-2"></div>
                    <div class="col-md-4">
                        <label class="col-md-12 control-label" style="text-align: left;" for="w4-first-name">Desde: </label>
                        <div class="col-md-12">

                            <input type="date"  class="form-control field-input" name="fecha_desde" id="fecha_desde" readonly class="form-control field-input {{ $errors->has('fecha_desde') ? 'field-error' : '' }}"> 
                            
                            @if ($errors->has('fecha_desde'))<span class="error-message">{{ $errors->first('fecha_desde') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-4">
                        <label class="col-md-12 control-label"style="text-align: left;" for="w4-first-name">Hasta: </label>
                        <div class="col-md-12">

                            <input type="date"  class="form-control field-input" name="fecha_hasta" id="fecha_hasta" readonly class="form-control field-input {{ $errors->has('fecha_hasta') ? 'field-error' : '' }}"> 
                            
                            @if ($errors->has('fecha_hasta'))<span class="error-message">{{ $errors->first('fecha_hasta') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group"></div>
                    <div class="col-md-12">

                        <input type="hidden"  class="form-control field-input" name="codigo_referencia" id="codigo_referencia"  style="background: white" placeholder="dd/mm/yyyy" class="form-control field-input {{ $errors->has('codigo_referencia') ? 'field-error' : '' }}"> 

                        @if ($errors->has('codigo_referencia'))<span class="error-message">{{ $errors->first('codigo_referencia') }}</span>
                        @endif
                    </div>
                    <div class="col-lg-12 text-center" >    
                       <button type="submit" class="btn btn-primary">Aceptar</button>
                       <a href="{{route('home')}}" class="btn btn-default">Cancelar</a>
                   </div> 
                   <div class="form-group"></div>
               </form>

           </section>
           <section class="panel">
            <header class="panel-heading">
                <div class="panel-actions">
                        <a href="#" class="fa fa-caret-down"></a>
                    </div>
                <h2 class="panel-title">Liquidar de  CETIA</h2>
            </header>
            <div class="panel-body" style="max-height: 1000px; overflow: scroll; height: 250px">
                <table class="table table-striped mb-none">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Codigo</th>
                            <th>Nombre</th>
                            <th>Fecha</th>
                            <th>Fecha MOD</th>
                            <th class="col-md-2">Pagado</th>
                            <th class="col-md-2">Anulado</th>
                            <th>Acción</th>

                        </tr>
                    </thead>
                    <tbody id="cuerpo">
                    </tbody>
                </table>
                <table class="table table-striped mb-none">
                    <thead>
                        <tr>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th class="col-md-2"></th>
                            <th class="col-md-2"></th>
                            <th class="col-md-2"></th>
                            <th class="col-md-1"></th>
                        </tr>
                    </thead>
                    <tbody id="pagado">
                        
                    </tbody>
                    <tfoot>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td> <strong>Total</strong> </td>
                        <td><input type="text" name="totales" id="totales" readonly class="field-input form-control"></td>
                      
                        <td><input type="text" name="anulados" id="anulados" readonly class="field-input form-control"></td>

                    </tfoot>
                    
                </table>

                  
            </div>
        </section>
    </div>
</div>
<!-- end: page -->
</section>

@endsection

@section('js')

<script type="text/javascript">

    function anularReferencia(idProgramacion,referencia,anulado){
        var _token = $("input[name='_token']").val();
        if(anulado==0){
            var titulo="¿Esta seguro de anular viatico?";
            var mensaje="El viatico sera anulado";
        }else{
            var titulo="¿Esta seguro de habilitar viatico?";
            var mensaje="El viatico sera habilitado";
        }

        swal({
            title: titulo,
            text: mensaje,
            icon: "warning",
            buttons: ["Cancelar","Aceptar"],
        }).then(function(value){
            if(value){
                $.ajax({
                    url: "/viaticos/anular/"+idProgramacion+"/programacion",
                    type:'post',
                    data: {
                        referencia:referencia,
                        _token:_token,
                    },
                    success:function(data){   
                        swal({
                            title: data.mensajes.titulo,
                            text: data.mensajes.mensaje,
                            icon: "success",
                            timer: 2000,
                        }).then(function(value){
                        var arrayProgramacion = data.programacion;
                        var totalProgramacion = data.totales.total;
                        var totalAnulado = data.anulados.anulado;
                        var referencia = data.ref.referencia;
                        var pagado =data.pagado;
                    
                        cargarProgramacion(arrayProgramacion,totalProgramacion,referencia,totalAnulado,pagado);
                        });
                    }
                });
            }
        });
    }

    function cargarProgramacion(arrayProgramacion,totalProgramacion,referencia,totalAnulado,pagado){
        $("#cuerpo").html("");
        for(var i=0; i<arrayProgramacion.length; i++){

            var tr = `<tr>
            <td>`+arrayProgramacion[i].id+`</td>
            <td>`+arrayProgramacion[i].codigo_empleado+`</td>
            <td>`+arrayProgramacion[i].nombre+`</td>
            <td>`+arrayProgramacion[i].fecha_programacion+`</td>
            <td>`+arrayProgramacion[i].fecha_programacion+`</td>
            <td>$`+arrayProgramacion[i].pago_viatico+`.00</td>
            <td>$`+arrayProgramacion[i].anulado+`.00</td>
            <td> <a onclick="anularReferencia(`+arrayProgramacion[i].id+`,'`+referencia+`',`+arrayProgramacion[i].anulado+`);" class="btn btn-danger"><span class="fa fa-ban"></span></a>
            </td>

            </tr>`;
            $("#cuerpo").append(tr);
        }

         $("#pagado").html("");
        for(var i=0; i<pagado.length; i++){

            var tr = `<tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
           <td><strong>`+pagado[i].centro+`</strong></td>
            <td><strong>$`+pagado[i].pagado+`.00</strong></td>
            <td><strong>$`+pagado[i].anulado+`.00</strong></td>
            <td></td> 
            </tr>`;
            $("#pagado").append(tr);
        }

        $('#totales').val(totalProgramacion);
        $('#anulados').val(totalAnulado);
    }

    $(document).ready(function() {
        function cargarReferencia(){
            var referencia = $('#referencia').val();
            if($.trim('referencia') != ''){
                $.get('/viaticos/getReferencia', {referencia: referencia}, function (data){
                    $('#estado').val(data.viatico.estado);
                    $('#codigo_referencia').val(data.viatico.referencia);
                    $('#forma_pago').val(data.viatico.forma_pago);
                    $('#iniciales').val(data.viatico.iniciales);
                    $('#fecha_desde').val(data.viatico.fecha_desde);
                    $('#fecha_hasta').val(data.viatico.fecha_hasta);
                    $('#centro').val(data.viatico.centro);

                    var arrayProgramacion = data.programacion;
                    var totalProgramacion = data.totales.total;
                    var totalAnulado = data.anulados.anulado;
                    var referencia = data.viatico.referencia;
                    var pagado =data.pagado;
                  
                    cargarProgramacion(arrayProgramacion,totalProgramacion,referencia,totalAnulado,pagado);
                });
            }
        }
        $('#referencia').on('change', cargarReferencia);
    });



</script>


@endsection
