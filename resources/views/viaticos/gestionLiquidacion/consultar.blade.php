@extends('layouts.template')
@section('content')
<section role="main" class="content-body">
    <header class="page-header">
        <h2>Gestión de Liquidación de Viáticos</h2>
        <div class="right-wrapper pull-right">
            <ol class="breadcrumbs">
                <li style="margin-left: -25%"><a href="{{route('liquidaciones.listar')}}"><button class="btn btn-default"><span class="fa fa-chevron-left"> </span> Regresar </button></a></li>
            </ol>
        </div>
    </header>

    <!-- start: page -->
    <div class="row">
        <div class="col-lg-12">
            <div>
                @if ($errors->any())
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    
                    <div class="errors">
                        <ul>
                            @foreach ($errors->all() as $error)

                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif

                </div>
            </div>
            <section class="panel">
                <header class="panel-heading">
                    <div class="panel-actions">
                    </div>
                    <h2 class="panel-title">Liquidación de Viáticos</h2>
                </header>
                <div class="panel-body">

                    <div class="col-md-4">
                     <label class="col-md-12 control-label text-right" style="text-align: left;">N° Referencia RH</label>
                     <div class="col-md-12">
                        <input id="referencia" type="text" value="{{$viatico->referencia}}" readonly name="referencia" required readonly class="form-control field-input {{ $errors->has('referencia') ? 'field-error' : '' }}">
                        @if ($errors->has('referencia'))<span class="error-message">{{ $errors->first('referencia') }}</span>
                        @endif
                    </div>
                </div>
                <div class="col-md-4">
                    <label class="col-md-12 control-label text-right" style="text-align: left;">Cerrado en RH</label>
                    <div class="col-md-12">

                        <input id="estado" type="text" value="{{$estado}}" name="estado" required readonly class="form-control field-input {{ $errors->has('estado') ? 'field-error' : '' }}">

                        @if ($errors->has('estado'))<span class="error-message">{{ $errors->first('estado') }}</span>
                        @endif
                    </div>
                </div>
                <div class="col-md-4">
                    <label class="col-md-12 control-label text-right" style="text-align: left;">Forma Pago</label>
                    <div class="col-md-12"  >
                        <input id="forma_pago" type="text" value="{{$formaPago}}" name="forma_pago" required readonly class="form-control field-input {{ $errors->has('forma_pago') ? 'field-error' : '' }}">
                        @if ($errors->has('forma_pago'))<span class="error-message">{{ $errors->first('forma_pago') }}</span>
                        @endif
                    </div>
                </div>
                <div class="col-md-12"></div>
                <div class="col-md-4">
                    <label class="col-md-12 control-label text-right" style="text-align: left;">Región</label>
                    <div class="col-md-12"  >
                        <input id="centro" type="text" value="{{$centros}}" name="centro" required readonly class="form-control field-input {{ $errors->has('centro') ? 'field-error' : '' }}">
                        @if ($errors->has('centro'))<span class="error-message">{{ $errors->first('centro') }}</span>
                        @endif
                    </div>
                </div>
                <div class="col-md-4">
                    <label class="col-md-12 control-label" style="text-align: left;" for="w4-first-name">Desde: </label>
                    <div class="col-md-12">

                        <input type="date" value="{{$fechas->fecha_inicio}}" class="form-control field-input" name="fecha_desde" id="fecha_desde"    class="form-control field-input {{ $errors->has('fecha_desde') ? 'field-error' : '' }}" readonly> 

                        @if ($errors->has('fecha_desde'))<span class="error-message">{{ $errors->first('fecha_desde') }}</span>
                        @endif
                    </div>
                </div>
                <div class="col-md-4">
                    <label class="col-md-12 control-label"style="text-align: left;" for="w4-first-name">Hasta: </label>
                    <div class="col-md-12">

                        <input type="date" value="{{$fechas->fecha_final}}" class="form-control field-input" name="fecha_hasta" id="fecha_hasta"    class="form-control field-input {{ $errors->has('fecha_hasta') ? 'field-error' : '' }}" readonly> 

                        @if ($errors->has('fecha_hasta'))<span class="error-message">{{ $errors->first('fecha_hasta') }}</span>
                        @endif
                    </div>
                </div>                   

                <div class="form-group"></div>
                <div class="col-md-12"></div>
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <label class="col-md-12 control-label text-right" for="w4-first-name" style="text-align: left">N° Referencia UFI: </label>
                </div>
                <div class="col-md-12"></div>
                <div class="col-md-2"></div>
                <div class="col-md-6">
                    <div class="col-md-12">
                        <input type="text" readonly name="referencia_ufi" value="{{$viatico->referencia_ufi}}" class="form-control field-input" readonly id="referencia_ufi" required>
                    </div>
                 
             </div>
             <div class="col-md-12"></div>
             <div class="col-md-2"></div>
             <div class="col-md-3 ">
                <label class="col-md-12 control-label text-right" style="text-align: left;">N° Cheque</label>
                <div class="col-md-12"  >
                    <input id="cheque" type="text" value="{{$viatico->cheque}}" name="cheque" autocomplete=""  placeholder="" readonly class="form-control field-input {{ $errors->has('cheque') ? 'field-error' : '' }}">
                    @if ($errors->has('cheque'))<span class="error-message">{{ $errors->first('cheque') }}</span>
                    @endif
                </div>
            </div>
            <div class="col-md-3">
                <label class="col-md-12 control-label" style="text-align: left;" >Fecha Cheque</label>
                <div class="col-md-12">

                    <input type="date" value="{{$viatico->fecha_cheque}}" class="form-control field-input" name="fecha_cheque" id="fecha_cheque" readonly style="background: white" placeholder="dd/mm/yyyy" class="form-control field-input {{ $errors->has('fecha_cheque') ? 'field-error' : '' }}"> 

                    @if ($errors->has('fecha_cheque'))<span class="error-message">{{ $errors->first('fecha_cheque') }}</span>
                    @endif
                </div>
            </div>

            <div class="col-md-12"></div>
            <div class="col-md-2"></div>
            <div class="col-md-3 ">
                <label class="col-md-12 control-label text-right" style="text-align: left;">Cantidad a Remesar</label>
                <div class="col-md-12"  >
                    <input id="cantidad_remesa" readonly type="number" value="{{$viatico->cantidad_remesa}}" name="cantidad_remesa" autocomplete="" autofocus placeholder="" class="form-control field-input {{ $errors->has('cantidad_remesa') ? 'field-error' : '' }}">
                    @if ($errors->has('cantidad_remesa'))<span class="error-message">{{ $errors->first('cantidad_remesa') }}</span>
                    @endif
                </div>
            </div>
            <div class="col-md-3">
                <label class="col-md-12 control-label" style="text-align: left;" >Fecha Remesa</label>
                <div class="col-md-12">

                    <input type="date" readonly value="{{$viatico->fecha_remesa}}" class="form-control field-input" name="fecha_remesa" id="fecha_remesa"  style="background: white" placeholder="dd/mm/yyyy" class="form-control field-input {{ $errors->has('fecha_remesa') ? 'field-error' : '' }}"> 

                    @if ($errors->has('fecha_remesa'))<span class="error-message">{{ $errors->first('fecha_remesa') }}</span>
                    @endif
                </div>
            </div>
            <div class="col-md-3">
                <label class="col-md-12 control-label text-right" style="text-align: left;">N° Folio</label>
                <div class="col-md-12"  >
                    <input id="folio" type="text" value="{{$viatico->folio}}" readonly name="folio" autocomplete="" autofocus placeholder="" class="form-control field-input {{ $errors->has('folio') ? 'field-error' : '' }}">
                    @if ($errors->has('folio'))<span class="error-message">{{ $errors->first('folio') }}</span>
                    @endif
                </div>
            </div>
            <div class="col-md-12"></div>
            <div class="col-md-2"></div>
            <div class="col-md-3 ">
                <label class="col-md-12 control-label text-right" style="text-align: left;">Cantidad a Remesar</label>
                <div class="col-md-12"  >
                    <input id="cantidad_remesa" readonly type="number" value="{{$viatico->cantidad_remesa_segundo}}" name="cantidad_remesa" autocomplete="" autofocus placeholder="" class="form-control field-input {{ $errors->has('cantidad_remesa') ? 'field-error' : '' }}">
                    @if ($errors->has('cantidad_remesa'))<span class="error-message">{{ $errors->first('cantidad_remesa') }}</span>
                    @endif
                </div>
            </div>
            <div class="col-md-3">
                <label class="col-md-12 control-label" style="text-align: left;" >Fecha Remesa</label>
                <div class="col-md-12">

                    <input type="date" readonly value="{{$viatico->fecha_remesa_segundo}}" class="form-control field-input" name="fecha_remesa" id="fecha_remesa"  style="background: white" placeholder="dd/mm/yyyy" class="form-control field-input {{ $errors->has('fecha_remesa') ? 'field-error' : '' }}"> 

                    @if ($errors->has('fecha_remesa'))<span class="error-message">{{ $errors->first('fecha_remesa') }}</span>
                    @endif
                </div>
            </div>
            <div class="col-md-3">
                <label class="col-md-12 control-label text-right" style="text-align: left;">N° Folio</label>
                <div class="col-md-12"  >
                    <input id="folio" type="text" value="{{$viatico->folio_segundo}}" readonly name="folio" autocomplete="" autofocus placeholder="" class="form-control field-input {{ $errors->has('folio') ? 'field-error' : '' }}">
                    @if ($errors->has('folio'))<span class="error-message">{{ $errors->first('folio') }}</span>
                    @endif
                </div>
            </div>
            <div class="col-md-12"></div>
            <div class="col-md-2"></div>
            <div class="col-md-8">
                <label class="col-md-12 control-label" style="text-align: left">Periodo</label>
            </div>
            <div class="col-md-12"></div>
            <div class="col-md-2"></div>
            <div class="col-md-3">
                <div class="col-md-12">
                    <input type="text" name="mes_periodo" value="{{$viatico->mes_liquidar}}" id="mes_periodo" readonly class="form-control field-input">
                </div>
            </div>
            <div class="col-md-3">
                <div class="col-md-12">
                    <input type="text" name="anio_periodo" id="anio_periodo" value="{{$viatico->anio_liquidar}}" readonly class="form-control field-input">
                </div>
            </div>

        </section>
        <section class="panel">
            <header class="panel-heading">
                <h2 class="panel-title">Liquidar de CETIA </h2>
            </header>
            <div class="panel-body" style="max-height: 1000px; overflow: scroll; height: 250px">
                <table class="table table-striped mb-none">
                    <thead>
                        <tr>
                            <th>Codigo</th>
                            <th>Nombre</th>
                            <th>Fecha</th>
                            <th>Fecha MOD</th>
                            <th>Pagado</th>
                            <th>Anulado</th>
                        </tr>
                    </thead>
                    <tbody id="cuerpo">
                     <tr>
                        @foreach($listadoViatico as $viatico)
                        <td>{{$viatico->codigo_empleado}}</td>
                        <td>{{$viatico->nombre}} {{$viatico->apellido}}</td>
                        <td>{{$viatico->fecha_programacion}}</td>
                        <td>{{$viatico->fecha_programacion}}</td>
                        @if($viatico->estado_programacion==1)
                        <td>${{$viatico->pago_viatico}}.00</td>
                        @else
                        <td>$0.00</td>
                        @endif
                        @if($viatico->estado_programacion==1)
                        <td>$0.00</td>
                        @else
                        <td>${{$viatico->pago_viatico}}.00</td>
                        @endif
                    </tr>
                    @endforeach

                </tbody>
                <tfoot>
                 <td colspan="4" class="text-right">Total</td>
                 <td>${{$totalPago->pago}}.00</td>
                 <td>${{$totalRemesa->remesa}}.00</td>
             </tfoot>
         </table>
     </div>
 </section>
</div>
</div>
<!-- end: page -->
</section>

@endsection
