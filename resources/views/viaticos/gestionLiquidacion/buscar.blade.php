@extends('layouts.template')
@section('content')
<section role="main" class="content-body">
  <header class="page-header">
    <h2>Liquidación de Viáticos</h2>
    <div class="right-wrapper pull-right">
            <ol class="breadcrumbs">
                <li style="margin-left: -25%"><a href="{{route('liquidaciones.listar')}}"><button class="btn btn-default"><span class="fa fa-chevron-left"> </span> Regresar </button></a></li>
            </ol>
        </div>
  </header>
  <!-- start: page -->
  <div class="row">
    <div class="col-lg-12">

      @if ($errors->any())
      <div class="alert alert-danger">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

        <div class="errors">
          <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
          </ul>
        </div>
        @endif

      </div>
    </div>

    <section class="panel">
      @include('flash::message') 
      <header class="panel-heading">
        <h2 class="panel-title">Reporte de Liquidación de Viaticos</h2>
      </header>
      <div class="row">
        <div class="col-md-12">
         <div class="panel-body">

           <form class="form-horizontal" method="POST" action="{{route('reporte.liquidacion')}}" target="_blank">
            {{ csrf_field() }} 
           
            <div class="col-md-6">
             <label class="col-md-12 control-label text-right" style="text-align: left;">N° Referencia RH</label>
             <div class="col-md-12">

              <select data-plugin-selectTwo id="referencia" class="form-control populate" name="referencia" required class="form-control field-input {{ $errors->has('referencia') ? 'field-error' : '' }}">

                <option value="" selected>Seleccione Referencia</option>
                @foreach($viatico as $viatico)
                <option value="{{$viatico->referencia}}">{{$viatico->referencia}}</option>
                @endforeach

              </select>

              @if ($errors->has('referencia'))<span class="error-message">{{ $errors->first('referencia') }}</span>
              @endif
            </div>
          </div>
          <div class="col-md-6">
          <label class="col-md-12 control-label text-right" style="text-align: left;">Estatus</label>
          <div class="col-md-12">

            <select data-plugin-selectTwo id="estado" class="form-control populate" name="estado" class="form-control field-input {{ $errors->has('referencia') ? 'field-error' : '' }}" required>

              <option value="" selected>Seleccione Estatus</option>
              <option value="1">Pagado</option>
              <option value="0">Anulado</option>
            </select>

            @if ($errors->has('estado'))<span class="error-message">{{ $errors->first('estado') }}</span>
            @endif
          </div>
        </div>
          <div class="form-group"></div>
          <div class="col-md-4">
           <label class="col-md-12 control-label text-right" style="text-align: left;">N° Referencia RH</label>
           <div class="col-md-12">

            <input id="numero_referencia" type="text" value="" name="numero_referencia" required readonly class="form-control field-input {{ $errors->has('numero_referencia') ? 'field-error' : '' }}">
            @if ($errors->has('numero_referencia'))<span class="error-message">{{ $errors->first('numero_referencia') }}</span>
            @endif

            @if ($errors->has('numero_referencia'))<span class="error-message">{{ $errors->first('numero_referencia') }}</span>
            @endif
          </div>
        </div>
        <div class="col-md-4">
          <label class="col-md-12 control-label text-right" style="text-align: left;">Forma Pago</label>
          <div class="col-md-12"  >
            <input id="forma_pago" type="text" value="" name="forma_pago" required readonly class="form-control field-input {{ $errors->has('forma_pago') ? 'field-error' : '' }}">
            @if ($errors->has('forma_pago'))<span class="error-message">{{ $errors->first('forma_pago') }}</span>
            @endif
          </div>
        </div>
        <div class="col-md-4">
          <label class="col-md-12 control-label text-right" style="text-align: left;">Periodo</label>
          <div class="col-md-12"  >
            <input id="periodo" type="text" value="" name="periodo" required readonly class="form-control field-input {{ $errors->has('forma_pago') ? 'field-error' : '' }}">
            @if ($errors->has('periodo'))<span class="error-message">{{ $errors->first('periodo') }}</span>
            @endif
          </div>
        </div>
        <div class="col-md-12"></div>
        <div class="col-md-4">
          <label class="col-md-12 control-label text-right" style="text-align: left;">CETIA</label>
          <div class="col-md-12"  >
            <input id="centro" type="text" value="" name="centro" required readonly class="form-control field-input {{ $errors->has('centro') ? 'field-error' : '' }}">
            @if ($errors->has('centro'))<span class="error-message">{{ $errors->first('centro') }}</span>
            @endif
          </div>
        </div>
        <div class="col-md-4 ">
          <label class="col-md-12 control-label text-right" style="text-align: left;">N° Cheque</label>
          <div class="col-md-12"  >
            <input id="cheque" type="text" value="" name="cheque" autocomplete=""  placeholder="" class="form-control field-input {{ $errors->has('cheque') ? 'field-error' : '' }}" readonly>
            @if ($errors->has('cheque'))<span class="error-message">{{ $errors->first('cheque') }}</span>
            @endif
          </div>
        </div>
        <div class="col-md-4 ">
        <label class="col-md-12 control-label text-right" style="text-align: left;">Cantidad a Remesar</label>
        <div class="col-md-12"  >
          <input id="cantidad_remesa" type="text" value="" name="cantidad_remesa" autocomplete="" autofocus placeholder="" class="form-control field-input {{ $errors->has('cantidad_remesa') ? 'field-error' : '' }}" readonly >
          @if ($errors->has('cantidad_remesa'))<span class="error-message">{{ $errors->first('cantidad_remesa') }}</span>
          @endif
        </div>
      </div>
             
      <div class="col-md-12"></div>     
      <div class="col-md-4">
         <label class="col-md-12 control-label text-right" style="text-align: left;">Liquidada</label>
         <div class="col-md-12"  >
          <input id="pagado" type="text" value="" name="pagado" required readonly class="form-control field-input {{ $errors->has('pagado') ? 'field-error' : '' }}">
          @if ($errors->has('pagado'))<span class="error-message">{{ $errors->first('pagado') }}</span>
          @endif
        </div>
      </div> 
      <div class="col-md-4">
        <label class="col-md-12 control-label" style="text-align: left;" >Fecha Cheque</label>
        <div class="col-md-12">

          <input type="date"  class="form-control field-input" name="fecha_cheque" id="fecha_cheque"   placeholder="dd/mm/yyyy" class="form-control field-input {{ $errors->has('fecha_cheque') ? 'field-error' : '' }}" readonly> 

          @if ($errors->has('fecha_cheque'))<span class="error-message">{{ $errors->first('fecha_cheque') }}</span>
          @endif
        </div>
      </div>
      
      <div class="col-md-4">
        <label class="col-md-12 control-label text-right" style="text-align: left;">N° Folio</label>
        <div class="col-md-12"  >
          <input id="folio" type="text" value="" name="folio" autocomplete="" autofocus placeholder="" class="form-control field-input {{ $errors->has('folio') ? 'field-error' : '' }}" readonly>
          @if ($errors->has('folio'))<span class="error-message">{{ $errors->first('folio') }}</span>
          @endif
        </div>
      </div>
      <div class="form-group"></div>
      
      <div class="form-group" align="center">
        <button type="submit" class="btn btn-success" name="filp80" value="1"> Aceptar</button>
        <button type="submit" class="btn btn-default" name="filp27" value="2"> FIPL 27</button>
        <button type="submit" class="btn btn-primary" name="filp28" value="3"> FIPL 28</button>
      </div>

  </form> 

</div>

</div>
</div>

</section>



</div>
</div>

<!-- end: page -->
</section>

@endsection
@section('js')

<script type="text/javascript">

    $(document).ready(function() {

            function cargarReferencia(){
                var referencia = $('#referencia').val();
                if($.trim('referencia') != ''){
                    $.get('/viaticos/obtenerReferencia', {referencia: referencia}, function (data){
                       
                        $('#estado').val(data.estado);
                        $('#numero_referencia').val(data.referencia);
                        $('#forma_pago').val(data.forma_pago);
                        $('#periodo').val(data.periodo);
                        $('#centro').val(data.centro);
                        $('#cheque').val(data.cheque);
                        $('#pagado').val(data.estado);
                        $('#cantidad_remesa').val(data.cantidad_remesa);
                        $('#fecha_cheque').val(data.fecha_cheque);
                        $('#folio').val(data.folio);
                       
                    });


                }
            }
            $('#referencia').on('change', cargarReferencia);
    });

</script>


@endsection
