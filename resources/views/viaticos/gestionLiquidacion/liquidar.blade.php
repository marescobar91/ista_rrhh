@extends('layouts.template')
@section('content')
<section role="main" class="content-body">
    <header class="page-header">
        <h2>Gestión de Liquidación de Viáticos</h2>
        <div class="right-wrapper pull-right">
            <ol class="breadcrumbs">
                <li style="margin-left: -25%"><a href="{{ route('liquidaciones.crear') }}"><button class="btn btn-default"><span class="fa fa-chevron-left"> </span> Regresar </button></a></li>
            </ol>
        </div>
    </header>

    <!-- start: page -->
    <div class="row">
        <div class="col-lg-12">
            <div>
                @if ($errors->any())
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    
                    <div class="errors">
                        <ul>
                            @foreach ($errors->all() as $error)

                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif

                </div>
            </div>
            <section class="panel">
                <header class="panel-heading">
                    <div class="panel-actions">
                        <a href="#" class="fa fa-caret-down"></a>
                    </div>
                    <h2 class="panel-title">Liquidación de Viáticos</h2>
                </header>
                <div class="panel-body">

                    <form class="form-horizontal form-bordered" method="POST" action="{{ route('liquidaciones.guardar') }}">
                        @csrf                        
                        <div class="col-md-4">
                         <label class="col-md-12 control-label text-right" style="text-align: left;">N° Referencia RH</label>
                         <div class="col-md-12">
                            <input id="referencia" type="text" value="{{$referencia}}" name="referencia" required readonly class="form-control field-input {{ $errors->has('referencia') ? 'field-error' : '' }}">
                            @if ($errors->has('referencia'))<span class="error-message">{{ $errors->first('referencia') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-4">
                        <label class="col-md-12 control-label text-right" style="text-align: left;">Cerrado en RH</label>
                        <div class="col-md-12">
                            <input id="estado" type="text" value="{{$estado}}" name="estado" required readonly class="form-control field-input {{ $errors->has('estado') ? 'field-error' : '' }}">
                            @if ($errors->has('estado'))<span class="error-message">{{ $errors->first('estado') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-4">
                        <label class="col-md-12 control-label text-right" style="text-align: left;">Forma Pago</label>
                        <div class="col-md-12"  >
                            <input id="forma_pago" type="text" value="{{$formaPago}}" name="forma_pago" required readonly class="form-control field-input {{ $errors->has('forma_pago') ? 'field-error' : '' }}">
                            @if ($errors->has('forma_pago'))<span class="error-message">{{ $errors->first('forma_pago') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-12"></div>
                    <div class="col-md-4">
                        <label class="col-md-12 control-label text-right" style="text-align: left;">Región</label>
                        <div class="col-md-12"  >
                            <input id="centro" type="text" value="{{$centro}}" name="centro" required readonly class="form-control field-input {{ $errors->has('centro') ? 'field-error' : '' }}">
                            @if ($errors->has('centro'))<span class="error-message">{{ $errors->first('centro') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-4">
                        <label class="col-md-12 control-label" style="text-align: left;" for="w4-first-name">Desde: </label>
                        <div class="col-md-12">

                            <input type="date" value="{{$fecha_inicio}}" class="form-control field-input" name="fecha_desde" id="fecha_desde"    class="form-control field-input {{ $errors->has('fecha_desde') ? 'field-error' : '' }}" readonly> 
                            
                            @if ($errors->has('fecha_desde'))<span class="error-message">{{ $errors->first('fecha_desde') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-4">
                        <label class="col-md-12 control-label"style="text-align: left;" for="w4-first-name">Hasta: </label>
                        <div class="col-md-12">

                            <input type="date" value="{{$fecha_final}}" class="form-control field-input" name="fecha_hasta" id="fecha_hasta"    class="form-control field-input {{ $errors->has('fecha_hasta') ? 'field-error' : '' }}" readonly> 
                            
                            @if ($errors->has('fecha_hasta'))<span class="error-message">{{ $errors->first('fecha_hasta') }}</span>
                            @endif
                        </div>
                    </div>                   
                    
                    <div class="form-group"></div>
                    <div class="col-md-12"></div>
                    <div class="col-md-2"></div>
                    <div class="col-md-8">
                        <label class="col-md-12 control-label text-right" style="text-align: left">N° Referencia UFI: </label>
                    </div>
                    <div class="col-md-12"></div>
                    <div class="col-md-2"></div>
                    <div class="col-md-2">
                        <div class="col-md-12">
                            <input autofocus type="text" name="abreviatura" value="LVRCO" class="form-control field-input" id="abreviatura">
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="col-md-12"> <input type="number" name="" onchange="procesar();" class="form-control field-input" id="num1" minlength="4" maxlength="4" required min="0"></div>

                    </div>
                    <div class="col-md-2">
                        <div class="col-md-12"><input type="text" name="" class="form-control field-input" id="anio" value="{{$anio}}" required> </div>

                    </div>
                    <div class="col-md-3">
                        <div class="col-md-12"><input type="text" name="referencia_ufi" class="form-control field-input" readonly id="referencia_ufi" required></div>

                    </div>
                    <div class="col-md-12"></div>
                    <div class="col-md-2"></div>
                    <div class="col-md-3 ">
                        <label class="col-md-12 control-label text-right" style="text-align: left;">N° Cheque</label>
                        <div class="col-md-12"  >
                            <input id="cheque" type="text" value="" name="cheque" autocomplete=""  placeholder="" class="form-control field-input {{ $errors->has('cheque') ? 'field-error' : '' }}" required>
                            @if ($errors->has('cheque'))<span class="error-message">{{ $errors->first('cheque') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-3">
                        <label class="col-md-12 control-label" style="text-align: left;" >Fecha Cheque</label>
                        <div class="col-md-12">

                            <input type="date"  class="form-control field-input" name="fecha_cheque" value="" id="fecha_cheque"  style="background: white" placeholder="dd/mm/yyyy" class="form-control field-input {{ $errors->has('fecha_cheque') ? 'field-error' : '' }}" required> 

                            @if ($errors->has('fecha_cheque'))<span class="error-message">{{ $errors->first('fecha_cheque') }}</span>
                            @endif
                        </div>
                    </div>
                    
                    <div class="col-md-12"></div>
                    <div class="col-md-2"></div>
                    <div class="col-md-3 ">
                        <label class="col-md-12 control-label text-right" style="text-align: left;">Cantidad a Remesar</label>
                        <div class="col-md-12"  >
                            <input id="cantidad_remesa_uno" type="number" min="0.0" value="{{$remesa}}" name="cantidad_remesa_uno" required autocomplete="" autofocus placeholder="" class="form-control field-input {{ $errors->has('cantidad_remesa_uno') ? 'field-error' : '' }}">
                            @if ($errors->has('cantidad_remesa_uno'))<span class="error-message">{{ $errors->first('cantidad_remesa_uno') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-3">
                        <label class="col-md-12 control-label" style="text-align: left;" >Fecha Remesa</label>
                        <div class="col-md-12">

                            <input type="date"  class="form-control field-input" value="" name="fecha_remesa_uno" id="fecha_remesa_uno"  style="background: white" placeholder="dd/mm/yyyy" class="form-control field-input {{ $errors->has('fecha_remesa_uno') ? 'field-error' : '' }}"> 

                            @if ($errors->has('fecha_remesa_uno'))<span class="error-message">{{ $errors->first('fecha_remesa_uno') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-3">
                        <label class="col-md-12 control-label text-right" style="text-align: left;">N° Folio</label>
                        <div class="col-md-12"  >
                            <input id="folio_uno" type="text" value="" name="folio_uno" autocomplete="" autofocus placeholder="" class="form-control field-input {{ $errors->has('folio_uno') ? 'field-error' : '' }}">
                            @if ($errors->has('folio_uno'))<span class="error-message">{{ $errors->first('folio_uno') }}</span>
                            @endif
                        </div>
                    </div> 
                    <div class="col-md-12"></div>
                    <div class="col-md-2"></div>
                    <div class="col-md-3 ">
                        <label class="col-md-12 control-label text-right" style="text-align: left;">Cantidad a Remesar</label>
                        <div class="col-md-12"  >
                            <input id="cantidad_remesa_dos" type="number" value="0.0" name="cantidad_remesa_dos" autocomplete="" autofocus placeholder="" class="form-control field-input {{ $errors->has('cantidad_remesa_dos') ? 'field-error' : '' }}">
                            @if ($errors->has('cantidad_remesa_dos'))<span class="error-message">{{ $errors->first('cantidad_remesa_dos') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-3">
                        <label class="col-md-12 control-label" style="text-align: left;" >Fecha Remesa</label>
                        <div class="col-md-12">

                            <input type="date"  class="form-control field-input" name="fecha_remesa_dos" id="fecha_remesa_dos"  style="background: white" placeholder="dd/mm/yyyy" value="" class="form-control field-input {{ $errors->has('fecha_remesa_dos') ? 'field-error' : '' }}"> 

                            @if ($errors->has('fecha_remesa_dos'))<span class="error-message">{{ $errors->first('fecha_remesa_dos') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-3">
                        <label class="col-md-12 control-label text-right" style="text-align: left;">N° Folio</label>
                        <div class="col-md-12"  >
                            <input id="folio_dos" type="text" value="" name="folio_dos" autocomplete="" autofocus placeholder="" class="form-control field-input {{ $errors->has('folio_dos') ? 'field-error' : '' }}">
                            @if ($errors->has('folio_dos'))<span class="error-message">{{ $errors->first('folio_dos') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-12"></div>
                    <div class="col-md-2"></div>
                    <div class="col-md-8">
                        <label class="col-md-12 control-label" style="text-align: left">Periodo</label>
                    </div>
                    <div class="col-md-12"></div>
                    <div class="col-md-2"></div>
                    <div class="col-md-3">
                        <div class="col-md-12">
                            @if($periodo!= null)
                            <input type="text" name="mes_periodo" value="{{$periodo->mes}}" id="mes_periodo" readonly class="form-control field-input">
                            @else
                            <input type="text" name="mes_periodo" id="mes_periodo" readonly class="form-control field-input">
                            @endif
                        </div>

                    </div>
                    <div class="col-md-3">
                        <div class="col-md-12">@if($periodo!=null)
                         <input type="text" name="anio_periodo" value="{{$periodo->anio}}" id="anio_periodo" readonly class="form-control field-input">
                         @else
                         <input type="text" name="anio_periodo" id="anio_periodo" readonly class="form-control field-input">
                         @endif
                     </div>

                 </div>                    
                 <div class="form-group"></div> 

                 <div class="col-md-12 text-center" >
                    <button type="submit" class="btn btn-success">Liquidar</button>
                </div>
            </div>

        </form>
    </section>
    <section class="panel" style="max-height: 1000px; overflow: scroll; height: 250px">
        <header class="panel-heading">
            <div class="panel-actions">
                        <a href="#" class="fa fa-caret-down"></a>
                    </div>
            <h2 class="panel-title">Liquidar de {{$centro}}</h2>
        </header>
        <div class="panel-body">
            <table class="table table-striped mb-none">
                <thead>
                    <tr>
                        <th>Codigo</th>
                        <th>Nombre</th>
                        <th>Fecha</th>
                        <th>Fecha MOD</th>
                        <th class="col-md-2">Pagado</th>
                        <th class="col-md-2">Anulado</th>


                    </tr>
                </thead>
                <tbody id="cuerpo">
                    @foreach($programaciones as $programacion)
                    <tr class="gradeX" >
                        <td>{{$programacion->codigo_empleado}}</td>
                        <td>{{$programacion->nombre }} {{ $programacion->apellido}}</td>
                        <td>{{date('d-m-Y', strtotime($programacion->fecha_programacion))}}</td>
                        <td>{{date('d-m-Y', strtotime($programacion->fecha_programacion))}}</td>
                        @if($programacion->estado_programacion==1)
                        <td>${{$programacion->pago_viatico}}.00</td>
                        @elseif($programacion->estado_programacion==0)
                        <td>$00.00</td>
                        @endif
                        @if($programacion->estado_programacion==1)
                        <td>$00.00</td>

                        @elseif($programacion->estado_programacion==0)
                        <td>${{$programacion->pago_viatico}}.00</td>

                        @endif

                    </tr>
                    @endforeach
                </tbody>

            </table>


            <table class="table table-striped mb-none">
               <thead>
                   <tr>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th class="col-md-2"></th>
                    <th class="col-md-2"></th>
                </tr>
            </thead>
            <tbody>
                @foreach($detalleSubtotal as $key =>$pagado)
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>

                    <td class="text-right">{{$pagado['centro']}}</td>
                    <td >${{$pagado['pagado']}}.00</td>
                    <td >${{$pagado['anulado']}}.00</td>
                </tr>
                @endforeach
            </tbody>
            <tfoot>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td class="text-right">Total</td>
                    <td>${{$totalPago->pago}}.00</td>

                    <td>${{$remesa}}.00</td>


                </tr>
            </tfoot>
        </table>
    </div>
</section>


</div>
</div>
<!-- end: page -->
</section>

@endsection

@section('js')

<script type="text/javascript">

    function procesar() {

        abreviatura=document.getElementById('abreviatura').value;
        num1=document.getElementById('num1').value;
        anio=document.getElementById('anio').value;
        
        referencia_ufi=abreviatura+'-'+num1+'-'+anio;

        document.getElementById('referencia_ufi').value=referencia_ufi;
        if (referencia_ufi == null) {
           return false;
       }

       document.forms.ejemplo.submit();


   }

</script>


@endsection