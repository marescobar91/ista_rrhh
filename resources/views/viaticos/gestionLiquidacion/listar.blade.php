@extends('layouts.template')
@section('content')
<section role="main" class="content-body">
    <header class="page-header">
        <h2>Gesti&oacute;n de Liquidación de Viáticos</h2>
        <div class="right-wrapper pull-right">
            <ol class="breadcrumbs">
                <li style="margin-left: -25%"><a href="{{url('/')}}"><button class="btn btn-default"><span class="fa fa-chevron-left"> </span> Regresar </button></a></li>
            </ol>
        </div>
    </header>

    <!-- start: page -->
    <div class="row">
        <div class="col-lg-12">
            <div>
                @if ($errors->any())
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    
                    <div class="errors">
                        <ul>
                            @foreach ($errors->all() as $error)

                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif

                </div>
            </div>
            <section class="panel">
             @include('flash::message') 
             <header class="panel-heading">
                 <div class="panel-actions">
                     <a href="{{route('liquidacion.buscar')}}"><button class="btn btn-info"><span class="fa fa-file-text-o"> </span> Reportes</button></a>

                     @can('crear.liquidacion')
                     <a href="{{route('liquidaciones.crear')}}"><button class="btn btn-primary"><span class="fa fa-plus"></span> Agregar Liquidación </button></a>
                     @endcan
                     @can('consultar.periodo')
                     <a href="{{route('periodos.listar')}}"><button class="btn btn-success"><span class="fa fa-plus"></span> Activar Periodo</button></a>
                     @endcan 
                 </div>
                 <h2 class="panel-title">Listado de Liquidaciones</h2>
             </header>
             <div class="row">
                <div class="col-md-12">

                 <div class="panel-body">
                    <table class="table table-bordered table-striped mb-none" id="datatable-default">
                        <thead>
                            <tr>

                                <th >CETIA</th>
                                <th> Referencia</th>
                                <th>Referencia UFI</th>
                                <th>Fecha Desde</th>
                                <th>Fecha Hasta</th>
                                <th>Pago</th>
                                <th>Estado</th>
                                <th>Accion</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($viatico as $viatico)
                            <tr>
                                <td>{{$viatico->centro}}</td>

                                <td>{{$viatico->referencia}}</td>
                                <td>{{ $viatico->referencia_ufi }}</td>
                                <td>{{date('d-m-Y', strtotime($viatico->fecha_inicio))}}</td>
                                <td>{{date('d-m-Y', strtotime($viatico->fecha_final))}}</td>
                                <td>${{ $viatico->pago}}.00</td>
                                @if($viatico->estado == 2 && $viatico->referencia_ufi == null) 
                                <td>
                                    <form action="{{route('liquidado.cerrado',$viatico->id_viatico)}}" method="post">
                                      {{ csrf_field() }}
                                      <button class="btn btn-danger" title="Cerrado" disabled><span class="fa fa-circle-o"></span> </button>
                                  </form>
                              </td>
                                @elseif($viatico->estado == 2 && $viatico->referencia_ufi != null) 
                                <td>
                                    <form action="{{route('liquidado.cerrado',$viatico->id_viatico)}}" method="post">
                                      {{ csrf_field() }}
                                      <button class="btn btn-danger" title="Cerrado"><span class="fa fa-circle-o"></span> </button>
                                  </form>
                              </td>
                              @elseif($viatico->estado == 3)
                              <td>
                                  <form action="{{route('liquidado.liquidado', $viatico->id_viatico)}}" method="post">
                                    {{ csrf_field() }}  
                                    <button class="btn btn-success" title="Liquidado"><span class="fa fa-dollar"></span> </button>
                                </form>
                            </td>
                            @endif
                            <td><a href="{{ route('liquidaciones.mostrar', $viatico->referencia) }}" class="btn btn-info" ><span class="fa fa-eye"></span></a></td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</section>
</div>
</div>
<!-- end: page -->
</section>

@endsection
