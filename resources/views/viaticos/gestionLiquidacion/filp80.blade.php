<!DOCTYPE html>

<head>
  <title>ISTA - RRHH</title>
  <link href="css/style.css" rel="stylesheet">
  <style>
    .celda{
            border-bottom: lightgrey 0.1em solid;
        }
        .celdaa{
            border-bottom: lightgrey 0.1em solid;
        }
    .encabezado{
            border-bottom: grey 0.1em solid;
            background-color: grey;
            color: white;
        }
    .footer{
     position: fixed; left: 0px; bottom: -120px; right: 0px; height: 150px; 
   }
   .font{
    text-align: center;
    font-size: 11.3px;
    font-family: sans-serif;
  }
</style>
</head>
<body>
  <div id="wrapper">
    <!-- Page Content -->
    <div id="page-content-wrapper">
      <div class="container-fluid">
        <div class="container" style="margin-left: 13px">
          <div>
            <div class="table-title" style="background-color: #fff; margin-bottom: 1%">
              <div class="row">
                 <table>
                <tr>
                  <td>
                    <div style="width: 170px; margin-top: -25px">
                      <div class="row" align="center">
                        <img src="{{public_path().'/img/Logo-Institucional.svg'}}" width="50%" align="left" style="padding-top: -20px">
                      </div> 
                    </div>
                  </td>
                  <td >
                    <div style="width: 550px;margin-left: -150px;margin-top: 20px">
                      <h5 style="text-transform: uppercase; margin-bottom:-2%;margin-top: -20px" align="center">Instituto Salvadoreño de Transformación Agraria</h5>
                      <h5 style="text-transform: uppercase; margin-bottom:-2%;margin-left: 20%" align="center">Referencia {{$referencia}}</h5>
                      <h5 style="text-transform: uppercase; margin-bottom:-2%;padding-left: 80px" align="center">periodo del {{date('d-m-Y', strtotime($fecha_inicio))}} al {{date('d-m-Y', strtotime($fecha_final))}}</h5>
                      <h5 style="text-transform: uppercase; margin-bottom:-2%; padding-left: 100px" align="center">N° Liquidación: {{$referencia_ufi}}</h5>
                    </div>
                  </td>
                  <td>
                    <div style="width: 120px;" align="center">
                      @if($estado == 1)
                        <h5 style="margin-bottom: -2%" >FIPL - 80 R_0</h5>
                        @elseif($estado==0)
                        <h5 style="margin-bottom: -2%" >FIPL - 79 R_0</h5>
                        @endif
                        <h5 style="margin-bottom: -2%" >Fecha:{{$dia}}</h5> 
                        <h5 >Hora: {{$hora}}</h5>
                    </div>
                  </td>
                </tr>
              </table>
              </div>
            </div>
            <div class="row">
              <p>{{$forma_pago}}</p>
              <table class="table table-striped table-hover col-md-5" id="reporte" style="background: #fff;" width="97%"  >
                <thead class="font">
                  <tr class="encabezado">
                    <th>CODIGO</th>
                    <th>NOMBRE DEL EMPLEADO</th>
                    <th>FECHA SALIDA</th>
                    <th>VALOR</th>
                  </tr>
                </thead>
                @foreach($filp80 as $filp80)
                <tbody class="font">
                  <tr>
                    <td class="celda">{{$filp80->codigo}}</td>
                    <td class="celda">{{$filp80->nombre}} {{$filp80->apellido}}</td>
                    <td class="celda">{{date('d-m-Y', strtotime($filp80->fecha))}}</td>
                    <td class="celda">${{$filp80->valor}}.00</td>
                  </tr>
                </tbody>
              @endforeach
            </table>
             @if($estado==1)
            <table class="table table-striped table-hover col-md-5" id="reporte" style="background: #fff;" width="97%" >
              <tbody class="font">
                
                <tr>

                  <td colspan="3"  class="celda" align="right"><strong>Total Pagados</strong> </td>
                  <td class="celda">${{$totalPagados->monto}}.00</td>
                </tr>
               
               <tr>
                  <td colspan="3"  class="celda" align="right"><strong>Total Anulados</strong> </td>
                  <td class="celda">${{$totalAnulados->monto}}.00</td>
                </tr>
              
                <tr>
                  <td colspan="3"  class="celda" align="right"><strong>Total Viaticos Programados </strong> </td>
                  <td class="celda">${{$totalPagados->monto}}.00</td>
                </tr>
                
              </tbody>
            </table>
              @endif

               @if($estado==0)
            <table class="table table-striped table-hover col-md-5" id="reporte" style="background: #fff;" width="97%" >
              <tbody class="font">
               
               <tr>
                  <td colspan="3"  class="celda" align="right"><strong>Total Viáticos Anulados</strong> </td>
                  <td class="celda">${{$totalAnulados->monto}}.00</td>
                </tr>
              
                <tr>
                  <td colspan="3"  class="celda" align="right"><strong>Total Anulados</strong> </td>
                  <td class="celda">${{$totalAnulados->monto}}.00</td>
                </tr>
                
              </tbody>
            </table>
              @endif
            <br/>
            <br/> 
            <p class="font"> PAGO Y ELABORO</p>
            <table class="font">
            <tr>
              <td width="350px">F)____________________________</td>
              <td width="350px">F)____________________________</td>
            </tr>
            <tr>
              <td>ELABORO</td>
              <td>AUTORIZO</td>
            </tr>
          </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
<footer style="margin-left: 20px; padding-top: 5%" class="font">
  <table>
    <tr>
      <td style="text-align: left; width: 60%">Sistema Informatico para el manejo de Marcaciones y Viaticos de la Gerencia de Recursos Humanos del ISTA</td>
      <td></td>
      <td></td>
    </tr>
  </table>
</footer>
</body>
</html>