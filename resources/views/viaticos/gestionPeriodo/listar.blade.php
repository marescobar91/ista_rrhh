@extends('layouts.template')
@section('content')
<section role="main" class="content-body">
    <header class="page-header">
        <h2>Gestión de Periodo para Pagos</h2>
        <div class="right-wrapper pull-right">
            <ol class="breadcrumbs">
                <li style="margin-left: -25%"><a href="{{route('liquidaciones.listar')}}"><button class="btn btn-default"><span class="fa fa-chevron-left"> </span> Regresar </button></a></li>
            </ol>
        </div>
    </header>
    <!-- start: page -->
    <div class="row">
        <div class="col-lg-12">
            <div>
                @if ($errors->any())
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    
                    <div class="errors">
                        <ul>
                            @foreach ($errors->all() as $error)

                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif

                </div>
            </div> 
            <section class="panel">

                <header class="panel-heading">
                    @can('crear.periodo')
                    <div class="panel-actions">
                        <a href="{{route('periodos.crear')}}"><button class="btn btn-primary"><span class="fa fa-plus"></span> Agregar Periodo </button></a>
                         
                                </div>
                            @endcan
                                <h2 class="panel-title">Listado de Periodos de Pagos</h2>
                            </header>
                            <div class="row">
                                <div class="col-md-12">
                                   
                                        <div class="tab-content">
                                            <div id="popular10" class="tab-pane active">
                                             <div class="panel-body">
                                                <table class="table table-bordered table-striped mb-none" id="datatable-default">
                                                    <thead>
                                                        <tr>
                                                            <th>Centro</th>
                                                            <th >Mes</th>
                                                            <th>Año</th>
                                                            <th>Activo</th>
                                                           

                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                      @foreach($periodo as $periodo)
                                                     <tr class="gradeX" >
                                                        <td>{{ $periodo->centro->centro }}</td>
                                                        <td>{{ $periodo->mes }}</td>
                                                        <td>{{ $periodo->anio }}</td>
                                                           <td class="text-center">
                                                        @if($periodo->estado==1)
                                                        @can('eliminar.periodo')
                                                            <a href="{{ route('periodos.desactivar', $periodo->id_periodo) }}" ><span class="fa fa-check fa-2x"></span></a>
                                                            @endif
                                                            @endcan
                                                            @can('habilitar.periodo')
                                                            @if($periodo->estado==0)
                                                            <a href="{{ route('periodos.activar', $periodo->id_periodo) }}"><span class="fa fa-times fa-2x"></span></a>
                                                            @endif
                                                            @endcan
                                                        </td>
                                              
                                            
                                                     
                                                    </tr>
                                          @endforeach

                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                   
                            </div>
                       
                    </div>
                </div>
            </section>
        </div>
    </div>
    <!-- end: page -->
</section>

@endsection
