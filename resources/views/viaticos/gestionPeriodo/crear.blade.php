@extends('layouts.template')
@section('content')
<section role="main" class="content-body">
    <header class="page-header">
        <h2>Gestión de Periodos de Pago</h2>
        <div class="right-wrapper pull-right">
            <ol class="breadcrumbs">
                <li style="margin-left: -25%"><a href="{{route('periodos.listar')}}"><button class="btn btn-default"><span class="fa fa-chevron-left"> </span> Regresar </button></a></li>
            </ol>
        </div>
    </header>

    <!-- start: page -->
    <div class="row">
        <div class="col-lg-12">
            <div>
                @if ($errors->any())
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    
                    <div class="errors">
                        <ul>
                            @foreach ($errors->all() as $error)

                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif

                </div>
            </div>
            <section class="panel">
                <header class="panel-heading">
                    <div class="panel-actions">
                    </div>
                    <h2 class="panel-title">Crear Periodo</h2>
                </header>
                <div class="panel-body">
                    <form class="form-horizontal form-bordered" method="POST" action="{{ route('periodos.guardar') }}">
                        @csrf                        
                        <div class="form-group">
                            <div class="col-md-2"></div>
                            <div class="col-md-8">
                                <label class="control-label col-md-12" style="text-align: left;">Centros</label>
                            
                                <select multiple data-plugin-selectTwo class="form-control populate" name="centros[]">
                                        @foreach ($centro as $centro)
                                        <option value="{{$centro->id_centro}}">{{$centro->centro}}</option>
                                        @endforeach
                                    </select>

                                @if ($errors->has('centros'))<span class="error-message">{{ $errors->first('centros') }}</span>
                                @endif
                           
                            </div>
                            
                        </div>

                        <div class="form-group">
                            <div class="col-md-2"></div>
                            <div class="col-md-8">
                                <label class="col-md-12 control-label" style="text-align: left;">Mes</label>
                                <div class="col-md-12">
                                <select data-plugin-selectTwo id="mes_periodo" class="form-control" name="mes_periodo" required>
                                    <option value="">Seleccione Uno</option>
                                    <option value="Enero">Enero</option>
                                    <option value="Febrero">Febrero</option>
                                    <option value="Marzo">Marzo</option>
                                    <option value="Abril">Abril</option>
                                    <option value="Mayo">Mayo</option>
                                    <option value="Junio">Junio</option>
                                    <option value="Julio">Julio</option>
                                    <option value="Agosto">Agosto</option>
                                    <option value="Septiembre">Septiembre</option>
                                    <option value="Octubre">Octubre</option>
                                    <option value="Noviembre">Noviembre</option>
                                    <option value="Diciembre">Diciembre</option>
                                </select> 
                                @if ($errors->has('mes_periodo'))<span class="error-message">{{ $errors->first('mes_periodo') }}</span>
                                @endif
                            </div>
                            </div>
                            
                        </div> 

                         <div class="form-group">
                            <div class="col-md-2"></div>
                            <div class="col-md-8">
                                <label class="col-md-12 control-label " style="text-align: left;">Año</label>
                            <div class="col-md-12">
                                <input type="number" min="0" name="anio_periodo" id="anio_periodo"  value="{{$anio}}"  required autocomplete="kilometros" autofocus placeholder="Año" class="form-control field-input {{ $errors->has('anio_periodo') ? 'field-error' : '' }}">
                                @if ($errors->has('anio_periodo'))<span class="error-message">{{ $errors->first('anio_periodo') }}</span>
                                @endif
                            </div>
                            </div>
                            
                        </div>
                        
                        <div class="col-lg-12 text-center" >    
                         <button type="submit" class="btn btn-primary">Guardar</button>
                         <a href="{{route('home')}}" class="btn btn-default">Cancelar</a>
                     </div>                              
                 </form>
             </div>
         </section>
         
     </div>
 </div>
 <!-- end: page -->
</section>

@endsection