<!doctype html>
<html class="fixed js flexbox flexboxlegacy csstransforms csstransforms3d no-overflowscrolling no-mobile-device custom-scroll sidebar-left-collapsed">

<!--fixed js flexbox flexboxlegacy csstransforms csstransforms3d no-overflowscrolling no-mobile-device custom-scroll para menu normal-->
<!--fixed js flexbox flexboxlegacy csstransforms csstransforms3d no-overflowscrolling no-mobile-device custom-scroll sidebar-left-collapsed      Para Menu minizado-->
<head>

	<!-- Basic -->
	<meta charset="UTF-8">

	<title>ISTA | Recursos Humanos</title>
	<meta name="keywords" content="HTML5 Admin Template" />
	<meta name="description" content="JSOFT Admin - Responsive HTML5 Template">
	<meta name="author" content="JSOFT.net">

	<!-- Mobile Metas -->
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

	<!-- Web Fonts  -->
	<link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800|Shadows+Into+Light" rel="stylesheet" type="text/css">

	<!-- Vendor CSS -->
	<link rel="stylesheet" href="{{asset('vendor/bootstrap/css/bootstrap.css')}}" />
	<link rel="stylesheet" href="{{asset('vendor/font-awesome/css/font-awesome.css')}}" />
	<link rel="stylesheet" href="{{asset('vendor/magnific-popup/magnific-popup.css')}}" />
	<link rel="stylesheet" href="{{asset('vendor/bootstrap-datepicker/css/datepicker3.css')}}" />
	<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
	<link rel="stylesheet" href="{{asset('vendor/bootstrap-colorpicker/css/bootstrap-colorpicker.css')}}" />

	<!-- Specific Page Vendor CSS -->
	<link rel="stylesheet" href="{{asset('vendor/jquery-ui/css/ui-lightness/jquery-ui-1.10.4.custom.css')}}" />
	<link rel="stylesheet" href="{{asset('vendor/bootstrap-multiselect/bootstrap-multiselect.css')}}" />
	<link rel="stylesheet" href="{{asset('vendor/morris/morris.css')}}" />
	<link rel="stylesheet" href="{{asset('vendor/pnotify/pnotify.custom.css')}}" />
	<!-- Theme CSS -->
	<link rel="stylesheet" href="{{asset('css/theme.css')}}" />
	<link rel="stylesheet" href="{{asset('css/personalization.css')}}" />

	<link rel="stylesheet" href="{{asset('css/multiselect.css')}}" />


	<!-- Skin CSS -->
	<link rel="stylesheet" href="{{asset('css/skins/default.css')}}" />

	<!-- Theme Custom CSS -->
	<link rel="stylesheet" href="{{asset('css/theme-custom.css')}}">

	<link rel="stylesheet" href="{{asset('vendor/select2/select2.css')}}" />
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.21/datatables.min.css"/>
	@yield('css')
	<!-- Head Libs -->
	<script src="{{asset('vendor/modernizr/modernizr.js')}}"></script>

</head>
<body>
	<section class="body">

		<!-- start: header -->

		<header class="header" >
			<div class="logo-container">
				<a href="#" class="logo">
					<img src="{{asset('img/Logo-Institucional.svg')}}" height="30" alt="JSOFT Admin" />
				</a>
				<div class="visible-xs toggle-sidebar-left" data-toggle-class="sidebar-left-opened" data-target="html" data-fire-event="sidebar-left-opened">
						<i class="fa fa-bars" aria-label="Toggle sidebar"></i>
					</div>
				
			</div>
			@auth
			<!-- start: search & user box -->
			<div class="header-right">	
				<span class="separator"></span>

				<div id="userbox" class="userbox">
					<a href="#" data-toggle="dropdown">
						<div class="profile-info">
							<span class="name"><b>{{ Auth::user()->nombre_usuario }}</b></span>
							
						</div>

						<i class="fa custom-caret"></i>
					</a>
					<div class="dropdown-menu">
						<ul class="list-unstyled">
							<li class="divider"></li>

							<li>
								<a role="menuitem" tabindex="-1" href="{{ route('cerrarSesion') }}" onclick="event.preventDefault();
								document.getElementById('logout-form').submit();"><i class="fa fa-power-off"></i> {{ __('Logout') }}</a>
								<form id="logout-form" action="{{ route('cerrarSesion') }}" method="POST" style="display: none;">
									@csrf
								</form>
							</li>
						</ul>
					</div>
				</div>
			</div>
			@endauth
			<!-- end: search & user box -->
		</header>
		<!-- end: header -->

		<div class="inner-wrapper">
			<!-- start: sidebar -->
			<aside id="sidebar-left" class="sidebar-left">

				<div class="sidebar-header">
					<div class="sidebar-title">
						Panel de Navegacion
					</div>
					<div class="sidebar-toggle hidden-xs" data-toggle-class="sidebar-left-collapsed" data-target="html" data-fire-event="sidebar-left-toggle">	
						<i class="fa fa-bars" aria-label="Toggle sidebar"></i>
					</div>
				</div>

				<div class="nano">
					<div class="nano-content">
						<nav id="menu" class="nav-main" role="navigation">
							<ul class="nav nav-main">
								<li class="nav-active">
									<a href="{{url('/')}}">
										<i class="fa fa-home" aria-hidden="true"></i>
										<span>Pagina Principal</span>
									</a>
								</li>
								@hasanyrole('Administrador|Gestor de Usuarios|Gestor de Claves|Gestor de Permiso')
								<li class="nav-parent">
									<a>
										<i class="fa fa-gear" aria-hidden="true"></i>
										<span>Administraci&oacute;n</span>
									</a>
									<ul class="nav nav-children">
										@can('consultar.usuarios')
										<li>
											<a href="{{route('usuarios.listar')}}">
												Gesti&oacute;n de Usuarios
											</a>
										</li>
										@endcan
										@can('consultar.roles')
										<li>
											<a href="{{route('roles.listar')}}">
												Gesti&oacute;n de Roles
											</a>
										</li>
										@endcan
										@can('consultar.permisos')
										<li>
											<a href="{{ route('permisos.listar') }}">
												Gesti&oacute;n de Permisos
											</a>
										</li>
										@endcan

										@can('consultar.catalogos')
										<li>
											<a href="{{route('admin.catalogos')}}">
												Gestión de Catalogos
											</a>
										</li>
										@endcan
									</ul>
								</li>
								@endhasanyrole
								@hasanyrole('Gestor de Empleados|Consultor de Empleados|Generar Reporte Empleados')
								<li class="nav-parent">
									<a>
										<i class="fa fa-user" aria-hidden="true"></i>
										<span>Gesti&oacute;n de Personal</span>
									</a>
									<ul class="nav nav-children">
										
										<li>
											<a href="{{route('empleados.listar')}}">
												<span>Gesti&oacute;n de Empleados</span></a>
											</li>
											
										</ul>
									</li>
									@endhasanyrole

									@hasanyrole('Administrador Marcaciones|Consultar de Marcaciones|Generar Reporte Marcaciones|Gestor de Marcaciones|Administrador Permisos|Consultor de Permisos|Generador Reporte Permisos|Gestor de Permisos|Administrador Horas Extras|Consultor de Horas Extras|Generar Reporte Horas Extras|Gestor de Horas Extras')
									<li class="nav-parent">
										<a>
											<i class="fa fa-check-square-o" aria-hidden="true"></i>
											<span>Marcaciones</span>
										</a>
										<ul class="nav nav-children">
											@can('consultar.asuetos')
											<li>
												<a href="{{route('asuetos.listar')}}">
													Gesti&oacute;n de Asuetos
												</a>
											</li>
											@endcan
											@can('listar.historial')										
											<li>
												<a href="{{route('historialM.listar')}}">
													Gestión de Marcaciones
												</a>
											</li>
											@endcan
											@can('consultar.permisos.empleado')
											<li>
												<a href="{{ route('permiso.empleado.listar') }}">
													Gesti&oacute;n de Permisos
												</a>
											</li> 
											@endcan
											@can('consultar.horas.extras')
											<li>
												<a href="{{ route('horas.extras.listar') }}">
													Gesti&oacute;n de Horas Extras
												</a>
											</li>
											@endcan
										</ul>
									</li>
									@endhasanyrole
									@hasanyrole('Administrador Viáticos|Gestor de Recursos Humanos|Administrador Regional|Secretaria|Administrador Liquidaciones|Gestor de Caja Chica|Gestor de Fondo Circulante')
									<li class="nav-parent">
										<a>
											<i class="fa fa-car" aria-hidden="true"></i>
											<span>Viáticos</span>
										</a>
										<ul class="nav nav-children">
											@can('consultar.programacion')
											<li> 
												<a href="{{route('programacion.listar')}}">
													Programación
												</a>
											</li>
											@endcan
											@can('buscar.programacion')
											<li> 
												<a href="{{route('busqueda.programacion')}}">
													Buscar Programaciones
												</a>
											</li>
											@endcan
											@can('crear.viatico')
											<li>
												<a href="{{route('nota.viaticos.crear')}}">
													Notas de Viaticos
												</a>
											</li>
											@endcan
											@can('buscar.informe')
											<li>
												<a href="{{route('viaticos.informes.buscar')}}">
													Informes de Cierre
												</a>
											</li>
											@endcan
											@can('consultar.liquidacion')
											
											<li>
												<a href="{{route('liquidaciones.listar')}}">
													Liquidaciones
												</a>
											</li>
											@endcan
											
											
										</ul>
									</li>
									@endhasanyrole
							</ul>
						</nav>

							<hr class="separator" />
							<hr class="separator" />

							<footer class="sidebar-widget widget-stats" style="padding-top: 55%">
								<div class="widget-header">
									<h6>Creado Por</h6>
									<div class="widget-toggle">+</div>
								</div>
								<div class="widget-content">
									<ul>
										<li>
											<span class="stats-title">Estudiantes de EISI-UES</span>
											<span class="stats-complete">07-2020</span>
										</li>
										<hr class="separator" />
										<li>
											<span class="stats-title">Instituto Salvadoreño de Transformación Agraria</span>
											<span class="stats-complete">ISTA</span>
										</li>
									</ul>
								</div>
							</footer>
						</div>

					</div>

				</aside>
				<!-- end: sidebar -->
				<!--Contenido de las paginas-->
				@include('sweetalert::alert')
				@yield('content')
				
				<!--Fin del contenido de las paginas-->
			</div>
		</section>

		<!-- Vendor -->

		<script src="{{asset('vendor/jquery/jquery.js')}}"></script>
		<script src="{{asset('vendor/jquery-browser-mobile/jquery.browser.mobile.js')}}"></script>
		<script src="{{asset('vendor/bootstrap/js/bootstrap.js')}}"></script>
		<script src="{{asset('vendor/nanoscroller/nanoscroller.js')}}"></script>
		<script src="{{asset('vendor/bootstrap-datepicker/js/bootstrap-datepicker.js')}}"></script>
		<script src="{{asset('vendor/magnific-popup/magnific-popup.js')}}"></script>
		<script src="{{asset('vendor/jquery-placeholder/jquery.placeholder.js')}}"></script>
		<script src="{{asset('vendor/bootstrap-colorpicker/js/bootstrap-colorpicker.js')}}"></script>

		<!-- Specific Page Vendor -->
		<script src="{{asset('vendor/jquery-ui/js/jquery-ui-1.10.4.custom.js')}}"></script>
		<script src="{{asset('vendor/jquery-ui-touch-punch/jquery.ui.touch-punch.js')}}"></script>
		<script src="{{asset('vendor/jquery-appear/jquery.appear.js')}}"></script>
		<script src="{{asset('vendor/bootstrap-multiselect/bootstrap-multiselect.js')}}"></script>
		<script src="{{asset('vendor/bootstrap-tagsinput/bootstrap-tagsinput.js')}}"></script>
		<script src="{{asset('vendor/jquery-maskedinput/jquery.maskedinput.js')}}"></script>
		<script src="{{asset('vendor/jquery-easypiechart/jquery.easypiechart.js')}}"></script>
		<script src="{{asset('vendor/flot/jquery.flot.js')}}"></script>
		<script src="{{asset('vendor/flot-tooltip/jquery.flot.tooltip.js')}}"></script>
		<script src="{{asset('vendor/flot/jquery.flot.pie.js')}}"></script>
		<script src="{{asset('vendor/flot/jquery.flot.categories.js')}}"></script>
		<script src="{{asset('vendor/flot/jquery.flot.resize.js')}}"></script>
		<script src="{{asset('vendor/jquery-sparkline/jquery.sparkline.js')}}"></script>
		<script src="{{asset('vendor/raphael/raphael.js')}}"></script>
		<script src="{{asset('vendor/morris/morris.js')}}"></script>
		<script src="{{asset('vendor/gauge/gauge.js')}}"></script>
		<script src="{{asset('vendor/snap-svg/snap.svg.js')}}"></script>
		<script src="{{asset('vendor/liquid-meter/liquid.meter.js')}}"></script>
		<script src="{{asset('vendor/jqvmap/jquery.vmap.js')}}"></script>
		<script src="{{asset('vendor/jquery-validation/jquery.validate.js')}}"></script>
		<script src="{{asset('vendor/bootstrap-wizard/jquery.bootstrap.wizard.js')}}"></script>
		<script src="{{asset('vendor/pnotify/pnotify.custom.js')}}"></script>

		<!-- Theme Base, Components and Settings -->
		<script src="{{asset('js/theme.js')}}"></script>

		<!-- Theme Custom -->
		<script src="{{asset('js/theme.custom.js')}}"></script>

		<!-- Theme Initialization Files -->
		<script src="{{asset('js/theme.init.js')}}"></script>

		<!--Sweet Alert
		<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>-->
		<script src="{{asset('js/sweetalert.js')}}"></script>

		<script src="{{asset('js/modal-confirmation.js')}}"></script>

		<!-- Examples -->
		<script src="{{asset('js/dashboard/examples.dashboard.js')}}"></script>
		<script src="{{asset('vendor/select2/select2.js')}}"></script>
		<script src="{{asset('vendor/jquery-datatables/media/js/jquery.dataTables.js')}}"></script>
		<script src="{{asset('vendor/jquery-datatables/extras/TableTools/js/dataTables.tableTools.min.js')}}"></script>
		<script src="{{asset('vendor/jquery-datatables-bs3/assets/js/datatables.js')}}"></script>
		<script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.21/datatables.min.js"></script>

		<!-- Examples -->
		<script src="{{asset('js/tables/examples.datatables.default.js')}}"></script>
		<script src="{{asset('js/tables/examples.datatables.row.with.details.js')}}"></script>
		<script src="{{asset('js/tables/examples.datatables.tabletools.js')}}"></script>
		<script src="{{asset('js/ui-elements/examples.modals.js')}}"></script>
		<script src="{{asset('js/espanol.js')}}"></script>
		<script src="{{asset('js/select2.js')}}"></script>
		<script src="{{asset('js/forms/examples.wizard.js')}}"></script>
		<script src="{{asset('js/ui-elements/examples.notifications.js')}}"></script>
		@yield('js')
		@yield('script')

	</body>
	</html>