@extends('layouts.template')
@section('content')
<section role="main" class="content-body" >
	<header class="page-header">
		<h2 class="text-light">Bienvenidos</h2>
		
		<div class="right-wrapper pull-right">
		</div>
	</header>
	<div class="row">
		<div class="col-md-12">
			<section class="panel">
				<header class="panel-heading">
					<div class="panel-actions">
					</div>
					<h2 class="panel-title">Proyecto ISTA-RRHH</h2>
				</header>
				<div class="panel-body">
					<div class="table-responsive text-center">
						<h1 class="text-center"> Sistema Informático para el manejo de Marcaciones y Viáticos de la Gerencia de Recursos Humanos del Instituto Salvadoreño de Transformación Agraria</h1>
						<img src="{{asset('img/logo-ista.jpg')}}" style="border-radius: 20px; margin-top: 3%;margin-bottom: 2%; width: 25%">
					</div>
				</div>
			</section>
		</div>
	</div>
</section>
@endsection