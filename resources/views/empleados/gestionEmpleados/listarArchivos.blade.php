@extends('layouts.template')
@section('content')
<section role="main" class="content-body">
    <header class="page-header">
        <h2>Gesti&oacute;n de Empleados</h2>
        <div class="right-wrapper pull-right">
            <ol class="breadcrumbs">
                <li style="margin-left: -25%"><a href="{{route('empleados.listar')}}"><button class="btn btn-default"><span class="fa fa-chevron-left"> </span> Regresar </button></a></li>
            </ol>
        </div>
    </header>

    <!-- start: page -->
    <div class="row">
        <div class="col-lg-12">
            <div>
                @if ($errors->any())
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    
                    <div class="errors">
                        <ul>
                            @foreach ($errors->all() as $error)

                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif

                </div>
            </div>
            <section class="panel">
                @include('flash::message')
                <header class="panel-heading">
                     <div class="panel-actions">
                        @can('agregar.documentos')
                    <a href="{{ route('documentos.agregar', $empleado->id_empleado) }}"><button class="btn btn-primary"><span class="fa fa-plus"></span> Agregar Documento </button></a>  
                    @endcan               
                </div>
                    <div class="panel-actions">
                        
                    </div>
                  
                    
                    <h2 class="panel-title">Listado de Documentos del Empleado {{ $empleado->nombre_completo }}</h2>
                </header>
                <div class="row">
                    <div class="col-md-12">
                        
                        <div class="tab-content">
                            <div id="popular10" class="tab-pane active">
                             <div class="panel-body">
                                
                                <table class="table table-bordered table-striped mb-none" id="datatable-default">
                                    <thead>

                                        <tr>
                                         
                                            <th>Nombre del Documento</th>
                                            <th>Descripci&oacute;n</th>
                                            <th>Extensión</th>
                                            <th class="col-md-2">Accion</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                     @foreach($documento as $documentos)
                                     <tr class="gradeX" >
                                        
                                        <td>{{$documentos->nombre_documento  }}</td>
                                        <td>{{ $documentos->descripcion }}</td>
                                        <td>.{{ $documentos->extension }}</td>
                                       <td class="text-center">
                                            @can('reemplazar.documentos')
                                            <a href="{{ route('documentos.editar', $documentos->id_documento) }}" class="btn btn-warning" ><span class="fa fa-pencil"></span> </a>
                                                @endcan
                                                @can('consultar.documentos')
                                                <a target="_blank" href="{{ route('documentos.mostrar', $documentos->id_documento) }} " class="btn btn-info" ><span class="fa fa-eye"></span></a>
                                                @endcan
                                                @can('eliminar.documentos')
                                                <a href="{{ route('documentos.eliminar', $documentos->id_documento) }}"  class="btn btn-danger eliminar"><span class="fa fa-trash-o"></span></a>
                                                @endcan
                                        </td>
                                        
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    
                </div>
                
            </div>
        </div>
        
    </section>
</div>
</div>
<!-- end: page -->
</section>

@endsection
