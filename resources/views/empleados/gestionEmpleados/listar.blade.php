@extends('layouts.template')
@section('css')
<link rel="stylesheet" href="https://cdn.datatables.net/fixedheader/3.1.6/css/fixedHeader.dataTables.min.css">
@endsection

@section('content')
<section role="main" class="content-body">
    <header class="page-header">
        <h2>Gesti&oacute;n de Empleados</h2>
        <div class="right-wrapper pull-right">
            <ol class="breadcrumbs">
                <li style="margin-left: -25%"><a href="{{url('/')}}"><button class="btn btn-default"><span class="fa fa-chevron-left"> </span> Regresar </button></a></li>
            </ol>
        </div>
    </header>

    <!-- start: page -->
    <div class="row">
        <div class="col-lg-12">
            <div>
                @if ($errors->any())
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                    <div class="errors">
                        <ul>
                            @foreach ($errors->all() as $error)

                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
 
                </div>
            </div>
            <section class="panel">
                @include('flash::message')
                @include('flash::message')
                <header class="panel-heading"> 

                 <div class="panel-actions">
                    <a href="{{route('listado.reporte')}}"><button class="btn btn-success"><span class="fa fa-file-pdf-o"></span>  Reportes </button></a>
                    <a href="{{route('empleados.crear')}}"><button class="btn btn-primary"><span class="fa fa-plus"></span> Agregar Empleado </button></a>
                                   
                </div>

                <h2 class="panel-title">Listado de Empleados</h2>
                <div class="form-group" style="padding-top: 2%">
                </div>

            </header>
            <div class="row">
                <div class="col-md-12">
                    <div class="tabs">
                        <ul class="nav nav-tabs nav-justified">
                            <li class="active">
                                <a href="#popular10" data-toggle="tab" class="text-center"><i class="fa fa-star"></i> Habilitados</a>
                            </li>
                            <li>
                                <a href="#recent10" data-toggle="tab" class="text-center">Deshabilitados</a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div id="popular10" class="tab-pane active"  style="overflow: scroll;">
                             <div class="panel-body">
                                <table class="table table-bordered table-striped mb-none" id="datatable-default">
                                    <thead>
                                        <tr>
                                            <th>C&oacute;digo</th>
                                            <th >Nombre Completo</th>
                                            <th>Unidad Administrativa</th>
                                            <th>Oficina</th> 
                                            <th >Accion</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($empleado as $empleados)
                                        <tr>
                                            <td class="col-md-2">{{ $empleados->codigo_empleado }}</td>
                                            <td class="col-md-3">{{ $empleados->nombre }} {{ $empleados->apellido }}</td>
                                            @if($empleados->oficina_id==null)
                                            <td></td>
                                            <td></td>
                                            @else
                                            <td class="col-md-2">{{ $empleados->oficina->unidadAdministrativa->unidad_administrativa }}</td>

                                            <td>{{$empleados->oficina->oficina}}</td>
                                            @endif
                                            <td class="col-md-3">
                                                @can('consultar.documentos')
                                                <a href="{{ route('documentos.listar', $empleados->id_empleado) }}" class="btn btn-success" ><span class="fa fa-file"></span></a>
                                                @endcan
                                                @can('consultar.empleados')
                                                <a href="{{ route('empleados.mostrar', $empleados->id_empleado) }}" class="btn btn-info" target="_blank" ><span class="fa fa-eye"></span></a>
                                                @endcan
                                                @can('editar.empleados')
                                                <a href="{{route('empleados.editar', $empleados->id_empleado)}}" class="btn btn-warning" ><span class="fa fa-pencil"></span></a>
                                                @endcan
                                                @can('eliminar.empleados')
                                                @if($empleados->id_empleado != $empleadoId)
                                                <a  href="{{ route('empleados.deshabilitar', $empleados->id_empleado) }}"
                                                class="btn btn-danger" ><span class="fa fa-trash-o"></span></a>
                                                @endif
                                                 @endcan
                                                 @can('crear.traslados')
                                                <a href="{{ route('traslados.listar', $empleados->id_empleado) }}" class="btn btn-default" style="background-color: lightgrey"><span class="fa fa-exchange"></span></a>
                                                @endcan
                                                <a href="{{route('empleado.exportar.contrato', $empleados->id_empleado) }}" class="btn btn-primary"><span class="fa fa-file-word-o"></span></a>

                                                
                                             </td>
                                         </tr>
                                         @endforeach
                                     </tbody>
                                 </table>
                             </div>
                         </div>
                         <div id="recent10" class="tab-pane"  style="overflow: scroll;">
                            <div class="panel-body">
                                <table class="table table-bordered table-striped mb-none" id="datatable-pestana">
                                    <thead>
                                        <tr>
                                            <th>C&oacute;digo</th>
                                            <th>Nombre Completo</th>
                                            <th>Unidad Administrativa</th>
                                            <th>Oficina</th>
                                            <th>Accion</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($empleadoDeshabilitados as $empleados)
                                        <tr>
                                            <td class="col-md-2">{{ $empleados->codigo_empleado }}</td>
                                            <td class="col-md-3">{{ $empleados->nombre }} {{ $empleados->apellido }}</td>
                                            <td class="col-md-2">{{ $empleados->oficina->unidadAdministrativa->unidad_administrativa }}</td>
                                            <td>{{ $empleados->oficina->oficina }}</td>
                                            <td class="col-md-3">
                                                @can('habilitar.empleados')
                                                <a  href="{{ route('empleados.habilitar', $empleados->id_empleado) }}" class="btn btn-primary"><span class="fa fa-caret-up"></span></a>
                                                @endcan
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
</div>
<!-- end: page -->
</section>

@endsection
@section('js')

<script src="https://cdn.datatables.net/fixedheader/3.1.6/js/dataTables.fixedHeader.min.js"></script> 
<script> 

    $(document).ready(function(){
        var table = $('#datatable-default').DataTable();
        new $.fn.dataTable.FixedHeader( table, {
           orderCellsTop: true,
           fixedHeader: true,
           "columnDefs": [
           { "searchable": false, "targets": 0 }
           ]

       });

    //Creamos una fila en el head de la tabla y lo clonamos para cada columna
    $('#datatable-default thead tr').clone(true).appendTo( '#datatable-default thead' );

    $('#datatable-default thead tr:eq(1) th').each( function (i) {
        var title = $(this).text(); //es el nombre de la columna
        $(this).html( '<input type="text" placeholder="'+title+'" class=""/>' );

        $( 'input', this ).on( 'keyup change', function () {
            if ( table.column(i).search() !== this.value ) {
                table
                .column(i)
                .search( this.value)
                .draw();
            }
        } );
        
    });   
});


    $(document).ready(function(){
        var table = $('#datatable-pestana').DataTable();
        new $.fn.dataTable.FixedHeader( table, {
           orderCellsTop: true,
           fixedHeader: true 
       });

    //Creamos una fila en el head de la tabla y lo clonamos para cada columna
    $('#datatable-pestana thead tr').clone(true).appendTo( '#datatable-pestana thead' );

    $('#datatable-pestana thead tr:eq(1) th').each( function (i) {
        var title = $(this).text(); //es el nombre de la columna
        $(this).html( '<input type="text" placeholder="'+title+'" class=""/>' );

        $( 'input', this ).on( 'keyup change', function () {
            if ( table.column(i).search() !== this.value ) {
                table
                .column(i)
                .search( this.value )
                .draw();
            }
        } );
    } );   
});
</script>
@endsection