@extends('layouts.template')
@section('content')
<section role="main" class="content-body">
    <header class="page-header">
        <h2>Gesti&oacute;n de Empleados</h2>
        <div class="right-wrapper pull-right">
            <ol class="breadcrumbs">
                <li style="margin-left: -25%"><a href="{{route('empleados.listar')}}"><button class="btn btn-default"><span class="fa fa-chevron-left"> </span> Regresar </button></a></li>
            </ol>
        </div>
    </header>

    <!-- start: page -->
    <div class="row">
        <div class="col-lg-12">
            <div>
                @include('flash::message')
                @if ($errors->any())
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                    <div class="errors">
                        <ul>
                            @foreach ($errors->all() as $error)

                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif 

                </div> 
            </div>
            <section class="panel">
                <header class="panel-heading">
                   <div class="panel-actions">
                      <a href="#" class="fa fa-caret-down"></a>
                      
                  </div>
                    <h2 class="panel-title">Informaci&oacute;n Personal</h2>
                </header>
                <div class="panel-body">

                    <div>
                        <div class="form-group">
                            <!-- Codigo -->
                            <label class="col-md-1 control-label" for="w4-first-name">C&oacute;digo</label>
                            <div class="col-md-2">
                                <input type="text"  class="form-control " name="codigo_empleado" id="codigo_empleado" placeholder="Codigo" value="{{ $empleado->codigo_empleado }}" disabled>
                            </div>
                            <!-- Nombre Empleado-->
                            <label class="col-md-2 control-label" for="w4-first-name">Nombre Completo</label>
                            <div class="col-md-7">
                                <input type="text" class="form-control" name="nombre_completo" id="nombre_completo" placeholder="Nombre Completo" value="{{ $empleado->nombre }} {{ $empleado->apellido }}" disabled>
                                
                            </div>
                        </div> 
                        <div class="form-group">
                            <!-- Edad -->
                            <label class="col-md-1 control-label" for="w4-first-name">Edad</label>
                            <div class="col-md-2">
                                <input type="number"  class="form-control" name="edad" id="edad" placeholder="Edad" value="{{ $edad }}" disabled min="0">
                                
                            </div>
                            <!--Fecha de Nacimiento -->
                            <label class="col-md-2 control-label" for="w4-first-name">Fecha de Nacimiento</label>
                            <div class="col-md-3">
                                <input type="date" class="form-control f" name="fecha_nacimiento" id="fecha_nacimiento" value="{{ $empleado->fecha_nacimiento }}" disabled>
                                
                            </div>
                            <!-- Estado Civil -->
                           <label class="col-md-2 control-label" for="w4-first-name">Estado Civil</label>
                           <div class="col-md-2">
                               <input type="text" class="form-control" name="estado_civil" id="estado_civil" placeholder="Municipio, Departamento" value="{{ $empleado->estadoCivil->estado_civil }}" disabled>
                           </div>
                            
                        </div>
                        <div class="form-group">
                            <!-- Sexo-->
                            <label class="col-md-1 control-label" for="w4-first-name">Sexo</label>
                            <div class="col-md-2">
                               <input type="text" class="form-control" name="sexo" id="sexo" value="{{ $empleado->sexo }}" disabled>
                           </div>
                           <!-- Lugar de Nacimiento -->
                            <label class="col-md-2 control-label" for="w4-first-name">Lugar de Nacimiento</label>
                            <div class="col-md-3">
                                <input type="text" class="form-control" name="lugar_nacimiento" id="lugar_nacimiento" placeholder="Municipio, Departamento" value="{{ $empleado->lugar_nacimiento }}" disabled>
                                
                            </div> 
                           <!-- Correo Electronico -->
                           <label class="col-md-1 control-label" for="w4-first-name">Email</label>
                           <div class="col-md-3">
                            <input type="email" class="form-control" name="correo" id="correo" value="{{ $empleado->correo }}" disabled>
                        </div>
                    </div>
                    <div class="form-group">

                        <!-- Telefono -->
                        <label class="col-md-1 control-label" for="w4-first-name">Tel&eacute;fono</label>
                        <div class="col-md-2"> 
                            <input type="text"  class="form-control" name="telefono" id="telefono" value="{{ $empleado->telefono }}" disabled >
                        </div>

                        <!-- Direccion-->
                        <label class="col-md-2 control-label" for="w4-first-name">Direcci&oacute;n</label>
                        <div class="col-md-7">
                            <input type="text" class="form-control" name="direccion" id="direccion"  value="{{ $empleado->direccion }}" disabled>
                            
                        </div>
                    </div>
                    <div class="form-group">

                       <!-- Departamento -->
                       <label class="col-md-2 control-label" for="w4-first-name">Departamento Residencia</label>
                       <div class="col-md-3">
                           <input type="text" class="form-control" name="departamento" id="departamento"  value="{{ $empleado->municipio->departamento->departamento }}" disabled>
                           
                       </div>
                       <!-- Municipio-->
                       <label class="col-md-2 control-label" for="w4-first-name">Municipio Residencia</label>
                       <div class="col-md-3">
                           <input type="text" class="form-control" name="municipio" id="municipio"  value="{{ $empleado->municipio->municipio }}" disabled>
                           
                       </div>
                   </div>
               </div>
           </div>
       </section>
       <section class="panel">
        <header class="panel-heading">
           <div class="panel-actions">
                <a href="#" class="fa fa-caret-down"></a>
                
            </div>
            <h2 class="panel-title">Documentos Personales</h2>
        </header>
        <div class="panel-body">
            @csrf         
            <div id="w4-document" class="tab-pane">

                <div class="form-group">
                    <!--Tipo de documento -->
                    <label class="col-md-2 control-label" for="w4-first-name">Tipo de Documento</label>
                    <div class="col-md-2">
                       <input type="text" class="form-control" name="tipo_documento" id="tipo_documento"  value="{{ $empleado->tipo_documento }}" disabled>
                   </div>

                   <!-- Nº de documento-->
                   <label class="col-md-2 control-label" for="w4-first-name">N° Documento</label>
                   <div class="col-md-2">
                    <input type="text"  class="form-control" name="numero_documento" id="numero_documento" value="{{ $empleado->numero_documento }}" disabled>
                    
                </div>

                <!--NIT -->
                <label class="col-md-1 control-label" for="w4-first-name">NIT</label>
                <div class="col-md-3">
                    <input type="text"  class="form-control" name="nit" id="nit" value="{{ $empleado->nit }}" disabled>
                    
                </div>
            </div>
            <div class="form-group">
                <!--Prevision -->
                <label class="col-md-2 control-label" for="w4-first-name">Previsi&oacute;n</label>
                <div class="col-md-2">
                   <input type="text" class="form-control" name="prevision" id="prevision" value="{{ $empleado->prevision }}" disabled>
               </div>

               <!--NUP -->
               <label class="col-md-2 control-label" for="w4-first-name">NUP</label>
               <div class="col-md-2">
                <input type="text"  class="form-control" name="nup" id="nup" value="{{ $empleado->nup }}" disabled>
                
            </div>

            <!--ISSS -->
            <label class="col-md-1 control-label" for="w4-first-name">ISSS</label>
            <div class="col-md-3">
                <input type="text"  class="form-control" name="isss" id="isss"  value="{{ $empleado->isss }}" disabled>
                
            </div>
        </div>
        <div class="form-group">
            <!--Nivel Academico -->
            <label class="col-md-2 control-label" for="w4-first-name">Nivel Acad&eacute;mico</label>
            <div class="col-md-10">                                    
              <input type="text" class="form-control" name="nivel_academico" id="nivel_academico" value="{{ $empleado->nivelAcademico->nivel_academico }}" disabled>
          </div>
      </div>
      <div class="form-group">
        <h4 class="col-md-3">Contacto de Emergencia</h4>
    </div>
    <div class="form-group">
        <!--Nombre de Contacto -->
        <label class="col-md-2 control-label" for="w4-first-name">Nombre Contacto</label>
        <div class="col-md-6">
            <input type="text"  class="form-control" name="nombre_contacto" id="nombre_contacto" value="{{ $empleado->nombre_contacto }}" disabled>
        </div>

        <!-- Telefono Contacto-->
        <label class="col-md-2 control-label" for="w4-first-name">Tel&eacute;fono Contacto</label>
        <div class="col-md-2"> 
            <input type="text"  class="form-control" name="telefono_contacto" id="telefono_contacto"  value="{{ $empleado->telefono_contacto }}" disabled>
        </div>
    </div>
</div>
</div>
</section>
<section class="panel">
    <header class="panel-heading">
       <div class="panel-actions">
                <a href="#" class="fa fa-caret-down"></a>
                
            </div>
        <h2 class="panel-title">Informaci&oacute;n Laboral</h2>
    </header>
    <div class="panel-body">

        @csrf   
        <div id="w4-jobs" class="tab-pane">
            <div class="form-group">

                <!-- Fecha de Ingreso-->
                <label class="col-md-2 control-label" for="w4-first-name">Fecha de Ingreso</label>
                <div class="col-md-4">
                    <input type="date" class="form-control" name="fecha_ingreso" id="fecha_ingreso" value="{{ $empleado->fecha_ingreso }}" disabled>
                    
                </div>
                <!-- Fecha de Reingreso -->
                <label class="col-md-2 control-label" for="w4-first-name">Fecha de Reingreso</label>
                <div class="col-md-4">
                    <input type="date" class="form-control" name="fecha_reingreso" id="fecha_reingreso" value="{{ $empleado->fecha_reingreso }}" disabled>
                </div>
            </div>
            <div class="form-group">
                <!--Salario Inicial -->
                <label class="col-md-2 control-label" for="w4-first-name">Salario Inicial</label>
                <div class="col-md-4">
                    <input type="number" class="form-control" name="salario_actual" id="salario_actual" value="{{ $empleado->salario_inicial }}" min="0" disabled>
                </div>
                <!--Salario Actual -->
                <label class="col-md-2 control-label" for="w4-first-name">Salario Actual</label>
                <div class="col-md-4">
                    <input type="number" class="form-control" name="salario_actual" id="salario_actual" value="{{ $empleado->salario_actual }}" min="0" disabled>
                </div>
            </div>
            <div class="form-group">

                <!--Unidad Administrativa -->
                <label class="col-md-2 control-label" for="w4-first-name">Unidad Administrativa</label>
                <div class="col-md-4">
                    <input type="text"  class="form-control" name="unidad_administrativa" id="unidad_administrativa"  value="{{ $empleado->oficina->unidadAdministrativa->unidad_administrativa}}" disabled>
                </div>

                <!-- Oficina-->
                <label class="col-md-2 control-label" for="w4-first-name">Oficina</label>
                <div class="col-md-4">
                    <input type="text"  class="form-control" name="oficina" id="oficina"  value="{{ $empleado->oficina->oficina}}" disabled>
                </div>
            </div>
            <div class="form-group">

                <!--Cargo Funcional -->
                <label class="col-md-2 control-label" for="w4-first-name">Cargo Funcional</label>
                <div class="col-md-4">                                    
                  <input type="text"  class="form-control" name="cargo_funcional" id="cargo_funcional"  value="{{ $empleado->cargoFuncional() }}" disabled>
              </div>

              <!-- Cargo Nominal-->
              <label class="col-md-2 control-label" for="w4-first-name">Cargo Nominal</label>
              <div class="col-md-4">
                <input type="text"  class="form-control" name="cargo_nominal" id="cargo_nominal"  value="{{ $empleado->cargoNominal() }}" disabled>
            </div>
        </div>
        <div class="form-group">

            <!--Cargo Funcional MP -->
            <label class="col-md-2 control-label" for="w4-first-name">Cargo Funcional MP</label>
            <div class="col-md-4">

             <input type="text"  class="form-control" name="cargo_mp" id="cargo_mp"  value="{{ $empleado->cargoMP->cargo_mp }}" disabled>
         </div>

         <!-- Cargo Nominal Hacienda-->
         <label class="col-md-2 control-label" for="w4-first-name">Cargo Nominal Hacienda</label>
         <div class="col-md-4">
           <input type="text"  class="form-control" name="cargo_hacienda" id="cargo_hacienda"  value="{{ $empleado->cargoHacienda->cargo_hacienda }}" disabled>
           
       </div>
   </div>
</div>
  <div class="form-group"></div>       
        <div id="w4-bank" class="tab-pane">
            <div class="form-group">

                <!--Banco -->
                <label class="col-md-2 control-label" for="w4-first-name">Banco</label>
                <div class="col-md-4">                        
                 <input type="text"  class="form-control" name="banco" id="banco"  value="{{ $empleado->banco->nombre_banco }}" disabled>
             </div>

             <!--Cuenta -->
             <label class="col-md-2 control-label" for="w4-first-name">Cuenta</label>
             <div class="col-md-3">
                <input type="text"  class="form-control" name="cuenta_banco" id="cuenta_banco" value="{{ $empleado->cuenta_banco }}" disabled >
                
            </div>
          </div>
          <div class="form-group">

            <!--Tipo Plaza -->
            <label class="col-md-2 control-label" for="w4-first-name">Tipo Plaza</label>
            <div class="col-md-4">
               <input type="text"  class="form-control" name="tipo_plaza" id="tipo_plaza"  value="{{ $empleado->tipoPlaza->tipo_plaza }}" disabled>
           </div>
           <!--Numero de Partida -->
        <label class="col-md-2 control-label" for="w4-first-name">Numero Partida</label>
        <div class="col-md-3">
            <input type="text" class="form-control" name="numero_partida" id="numero_partida" value="{{ $empleado->numero_partida }}" disabled>

        </div>
       </div>
       <div class="form-group">
        <!--Numero de Acuerdo -->
        <label class="col-md-2 control-label" for="w4-first-name">Numero Acuerdo</label>
        <div class="col-md-4">
            <input type="text" class="form-control" name="numero_acuerdo" id="numero_acuerdo" value="{{ $empleado->numero_acuerdo }}" disabled >
            
        </div>
        
        <!--Numero Sub Partida -->
        <label class="col-md-2 control-label" for="w4-first-name">Numero Sub Partida</label>
        <div class="col-md-3">
            <input type="text" class="form-control" name="numero_subpartida" id="numero_subpartida" value="{{ $empleado->numero_subpartida }}" disabled>
        </div>
    </div>
    <div class="form-group">
        <!--Cifra Presupuestaria -->
        <label class="col-md-2 control-label" for="w4-first-name">Cifra Presupuestaria</label>
        <div class="col-md-4">
            <input type="text" class="form-control" name="cifra_presupuestaria" id="cifra_presupuestaria" value="{{ $empleado->cifra_presupuestaria }}" disabled>
        </div>
        <!--GifCards -->
        <label class="col-md-2 control-label" for="w4-first-name">GifCards</label>
        <div class="col-md-3">
            <input type="text" class="form-control" name="gifcard" id="gifcard" value="{{ $empleado->gifcard }}" disabled>
            
        </div>
  </div>
  <div class="form-group">
        <!--PIN -->
        <label class="col-md-2 control-label" for="w4-first-name">PIN</label>
        <div class="col-md-4">
            <input type="number"  class="form-control" name="pin_empleado" id="pin_empleado" value="{{ $empleado->pin_empleado }}" disabled>
            
        </div>

        <!--Expediente -->
        <label class="col-md-2 control-label" for="w4-first-name">Expediente</label>
        <div class="col-md-3">
            <input type="number"  class="form-control" name="numero_expediente" id="numero_expediente" value="{{ $empleado->numero_expediente }}" disabled>
            
        </div>
    </div>
</div>
</div>
</section>
<section class="panel">
    <header class="panel-heading">
       <div class="panel-actions">
                <a href="#" class="fa fa-caret-down"></a>
                
            </div>
        <h2 class="panel-title">Estado del Empleado</h2>
    </header>
    <div class="panel-body">

        @csrf         
        <div id="w4-status" class="tab-pane">
            <div class="form-group">
                <div class="col-md-2"></div>
                <div class="col-md-2">

                    <!--Afiliado Sindicato -->
                    <div class="checkbox-custom checkbox-default">                                  
                        <input type="checkbox" id="afiliado_sindicato" disabled name="afiliado_sindicato" {{  ($empleado->afiliado_sindicato == 1 ? ' checked' : '') }} > 
                        
                        <label for="w4-terms">Afiliado Sindicato</label>
                    </div>
                    <!--Rendir Probidad -->
                    <div class="checkbox-custom checkbox-default">

                        <input type="checkbox"  id="rendir_probidad" disabled name="rendir_probidad" {{  ($empleado->rendir_probidad == 1 ? ' checked' : '') }}>
                        <label for="w4-terms">Rendir Probidad</label>
                    </div>
                    <!--Es Jefe -->
                    <div class="checkbox-custom checkbox-default">

                        <input type="checkbox"  id="es_jefe" disabled name="es_jefe" {{  ($empleado->es_jefe == 1 ? ' checked' : '') }}>
                        <label for="w4-terms">Es Jefe</label>
                    </div>
                </div>
                <div class="col-md-1"></div>
                <div class="col-md-2">
                    <!-- Marca-->
                    <div class="checkbox-custom checkbox-default">
                        <input type="checkbox"  id="marca" disabled name="marca" {{ ($empleado->marca == 1 ? ' checked' : '') }}>
                        <label for="w4-terms">Marca</label>
                    </div>

                    <!--Autorizado Estudio -->
                    <div class="checkbox-custom checkbox-default">
                        <input type="checkbox" id="autorizado_estudio" disabled name="autorizado_estudio" {{  ($empleado->autorizado_estudio == 1 ? ' checked' : '') }}>
                        <label for="w4-terms">Autorizado Estudio</label>
                    </div>

                    <!--Pensionado -->
                    <div class="checkbox-custom checkbox-default">
                        <input type="checkbox" id="pensionado" disabled name="pensionado" {{  ($empleado->pensionado == 1 ? ' checked' : '') }}>
                        <label for="w4-terms">Pensionado</label>
                    </div>
                </div>
                <div class="col-md-1"></div>
                <div class="col-md-2">

                    <!--Enviar Jefe -->
                    <div class="checkbox-custom checkbox-default">
                       <input type="checkbox"  id="enviar_jefe" disabled name="enviar_jefe" {{  ($empleado->enviar_jefe == 1 ? ' checked' : '') }}>
                       <label for="w4-terms">Enviar Jefe</label>
                   </div>

                   <!--Activo -->
                   <div class="checkbox-custom checkbox-default">
                    <input type="checkbox" id="activo" disabled name="activo" {{  ($empleado->activo == 1 ? ' checked' : '') }}>
                    <label for="w4-terms">Activo</label>
                </div>

                <!-- Pasivo-->
                <div class="checkbox-custom">
                    <input type="checkbox"  id="pasivo" disabled name="pasivo" {{  ($empleado->pasivo == 1 ? ' checked' : '') }}>
                    <label for="w4-terms">Pasivo</label>
                </div>
            </div>
            

        </section>
        <section class="panel">
            <header class="panel-heading">
               <div class="panel-actions">
                <a href="#" class="fa fa-caret-down"></a>
                
            </div>
                <h2 class="panel-title">Retiro</h2>
            </header>
            <div class="panel-body">
                @csrf         
                <div id="w4-confirm" class="tab-pane">
                    <div class="form-group">

                        <!--Numero de Acuerdo Retiro -->
                        <label class="col-md-3 control-label" for="w4-first-name">Numero Acuerdo Retiro</label>
                        <div class="col-md-6">
                            <input type="text" class="form-control" name="numero_acuerdo_retiro" id="numero_acuerdo_retiro" value="{{ $empleado->numero_acuerdo_retiro }}" disabled>
                            
                        </div>
                    </div>
                    <div class="form-group">

                        <!--Fecha Retiro -->
                        <label class="col-md-3 control-label" for="w4-first-name">Fecha Retiro</label>
                        <div class="col-md-6">
                            <input type="date" class="form-control" name="fecha_retiro" id="fecha_retiro" value="{{ $empleado->fecha_retiro }}" disabled>
                        </div>
                    </div>
                    <div class="form-group">

                        <!-- Motivo Retiro-->
                        <label class="col-md-3 control-label" for="w4-first-name">Motivo Retiro</label>
                        <div class="col-md-6">
                            <input type="text" class="form-control" name="motivo_retiro" id="motivo_retiro" value="{{ $empleado->motivo_retiro }}" disabled>
                            
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!-- end: page -->
    </section>
    @endsection



