@extends('layouts.template')
@section('content')
<section role="main" class="content-body">
    <header class="page-header">
        <h2>Gesti&oacute;n de Empleados</h2>
        <div class="right-wrapper pull-right">
            <ol class="breadcrumbs">
                <li style="margin-left: -25%"><a href="{{route('empleados.listar')}}"><button class="btn btn-default"><span class="fa fa-chevron-left"> </span> Regresar </button></a></li>
            </ol>
        </div>
    </header>

    <!-- start: page -->
    <div class="row">
        <div class="col-lg-12">
            <div>
                @include('flash::message')
                @if ($errors->any())
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                    <div class="errors">
                        <ul>
                            @foreach ($errors->all() as $error)

                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif 

                </div> 
            </div>
            <form  role="form" class="contactForm" action="{{ route('empleados.desactivar', $empleado->id_empleado) }}" method="post">

                {{csrf_field()}}
                {{ method_field('PUT') }}
                <section class="panel">
                    <header class="panel-heading">
                     
                        <h2 class="panel-title">Desactivar Empleado</h2>
                        
                    </header>
                    <div class="panel-body">

                        @csrf  
                        <div class="col-md-1"></div>
                        <div class="col-md-10">
                            <div class="form-group"></div>
                            <div class="col-md-12">
                                <div class="col-md-6">
                                    <!-- Nombre Empleado-->
                                
                                    <label class="col-md-12 control-label text-right" for="w4-first-name" style="text-align: left;">Nombre Completo</label>
                                    <div class="col-md-12"> 
                                        <input type="text" class="form-control" name="nombre_completo" id="nombre_completo"  value="{{ $empleado->nombre }} {{ $empleado->apellido }}" disabled>
                                        
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <!--Unidad Administrativa -->
                                    <label class="col-md-12 control-label text-right" for="w4-first-name" style="text-align: left;">Centro</label>
                                    <div class="col-md-12">
                                       <input type="text"  class="form-control" name="centro" id="centro"  value="{{ $empleado->oficina->centro->centro}}" disabled>
                                   </div>
                                    
                                </div>
                            </div>
                            <div class="form-group"></div>
                            <div class="col-md-12">
                                <div class="col-md-6">
                                    <!--Unidad Administrativa -->
                                    <label class="col-md-12 control-label text-right" for="w4-first-name" style="text-align: left;">Unidad Administrativa</label>
                                    <div class="col-md-12">
                                       <input type="text"  class="form-control" name="unidad_administrativa" id="unidad_administrativa"  value="{{ $empleado->oficina->unidadAdministrativa->unidad_administrativa}}" disabled>
                                   </div>
                                </div>
                                <div class="col-md-6">
                                    <!-- Oficina-->
                                   <label class="col-md-12 control-label text-right" for="w4-first-name" style="text-align: left;">Oficina</label>
                                   <div class="col-md-12">
                                       <input type="text"  class="form-control" name="oficina" id="oficina"  value="{{ $empleado->oficina->oficina}}" disabled>
                                   </div>
                                </div>
                            </div>
                                
                        </div> 
                            <div class="form-group">

                           </div>
                           <div class="form-group">
                               
                           </div>
                       </div>
                  
               </section>


               <section class="panel">
                <div class="panel-body">
                    @csrf         
                    <div id="w4-confirm" class="tab-pane">
                        <div class="col-md-2"></div>
                        <div class="col-md-8">
                            <div class="form-group">

                                <!--Numero de Acuerdo Retiro -->
                                <label class="col-md-12 control-label text-right" for="w4-first-name" style="text-align: left;">Numero Acuerdo Retiro</label>
                                <div class="col-md-12">
                                    <input type="text" class="form-control field-input {{ $errors->has('numero_acuerdo_retiro') ? 'field-error' : '' }}" name="numero_acuerdo_retiro" id="numero_acuerdo_retiro" value="{{ $empleado->numero_acuerdo_retiro }}" placeholder="Numero de Acuerdo para Retiro">
                                    @if ($errors->has('numero_acuerdo_retiro'))<span class="error-message">{{ $errors->first('numero_acuerdo_retiro') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <!--Fecha Retiro -->
                                <label class="col-md-12 control-label text-right" for="w4-first-name" style="text-align: left;">Fecha Retiro </label>
                                <div class="col-md-12">
                                   
                                        <input type="date" class="form-control field-input {{ $errors->has('fecha_retiro') ? 'field-error' : '' }}" name="fecha_retiro" id="fecha_retiro" value="{{ $empleado->fecha_retiro }}"> 
                                        @if ($errors->has('fecha_retiro'))<span class="error-message">{{ $errors->first('fecha_retiro') }}</span>
                                            @endif 
                                    
                                </div>
                            </div>
                            <div class="form-group">

                                <!-- Motivo Retiro-->
                                <label class="col-md-12 control-label text-right" for="w4-first-name" style="text-align: left;">Motivo Retiro</label>
                                <div class="col-md-12">
                                    <input type="text" class="form-control field-input {{ $errors->has('motivo_retiro') ? 'field-error' : '' }}" name="motivo_retiro" id="motivo_retiro" value="{{ $empleado->motivo_retiro }}" placeholder="Motivo de Retiro">
                                    @if ($errors->has('motivo_retiro'))<span class="error-message">{{ $errors->first('motivo_retiro') }}</span>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            
        <section class="panel">
            <div class="panel-body">
                @csrf         
                <div id="w4-confirm" class="tab-pane">
                 
                    <div class="form-group text-center">
                        <button class="btn btn-danger">Desactivar</button>
                        <a href="{{route('home')}}" class="btn btn-default">Cancelar</a>
                    </div>
                    

                </div>
            </div>
        </section>
    </form>
</div>
</div>
<!-- end: page -->
</section>
@endsection

