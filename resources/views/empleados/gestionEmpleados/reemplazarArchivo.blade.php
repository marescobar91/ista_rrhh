@extends('layouts.template')
@section('css')
<style type="text/css">
.btn-primary {
    border-color: #cccccc;
    background-color: #cccccc;
    border-color: #cccccc #cccccc #b3b3b3;
    color: white;
    text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.25);
}

.btn-primary:hover {
    border-color: #d9d9d9;
    background-color: #d9d9d9;
    color: white;
}
</style>
@endsection
@section('content')
<section role="main" class="content-body">
    <header class="page-header">
        <h2>Gesti&oacute;n de Empleados</h2>
        <div class="right-wrapper pull-right">
            <ol class="breadcrumbs">
                <li style="margin-left: -25%"><a href="{{route('empleados.listar')}}"><button class="btn btn-default"><span class="fa fa-chevron-left"> </span> Regresar </button></a></li>
            </ol>
        </div>
    </header>

    <!-- start: page -->
    <div class="row">
        <div class="col-lg-12">
            <div>
                @include('flash::message')
                @if ($errors->any())
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                    <div class="errors">
                        <ul>
                            @foreach ($errors->all() as $error)

                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif 

                </div> 
            </div>
            <section class="panel">
                <header class="panel-heading">

                    <h2 class="panel-title">Agregar nuevo Documento </h2>
                </header>
                <div class="panel-body">
                    
                        <form action="{{ route('documentos.reemplazar', $documento->id_documento) }}" method="post" enctype="multipart/form-data" >
                         {{csrf_field()}}
                    {{ method_field('PUT') }}
                        <div class="form-group">
                            <div class="col-md-2"></div>
                               <h4>Empleado: {{ $empleado->nombre }} {{ $empleado->apellido }}</h4>
                           </div>
                        <div class="form-group">
                            <div class="col-md-2"></div>
                            <label>El nuevo documento reemplazará a: {{ $documento->nombre_documento }}</label>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label text-right">Descripci&oacute;n</label>
                          <div class="col-md-4">
                                        <input type="text" class="form-control field-input {{ $errors->has('descripcion') ? 'field-error' : '' }}" name="descripcion" id="descripcion" placeholder="Descripcion del Documento" value="{{ $documento->descripcion }}" required>
                                        @if ($errors->has('descripcion'))<span class="error-message">{{ $errors->first('descripcion') }}</span>
                                        @endif
                            </div>
                        <div class="col-md-2" align="right">
                        
                        <label for="documento" class="btn btn-success">
                         <i class="fa fa-cloud-upload"></i> Subir archivo
                        </label>
                    </div>
                    <div class="col-md-2">
                        <input id="documento" name="documento" onchange='cambiar()' type="file" style='display: none;'/>
                        <div id="info"></div>
                        <input type="submit" value="Enviar" class="btn btn-warning">  
                        </div>
                         <div class="col-md-1" align="left"></div>
                         <a href="{{route('home')}}" class="btn btn-default">Cancelar</a>
                        </div>
                        </div>
                    </form>
                
            </div>
        </section>
    </div>
</div>
<!-- end: page -->
</section>

@endsection
@section('js')
<script type="text/javascript">
    function cambiar(){
    var pdrs = document.getElementById('documento').files[0].name;
    document.getElementById('info').innerHTML = pdrs;
}

</script>
@endsection




