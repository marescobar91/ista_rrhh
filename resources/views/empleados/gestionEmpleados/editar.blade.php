@extends('layouts.template')
@section('content')
<section role="main" class="content-body">
    <header class="page-header">
        <h2>Gesti&oacute;n de Empleados</h2>
        <div class="right-wrapper pull-right">
            <ol class="breadcrumbs">
                <li style="margin-left: -25%"><a href="{{route('empleados.listar')}}"><button class="btn btn-default"><span class="fa fa-chevron-left"> </span> Regresar </button></a></li>
            </ol>
        </div>
    </header>

    <!-- start: page -->
    <div class="row">
        <div class="col-lg-12">
            <div>
                @include('flash::message')
                @if ($errors->any())
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                    <div class="errors">
                        <ul>
                            @foreach ($errors->all() as $error)

                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif 

                </div> 
            </div>
            <form action="{{route('empleados.actualizar', $empleado->id_empleado)}}" method="post"  role="form" class="contactForm">

                {{csrf_field()}}
                {{ method_field('PUT') }}
                <section class="panel">
                    <header class="panel-heading">
                        <div class="panel-actions">
                            <a href="#" class="fa fa-caret-down"></a>
                            
                        </div>
                        <h2 class="panel-title">Informaci&oacute;n Personal</h2>
                    </header>
                    <div class="panel-body">

                        @csrf  
                        <div>
                            <div class="form-group">
                                <!-- Codigo -->
                                <label class="col-md-1 control-label" for="w4-first-name">C&oacute;digo</label>
                                <div class="col-md-2">
                                    <input type="text"  class="form-control field-input {{ $errors->has('codigo_empleado') ? 'field-error' : '' }}" name="codigo_empleado" id="codigo_empleado" placeholder="Codigo" value="{{ $empleado->codigo_empleado }}" required>
                                    @if ($errors->has('codigo_empleado'))<span class="error-message">{{ $errors->first('codigo_empleado') }}</span>
                                    @endif
                                </div>
                                <!-- Nombre Empleado-->
                                <label class="col-md-2 control-label" for="w4-first-name">Nombre</label>
                                <div class="col-md-3">
                                    <input type="text" class="form-control field-input {{ $errors->has('nombre') ? 'field-error' : '' }}" name="nombre" id="nombre" placeholder="Nombre" value="{{ $empleado->nombre }}" required>
                                    @if ($errors->has('nombre'))<span class="error-message">{{ $errors->first('nombre') }}</span>
                                    @endif
                                </div>
                                <!-- Nombre Empleado-->
                                <label class="col-md-1 control-label" for="w4-first-name">Apellido</label>
                                <div class="col-md-3">
                                    <input type="text" class="form-control field-input {{ $errors->has('apellido') ? 'field-error' : '' }}" name="apellido" id="apellido" placeholder="Apellido" value="{{ $empleado->apellido }}" required>
                                    @if ($errors->has('apellido'))<span class="error-message">{{ $errors->first('apellido') }}</span>
                                    @endif
                                </div>
                            </div> 
                            <div class="form-group">
                                <!-- Edad -->
                                <label class="col-md-1 control-label" for="w4-first-name">Edad</label>
                                <div class="col-md-2">
                                    <input type="number"  class="form-control field-input {{ $errors->has('edad') ? 'field-error' : '' }}" name="edad" id="edad" placeholder="Edad" value="{{ $edad}}" required min="0" disabled>
                                    @if ($errors->has('edad'))<span class="error-message"> {{$errors->first('edad') }}</span>
                                    @endif
                                </div>
                                <!--Fecha de Nacimiento -->
                                <label class="col-md-2 control-label" for="w4-first-name">Fecha Nacimiento </label>
                                <div class="col-md-3">
                                    <input type="date" class="form-control field-input {{ $errors->has('fecha_nacimiento') ? 'field-error' : '' }}" name="fecha_nacimiento" id="fecha_nacimiento" value="{{ $empleado->fecha_nacimiento }}" > 
                                    @if ($errors->has('fecha_nacimiento'))<span class="error-message">{{ $errors->first('fecha_nacimiento') }}</span>
                                    @endif 

                                </div>
                                <!-- Estado Civil -->
                                <label class="col-md-2 control-label" for="w4-first-name">Estado Civil</label>
                                <div class="col-md-2">
                                    <?php $selectedvalue=$empleado['estado_civil_id'] ?>
                                    <select class="form-control" id="estadoCivil" name="estadoCivil" required>

                                        <option value="">Seleccion Estado Civil</option>
                                        @foreach($estadoCivil as $estado)

                                        <option value="{{ $estado['id_estado_civil']}}" {{$selectedvalue == $estado['id_estado_civil'] ? 'selected="selected"' : ''}}> 
                                        {{$estado['estado_civil']}}</option>
                                        
                                        @endforeach

                                    </select>
                                    @if ($errors->has('estadoCivil '))<span class="error-message">{{ $errors->first('estadoCivil') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <!-- Sexo-->
                                <label class="col-md-1 control-label" for="w4-first-name">Sexo</label>
                                <div class="col-md-2">
                                    <select class="form-control" name="sexo" id="sexo" required>
                                        <option value="Mujer" {{($empleado->sexo ==='Mujer') ? 'selected' : ''}}> Mujer </option>
                                        <option value="Hombre" {{($empleado->sexo ==='Hombre') ? 'selected' : ''}}> Hombre </option>
                                    </select>
                                    @if ($errors->has('sexo'))<span class="error-message">{{ $errors->first('sexo') }}</span>
                                    @endif
                                </div>
                                <!-- Lugar de Nacimiento -->
                                <label class="col-md-2 control-label" for="w4-first-name">Lugar de Nacimiento</label>
                                <div class="col-md-3">
                                    <input type="text" class="form-control field-input {{ $errors->has('lugar_nacimiento') ? 'field-error' : '' }}" name="lugar_nacimiento" id="lugar_nacimiento" placeholder="Municipio, Departamento" value="{{ $empleado->lugar_nacimiento }}" required>
                                    @if ($errors->has('lugar_nacimiento'))<span class="error-message">{{ $errors->first('lugar_nacimiento') }}</span>
                                    @endif
                                </div> 
                                <!-- Correo Electronico -->
                                <label class="col-md-1 control-label" for="w4-first-name">Email</label>
                                <div class="col-md-3">
                                    <input type="email" class="form-control field-input {{ $errors->has('correo') ? 'field-error' : '' }}" name="correo" id="correo" placeholder="Correo Electronico" value="{{ $empleado->correo }}" >
                                    @if ($errors->has('correo'))<span class="error-message">{{ $errors->first('correo') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">

                                <!-- Telefono -->
                                <label class="col-md-1 control-label" for="w4-first-name">Tel&eacute;fono</label>
                                <div class="col-md-2"> 
                                    <input type="text"  class="form-control field-input {{ $errors->has('telefono') ? 'field-error' : '' }}" name="telefono" id="telefono" placeholder="####-####" value="{{ $empleado->telefono }}" pattern="[0-9]{4}[ -][0-9]{4}" title="Formato solicitado ####-####" required >
                                    @if ($errors->has('telefono'))<span class="error-message">{{ $errors->first('telefono') }}</span>
                                    @endif
                                </div>

                                <!-- Direccion-->
                                <label class="col-md-2 control-label" for="w4-first-name">Direcci&oacute;n</label>
                                <div class="col-md-7">
                                    <input type="text" class="form-control field-input {{ $errors->has('direccion') ? 'field-error' : '' }}" name="direccion" id="direccion" placeholder="Direccion" value="{{ $empleado->direccion }}" required>
                                    @if ($errors->has('direccion'))<span class="error-message">{{ $errors->first('direccion') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">

                             <!-- Departamento -->
                             <label class="col-md-2 control-label" for="w4-first-name">Departamento Residencia</label>
                             <div class="col-md-3">
                                <select data-plugin-selectTwo class="form-control" id="departamento_id" name="departamento" required>
                                   <option value="">Seleccion Departamento</option>
                                   @foreach($departamento as $departamentos)


                                   <option value="{{ $departamentos['id_departamento']}}" {{$departamentoActual == $departamentos['id_departamento'] ? 'selected="selected"' : ''}}> 
                                   {{$departamentos['departamento']}}</option>

                                   @endforeach

                               </select>
                               @if ($errors->has('departamento'))<span class="error-message">{{ $errors->first('departamento') }}</span>
                               @endif
                           </div>
                           <!-- Municipio-->
                           <label class="col-md-2 control-label" for="w4-first-name">Municipio Residencia</label>
                           <div class="col-md-3">

                            <select data-plugin-selectTwo id="municipio_id" data-old="{{ $municipioActual}}" name="municipio" class="form-control{{ $errors->has('municipio') ? ' is-invalid' : '' }}">

                            </select>


                            @if ($errors->has('municipio'))<span class="error-message">{{ $errors->first('municipio') }}</span>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="panel">
            <header class="panel-heading">
                <div class="panel-actions">
                    <a href="#" class="fa fa-caret-down"></a>
                    
                </div>
                <h2 class="panel-title">Documentos Personales</h2>
            </header>
            <div class="panel-body">
                @csrf         
                <div id="w4-document" class="tab-pane">

                    <div class="form-group">
                        <!--Tipo de documento -->
                        <label class="col-md-2 control-label" for="w4-first-name">Tipo de Documento</label>
                        <div class="col-md-2">
                            <select class="form-control" id="tipo_documento", name="tipo_documento">
                                <option value="DUI" {{($empleado->tipo_documento ==='DUI') ? 'selected' : ''}}> DUI </option>
                                <option value="Pasaporte" {{($empleado->tipo_documento ==='Pasaporte') ? 'selected' : ''}}> Pasaporte </option>

                            </select> 
                            @if ($errors->has('tipo_documento'))<span class="error-message">{{ $errors->first('tipo_documento') }}</span>
                            @endif
                        </div>

                        <!-- Nº de documento-->
                        <label class="col-md-2 control-label" for="w4-first-name">N° Documento</label>
                        <div class="col-md-2">
                            <input type="text"  class="form-control field-input {{ $errors->has('numero_documento') ? 'field-error' : '' }}" name="numero_documento" id="numero_documento" placeholder="N° documento" value="{{ $empleado->numero_documento }}" required>
                            @if ($errors->has('numero_documento'))<span class="error-message">{{ $errors->first('numero_documento') }}</span>
                            @endif
                        </div>

                        <!--NIT -->
                        <label class="col-md-1 control-label" for="w4-first-name">NIT</label>
                        <div class="col-md-3">
                            <input type="text"  class="form-control field-input {{ $errors->has('nit') ? 'field-error' : '' }}" name="nit" id="nit" placeholder="####-######-###-#" pattern="[0-9]{4}[-][0-9]{6}[-][0-9]{3}[-][0-9]{1}" title="Formato solicitado ####-######-###-#" value="{{ $empleado->nit }}" >
                            @if ($errors->has('nit'))<span class="error-message">{{ $errors->first('nit') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <!--Prevision -->
                        <label class="col-md-2 control-label" for="w4-first-name">Previsi&oacute;n</label>
                        <div class="col-md-2">
                            <select  class="form-control" id="prevision" name="prevision">

                             <option value="AFP CONFIA" {{($empleado->prevision ==='AFP CONFIA') ? 'selected' : ''}}> AFP CONFIA </option>
                             <option value="AFP CRECER" {{($empleado->prevision ==='AFP CRECER') ? 'selected' : ''}}> AFP CRECER </option>
                             <option value="INPEP" {{($empleado->prevision ==='INPEP') ? 'selected' : ''}}> INPEP </option>
                             <option value="INSFA" {{($empleado->prevision ==='INSFA') ? 'selected' : ''}}> INSFA </option>  

                         </select> 
                         @if ($errors->has('prevision'))<span class="error-message">{{ $errors->first('prevision') }}</span>
                         @endif
                     </div>

                     <!--NUP -->
                     <label class="col-md-2 control-label" for="w4-first-name">NUP</label>
                     <div class="col-md-2">
                        <input type="text"  class="form-control field-input {{ $errors->has('nup') ? 'field-error' : '' }}" name="nup" id="nup" placeholder="" value="{{ $empleado->nup }}" >
                        @if ($errors->has('nup'))<span class="error-message">{{ $errors->first('nup') }}</span>
                        @endif
                    </div>

                    <!--ISSS -->
                    <label class="col-md-1 control-label" for="w4-first-name">ISSS</label>
                    <div class="col-md-3">
                        <input type="text"  class="form-control field-input {{ $errors->has('isss') ? 'field-error' : '' }}" name="isss" id="isss" placeholder="" value="{{ $empleado->isss }}" >
                        @if ($errors->has('isss'))<span class="error-message">{{ $errors->first('isss') }}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group">

                    <!--Nivel Academico -->
                    <label class="col-md-2 control-label" for="w4-first-name">Nivel Acad&eacute;mico</label>
                    <div class="col-md-10"> 
                        <?php $selectedvalue=$empleado['nivel_academico_id'] ?>                                   
                        <select data-plugin-selectTwo class="form-control" id="nivel_academico" name="nivel_academico" required>
                            <option value="">Seleccion Nivel Academico</option>
                            @foreach($nivelAcademico as $nivel)

                            <?php $selectedvalue=$empleado['nivel_academico_id'] ?>
                            <option value="{{ $nivel['id_nivel_academico']}}" {{$selectedvalue == $nivel['id_nivel_academico'] ? 'selected="selected"' : ''}}> 
                            {{$nivel['nivel_academico']}}</option>                                                           
                            @endforeach
                        </select> 
                        @if ($errors->has('nivel_academico'))<span class="error-message">{{ $errors->first('nivel_academico') }}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <h4 class="col-md-3">Contacto de Emergencia</h4>
                </div>
                <div class="form-group">
                    <!--Nombre de Contacto -->
                    <label class="col-md-2 control-label" for="w4-first-name">Nombre Contacto</label>
                    <div class="col-md-6">
                        <input type="text"  class="form-control field-input {{ $errors->has('nombre_contacto') ? 'field-error' : '' }}" name="nombre_contacto" id="nombre_contacto" placeholder="Nombre Contacto" value="{{ $empleado->nombre_contacto }}" >
                        @if ($errors->has('nombre_contacto'))<span class="error-message">{{ $errors->first('nombre_contacto') }}</span>
                        @endif
                    </div>

                    <!-- Telefono Contacto-->
                    <label class="col-md-2 control-label" for="w4-first-name">Tel&eacute;fono Contacto</label>
                    <div class="col-md-2"> 
                        <input type="text"  class="form-control field-input {{ $errors->has('telefono_contacto') ? 'field-error' : '' }}" name="telefono_contacto" id="telefono_contacto" placeholder="####-####" pattern="[0-9]{4}[ -][0-9]{4}" title="Formato solicitado ####-####" value="{{ $empleado->telefono_contacto }}" >
                        @if ($errors->has('telefono_contacto'))<span class="error-message">{{ $errors->first('telefono_contacto') }}</span>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="panel">
        <header class="panel-heading">
            <div class="panel-actions">
                <a href="#" class="fa fa-caret-down"></a>
                
            </div>
            <h2 class="panel-title">Informaci&oacute;n Laboral</h2>
        </header>
        <div class="panel-body">

            @csrf   
            <div id="w4-jobs" class="tab-pane">
                <div class="form-group">
                    <!--Centro -->
                    <label class="col-md-2 control-label" for="w4-first-name">Centro</label>
                    <div class="col-md-4">
                        <select data-plugin-selectTwo class="form-control" id="centro_id" name="centro" required>
                            <option value="">Seleccion Centro</option>
                            @foreach($centro as $centroB)

                            <?php $selectedvalue=$centroB['id_centro'] ?>
                            <option value="{{ $centroB['id_centro' ]}}" {{$centroActual == $centroB['id_centro'] ? 'selected ="selected"' : ''}}> {{ $centroB['centro']}}</option>                                                            
                            @endforeach
                        </select> 
                        @if ($errors->has('centro'))<span class="error-message">{{ $errors->first('centro') }}</span>
                        @endif
                    </div>
                    <!--Unidad Administrativa -->
                    <label class="col-md-2 control-label" for="w4-first-name">Unidad Administrativa</label>
                    <div class="col-md-4">
                        <select data-plugin-selectTwo class="form-control" id="unidad_id" name="unidad" required>
                            <option value="">Seleccion Unidad Administrativa</option>
                            @foreach($unidad as $unidadB)

                            <?php $selectedvalue=$unidadB['id_unidad_administrativa'] ?>
                            <option value="{{ $unidadB['id_unidad_administrativa' ]}}" {{$unidadActual == $unidadB['id_unidad_administrativa'] ? 'selected ="selected"' : ''}}> {{ $unidadB['unidad_administrativa']}}</option>                                                            
                            @endforeach
                        </select> 


                        @if ($errors->has('unidad'))<span class="error-message">{{ $errors->first('unidad') }}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <!-- Fecha de Ingreso-->
                    <label class="col-md-2 control-label" for="w4-first-name">Fecha Ingreso </label>
                    <div class="col-md-4">

                        <input type="date" class="form-control field-input {{ $errors->has('fecha_ingreso') ? 'field-error' : '' }}" name="fecha_ingreso" id="fecha_ingreso" value="{{ $empleado->fecha_ingreso }}"> 
                        @if ($errors->has('fecha_ingreso'))<span class="error-message">{{ $errors->first('fecha_ingreso') }}</span>
                        @endif 

                    </div>

                    <!--Salario Inicial -->
                    <label class="col-md-2 control-label" for="w4-first-name">Salario Inicial</label>
                    <div class="col-md-4">
                        <input type="number" class="form-control field-input {{ $errors->has('salario_inicial') ? 'field-error' : '' }}" name="salario_inicial" id="salario_inicial" value="{{ $empleado->salario_inicial }}" placeholder="Salario" min="0" disabled>
                        @if ($errors->has('salario_inicial'))<span class="error-message">{{ $errors->first('salario_inicial') }}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <!-- Oficina-->
                    <label class="col-md-2 control-label" for="w4-first-name">Oficina</label>
                    <div class="col-md-4">
                        <select data-plugin-selectTwo  id="oficina_id" data-old="{{$oficinaActual}}" name="oficina" class="form-control{{ $errors->has('oficina') ? ' is-invalid' : '' }}"> 

                        </select>


                        @if ($errors->has('oficina'))<span class="error-message">{{ $errors->first('oficina') }}</span>
                        @endif
                    </div>
                    <!--Salario Actual -->
                    <label class="col-md-2 control-label" for="w4-first-name">Salario Actual</label>
                    <div class="col-md-4">
                        <input type="number" class="form-control field-input {{ $errors->has('salario_actual') ? 'field-error' : '' }}" name="salario_actual" id="salario_actual" value="{{ $empleado->salario_actual }}" placeholder="Salario" min="0" required>
                        @if ($errors->has('salario_actual'))<span class="error-message">{{ $errors->first('salario_actual') }}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group">

                    <!--Cargo Funcional -->
                    <label class="col-md-2 control-label" for="w4-first-name">Cargo Funcional</label>
                    <div class="col-md-4">                                    
                       <select data-plugin-selectTwo class="form-control" id="cargoF" name="cargoF" required>
                        <option value="">Seleccion Cargo Funcional</option>
                        @foreach($cargoFuncional as $cargoF)

                        <?php $selectedvalue=$empleado['cargo_funcional_id'] ?>
                        <option value="{{ $cargoF['id_cargo']}}" {{$selectedvalue == $cargoF['id_cargo'] ? 'selected="selected"' : ''}}> 
                        {{$cargoF['cargo']}}</option>                                                           
                        @endforeach
                    </select> 
                    @if ($errors->has('cargoF'))<span class="error-message">{{ $errors->first('cargoF') }}</span>
                    @endif
                </div>

                <!-- Cargo Nominal-->
                <label class="col-md-2 control-label" for="w4-first-name">Cargo Nominal</label>
                <div class="col-md-4">

                 <select data-plugin-selectTwo class="form-control" id="cargoN" name="cargoN" required>
                    <option value="">Seleccion Cargo Nominal</option>
                    @foreach($cargoNominal as $cargoN)

                    <?php $selectedvalue=$empleado['cargo_nominal_id'] ?>
                    <option value="{{ $cargoN['id_cargo']}}" {{$selectedvalue == $cargoN['id_cargo'] ? 'selected="selected"' : ''}}> 
                    {{$cargoN['cargo']}}</option>                                                           
                    @endforeach
                </select> 

                @if ($errors->has('cargoN'))<span class="error-message">{{ $errors->first('cargoN') }}</span>
                @endif
            </div>
        </div>
        <div class="form-group">

            <!--Cargo Funcional MP -->
            <label class="col-md-2 control-label" for="w4-first-name">Cargo Funcional MP</label>
            <div class="col-md-4">

                <select data-plugin-selectTwo class="form-control" id="cargoMP" name="cargoMP" required>
                    <option value="">Seleccion Cargo Funcional MP</option>
                    @foreach($cargoMP as $cargo)

                    <?php $selectedvalue=$empleado['cargo_mp_id'] ?>
                    <option value="{{ $cargo['id_cargo_mp']}}" {{$selectedvalue == $cargo['id_cargo_mp'] ? 'selected="selected"' : ''}}> 
                    {{$cargo['cargo_mp']}}</option>                                                           
                    @endforeach
                </select> 

                @if ($errors->has('cargoMP'))<span class="error-message">{{ $errors->first('cargoMP') }}</span>
                @endif
            </div>

            <!-- Cargo Nominal Hacienda-->
            <label class="col-md-2 control-label" for="w4-first-name">Cargo Nominal Hacienda</label>
            <div class="col-md-4">

                <select data-plugin-selectTwo class="form-control" id="cargoH" name="cargoH" required>
                    <option value="">Seleccion Cargo Hacienda</option>
                    @foreach($cargoHacienda as $cargoH)

                    <?php $selectedvalue=$empleado['cargo_hacienda_id'] ?>
                    <option value="{{ $cargoH['id_cargo_hacienda']}}" {{$selectedvalue == $cargoH['id_cargo_hacienda'] ? 'selected="selected"' : ''}}> 
                    {{$cargoH['cargo_hacienda']}}</option>                                                           
                    @endforeach
                </select> 

                @if ($errors->has('cargoH'))<span class="error-message">{{ $errors->first('cargoH') }}</span>
                @endif
            </div>
        </div>
    </div>
    <div class="form-group"></div>

    @csrf         
    <div id="w4-bank" class="tab-pane">
        <div class="form-group">

            <!--Banco -->
            <label class="col-md-2 control-label" for="w4-first-name">Banco</label>
            <div class="col-md-4">                        
               <select data-plugin-selectTwo class="form-control" id="banco" name="banco" required>
                <option value="">Seleccion Banco</option>
                @foreach($banco as $bancos)
                
                <?php $selectedvalue=$empleado['banco_id'] ?>
                <option value="{{ $bancos['id_banco']}}" {{$selectedvalue == $bancos['id_banco'] ? 'selected="selected"' : ''}}> 
                {{$bancos['nombre_banco']}}</option>                                @endforeach
            </select> 
            @if ($errors->has('banco'))<span class="error-message">{{ $errors->first('banco') }}</span>
            @endif
        </div>

        <!--Cuenta -->
        <label class="col-md-2 control-label" for="w4-first-name">Cuenta</label>
        <div class="col-md-3">
            <input type="text"  class="form-control field-input {{ $errors->has('cuenta_banco') ? 'field-error' : '' }}" name="cuenta_banco" id="cuenta_banco" value="{{ $empleado->cuenta_banco }}" required placeholder="Numero de Cuenta">
            @if ($errors->has('cuenta_banco'))<span class="error-message">{{ $errors->first('cuenta_banco') }}</span>
            @endif
        </div>
    </div>
    <div class="form-group">
     <!--PIN -->
     <label class="col-md-2 control-label" for="w4-first-name">PIN</label>
     <div class="col-md-4">
        <input type="number"  class="form-control field-input {{ $errors->has('pin_empleado') ? 'field-error' : '' }}" name="pin_empleado" id="pin_empleado" value="{{ $empleado->pin_empleado }}" required placeholder="####">
        @if ($errors->has('pin_empleado'))<span class="error-message">{{ $errors->first('pin_empleado') }}</span>
        @endif
    </div>

    <!--Expediente -->
    <label class="col-md-2 control-label" for="w4-first-name">Expediente</label>
    <div class="col-md-3">
        <input type="number"  class="form-control field-input {{ $errors->has('numero_expediente') ? 'field-error' : '' }}" name="numero_expediente" id="numero_expediente" value="{{ $empleado->numero_expediente }}" required placeholder="####">
        @if ($errors->has('numero_expediente'))<span class="error-message">{{ $errors->first('numero_expediente') }}</span>
        @endif
    </div>
</div>
<div class="form-group">

    <!--Tipo Plaza -->
    <label class="col-md-2 control-label" for="w4-first-name">Tipo Contrato</label>
    <div class="col-md-4">
      <select class="form-control" id="tipo_plaza" name="tipo_plaza" required>
        <option value="">Seleccion Tipo Plaza</option>
        @foreach($tipoPlaza as $tipo)

        <?php $selectedvalue=$tipo['id_tipo_plaza'] ?>
        <option value="{{ $tipo['id_tipo_plaza']}}" {{$selectedvalue == $tipo['id_tipo_plaza'] ? 'selected="selected"' : ''}}> 
        {{$tipo['tipo_plaza']}}</option>                                                           
        @endforeach
    </select> 
    @if ($errors->has('tipo_plaza'))<span class="error-message">{{ $errors->first('tipo_plaza') }}</span>
    @endif
</div>
<!--Numero de Partida -->
<label class="col-md-2 control-label" for="w4-first-name">Numero Partida</label>
<div class="col-md-3">
    <input type="text" class="form-control field-input {{ $errors->has('numero_partida') ? 'field-error' : '' }}" name="numero_partida" id="numero_partida" value="{{ $empleado->numero_partida }}" required placeholder="N° Partida">
    @if ($errors->has('numero_partida'))<span class="error-message">{{ $errors->first('numero_partida') }}</span>
    @endif
</div>
</div>
<div class="form-group">
    <!--Numero de Acuerdo -->
    <label class="col-md-2 control-label" for="w4-first-name">Numero Acuerdo</label>
    <div class="col-md-4">
        <input type="text" class="form-control field-input {{ $errors->has('numero_acuerdo') ? 'field-error' : '' }}" name="numero_acuerdo" id="numero_acuerdo" value="{{ $empleado->numero_acuerdo }}" required placeholder="N° Acuerdo">
        @if ($errors->has('numero_acuerdo'))<span class="error-message">{{ $errors->first('numero_acuerdo') }}</span>
        @endif
    </div>

    <!--Numero Sub Partida -->
    <label class="col-md-2 control-label" for="w4-first-name">Numero Sub Partida</label>
    <div class="col-md-3">
        <input type="text" class="form-control field-input {{ $errors->has('numero_subpartida') ? 'field-error' : '' }}" name="numero_subpartida" id="numero_subpartida" value="{{ $empleado->numero_subpartida }}" required placeholder="N° Partida">
        @if ($errors->has('numero_subpartida'))<span class="error-message">{{ $errors->first('numero_subpartida') }}</span>
        @endif
    </div>
</div>
<div class="form-group">
    <!--Cifra Presupuestaria -->
    <label class="col-md-2 control-label" for="w4-first-name">Cifra Presupuestaria</label>
    <div class="col-md-4">
        <input type="text" class="form-control field-input {{ $errors->has('cifra_presupuestaria') ? 'field-error' : '' }}" name="cifra_presupuestaria" id="cifra_presupuestaria" value="{{ $empleado->cifra_presupuestaria }}" required placeholder="Cifra Presupuestaria">
        @if ($errors->has('cifra_presupuestaria'))<span class="error-message">{{ $errors->first('cifra_presupuestaria') }}</span>
        @endif
    </div>
    <!--GifCards -->
    <label class="col-md-2 control-label" for="w4-first-name">GifCards</label>
    <div class="col-md-3">
        <input type="text" class="form-control field-input {{ $errors->has('gifcard') ? 'field-error' : '' }}" name="gifcard" id="gifcard" value="{{ $empleado->gifcard }}" placeholder="GifCards">
        @if ($errors->has('gifcard'))<span class="error-message">{{ $errors->first('gifcard') }}</span>
        @endif
    </div>


</div>
</div>
</div>
</section>
<section class="panel">
    <header class="panel-heading">
        <div class="panel-actions">
            <a href="#" class="fa fa-caret-down"></a>
            
        </div>
        <h2 class="panel-title">Estado del Empleado</h2>
    </header>
    <div class="panel-body">

        @csrf         
        <div id="w4-status" class="tab-pane">
            <div class="form-group">
                <div class="col-md-2"></div>
                <div class="col-md-2">

                    <!--Afiliado Sindicato -->
                    <div class="checkbox-custom checkbox-default">                                  
                        <input type="checkbox" id="afiliado_sindicato" disabled name="afiliado_sindicato" {{  ($empleado->afiliado_sindicato == 1 ? ' checked' : '') }} > 
                        
                        <label for="w4-terms">Afiliado Sindicato</label>
                    </div>
                    <!--Rendir Probidad -->
                    <div class="checkbox-custom checkbox-default">

                        <input type="checkbox"  id="rendir_probidad" name="rendir_probidad" {{  ($empleado->rendir_probidad == 1 ? ' checked' : '') }}>
                        <label for="w4-terms">Rendir Probidad</label>
                    </div>
                    <!--Es Jefe -->
                    <div class="checkbox-custom checkbox-default">

                        <input type="checkbox"  id="es_jefe" name="es_jefe" {{  ($empleado->es_jefe == 1 ? ' checked' : '') }}>
                        <label for="w4-terms">Es Jefe</label>
                    </div>
                </div>
                <div class="col-md-1"></div>
                <div class="col-md-2">
                    <!-- Marca-->
                    <div class="checkbox-custom checkbox-default">
                        <input type="checkbox"  id="marca" name="marca" {{ ($empleado->marca == 1 ? ' checked' : '') }}>
                        <label for="w4-terms">Marca</label>
                    </div>

                    <!--Autorizado Estudio -->
                    <div class="checkbox-custom checkbox-default">
                        <input type="checkbox" id="autorizado_estudio" name="autorizado_estudio" {{  ($empleado->autorizado_estudio == 1 ? ' checked' : '') }}>
                        <label for="w4-terms">Autorizado Estudio</label>
                    </div>

                    <!--Pensionado -->
                    <div class="checkbox-custom checkbox-default">
                        <input type="checkbox" id="pensionado" name="pensionado" {{  ($empleado->pensionado == 1 ? ' checked' : '') }}>
                        <label for="w4-terms">Pensionado</label>
                    </div>
                </div>
                <div class="col-md-1"></div>
                <div class="col-md-2">

                    <!--Enviar Jefe -->
                    <div class="checkbox-custom checkbox-default">
                       <input type="checkbox"  id="enviar_jefe" name="enviar_jefe" {{  ($empleado->enviar_jefe == 1 ? ' checked' : '') }}>
                       <label for="w4-terms">Enviar Jefe</label>
                   </div>

                   <!--Activo -->
                   <div class="checkbox-custom checkbox-default">
                    <input type="checkbox" id="activo" name="activo" {{  ($empleado->activo == 1 ? ' checked' : '') }}>
                    <label for="w4-terms">Activo</label>
                </div>

                <!-- Pasivo-->
                <div class="checkbox-custom">
                    <input type="checkbox"  id="pasivo" name="pasivo" {{  ($empleado->pasivo == 1 ? ' checked' : '') }}>
                    <label for="w4-terms">Pasivo</label>
                </div>
            </div>
        </div>
    </div>
</div>
</section>
<section class="panel">
    <div class="panel-body">
        @csrf         
        <div id="w4-confirm" class="tab-pane">
            <div class="form-group text-center">
                <button class="btn btn-warning">Editar</button>
                <a href="{{route('home')}}" class="btn btn-default">Cancelar</a>
            </div>
        </div>
    </div>
</section>
</form>
</div>
</div>
<!-- end: page -->
</section>
@endsection

@section('script')

<script type="text/javascript">


    $(document).ready(function() {
        function cargarMuncipio(){
            var departamento_id = $('#departamento_id').val();
            if($.trim('departamento_id') != ''){
                $.get('/municipios', {departamento:  departamento_id}, function (municipios){

                    var old = $('#municipio_id').data('old') != '' ? $('#municipio_id').data('old') : '';
                    console.log('valor para este elemento: '+old);

                    $('#municipio_id').empty();
                    $('#municipio_id').append("<option value=''>Selecciona un Municipio </option>");
                    $.each(municipios, function(index, value){
                        $('#municipio_id').append("<option value='" + index +"'" + (old == index ? 'selected' : '') + ">" + value + "</option>");
                        console.info(`valor para old: ${old}
                            valor para value: ${value}
                            valor para indece: ${index}`);
                    })

                });
            }
        }
        cargarMuncipio();
        $('#departamento_id').on('change', cargarMuncipio);
    });

</script>

<script type="text/javascript">


    $(document).ready(function() {
        function cargarOficinas(){
            var unidad_id = $('#unidad_id').val(); 
            var centro_id = $('#centro_id').val();
            if($.trim('unidad_id') != '' && $.trim('centro_id') != ''){
                $.get('/selecionOficinas', {unidad_administrativa:  unidad_id,centro: centro_id}, function (oficinas){

                    var old = $('#oficina_id').data('old') != '' ? $('#oficina_id').data('old') : '';
                    console.log('valor para este elemento: '+old);

                    $('#oficina_id').empty();
                    $('#oficina_id').append("<option value=''>Selecciona Oficina </option>");
                    $.each(oficinas, function(index, value){
                        $('#oficina_id').append("<option value='" + index + "'" + (old == index ? 'selected' : '' ) + ">" + value + "</option>");
                        console.info(`valor para old: ${old}
                            valor para value: ${value}
                            valor para indece: ${index}`);
                    })

                });
            }
        }
        cargarOficinas();
        $('#centro_id').on('change', cargarOficinas);
        $('#unidad_id').on('change', cargarOficinas);
    });

</script>
<!--Mascaras para los input de telefono, nit  -->
<script>
    $(document).ready(function(){
        $('#telefono').mask('9999-9999');
    });
</script>

<script>
    $(document).ready(function(){
        $('#nit').mask('9999-999999-999-9');
    });
</script>
@endsection
