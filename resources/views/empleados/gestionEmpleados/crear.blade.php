@extends('layouts.template')
@section('content')
<section role="main" class="content-body">
	<header class="page-header">
		<h2>Gestión de Empleados</h2>
		<div class="right-wrapper pull-right">
            <ol class="breadcrumbs">
                <li style="margin-left: -25%"><a href="{{route('empleados.listar')}}"><button class="btn btn-default"><span class="fa fa-chevron-left"> </span> Regresar </button></a></li>
            </ol>
        </div>
	</header>

	<!-- start: page -->
	<div class="row">
		<div class="col-xs-12">
			<div>
				<div class="row" id="alertaError" style="display:none">
						<div class="col-md-12">
							<div class="alert alert-danger" role="alert">
								<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
								<p>Errores</p>
								<ul id="listaErrores">
								</ul>
							</div>
						</div>
					</div>
			</div>	

			<section class="panel form-wizard" >

				<header class="panel-heading">
					<div class="panel-actions">
					</div>
					<h2 class="panel-title" id="titulo">Informacion Personal</h2>
					<div class="form-group"></div>
					

				</header>
				<div class="panel-body">

							<input type="hidden" id="id_empleado" name="id_empleado" value="">
							<div id="personal" >

								<div class="col-md-1"></div>
								<div class="col-md-11">
									<div class="col-md-6">
										<!--PIN-->
										<label class="col-md-12 control-label" for="w4-first-name" style="text-align: left;">PIN</label>
										<div class="col-md-12">
											<input type="number" class="form-control field-input {{ $errors->has('pin_empleado') ? 'field-error' : '' }}" name="pin_empleado" id="pin_empleado" value="{{ old('pin_empleado') }}" required placeholder="####">
											@if ($errors->has('pin_empleado'))<span class="error-message">{{ $errors->first('pin_empleado') }}</span>
											@endif
										</div>
									</div>
									<div class="col-md-6">
										<!--Expediente-->
										<label class="col-md-12 control-label" for="w4-first-name" style="text-align: left;">Expediente</label>
										<div class="col-md-12">
											<input type="number" class="form-control field-input {{ $errors->has('numero_expediente') ? 'field-error' : '' }}" name="numero_expediente" id="numero_expediente" value="{{ old('numero_expediente') }}" required placeholder="####">
											@if ($errors->has('numero_expediente'))<span class="error-message">{{ $errors->first('numero_expediente') }}</span>
											@endif
										</div>
									</div>
								</div>
								<div class="form-group"></div>
								<div class="col-md-1"></div>
								<div class="col-md-11">
									<div class="col-md-6">
										<!--Nombre Empleado-->
										<label class="col-md-12 control-label" for="w4-first-name" style="text-align: left;">Nombre</label>
										<div class="col-md-12">
											<input type="text" class="form-control field-input {{ $errors->has('nombre') ? 'field-error' : '' }}" name="nombre" id="nombre" placeholder="Nombre" value="{{ old('nombre') }}" required style="text-align: left;">
											@if ($errors->has('nombre'))<span class="error-message">{{ $errors->first('nombre') }}</span>
											@endif
										</div>
									</div>
									<div class="col-md-6">
										<!--Apellido Empleado-->
										<label class="col-md-12 control-label" for="w4-first-name" style="text-align: left;">Apellido</label>
										<div class="col-md-12">
											<input type="text" class="form-control field-input {{ $errors->has('apellido') ? 'field-error' : '' }}" name="apellido" id="apellido" placeholder="Apellido" value="{{ old('apellido') }}" required>
											@if ($errors->has('apellido'))<span class="error-message">{{ $errors->first('apellido') }}</span>
											@endif
										</div>
									</div>
								</div>
								<div class="form-group"></div>
								<div class="col-md-1"></div>
								<div class="col-md-11">
									<div class="col-md-6">
										<!--Codigo Empleado-->
										<label class="col-md-12 control-label" for="w4-first-name" style="text-align: left;">Codigo</label>
										<div class="col-md-12">
											<input type="text" class="form-control field-input {{ $errors->has('codigo_empleado') ? 'field-error' : '' }}" name="codigo_empleado" id="codigo_empleado" placeholder="Codigo del Empleado" required value="" maxlength="7" pattern="[a-z]{2}[][0-9]{5}" title="Formato solicitado AA#####">
											@if ($errors->has('codigo_empleado'))<span class="error-message">{{ $errors->first('codigo_empleado') }}</span>
											@endif
										</div>
									</div>
									<div class="col-md-6">
										<!--Estado Civil-->
										<label class="col-md-12 control-label" for="w4-first-name" style="text-align: left;">Estado Civil</label>
										<div class="col-md-12">
											<select class="form-control" name="estadoCivil" id="estadoCivil"required>
												<option value="" selected>Seleccione uno</option>
												@foreach($estadoCivil as $estado)
												<option value="{{$estado->id_estado_civil}}">{{$estado->estado_civil}}</option>
												@endforeach
												
											</select> 
											@if ($errors->has('estadoCivil'))<span class="error-message">{{ $errors->first('estadoCivil') }}</span>
											@endif
										</div>
									</div>
								</div>
								<div class="form-group"></div>
								<div class="col-md-1"></div>
								<div class="col-md-11">
									<div class="col-md-6">
										<!--Fecha de Nacimiento-->
										<label class="col-md-12 control-label" for="w4-first-name" style="text-align: left;">Fecha de Nacimiento</label>
										<div class="col-md-12">
											<input type="date" class="form-control field-input {{ $errors->has('fecha_nacimiento') ? 'field-error' : '' }}" name="fecha_nacimiento" id="fecha_nacimiento" value="{{ old('fecha_nacimiento') }}">
											@if ($errors->has('fecha_nacimiento'))<span class="error-message">{{ $errors->first('fecha_nacimiento') }}</span>
											@endif
										</div>
									</div>
									<div class="col-md-6">
										<!--Lugar de Nacimiento-->
										<label class="col-md-12 control-label" for="w4-first-name" style="text-align: left;">Lugar de Nacimiento</label>
										<div class="col-md-12">
											<input type="text" class="form-control field-input {{ $errors->has('lugar_nacimiento') ? 'field-error' : '' }}" name="lugar_nacimiento" id="lugar_nacimiento" placeholder="Municipio, Departamento" value="{{ old('lugar_nacimiento') }}" required>
											@if ($errors->has('lugar_nacimiento'))<span class="error-message">{{ $errors->first('lugar_nacimiento') }}</span>
											@endif
										</div>
									</div>
								</div>
								<div class="form-group"></div>
								<div class="col-md-1"></div>
								<div class="col-md-11">
									<div class="col-md-6">
										<!--Sexo-->
										<label class="col-md-12 control-label" for="w4-first-name" style="text-align: left;">Sexo</label>
										<div class="col-md-12">
											<select class="form-control populate" name="sexo" id="sexo" required>
												<option value="" selected>Seleccione uno</option>
												<option value="Mujer">Mujer</option>
												<option value="Hombre">Hombre</option>
											</select>
											@if ($errors->has('sexo'))<span class="error-message">{{ $errors->first('sexo') }}</span>
											@endif
										</div>
									</div>
									<div class="col-md-6">
										<!--Telefono-->
										<label class="col-md-12 control-label" for="w4-first-name" style="text-align: left;">Telefono</label>
										<div class="col-md-12"> 
											<input type="text" class="form-control field-input {{ $errors->has('telefono') ? 'field-error' : '' }}" name="telefono" id="telefono" placeholder="####-####" pattern="[0-9]{4}[ -][0-9]{4}" title="Formato solicitado ####-####" required maxlength="9">
											@if ($errors->has('telefono'))<span class="error-message">{{ $errors->first('telefono') }}</span>
											@endif
										</div>
									</div>
								</div>
								<div class="form-group"></div>
								<div class="col-md-1"></div>
								<div class="col-md-11">
									<div class="col-md-6">
										<!--Correo Electronico-->
										<label class="col-md-12 control-label" for="w4-first-name" style="text-align: left;">Email</label>
										<div class="col-md-12">
											<input type="email" class="form-control field-input {{ $errors->has('correo') ? 'field-error' : '' }}" name="correo" id="correo" placeholder="Correo Electronico (Opcional)" value="{{ old('correo') }}" >
											@if ($errors->has('correo'))<span class="error-message">{{ $errors->first('correo') }}</span>
											@endif
										</div>
									</div>
									<div class="col-md-6">
										<!--Direccion-->
										<label class="col-md-12 control-label" for="w4-first-name" style="text-align: left;">Direccion</label>
										<div class="col-md-12">
											<textarea type="text" class="form-control field-input {{ $errors->has('direccion') ? 'field-error' : '' }}" name="direccion" id="direccion" placeholder="Direccion" value="{{ old('direccion') }}" required rows="3"></textarea>
											@if ($errors->has('direccion'))<span class="error-message">{{ $errors->first('direccion') }}</span>
											@endif
										</div>
									</div>
								</div>
								<div class="form-group"></div>
								<div class="col-md-1"></div>
								<div class="col-md-11">
									<div class="col-md-6">
										<!--Departamento-->
										<label class="col-md-12 control-label" for="w4-first-name" style="text-align: left;">Departamento Residencia</label>
										<div class="col-md-12">
											<select data-plugin-selectTwo id="departamento_id" class="form-control populate" name="departamento">
												<option value="" selected>Seleccione el departamento</option>
												@foreach($departamento as $departamentos)
												<option value="{{$departamentos->id_departamento}}">{{$departamentos->departamento}}</option>
												@endforeach
												
											</select>
											@if ($errors->has('departamento'))<span class="error-message">{{ $errors->first('departamento') }}</span>
											@endif
										</div>
									</div>
									<div class="col-md-6">
										<!--Municipio-->
										<label class="col-md-12 control-label" for="w4-first-name" style="text-align: left;">Municipio Residencia</label>
										<div class="col-md-12">
											<select data-plugin-selectTwo id="municipio_id" data-old="{{ old('municipio_id') }}" required name="municipio_id" class="form-control{{ $errors->has('municipio_id') ? ' is-invalid' : '' }} populate"></select>

											@if ($errors->has('municipio_id'))<span class="error-message">{{ $errors->first('municipio_id') }}</span>
											@endif
										</div>
									</div>
								</div>
								<div class="form-group"></div>							
							
									<div class="col-lg-12 text-center" >    
										<button  class="btn btn-default" id="anterior1" disabled>Anterior</button>
										<button type="submit" class="btn btn-primary" id="guardar1" name="guardar1">Siguiente</button>
									</div>
							
							</div>
							<div id="document" style="display:none">
								<div class="form-group">
									<input type="hidden" id="document-edit" value="">
									<div class="col-md-12">
										<div class="col-md-4">
											<!--Tipo de Documento-->
											<label class="col-md-12 control-label" for="w4-first-name" style="text-align: left;">Tipo de Documento</label>
											<div class="col-md-12">
												<select class="form-control" name="tipo_documento" id="tipo_documento" required>
													<option value="" selected>Seleccione uno</option>
													<option value="Dui">DUI</option>
													<option value="Pasaporte">Pasaporte</option>
												</select> 
												@if ($errors->has('tipo_documento'))<span class="error-message">{{ $errors->first('tipo_documento') }}</span>
												@endif
											</div>
										</div>
										<div class="col-md-4">
											<!--N° de Documento-->
											<label class="col-md-12 control-label" for="w4-first-name" style="text-align: left;">N° Documento</label>
											<div class="col-md-12">
												<input type="text" class="form-control field-input {{ $errors->has('numero_documento') ? 'field-error' : '' }}" name="numero_documento" id="numero_documento" placeholder="N° documento" value="{{ old('numero_documento') }}" required>
												@if ($errors->has('numero_documento'))<span class="error-message">{{ $errors->first('numero_documento') }}</span>
												@endif
											</div>
										</div>
										<div class="col-md-4">
											<!--NIT-->
											<label class="col-md-12 control-label" for="w4-first-name" style="text-align: left;">NIT</label>
											<div class="col-md-12">
												<input type="text" class="form-control field-input {{ $errors->has('nit') ? 'field-error' : '' }}" name="nit" id="nit" placeholder="####-######-###-#" value="{{ old('nit') }}"  pattern="[0-9]{4}[-][0-9]{6}[-][0-9]{4}[-][0-9]{1}" title="Formato solicitado ####-######-###-#">
												@if ($errors->has('nit'))<span class="error-message">{{ $errors->first('nit') }}</span>
												@endif
											</div>
										</div>
									</div>
									<div class="form-group"></div>
									<div class="col-md-12">
										<div class="col-md-4">
											<!--Prevision-->
											<label class="col-md-12 control-label" for="w4-first-name" style="text-align: left;">Prevision</label>
											<div class="col-md-12">
												<select class="form-control" name="prevision" id="prevision">
													<option value="" selected>Seleccione uno</option>
													<option value="AFP CONFIA" >AFP CONFIA</option>
													<option value="AFP CRECER">AFP CRECER</option>
													<option value="INPEP">INPEP</option>
													<option value="IPSFA">IPSFA</option>
												</select> 
												@if ($errors->has('prevision'))<span class="error-message">{{ $errors->first('prevision') }}</span>
												@endif
											</div>
										</div>
										<div class="col-md-4">
											<!--NUP-->
											<label class="col-md-12 control-label" for="w4-first-name" style="text-align: left;">NUP</label>
											<div class="col-md-12">
												<input type="text" class="form-control field-input {{ $errors->has('nup') ? 'field-error' : '' }}" name="nup" id="nup" placeholder="" value="{{ old('nup') }}" >
												@if ($errors->has('nup'))<span class="error-message">{{ $errors->first('nup') }}</span>
												@endif
											</div>
										</div>
										<div class="col-md-4">
											<!--ISSS-->
											<label class="col-md-12 control-label" for="w4-first-name" style="text-align: left;">ISSS</label>
											<div class="col-md-12">
												<input type="text" class="form-control field-input {{ $errors->has('isss') ? 'field-error' : '' }}" name="isss" id="isss" placeholder="" value="{{ old('isss') }}" maxlength="9">
												@if ($errors->has('isss'))<span class="error-message">{{ $errors->first('isss') }}</span>
												@endif
											</div>
										</div>
									</div>
									<div class="form-group"></div>
									<div class="col-md-11" style="margin-left: 1%; width: 97.7%" >
										
											<!--Nivel Academico-->
											<label class="col-md-12 control-label" for="w4-first-name" style="text-align: left;">Nivel Academico</label>
											<div class="col-md-12">
												<select data-plugin-selectTwo class="form-control" name="nivelAcademico" id="nivelAcademico" required>
													<option value="" selected>Seleccione uno</option>
													@foreach($nivelAcademico as $niveles)
													<option value="{{$niveles->id_nivel_academico}}">{{$niveles->nivel_academico}}</option>
													@endforeach
												</select> 
												@if ($errors->has('nivelAcademico'))<span class="error-message">{{ $errors->first('nivelAcademico') }}</span>
												@endif
											</div>
										
									</div>
								</div>							
								<div class="form-group">
									<div class="col-md-1"></div>
									<h4 class="col-md-3">Contacto de Emergencia</h4>
								</div>
								<div class="col-md-12">
									<div class="col-md-6">
										<!--Nombre de Contacto-->
										<label class="col-md-12 control-label" for="w4-first-name" style="text-align: left;">Nombre Contacto</label>
										<div class="col-md-12">
											<input type="text" class="form-control field-input {{ $errors->has('nombre_contacto') ? 'field-error' : '' }}" name="nombre_contacto" id="nombre_contacto" placeholder="Nombre Contacto (Opcional)" value="{{ old('nombre_contacto') }}" >
											@if ($errors->has('nombre_contacto'))<span class="error-message">{{ $errors->first('nombre_contacto') }}</span>
											@endif
										</div>
									</div>
									<div class="col-md-6">
										<!--Telefono Contacto-->
										<label class="col-md-12 control-label" for="w4-first-name" style="text-align: left;">Telefono Contacto</label>
										<div class="col-md-12"> 
											<input type="text" class="form-control field-input {{ $errors->has('telefono_contacto') ? 'field-error' : '' }}" name="telefono_contacto" id="telefono_contacto" placeholder="####-#### (Opcional)" value="{{ old('telefono_contacto') }}" pattern="[0-9]{4}[ -][0-9]{4}" title="Formato solicitado ####-####" maxlength="9">
											@if ($errors->has('telefono_contacto'))<span class="error-message">{{ $errors->first('telefono_contacto') }}</span>
											@endif
										</div>
									</div>
								</div>
								<div class="form-group"></div>							
								<div class="form-group">
									<div class="col-lg-12 text-center" >    
										<button  class="btn btn-default" id="anterior2">Anterior</button>
										<button type="submit" class="btn btn-primary" id="guardar2">Siguiente</button>
									</div>
								</div>
							</div>
							<div id="jobs"  style="display:none">
								<div class="col-md-12">
									<input type="hidden" id="jobs-edit" value="">
									<div class="col-md-4">
										<!--Centro-->
										<label class="col-md-12 control-label" for="w4-first-name" style="text-align: left;">Centro</label>
										<div class="col-md-12">
											<select data-plugin-selectTwo id="centro_id" class="form-control" name="centro" required>
												<option value="" selected>Seleccione uno</option>
												@foreach($centro as $centro)
												<option value="{{$centro->id_centro}}">{{$centro->centro}}</option>
												@endforeach
											</select> 
											@if ($errors->has('centro'))<span class="error-message">{{ $errors->first('centro') }}</span>
											@endif
										</div>
									</div>
									<div class="col-md-4">
										<!--Unidad Administrativa-->
										<label class="col-md-12 control-label" for="w4-first-name" style="text-align: left;">Unidad Administrativa</label>
										<div class="col-md-12">
											<select data-plugin-selectTwo id="unidad_id" class="form-control" name="unidad" required>
												<option value="" selected>Seleccione uno</option>
												@foreach($unidad as $unidad)
												<option value="{{$unidad->id_unidad_administrativa}}">{{$unidad->unidad_administrativa}}</option>
												@endforeach
											</select> 
											@if ($errors->has('unidad'))<span class="error-message">{{ $errors->first('unidad') }}</span>
											@endif
										</div>
									</div>
									<div class="col-md-4">
										<!--Oficina-->
										<label class="col-md-12 control-label" for="w4-first-name" style="text-align: left;">Oficina</label>
										<div class="col-md-12">
											<select data-plugin-selectTwo id="oficina_id" data-old="{{ old('oficina') }}" name="oficina" class="form-control{{ $errors->has('oficina') ? ' is-invalid' : '' }}"></select>
											@if ($errors->has('oficina'))<span class="error-message">{{ $errors->first('oficina') }}</span>
											@endif
										</div>

									</div>
								</div>
								<div class="form-group"></div>
								<div class="col-md-12">
									<div class="col-md-4">
										<!--Fecha de Ingreso-->
										<label class="col-md-12 control-label text-right" for="w4-first-name" style="text-align: left;">Fecha Ingreso </label>
										<div class="col-md-12">
											
											<input type="date" class="form-control field-input {{ $errors->has('fecha_ingreso') ? 'field-error' : '' }}" name="fecha_ingreso" id="fecha_ingreso" value="{{ old('fecha_ingreso') }}"> 
											@if ($errors->has('fecha_ingreso'))<span class="error-message">{{ $errors->first('fecha_ingreso') }}</span>
											@endif 

											
										</div>
									</div>
									<div class="col-md-4">
										<!--Salario Actual-->
										<label class="col-md-11 control-label" for="w4-first-name" style="text-align: left;">Salario</label>
										<div class="col-md-12">
											<input type="number" class="form-control field-input {{ $errors->has('salario_actual') ? 'field-error' : '' }}" name="salario_actual" id="salario_actual" value="{{ old('salario_actual') }}" placeholder="Salario" min="0" required>
											@if ($errors->has('salario_actual'))<span class="error-message">{{ $errors->first('salario_actual') }}</span>
											@endif
										</div>
									</div>
									<div class="col-md-4">
										<!--Cargo Funcional-->
										<label class="col-md-11 control-label" for="w4-first-name" style="text-align: left;">Cargo Funcional</label>
										<div class="col-md-12">
											<select data-plugin-selectTwo class="form-control" name="cargoF" id="cargoF" required>
												<option value="" selected>Seleccione uno</option>
												@foreach($cargoFuncional as $cargoF)
												<option value="{{$cargoF->id_cargo}}">{{$cargoF->cargo}}</option>
												@endforeach
											</select> 
											@if ($errors->has('cargoF'))<span class="error-message">{{ $errors->first('cargoF') }}</span>
											@endif
										</div>
									</div>
								</div>
								<div class="form-group"></div>
									<div class="col-md-12">
										<div class="col-md-4">
											<!--Cargo Funcional MP-->
											<label class="col-md-12 control-label" for="w4-first-name" style="text-align: left;">Cargo Funcional MP</label>
											<div class="col-md-12">
												<select data-plugin-selectTwo class="form-control" name="cargoMP" id="cargoMP" required>
													<option value="" selected>Seleccione uno</option>
													@foreach($cargoMP as $cargoMP)
													<option value="{{$cargoMP->id_cargo_mp}}">{{$cargoMP->cargo_mp}}</option>
													@endforeach
												</select> 
												@if ($errors->has('cargoMP'))<span class="error-message">{{ $errors->first('cargoMP') }}</span>
												@endif
											</div>
										</div>
										<div class="col-md-4">
											<!--Cargo Nominal-->
											<label class="col-md-12 control-label" for="w4-first-name" style="text-align: left;">Cargo Nominal</label>
											<div class="col-md-12">
												<select data-plugin-selectTwo class="form-control" name="cargoN" id="cargoN"  required>
													<option value="" selected>Seleccione uno</option>
													@foreach($cargoNominal as $cargoN)
													<option value="{{$cargoN->id_cargo}}">{{$cargoN->cargo}}</option>
													@endforeach
												</select> 
												@if ($errors->has('cargoN'))<span class="error-message">{{ $errors->first('cargoN') }}</span>
												@endif
											</div>
										</div>
										<div class="col-md-4">
											<!--Cargo Nominal Hacienda-->
											<label class="col-md-12 control-label" for="w4-first-name" style="text-align: left;">Cargo Nominal Hacienda</label>
											<div class="col-md-12">
												<select data-plugin-selectTwo class="form-control" name="cargoHacienda" id="cargoHacienda" required>
													<option value="" selected>Seleccione uno</option>
													@foreach($cargoHacienda as $cargoHacienda)
													<option value="{{$cargoHacienda->id_cargo_hacienda}}">{{$cargoHacienda->cargo_hacienda}}</option>
													@endforeach
												</select> 
												@if ($errors->has('cargoHacienda'))<span class="error-message">{{ $errors->first('cargoHacienda') }}</span>
												@endif
											</div>
										</div>
									</div>
								<div class="form-group"></div>
								<div class="form-group">
									<div class="col-lg-12 text-center" >    
										<button  class="btn btn-default" id="anterior3">Anterior</button>
										<button type="submit" class="btn btn-primary" id="guardar3">Siguiente</button>
									</div>
								</div>
							</div>
							<div id="bank"  style="display:none">
								<div class="col-md-1"></div>
								<div class="col-md-11">
									<input type="hidden" id="bank-edit" value="">
									<div class="col-md-5">
										<!--Banco-->
										<label class="col-md-12 control-label" for="w4-first-name" style="text-align: left;">Banco</label>
										<div class="col-md-12">
											<select data-plugin-selectTwo class="form-control" name="banco" id="banco" required>
												<option value="" selected>Seleccione uno</option>
												@foreach($banco as $bancos)
												<option value="{{$bancos->id_banco}}">{{$bancos->nombre_banco}}</option>
												@endforeach
											</select> 
											@if ($errors->has('banco'))<span class="error-message">{{ $errors->first('banco') }}</span>
											@endif
										</div>
									</div>
									<div class="col-md-5">
										<!--Cuenta-->
										<label class="col-md-12 control-label" for="w4-first-name" style="text-align: left;">Cuenta</label>
										<div class="col-md-12">
											<input type="text" class="form-control field-input {{ $errors->has('cuenta_banco') ? 'field-error' : '' }}" name="cuenta_banco" id="cuenta_banco" value="{{ old('cuenta_banco') }}" required placeholder="Numero de Cuenta">
											@if ($errors->has('cuenta_banco'))<span class="error-message">{{ $errors->first('cuenta_banco') }}</span>
											@endif
										</div>
									</div>
								</div>
								<div class="form-group"></div>
								<div class="col-md-1"></div>
								<div class="col-md-11">
									<div class="col-md-5">
										<!--Tipo de Plaza-->
										<label class="col-md-12 control-label" for="w4-first-name" style="text-align: left;">Tipo de Contrato</label>
										<div class="col-md-12">
											<select data-plugin-selectTwo class="form-control" name="tipoPlaza" id="tipoPlaza" required>
												<option value="" selected>Seleccione uno</option>
												@foreach($tipoPlaza as $tipoPlaza)
												<option value="{{$tipoPlaza->id_tipo_plaza}}">{{$tipoPlaza->tipo_plaza}}</option>
												@endforeach
											</select> 
											@if ($errors->has('tipoPlaza'))<span class="error-message">{{ $errors->first('tipoPlaza') }}</span>
											@endif
										</div>
									</div>
									<div class="col-md-5">
										<!--Numero Partida-->
										<label class="col-md-12 control-label" for="w4-first-name" style="text-align: left;">Numero Partida</label>
										<div class="col-md-12">
											<input type="text" class="form-control field-input {{ $errors->has('numero_partida') ? 'field-error' : '' }}" name="numero_partida" id="numero_partida" value="{{ old('numero_partida') }}" required placeholder="N° Partida">
											@if ($errors->has('numero_partida'))<span class="error-message">{{ $errors->first('numero_partida') }}</span>
											@endif
										</div>
									</div>
								</div>
								<div class="form-group"></div>
								<div class="col-md-1"></div>
								<div class="col-md-11">
									<div class="col-md-5">
										<!--Numero Acuerdo-->
										<label class="col-md-12 control-label" for="w4-first-name" style="text-align: left;">Numero Acuerdo</label>
										<div class="col-md-12">
											<input type="text" class="form-control field-input {{ $errors->has('numero_acuerdo') ? 'field-error' : '' }}" name="numero_acuerdo" id="numero_acuerdo" value="{{ old('numero_acuerdo') }}" required placeholder="N° Acuerdo">
											@if ($errors->has('numero_acuerdo'))<span class="error-message">{{ $errors->first('numero_acuerdo') }}</span>
											@endif
										</div>
									</div>
									<div class="col-md-5">
										<!--Numero SubPartida-->
										<label class="col-md-12 control-label" for="w4-first-name" style="text-align: left;">Numero Sub Partida</label>
										<div class="col-md-12">
											<input type="text" class="form-control field-input {{ $errors->has('numero_subpartida') ? 'field-error' : '' }}" name="numero_subpartida" id="numero_subpartida" value="{{ old('numero_subpartida') }}" required placeholder="N° Partida">
											@if ($errors->has('numero_subpartida'))<span class="error-message">{{ $errors->first('numero_subpartida') }}</span>
											@endif
										</div>
									</div>
								</div>
								<div class="form-group"></div>
								<div class="col-md-1"></div>
								<div class="col-md-11">
									<div class="col-md-5">
										<!--Cifra Presupuestaria-->
										<label class="col-md-12 control-label" for="w4-first-name" style="text-align: left;">Cifra Presupuestaria</label>
										<div class="col-md-12">
											<input type="text" class="form-control field-input {{ $errors->has('cifra_presupuestaria') ? 'field-error' : '' }}" name="cifra_presupuestaria" id="cifra_presupuestaria" value="{{ old('cifra_presupuestaria') }}" required placeholder="Cifra Presupuestaria">
											@if ($errors->has('cifra_presupuestaria'))<span class="error-message">{{ $errors->first('cifra_presupuestaria') }}</span>
											@endif
										</div>
									</div>
									<div class="col-md-5">
										<!--Gifcards-->
										<label class="col-md-12 control-label" for="w4-first-name" style="text-align: left;">GifCards</label>
										<div class="col-md-12">
											<input type="text" class="form-control field-input {{ $errors->has('gifcard') ? 'field-error' : '' }}" name="gifcard" id="gifcard" value="{{ old('gifcard') }}"  placeholder="GifCards">
											@if ($errors->has('gifcard'))<span class="error-message">{{ $errors->first('gifcard') }}</span>
											@endif
										</div>
									</div>
								</div>					
								<div class="form-group"></div>
								<div class="form-group">
									<div class="col-lg-12 text-center" >    
										<button  class="btn btn-default" id="anterior4">Anterior</button>
										<button type="submit" class="btn btn-primary" id="guardar4">Siguiente</button>
									</div>
								</div>
							</div>

							<form class="form-horizontal" method="post" action="{{route('empleados.guardarEstadoEmpleado')}}">
							@csrf         
							<div id="status" style="display:none">
								<input type="hidden" id="id_emp" name="id_emp" value="">
								<div class="form-group">
									<div class="col-md-2"></div>
									<div class="col-md-2">
										<!--Afiliado Sindicato-->
										<div class="checkbox-custom checkbox-default">
											<input type="checkbox" id="afiliado_sindicato" name="afiliado_sindicato">
											<label for="w4-terms">Afiliado Sindicato</label>
										</div>
										<!--Rendir Probidad-->
										<div class="checkbox-custom checkbox-default">
											<input type="checkbox"  id="rendir_probidad" name="rendir_probidad">
											<label for="w4-terms">Rendir Probidad</label>
										</div>
										<!--Es Jefe-->
										<div class="checkbox-custom checkbox-default">
											<input type="checkbox"  id="es_jefe" name="es_jefe">
											<label for="w4-terms">Es Jefe</label>
										</div>
									</div>
									<div class="col-md-1"></div>
									<div class="col-md-2">
										<!--Marca-->
										<div class="checkbox-custom checkbox-default">
											<input type="checkbox"  id="marca" name="marca">
											<label for="w4-terms">Marca</label>
										</div>
										<!--Autorizado Estudio-->
										<div class="checkbox-custom checkbox-default">
											<input type="checkbox" id="autorizado_estudio" name="autorizado_estudio">
											<label for="w4-terms">Autorizado Estudio</label>
										</div>
										<!--Pensionado-->
										<div class="checkbox-custom checkbox-default">
											<input type="checkbox" id="pensionado" name="pensionado">
											<label for="w4-terms">Pensionado</label>
										</div>
									</div>
									<div class="col-md-1"></div>
									<div class="col-md-2">
										<!--Enviar Jefe-->
										<div class="checkbox-custom checkbox-default">
											<input type="checkbox"  id="enviar_jefe" name="enviar_jefe">
											<label for="w4-terms">Enviar Jefe</label>
										</div>
										<!--Activo-->
										<div class="checkbox-custom checkbox-default">
											<input type="checkbox"  id="activo" name="activo" checked="checked" disabled>
											<label for="w4-terms">Activo</label>
										</div>
										<!--Pasivo-->
										<div class="checkbox-custom">
											<input type="checkbox"  id="pasivo" name="pasivo" disabled>
											<label for="w4-terms">Pasivo</label>
										</div>
									</div>
								</div>
								<div class="form-group"></div>
								<div class="form-group">
									<div class="col-lg-12 text-center" >    
										<button  class="btn btn-default" id="anterior5">Anterior</button>
										<button type="submit" class="btn btn-primary" id="guardar5">Guardar</button>
									</div>
								</div>
							</div>
							

						</form>
					</div>
				</section>
			</div>
		</div>
		<!-- end: page -->
	</section>
	@endsection
	@section('js')

	<script type="text/javascript">
		$(document).ready(function() {

			$("#guardar1").on("click",function(e){
				e.preventDefault();
				var id = $("#id_empleado").val();
				var pin_empleado =$("#pin_empleado").val();
				var numero_expediente =$("#numero_expediente").val();
				var codigo_empleado =$("#codigo_empleado").val();
				var nombre =$("#nombre").val();
				var apellido =$("#apellido").val();
				var estadoCivil =$("#estadoCivil").val();
				var fecha_nacimiento =$("#fecha_nacimiento").val();
				var sexo =$("#sexo").val();
				var lugar_nacimiento =$("#lugar_nacimiento").val();
				var correo =$("#correo").val();
				var telefono =$("#telefono").val();
				var direccion =$("#direccion").val();
				var municipio_id =$("#municipio_id").val();
				var _token = $("input[name='_token']").val();
				if($("#id_empleado").val()==""){
					$.ajax({
					type:'POST',
					url: "{{route('empleados.guardarInformacionPersonal')}}",
					data: {
							pin_empleado:pin_empleado,
							numero_expediente:numero_expediente,
							codigo_empleado:codigo_empleado,
							nombre:nombre,
							apellido:apellido,
							estadoCivil:estadoCivil,
							fecha_nacimiento:fecha_nacimiento,
							sexo:sexo,
							lugar_nacimiento:lugar_nacimiento,
							correo:correo,
							telefono:telefono,
							direccion:direccion,
							municipio_id:municipio_id,
							_token:_token,
						},
					success:function(data){
						$("#personal").hide();
						$("#document").show();
						$("#alertaError").hide();
						$("#id_empleado").val(data.id);
						$("#titulo").empty();
						$("#titulo").append("Documentos Personales");
					},
					error:function(data){
						var errores = data.responseJSON.errors;
						var msjError ="";
						Object.values(errores).forEach(function(valor){
							msjError +='<li>'+valor[0]+'</li>';
						});
						$("#listaErrores").html(msjError);
						$("#alertaError").show();
					}
				});
				}else{
					$.ajax({
					type:'PUT',
					url: "/empleados/editar/"+id+"/informacionPersonal",
					data: {
							id:id,
							pin_empleado:pin_empleado,
							numero_expediente:numero_expediente,
							codigo_empleado:codigo_empleado,
							nombre:nombre,
							apellido:apellido,
							estadoCivil:estadoCivil,
							fecha_nacimiento:fecha_nacimiento,
							sexo:sexo,
							lugar_nacimiento:lugar_nacimiento,
							correo:correo,
							telefono:telefono,
							direccion:direccion,
							municipio_id:municipio_id,
							_token:_token,
						},
					success:function(data){
						$("#personal").hide();
						$("#document").show();
						$("#alertaError").hide();
						$("#id_empleado").val(data.id);
						$("#titulo").empty();
						$("#titulo").append("Documentos Personales");
					},
					error:function(data){
						var errores = data.responseJSON.errors;
						var msjError ="";
						Object.values(errores).forEach(function(valor){
							msjError +='<li>'+valor[0]+'</li>';
						});
						$("#listaErrores").html(msjError);
						$("#alertaError").show();
					}
				});
				}
			});

			$("#anterior2").on("click",function(e){
				e.preventDefault();
				$("#document").hide();
				$("#personal").show();
				$("#titulo").empty();
				$("#titulo").append("Informacion Personal");
			});
			$("#guardar2").on("click",function(e){
				e.preventDefault();
				var id = $("#id_empleado").val();
				var tipo_documento =$("#tipo_documento").val();
				var numero_documento =$("#numero_documento").val();
				var nit =$("#nit").val();
				var prevision =$("#prevision").val();
				var nup =$("#nup").val();
				var isss =$("#isss").val();
				var nivelAcademico =$("#nivelAcademico").val();
				var nombre_contacto =$("#nombre_contacto").val();
				var telefono_contacto =$("#telefono_contacto").val();
				var _token = $("input[name='_token']").val();
				if($("#document-edit").val()==""){
					var url = "/empleados/guardar/"+id+"/documentosPersonales";
				}
				else{
					var url="/empleados/editar/"+id+"/documentosPersonales";
				}
				$.ajax({
					type:'PUT',
					url: url,
					data: {
							id:id,
							tipo_documento:tipo_documento,
							numero_documento:numero_documento,
							nit:nit,
							prevision:prevision,
							nup:nup,
							isss:isss,
							nivelAcademico:nivelAcademico,
							nombre_contacto:nombre_contacto,
							telefono_contacto:telefono_contacto,
							_token:_token,
						},
					success:function(data){
						$("#document").hide();
						$("#jobs").show();
						$("#alertaError").hide();
						$("#titulo").empty();
						$("#titulo").append("Informacion Laboral");
						$("#document-edit").val("guardado");
					},
					error:function(data){
						console.log(data.responseJSON);
						var errores = data.responseJSON.errors;
						var msjError ="";
						Object.values(errores).forEach(function(valor){
							msjError +='<li>'+valor[0]+'</li>';
						});
						$("#listaErrores").html(msjError);
						$("#alertaError").show();
					}
				});	
			});

			$("#anterior3").on("click",function(e){
				e.preventDefault();
				$("#jobs").hide();
				$("#document").show();
				$("#titulo").empty();
				$("#titulo").append("Documentos Personales");
			});
			$("#guardar3").on("click",function(e){
				e.preventDefault();
				var id = $("#id_empleado").val();
				var centro_id =$("#centro_id").val();
				var unidad_id =$("#unidad_id").val();
				var oficina_id =$("#oficina_id").val();
				var fecha_ingreso =$("#fecha_ingreso").val();
				var salario_actual =$("#salario_actual").val();
				var cargoF =$("#cargoF").val();
				var cargoMP =$("#cargoMP").val();
				var cargoN =$("#cargoN").val();
				var cargoHacienda =$("#cargoHacienda").val();
				var _token = $("input[name='_token']").val();
				// if($("#jobs-edit").val()==""){
				// 	var url = "/empleados/guardar/"+id+"/informacionLaboral";
				// }
				// else{
				// 	var url="/empleados/editar/"+id+"/informacionLaboral";
				// }

				$.ajax({
					type:'PUT',
					url: "/empleados/guardar/"+id+"/informacionLaboral",
					data: {
							id:id,
							centro_id:centro_id,
							unidad_id:unidad_id,
							oficina_id:oficina_id,
							fecha_ingreso:fecha_ingreso,
							salario_actual:salario_actual,
							cargoF:cargoF,
							cargoMP:cargoMP,
							cargoN:cargoN,
							cargoHacienda:cargoHacienda,
							_token:_token,
						},
					success:function(data){
						$("#jobs").hide();
						$("#bank").show();
						$("#alertaError").hide();
						$("#titulo").empty();
						$("#titulo").append("Informacion Financiera");
						$("#jobs-edit").val("guardado");
					},
					error:function(data){
						console.log(data.responseJSON);
						var errores = data.responseJSON.errors;
						var msjError ="";
						Object.values(errores).forEach(function(valor){
							msjError +='<li>'+valor[0]+'</li>';
						});
						$("#listaErrores").html(msjError);
						$("#alertaError").show();
					}
				});	
			});

			$("#anterior4").on("click",function(e){
				e.preventDefault();
				$("#bank").hide();
				$("#jobs").show();
				$("#titulo").empty();
				$("#titulo").append("Informacion Laboral");
			});
			$("#guardar4").on("click",function(e){
				e.preventDefault();
				var id = $("#id_empleado").val();
				var banco =$("#banco").val();
				var cuenta_banco =$("#cuenta_banco").val();
				var tipoPlaza =$("#tipoPlaza").val();
				var pin_empleado =$("#pin_empleado").val();
				var numero_expediente =$("#numero_expediente").val();
				var numero_partida =$("#numero_partida").val();
				var numero_acuerdo =$("#numero_acuerdo").val();
				var numero_subpartida =$("#numero_subpartida").val();
				var cifra_presupuestaria =$("#cifra_presupuestaria").val();
				var gifcard =$("#gifcard").val();
				var _token = $("input[name='_token']").val();
				if($("#bank-edit").val()==""){
					var url = "/empleados/guardar/"+id+"/informacionFinanciera";
				}
				else{
					var url="/empleados/editar/"+id+"/informacionFinanciera";
				}
				$.ajax({
					type:'PUT',
					url: url,
					data: {
							id:id,
							banco:banco,
							cuenta_banco:cuenta_banco,
							tipoPlaza:tipoPlaza,
							pin_empleado:pin_empleado,
							numero_expediente:numero_expediente,
							numero_partida:numero_partida,
							numero_acuerdo:numero_acuerdo,
							numero_subpartida:numero_subpartida,
							cifra_presupuestaria:cifra_presupuestaria,
							gifcard:gifcard,
							_token:_token,
						},
					success:function(data){
						$("#bank").hide();
						$("#status").show();
						$("#alertaError").hide();
						$("#titulo").empty();
						$("#titulo").append("Estado de Empleado");
						$("#bank-edit").val("guardado");
						$("#id_emp").val(data.id);
					},
					error:function(data){
						console.log(data.responseJSON);
						var errores = data.responseJSON.errors;
						var msjError ="";
						Object.values(errores).forEach(function(valor){
							msjError +='<li>'+valor[0]+'</li>';
						});
						$("#listaErrores").html(msjError);
						$("#alertaError").show();
					}
				});	
			});

			$("#anterior5").on("click",function(e){
				e.preventDefault();
				$("#status").hide();
				$("#bank").show();
				$("#titulo").empty();
				$("#titulo").append("Informacion Laboral");
			});

		});
	//});
	</script>

	<script type="text/javascript">
		$(document).ready(function() {
			function cargarMuncipio(){
				var departamento_id = $('#departamento_id').val();
				if($.trim('departamento_id') != ''){
					$.get('/municipios', {departamento:  departamento_id}, function (municipios){
						$('#municipio_id').empty();
						$('#municipio_id').append("<option value=''>Selecciona un Municipio </option>");
						$.each(municipios, function(index, value){
							$('#municipio_id').append("<option value='" + index +"'>"+value+ "</option>");
						})

					});
				}
			}
			cargarMuncipio();
			$('#departamento_id').on('change', cargarMuncipio);
		});
	</script>

	<script type="text/javascript">

		$(document).ready(function() {
			function cargarOficinas(){

				var unidad_id = $('#unidad_id').val();
				var centro_id = $('#centro_id').val();
				if($.trim('unidad_id') != '' &&  $.trim('centro_id') != '' ){
					$.get('/seleccionarOficina', {unidad_administrativa:  unidad_id, centro: centro_id},   function (oficinas){
						
								$('#oficina_id').empty();
								$('#oficina_id').append("<option value=''>Selecciona una Oficina </option>");
								$.each(oficinas, function(index, value){
								$('#oficina_id').append("<option value='" + index +"'>"+value+ "</option>");
								})
							
						
					});
				}
			}
			cargarOficinas();
			$('#unidad_id').on('change', cargarOficinas);
			$('#centro_id').on('change', cargarOficinas);
		});

	</script>

	<script>
		$(document).ready(function(){
			$('#telefono').mask('9999-9999');
		});
	</script>
	<script>
		$(document).ready(function(){
			$('#telefono-contacto').mask('9999-9999');
		});
	</script>

	<script>
		$(document).ready(function(){
			$('#nit').mask('9999-999999-999-9');
		});
	</script>


	@endsection

