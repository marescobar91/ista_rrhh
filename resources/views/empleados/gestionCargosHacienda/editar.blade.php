@extends('layouts.template')
@section('content')
<section role="main" class="content-body">
    <header class="page-header">
        <h2>Gestión de Cargos Hacienda</h2>
        <div class="right-wrapper pull-right">
            <ol class="breadcrumbs">
                <li style="margin-left: -25%"><a href="{{url('/')}}"><button class="btn btn-default"><span class="fa fa-chevron-left"> </span> Regresar </button></a></li>
            </ol>
        </div>
    </header>

    <!-- start: page -->
    <div class="row">
        <div class="col-lg-12">
            <div>
                @if ($errors->any())
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    
                    <div class="errors">
                        <ul>
                            @foreach ($errors->all() as $error)

                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif

                </div>
            </div>
            <section class="panel">
                <header class="panel-heading">
                    <div class="panel-actions">
                    </div>
                    <h2 class="panel-title">Editar Cargos de Hacienda</h2>
                </header>
                <div class="panel-body">
                  <form action="{{ route('cargosHacienda.actualizar', $cargo_hacienda->id_cargo_hacienda)}}" method="post" role="form" class="contactForm">
                    {{csrf_field()}}
                    {{ method_field('PUT') }} 
                    <div class="form-group">
                            <div class="col-md-3"></div>
                            <div class="col-md-6">
                                <label class="col-md-12 control-label" style="text-align: left;">Nombre Cargo Hacienda</label>
                                <div class="col-md-12">
                                    <input id="cargo_hacienda" type="text" value="{{ $cargo_hacienda->cargo_hacienda}}" name="cargo_hacienda" required autocomplete="cargo_hacienda" autofocus placeholder="Nombre del Cargo de Hacienda" class="form-control field-input {{ $errors->has('cargo_hacienda') ? 'field-error' : '' }}">
                                    @if ($errors->has('cargo_hacienda'))<span class="error-message">{{ $errors->first('cargo_hacienda') }}</span>
                                    @endif
                                </div>
                            </div>
                        </div> 
                    <div class="col-lg-12 text-center" >    
                     <button type="submit" class="btn btn-warning">Editar</button>
                     <a href="{{route('home')}}" class="btn btn-default">Cancelar</a>
                 </div>                              
             </form>
         </div>
     </section>
     
 </div>
</div>
<!-- end: page -->
</section>

@endsection
