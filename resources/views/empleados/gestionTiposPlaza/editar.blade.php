@extends('layouts.template')
@section('content')
<section role="main" class="content-body">
    <header class="page-header">
        <h2>Gestión de Tipos de Plaza</h2>
        <div class="right-wrapper pull-right">
            <ol class="breadcrumbs">
                <li style="margin-left: -25%"><a href="{{url('/')}}"><button class="btn btn-default"><span class="fa fa-chevron-left"> </span> Regresar </button></a></li>
            </ol>
        </div>
    </header>

    <!-- start: page -->
    <div class="row">
        <div class="col-lg-12">
            <div>
                @if ($errors->any())
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                    <div class="errors">
                        <ul>
                            @foreach ($errors->all() as $error)

                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif

                </div>
            </div>
            <section class="panel">
                <header class="panel-heading">
                    <div class="panel-actions">
                    </div>
                    <h2 class="panel-title">Editar Tipos de Plaza</h2>
                </header>
                <div class="panel-body">
                  <form action="{{ route('tiposPlaza.actualizar', $tipoPlaza->id_tipo_plaza) }}" method="post" role="form" class="contactForm">
                    {{csrf_field()}}
                    {{ method_field('PUT') }}
                     <div class="form-group">
                            <div class="col-md-2"></div>
                            <div class="col-md-8">
                                <label class="col-md-12 control-label text-right">Tipo de Plaza</label>
                            <div class="col-md-12">
                                <input id="tipo_plaza" type="text" value="{{ $tipoPlaza->tipo_plaza }}" name="tipo_plaza" required autocomplete="tipo_plaza" autofocus placeholder="Nombre del Tipo de Plaza" class="form-control field-input {{ $errors->has('tipo_plaza') ? 'field-error' : '' }}">
                                @if ($errors->has('tipo_plaza'))<span class="error-message">{{ $errors->first('tipo_plaza') }}</span>
                                @endif
                            </div>
                            </div>
                            
                        </div>  
                    <div class="col-lg-12 text-center" >    
                     <button type="submit" class="btn btn-warning">Editar</button>
                     <a href="{{route('home')}}" class="btn btn-default">Cancelar</a>
                 </div>                              
             </form>
         </div>
     </section>

 </div>
</div>
<!-- end: page -->
</section>

@endsection
