@extends('layouts.template')
@section('content')
<section role="main" class="content-body">
    <header class="page-header">
        <h2>Gestión de Oficinas</h2>
        <div class="right-wrapper pull-right">
            <ol class="breadcrumbs">
                <li style="margin-left: -25%"><a href="{{url('/')}}"><button class="btn btn-default"><span class="fa fa-chevron-left"> </span> Regresar </button></a></li>
            </ol>
        </div>
    </header>

    <!-- start: page -->
    <div class="row">
        <div class="col-lg-12">
            <div>
                @if ($errors->any())
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    
                    <div class="errors">
                        <ul>
                            @foreach ($errors->all() as $error)

                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif

                </div>
            </div>
            <section class="panel">
                <header class="panel-heading">
                    <div class="panel-actions">
                    </div>
                    <h2 class="panel-title">Editar Oficina</h2>
                </header>
                <div class="panel-body">
                  <form action="{{ route('oficinas.actualizar', $oficina->id_oficina) }}" method="post" role="form" class="contactForm">
                    {{csrf_field()}}
                    {{ method_field('PUT') }}

                    <div class="form-group">
                        <div class="col-md-2"></div>
                        <div class="col-md-8">
                             <label class="control-label col-md-12">Unidad Administrativa</label>
                        <div class="col-md-12">
                            <select data-plugin-selectTwo name="unidad_administrativa" class="form-group col-md-12" required>
                                <option value="">Seleccion Unidad Administrativa</option>
                                @foreach($unidadAdministrativa as $unidades)
                                <option value="{{$unidades->id_unidad_administrativa}}" @if($unidades->id_unidad_administrativa==$oficina->unidad_administrativa_id)
                                    selected='selected' @endif>{{$unidades->unidad_administrativa}} </option>
                                    @endforeach
                                </select>

                                @if ($errors->has('unidad_administrativa'))<span class="error-message">{{ $errors->first('unidad_administrativa') }}</span>
                                @endif
                            </div>
                        </div>
                       
                        </div>
                        <div class="form-group">
                            <div class="col-md-2"></div>
                            <div class="col-md-8">
                                <label class="control-label col-md-12" style="text-align: left;">Centro</label>
                            <div class="col-md-12">
                                <select data-plugin-selectTwo name="centro" class="form-group col-md-12" required>
                                    <option value=""selected>Seleccione el Centro</option>
                                    @foreach($centro as $centro)
                                    
                                    <option value="{{$centro->id_centro}}" @if($centro->id_centro==$oficina->centro_id) selected="selected" @endif>{{ $centro->centro }}</option>
                                    
                                    @endforeach
                                </select>
                            </div>
                                @if ($errors->has('centro'))<span class="error-message">{{ $errors->first('centro') }}</span>
                                @endif
                           
                            </div>
                            
                        </div>


                        <div class="form-group">
                            <div class="col-md-2"></div>
                            <div class="col-md-8">
                                 <label class="col-md-12 control-label" style="text-align: left;">Oficina</label>
                            <div class="col-md-12">
                                <input id="oficina" type="text" value="{{ $oficina->oficina }}" name="oficina" required autocomplete="oficina" autofocus placeholder="Oficina" class="form-control field-input {{ $errors->has('oficina') ? 'field-error' : '' }}">
                                @if ($errors->has('oficina'))<span class="error-message">{{ $errors->first('oficina') }}</span>
                                @endif
                            </div>
                            </div>
                           
                        </div> 
                         <div class="form-group">
                        <div class="col-md-2"></div>
                        <div class="col-md-8">
                            <label class="col-md-12 control-label" style="text-align: left;">Teléfono Oficina</label>
                        <div class="col-md-12">
                            <input type="tel" name="telefono_oficina" id="telefono_oficina" pattern="[0-9]{4}[ -][0-9]{4}" title="Un número de teléfono válido consiste en cutro números de telefono, un guión (-), y otros cuatro números más" value="{{ $oficina->telefono_oficina }}" name="telefono_oficina" required autocomplete="telefono_oficina" autofocus placeholder="Teléfono de Oficina" class="form-control field-input {{ $errors->has('telefono_oficina') ? 'field-error' : '' }}">
                            @if ($errors->has('telefono_oficina'))<span class="error-message">{{ $errors->first('telefono_oficina') }}</span>
                            @endif
                        </div>
                        </div>
                        
                    </div> 
                        <div class="col-lg-12 text-center" >    
                         <button type="submit" class="btn btn-warning">Editar</button>
                         <a href="{{route('home')}}" class="btn btn-default">Cancelar</a>
                     </div>                              
                 </form>
             </div>
         </section>
         
     </div>
 </div>
 <!-- end: page -->
</section>

@endsection
