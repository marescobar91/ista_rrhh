@extends('layouts.template')
@section('content')
<section role="main" class="content-body">
    <header class="page-header">
        <h2>Gestión de Oficinas</h2>
        <div class="right-wrapper pull-right">
            <ol class="breadcrumbs">
                <li style="margin-left: -25%"><a href="{{url('/')}}"><button class="btn btn-default"><span class="fa fa-chevron-left"> </span> Regresar </button></a></li>
            </ol>
        </div>
    </header>

    <!-- start: page -->
    <div class="row">
        <div class="col-lg-12">
            <div>
                @if ($errors->any())
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <div class="errors">
                        <ul>
                            @foreach ($errors->all() as $error)

                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif

                </div>
            </div>
            <section class="panel">
                <header class="panel-heading">
                    <div class="panel-actions">
                    </div>
                    <h2 class="panel-title">Crear Oficina</h2>
                </header>
                <div class="panel-body">
                    <form class="form-horizontal form-bordered" method="POST" action="{{route('oficinas.guardar')}}">
                        @csrf      

                        <div class="form-group">
                            <div class="col-md-2"></div>
                            <div class="col-md-8">
                                <label class="control-label col-md-12" style="text-align: left;">Unidad Administrativa</label>
                            
                                <select data-plugin-selectTwo name="unidad_administrativa" class="form-group col-md-12" required>
                                    <option value=""selected>Seleccione la Unidad Administrativa</option>
                                    @foreach($unidadAdministrativa as $unidad)
                                    
                                    <option value="{{ $unidad->id_unidad_administrativa }}" >{{ $unidad->unidad_administrativa }}</option>
                                    
                                    @endforeach
                                </select>

                                @if ($errors->has('unidad_administrativa'))<span class="error-message">{{ $errors->first('unidad_administrativa') }}</span>
                                @endif
                           
                            </div>
                            
                        </div>
                        <div class="form-group">
                            <div class="col-md-2"></div>
                            <div class="col-md-8">
                                <label class="control-label col-md-12" style="text-align: left;">Centro</label>
                            
                                <select data-plugin-selectTwo name="centro" class="form-group col-md-12" required>
                                    <option value=""selected>Seleccione el Centro</option>
                                    @foreach($centro as $centro)
                                    
                                    <option value="{{ $centro->id_centro }}" >{{ $centro->centro }}</option>
                                    
                                    @endforeach
                                </select>

                                @if ($errors->has('centro'))<span class="error-message">{{ $errors->first('centro') }}</span>
                                @endif
                           
                            </div>
                            
                        </div>

                        <div class="form-group">
                            <div class="col-md-2"></div>
                            <div class="col-md-8">
                                <label class="col-md-12 control-label" style="text-align: left;">Nombre Oficina</label>
                                <div class="col-md-12">
                                    <input id="oficina" type="text" value="{{ old('oficina') }}" name="oficina" required autocomplete="oficina" autofocus placeholder="Oficina" class="form-control field-input {{ $errors->has('oficina') ? 'field-error' : '' }}">
                                    @if ($errors->has('oficina'))<span class="error-message">{{ $errors->first('oficina') }}</span>
                                    @endif
                                </div>
                            </div>
                            
                        </div> 

                         <div class="form-group">
                            <div class="col-md-2"></div>
                            <div class="col-md-8">
                                <label class="col-md-12 control-label " style="text-align: left;">Teléfono Oficina</label>
                            <div class="col-md-12">
                                <input type="tel" name="telefono_oficina" id="telefono_oficina" pattern="[0-9]{4}[ -][0-9]{4}" title="Fomato solicitado ####-####" value="{{ old('telefono_oficina') }}" name="telefono_oficina" required autocomplete="telefono_oficina" autofocus placeholder="Teléfono de Oficina" class="form-control field-input {{ $errors->has('telefono_oficina') ? 'field-error' : '' }}">
                                @if ($errors->has('telefono_oficina'))<span class="error-message">{{ $errors->first('telefono_oficina') }}</span>
                                @endif
                            </div>
                            </div>
                            
                        </div>
                        
                        <div class="col-lg-12 text-center" >    
                         <button type="submit" class="btn btn-primary">Guardar</button>
                         <a href="{{route('home')}}" class="btn btn-default">Cancelar</a>
                     </div>                              
                 </form>
             </div>
         </section>
         
     </div>
 </div>
 <!-- end: page -->
</section>

@endsection
@section('js')
<script>
        $(document).ready(function(){
            $('#telefono_oficina').mask('9999-9999');
        });
    </script>
@endsection