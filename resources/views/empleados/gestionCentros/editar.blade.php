@extends('layouts.template')
@section('content')
<section role="main" class="content-body">
    <header class="page-header">
        <h2>Gestión de Centros</h2>
        <div class="right-wrapper pull-right">
            <ol class="breadcrumbs">
                <li style="margin-left: -25%"><a href="{{url('/')}}"><button class="btn btn-default"><span class="fa fa-chevron-left"> </span> Regresar </button></a></li>
            </ol>
        </div>
    </header>

    <!-- start: page -->
    <div class="row">
        <div class="col-lg-12">
            <div>
                @if ($errors->any())
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                    <div class="errors">
                        <ul>
                            @foreach ($errors->all() as $error)

                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif

                </div>
            </div>
            <section class="panel">
                <header class="panel-heading">
                    <div class="panel-actions">
                    </div>
                    <h2 class="panel-title">Editar Centros</h2>
                </header>
                <div class="panel-body">
                  <form action="{{ route('centros.actualizar', $centro->id_centro) }}" method="post" role="form" class="contactForm">
                    {{csrf_field()}}
                    {{ method_field('PUT') }}
                    <div class="form-group">
                            <div class="col-md-3"></div>
                            <div class="col-md-6">
                                <label class="col-sm-12 control-label text-left">Unidad Administrativa</label>
                            <div class="col-md-12">
                                <input id="centro" type="text" value="{{ $centro->centro }}" name="centro" required autocomplete="centro" autofocus placeholder="Unidad Administrativa" class="form-control field-input {{ $errors->has('centro') ? 'field-error' : '' }}">
                                @if ($errors->has('centro'))<span class="error-message">{{ $errors->first('centro') }}</span>
                                @endif
                            </div>
                            </div>
                            
                        </div> 
                        <div class="form-group">
                            <div class="col-md-3"></div>
                            <div class="col-md-6">
                                <label class="col-sm-12 control-label text-left">Centro</label>
                            <div class="col-md-12">
                                <input id="abreviatura" type="text" value="{{ $centro->abreviatura }}" name="abreviatura" required autocomplete="abreviatura" autofocus placeholder="Abreviatura" class="form-control field-input {{ $errors->has('abreviatura') ? 'field-error' : '' }}">
                                @if ($errors->has('abreviatura'))<span class="error-message">{{ $errors->first('abreviatura') }}</span>
                                @endif
                            </div>
                            </div>
                            
                        </div> 
                         <div class="form-group">
                            <div class="col-md-3"></div>
                            <div class="col-md-6">
                                <label class="col-sm-12 control-label" style="text-align: left;">Encargado</label>
                            <div class="col-md-12">
                                <input id="encargado" type="text" value="{{ $centro->encargado}}" name="encargado" required autocomplete="encargado" autofocus placeholder="Encargado" class="form-control field-input {{ $errors->has('encargado') ? 'field-error' : '' }}">
                                @if ($errors->has('encargado'))<span class="error-message">{{ $errors->first('encargado') }}</span>
                                @endif
                            </div>
                            </div>
                            
                        </div>

                     
                    <div class="col-lg-12 text-center" >    
                     <button type="submit" class="btn btn-warning">Editar</button>
                     <a href="{{route('home')}}" class="btn btn-default">Cancelar</a>
                 </div>                              
             </form>
         </div>
     </section>

 </div>
</div>
<!-- end: page -->
</section>

@endsection