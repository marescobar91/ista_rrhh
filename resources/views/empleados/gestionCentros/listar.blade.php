@extends('layouts.template')
@section('content')
<section role="main" class="content-body">
    <header class="page-header">
        <h2>Gestión de Centros</h2>
        <div class="right-wrapper pull-right">
            <ol class="breadcrumbs">
                <li style="margin-left: -25%"><a href="{{route('admin.catalogos')}}"><button class="btn btn-default"><span class="fa fa-chevron-left"> </span> Regresar </button></a></li>
            </ol>
        </div>
    </header>

    <!-- start: page -->
    <div class="row">
        <div class="col-lg-12">
            <div>
                @if ($errors->any())
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                    <div class="errors">
                        <ul>
                            @foreach ($errors->all() as $error)

                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif

                </div>
            </div>
            <section class="panel">
                @include('flash::message')
                <header class="panel-heading">
                    
                    <div class="panel-actions">
                        <a href="{{route('centros.crear')}}"><button class="btn btn-primary"><span class="fa fa-plus"></span> Agregar Centro</button></a>

                                  <!--  <a href="#" class="fa fa-caret-down"></a>
                                    <a href="#" class="fa fa-times"></a>-->
                                </div>
                              

                                <h2 class="panel-title">Listado de Centros</h2>
                            </header>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="tabs">
                                        <ul class="nav nav-tabs nav-justified">
                                            <li class="active">
                                                <a href="#popular10" data-toggle="tab" class="text-center"><i class="fa fa-star"></i> Habilitados</a>
                                            </li>
                                            <li>
                                                <a href="#recent10" data-toggle="tab" class="text-center">Deshabilitados</a>
                                            </li>
                                        </ul>
                                        <div class="tab-content">
                                            <div id="popular10" class="tab-pane active">
                                               <div class="panel-body">
                                                <table class="table table-bordered table-striped mb-none" id="datatable-default">
                                                    <thead>
                                                        <tr>
                                                            <th>Centros</th>
                                                            <th>Abreviaturas</th>
                                                             <th>Encargado</th>
                                                            <th class="col-md-2">Accion</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                       @foreach($centros as $centro)
                                                       <tr class="gradeX" >
                                                        <td>{{$centro->centro}}</td>
                                                        <td>{{$centro->abreviatura}}</td>
                                                        <td>{{$centro->encargado}}</td>
                                                        <td class="text-center">
                                                           
                                                            <a href="{{ route('centros.editar', $centro->id_centro) }}" class="btn btn-warning" ><span class="fa fa-pencil"></span></a>
                                                           
                                                            <a href="{{ route('centros.eliminar', $centro->id_centro) }}" class="btn btn-danger eliminar"  ><span class="fa fa-trash-o"></span></a>
                                                            
                                                        </td>
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div id="recent10" class="tab-pane">
                                        <div class="panel-body">
                                            <table class="table table-bordered table-striped mb-none" id="datatable-pestana">
                                                <thead>
                                                    <tr>
                                                        <th>Centro</th>
                                                        <th>Abreviatura</th>
                                                        <th>Encargado</th>
                                                        <th>Accion</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                   @foreach($centroDeshabilitado as $centro)
                                                   <tr class="gradeX" >
                                                    <td>{{$centro->centro}}</td>
                                                    <td>{{$centro->abreviatura}}</td>
                                                     <td>{{$centro->encargado}}</td>
                                                    <td class="text-center col-md-2" >
                                                        
                                                        <a href="{{ route('centros.habilitar', $centro->id_centro) }}" class="btn btn-primary habilitar"  ><span class="fa fa-caret-up"></span></a>
                                                        
                                                    </td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
    <!-- end: page -->
</section>

@endsection
