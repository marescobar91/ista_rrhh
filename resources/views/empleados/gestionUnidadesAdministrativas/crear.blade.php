@extends('layouts.template')
@section('content')
<section role="main" class="content-body">
    <header class="page-header">
        <h2>Gestión de Unidades Administrativas</h2>
    </header>

    <!-- start: page -->
    <div class="row">
        <div class="col-lg-12">
            <div>
                @if ($errors->any())
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                    <div class="errors">
                        <ul>
                            @foreach ($errors->all() as $error)

                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif

                </div>
            </div>
            <section class="panel">
                <header class="panel-heading">
                    <div class="panel-actions">
                    </div>
                    <h2 class="panel-title">Crear Unidad Administrativa</h2>
                </header>
                <div class="panel-body">
                    <form class="form-horizontal form-bordered" method="POST" action="{{route('unidadesAdministrativas.guardar')}}">
                        @csrf                        
                        <div class="form-group">
                            <div class="col-md-2"></div>
                            <div class="col-md-8">
                                <label class="col-sm-2 control-label text-right">Unidad Administrativa</label>
                            <div class="col-md-6">
                                <input id="unidad_administrativa" type="text" value="{{ old('unidad_administrativa') }}" name="unidad_administrativa" required autocomplete="unidad_administrativa" autofocus placeholder="Unidad Administrativa" class="form-control field-input {{ $errors->has('unidad_administrativa') ? 'field-error' : '' }}">
                                @if ($errors->has('unidad_administrativa'))<span class="error-message">{{ $errors->first('unidad_administrativa') }}</span>
                                @endif
                            </div>
                            </div>
                            
                        </div> 

                        
                        <div class="col-lg-12 text-center" >    
                         <button type="submit" class="btn btn-primary">Guardar</button>
                         <a href="{{route('home')}}" class="btn btn-default">Cancelar</a>
                     </div>                              
                 </form>
             </div>
         </section>

     </div>
 </div>
 <!-- end: page -->
</section>

@endsection