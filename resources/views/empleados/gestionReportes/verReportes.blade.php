@extends('layouts.template')
@section('content')
<section role="main" class="content-body">
    <header class="page-header">
        <h2>Reportes de Empleados</h2>
        <div class="right-wrapper pull-right">
            <ol class="breadcrumbs">
                <li style="margin-left: -25%"><a href="{{url('/')}}"><button class="btn btn-default"><span class="fa fa-chevron-left"> </span> Regresar </button></a></li>
            </ol>
        </div>
    </header>

    <!-- start: page -->
    <div class="row">
        <div class="col-lg-12">
            <div>
                @if ($errors->any())
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    
                    <div class="errors">
                        <ul>
                            @foreach ($errors->all() as $error)

                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif

                </div>
            </div>
            <section class="panel">
                <header class="panel-heading">
                    <div class="panel-actions">
                    </div>
                    <h2 class="panel-title">Listado de Reportes</h2>
                </header>
                <div class="panel-body ">
                    @can('consultar.reportes')
                    <div class="col-md-6" >
                        <section class="panel panel-primary">
                            <header class="panel-heading">
                                 <a href="{{route('reporte.jefatura')}}"><h2 class="panel-title">Listado de Jefaturas</h2></a>     
                            </header>
                        </section>
                        <section class="panel panel-primary">
                            <header class="panel-heading">
                                <a href="{{ route('reporte.unidad') }}"><h2 class="panel-title"> Listado por Unidad Organizativa </h2></a>
                            </header>
                        </section>
                    </div>
                    <div class="col-md-6" >
                        <section class="panel panel-primary">
                            <header class="panel-heading">
                                <a href="{{route('reporte.genero')}}"><h2 class="panel-title"> Listado por Genero</h2></a>
                            </header>
                        </section>
                        <section class="panel panel-primary">
                            <header class="panel-heading">
                                <a href="{{ route('reporte.filtroReporte') }}"><h2 class="panel-title"> Reporte de Empleados Dinámicos </h2></a>
                            </header>
                        </section>
                    </div>
                    @endcan
                </div>
            </section>
            

        </div>
    </div>
    <!-- end: page -->
</section>

@endsection