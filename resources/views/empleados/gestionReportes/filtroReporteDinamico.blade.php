@extends('layouts.template')
@section('content')
<section role="main" class="content-body">
    <header class="page-header">
        <h2>Reporte de Empleados - Dinámico</h2>
        <div class="right-wrapper pull-right">
            <ol class="breadcrumbs">
                <li style="margin-left: -25%"><a href="{{ route('listado.reporte') }}"><button class="btn btn-default"><span class="fa fa-chevron-left"> </span> Regresar </button></a></li>
            </ol>
        </div>
    </header>


    <!-- start: page -->
    <div class="row">
        <div class="col-lg-12">
            <div>
                @include('flash::message')
                @if ($errors->any())
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                    <div class="errors">
                        <ul>
                            @foreach ($errors->all() as $error)

                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif 

                </div> 
            </div>
            <section class="panel">
                <header class="panel-heading">
                    <h2 class="panel-title">Primer Reporte</h2>
                </header>
                <div class="panel-body text-center">
                    <form action="{{ route('reporte.verReporteUno') }}" method="POST" class="form-horizontal form-bordered">
                      {{ csrf_field() }}
                        <h4 align="left">Reporte ya contiene los siguientes datos: Código de Empleado, Nombre, Unidad, Oficina, Sexo</h4>
                        <div class="form-group" align="left" style="margin-left: 2%">
                            <label class="control-label">Seleccione las variables que desea agregar al reporte</label>
                        </div>
                        <div class="col-md-4"></div>
                        <div class="form-group col-md-4">
                            <div class="checkbox-custom">
                                <div><input type="checkbox" name="reporteUno[]" value="1" class=""><label>Departamento</label></div>                                
                            </div>
                            <div class="checkbox-custom">
                                <div><input type="checkbox" name="reporteUno[]" value="2" class=""><label>Direccion</label></div>                                
                            </div>
                        </div>
                        <div class="form-group text-center col-md-12">
                            <button class="btn btn-primary" type="submit" name="enviar"> Generar </button>
                         
                      </div>
                    </form>
              
                </div>
            </section>
            <section class="panel">
                <header class="panel-heading">
                    <h2 class="panel-title">Segundo Reporte</h2>
                </header>
                <div class="panel-body text-center">
                    <form action="{{ route('reporte.verReporteDos') }}" method="POST" class="form-horizontal form-bordered">
                      {{ csrf_field() }}
                        <h4 align="left">Reporte ya contiene los siguientes datos: Código de Empleado, Nombre, Unidad, Oficina, Sexo</h4>
                        <div class="form-group" align="left" style="margin-left: 2%">
                            <label class="control-label">Seleccione las variables que desea agregar al reporte</label>
                        </div>
                        <div class="col-md-4"></div>
                        <div class="form-group col-md-4">
                            <div class="checkbox-custom">
                                <div><input type="checkbox" name="reporteDos[]" value="1"><label>Fecha de Nacimiento</label></div>                                
                            </div>
                            <div class="checkbox-custom">
                                <div><input type="checkbox" name="reporteDos[]" value="2"><label>Edad</label></div>                                
                            </div>
                        </div>
                        <div class="form-group text-center col-md-12">
                            <button class="btn btn-primary" type="submit" name="enviar"> Generar </button>
                         
                      </div>
                    </form>
              
                </div>
            </section>
            <section class="panel">
                <header class="panel-heading">
                    <h2 class="panel-title">Tercer Reporte</h2>
                </header>
                <div class="panel-body text-center">
                    <form action="{{ route('reporte.verReporteTres') }}" method="POST" class="form-horizontal form-bordered">
                      {{ csrf_field() }}
                        <h4 align="left">Reporte ya contiene los siguientes datos: Código de Empleado, Nombre, Unidad, Oficina, Sexo</h4>
                        <div class="form-group" align="left" style="margin-left: 2%">
                            <label class="control-label">Seleccione las variables que desea agregar al reporte</label>
                        </div>
                        <div class="col-md-4"></div>
                        <div class="form-group col-md-4">
                            <div class="checkbox-custom">
                                <div><input type="checkbox" name="reporteTres[]" value="1"><label>Tipo de Plaza</label></div>                                
                            </div>
                            <div class="checkbox-custom">
                                <div><input type="checkbox" name="reporteTres[]" value="2"><label>Fecha de Ingreso</label></div>                                
                            </div>
                            <div class="checkbox-custom">
                                <div><input type="checkbox" name="reporteTres[]" value="3"><label>Salario</label></div>                                
                            </div>
                        </div>
                        <div class="form-group text-center col-md-12">
                            <button class="btn btn-primary" type="submit" name="enviar"> Generar </button>
                         
                      </div>
                    </form>
              
                </div>
            </section>

        <!-- end: page -->
    </section>
@endsection
