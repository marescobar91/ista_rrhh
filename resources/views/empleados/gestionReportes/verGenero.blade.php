@extends('layouts.template')
@section('content')
<section role="main" class="content-body">
    <header class="page-header">
        <h2>Reporte de Unidad Administrativa por Genero</h2>
        <div class="right-wrapper pull-right">
            <ol class="breadcrumbs">
                <li style="margin-left: -25%"><a href="{{ route('listado.reporte') }}"><button class="btn btn-default"><span class="fa fa-chevron-left"> </span> Regresar </button></a></li>
            </ol>
        </div>
    </header>

    <!-- start: page -->
    <div class="row">
        <div class="col-lg-12">
            <div>
                @if ($errors->any())
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    
                    <div class="errors">
                        <ul>
                            @foreach ($errors->all() as $error)

                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif

                </div>
            </div>
            <section class="panel">
                @include('flash::message')
                <header class="panel-heading">
                    <div class="panel-actions">
                        
                        @can('consultar.reportes')
                        <a href="{{route('reporte.generoImprimir')}}" target="_blank"><button class="btn btn-info"><span class="fa fa-print"></span></button></a>
                         <a href="{{route('excel.generoImprimir')}}"><button class="btn btn-default"><span class="fa fa-file-excel-o"></span></button></a>
                        @endcan
                    </div>
                    <h2 class="panel-title">Unidad Administrativa por Genero</h2>
                </header>
                <div class="row">
                    <div class="col-md-12"> 
                        <div class="panel-body">
                            <div class="col-md-1">
                                <img src="{{asset('img/Logo-Institucional.svg')}}" width="200%" style="padding-top: 10%">
                            </div>
                            <div class="col-md-10">
                                <h4 class="text-center" style="text-transform: uppercase;padding-bottom: 0.75%">Instituto Salvadoreño de Transformación Agraria</h4>
                                <h4 class="text-center" style="text-transform: uppercase; padding-bottom: 0.75%">Reporte de Gestión de Personal</h4>
                                <h4 class="text-center" style="text-transform: uppercase;padding-bottom: 0.75%">Unidad Administrativa por Genero</h4>
                                <h4 class="text-center" style="text-transform: uppercase;padding-bottom: 0.75%">Mes: {{$mes}} {{$anio}}</h4>
                            </div>
                            <table class="table table-bordered table-striped mb-none">
                                <thead>
                                    <tr>
                                        <th>Oficina</th>
                                        <th>Mujeres</th>
                                        <th>Hombres</th>
                                        <th>Total</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($genero as $generos)
                                    <!--Aqui va el forech-->
                                     <tr class="gradeX" >
                                        <td>{{$generos->Oficina}}</td>
                                        <td class="text-center">{{$generos->mujer}}</td>
                                        <td class="text-center">{{$generos->hombre}}</td>
                                        <td class="text-center">{{$generos->total}}</td>
                                    </tr>
                                    @endforeach
                                    <!--Fin de for-->
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
<!-- end: page -->
</section>

@endsection
