<table >
    <thead>
        <tr>
            <th>Código de Empleado</th>
            <th>Nombre Completo </th>
            <th>Sexo</th>
            <th>Unidad Administrativa</th>
            <th>Oficina</th>
            <th>Cargo</th>
            @if($departamento=='Si')
            <th>Departamento</th>
            @endif
            @if($direccion=='Si')
            <th>Dirección</th>
            @endif
        </tr>
    </thead>
    <tbody>
        <!--Aqui va el forech-->
        @foreach($empleado as $empleados)
        <tr >
            <td>{{ $empleados->codigo_empleado }}</td>
            <td>{{ $empleados->nombre}} {{ $empleados->apellido }}</td>
            <td>{{ $empleados->sexo }}</td>
            <td>{{ $empleados->unidad_administrativa }}</td>
            <td>{{ $empleados->oficina }}</td>
            <td>{{ $empleados->cargo_mp }}</td>
            @if($departamento=='Si')
            <td>{{ $empleados->departamento }}</td>
            @endif
            @if($direccion=='Si')
            <td>{{ $empleados->direccion }}</td>
            @endif

        </tr>
        @endforeach
        <!--Fin de for-->
    </tbody>
</table>
