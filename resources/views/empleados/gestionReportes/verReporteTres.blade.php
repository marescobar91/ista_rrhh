@extends('layouts.template')
@section('content')
<section role="main" class="content-body">
    <header class="page-header">
        <h2>Reporte de Empleado de Datos Personales</h2>
        <div class="right-wrapper pull-right">
            <ol class="breadcrumbs">
                <li style="margin-left: -25%"><a href="{{ route('listado.reporte') }}"><button class="btn btn-default"><span class="fa fa-chevron-left"> </span> Regresar </button></a></li>
            </ol>
        </div>
    </header>

    <!-- start: page -->
    <div class="row">
        <div class="col-lg-12">
            <div>
                @if ($errors->any())
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    
                    <div class="errors">
                        <ul>
                            @foreach ($errors->all() as $error)

                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif

                </div>
            </div>
            <section class="panel">
                @include('flash::message')
                <header class="panel-heading">
                    <div class="panel-actions">
                      @can('consultar.reportes')
                      <div class="col-md-4"></div>
                      <div class="col-md-8">
                          <div class="col-md-4">
                              <form action="{{ route('reporte.imprimirReporteTres') }}" target="_blank" method="post" role="form" class="contactForm" target="_blank">
                               {{ csrf_field() }}
                               <div class="">
                                   @foreach($dato as $key=>$value)
                                   <input type="hidden" name="reporteTres[]" value="{{ $value }}">                 
                                   @endforeach
                                   <button class="btn btn-info"><span class="fa fa-print"></span></button> 
                               </div>
                           </form>
                       </div>
                       <div class="col-md-4">
                          <form action="{{ route('excel.imprimirReporteTres') }}" method="post" role="form" class="contactForm" target="_blank">
                           {{ csrf_field() }}
                           <div class="">
                               @foreach($dato as $key=>$value)
                               <input type="hidden" name="reporteTres[]" value="{{ $value }}">                 
                               @endforeach
                               <button class="btn btn-default"><span class="fa fa-file-excel-o"></span></button> 
                           </div>
                       </form>
                   </div>
                   <div class="col-md-4">
                     <a href="{{route('reporte.filtroReporte')}}"class="btn btn-success" style="width: 40px;height: 34px"><span class="fa fa-undo"></span></a> 
                 </div>
             </div>
             @endcan
         </div>
         <h2 class="panel-title">Reporte de Empleado de Datos Personales</h2>
     </header>
     <div class="row">
        <div class="col-md-12"> 
            <div class="panel-body">
                <div class="col-md-1">
                    <img src="{{asset('img/Logo-Institucional.svg')}}" width="200%" style="padding-top: 10%">
                </div>
                <div class="col-md-10">
                    <h4 class="text-center" style="text-transform: uppercase;padding-bottom: 0.75%">Instituto Salvadoreño de Transformación Agraria</h4>
                    <h4 class="text-center" style="text-transform: uppercase; padding-bottom: 0.75%">Reporte de Gestión de Personal</h4>
                    <h4 class="text-center" style="text-transform: uppercase;padding-bottom: 0.75%">Reporte de Empleado de Datos Personales</h4>
                    <h4 class="text-center" style="text-transform: uppercase;padding-bottom: 0.75%">Mes: {{$mes}} {{$anio}}</h4>
                </div>
                <table class="table table-bordered table-striped mb-none">
                    <thead>
                        <tr>
                            <th>Código de Empleado</th>
                            <th>Nombre Completo</th>
                            <th>Sexo</th>
                            <th>Unidad Administrativa</th>
                            <th>Oficina </th>
                            <th>Cargo</th>
                            @if($tipoPlaza=='Si')
                            <th>Tipo de Plaza</th>
                            @endif
                            @if($fechaIngreso == 'Si')
                            <th>Fecha de Ingreso</th>
                            @endif
                            @if($salario=='Si')
                            <th>Salario Actual</th>
                            @endif
                        </tr>
                    </thead>
                    <tbody>
                        <!--Aqui va el forech-->
                        @foreach($empleado as $empleados)
                        <tr class="gradeX" >
                            <td>{{ $empleados->codigo_empleado }}</td>
                            <td>{{ $empleados->nombre}} {{ $empleados->apellido }}</td>
                            <td>{{ $empleados->sexo }}</td>
                            <td>{{ $empleados->unidad_administrativa }}</td>
                            <td>{{ $empleados->oficina }}</td>
                            <td>{{ $empleados->cargo_mp }}</td>
                            @if($tipoPlaza=='Si')
                            <td>{{ $empleados->tipo_plaza }}</td>
                            @endif
                            @if($fechaIngreso=='Si')
                            <td>{{date('d-m-Y', strtotime($empleados->fecha_ingreso))}}</td>
                            @endif
                            @if($salario == 'Si')
                            <td>{{ $empleados->salario_actual }}</td>
                            @endif
                        </tr>
                        @endforeach
                        <!--Fin de for-->
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</section>
</div>
</div>
<!-- end: page -->
</section>

@endsection
