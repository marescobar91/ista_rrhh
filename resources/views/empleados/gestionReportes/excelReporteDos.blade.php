 <table >
                                <thead>
                                    <tr>
                                        <th>Código de Empleado</th>
                                        <th>Nombre Completo </th>
                                        <th>Sexo</th>
                                        <th>Unidad Administrativa</th>
                                        <th>Oficina</th>
                                        <th>Cargo</th>
                                        @if($fechaNacimiento=='Si')
                                        <th>Fecha de Nacimiento </th>
                                        @endif
                                        @if($edad=='Si')
                                        <th>Edad</th>
                                        @endif
                                    </tr>
                                </thead>
                                <tbody>
                                    <!--Aqui va el forech-->
                                    @foreach($empleado as $empleados)
                                     <tr>
                                        <td >{{ $empleados->codigo_empleado }}</td>
                                        <td >{{ $empleados->nombre}} {{ $empleados->apellido }}</td>
                                        <td >{{ $empleados->sexo }}</td>
                                        <td >{{ $empleados->unidad_administrativa }}</td>
                                        <td >{{ $empleados->oficina }}</td>
                                        <td >{{ $empleados->cargo_mp }}</td>
                                         @if($fechaNacimiento=='Si')
                                        <td >{{date('d-m-Y', strtotime($empleados->fecha_nacimiento))}}</td>
                                        @endif
                                         @if($edad=='Si')
                                        <td >{{\Carbon\Carbon::parse($empleados->fecha_nacimiento)->age}}</td>
                                        @endif

                                    </tr>
                                    @endforeach
                                    <!--Fin de for-->
                                </tbody>
                            </table>
                       