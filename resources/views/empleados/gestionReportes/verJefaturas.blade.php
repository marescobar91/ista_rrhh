@extends('layouts.template')
@section('content')
<section role="main" class="content-body">
    <header class="page-header">
        <h2>Reporte de Listado de Jefaturas</h2>
        <div class="right-wrapper pull-right">
            <ol class="breadcrumbs">
                <li style="margin-left: -25%"><a href="{{ route('listado.reporte') }}"><button class="btn btn-default"><span class="fa fa-chevron-left"> </span> Regresar </button></a></li>
            </ol>
        </div>
    </header>

    <!-- start: page -->
    <div class="row">
        <div class="col-lg-12">
            <div>
                @if ($errors->any())
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    
                    <div class="errors">
                        <ul>
                            @foreach ($errors->all() as $error)

                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif

                </div>
            </div>
            <section class="panel">
                @include('flash::message')
                <header class="panel-heading">
                    <div class="panel-actions">
                        
                          @can('consultar.reportes')
                        <a href="{{route('reporte.jefaturaImprimir')}}" target="_blank"><button class="btn btn-info"><span class="fa fa-print"></span></button></a>
                         <a href="{{route('excel.jefaturaImprimir')}}" ><button class="btn btn-default"><span class="fa fa-file-excel-o"></span></button></a>
                        @endcan
                       
                    </div>
                    <h2 class="panel-title">Listado de Jefaturas</h2>
                </header>
                <div class="row">
                    <div class="col-md-12"> 
                        <div class="panel-body">
                            <div class="col-md-1">
                                <img src="{{asset('img/Logo-Institucional.svg')}}" width="200%" style="padding-top: 10%">
                            </div>
                            <div class="col-md-10">
                                <h4 class="text-center" style="text-transform: uppercase;padding-bottom: 0.75%">Instituto Salvadoreño de Transformación Agraria</h4>
                                <h4 class="text-center" style="text-transform: uppercase; padding-bottom: 0.75%">Reporte de Gestión de Personal</h4>
                                <h4 class="text-center" style="text-transform: uppercase;padding-bottom: 0.75%">Listado de Jefaturas</h4>
                                <div class="col"></div>
                                <h4 class="text-center" style="text-transform: uppercase;padding-bottom: 0.75%">Mes: {{$mes}} {{$anio}}</h4>
                            </div>
                            <table class="table table-bordered table-striped mb-none">
                                <thead>
                                    <tr>
                                        <th>Oficina</th>
                                        <th>Teléfono</th>
                                        <th>Correo Electrónico</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($jefatura as $jefaturas)
                                     <tr class="gradeX" >
                                        <td><strong>{{$jefaturas->oficina}}</strong></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr class="gradeX" >
                                        <td>{{$jefaturas->nombre}} {{ $jefaturas->apellido }}</td>
                                        <td>{{$jefaturas->telefono_oficina}}</td>
                                        <td>{{$jefaturas->correo}}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
<!-- end: page -->
</section>

@endsection
