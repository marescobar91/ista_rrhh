<!DOCTYPE html>

<head>
    <title>ISTA - RRHH</title>
    <link href="css/style.css" rel="stylesheet">
    <style>
        .celda{
            border-bottom: lightgrey 0.1em solid;
        }
        .encabezado{
            border-bottom: grey 0.1em solid;
            background-color: grey;
            color: white;
        }
        .subencabezado{
            padding-top: 1%;
            background-color: #ece8e8;"
        }
        .footer{
           position: fixed; left: 0px; bottom: -120px; right: 0px; height: 150px; 
       }
   </style>
</head>
<body>
    <div id="wrapper">
        <!-- Page Content -->
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="container" style="margin-left: 20px">

                    <div class="col-md-9 col-md-offset-1">
                        <div class="table-title" style="background-color: #fff; margin-bottom: 1%">

                            <div class="row" align="center">
                                <img src="{{public_path().'/img/Logo-Institucional.svg'}}" width="13%" align="left" style="padding-top: 2%">
                            </div>
                            <div style="width: 600px" align="center">
                                <p style="color: #1a1a1a; font-size: 20px;padding-bottom: -12px"><b>Instituto Salvadoreño de Transformaci&oacute;n Agraria</b></p>
                                <p style="color: #1a1a1a; font-size: 18px;padding-bottom: -12px"><b>Reporte de Gesti&oacute;n de Personal</b></p>
                                <p style="color: #1a1a1a; text-transform: capitalize;font-size: 16px; padding-left: 100px" ><b>Mes: {{$mes}} {{$anio}}</b></p>
                            </div>
                        </div>

                        <div class="row">
                            <div style="max-height: 700px;">
                             <div>
                                <p align="center" style="font-size: 20px; padding-top: -20px"> <b>Reporte de Listado de Genero </b></p>
                            </div>
                        </div>
                        <table class="table table-striped table-hover col-md-5" id="reporte" style="background: #fff;" width="97%">
                            <thead>
                                <tr class="encabezado">

                                    <th>Oficina</th>
                                    <th>Mujeres</th>
                                    <th>Hombres</th>
                                    <th>Total</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($genero as $generos)
                                <!--Aqui va el forech-->
                                <tr class="gradeX" >

                                    <td class="celda">{{$generos->Oficina}}</td>
                                    <td class="celda" align="center">{{$generos->mujer}}</td>
                                    <td class="celda" align="center">{{$generos->hombre}}</td>
                                    <td class="celda" align="center">{{$generos->total}}</td>
                                </tr>
                                @endforeach
                                <!--Fin de for-->
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<div class="footer">
    <table>
        <tr>
            <td style="text-align: left; width: 60%">Sistema Informatico para el manejo de Marcaciones y Viaticos de la Gerencia de Recursos Humanos del ISTA</td>
            <td></td>
            <td>{{$fechaF}}</td>
        </tr>
    </table>
</div>
</body>
</html>