<table>
    @foreach($unidad as $uni)
    <thead>
        <tr>
         <th colspan="2">{{$uni->unidad_administrativa}}</th>
     </tr>
 </thead>
 <tbody>
    <tr> 
        <th>Nombre Completo</th>
        <th>Cargo</th> 
    </tr> 

    @foreach($unidades as $unidad)
    @if($uni->unidad_administrativa == $unidad->unidad_administrativa)
    <tr>
        <td>{{$unidad->nombre}} {{ $unidad->apellido }}</td>
        <td>{{$unidad->cargo_mp}}</td>
    </tr>
    @endif
    @endforeach
</tbody>

@endforeach

</table>