<table>

    <thead>
        <tr >
           <th>Oficina</th>
           <th>Telefono</th>
           <th>Correo Electronico</th>
       </tr>
   </thead>
   <tbody>
    @foreach($jefatura as $jefaturas)
    <tr>
        <td colspan="3"><strong>{{$jefaturas->oficina}}</strong></td>
    </tr>
    <tr>
        <td>{{$jefaturas->nombre}} {{ $jefaturas->apellido }}</td>
        <td>{{$jefaturas->telefono_oficina}}</td>
        <td>{{$jefaturas->correo}}</td>
    </tr>
    @endforeach
</tbody>
</table>