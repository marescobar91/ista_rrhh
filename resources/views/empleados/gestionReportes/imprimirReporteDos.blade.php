<!DOCTYPE html>

<head>
    <title>ISTA - RRHH</title>
    <link href="css/style.css" rel="stylesheet">
    <style>
        .celda{
            border-bottom: lightgrey 0.1em solid;
        
        }
        .encabezado{
            border-bottom: grey 0.1em solid;
            background-color: grey;
            color: white;
        }
        .subencabezado{
            padding-top: 1%;
            background-color: #ece8e8;"
        }
        .footer{
             position: fixed; left: 0px; bottom: -160px; right: 0px; height: 150px; 
        }
    </style>
</head>
<body>
    <div id="wrapper">
        <!-- Page Content -->
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="container" style="margin-left: 20px">

                    <div class="col-md-9 col-md-offset-1">
                        <div class="table-title" style="background-color: #fff; margin-bottom: 1%">

                            <div class="row">
                                 <div class="row" align="center">
                                <img src="{{public_path().'/img/Logo-Institucional.svg'}}" width="13%" align="left" style="padding-top: 2%">
                                </div>
                                <div style="width: 900px" align="center">
                                    <p style="color: #1a1a1a; font-size: 20px;padding-bottom: -12px"><b>Instituto Salvadoreño de Transformaci&oacute;n Agraria</b></p>
                                    <p style="color: #1a1a1a; font-size: 18px;padding-bottom: -12px"><b>Reporte de Gesti&oacute;n de Personal</b></p>
                                    <p style="color: #1a1a1a; text-transform: capitalize;font-size: 16px; padding-left: 100px" ><b>Mes: {{$mes}} {{$anio}}</b></p>
                                </div>

                            </div>
                            <div class="row">

                                <div style="max-height: 700px;">
                                   <div>
                                    <p align="center" style="font-size: 20px;  padding-top: -20px"> <b>Reporte de Empleado de Datos Laborales </b></p>

                                </div>
                            </div>
                            <table class="table table-striped table-hover col-md-5" id="reporte" style="background: #fff;" width="97%">
                                <thead>
                                    <tr class="encabezado">
                                        <th>Código de Empleado</th>
                                        <th>Nombre Completo </th>
                                        <th>Sexo</th>
                                        <th>Unidad Administrativa</th>
                                        <th>Oficina</th>
                                        <th>Cargo</th>
                                        @if($fechaNacimiento=='Si')
                                        <th>Fecha de Nacimiento </th>
                                        @endif
                                        @if($edad=='Si')
                                        <th>Edad</th>
                                        @endif
                                    </tr>
                                </thead>
                                <tbody>
                                    <!--Aqui va el forech-->
                                    @foreach($empleado as $empleados)
                                     <tr class="gradeX" >
                                        <td class="celda">{{ $empleados->codigo_empleado }}</td>
                                        <td class="celda">{{ $empleados->nombre}} {{ $empleados->apellido }}</td>
                                        <td class="celda">{{ $empleados->sexo }}</td>
                                        <td class="celda">{{ $empleados->unidad_administrativa }}</td>
                                        <td class="celda">{{ $empleados->oficina }}</td>
                                        <td class="celda">{{ $empleados->cargo_mp }}</td>
                                         @if($fechaNacimiento=='Si')
                                        <td class="celda">{{date('d-m-Y', strtotime($empleados->fecha_nacimiento))}}</td>
                                        @endif
                                         @if($edad=='Si')
                                        <td class="celda">{{\Carbon\Carbon::parse($empleados->fecha_nacimiento)->age}}</td>
                                        @endif

                                    </tr>
                                    @endforeach
                                    <!--Fin de for-->
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="footer">
        <table>
            <tr>
                <td style="text-align: left; width: 60%">Sistema Informatico para el manejo de Marcaciones y Viaticos de la Gerencia de Recursos Humanos del ISTA</td>
                <td></td>
                <td>{{$fechaF}}</td>
            </tr>
        </table>
</div>
</body>
</html>
                       