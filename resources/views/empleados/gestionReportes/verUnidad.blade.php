@extends('layouts.template')
@section('css')
 
<style >
    tr.group,
tr.group:hover {
    background-color: #ddd !important;
}

</style>

@endsection
@section('content')
<section role="main" class="content-body">
    <header class="page-header">
        <h2>Reporte de Listado de Empleados por Unidad Organizativa</h2>
        <div class="right-wrapper pull-right">
            <ol class="breadcrumbs">
                <li style="margin-left: -25%"><a href="{{ route('listado.reporte') }}"><button class="btn btn-default"><span class="fa fa-chevron-left"> </span> Regresar </button></a></li>
            </ol>
        </div>
    </header>

    <!-- start: page -->
    <div class="row">
        <div class="col-lg-12">
            <div>
                @if ($errors->any())
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    
                    <div class="errors">
                        <ul>
                            @foreach ($errors->all() as $error)

                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif

                </div>
            </div>
            <section class="panel">
                @include('flash::message')
                <header class="panel-heading">
                    <div class="panel-actions">
                          @can('consultar.reportes')
                        <a href="{{route('reporte.unidadImprimir')}}" target="_blank"><button class="btn btn-info"><span class="fa fa-print"></span></button></a>
                         <a href="{{route('excel.unidadImprimir')}}"><button class="btn btn-default"><span class="fa fa-file-excel-o"></span></button></a>
                        @endcan
                        
                    </div>
                    <h2 class="panel-title">Listado de Empleados por Unidad Organizativa</h2>
                </header>
                <div class="row">
                    <div class="col-md-12"> 
                        <div class="panel-body">
                            <div class="col-md-1">
                                <img src="{{asset('img/Logo-Institucional.svg')}}" width="200%" style="padding-top: 10%">
                            </div>
                            <div class="col-md-10">
                                <h4 class="text-center" style="text-transform: uppercase;padding-bottom: 0.75%">Instituto Salvadoreño de Transformación Agraria</h4>
                                <h4 class="text-center" style="text-transform: uppercase;padding-bottom: 0.75%">Empleados por Unidad Organizativa</h4>
                                <h4 class="text-center" style="text-transform: uppercase; padding-bottom: 0.75%">Reporte de Gestión de Personal</h4>
                                <h4 class="text-center" style="text-transform: uppercase;padding-bottom: 0.75%">Mes: {{$mes}} {{$anio}}</h4>
                            </div>
                            <table id="example" class="table table-bordered table-striped mb-none">
                                <!-- Aqui ira el for-->
                            @foreach($unidad as $uni)
                           
                                 <thead>
                                    <tr>
                                       <th colspan="3" class="text-center">{{$uni->unidad_administrativa}}</th>
                                   </tr>
                               </thead>
                          
                                <tbody>
                                    <!--Este bloque debe ser estatico--> 
                                     <tr class="gradeX" >
                                        <th>Nombre Completo</th>
                                        <th>Cargo</th> <!--Cargo MP-->                              
                                    </tr>  
                                    
                                    @foreach($unidades as $unidad)
                                     @if($uni->unidad_administrativa == $unidad->unidad_administrativa)
                                    <!--Estatico hasta aqui-->
                                    <tr class="gradeX" >

                                        <td>{{$unidad->nombre}} {{ $unidad->apellido }}</td>
                                        <td>{{$unidad->cargo_mp}}</td>
                                   </tr>  
                                   @endif                   
                                </tbody>
                                
                             @endforeach
                             @endforeach
                                <!--Hasta aqui-->
                            </table>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
<!-- end: page -->
</section>
@endsection



