  <table>
                                <thead>
                                    <tr>
                                        <th>Código de Empleado</th>
                                        <th>Nombre Completo</th>
                                        <th>Sexo</th>
                                        <th>Unidad Administrativa</th>
                                        <th>Oficina </th>
                                        <th>Cargo</th>
                                        @if($tipoPlaza=='Si')
                                        <th>Tipo de Plaza</th>
                                        @endif
                                        @if($fechaIngreso == 'Si')
                                        <th>Fecha de Ingreso</th>
                                        @endif
                                        @if($salario=='Si')
                                        <th>Salario Actual</th>
                                        @endif
                                    </tr>
                                </thead>
                                <tbody>
                                    <!--Aqui va el forech-->
                                    @foreach($empleado as $empleados)
                                    <tr>
                                        <td >{{ $empleados->codigo_empleado }}</td>
                                        <td >{{ $empleados->nombre}} {{ $empleados->apellido }}</td>
                                        <td >{{ $empleados->sexo }}</td>
                                        <td >{{ $empleados->unidad_administrativa }}</td>
                                        <td >{{ $empleados->oficina }}</td>
                                        <td >{{ $empleados->cargo_mp }}</td>
                                        @if($tipoPlaza=='Si')
                                        <td > {{ $empleados->tipo_plaza }}</td>
                                        @endif
                                        @if($fechaIngreso=='Si')
                                        <td>{{date('d-m-Y', strtotime($empleados->fecha_ingreso))}}</td>
                                        @endif
                                        @if($salario=='Si')
                                        <td >{{ $empleados->salario_actual }}</td>
                                        @endif

                                    </tr>
                                    @endforeach
                                    <!--Fin de for-->
                                </tbody>
                            </table>