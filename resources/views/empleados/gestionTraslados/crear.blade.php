@extends('layouts.template')
@section('content')
<section role="main" class="content-body">
    <header class="page-header">
        <h2>Gestión de Traslado de Empleados</h2>
        <div class="right-wrapper pull-right">
            <ol class="breadcrumbs">
                <li style="margin-left: -25%"><a href="{{ URL::previous() }}"><button class="btn btn-default"><span class="fa fa-chevron-left"> </span> Regresar </button></a></li>
            </ol>
        </div>
    </header>

    <!-- start: page -->
    <div class="row">
        <div class="col-lg-12">
            <div>
                @include('flash::message')
                @if ($errors->any())
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                    <div class="errors">
                        <ul>
                            @foreach ($errors->all() as $error)

                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif 

                </div> 
            </div>
            <form  role="form" class="contactForm" action="{{ route('traslados.guardar', $empleado->id_empleado) }}" method="post">

                {{csrf_field()}}
                {{ method_field('PUT') }}
                <section class="panel">
                    <header class="panel-heading">

                        <h2 class="panel-title">Crear Traslado</h2>
                        
                    </header>
                    <div class="panel-body">

                        @csrf  
                        <div class="form-group"></div>
                        <div class="col-md-12">
                            <div class="col-md-4">
                                <!-- Nombre Empleado-->
                                <label class="col-md-12 control-label" for="w4-first-name" >Nombre Completo</label>
                                <div class="col-md-12">
                                    <input type="text" class="form-control" name="nombre_completo" id="nombre_completo"  value="{{ $empleado->nombre }} {{ $empleado->apellido }}" disabled>

                                </div>
                            </div>
                            
                            <div class="col-md-4">
                                <label class="control-label col-md-12">Fecha de Traslado</label>
                                <div class="col-md-12">
                                
                                        <input type="date" class="form-control field-input {{ $errors->has('fecha_traslado') ? 'field-error' : '' }}" name="fecha_traslado" id="fecha_traslado" value="" required> 
                                        @if ($errors->has('fecha_traslado'))<span class="error-message">{{ $errors->first('fecha_traslado') }}</span>
                                        @endif 

                                    
                                </div>
                            </div>
                            
                                
                            <div class="col-md-4">
                                <!--Salario Actual -->
                                <label class="col-md-12 control-label" for="w4-first-name">Salario Actual</label>
                                <div class="col-md-12">
                                    <input type="number" class="form-control field-input {{ $errors->has('salario_actual') ? 'field-error' : '' }}" name="salario_actual" id="salario_actual" value="{{ $empleado->salario_actual }}" placeholder="Salario" min="0" required>
                                    @if ($errors->has('salario_actual'))<span class="error-message">{{ $errors->first('salario_actual') }}</span>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="form-group"></div>
                        <div class="col-md-12">
                            <div class="col-md-4">
                                <!--Unidad Administrativa -->
                                <label class="col-md-12 control-label" for="w4-first-name">Centro</label>
                                <div class="col-md-12">
                                    <select data-plugin-selectTwo class="form-control" id="centro_id" name="centro" required>
                                        <option value="">Seleccion Centro</option>
                                        @foreach($centro as $centroB)

                                        <?php $selectedvalue=$centroB['id_centro'] ?>
                                        <option value="{{ $centroB['id_centro' ]}}" {{$centroActual == $centroB['id_centro'] ? 'selected ="selected"' : ''}}> {{ $centroB['centro']}}</option>                                                            
                                        @endforeach
                                    </select> 
                                    @if ($errors->has('centro'))<span class="error-message">{{ $errors->first('centro') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-4">
                                <!--Unidad Administrativa -->
                                <label class="col-md-12 control-label" for="w4-first-name">Unidad Administrativa</label>
                                <div class="col-md-12">
                                    <select data-plugin-selectTwo class="form-control" id="unidad_id" name="unidad" required>
                                        <option value="">Seleccion Unidad Administrativa</option>
                                        @foreach($unidad as $unidadB)

                                        <?php $selectedvalue=$unidadB['id_unidad_administrativa'] ?>
                                        <option value="{{ $unidadB['id_unidad_administrativa' ]}}" {{$unidadActual == $unidadB['id_unidad_administrativa'] ? 'selected ="selected"' : ''}}> {{ $unidadB['unidad_administrativa']}}</option>                                                            
                                        @endforeach
                                    </select> 


                                    @if ($errors->has('unidad'))<span class="error-message">{{ $errors->first('unidad') }}</span>
                                    @endif
                                </div>
                            </div>
                            
                            <div class="col-md-4">
                                <!-- Oficina-->
                            <label class="col-md-12 control-label" for="w4-first-name">Oficina</label>
                            <div class="col-md-12">
                                <select data-plugin-selectTwo  id="oficina_id" data-old="{{$oficinaActual}}" name="oficina" class="form-control{{ $errors->has('oficina') ? ' is-invalid' : '' }}"> 

                                </select>


                                @if ($errors->has('oficina'))<span class="error-message">{{ $errors->first('oficina') }}</span>
                                @endif
                            </div>
                            </div>


                            
                        </div>
                        <div class="form-group"></div>
                    </section>


                    <section class="panel">
                        <div class="panel-body">
                            @csrf         
                            <div class="form-group"></div>
                            <div class="col-md-12">
                                <div class="col-md-6">
                                    <!--Cargo Funcional -->
                                <label class="col-md-12 control-label" for="w4-first-name">Cargo Funcional</label>
                                <div class="col-md-12">                                    
                                   <select data-plugin-selectTwo  class="form-control" id="cargoF" name="cargoF" required>
                                    <option value="">Seleccion Cargo Funcional</option>
                                    @foreach($cargoFuncional as $cargoF)

                                    <?php $selectedvalue=$cargoF['id_cargo'] ?>
                                    <option value="{{ $cargoF['id_cargo']}}" {{$selectedvalue == $empleado['cargo_funcional_id'] ? 'selected="selected"' : ''}}> 
                                    {{$cargoF['cargo']}}</option>                                                           
                                    @endforeach
                                </select> 
                                @if ($errors->has('cargoF'))<span class="error-message">{{ $errors->first('cargoF') }}</span>
                                @endif
                                </div>
                                
                            </div>
                            <div class="col-md-6">
                                <label class="col-md-12 control-label" for="w4-first-name">Cargo Nominal</label>
                            <div class="col-md-12">

                             <select data-plugin-selectTwo class="form-control" id="cargoN" name="cargoN" required>
                                <option value="">Seleccion Cargo Nominal</option>
                                @foreach($cargoNominal as $cargoN)

                                <?php $selectedvalue=$cargoN['id_cargo'] ?>
                                <option value="{{ $cargoN['id_cargo']}}" {{$selectedvalue == $empleado['cargo_nominal_id'] ? 'selected="selected"' : ''}}> 
                                {{$cargoN['cargo']}}</option>                                                           
                                @endforeach
                            </select> 

                            @if ($errors->has('cargoN'))<span class="error-message">{{ $errors->first('cargoN') }}</span>
                            @endif
                        </div>
                            </div>
                            <!-- Cargo Nominal-->
                            
                    </div>
                    <div class="form-group"></div>
                    <div class="col-md-12">
                        <div class="col-md-6">
                            <!--Cargo Funcional MP -->
                        <label class="col-md-12 control-label" for="w4-first-name">Cargo Funcional MP</label>
                        <div class="col-md-12">

                            <select data-plugin-selectTwo  class="form-control" id="cargoMP" name="cargoMP" required>
                                <option value="">Seleccion Cargo Funcional MP</option>
                                @foreach($cargoMP as $cargo)

                                <?php $selectedvalue=$cargo['id_cargo_mp'] ?>
                                <option value="{{ $cargo['id_cargo_mp']}}" {{$selectedvalue == $empleado['cargo_mp_id'] ? 'selected="selected"' : ''}}> 
                                {{$cargo['cargo_mp']}}</option>                                                           
                                @endforeach
                            </select> 

                            @if ($errors->has('cargoMP'))<span class="error-message">{{ $errors->first('cargoMP') }}</span>
                            @endif
                        </div>
                        </div>
                        
                        <div class="col-md-6">
                            <!-- Cargo Nominal Hacienda-->
                        <label  class="col-md-12 control-label" for="w4-first-name">Cargo Nominal Hacienda</label>
                        <div class="col-md-12">

                            <select data-plugin-selectTwo class="form-control" id="cargoH" name="cargoH" required>
                                <option value="">Seleccion Cargo Hacienda</option>
                                @foreach($cargoHacienda as $cargoH)

                                <?php $selectedvalue=$cargoH['id_cargo_hacienda'] ?>
                                <option value="{{ $cargoH['id_cargo_hacienda']}}" {{$selectedvalue == $empleado['cargo_hacienda_id'] ? 'selected="selected"' : ''}}> 
                                {{$cargoH['cargo_hacienda']}}</option>                                                           
                                @endforeach
                            </select> 

                            @if ($errors->has('cargoH'))<span class="error-message">{{ $errors->first('cargoH') }}</span>
                            @endif
                        </div>
                        </div>
                        

                    </div>
                </section>
                <section class="panel">
                    <div class="panel-body">
                        @csrf         
                        <div id="w4-confirm" class="tab-pane">

                            <div class="form-group text-center">
                                <button class="btn btn-primary">Trasladar</button>
                                <a href="{{route('home')}}" class="btn btn-default">Cancelar</a>
                            </div>


                        </div>
                    </div>
                </section>
            </form>
        </div>
    </div>
    <!-- end: page -->
</section>
@endsection
@section('script')

<script type="text/javascript">


    $(document).ready(function() {
        function cargarOficinas(){
            var unidad_id = $('#unidad_id').val(); 
            var centro_id = $('#centro_id').val();
            if($.trim('unidad_id') != '' && $.trim('centro_id') != ''){
                $.get('/selecionOficinas', {unidad_administrativa:  unidad_id,centro: centro_id}, function (oficinas){

                    var old = $('#oficina_id').data('old') != '' ? $('#oficina_id').data('old') : '';
                    console.log('valor para este elemento: '+old);

                    $('#oficina_id').empty();
                    $('#oficina_id').append("<option value=''>Selecciona Oficina </option>");
                    $.each(oficinas, function(index, value){
                        $('#oficina_id').append("<option value='" + index + "'" + (old == index ? 'selected' : '' ) + ">" + value + "</option>");
                        console.info(`valor para old: ${old}
                            valor para value: ${value}
                            valor para indece: ${index}`);
                    })

                });
            }
        }
        cargarOficinas();
        $('#centro_id').on('change', cargarOficinas);
        $('#unidad_id').on('change', cargarOficinas);
    });

</script>


@endsection
