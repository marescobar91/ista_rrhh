@extends('layouts.template')
@section('content')
<section role="main" class="content-body">
    <header class="page-header">
        <h2>Bitácora de Traslado</h2>
        <div class="right-wrapper pull-right">
            <ol class="breadcrumbs">
                <li style="margin-left: -25%"><a href="{{route('empleados.listar')}}"><button class="btn btn-default"><span class="fa fa-chevron-left"> </span> Regresar </button></a></li>
            </ol>
        </div>
    </header>

    <!-- start: page -->
    <div class="row">
        <div class="col-lg-12">
            <div>
                @if ($errors->any())
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                    <div class="errors">
                        <ul>
                            @foreach ($errors->all() as $error)

                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif

                </div>
            </div>
            <section class="panel">
                @include('flash::message')
                <header class="panel-heading">
                    <h2 class="panel-title">Listado de Bitácora de {{$empleado->nombre}} {{$empleado->apellido}} </h2>

                <div class="panel-actions">
                    <a href="{{route('traslados.crear', $empleado->id_empleado)}}"><button class="btn btn-primary"><span class="fa fa-plus"></span> Agregar Traslado </button></a>                 
                </div>
                </header>
                <div class="panel-body" style="overflow: scroll">
                    <table class="table table-bordered table-striped mb-none" id="datatable-default">
                       <thead>
                        <tr>
                            
                            <th>Código</th>
                            <th>Nombre Completo</th>
                            <th>Unidad Administrativa</th>
                            <th>Oficina</th>
                            <th>Fecha Ingreso</th>
                            <th>Fecha Traslado</th>                       
                            <th>Cargos MP</th>
                            <th>Salario Actual</th>
                            <th>Modificado por:</th>

                            
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($traslados as $traslado)
                            <tr>  
                                <td>{{$traslado->codigo_empleado}}</td>
                                <td>{{$traslado->nombre}} {{$traslado->apellido}}</td>
                                <td>{{$traslado->oficina->unidadAdministrativa->unidad_administrativa}}</td> 
                                <td>{{$traslado->oficina->oficina}}</td>
                                <td>{{$traslado->fecha_ingreso}}</td>
                                <td>{{$traslado->fecha_traslado}}</td>
                                <td>{{$traslado->cargoMP->cargo_mp}}</td>
                                <td>{{$traslado->salario_actual}}</td> 
                                <td>{{$traslado->usuario->nombre_usuario}}</td>                            
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </section>
        </div>
    </div>
<!-- end: page -->
</section>

@endsection

