@extends('layouts.template')
@section('content')
<section role="main" class="content-body">
    <header class="page-header">
        <h2>Gestión de Cargos MP</h2>
        <div class="right-wrapper pull-right">
            <ol class="breadcrumbs">
                <li style="margin-left: -25%"><a href="{{url('/')}}"><button class="btn btn-default"><span class="fa fa-chevron-left"> </span> Regresar </button></a></li>
            </ol>
        </div>
    </header>

    <!-- start: page -->
    <div class="row">
        <div class="col-lg-12">
            <div>
                @if ($errors->any())
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    
                    <div class="errors">
                        <ul>
                            @foreach ($errors->all() as $error)

                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif

                </div>
            </div>
            <section class="panel">
                <header class="panel-heading">
                    <div class="panel-actions">
                    </div>
                    <h2 class="panel-title">Crear Cargo MP</h2>
                </header>
                <div class="panel-body">
                    <form class="form-horizontal form-bordered" method="POST" action="{{route('cargosMP.guardar')}}">
                        @csrf                        
                        <div class="form-group">
                            <div class="col-md-3"></div>
                            <div class="col-md-6">
                            <label class="col-md-12 control-label text-right" style="text-align: left;">Nombre Cargo MP</label>
                            <div class="col-md-12">
                                <input id="cargo_mp" type="text" value="{{ old('cargo_mp') }}" name="cargo_mp" required autocomplete="cargo_mp" autofocus placeholder="Nombre del Cargo MP" class="form-control field-input {{ $errors->has('cargo_mp') ? 'field-error' : '' }}">
                                @if ($errors->has('cargo_mp'))<span class="error-message">{{ $errors->first('cargo_mp') }}</span>
                                @endif
                            </div>
                        </div>
                        </div> 
                        
                        <div class="col-lg-12 text-center" >    
                         <button type="submit" class="btn btn-primary">Guardar</button>
                         <a href="{{route('home')}}" class="btn btn-default">Cancelar</a>
                     </div>                              
                 </form>
             </div>
         </section>
         
     </div>
 </div>
 <!-- end: page -->
</section>

@endsection