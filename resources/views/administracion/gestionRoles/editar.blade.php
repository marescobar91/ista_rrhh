@extends('layouts.template')
@section('content')
<section role="main" class="content-body">
    <header class="page-header">
        <h2>Gesti&oacute;n de Roles</h2>
        <div class="right-wrapper pull-right">
            <ol class="breadcrumbs">
                <li style="margin-left: -25%"><a href="{{route('roles.listar')}}"><button class="btn btn-default"><span class="fa fa-chevron-left"> </span> Regresar </button></a></li>
            </ol>
        </div>
    </header>

    <!-- start: page -->
    <div class="row">
        <div class="col-lg-12">
            <div>
                @if ($errors->any())
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                    <div class="errors">
                        <ul>
                            @foreach ($errors->all() as $error)

                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif

                </div>
            </div>
            <section class="panel">
                <header class="panel-heading">
                    <div class="panel-actions">
                    </div>
                    <h2 class="panel-title">Editar Roles</h2>
                </header>
                <div class="panel-body">
                 <form class="form-horizontal form-bordered" action="{{ route('roles.actualizar', $rol->id) }}" method="post" >
                    {{csrf_field()}}                               
                    {{ method_field('PUT') }}                       
                    <div class="form-group">
                        <div class="col-md-2"></div>
                        <div class="col-md-8">
                            <label class="col-sm-12 control-label" value="{{ $rol->name}}" style="text-align: left;">Nombre de Rol</label>
                            <div class="col-md-12">
                                <input id="nombre_rol" type="text"  name="nombre_rol" required autocomplete="nombre_rol" autofocus placeholder="Nombre del Rol" class="form-control field-input {{ $errors->has('nombre_rol') ? 'field-error' : '' }}" value="{{$rol->name}}">
                                @if ($errors->has('nombre_rol'))<span class="error-message">{{ $errors->first('nombre_rol') }}</span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="form-group" align="center">
                        <div class="col-md-2"></div>
                        <div class="col-md-8">
                        <label class="col-sm-12 control-label" style="text-align: left;">Asignar Permisos</label>

                        <div class="col-sm-12">

                            <select multiple data-plugin-selectTwo class="form-control populate" name="permisos[]">
                                @foreach ($permiso as $permisos)
                                <option value="{{$permisos->id}}"  {{in_array($permisos->id, $permission) ? "selected" : null}}>{{ucfirst(str_replace(".", " ", $permisos->name)) }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    </div>

                    <div class="col-lg-12 text-center" >    
                       <button type="submit" class="btn btn-warning">Editar</button>
                       <a href="{{route('home')}}" class="btn btn-default">Cancelar</a>
                   </div>                              
               </form>
           </div>
       </section>

   </div>
</div>
<!-- end: page -->
</section>

@endsection

