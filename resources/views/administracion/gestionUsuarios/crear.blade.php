@extends('layouts.template')
@section('content')
<section role="main" class="content-body">
    <header class="page-header">
        <h2>Gesti&oacute;n de Usuarios</h2>
        <div class="right-wrapper pull-right">
            <ol class="breadcrumbs">
                <li style="margin-left: -25%"><a href="{{url('/')}}"><button class="btn btn-default"><span class="fa fa-chevron-left"> </span> Regresar </button></a></li>
            </ol>
        </div>
    </header>

    <!-- start: page -->
    <div class="row">
        <div class="col-lg-12">
            <div>
                @if ($errors->any())
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                    <div class="errors">
                        <ul>
                            @foreach ($errors->all() as $error)

                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif

                </div>
            </div>

            <section class="panel">
                <header class="panel-heading">
                    <div class="panel-actions">
                    </div>
                    <h2 class="panel-title">Crear Usuario de {{ $empleado->nombre }} {{ $empleado->apellido }}</h2>
                </header>
                <div class="panel-body">
                    <form action="{{ route('usuarios.guardar', $empleado->id_empleado) }}" method="post" role="form" class="contactForm">
                        @csrf 
                        <div class="col-md-2"></div>        
                        <div class="col-md-8">
                            <div class="form-group">
                            
                                <label class="col-md-12 control-label ">Nombre de Usuario</label>
                                <div class="col-md-12">
                                    <input id="nombre_usuario" type="text"  name="nombre_usuario" required autocomplete="nombre_usuario" autofocus placeholder="Nombre de Usuario" class="form-control field-input {{ $errors->has('nombre_usuario') ? 'field-error' : '' }}">
                                    @if ($errors->has('nombre_usuario'))<span class="error-message">{{ $errors->first('nombre_usuario') }}</span>
                                    @endif
                                </div>
                            </div>  
                            <div class="form-group">
                            
                                <label class="col-md-12 control-label ">Correo Electronico</label>
                                <div class="col-md-12">
                                    <input id="email" type="email" name="email" required autocomplete="email" autofocus placeholder="example@email.com" class="form-control field-input {{ $errors->has('email') ? 'field-error' : '' }}">
                                    @if ($errors->has('email'))<span class="error-message">{{ $errors->first('email') }}</span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                
                                <label class="col-md-12 control-label ">Contraseña</label>
                                <div class="col-md-12">
                                    <input id="password" type="password" name="password" required autocomplete="contraseña" autofocus placeholder="***********" class="form-control field-input {{ $errors->has('password') ? 'field-error' : '' }}">
                                    @if ($errors->has('password'))<span class="error-message">{{ $errors->first('password') }}</span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                
                                <label class="control-label col-md-12">Asignar Rol</label>
                        
                                <div class="col-md-12">
                                    <select multiple data-plugin-selectTwo class="form-control populate" name="rol[]">
                                        
                                        @foreach($rol as $roles)

                                        <option value="{{ $roles->id }}" >{{ $roles->name }}</option>

                                        @endforeach
                                    </select>

                                    @if ($errors->has('rol'))<span class="error-message">{{ $errors->first('rol') }}</span>
                                    @endif
                                </div>
                            </div>

                        </div>
                        <div class="form-group"></div>
                        <div class="col-lg-12 text-center" >    
                         <button type="submit" class="btn btn-primary">Guardar</button>
                         <a href="{{route('home')}}" class="btn btn-default">Cancelar</a>
                     </div>  

                 </form>
             </div>
         </section>

     </div>
 </div>
 <!-- end: page -->
</section>

@endsection