@extends('layouts.template')
@section('content')
<section role="main" class="content-body">
    <header class="page-header">
        <h2>Gesti&oacute;n de Usuarios</h2>
        <div class="right-wrapper pull-right">
            <ol class="breadcrumbs">
                <li style="margin-left: -25%"><a href="{{url('/')}}"><button class="btn btn-default"><span class="fa fa-chevron-left"> </span> Regresar </button></a></li>
            </ol>
        </div>
    </header>

    <!-- start: page -->
    <div class="row">
        <div class="col-lg-12">
            <div>
                @if ($errors->any())
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                    <div class="errors">
                        <ul>
                            @foreach ($errors->all() as $error)

                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif

                </div>
            </div>
            <section class="panel">
                @include('flash::message')
                <header class="panel-heading">


                    <h2 class="panel-title">Listado de Empleados - Asignar Usuario</h2>
                </header>
                <div class="panel-body">
                    <table class="table table-bordered table-striped mb-none" id="datatable-default">
                        <thead>
                            <tr>
                                <th>C&oacute;digo</th>
                                <th>Nombre Completo</th>
                                <th>Oficina</th>
                                <th>Unidad</th>
                                <th class="col-md-2">Accion</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($empleado as $empleados)
                            <tr>
                                <td>{{ $empleados->codigo_empleado }}</td>
                                <td>{{ $empleados->nombre}} {{$empleados->apellido}}</td>
                                <td>{{ $empleados->oficina->oficina}}</td>
                                <td>{{ $empleados->oficina->unidadAdministrativa->unidad_administrativa}}</td>
                                <td align="center">
                                    <a href="{{ route('usuarios.crear', $empleados->id_empleado) }}" class="btn btn-primary" ><span class="fa fa-pencil"></span></a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </section>
        </div>
    </div>
    <!-- end: page -->
</section>

@endsection
