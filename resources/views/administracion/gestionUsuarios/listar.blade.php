@extends('layouts.template')
@section('content')
<section role="main" class="content-body">
    <header class="page-header">
        <h2>Gesti&oacute;n de Usuarios</h2>
        <div class="right-wrapper pull-right">
            <ol class="breadcrumbs">
                <li style="margin-left: -25%"><a href="{{url('/')}}"><button class="btn btn-default"><span class="fa fa-chevron-left"> </span> Regresar </button></a></li>
            </ol>
        </div>
    </header>

    <!-- start: page -->
    <div class="row">
        <div class="col-lg-12">
            <div>
                @if ($errors->any())
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    
                    <div class="errors">
                        <ul>
                            @foreach ($errors->all() as $error)

                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif

                </div>
            </div>
            <section class="panel">
                @include('flash::message')
                <header class="panel-heading">
                    @can('crear.usuarios')
                    <div class="panel-actions">
                        <a href="{{route('usuarios.asignarEmpleado')}}"><button class="btn btn-primary"><span class="fa fa-plus"></span> Agregar Usuario </button></a>
                        
                                  <!--  <a href="#" class="fa fa-caret-down"></a>
                                    <a href="#" class="fa fa-times"></a>-->
                                </div>
                                @endcan
                                
                                <h2 class="panel-title">Listado de Usuarios</h2>
                            </header>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="tabs">
                                        <ul class="nav nav-tabs nav-justified">
                                            <li class="active">
                                                <a href="#popular10" data-toggle="tab" class="text-center"><i class="fa fa-star"></i> Habilitados</a>
                                            </li>
                                            <li>
                                                <a href="#recent10" data-toggle="tab" class="text-center">Deshabilitados</a>
                                            </li>
                                        </ul>
                                        <div class="tab-content">
                                            <div id="popular10" class="tab-pane active">
                                               <div class="panel-body">
                                                <table class="table table-bordered table-striped mb-none" id="datatable-default">
                                        <thead>
                                            <tr>
                                                <th>Nombre Completo</th>
                                                <th>Usuario</th>
                                                <th>Correo</th>
                                                <th>Rol</th>
                                                <th class="col-md-2" align="center">Accion</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($usuario as $usuarios)
                                            <tr>@if(!empty($usuarios->empleado_id))
                                                <td>{{ $usuarios->empleado->nombre}} {{$usuarios->empleado->apellido}}</td>
                                                @else
                                                <td></td>
                                                @endif
                                                <td>{{ $usuarios->nombre_usuario }}</td>
                                                <td>{{ $usuarios->email }}</td>
                                                <td>{{ $usuarios->roles->implode('name', ', ') }}</td>
                                                <td align="center">
                                                    @if($usuarios->id_usuario != $id)
                                                    @can('editar.usuarios')
                                                    <a href="{{ route('usuarios.editar', $usuarios->id_usuario) }}" class="btn btn-warning" ><span class="fa fa-pencil"></span></a>
                                                    @endcan
                                                    @can('eliminar.usuarios')
                                                    <a href="{{ route('usuarios.eliminar', $usuarios->id_usuario) }}" 
                                                        class="btn btn-danger eliminar" ><span class="fa fa-trash-o"></span></a>
                                                        @endcan
                                                        @endif
                                                        @can('resetear.password')
                                                        <a href="{{ route('usuarios.cambiarContraseña', $usuarios->id_usuario) }}" class="btn btn-success" ><span class="fa fa-key"></span></a>
                                                        @endcan
                                                    </td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                        </div>
                                    </div>
                                    <div id="recent10" class="tab-pane">
                                        <div class="panel-body">
                                            <table class="table table-bordered table-striped mb-none" id="datatable-pestana">
                                        <thead>
                                            <tr>
                                                <th>Nombre Completo</th>
                                                <th>Usuario</th>
                                                <th>Correo</th>
                                                <th>Rol</th>
                                                <th>Accion</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($usuarioDeshabilitado as $usuarios)
                                            <tr>
                                                <td>{{ $usuarios->empleado->nombre}} {{$usuarios->empleado->apellido}}</td>
                                                <td>{{ $usuarios->nombre_usuario }}</td>
                                                <td>{{ $usuarios->email }}</td>
                                                <td>{{ $usuarios->roles->implode('name', ', ') }}</td>
                                                <td class="col-md-1">
                                                    @if($usuarios->empleado->activo==1)
                                                   <a href="{{ route('usuarios.activar', $usuarios->id_usuario) }}"  class="btn btn-primary habilitar"  ><span class="fa fa-caret-up"></span></a>
                                                   @endif
                                                    </td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
    <!-- end: page -->
</section>

@endsection

