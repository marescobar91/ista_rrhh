@extends('layouts.template')
@section('content')
<section role="main" class="content-body">
    <header class="page-header">
        <h2>Gesti&oacute;n de Usuarios</h2>
        <div class="right-wrapper pull-right">
            <ol class="breadcrumbs">
                <li style="margin-left: -25%"><a href="{{url('/')}}"><button class="btn btn-default"><span class="fa fa-chevron-left"> </span> Regresar </button></a></li>
            </ol>
        </div>
    </header>

    <!-- start: page -->
    <div class="row">
        <div class="col-lg-12">
            <div>
                @if ($errors->any())
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                    <div class="errors">
                        <ul>
                            @foreach ($errors->all() as $error)

                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif

                </div>
            </div>

            <section class="panel">
                <header class="panel-heading">
                    <div class="panel-actions">
                    </div>
                    <h2 class="panel-title">Cambiar contraseña de {{$usuario->nombre_usuario}}</h2>
                </header>
                <div class="panel-body">
                    <form action="{{ route('usuarios.guardarContraseña', $usuario->id_usuario) }}" method="post" role="form" class="contactForm">
                       {{csrf_field()}}
                       {{ method_field('PUT') }}
                       <div class="col-md-2"></div>
                       <div class="col-md-8">
                           <div class="form-group">
                            
                             <label class="col-sm-12 control-label">Nueva Contraseña</label>
                            <div class="col-md-12">
                                <input id="password" type="password" name="password" required autocomplete="contraseña" autofocus placeholder="Nueva Contraseña" class="form-control field-input {{ $errors->has('password') ? 'field-error' : '' }}">
                                @if ($errors->has('password'))<span class="error-message">{{ $errors->first('password') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            
                             <label class="col-sm-12 control-label">Confirmar Contraseña</label>
                            <div class="col-md-12">
                                <input id="newpassword" type="password" name="newpassword" required autocomplete="nuevacontraseña" autofocus placeholder="Confirmar Contraseña" class="form-control field-input {{$errors->has('newpassword') ? 'field-error' : '' }}">
                                @if ($errors->has('newpassword'))<span class="error-message">{{ $errors->first('newpassword') }}</span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="form-group"></div>


                    <div class="col-lg-12 text-center" >    
                     <button type="submit" class="btn btn-warning">Editar</button>
                     <a href="{{route('home')}}" class="btn btn-default">Cancelar</a>
                 </div>  

             </form>
         </div>
     </section>

 </div>
</div>
<!-- end: page -->
</section>

@endsection