@extends('layouts.template')
@section('content')
<section role="main" class="content-body">
    <header class="page-header">
        <h2>Gesti&oacute;n de Usuarios</h2>
        <div class="right-wrapper pull-right">
            <ol class="breadcrumbs">
                <li style="margin-left: -25%"><a href="{{url('/')}}"><button class="btn btn-default"><span class="fa fa-chevron-left"> </span> Regresar </button></a></li>
            </ol>
        </div>
    </header>
    <!-- start: page -->
    <div class="row">
        <div class="col-lg-12">
            <div>
                @if ($errors->any())
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                    <div class="errors">
                        <ul>
                            @foreach ($errors->all() as $error)

                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                </div>
            </div>
            <section class="panel">
                <header class="panel-heading">
                    <div class="panel-actions">
                    </div>
                    <h2 class="panel-title">Asignar Oficina a Secretaria</h2>
                </header>
                <div class="panel-body">
                    <form action="{{ route('usuario.oficina.guardar', $usuario->id_usuario) }}" method="post" role="form" class="contactForm">
                      {{csrf_field()}}                               
                    {{ method_field('PUT') }}     
                        <div class="col-md-2"></div>        
                        <div class="col-md-8">
                            <div class="form-group">
                                <label class="col-md-12 control-label ">Nombre de Empleado</label>
                                <div class="col-md-12">
                                    <input  type="text" name="nombre_usuario" value="{{$usuario->empleado->nombre}} {{$usuario->empleado->apellido}}" required autocomplete="" autofocus class="form-control field-input {{ $errors->has('nombre_usuario') ? 'field-error' : '' }}" readonly>
                                    @if ($errors->has('nombre_usuario'))<span class="error-message">{{ $errors->first('nombre_usuario') }}</span>
                                    @endif
                                </div>
                            </div>  
                            <div class="form-group">
                                <label class="col-md-12 control-label ">Oficina</label>
                                <div class="col-md-12">
                                    <input id="" type="text"  name="" value="{{$usuario->empleado->oficina->oficina}}" required autocomplete="" autofocus class="form-control field-input {{ $errors->has('oficina') ? 'field-error' : '' }}" readonly>
                                    @if ($errors->has('oficina'))<span class="error-message">{{ $errors->first('oficina') }}</span>
                                    @endif
                                </div>
                            </div> 
                            <div class="form-group">
                                <p>Agregar oficina para programación de viaticos</p>
                                <label class="control-label col-md-12">Asignar Otra Oficina</label>
                                <div class="col-md-12">

                                    @if($accion=='Crear')
                                    <select multiple data-plugin-selectTwo class="form-control populate" name="oficinas[]">
                                      @foreach($oficinas as $oficinas)
                                        <?php $selectedvalue=$oficinas['id_oficina'] ?>
                                 <option value="{{ $oficinas['id_oficina']}}" {{$oficinaActual == $oficinas['id_oficina'] ? 'selected="selected"' : ''}}> 
                                 {{$oficinas['oficina']}}</option>

                                   @endforeach
                                    </select>
                                    @endif
                                    @if($accion=='Editar')

                                    <select multiple data-plugin-selectTwo class="form-control populate" name="oficinas[]">
                                @foreach($oficinas as $oficina)
                        @if(in_array($oficina->id_oficina, $oficinasActuales))
                        <option value="{{ $oficina->id_oficina }}" selected="true">{{ $oficina->oficina }}</option>
                        @else
                        <option value="{{ $oficina->id_oficina }}">{{ $oficina->oficina }}</option>
                        @endif 
                    @endforeach

                                    </select>


                                    @endif
                                    @if ($errors->has('oficinas'))<span class="error-message">{{ $errors->first('oficinas') }}</span>
                                    @endif

                                    <input type="hidden" name="accion" value="{{$accion}}">

                                </div>
                            </div>
                        </div>
                        <div class="form-group"></div>
                        <div class="col-lg-12 text-center" >    
                         <button type="submit" class="btn btn-primary">Guardar</button>
                         <a href="{{route('home')}}" class="btn btn-default">Cancelar</a>
                     </div>  

                 </form>
             </div>
         </section>

     </div>
 </div>
 <!-- end: page -->
</section>

@endsection