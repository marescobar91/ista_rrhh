@extends('layouts.template')
@section('content')
<section role="main" class="content-body">
    <header class="page-header">
        <h2>Gesti&oacute;n de Usuarios</h2>
        <div class="right-wrapper pull-right">
            <ol class="breadcrumbs">
                <li style="margin-left: -25%"><a href="{{url('/')}}"><button class="btn btn-default"><span class="fa fa-chevron-left"> </span> Regresar </button></a></li>
            </ol>
        </div>
    </header>

    <!-- start: page -->
    <div class="row">
        <div class="col-lg-12">
            <div>
                @if ($errors->any())
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                    <div class="errors">
                        <ul>
                            @foreach ($errors->all() as $error)

                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif

                </div>
            </div>
            <section class="panel">
                <header class="panel-heading">
                    <div class="panel-actions">
                    </div>
                    <h2 class="panel-title">Editar Usuarios</h2>
                </header>
                <div class="panel-body">
                  <form action="{{ route('usuarios.actualizar', $usuario->id_usuario) }}" method="post" role="form" class="contactForm">
                    {{csrf_field()}}                               
                    {{ method_field('PUT') }}     
                    <div class="col-md-2"></div>
                    <div class="col-md-8">                  
                        <div class="form-group">
                            
                                <div class="col-md-3"></div>
                                @if($usuario->empleado_id != null)
                                <h4>Nombre del Empleado: {{ $usuario->empleado->nombre }} {{ $usuario->empleado->apellido }}</h4>
                                @endif
                            </div>
                            <div class="form-group">
                                
                                <label class="col-md-12 control-label">Nombre de Usuario</label>
                                <div class="col-md-12">
                                <input id="nombre_usuario" type="text" value="{{ $usuario->nombre_usuario}}" name="nombre_usuario" required autocomplete="nombre_usuario" autofocus placeholder="Nombre del Usuario" class="form-control field-input {{ $errors->has('nombre_usuario') ? 'field-error' : '' }}">
                                @if ($errors->has('nombre_usuario'))<span class="error-message">{{ $errors->first('nombre_usuario') }}</span>
                                @endif
                          
                            </div>
                        </div>  
                        <div class="form-group">
                            
                            <label class="col-sm-12 control-label ">Correo Electr&oacute;nico</label>
                            <div class="col-md-12">
                                <input id="email" type="email" value="{{ $usuario->email}}" name="email" required autocomplete="correo" autofocus placeholder="Nombre del Usuario" class="form-control field-input {{ $errors->has('email') ? 'field-error' : '' }}">
                                @if ($errors->has('email'))<span class="error-message">{{ $errors->first('email') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                           
                                <label class="control-label col-md-12">Asignar Rol</label>
                              
                             @if($usuario->hasRole('Administrador'))
                               <div class="col-sm-12">
                                <select multiple data-plugin-selectTwo class="form-control populate" name="rol[]" required="required">

                                    @foreach($roles as $role)

                                    <option value="{{ $role->id }}" selected>{{ $role->name }}</option>
                                  
                                    @endforeach
                                </select>

                                @if ($errors->has('rol'))<span class="error-message">{{ $errors->first('rol') }}</span>
                                @endif
                            </div>
                             @else
                              <div class="col-sm-12">
                                <select multiple data-plugin-selectTwo class="form-control populate" name="rol[]" required="required">
                                @foreach($rol as $roles)

                                    @if($usuario->hasRole($roles->name))

                                    <option value="{{ $roles->id }}" selected>{{ $roles->name }}</option>
                                    @else
                                    <option value="{{ $roles->id }}">{{ $roles->name }}</option>
                                    @endif
                                    @endforeach
                                </select>

                                @if ($errors->has('rol'))<span class="error-message">{{ $errors->first('rol') }}</span>
                                @endif
                            </div>
                            @endif
                        </div>
                    </div>
                    <div class="form-group"></div>
                    <div class="col-lg-12 text-center" >    
                     <button type="submit" class="btn btn-warning">Editar</button>
                     <a href="{{route('home')}}" class="btn btn-default">Cancelar</a>
                 </div>                              
             </form>
         </div>
     </section>

 </div>
</div>
<!-- end: page -->
</section>

@endsection