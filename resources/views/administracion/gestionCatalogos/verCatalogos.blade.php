@extends('layouts.template')
@section('content')
<section role="main" class="content-body">
    <header class="page-header">
        <h2>Gestión de Catalogos</h2>
        <div class="right-wrapper pull-right">
            <ol class="breadcrumbs">
                <li style="margin-left: -25%"><a href="{{url('/')}}"><button class="btn btn-default"><span class="fa fa-chevron-left"> </span> Regresar </button></a></li>
            </ol>
        </div>
    </header>

    <!-- start: page -->
    <div class="row">
        <div class="col-lg-12">
            <div>
                @if ($errors->any())
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    
                    <div class="errors">
                        <ul>
                            @foreach ($errors->all() as $error)

                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif

                </div>
            </div>
            <section class="panel">
                <header class="panel-heading">
                    <div class="panel-actions">
                    </div>
                    <h2 class="panel-title">Listado de Catalogos</h2>
                </header>
                <div class="panel-body ">
                    <div class="col-md-4" >
                        <section class="panel panel-primary">
                            <header class="panel-heading">
                                @can('consultar.catalogos')
                                <a href="{{route('bancos.listar')}}"><h2 class="panel-title">Bancos</h2></a> 
                                <br/>        
                                @endcan

                            </header>
                        </section>
                        <section class="panel panel-primary">
                            <header class="panel-heading">
                                @can('consultar.catalogos')
                                <a href="{{ route('centros.listar') }}"><h2 class="panel-title">  Centros</h2></a>
                                @endcan
                                <br/>   
                            </header>
                        </section>
                        <section class="panel panel-primary">
                            <header class="panel-heading">
                                @can('consultar.catalogos')
                                <a href="{{route('nivelesAcademicos.listar')}}"><h2 class="panel-title"> Niveles Académicos</h2></a>      
                                @endcan
                                <br/>   
                            </header>
                        </section>
                        <section class="panel panel-primary">
                            <header class="panel-heading">
                                <!--Falta el can-->
                                <a href="{{ route('tipo.permiso.listar') }}"><h2 class="panel-title">  Tipo de Permiso</h2></a><br/>   
                            </header>
                        </section>
                         <section class="panel panel-primary">
                            <header class="panel-heading">
                                @can('consultar.catalogos')
                                <a href="{{ route('lugar.misiones.listar') }}"><h2 class="panel-title">  Lugar de Misión</h2></a> 
                                @endcan
                                <br/>         
                            </header>
                        </section>
                    </div>
                    <div class="col-md-4" >
                        <section class="panel panel-primary">
                            <header class="panel-heading">
                                @can('consultar.catalogos')
                                <a href="{{route('cargos.listar')}}"><h2 class="panel-title">  Cargos</h2></a>
                                @endcan
                                <br/>   
                            </header>
                        </section>
                        <section class="panel panel-primary">
                            <header class="panel-heading">
                                @can('consultar.catalogos')
                                <a href="{{ route('unidadesAdministrativas.listar') }}"><h2 class="panel-title">  Unidad Administrativa </h2></a>
                                @endcan
                                <br/>   
                            </header>
                        </section>
                        <section class="panel panel-primary">
                            <header class="panel-heading">
                                @can('consultar.catalogos')
                                <a href="{{route('tiposPlaza.listar')}}"><h2 class="panel-title">  Tipo de Plazas</h2></a>
                                @endcan
                                <br/>   
                            </header>
                        </section>
                         <section class="panel panel-primary">
                            <header class="panel-heading">
                               @can('consultar.catalogos')
                                <a href="{{route('actividades.listar')}}"><h2 class="panel-title">Actividades</h2></a>
                             @endcan
                             <br/>   
                            </header>
                        </section>
                          <section class="panel panel-primary">
                            <header class="panel-heading">
                               @can('consultar.catalogos')
                                <a href="{{route('tipo.programaciones.listar')}}"><h2 class="panel-title">Tipo de Programaciones</h2></a>
                             @endcan
                             <br/>   
                            </header>
                        </section>
                    </div>
                    <div class="col-md-4" >
                        <section class="panel panel-primary">
                            <header class="panel-heading">
                                @can('consultar.catalogos')
                                <a href="{{route('cargosMP.listar')}}"><h2 class="panel-title">  Cargos Manual Puestos</h2></a>     
                                @endcan
                                <br/>   
                            </header>
                        </section>
                        <section class="panel panel-primary">
                            <header class="panel-heading">
                                @can('consultar.catalogos')
                                <a href="{{ route('oficinas.listar') }}"><h2 class="panel-title">  Oficinas </h2></a> 
                                @endcan
                                <br/>   
                            </header>
                        </section>
                        <section class="panel panel-primary">
                            <header class="panel-heading">
                                @can('consultar.catalogos')
                                <a href="{{route('cargosHacienda.listar')}}"><h2 class="panel-title">  Cargos de Hacienda</h2></a>
                                @endcan
                                <br/>       
                            </header>
                        </section>
                        <section class="panel panel-primary">
                            <header class="panel-heading">
                              @can('consultar.catalogos')
                                <a href="{{route('formas.pago.listar')}}"><h2 class="panel-title"> Formas de Pago</h2></a>
                         @endcan
                         <br/>   
                            </header>
                        </section>
                         <section class="panel panel-primary">
                            <header class="panel-heading">
                              @can('consultar.catalogos')
                                <a href="{{route('tipo.viaticos.listar')}}"><h2 class="panel-title"> Tipo de Viáticos</h2></a>
                            @endcan
                            <br/>   
                            </header>
                        </section>
                        
                       
                    </div>
                </div>
            </section>
            

        </div>
    </div>
    <!-- end: page -->
</section>

@endsection