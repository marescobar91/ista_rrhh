@extends('layouts.template')
@section('content')
<section role="main" class="content-body">
    <header class="page-header">
        <h2>Gesti&oacute;n de Permisos</h2>
        <div class="right-wrapper pull-right">
            <ol class="breadcrumbs">
                <li style="margin-left: -25%"><a href="{{url('/')}}"><button class="btn btn-default"><span class="fa fa-chevron-left"> </span> Regresar </button></a></li>
            </ol>
        </div>
    </header>

    <!-- start: page -->
    <div class="row">
        <div class="col-lg-12">
            <div>
                @if ($errors->any())
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    
                    <div class="errors">
                        <ul>
                            @foreach ($errors->all() as $error)

                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif

                </div>
            </div>
            <section class="panel">
                @include('flash::message')
                <header class="panel-heading">
                    @can('crear.permisos')
                    <div class="panel-actions">
                        <a href="{{route('permisos.crear')}}"><button class="btn btn-primary"><span class="fa fa-plus"></span> Agregar Permiso </button></a>
                        
                                  <!--  <a href="#" class="fa fa-caret-down"></a>
                                    <a href="#" class="fa fa-times"></a>-->
                                </div>
                                @endcan
                                
                                <h2 class="panel-title">Listado de Permisos</h2>
                            </header>
                            <div class="panel-body" style="overflow: scroll;">
                                <table class="table table-bordered table-striped mb-none" id="datatable-default">
                                    <thead>
                                        <tr>
                                            <th>Nombre del Permiso</th>
                                            <th class="col-md-2">Accion</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                       @foreach($permiso as $permisos)
                                       <tr class="gradeX" >
                                        <td>{{ucfirst(str_replace(".", " ", $permisos->name)) }}</td>
                                        <td class="text-center">
                                            @can('editar.permisos')
                                            <a href="{{ route('permisos.editar', $permisos->id) }}" class="btn btn-warning" ><span class="fa fa-pencil"></span></a>
                                            @endcan
                                            @can('eliminar.permisos')
                                            <a href="{{ route('permisos.eliminar', $permisos->id) }}" class="btn btn-danger eliminar"  ><span class="fa fa-trash-o"></span></a>
                                            @endcan
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </section>
                </div>
            </div>
            <!-- end: page -->
        </section>

        @endsection

