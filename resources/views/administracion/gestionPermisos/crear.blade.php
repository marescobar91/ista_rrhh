@extends('layouts.template')
@section('content')
<section role="main" class="content-body">
    <header class="page-header">
        <h2>Gesti&oacute;n de Permisos</h2>
        <div class="right-wrapper pull-right">
            <ol class="breadcrumbs">
                <li style="margin-left: -25%"><a href="{{route('permisos.listar')}}"><button class="btn btn-default"><span class="fa fa-chevron-left"> </span> Regresar </button></a></li>
            </ol>
        </div>
    </header>

    <!-- start: page -->
    <div class="row">
        <div class="col-lg-12">
            <div>
                @if ($errors->any())
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    
                    <div class="errors">
                        <ul>
                            @foreach ($errors->all() as $error)

                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif

                </div>
            </div>
            <section class="panel">
                <header class="panel-heading">
                    <div class="panel-actions">
                    </div>
                    <h2 class="panel-title">Crear Permisos</h2>
                </header>
                <div class="panel-body" >
                    <form class="form-horizontal form-bordered" method="POST" action="{{route('permisos.guardar')}}">
                        @csrf                        
                        <div class="form-group">
                            <div class="col-md-3"></div>
                            <div class="col-md-6">
                                <label class="col-md-12 control-label" style="text-align: left;">Nombre de Permiso</label>
                                <div class="col-md-12">
                                    <input id="nombre_permiso" type="text" value="{{ old('nombre_permiso') }}" name="nombre_permiso" required autocomplete="nombre_permiso" autofocus placeholder="Nombre del Permiso" class="form-control field-input {{ $errors->has('nombre_permiso') ? 'field-error' : '' }}">
                                    @if ($errors->has('nombre_permiso'))<span class="error-message">{{ $errors->first('nombre_permiso') }}</span>
                                    @endif
                                </div>
                           
                            </div>
                        </div>  
                        <div class="col-lg-12 text-center" >    
                         <button type="submit" class="btn btn-primary">Guardar</button>
                         <a href="{{route('home')}}" class="btn btn-default">Cancelar</a>
                     </div>                              
                 </form>
             </div>
         </section>
         
     </div>
 </div>
 <!-- end: page -->
</section>

@endsection