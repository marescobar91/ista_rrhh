@extends('layouts.template')
@section('content')
<section role="main" class="content-body">
    <header class="page-header">
        <h2>Gestión de Tipo de Permisos</h2>
        <div class="right-wrapper pull-right">
            <ol class="breadcrumbs">
                <li style="margin-left: -25%"><a href="{{route('tipo.permiso.listar')}}"><button class="btn btn-default"><span class="fa fa-chevron-left"> </span> Regresar </button></a></li>
            </ol>
        </div>
    </header>

    <!-- start: page -->
    <div class="row">
        <div class="col-lg-12">
            <div>
                @if ($errors->any())
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    
                    <div class="errors">
                        <ul>
                            @foreach ($errors->all() as $error)

                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif

                </div>
            </div>
            <section class="panel">
                <header class="panel-heading">
                    <div class="panel-actions">
                    </div>
                    <h2 class="panel-title">Editar Tipo de Permiso {{ $tipoPermiso->descripcion }}</h2>
                </header>
                <div class="panel-body">
                 <form action="{{ route('tipo.permiso.actualizar', $tipoPermiso->id_tipo_permiso) }}" method="post" role="form" class="contactForm">
                    {{csrf_field()}}
                    {{ method_field('PUT') }}
                      
                        <div class="col-md-2"></div>
                        <div class="col-md-8">

                            <div class="form-group">
                                
                                 <label class="col-sm-12 control-label" style="text-align: left;">Código de Permiso</label>
                                <div class="col-md-12">
                                    <input id="codigo" type="number" value="{{ $tipoPermiso->codigo_permiso }}" name="codigo" required autocomplete="codigo" autofocus placeholder="Codigo" class="form-control field-input {{ $errors->has('codigo') ? 'field-error' : '' }}">
                                    @if ($errors->has('codigo'))<span class="error-message">{{ $errors->first('codigo') }}</span>
                                    @endif
                                </div>
                            </div>     
                            <div class="form-group">
                                
                                 <label class="col-sm-12 control-label" style="text-align: left;">Descripción</label>
                                <div class="col-md-12">
                                    <input id="descripcion" type="text" value="{{ $tipoPermiso->descripcion }}" name="descripcion" required autocomplete="descripcion" autofocus placeholder="Descripcion" class="form-control field-input {{ $errors->has('descripcion') ? 'field-error' : '' }}">
                                    @if ($errors->has('descripcion'))<span class="error-message">{{ $errors->first('descripcion') }}</span>
                                    @endif
                                </div>
                            </div>                         
                            <div class="form-group">
                                
                                 <label class="col-sm-12 control-label" style="text-align: left;">Número de Horas</label>
                                <div class="col-md-12">
                                   
                                        <input type="number" placeholder="1.0" step="1.0" class="form-control field-input {{ $errors->has('numero_horas') ? 'field-error' : '' }}" name="numero_horas" id="numero_horas" value="{{ $tipoPermiso->numero_horas}}" > 
                                        @if ($errors->has('numero_horas'))<span class="error-message">{{ $errors->first('numero_horas') }}</span>
                                            @endif 
                                      
                                    
                                </div>
                            </div> 
                        </div>
                        <div class="form-group">
                            
                        </div>
     
                      <div class="col-lg-12 text-center" >    
                     <button type="submit" class="btn btn-warning">Editar</button>
                     <a href="{{route('home')}}" class="btn btn-default">Cancelar</a>
                 </div>                              
                 </form>
             </div>
         </section>
         
     </div>
 </div>
 <!-- end: page -->
</section>

@endsection