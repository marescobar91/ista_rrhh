@extends('layouts.template')
@section('content')
<section role="main" class="content-body">
    <header class="page-header">
        <h2>Gestión de Marcaciones</h2>
        <div class="right-wrapper pull-right">
            <ol class="breadcrumbs">
                <li style="margin-left: -25%"><a href="{{route('historialM.listar')}}"><button class="btn btn-default"><span class="fa fa-chevron-left"> </span> Regresar </button></a></li>
            </ol>
        </div>
    </header>

    <!-- start: page -->
    <div class="row">
        <div class="col-lg-12">
            <div>
                @if ($errors->any())
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    
                    <div class="errors">
                        <ul>
                            @foreach ($errors->all() as $error)

                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif

                </div>
            </div>
            @if($vacio == null) 
            <section class="panel"> 
                <header class="panel-heading">
                    <div class="panel-actions">
                    </div>
                    <h2 class="panel-title">Consulta de Marcaciones</h2>
                </header>
                <div class="panel-body">
                    <form class="form-horizontal form-bordered" method="POST" action="{{ route('marcaciones.consultar') }}">
                        @csrf
                        <div class="col-md-12">
                            <div class="col-md-4">
                                <label class="col-md-12 control-label" for="w4-first-name" style="text-align: left;">Codigo Empleado</label>
                                <div class="col-md-12">
                                    <select data-plugin-selectTwo id="codigo_id" class="form-control populate" name="codigo_id" required>
                                        <option value="" selected>Digite codigo</option>
                                        @foreach($empleado as $empleado)
                                        <option value="{{$empleado->id_empleado}}">{{$empleado->codigo_empleado}} || {{$empleado->nombre}} {{$empleado->apellido}}</option>
                                        @endforeach

                                    </select>
                                    @if ($errors->has('codigo_id'))<span class="error-message">{{ $errors->first('codigo_id') }}</span>
                                    @endif
                                </div>
                            </div>

                            <div class="col-md-4">
                                <label
                                class="col-md-12 control-label" for="w4-first-name" style="text-align: left;">Fecha Inicio</label>
                                <div class="col-md-12">
                                   <input id="fecha_inicio" type="date" name="fecha_inicio" required autocomplete="fecha_inicio" autofocus class="form-control field-input {{ $errors->has('fecha_inicio') ? 'field-error' : '' }}">
                                   @if ($errors->has('fecha_inicio'))<span class="error-message">{{ $errors->first('fecha_inicio') }}</span>
                                   @endif
                               </div>
                           </div>
                           <div class="col-md-3">
                            <label class="col-md-12 control-label" for="w4-first-name" style="text-align: left;">Fecha Fin</label>
                            <div class="col-md-12">
                               <input id="fecha_fin" type="date" name="fecha_fin" required autocomplete="fecha_inicio" autofocus class="form-control field-input {{ $errors->has('fecha_fin') ? 'field-error' : '' }}">
                               @if ($errors->has('fecha_fin'))<span class="error-message">{{ $errors->first('fecha_fin') }}</span>
                               @endif
                           </div>
                       </div>
                       
                       <div class="col-md-1">
                        <label class="col-sm-12 control-label" style="text-align: left; color: white" >.</label>
                        <div class="col-md-12">
                            <button class="btn btn-info" ><span class="fa fa-search"></span></button>
                        </div>
                    </div>
                </div>
                
            </form>
        </div>
    </section>
    @endif
    @if($vacio != null)
    <section class="panel">
        <header class="panel-heading">
            <div class="panel-actions">
               <a href="{{route('marcaciones.buscar')}}"><button class="btn btn-success"><span class="fa fa-refresh"></span></button></a>
            </div>
            <h2 class="panel-title">Bitácora de Marcaciones por Empleado</h2>
            
        </header>
        <div class="panel-body">
            <div class="col-md-2">
                <img src="{{asset('img/Logo-Institucional.svg')}}" width="100%" style="padding-top: 10%">
            </div>
            <div class="col-md-8">
                <h5 class="text-center" style="text-transform: uppercase;padding-bottom: 0.75%">Instituto Salvadoreño de Transformación Agraria</h5>
                <h5 class="text-center" style="text-transform: uppercase; padding-bottom: 0.75%">Bitácora de Marcaciones por Empleado</h5>
                <h5 class="text-center" style="text-transform: uppercase; padding-bottom: 0.75%">Marcaciones de <strong>{{date('d-m-Y', strtotime($fechaInicio))}} </strong>desde <strong>{{date('d-m-Y', strtotime($fechaFin))}}</strong> </h5>
                <h5 class="text-center" style="text-transform: uppercase; padding-bottom: 0.75%"> del Empleado <strong>{{ $empleado->nombre }} {{ $empleado->apellido }}</strong></h5>
            </div>
            <div class="col-md-2"></div>
            <div class="form-group"></div>
            <div class="col-md-12">
                <table class="table table-bordered table-striped mb-none">
                    <thead>
                        <tr>
                            
                            <th>Fecha</th>
                            <th>Dia</th>
                            <th>Hora Entrada</th>
                            <th>Hora Salida</th>
                            <th>Trabajo</th>
                            <th>Corto</th>
                            <th>Asueto</th>
                            <th>Permiso</th>
                        </tr>
                    </thead>
                    <tbody>
                     @foreach($marcacionEmpleado as $key => $marcacion)
                      @if($marcacion[3]!=null)
                     <tr style="color:red">
                       
                        <td>{{date('d-m-Y', strtotime($marcacion[0]))}}</td>
                        <td>@php
                            if (date('l', strtotime($marcacion[0]))=='Sunday')
                            {echo 'Domingo';}
                            elseif (date('l', strtotime($marcacion[0]))=='Monday'){
                            {echo 'Lunes';}
                        } elseif (date('l', strtotime($marcacion[0]))=='Tuesday'){
                        {echo 'Martes';}
                    } elseif (date('l', strtotime($marcacion[0]))=='Wednesday'){
                    {echo 'Miércoles';}
                } elseif (date('l', strtotime($marcacion[0]))=='Thursday'){
                {echo 'Jueves';}
            } elseif (date('l', strtotime($$marcacion[0]))=='Friday'){
            {echo 'Viernes';}
        } elseif (date('l', strtotime($marcacion[0]))=='Saturday'){
        {echo 'Sábado';}
    }
@endphp</td>
<td>{{ $marcacion[1] }}</td>
<td>{{ $marcacion[2] }}</td>
<td>{{ $marcacion[6] }}</td>
<td>{{ $marcacion[7] }}</td>
<td>{{ $marcacion[4]}}</td>
<td>{{$marcacion[3]}}</td>

</tr>
@endif
@if($marcacion[3]==null)
<tr>
 @if(date('l', strtotime($marcacion[0]))!='Sunday' || date('l', strtotime($marcacion[0]))!='Saturday')
 <td>{{date('d-m-Y', strtotime($marcacion[0]))}}</td>
<td>@php
                            if(date('l', strtotime($marcacion[0]))=='Monday')
                            {echo 'Lunes';}
                        elseif (date('l', strtotime($marcacion[0]))=='Tuesday'){
                        {echo 'Martes';}
                    } elseif (date('l', strtotime($marcacion[0]))=='Wednesday'){
                    {echo 'Miércoles';}
                } elseif (date('l', strtotime($marcacion[0]))=='Thursday'){
                {echo 'Jueves';}
            } elseif (date('l', strtotime($marcacion[0]))=='Friday'){
            {echo 'Viernes';}
             }elseif(date('l', strtotime($marcacion[0]))=='Sunday')
                             {echo 'Domingo';}
                           elseif (date('l', strtotime($marcacion[0]))=='Saturday'){
                           {echo 'Sábado';}
    }
@endphp</td>
@endif
<td>{{ $marcacion[1] }}</td>
<td>{{ $marcacion[2] }}</td>
<td>{{ $marcacion[6] }}</td>
<td>{{ $marcacion[7] }}</td>
<td>{{ $marcacion[4]}}</td>
<td>{{$marcacion[3]}}</td>

</tr>
@endif
    
@endforeach
</tbody>
</table>
</div> 
</div>

</section>
@endif
</div>
</div>
<!-- end: page -->
</section>

@endsection
