@extends('layouts.template')
@section('content')
<section role="main" class="content-body">
    <header class="page-header">
        <h2>Gestión de Horas Extras</h2>
        <div class="right-wrapper pull-right">
            <ol class="breadcrumbs">
                <li style="margin-left: -25%"><a href="{{route('horas.extras.listar')}}"><button class="btn btn-default"><span class="fa fa-chevron-left"> </span> Regresar </button></a></li>
            </ol>
        </div>
    </header>

    <!-- start: page -->
    <div class="row">
        <div class="col-lg-12">
            <div>
                @if ($errors->any())
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    
                    <div class="errors">
                        <ul>
                            @foreach ($errors->all() as $error)

                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif

                </div>
            </div>
            @include('flash::message')
            <section class="panel"> 
                <header class="panel-heading">
                    <div class="panel-actions">
                    </div>
                    <h2 class="panel-title">Ingreso de Horas Extras</h2>
                </header>
                <div class="panel-body">
                    <form class="form-horizontal form-bordered">
                     <!--   @csrf  -->
                        <div class="col-md-12">
                            <div class="col-md-2"></div>
                            <div class="col-md-8">
                                <label class="col-md-12 control-label" for="w4-first-name" style="text-align: left;">Nombre Empleados</label>
                                <div class="col-md-12">
                                    <select data-plugin-selectTwo id="codigo_id" name="codigo_id" class="form-control populate" required>
                                        <option value="">Seleccione Empleado</option>
                                        @foreach($empleado as $empleado)
                                        <option value="{{$empleado->id_empleado}}">{{$empleado->codigo_empleado}} || {{$empleado->nombre}} {{$empleado->apellido}}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('codigo_id'))<span class="error-message">{{ $errors->first('codigo_id') }}</span>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="panel-body">
                    <form class="form-horizontal form-bordered" method="POST" action="{{route('horas.extras.guardar')}}">
                     @csrf
                      <input id="id_empleado" name="id_empleado" type="hidden" value="">
                      <div class="col-md-12">
                          <div class="col-md-2"></div>
                          <div class="col-md-2">                       
                            <label class="col-sm-12 control-label" style="text-align: left;" >PIN:</label>
                            <div class="col-md-12">
                                <input id="pin_empleado" type="text" name="pin_empleado" required autocomplete="pin_empleado" autofocus placeholder="Nombre del Cargo" disabled value="" class="form-control field-input {{ $errors->has('pin_empleado') ? 'field-error' : '' }}" >
                                @if ($errors->has('pin_empleado'))<span class="error-message">{{ $errors->first('pin_empleado') }}</span>
                                @endif
                            </div> 
                        </div>
                        <div class="col-md-6">
                            <label class="col-sm-12 control-label" style="text-align: left;" >Nombre:</label>
                            <div class="col-md-12">
                                <input id="nombre_empleado" type="text" name="nombre" required autocomplete="nombre" autofocus placeholder="Nombre" disabled value="" class="form-control field-input {{ $errors->has('nombre') ? 'field-error' : '' }}" >
                                @if ($errors->has('nombre'))<span class="error-message">{{ $errors->first('nombre') }}</span>
                                @endif
                            </div>
                        </div>
                        
                      </div>
                      <div class="col-md-12">
                        <div class="col-md-2"></div>
                        <div class="col-md-2">
                            <label class="col-sm-12 control-label" style="text-align: left;" >Codigo:</label>
                            <div class="col-md-12">
                                <input id="codigo_empleado" type="text" name="codigo_empleado" required autocomplete="codigo_empleado" autofocus disabled disabled value="" class="form-control field-input {{ $errors->has('pin_empleado') ? 'field-error' : '' }}" >
                                @if ($errors->has('pin_empleado'))<span class="error-message">{{ $errors->first('pin_empleado') }}</span>
                                @endif
                            </div> 
                        </div>
                        <div class="col-md-6">
                            <label class="col-sm-12 control-label" style="text-align: left;" >Oficina:</label>
                            <!--colocar fecha actual por defecto-->
                            <div class="col-md-12">
                                <input id="oficina" type="text" name="oficina" required autocomplete="oficina" autofocus placeholder="Oficina" value="" disabled class="form-control field-input {{ $errors->has('oficina') ? 'field-error' : '' }}">
                                @if ($errors->has('oficina'))<span class="error-message">{{ $errors->first('oficina') }}</span>
                                @endif
                            </div>
                        </div>
                        
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-2"></div>
                        <div class="col-md-2">
                            <label class="col-sm-12 control-label" style="text-align: left;" >Centro:</label>
                            <!--colocar fecha actual por defecto-->
                            <div class="col-md-12">
                                <input id="centro" type="text" name="centro" required autocomplete="centro" autofocus placeholder="Centro" value="" disabled class="form-control field-input {{ $errors->has('centro') ? 'field-error' : '' }}">
                                @if ($errors->has('centro'))<span class="error-message">{{ $errors->first('centro') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label class="col-sm-12 control-label" style="text-align: left;" >Unidad Administrativa:</label>
                            <!--colocar fecha actual por defecto-->
                            <div class="col-md-12">
                                <input id="unidad" type="text" name="unidad" required autocomplete="unidad" autofocus placeholder="Nombre Unidad" value="" disabled class="form-control field-input {{ $errors->has('unidad') ? 'field-error' : '' }}">
                                @if ($errors->has('unidad'))<span class="error-message">{{ $errors->first('unidad') }}</span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="form-group"></div>
                    <div class="col-md-12">
                        <div class="col-md-3"></div>
                        <div class="col-md-3">
                            <label class="col-sm-12 control-label" style="text-align: left;">Fecha Desde:</label>
                            <div class="col-md-12">
                                <input id="fecha_inicio" type="date" name="fecha_inicio" required autocomplete="fecha_inicio" autofocus placeholder="Fecha Desde" class="form-control field-input {{ $errors->has('fecha_inicio') ? 'field-error' : '' }}">
                                @if ($errors->has('fecha_inicio'))<span class="error-message">{{ $errors->first('fecha_inicio') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-3">
                            <label class="col-sm-12 control-label" style="text-align: left;" >Horas:</label>
                            <div class="col-md-12">
                                <input id="hora" type="number" name="hora" required autocomplete="hora" autofocus  class="form-control field-input {{ $errors->has('hora') ? 'field-error' : '' }}">
                                @if ($errors->has('hora'))<span class="error-message">{{ $errors->first('hora') }}</span>
                                @endif
                            </div>
                        </div>

                    </div>
                    <div class="col-md-12">
                        <div class="col-md-3"></div>
                        <div class="col-md-3">
                            <label class="col-sm-12 control-label" style="text-align: left;" >Fecha Hasta:</label>
                            <div class="col-md-12">
                                <input id="fecha_final" type="date" name="fecha_final" required autocomplete="fecha_final" autofocus  class="form-control field-input {{ $errors->has('fecha_final') ? 'field-error' : '' }}">
                                @if ($errors->has('fecha_final'))<span class="error-message">{{ $errors->first('fecha_final') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-3">
                            <label class="col-sm-12 control-label" style="text-align: left;" >Minutos:</label>
                            <!--colocar fecha actual por defecto-->
                            <div class="col-md-12">
                                <input id="minutos" type="number" name="minutos" required autocomplete="minutos" autofocus class="form-control field-input {{ $errors->has('minutos') ? 'field-error' : '' }}">
                                @if ($errors->has('minutos'))<span class="error-message">{{ $errors->first('minutos') }}</span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-3"></div>
                        <div class="col-md-3">
                            <label class="col-sm-12 control-label" style="text-align: left;" >Mes:</label>
                            <div class="col-md-12">
                                <select name="mes" id="mes"  data-plugin-selectTwo class="form-control populate" required>
                                        <option value="">Seleccionar Mes</option>
                                        <option value="1">Enero</option>
                                        <option value="2">Febrero</option>
                                        <option value="3">Marzo</option>
                                        <option value="4">Abril</option>
                                        <option value="5">Mayo</option>
                                        <option value="6">Junio</option>
                                        <option value="7">Julio</option>
                                        <option value="8">Agosto</option>
                                        <option value="9">Septiembre</option>
                                        <option value="10">Octubre</option>
                                        <option value="11">Noviembre</option>
                                        <option value="12">Diciembre</option>
                                    </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <label class="col-sm-12 control-label" style="text-align: left;" >Año:</label>
                            <div class="col-md-12">
                                <input id="anio" type="number" name="anio" required autocomplete="minutos" autofocus value="{{$anio}}" class="form-control field-input {{ $errors->has('anio') ? 'field-error' : '' }}" pattern="{0-9}{4}" min="4" maxlength="4">
                                @if ($errors->has('anio'))<span class="error-message">{{ $errors->first('anio') }}</span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="form-group"></div>
                    <div class="col-md-12 text-center">
                        <button type="submit" class="btn btn-primary">Guardar</button>
                        <a href="{{route('home')}}" class="btn btn-default">Cancelar</a>
                    </div>
                </form>
            </div>  
        </section>
    </div>
</div>
<!-- end: page -->
</section>

@endsection

@section('js')

<script type="text/javascript">
        $(document).ready(function() {
            function cargarEmpleado(){
                var codigo_id = $('#codigo_id').val();
                if($.trim('codigo_id') != ''){
                    $.get('/empleado/horas/extras/crear', {codigo_id:  codigo_id}, function (data){
                        //console.log(emp);
                        $("#id_empleado").val(data.id);
                        $("#pin_empleado").val(data.pin);
                        $("#codigo_empleado").val(data.codigo_empleado);
                        $("#nombre_empleado").val(data.nombre);
                        $("#oficina").val(data.oficina);
                        $("#unidad").val(data.unidad);
                        $("#centro").val(data.centro);
                    });
            }
        }
            //cargarEmpleado();
            $('#codigo_id').on('change', cargarEmpleado);
        });

    </script>
@endsection