<table>
    <thead>
        <tr>
            <th>Codigo </th>
            <th>Nombre</th>
            <th>Salario</th>
            <th>Horas</th>
            <th>Minutos </th>
            <th>Devengo</th>
            <th>Total</th>
            <th>Tipo de Contrato </th>
            <th>Unidad Administrativa</th>
        </tr>
    </thead>
    <tbody>
        @foreach($horasExtrasC as $horaE) 
        <tr class="gradeX" >
            <td>{{$horaE->codigo}}</td>
            <td>{{$horaE->nombre}} {{$horaE->apellido}}</td>
            <td>$ {{$horaE->salario_actual}}</td>
            <td>{{$horaE->horas}}</td>
            <td>$ {{round($horaE->devengo,2)}}</td>
            <td>$ {{round($horaE->total,2)}}</td>
            <td>{{$horaE->total}}</td>
            <td>{{$horaE->tipo_plaza}}</td>
            <td>{{$horaE->unidad_administrativa}}</td>
        </tr>
        @endforeach
    </tbody>
    <tfoot>
       <tr>
        <td colspan="5">Total</td>
        <td>$ {{$devengoC->Devengo}}</td>
        <td>$ {{$totalC->Total}}</td>
        <td></td>
        <td></td>
    </tr>
</tfoot>

</table>   
@if(!$horasExtrasL->isEmpty())
<table>
    <thead>
        <tr>
            <th>Codigo </th>
            <th>Nombre</th>
            <th>Salario</th>
            <th>Horas</th>
            <th>Minutos </th>
            <th>Devengo</th>
            <th>Total</th>
            <th>Tipo de Contrato </th>
            <th>Unidad Administrativa</th>
        </tr>
    </thead>
    <tbody>
        @foreach($horasExtrasL as $horaE) 
        <tr class="gradeX" >
            <td>{{$horaE->codigo}}</td>
            <td>{{$horaE->nombre}} {{$horaE->apellido}}</td>
            <td>$ {{$horaE->salario_actual}}</td>
            <td>{{$horaE->horas}}</td>
            <td>$ {{round($horaE->devengo,2)}}</td>
            <td>$ {{round($horaE->total,2)}}</td>
            <td>{{$horaE->total}}</td>
            <td>{{$horaE->tipo_plaza}}</td>
            <td>{{$horaE->unidad_administrativa}}</td>
        </tr>
        @endforeach
    </tbody>
    <tfoot>
       <tr>
        <td colspan="5">Total</td>
        <td>$ {{$devengoL->Devengo}}</td>
        <td>$ {{$totalL->Total}}</td>
        <td></td>
        <td></td>
    </tr>
</tfoot>

</table>   
@endif