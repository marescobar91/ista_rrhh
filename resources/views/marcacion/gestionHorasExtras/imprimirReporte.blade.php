<!DOCTYPE html>

<head>
    <title>ISTA - RRHH</title>
    <link href="css/style.css" rel="stylesheet">
    <style>
        @page { margin: 75px 50px; } 
        .celda{
            border-bottom: lightgrey 0.1em solid;
        }
        .encabezado{
            border-bottom: grey 0.1em solid;
            background-color: grey;
            color: white;
        }
        .subencabezado{
            padding-top: 1%;
            background-color: #ece8e8;"
        }
        #footer { position: fixed; left: 0px; bottom: -150px; right: 33px; height: 150px; } 
        #footer .page:after { content: counter(page, PAGE_NUM); } 
   </style>
</head>
<body>
    <div id="wrapper" style="margin-top: -8%">
        <!-- Page Content -->
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="container" style="margin-left: 20px">

                    <div class="col-md-9 col-md-offset-1">
                        <div class="table-title" style="background-color: #fff; margin-bottom: 1%">

                            <table>
                                <td style="width: 120px">
                                   <div class="row" align="center">
                                <img src="{{public_path().'/img/Logo-Institucional.svg'}}" width="100%" align="left" style="padding-top: 2%">
                                    </div> 
                                </td>
                                <td style="width: 720px" >
                                    <h5 style="text-transform: uppercase; margin-bottom:-16px; padding-left: 200px">Instituto Salvadoreño de Transformación Agraria</h5>
                                    <h5 style="text-transform: uppercase; margin-bottom:-16px;padding-left: 300px">Sección de Horas Extras</h5>
                                    <h5 style="text-transform: uppercase; padding-bottom:-10px; padding-left: 240px">Reporte de Personal con Horas Extras</h5>
                                    <h5 style=" padding-bottom:-10px; padding-left: 210px"> <b>Desde  : </b> {{date('j', strtotime($fechaIni))}} de {{ $mesIni }} del {{date('Y', strtotime($fechaIni)) }}   <b> Hasta : </b> {{date('j', strtotime($fechaFinal))}} de {{ $mesFin }} del {{date('Y', strtotime($fechaFinal))}} </h5>
                                </td>
                                <td>
                                    <div align="center">
                                        <br/>
                                        <h5>Fecha:{{$dia}}</h5> 
                                        <h5>Hora: {{$hora}}</h5>
                                    </div>
                                </td>
                            </table>
                        </div>

                        <div class="row">
                        <table class="table table-striped table-hover col-md-5" id="reporte" style="background: #fff;" width="97%">
                             <thead>
                                <tr>
                                    <td colspan="9" style="text-align: center">Tipo de Contrato: Contrato</td>
                                </tr>
                                <tr class="encabezado">
                                    <th>Codigo </th>
                                    <th>Nombre</th>
                                    <th>Salario</th>
                                    <th>Horas</th>
                                    <th>Minutos </th>
                                    <th>Devengo</th>
                                    <th>Total</th>
                                    <th>Tipo de Contrato </th>
                                    <th>Unidad Administrativa</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($horasExtrasC as $horaE) 
                               <tr class="gradeX" >
                                    <td class="celda">{{$horaE->codigo}}</td>
                                    <td class="celda">{{$horaE->nombre}} {{$horaE->apellido}}</td>
                                    <td class="celda">$ {{$horaE->salario_actual}}</td>
                                    <td class="celda">{{$horaE->horas}}</td>
                                    <td class="celda">{{$horaE->minutos}}</td>
                                    <td class="celda">$ {{round($horaE->devengo,2)}}</td>
                                    <td class="celda">$ {{round($horaE->total,2)}}</td>
                                    <td class="celda">{{$horaE->tipo_plaza}}</td>
                                    <td class="celda">{{$horaE->unidad_administrativa}}</td>
                                </tr>
                                @endforeach
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td colspan="5" class="celda"><strong>Total</strong> </td>
                                    <td class="celda"><strong>$ {{$devengoC->Devengo}}</strong></td>
                                    <td class="celda"><strong>$ {{$totalC->Total}}</strong></td>
                                    <td class="celda"></td>
                                    <td class="celda"></td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                    <br/>
                    @if(!$horasExtrasL->isEmpty())
                    <div style="page-break-after:always;"></div>
                    <div class="row"> 
                        <table class="table table-striped table-hover col-md-5" id="reporte" style="background: #fff;" width="97%">
                             <thead>
                                <tr>
                                    <td colspan="9" style="text-align: center">Tipo de Contrato: Ley de Salarios</td>
                                </tr>
                                <tr class="encabezado">
                                    <th>Codigo </th>
                                    <th>Nombre</th>
                                    <th>Salario</th>
                                    <th>Horas</th>
                                    <th>Minutos </th>
                                    <th>Devengo</th>
                                    <th>Total</th>
                                    <th>Tipo de Contrato </th>
                                    <th>Unidad Administrativa</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($horasExtrasL as $horaE) 
                               <tr class="gradeX" >
                                    <td class="celda">{{$horaE->codigo}}</td>
                                    <td class="celda">{{$horaE->nombre}} {{$horaE->apellido}}</td>
                                    <td class="celda">$ {{$horaE->salario_actual}}</td>
                                    <td class="celda">{{$horaE->horas}}</td>
                                    <td class="celda">{{$horaE->minutos}}</td>
                                    <td class="celda">$ {{round($horaE->devengo,2)}}</td>
                                    <td class="celda">$ {{round($horaE->total,2)}}</td>
                                    <td class="celda">{{$horaE->tipo_plaza}}</td>
                                    <td class="celda">{{$horaE->unidad_administrativa}}</td>
                                </tr>
                                @endforeach
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td colspan="5" class="celda"><strong>Total</strong> </td>
                                    <td class="celda"><strong>$ {{$devengoL->Devengo}}</strong></td>
                                    <td class="celda"><strong>$ {{$totalL->Total}}</strong></td>
                                    <td class="celda"></td>
                                    <td class="celda"></td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                     @endif
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<div id="footer"> 
    <p class="page" align="right">Página <?php $PAGE_NUM ?></p> 
    </div>  
<footer>
    <table>
        <tr >
            <td style="text-align: left; width: 60%">Sistema Informatico para el manejo de Marcaciones y Viaticos de la Gerencia de Recursos Humanos del ISTA</td>
            <td></td>
            <td></td>
        </tr>
    </table>
</footer>
</body>
</html>