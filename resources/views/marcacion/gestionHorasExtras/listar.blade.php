@extends('layouts.template')
@section('content')
<section role="main" class="content-body">
    <header class="page-header">
        <h2>Gesti&oacute;n de Horas Extras</h2>
        <div class="right-wrapper pull-right">
            <ol class="breadcrumbs">
                <li style="margin-left: -25%"><a href="{{url('/')}}"><button class="btn btn-default"><span class="fa fa-chevron-left"> </span> Regresar </button></a></li>
            </ol>
        </div>
    </header>

    <!-- start: page -->
    <div class="row">
        <div class="col-lg-12">
            <div>
                @if ($errors->any())
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    
                    <div class="errors">
                        <ul>
                            @foreach ($errors->all() as $error)

                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif

                </div>
            </div>
            <section class="panel">
                @include('flash::message')
                <header class="panel-heading">
                    <div class="panel-actions">
                        @can('reporte.horas.extras')
                        <a href="{{route('horas.extras.reporte')}}"><button class="btn btn-success"><span class="fa fa-file-pdf-o"></span> Reporte Horas Extras </button></a>
                        @endcan
                        @can('crear.horas.extras')
                        <a href="{{route('horas.extras.buscar')}}"><button class="btn btn-primary"><span class="fa fa-plus"></span> Agregar Horas Extras </button></a>
                        @endcan
                    </div>
                    <h2 class="panel-title">Listado de Horas Extras</h2>
                </header>
                <div class="row">
                    <div class="col-md-12">

                       <div class="panel-body">
                        <table class="table table-bordered table-striped mb-none" id="datatable-default">
                            <thead>
                                <tr>
                                    <th>Nombre del Empleado </th>
                                    <th>Oficina</th>
                                    <th>Fechas Desde</th>
                                    <th>Fecha Hasta</th>
                                    <th>Horas </th>
                                    <th class="col-md-2">Accion</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($horaExtra as $horaE) 
                               <tr class="gradeX" >
                                <td>{{$horaE->empleado->nombre}} {{$horaE->empleado->apellido}}</td>
                                <td>{{$horaE->empleado->oficina->oficina}}</td>
                                <td>{{date('d-m-Y', strtotime($horaE->fecha_desde))}}</td>
                                <td>{{date('d-m-Y', strtotime($horaE->fecha_hasta))}}</td>
                                <td>{{$horaE->horas}}:{{$horaE->minutos}}</td>
                                <td class="text-center">  
                                    @can('editar.horas.extras')
                                    <a href="{{route('horas.extras.editar', $horaE->id_hora_extra)}}" class="btn btn-warning" ><span class="fa fa-pencil"></span></a>
                                    @endcan
                                    @can('eliminar.horas.extras')
                                    <a href="{{route('horas.extras.eliminar', $horaE->id_hora_extra)}}" class="btn btn-danger eliminar"  ><span class="fa fa-trash-o"></span></a>
                                    @endcan
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
<!-- end: page -->
</section>

@endsection
