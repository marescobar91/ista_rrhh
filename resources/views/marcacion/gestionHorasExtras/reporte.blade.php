@extends('layouts.template')
@section('content')
<section role="main" class="content-body">
    <header class="page-header">
        <h2>Reporte de Horas Extras</h2>
        <div class="right-wrapper pull-right">
            <ol class="breadcrumbs">
                <li style="margin-left: -25%"><a href="{{route('horas.extras.listar')}}"><button class="btn btn-default"><span class="fa fa-chevron-left"> </span> Regresar </button></a></li>
            </ol>
        </div>
    </header>
    <!-- start: page -->
    <div class="row">
        <div class="col-lg-12">
           
            @if ($errors->any())
            <div class="alert alert-danger">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                
                <div class="errors">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif

            </div>
        </div>
        <section class="panel">
            @include('flash::message') 
            <header class="panel-heading">
                <h2 class="panel-title">Generar Reporte de Horas Extras</h2>
            </header>
            <div class="row">
                <div class="col-md-12">
                   <div class="panel-body">

                     <form class="form-horizontal" method="POST" action="{{ route('horas.extras.verReporte') }}" >
                        {{ csrf_field() }} 

                        
                        <!-- fecha inicio-->
                        <div class="form-group">
                            <div class="col-md-1"></div>
                            <label class="col-md-2 control-label text-right" for="w4-first-name">Mes de Hora Extra: </label>
                            <div class="col-md-2">
                                    <select name="mes" id="mes"  data-plugin-selectTwo class="form-control populate">
                                        <option value="">Seleccionar Mes</option>
                                        <option value="1">Enero</option>
                                        <option value="2">Febrero</option>
                                        <option value="3">Marzo</option>
                                        <option value="4">Abril</option>
                                        <option value="5">Mayo</option>
                                        <option value="6">Junio</option>
                                        <option value="7">Julio</option>
                                        <option value="8">Agosto</option>
                                        <option value="9">Septiembre</option>
                                        <option value="10">Octubre</option>
                                        <option value="11">Noviembre</option>
                                        <option value="12">Diciembre</option>
                                    </select>
                                    
                            </div>
                            <!-- fecha inicio-->
                            <label class="col-md-2 control-label text-right" for="w4-first-name">Año: </label>
                            <div class="col-md-2">
                                    <input type="text"  class="form-control field-input" name="anioHoy" id="anioHoy"   value="{{$anioHoy}}" min="4" maxlength="4" >                               
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-1"></div>
                            <label class="col-md-2 control-label text-right" for="w4-first-name">Registrado entre: </label>
                            <div class="col-md-2">
                                    <input type="date"  class="form-control field-input" name="fecha_inicio" id="fecha_inicio"  style="background: white" placeholder="dd/mm/yyyy"> 
                            </div>
                            <!-- fecha inicio-->
                            <label class="col-md-2 control-label text-right" for="w4-first-name">Y : </label>
                            <div class="col-md-2">
                                    <input type="date"  class="form-control field-input" name="fecha_final" id="fecha_final"  style="background: white" placeholder="dd/mm/yyyy">                                
                            </div>
                        </div>
                        <!--Boton de Generar Reporte-->
                        <div class="form-group" align="center">
                         <button type="submit" class="btn btn-success">
                            Generar
                        </button>
                    </div>
                    
                </form> 

            </div>
        </div>
    </div>
</section>
@if($vacio != null)
        <section class="panel">
            @include('flash::message') 
            <header class="panel-heading">
                @can('reporte.horas.extras')
                <div class="panel-actions">
                    <div class="col-md-4"></div>
                    <div class="col-md-8">
                        <div class="col-md-6">
                            <form action="{{ route('horas.extras.excel')}}" method="post" role="form" class="contactForm">
                               {{ csrf_field() }}
                            
                                  <input id="fecha_inicio" type="hidden" class="form-control" value="{{$fechaIni}}" name="fecha_inicio">
                                  <input id="fecha_final" type="hidden" class="form-control" value="{{$fechaFin}}" name="fecha_final">
                                  <input id="mesIni" type="hidden" class="form-control" value="{{$mesIni}}" name="mesIni">
                                   <input id="mesExtra" type="hidden" class="form-control" value="{{$mes}}" name="mesExtra">
                                  <input id="anioHoy" type="hidden" class="form-control" value="{{$anioHoy}}" name="anioHoy">
                                  <p> <button class="btn btn-success"><span class="fa fa-file-excel-o"> </span></button></p>                    
                         
                          </form> 
                      </div>
                      <div class="col-md-6">
                       <form action="{{route ('horas.extras.pdf')}}" method="post" role="form" class="contactForm" target="_blank">
                           {{ csrf_field() }}
                           
                               <input id="fecha_inicio" type="hidden" class="form-control" value="{{$fechaIni}}" name="fecha_inicio">
                               
                                  <input id="fecha_final" type="hidden" class="form-control" value="{{$fechaFin}}" name="fecha_final">
                                 
                                   <input id="mesExtra" type="hidden" class="form-control" value="{{$mes}}" name="mesExtra">
                               <button class="btn btn-info"><span class="fa fa-print"> </span></button>              
                        
                       </form> 
                   </div>
               </div>

               @endcan
           </div>
           <h2 class="panel-title">Generar Reporte de Horas Extras</h2>
       </header>
       <div class="row">
        <div class="col-md-12">
         <div class="panel-body">
            <div class="col-md-2">
                <img src="{{asset('img/Logo-Institucional.svg')}}" width="100%" style="padding-top: 10%">
            </div>
            <div class="col-md-8">
                <h5 class="text-center" style="text-transform: uppercase;padding-bottom: 0.75%">Instituto Salvadoreño de Transformación Agraria</h5>
                <h5 class="text-center" style="text-transform: uppercase; padding-bottom: 0.75%">Sección de Horas Extras</h5>
                <h5 class="text-center" style="text-transform: uppercase; padding-bottom: 0.75%">Reporte de Personal con Horas Extras</h5>
                <h5 class="text-center"> <b>Desde  : </b> {{date('j', strtotime($fechaIni))}} de {{ $mesIni }} del {{date('Y', strtotime($fechaIni))}} <b>  Hasta : </b> {{date('j', strtotime($fechaFin))}} de {{ $mesFin }} del {{date('Y', strtotime($fechaFin))}} </h5>
            </div>
            <div class="col-md-2">
                <br/>
                <h5 class="text-center">Fecha: {{ $dia }} </h5>
                <h5 class="text-center">Hora: {{ $hora }}</h5>
            </div>
            <div align="center" style="margin-top: 1%; margin-bottom: 1%" class="col-md-12">
                <h2 class="panel-title">Tipo de Contrato: Contrato</h2>
            </div>
            <table class="table table-bordered table-striped mb-none" >
                <thead>
                    <tr>
                        <th>Codigo </th>
                        <th>Nombre</th>
                        <th>Salario</th>
                        <th>Horas</th>
                        <th>Minutos </th>
                        <th>Devengo</th>
                        <th>Total</th>
                        <th>Tipo de Contrato </th>
                        <th>Unidad Administrativa</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($horasExtrasC as $horaE) 
                    <tr class="gradeX" >
                        <td>{{$horaE->codigo}}</td>
                        <td>{{$horaE->nombre}} {{$horaE->apellido}}</td>
                        <td>$ {{$horaE->salario_actual}}</td>
                        <td>{{$horaE->horas}}</td>
                        <td>{{$horaE->minutos}}</td>
                        <td>$ {{round($horaE->devengo,2)}}</td>
                        <td>$ {{round($horaE->total,2)}}</td>
                        <td>{{$horaE->tipo_plaza}}</td>
                        <td>{{$horaE->unidad_administrativa}}</td>
                    </tr>
                    @endforeach
                </tbody>

                <tfood>

                    <tr>
                        <td colspan="5"><strong>Total</strong> </td>
                        <td><strong>$ {{$devengoC->Devengo}}</strong></td>
                        <td><strong>$ {{$totalC->Total}}</strong> </td>
                        <td></td>
                        <td></td>
                    </tr>
                </tfood>
            </table>    
        </div>
    </div>
</div>
</section>
@if(!$horasExtrasL->isEmpty())
<section class="panel">
    <div class="row">
        <div class="col-md-12">
         <div class="panel-body">
            <div align="center">
                <h2 class="panel-title">Tipo de Contrato: Ley de Salarios</h2>
            </div>
            <table class="table table-bordered table-striped mb-none" >
                <thead>
                    <tr>
                        <th>Codigo </th>
                        <th>Nombre</th>
                        <th>Salario</th>
                        <th>Horas</th>
                        <th>Minutos </th>
                        <th>Devengo</th>
                        <th>Total</th>
                        <th>Tipo de Contrato </th>
                        <th>Unidad Administrativa</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($horasExtrasL as $horaE) 
                    <tr class="gradeX" >
                        <td>{{$horaE->codigo}}</td>
                        <td>{{$horaE->nombre}} {{$horaE->apellido}}</td>
                        <td>$ {{$horaE->salario_actual}}</td>
                        <td>{{$horaE->horas}}</td>
                        <td>{{$horaE->minutos}}</td>
                        <td>$ {{round($horaE->devengo,2)}}</td>
                        <td>$ {{round($horaE->total,2)}}</td>
                        <td>{{$horaE->tipo_plaza}}</td>
                        <td>{{$horaE->unidad_administrativa}}</td>
                    </tr>
                    @endforeach
                </tbody>

                <tfood>

                    <tr>
                        <td colspan="5"><strong>Total</strong> </td>
                        <td><strong>$ {{$devengoL->Devengo}}</strong></td>
                        <td><strong>$ {{$totalL->Total}}</strong> </td>
                        <td></td>
                        <td></td>
                    </tr>
                </tfood>
            </table>    
        </div>
    </div>
</div>
</section>
@endif
@endif
    
                </div>
            </div>
            <!-- end: page -->
        </section>
@endsection
