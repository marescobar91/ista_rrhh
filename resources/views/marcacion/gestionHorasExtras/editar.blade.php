@extends('layouts.template')
@section('content')
<section role="main" class="content-body">
    <header class="page-header">
        <h2>Gestión de Horas Extras</h2>
        <div class="right-wrapper pull-right">
            <ol class="breadcrumbs">
                <li style="margin-left: -25%"><a href="{{route('horas.extras.listar')}}"><button class="btn btn-default"><span class="fa fa-chevron-left"> </span> Regresar </button></a></li>
            </ol>
        </div>
    </header>

    <!-- start: page -->
    <div class="row">
        <div class="col-lg-12">
            <div>
                @if ($errors->any())
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    
                    <div class="errors">
                        <ul>
                            @foreach ($errors->all() as $error)

                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif

                </div>
            </div>
           
            <section class="panel"> 
                <header class="panel-heading">
                    <div class="panel-actions">
                    </div>
                    <h2 class="panel-title">Editar de Horas Extras</h2>
                </header>
            <div class="panel-body">
                <form class="form-horizontal form-bordered" method="POST" action="{{ route('horas.extras.actualizar', $horasExtra->id_hora_extra) }}">
                 {{csrf_field()}}
                {{ method_field('PUT') }}
                    <input id="id_empleado" name="id_empleado" type="hidden" value="{{$empleado->id_empleado}}">
                    <div class="col-md-1"></div>
                  <div class="col-md-10">
                    <div class="col-md-4">                       
                        <label class="col-sm-12 control-label" style="text-align: left;" >PIN:</label>
                        <div class="col-md-12">
                            <input id="pin_empleado" type="text" name="pin_empleado" required autocomplete="pin_empleado" autofocus placeholder="Nombre del Cargo" disabled value="{{$empleado->pin_empleado}}" class="form-control field-input {{ $errors->has('pin_empleado') ? 'field-error' : '' }}" >
                            @if ($errors->has('pin_empleado'))<span class="error-message">{{ $errors->first('pin_empleado') }}</span>
                            @endif
                        </div> 
                    </div>
                    <div class="col-md-8">
                        <label class="col-sm-12 control-label" style="text-align: left;" >Nombre:</label>
                                <div class="col-md-12">
                                    <input id="nombre" type="text" name="nombre" required autocomplete="nombre" autofocus placeholder="Nombre" disabled value="{{$empleado->nombre}} {{$empleado->apellido}}" class="form-control field-input {{ $errors->has('nombre') ? 'field-error' : '' }}" >
                                    @if ($errors->has('nombre'))<span class="error-message">{{ $errors->first('nombre') }}</span>
                                    @endif
                                </div>
                    </div>
                </div>
                <div class="form-group"></div>
                <div class="col-md-1"></div>
                <div class="col-md-10">
                    <div class="col-md-4">
                        <label class="col-sm-12 control-label" style="text-align: left;" >Unidad Administrativa:</label>
                                <!--colocar fecha actual por defecto-->
                                <div class="col-md-12">
                                    <input id="unidad" type="text" name="unidad" required autocomplete="unidad" autofocus placeholder="Nombre Unidad" value="{{$empleado->oficina->unidadAdministrativa->unidad_administrativa}}" disabled class="form-control field-input {{ $errors->has('unidad') ? 'field-error' : '' }}">
                                    @if ($errors->has('unidad'))<span class="error-message">{{ $errors->first('unidad') }}</span>
                                    @endif
                                </div>
                    </div>
                    <div class="col-md-4">
                        <label class="col-sm-12 control-label" style="text-align: left;" >Centro:</label>
                                <!--colocar fecha actual por defecto-->
                                <div class="col-md-12">
                                    <input id="centro" type="text" name="centro" required autocomplete="centro" autofocus placeholder="Centro" value="{{$empleado->oficina->centro->centro}}" disabled class="form-control field-input {{ $errors->has('centro') ? 'field-error' : '' }}">
                                    @if ($errors->has('centro'))<span class="error-message">{{ $errors->first('centro') }}</span>
                                    @endif
                                </div>
                    </div>
                    <div class="col-md-4">
                        <label class="col-sm-12 control-label" style="text-align: left;" >Oficina:</label>
                                <!--colocar fecha actual por defecto-->
                                <div class="col-md-12">
                                    <input id="oficina" type="text" name="oficina" required autocomplete="oficina" autofocus placeholder="Oficina" value="{{$empleado->oficina->oficina}}" disabled class="form-control field-input {{ $errors->has('oficina') ? 'field-error' : '' }}">
                                    @if ($errors->has('oficina'))<span class="error-message">{{ $errors->first('oficina') }}</span>
                                    @endif
                                </div>
                    </div>
                </div>                
                    <div class="form-group"></div>
                    <div class="col-md-12">
                        <div class="col-md-3"></div>
                        <div class="col-md-3">
                            <label class="col-sm-12 control-label" style="text-align: left;">Fecha Desde:</label>
                               <div class="col-md-12"> 
                                <input id="fecha_inicio" type="date" name="fecha_inicio" required autocomplete="fecha_inicio" value="{{$horasExtra->fecha_desde}}" autofocus placeholder="Fecha Desde" class="form-control field-input {{ $errors->has('fecha_inicio') ? 'field-error' : '' }}">
                                @if ($errors->has('fecha_inicio'))<span class="error-message">{{ $errors->first('fecha_inicio') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-3">
                            <label class="col-sm-12 control-label" style="text-align: left;" >Horas:</label>
                            <div class="col-md-12">
                                <input id="hora" type="number" name="hora" required autocomplete="hora" autofocus value="{{$horasExtra->horas}}" class="form-control field-input {{ $errors->has('hora') ? 'field-error' : '' }}">
                                @if ($errors->has('hora'))<span class="error-message">{{ $errors->first('hora') }}</span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-3"></div>
                        <div class="col-md-3">
                            <label class="col-sm-12 control-label" style="text-align: left;" >Fecha Hasta:</label>
                            <div class="col-md-12">
                                <input id="fecha_final" type="date" name="fecha_final" required autocomplete="fecha_final" value="{{$horasExtra->fecha_hasta}}" autofocus  class="form-control field-input {{ $errors->has('fecha_final') ? 'field-error' : '' }}">
                                @if ($errors->has('fecha_final'))<span class="error-message">{{ $errors->first('fecha_final') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-3">
                            <label class="col-sm-12 control-label" style="text-align: left;" >Minutos:</label>
                            <!--colocar fecha actual por defecto-->
                            <div class="col-md-12"> 
                                <input id="minutos" type="number" name="minutos" required autocomplete="minutos"  value="{{$horasExtra->minutos}}" autofocus class="form-control field-input {{ $errors->has('minutos') ? 'field-error' : '' }}">
                                @if ($errors->has('minutos'))<span class="error-message">{{ $errors->first('minutos') }}</span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-3"></div>
                        <div class="col-md-3">
                            <label class="col-sm-12 control-label" style="text-align: left;" >Mes:</label>
                            <div class="col-md-12">
                                <input id="mes" type="text" name="mes" required autocomplete="mes" autofocus value="{{$mes}}" disabled class="form-control field-input {{ $errors->has('mes') ? 'field-error' : '' }}">
                                @if ($errors->has('mes'))<span class="error-message">{{ $errors->first('mes') }}</span>
                                @endif
                                <input type="hidden" id="mesExtra" name="mesExtra" value="{{$mesExtra}}">
                            </div>
                        </div>
                        <div class="col-md-3">
                             <label class="col-sm-12 control-label" style="text-align: left;" >Año:</label>
                            <div class="col-md-12">
                                <input id="anio" type="number" name="anio" required autocomplete="minutos" autofocus value="{{$anio}}" disabled class="form-control field-input {{ $errors->has('anio') ? 'field-error' : '' }}">
                                @if ($errors->has('anio'))<span class="error-message">{{ $errors->first('anio') }}</span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="form-group"></div>
                    <div class="col-md-12 text-center">
                        <button type="submit" class="btn btn-primary">Editar</button>
                   <a href="{{route('home')}}" class="btn btn-default">Cancelar</a>
                    </div>
                </form>

            </div>
        </section>

</div>
</div>
<!-- end: page -->
</section>

@endsection

@section('js')

    <script type="text/javascript">


        $(document).ready(function() {
            function cargarEmpleado(){
                var oficina_id = $('#oficina_id').val();
                if($.trim('oficina_id') != ''){
                    $.get('/seleccionEmpleado', {oficina:  oficina_id}, function (empleados){
                        $('#empleado_id').empty();
                        $('#empleado_id').append("<option value=''>Selecciona Empleado </option>");
                        $.each(empleados, function(index, value){
                            $('#empleado_id').append("<option value='" + index +"'>"+value+ "</option>");
                        })

                    });
                }
            }
            cargarEmpleado();
            $('#oficina_id').on('change', cargarEmpleado);
        });

    </script>
@endsection