@extends('layouts.template')
@section('content')
<section role="main" class="content-body">
    <header class="page-header">
        <h2>Gestión de Permisos</h2>
        <div class="right-wrapper pull-right">
            <ol class="breadcrumbs">
                <li style="margin-left: -25%"><a href="{{route('permiso.empleado.listar')}}"><button class="btn btn-default"><span class="fa fa-chevron-left"> </span> Regresar </button></a></li>
            </ol>
        </div>
    </header>
   
    <!-- start: page -->

        <div class="col-lg-12">
            <div>
                @if ($errors->any())
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    
                    <div class="errors">
                        <ul>
                            @foreach ($errors->all() as $error)

                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif

                </div>
            </div>
             
            <section class="panel"> 
                <header class="panel-heading">
                    <div class="panel-actions">
                    </div>
                    <h2 class="panel-title">Consulta de Permisos por Empleados</h2>
                </header>
                <div class="panel-body">
                    <form class="form-horizontal form-bordered">
                        {{-- @csrf --}}
                        <div class="col-md-12">          
                            <div class="col-md-3"></div>
                            <div class="col-md-6">
                               <label class="col-md-12 control-label" for="w4-first-name" style="text-align: left;">Codigo Empleado</label>
                               <div class="col-md-12">
                                <select data-plugin-selectTwo id="codigo_id" class="form-control populate" name="codigo_id" required>
                                    <option value="" selected>Seleccione Empleado</option>
                                    @foreach($empleado as $empleado)
                                    <option value="{{$empleado->id_empleado}}">{{$empleado->codigo_empleado}} || {{$empleado->nombre}} {{$empleado->apellido}}</option>
                                    @endforeach

                                </select>
                                @if ($errors->has('codigo_id'))<span class="error-message">{{ $errors->first('codigo_id') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group"></div>  
                        </div> 

                    </form>
                </div>

            <div class="panel-body">
                <div class="col-md-12">
                    <div class="col-md-4">
                           <label class="col-md-12 control-label" >Año Actual:</label>
                           <div class="col-md-12">
                               <input id="anio_actual"  type="text" name="anio_actual" required  value="" disabled class="form-control field-input {{ $errors->has('numero_anios') ? 'field-error' : '' }}">
                               @if ($errors->has('anio_actual'))<span class="error-message">{{ $errors->first('anio_actual') }}</span>
                               @endif
                           </div>
                   </div>
                   <div class="col-md-4">                       
                    
                        <label class="col-sm-12 control-label" style="text-align: left;" >Codigo Empleado:</label>
                        <div class="col-md-12">
                            <input id="codigo_empleado" type="text" name="codigo_empleado" required disabled value="" class="form-control field-input {{ $errors->has('codigo_empleado') ? 'field-error' : '' }}" >
                            @if ($errors->has('codigo_empleado'))<span class="error-message">{{ $errors->first('codigo_empleado') }}</span>
                            @endif
                        </div> 
                </div>

                <div class="col-md-4">
                    
                        <label class="col-sm-12 control-label" style="text-align: left;" >Nombre</label>
                        <!--colocar fecha actual por defecto-->
                        <div class="col-md-12">
                            <input id="nombre_empleado" type="text" name="nombre_empleado" required value="" disabled class="form-control field-input {{ $errors->has('nombre_empleado') ? 'field-error' : '' }}">
                            @if ($errors->has('nombre_empleado'))<span class="error-message">{{ $errors->first('nombre_empleado') }}</span>
                            @endif
                        </div>
                </div>
            </div>
            <div class="form-group"></div>
            <div class="col-md-12">
                <div class="col-md-4">
                        <label class="col-sm-12 control-label" style="text-align: left;" >Centro:</label>
                        <!--colocar fecha actual por defecto-->
                        <div class="col-md-12">
                            <input id="centro" type="text" name="centro" required autocomplete="centro" autofocus placeholder="Centro" value="" disabled class="form-control field-input {{ $errors->has('centro') ? 'field-error' : '' }}">
                            @if ($errors->has('centro'))<span class="error-message">{{ $errors->first('centro') }}</span>
                            @endif
                        </div>
                   
                </div>
                <div class="col-md-4">
                    
                        <label class="col-sm-12 control-label" style="text-align: left;" >Unidad Administrativa:</label>
                        <!--colocar fecha actual por defecto-->
                        <div class="col-md-12">
                            <input id="unidad" type="text" name="unidad" required autocomplete="unidad" autofocus placeholder="Nombre Unidad" value="" disabled class="form-control field-input {{ $errors->has('unidad') ? 'field-error' : '' }}">
                            @if ($errors->has('unidad'))<span class="error-message">{{ $errors->first('unidad') }}</span>
                            @endif
                        </div>
                    
                </div>
                <div class="col-md-4">
                    
                        <label class="col-sm-12 control-label" style="text-align: left;" >Oficina:</label>
                        <!--colocar fecha actual por defecto-->
                        <div class="col-md-12">
                            <input id="oficina" type="text" name="oficina" required autocomplete="oficina" autofocus placeholder="Oficina" value="" disabled class="form-control field-input {{ $errors->has('oficina') ? 'field-error' : '' }}">
                            @if ($errors->has('oficina'))<span class="error-message">{{ $errors->first('oficina') }}</span>
                            @endif
                        </div>
                </div>
                

            </div>
               <div class="form-group"></div>   
            </div>
            
         
</section>
<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title">Listado de Permisos</h2>
    </header>
    <div class="panel-body">
        <div class="col-md-12">
        <div class="col-md-12">
            <table class="table table-bordered table-striped mb-none">
                <thead>
                    <tr>
                        <th>Codigo Acción</th>
                        <th>Descrición</th>
                        <th>Horas Permitidas</th>
                        <th>Horas Utilizadas</th>
                        <th>Horas Disponibles</th>
                    </tr>
                </thead>
                <tbody id="detalle_permiso">
                    
                </tbody>
            </table>
        </div> 
    </div>
</div>
</section>
<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title">Detalle de Permisos</h2>
    </header>
    <div class="panel-body">
        <div class="col-md-12">
        <div class="form-group"></div>
        <div class="col-md-12">
            <table class="table table-bordered table-striped mb-none">
                <thead>
                    <tr>
                        <th>Codigo Acción</th>
                        <th>Descrición</th>
                        <th>Fecha Emision</th>
                        <th>Fecha Desde</th>
                        <th>Hora Desde</th>
                        <th>Fecha Hasta</th>
                        <th>Hora Hasta</th>
                        <th>Horas Permiso</th>
                    </tr>
                </thead>
                <tbody id="detalle_permiso_acumulado">
                   
                    </tbody>
                    <tfoot id="t_foot">
                       
                    </tfoot>
                </table>
            </div>       

        </div>
    </div>
</section>
</div>
</div>
</div>
<!-- end: page -->
</section>

@endsection
@section('js')

<script type="text/javascript">
$(document).ready(function() {
    function cargarEmpleado(){
        var codigo_id = $('#codigo_id').val();
        if($.trim('codigo_id') != ''){
            $.get('/marcaciones/consultar/permisos', {codigo_id:  codigo_id}, function (data){
                //console.log(data);
                $("#anio_actual").val(data.empleado.fecha_actual);
                $("#codigo_empleado").val(data.empleado.codigo);
                $("#nombre_empleado").val(data.empleado.nombre);
                $("#centro").val(data.empleado.centro);
                $("#unidad").val(data.empleado.unidad);
                $("#oficina").val(data.empleado.oficina);

                $("#detalle_permiso").html("");
                for(var i=0; i<data.detalle.length; i++){
                    var tr = `<tr>
                    <td>`+data.detalle[i].codigo+`</td>
                    <td>`+data.detalle[i].descripcion+`</td>
                    <td>`+data.detalle[i].horas+`</td>
                    <td>`+data.detalle[i].horas_acumuladas+`</td>
                    <td>`+data.detalle[i].resta_horas+`</td>
                    </tr>`;
                    $("#detalle_permiso").append(tr);
                    }

                //console.log(data.detalleP);
                $("#detalle_permiso_acumulado").html("");
                for(var j=0; j<data.detalleP.length; j++){
                    var tr2 = `<tr>
                    <td>`+data.detalleP[j].codigo+`</td>
                    <td>`+data.detalleP[j].descripcion+`</td>
                    <td>`+data.detalleP[j].fecha_emision+`</td>
                    <td>`+data.detalleP[j].fecha_desde+`</td>
                    <td>`+data.detalleP[j].hora_inicio+`</td>
                    <td>`+data.detalleP[j].fecha_hasta+`</td>
                    <td>`+data.detalleP[j].hora_hasta+`</td>
                    <td>`+data.detalleP[j].horas_permiso+`</td>
                    </tr>`;
                    $("#detalle_permiso_acumulado").append(tr2);
                }

                $("#t_foot").html("");
                if(data.total.acumulado !=null){
                    var tr3 = `<tr>
                    <td colspan="7" >Total por permiso</td>
                    <td>`+data.total.acumulado+`</td>
                    </tr>`;
                    $("#t_foot").append(tr3);
                }
                
            });
        }
    }
    $('#codigo_id').on('change', cargarEmpleado);
});


</script>
@endsection