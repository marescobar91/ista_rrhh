@extends('layouts.template')
@section('content')
<section role="main" class="content-body">
    <header class="page-header">
        <h2>Gesti&oacute;n de Permisos de Empleados</h2>
        <div class="right-wrapper pull-right">
            <ol class="breadcrumbs">
                <li style="margin-left: -25%"><a href="{{url('/')}}"><button class="btn btn-default"><span class="fa fa-chevron-left"> </span> Regresar </button></a></li>
            </ol>
        </div>
    </header>

    <!-- start: page -->
    <div class="row">
        <div class="col-lg-12">
            <div>
                @if ($errors->any())
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    
                    <div class="errors">
                        <ul>
                            @foreach ($errors->all() as $error)

                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif

                </div>
            </div>
            <section class="panel">
                @include('flash::message')
                <header class="panel-heading">
                    <div class="panel-actions">
                       
                        @can('buscar.permisos.empleado')
                        <a href="{{ route('permiso.empleado.buscarPermiso') }}"><button class="btn btn-info"><span class="fa fa-search"></span> Consulta por Empleado </button></a>
                        @endcan
                        @can('crear.permisos.empleado')
                        <a href="{{route('permiso.empleado.buscar')}}"><button class="btn btn-primary"><span class="fa fa-plus"></span> Agregar Permiso a Empleado </button></a>
                        @endcan
                    </div>
                    <h2 class="panel-title">Listado de Permisos a Empleados</h2>
                </header>
                <div class="row">
                    <div class="col-md-12">

                        <div class="tabs">
                            <ul class="nav nav-tabs nav-justified">
                                <li class="active">
                                    <a href="#popular10" data-toggle="tab" class="text-center"><i class="fa fa-star"></i> Habilitados</a>
                                </li>
                                <li>
                                    <a href="#recent10" data-toggle="tab" class="text-center">Deshabilitados</a>
                                </li>
                            </ul>
                            <div class="tab-content">
                                <div id="popular10" class="tab-pane active">
                                    

                                   <div class="panel-body">
                                    <table class="table table-bordered table-striped mb-none" id="datatable-default">
                                        <thead>
                                            <tr>
                                                <th >Nombre del Empleado</th>
                                                <th >Tipo de Permiso</th>
                                                <th>Fecha Desde</th>
                                                <th>Fecha Hasta</th>
                                                <th>Horas Acumuladas</th>
                                                <th class="col-md-2">Accion</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                           @foreach($permiso as $permisos)
                                           <tr class="gradeX" >
                                            <td>{{$permisos->permisoEmpleado->nombre}} {{$permisos->permisoEmpleado->apellido}} </td>
                                            <td>{{$permisos->tipoPermiso->descripcion}}</td>
                                            <td>{{date('d-m-Y', strtotime($permisos->fecha_inicio))}}</td>
                                            <td>{{date('d-m-Y', strtotime($permisos->fecha_fin))}}</td>
                                            <td>{{ $permisos->horas_acumuladas }}</td> 
                                            <td class="text-center">
                                                @can('editar.permisos.empleado')
                                                <a href="{{route('permiso.empleado.editar', $permisos->id_permiso)}} " class="btn btn-warning" ><span class="fa fa-pencil"></span></a> 
                                                @endcan
                                                @can('eliminar.permisos.empleado')
                                                <a href="{{route('permiso.empleado.eliminar', $permisos->id_permiso)}}" class="btn btn-danger eliminar"  ><span class="fa fa-trash-o"></span></a>
                                                @endcan
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <div id="recent10" class="tab-pane">
                            <div class="panel-body">
                                <table class="table table-bordered table-striped mb-none" id="datatable-default">
                                    <thead>
                                        <tr>
                                            <th >Nombre del Empleado</th>
                                            <th >Tipo de Permiso</th>
                                            <th>Fecha Desde</th>
                                            <th>Fecha Hasta</th>
                                            <th>Horas Acumuladas</th>
                                            <th class="col-md-2">Accion</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                     @foreach($permisoDeshabilitados as $permisos)
                                     <tr class="gradeX" >

                                        <td>{{$permisos->permisoEmpleado->nombre}} {{$permisos->permisoEmpleado->apellido}}</td>
                                        <td>{{$permisos->tipoPermiso->descripcion}}</td>
                                        <td>{{date('d-m-Y', strtotime($permisos->fecha_inicio))}}</td>
                                        <td>{{date('d-m-Y', strtotime($permisos->fecha_fin))}}</td>
                                        <td>{{$permisos->horas_acumuladas}}</td>                           
                                        <td class="text-center col-md-2" >
                                            @can('habilitar.permisos.empleado')
                                            <a href="{{ route('permiso.empleado.habilitar', $permisos->id_permiso) }}" class="btn btn-primary habilitar"  ><span class="fa fa-caret-up"></span></a>
                                            @endcan
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
    <!-- end: page -->
</section>

@endsection
