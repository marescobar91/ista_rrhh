@extends('layouts.template')
@section('content')
<section role="main" class="content-body">
    <header class="page-header">
        <h2>Gestión de Permisos</h2>
        <div class="right-wrapper pull-right">
            <ol class="breadcrumbs">
                <li style="margin-left: -25%"><a href="{{route('permiso.empleado.listar')}}"><button class="btn btn-default"><span class="fa fa-chevron-left"> </span> Regresar </button></a></li>
            </ol>
        </div>
    </header>

    <!-- start: page -->
    <div class="row">
        <div class="col-lg-12">
            <div>
                @if ($errors->any())
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    
                    <div class="errors">
                        <ul>
                            @foreach ($errors->all() as $error)

                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif

                </div>
            </div>
           
            <section class="panel">
                <header class="panel-heading">
                    <div class="panel-actions">
                    </div>
                    <h2 class="panel-title">Editar de Permiso</h2>
                </header>
             
    </section> 
  
    <section class="panel">
        <form class="form-horizontal form-bordered" method="post" action="{{route('permiso.empleado.actualizar', $permiso->id_permiso)}}">
            @csrf
            {{method_field('PUT')}}
            <div class="panel-body">
            <div class="col-md-12">
                    <div class="col-md-3" style="margin-left: 1.3%"></div>
                    <div class="col-md-2">                       
                    <div class="form-group">
                        <label class="col-sm-12 control-label" style="text-align: left;" >Fecha Emisión:</label>
                        
                            <input id="fecha_registro" type="date"  name="fecha_registro" style="width: 90%" required autocomplete="fecha_registro" disabled="" autofocus class="form-control field-input {{ $errors->has('fecha_registro') ? 'field-error' : '' }}" value="{{$permiso->fecha_registro}}"> 
                            @if ($errors->has('fecha_registro'))<span class="error-message">{{ $errors->first('fecha_registro') }}</span>
                            @endif
                    </div>
                </div>
                    <div class="col-md-2">
                        <div class="form-group">

                           <label class="control-label" >Número años:</label>
                           
                            <input id="numero_anios" style="width: 92%" type="text" name="numero_anios" required autocomplete="numero_anios" value="{{$anios_trabajados}}" autofocus placeholder="Nombre del Cargo" disabled class="form-control field-input {{ $errors->has('numero_anios') ? 'field-error' : '' }}">
                            @if ($errors->has('numero_anios'))<span class="error-message">{{ $errors->first('numero_anios') }}</span>
                            @endif
                        

                    </div>  

                </div>
                <div class="col-md-2">                       
                    <div class="form-group">
                        <label class="col-sm-12 control-label" style="text-align: left;" >PIN:</label>
                        <div class="col-md-10">
                            <input id="pin_empleado" type="text" name="pin_empleado" required autocomplete="pin_empleado" autofocus placeholder="Nombre del Cargo" disabled value="{{$permiso->permisoEmpleado->pin_empleado}}" class="form-control field-input {{ $errors->has('pin_empleado') ? 'field-error' : '' }}" style="width: 120%; margin-left: -10%">
                            @if ($errors->has('pin_empleado'))<span class="error-message">{{ $errors->first('pin_empleado') }}</span>
                            @endif
                        </div> 
                        
                    </div>

                </div>
                
            </div>
            <div class="form-group"></div>
            <div class="col-md-12">
                <div class="col-md-3"></div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="col-sm-12 control-label" style="text-align: left;" >Se autoriza a:</label>
                        <!--colocar fecha actual por defecto-->
                        <div class="col-md-12">
                            <input id="nombre_empleado" type="text" name="nombre_empleado" required autocomplete="nombre_empleado" autofocus placeholder="Nombre del Empleado" value="{{$permiso->permisoEmpleado->nombre}} {{$permiso->permisoEmpleado->apellido}}" disabled class="form-control field-input {{ $errors->has('nombre_empleado') ? 'field-error' : '' }}">
                            @if ($errors->has('nombre_empleado'))<span class="error-message">{{ $errors->first('nombre_empleado') }}</span>
                            @endif
                        </div>
                    </div>
                    
                </div>
                
            </div>
            <div class="form-group"></div>
            <div class="col-md-12">
                <div class="col-md-3"></div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="col-sm-12 control-label" style="text-align: left;" >Gerencia:</label>
                        <!--colocar fecha actual por defecto-->
                        <div class="col-md-12">
                            <input id="oficina" type="text"  name="oficina" required autocomplete="oficina" autofocus placeholder="Nombre de la Oficina" value="{{$permiso->permisoEmpleado->oficina->oficina}}" disabled class="form-control field-input {{ $errors->has('oficina') ? 'field-error' : '' }}">
                            @if ($errors->has('oficina'))<span class="error-message">{{ $errors->first('oficina') }}</span>
                            @endif
                        </div>
                    </div>
                    
                </div>
                
            </div>
            <div class="form-group"></div>
            <div class="col-md-12">
                    <div class="col-md-3"></div>
                    <div class="col-md-3">
                        <div class="form-group">

                           <label class="col-sm-12 control-label" style="text-align: left;">Fecha Inicio:</label>
                           <div class="col-md-12">
                            <input id="fecha_inicio" type="date" name="fecha_inicio" required autocomplete="fecha_inicio" autofocus placeholder="Nombre del Cargo" class="form-control field-input {{ $errors->has('fecha_inicio') ? 'field-error' : '' }}" value="{{$permiso->fecha_inicio}}">
                            @if ($errors->has('fecha_inicio'))<span class="error-message">{{ $errors->first('fecha_inicio') }}</span>
                            @endif
                        </div>

                    </div>  

                </div>
                
                <div class="col-md-3">                       
                    <div class="form-group">
                        <label class="col-sm-12 control-label" style="text-align: left;" >Hora Inicio:</label>
                        <div class="col-md-12">
                            <input id="hora_inicio" type="time" name="hora_inicio" required autocomplete="hora_inicio" autofocus placeholder="Nombre del Cargo" class="form-control field-input {{ $errors->has('hora_inicio') ? 'field-error' : '' }}" value="{{$permiso->hora_inicio}}">
                            @if ($errors->has('hora_inicio'))<span class="error-message">{{ $errors->first('hora_inicio') }}</span>
                            @endif
                        </div>
                    </div>

                </div>
                
            </div>
            <div class="form-group"></div>
            <div class="col-md-12">
                    <div class="col-md-3"></div>
                <div class="col-md-3">                       
                    <div class="form-group">
                        <label class="col-sm-12 control-label" style="text-align: left;" >Fecha Final:</label>
                        <div class="col-md-12">
                            <input id="fecha_final" type="date" name="fecha_final" required autocomplete="fecha_final" autofocus placeholder="Nombre del Cargo" class="form-control field-input {{ $errors->has('fecha_final') ? 'field-error' : '' }}" value="{{$permiso->fecha_fin}}">
                            @if ($errors->has('fecha_final'))<span class="error-message">{{ $errors->first('fecha_final') }}</span>
                            @endif
                        </div>
                    </div>

                </div>
                
                <div class="col-md-3">                       
                    <div class="form-group">
                        <label class="col-sm-12 control-label" style="text-align: left;" >Hora Final:</label>
                        <!--colocar fecha actual por defecto-->
                        <div class="col-md-12">
                            <input id="hora_final" type="time" name="hora_final" required autocomplete="hora_final" autofocus placeholder="Nombre del Cargo" class="form-control field-input {{ $errors->has('hora_final') ? 'field-error' : '' }}" value="{{$permiso->hora_fin}}">
                            @if ($errors->has('hora_final'))<span class="error-message">{{ $errors->first('hora_final') }}</span>
                            @endif
                        </div>
                    </div>

                </div>
            </div>
            <div class="form-group"></div>
            <div class="col-md-12">
                <div class="col-md-3"></div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="col-sm-12 control-label" style="text-align: left;" >Motivo de permiso:</label>
                        <!--colocar fecha actual por defecto-->
                        <div class="col-md-12">
                            <textarea id="motivo_permiso" rows="3" class="form-control field-input" name="motivo_permiso"  required >{{$permiso->motivo_permiso}}</textarea>
                            @if ($errors->has('motivo_permiso'))<span class="error-message">{{ $errors->first('motivo_permiso') }}</span>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group"></div>
            <div class="col-md-12">
                <div class="col-md-3"></div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="col-sm-12 control-label" style="text-align: left;" >Tipo de Permiso:</label>
                        <!--colocar fecha actual por defecto-->
                        <div class="col-md-12">
                            <select data-plugin-selectTwo class="form-control populate" name="tipo_permiso"> 
                             @foreach ($tipoPermiso as $tipo)
                                <option value="{{$tipo->id_tipo_permiso}}" @if($tipo->id_tipo_permiso==$permiso->tipo_permiso_id)
                                    selected='selected' @endif>{{$tipo->descripcion}} </option>
                                   
                            @endforeach
                        </select>
                        </div>
                    </div>      
                </div>
                <div class="col-md-1">
                     <label class="col-sm-12 control-label" style="text-align: left; color: white" >.</label>
                    <a href="#modalBasic" class="mb-xs mr-xs modal-basic btn btn-info"><span class="fa fa-eye"></span></a>
                    <div class="modal-block mfp-hide" id="modalBasic">
                        <section class="panel">
                            <header class="panel-heading">
                                <h2 class="panel-title">Permisos</h2>
                            </header>
                            <div class="panel-body">
                                <div class="modal-wrapper">
                                    <div class="modal-text">
                                        <p>Detalle de Permiso</p>
                                        <table class="table table-bordered table-striped mb-none">
                                            <thead>
                                                <tr>
                                                    <th>Codigo Acción</th>
                                                    <th>Descrición</th>
                                                    <th>Horas Permitidas</th>
                                                    <th>Horas Utilizadas</th>
                                                    <th>Horas Disponibles</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($permisos as $permisos)
                                                <tr>
                                                   <td>{{$permisos->codigo_permiso}}</td>
                                                   <td>{{$permisos->descripcion}}</td>
                                                   <td>{{$permisos->numero_horas}}</td>
                                                   <td>{{$permisos->horas_acumuladas}}</td>
                                                    <td>{{$permisos->resta_hora}}</td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <footer class="panel-footer">
                                <div class="row">
                                    <div class="col-md-12 text-center">
                                        <button class="btn btn-danger modal-dismiss"> Cerrar</button>
                                    </div>
                                </div>
                            </footer>
                        </section>
                    </div>
                </div>     
            </div>  
            <div class="form-group"></div>       
            <div class="col-lg-12 text-center" >    
               <button type="submit" class="btn btn-primary">Guardar</button>
               <a href="{{route('home')}}" class="btn btn-default">Cancelar</a>
           </div>                              
       </form>
   </div>
</section>

</div>
</div>
<!-- end: page -->
</section>

@endsection