@extends('layouts.template')
@section('content')
<section role="main" class="content-body">
    <header class="page-header">
        <h2>Gestión de Permisos</h2>
        <div class="right-wrapper pull-right">
            <ol class="breadcrumbs">
                <li style="margin-left: -25%"><a href="{{route('permiso.empleado.listar')}}"><button class="btn btn-default"><span class="fa fa-chevron-left"> </span> Regresar </button></a></li>
            </ol>
        </div>
    </header>

    <!-- start: page -->
    <div class="row">
        <div class="col-lg-12">
            <div>
                @if ($errors->any())
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    
                    <div class="errors">
                        <ul>
                            @foreach ($errors->all() as $error)

                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif

                </div>
            </div>
            @include('flash::message')
            <section class="panel">
                <header class="panel-heading">
                    <div class="panel-actions">
                    </div>
                    <h2 class="panel-title">Ingreso de Permiso</h2>
                </header>
                <div class="panel-body">
                    <form class="form-horizontal form-bordered">
                        @csrf
                        <div class="col-md-12">
                            <div class="col-md-2"></div>
                            <div class="col-md-8">
                                <label class="col-md-12 control-label" for="w4-first-name" style="text-align: left;">Codigo Empleado</label>
                                <div class="col-md-12">
                                    <select data-plugin-selectTwo id="codigo_id" class="form-control populate" name="codigo_id" required>
                                        <option value="" selected>Digite codigo</option>
                                        @foreach($empleado as $empleado)
                                        <option value="{{$empleado->id_empleado}}">{{$empleado->codigo_empleado}} || {{$empleado->nombre}} {{$empleado->apellido}}</option>
                                        @endforeach

                                    </select>
                                    @if ($errors->has('codigo_id'))<span class="error-message">{{ $errors->first('codigo_id') }}</span>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </form>
                </div>

                <form class="form-horizontal form-bordered" action="{{ route('permiso.empleado.guardar') }}" method="post">
                     @csrf

                    <input id="id_empleado" name="id_empleado" type="hidden" value="">
                    <input id="id_codigo_tipo_permiso" name="id_codigo_tipo_permiso" type="hidden" value="">
                    <div class="panel-body">
                        <div class="col-md-12">
                            <div class="col-md-2"></div>
                            <div class="col-md-3">                       
                                    <label class="col-sm-12 control-label" style="text-align: left;" >Fecha Emisión:</label>
                                    <div class="col-md-12">
                                    <input id="fecha_registro" type="date" name="fecha_registro" required autocomplete="fecha_registro" autofocus class="form-control field-input {{ $errors->has('fecha_registro') ? 'field-error' : '' }}" value=""> 
                                    @if ($errors->has('fecha_registro'))<span class="error-message">{{ $errors->first('fecha_registro') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-3">
                                 <label class="control-label" >Número años:</label>
                                 <div class="col-md-12">
                                 <input id="numero_anios"  type="text" name="numero_anios" required autocomplete="numero_anios" value="" autofocus placeholder="Numero Años" disabled class="form-control field-input {{ $errors->has('numero_anios') ? 'field-error' : '' }}">
                                 @if ($errors->has('numero_anios'))<span class="error-message">{{ $errors->first('numero_anios') }}</span>
                                 @endif
                           </div>
                         </div>
                         <div class="col-md-2">                       
                          
                                <label class="col-md-12 control-label" style="text-align: left;" >PIN:</label>
                                <div class="col-md-12">
                                <input id="pin_empleado" type="text" name="pin_empleado" required autocomplete="pin_empleado" autofocus placeholder="Pin Empleado" disabled value="" class="form-control field-input {{ $errors->has('pin_empleado') ? 'field-error' : '' }}" >
                                @if ($errors->has('pin_empleado'))<span class="error-message">{{ $errors->first('pin_empleado') }}</span>
                                @endif
                                </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-2"></div>
                        <div class="col-md-8">
                                <label class="col-sm-12 control-label" style="text-align: left;" >Se autoriza a:</label>
                                <!--colocar fecha actual por defecto-->
                                <div class="col-md-12">
                                    <input id="nombre_empleado" type="text" name="nombre_empleado" required autocomplete="nombre_empleado" autofocus placeholder="Nombre del Empleado" value="" disabled class="form-control field-input {{ $errors->has('nombre_empleado') ? 'field-error' : '' }}">
                                    @if ($errors->has('nombre_empleado'))<span class="error-message">{{ $errors->first('nombre_empleado') }}</span>
                                    @endif
                                </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-2"></div>
                        <div class="col-md-8">
                                <label class="col-sm-12 control-label" style="text-align: left;" >Gerencia:</label>
                                <!--colocar fecha actual por defecto-->
                                <div class="col-md-12">
                                    <input id="oficina" type="text"  name="oficina" required autocomplete="oficina" autofocus placeholder="Nombre de la Oficina" value="" disabled class="form-control field-input {{ $errors->has('oficina') ? 'field-error' : '' }}">
                                    @if ($errors->has('oficina'))<span class="error-message">{{ $errors->first('oficina') }}</span>
                                    @endif
                                </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-2"></div>
                        <div class="col-md-3">
                             <label class="col-sm-12 control-label" style="text-align: left;">Fecha Inicio:</label>
                             <div class="col-md-12">
                                <input id="fecha_inicio" type="date" name="fecha_inicio" required autocomplete="fecha_inicio" autofocus placeholder="Nombre del Cargo" class="form-control field-input {{ $errors->has('fecha_inicio') ? 'field-error' : '' }}">
                                @if ($errors->has('fecha_inicio'))<span class="error-message">{{ $errors->first('fecha_inicio') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-3">                       
                                <label class="col-sm-12 control-label" style="text-align: left;" >Hora Inicio:</label>
                                <div class="col-md-12">
                                    <input id="hora_inicio" type="time" name="hora_inicio" required autocomplete="hora_inicio" autofocus placeholder="Nombre del Cargo" class="form-control field-input {{ $errors->has('hora_inicio') ? 'field-error' : '' }}">
                                    @if ($errors->has('hora_inicio'))<span class="error-message">{{ $errors->first('hora_inicio') }}</span>
                                    @endif
                                </div>
                        </div>
                    </div>
                <div class="col-md-12">
                    <div class="col-md-2"></div>
                    <div class="col-md-3">                       
                            <label class="col-sm-12 control-label" style="text-align: left;" >Fecha Final:</label>
                            <div class="col-md-12">
                                <input id="fecha_final" type="date" name="fecha_final" required autocomplete="fecha_final" autofocus placeholder="Nombre del Cargo" class="form-control field-input {{ $errors->has('fecha_final') ? 'field-error' : '' }}">
                                @if ($errors->has('fecha_final'))<span class="error-message">{{ $errors->first('fecha_final') }}</span>
                                @endif
                            </div>
                    </div>
                    <div class="col-md-3">                       
                            <label class="col-sm-12 control-label" style="text-align: left;" >Hora Final:</label>
                            <!--colocar fecha actual por defecto-->
                            <div class="col-md-12">
                                <input id="hora_final" type="time" name="hora_final" required autocomplete="hora_final" autofocus placeholder="Nombre del Cargo" class="form-control field-input {{ $errors->has('hora_final') ? 'field-error' : '' }}">
                                @if ($errors->has('hora_final'))<span class="error-message">{{ $errors->first('hora_final') }}</span>
                                @endif
                            </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="col-md-2"></div>
                    <div class="col-md-8">
                        
                            <label class="col-sm-12 control-label" style="text-align: left;" >Motivo de permiso:</label>
                            <!--colocar fecha actual por defecto-->
                            <div class="col-md-12">
                                <textarea id="motivo_permiso" rows="3" class="form-control field-input" name="motivo_permiso" value="" required ></textarea>
                                @if ($errors->has('motivo_permiso'))<span class="error-message">{{ $errors->first('motivo_permiso') }}</span>
                                @endif
                            </div>
                    </div>
            </div>
            <div class="col-md-12">
                <div class="col-md-2"></div>
               
                    <div class="col-md-2">
                        <label class="col-md-12 control-label" style="text-align: left;" >Tipo Permiso:</label>
                        <!--colocar fecha actual por defecto-->
                            <div class="col-md-12">
                            <select data-plugin-selectTwo class="form-control populate" name="tipo_permiso" id="tipo_permiso" required>
                            </select>
                            </div>
                    </div>
                    <div class="col-md-6">
                        <label class="col-md-12 control-label" style="text-align: left;" >Descripción:</label>
                            <div class="col-md-12">
                                <textarea id="permiso_descripcion" name="permiso_descripcion"  autofocus placeholder="Descripcion" class="form-control" autocomplete="permiso_descripcion" rows="2"> </textarea>
                            </div>
                    </div>  
                       
                </div>
                 
                    <div class="form-group"></div>       
                    <div class="col-lg-12 text-center" >    
                     <button type="submit" class="btn btn-primary">Guardar</button>
                     <a href="{{route('home')}}" class="btn btn-default">Cancelar</a>
                 </div>                              
             </form>
         </div>
     </section>
     <section class="panel">
        <header class="panel-heading">
            <h2 class="panel-title">Detalle de Permisos</h2>
        </header>
        <div class="panel-body">
            <div class="modal-wrapper">
                <div class="modal-text">
                    <table class="table table-bordered table-striped mb-none" id="datatable-default">
                        <thead>
                            <tr>
                                <th>Codigo Acción</th>
                                <th>Descrición</th>
                                <th>Horas Permitidas</th>
                                <th>Horas Utilizadas</th>
                                <th>Horas Disponibles</th>
                            </tr>
                        </thead>
                        <tbody id="cuerpo">
                        </tbody>
                     </table>
                 </div>
             </div>
         </div>
     </section>

 </div>
</div>
<!-- end: page -->
</section>

@endsection

@section('js')

<script type="text/javascript">


        $(document).ready(function() {
            function cargarEmpleado(){
                var codigo_id = $('#codigo_id').val();
                if($.trim('codigo_id') != ''){
                    $.get('/permiso/empleado/crear', {codigo_id:  codigo_id}, function (data){
                        $("#id_empleado").val(data.empleado.id);
                        $("#pin_empleado").val(data.empleado.pin);
                        $("#fecha_registro").val(data.empleado.fecha_actual);
                        $("#numero_anios").val(data.empleado.anios);
                        $("#nombre_empleado").val(data.empleado.nombre);
                        $("#oficina").val(data.empleado.oficina);

                        $('#tipo_permiso').empty();
                        $('#permiso_descripcion').val("");
                        $('#tipo_permiso').append("<option value=''>Codigo Permiso </option>");
                        $.each(data.permisos, function(index, value){
                            $('#tipo_permiso').append("<option value='" + index +"'>"+value+ "</option>");
                        })

                        $("#cuerpo").html("");
                        for(var i=0; i<data.detalle.length; i++){
                            var tr = `<tr>
                            <td>`+data.detalle[i].codigo+`</td>
                            <td>`+data.detalle[i].descripcion+`</td>
                            <td>`+data.detalle[i].horas+`</td>
                            <td>`+data.detalle[i].horas_acumuladas+`</td>
                            <td>`+data.detalle[i].resta_horas+`</td>
                            </tr>`;
                            $("#cuerpo").append(tr);
                        }
                    });
                }
            }
            $('#codigo_id').on('change', cargarEmpleado);

            $('#tipo_permiso').on('change', function(){
                var tipo_permiso = $('#tipo_permiso').val();
                if($.trim('tipo_permiso') != ''){
                    $.get('/getCodigoTipoPermiso', {tipo_permiso:  tipo_permiso}, function (data){
                        $('#permiso_descripcion').val(data.descripcion);
                        $('#id_codigo_tipo_permiso').val(data.id);
                    });
                }
            });
        });

    </script>
    @endsection