<table>
    <thead>
        <tr class="encabezado">
            <th>Codigo</th>
            <th>Nombre del Empleado</th>
            <th>Unidad ADM</th>
            <th>Fecha</th>
            <th>Contrato</th>
            <th>Salario</th>
            <th>Mes</th>
            <th>Horas</th>
            <th>Minutos</th>
        </tr>
    </thead>
    <tbody>
     @foreach($llegadaTarde as $key => $marc)
     <!--Aqui va el forech-->
   
     <tr>
       
        <td class="celda">{{$marc[0]}}</td>
        <td class="celda">{{$marc[1]}} {{$marc[2]}}</td>
        <td class="celda">{{$marc[4]}}</td>
        <td class="celda">{{date('d-m-Y', strtotime($marc[7]))}}</td>
        <td class="celda">{{$marc[5]}}</td>
        <td class="celda">{{$marc[6]}}</td>
        <td class="celda">{{$marc[9]}}</td>
        <td class="celda">{{date('H', strtotime($marc[8]))}}</td>
        <td class="celda">{{date('i', strtotime($marc[8]))}}</td>
     
    </tr>
    
    @endforeach
    <!--Fin de for-->
    
</tbody>
</table>
<br/>
<br/>