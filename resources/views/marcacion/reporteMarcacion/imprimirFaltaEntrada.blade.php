<!DOCTYPE html>

<head>
    <title>ISTA - RRHH</title>
    <link href="css/style.css" rel="stylesheet">
    <style>
        @page { margin: 75px 50px; } 
        .celda{
            border-bottom: lightgrey 0.1em solid;
        }
        .encabezado{
            border-bottom: grey 0.1em solid;
            background-color: grey;
            color: white;
        }
        .subencabezado{
            padding-top: 1%;
            background-color: #ece8e8;"
        }
        #footer { position: fixed; left: 0px; bottom: -150px; right: 33px; height: 150px; } 
        #footer .page:after { content: counter(page, PAGE_NUM); } 
 </style>
</head>
<body>
    <div id="wrapper" style="margin-top: -8%">
        <!-- Page Content -->
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="container" style="margin-left: 20px">

                    <div class="col-md-9 col-md-offset-1">
                        <div class="table-title" style="background-color: #fff; margin-bottom: 1%">
                            <table>
                                <td style="width: 120px;">
                                 <div class="row" align="center">
                                    <img src="{{public_path().'/img/Logo-Institucional.svg'}}" width="100%" align="left" style="padding-top: 2%">
                                </div> 
                            </td>
                            <td style="width: 720px" >
                                <h5 style="text-transform: uppercase; margin-bottom:-2.5%; margin-left: 200px">Instituto Salvadoreño de Transformación Agraria</h5>
                                <h5 style="text-transform: uppercase; margin-bottom:-2.5%; margin-left: 330px">Sección de Permisos</h5>
                                <h5 style="text-transform: uppercase; margin-bottom:-2.5%; margin-left: 230px">Reporte de Personal con Minutos Descuento</h5>
                                <h5 style="text-transform: uppercase;margin-bottom:-2.5%;margin-left: 270px">Tipo de Descuento: Falta Marc (Ent)</h5>
                                <h5 style="margin-bottom:-2.5%; margin-left: 220px"> <b>Desde  : </b> {{date('j', strtotime($fecha_inicio))}} de {{ $mesIni }} del {{date('Y', strtotime($fecha_inicio)) }}   <b> Hasta : </b> {{date('j', strtotime($fecha_final))}} de {{ $mesFin }} del {{date('Y', strtotime($fecha_final))}} </h5>
                            </td>
                            <td>
                                <div align="center">
                                    <br/>
                                    <h5>Fecha:{{$dia}}</h5> 
                                    <h5>Hora: {{$hora}}</h5>
                                </div>
                            </td>
                        </table>
                    </div>

                    <div class="row">
                        
                    <table class="table table-striped table-hover col-md-5" id="reporte" style="background: #fff;" width="97%">
                        <thead>
                            <tr class="encabezado">

                                <th>Codigo</th>
                                <th>Nombre del Empleado</th>
                                <th>Unidad ADM</th>
                                <th>Fecha</th>
                                <th>Hora</th>
                                <th>DESC</th>
        
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($marcacionFaltaEnt as $marc)
                    @foreach($marcacionFalta as $marcacion)

                    <!--Aqui va el forech-->
                    @if($marc->id_empleado==$marcacion->empleado && $marc->fecha_marcacion == $marcacion->fecha)

                    <tr class="gradeX" >
                      {{-- @if($marc->hora >='12:00:00') --}}
                      <td class="celda">{{$marc->codigo_empleado}}</td>
                      <td class="celda">{{$marc->nombre}} {{$marc->apellido}}</td>
                      <td class="celda">{{$marc->oficina}}</td>
                      <td class="celda">{{date('d-m-Y', strtotime($marc->fecha_marcacion))}}</td>
                      <td class="celda">{{$marc->hora_marcacion}}</td>
                      <td class="celda">04:00:00</td>
                      {{-- @endif --}}

                  </tr>
                  @endif

                  <!--Fin de for-->

                  @endforeach
                  @endforeach
                        </tbody>

                    </table>
                    <br/>
                    <br/>
                    
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
<div id="footer"> 
    <p class="page" align="right">Página <?php $PAGE_NUM ?></p> 
    </div>  
<footer>
    <table>
        <tr>
            <td style="text-align: left; width: 60%">Sistema Informatico para el manejo de Marcaciones y Viaticos de la Gerencia de Recursos Humanos del ISTA</td>
            <td></td>
            <td></td>
        </tr>
    </table>
</footer>
</body>
</html>

