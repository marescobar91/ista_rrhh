@extends('layouts.template')
@section('content')
<section role="main" class="content-body">
    <header class="page-header">
        <h2>Reportes de Marcaciones</h2>
        <div class="right-wrapper pull-right">
            <ol class="breadcrumbs">
                <li style="margin-left: -25%"><a href="{{route('historialM.listar')}}"><button class="btn btn-default"><span class="fa fa-chevron-left"> </span> Regresar </button></a></li>
            </ol>
        </div>
    </header>

    <!-- start: page -->
    <div class="row">
        <div class="col-lg-12">
            <div>
                @if ($errors->any())
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    
                    <div class="errors">
                        <ul>
                            @foreach ($errors->all() as $error)

                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif

                </div>
            </div>
            <section class="panel">
                <header class="panel-heading">
                    <div class="panel-actions">
                    </div>
                    <h2 class="panel-title">Listado de Reportes de Marcaciones</h2>
                </header>
                <div class="panel-body ">
                  
                    <div class="col-md-4" >
                        <section class="panel panel-primary">
                            <header class="panel-heading">
                                 <a href="{{route('marcaciones.buscar.llegadaTarde')}}"><h2 class="panel-title">Llegada Tarde</h2></a>     
                                 <br/>
                            </header>
                        </section>
                        <section class="panel panel-primary">
                            <header class="panel-heading">
                                <a href="{{ route('marcaciones.buscar.salidaTemprana') }}"><h2 class="panel-title">  Salida Temprana</h2></a>
                                <br/>
                            </header>
                        </section>
                       <!--- <section class="panel panel-primary">
                            <header class="panel-heading">
                                <a href="{{ route('marcaciones.reporte.descuento.buscar') }}"><h2 class="panel-title"> Descuentos</h2></a>
                                <br/>
                            </header>
                        </section>-->

                    </div>
                    <div class="col-md-4" >
                        <section class="panel panel-primary">
                            <header class="panel-heading">
                                <a href="{{route('marcaciones.buscar.faltaEntrada')}}"><h2 class="panel-title"> Falta Marcacion de Entrada</h2></a>
                                <br/>
                            </header>
                        </section>
                        <section class="panel panel-primary">
                            <header class="panel-heading">
                                <a href="{{ route('marcaciones.buscar.faltaSalida') }}"><h2 class="panel-title">Falta Marcacion de Salida</h2></a>
                                <br/>
                            </header>
                        </section>

                        <section class="panel panel-primary">
                            <header class="panel-heading">
                                <a href="{{route('buscar.permiso.empleado')}}"><h2 class="panel-title">Permiso sin Goce de Sueldo</h2></a>
                                <br/>
                            </header>
                        </section>
                         
                    </div>
                    <div class="col-md-4" >
                        <section class="panel panel-primary"> 
                            <header class="panel-heading">
                                <a href="{{route('marcaciones.buscar.ausencia')}}"><h2 class="panel-title"> Ausencia</h2></a>
                                <br/>
                            </header>
                        </section>
                        <section class="panel panel-primary">
                            <header class="panel-heading">
                                <a href="{{ route('marcaciones.buscar.intermedia') }}"><h2 class="panel-title"> Intermedias</h2></a>
                                <br/>
                            </header>
                        </section>
                        
                    </div>
                   
                </div>
            </section>
            

        </div>
    </div>
    <!-- end: page -->
</section>

@endsection