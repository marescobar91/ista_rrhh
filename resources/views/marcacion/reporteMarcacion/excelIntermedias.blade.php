<table>
    <thead>
        <tr class="encabezado">
            <th>Codigo</th>
            <th>Nombre del Empleado</th>
            <th>Unidad ADM</th>
            <th>Fecha</th>
            <th>Contrato</th>
            <th>Salario</th>
            <th>Mes</th>
            <th>Salida Intermedia</th>
            <th>Entrada Intermedia</th>
            <th>Horas</th>
            <th>Minutos</th>
        </tr>
    </thead>
    <tbody>
        @foreach($intermediaMin as $key => $min)
          @foreach($intermediaMax as $key => $max)

                  
     @if($min[0] == $max[0] && $min[4] == $max[4])
      
        <tr>
            <!--Aqui va el forech-->
    
          
          <td class="celda">{{$min[0]}}</td>
          <td class="celda">{{$min[1]}} {{$min[2]}}</td>
          <td class="celda">{{$min[3]}}</td>
          <td class="celda">{{date('d-m-Y', strtotime($min[4]))}}</td>
          <td class="celda">{{$min[5]}}</td>
          <td class="celda">{{$min[6]}}</td>
          <td class="celda">{{$min[7]}}</td>
          <td class="celda">{{date('H:i:s', strtotime($min[8]))}}</td>
          <td class="celda">{{date('H:i:s', strtotime($max[8]))}}</td>
          <td class="celda">{{date('H',strtotime($max[8]) - strtotime($min[8]))}}</td>
          <td class="celda">{{date('i',strtotime($max[8]) - strtotime($min[8]))}}</td>
          
          

      </tr>

            @endif

      <!--Fin de for-->

      @endforeach
      @endforeach
</tbody>
</table>
<br/>
<br/>