@extends('layouts.template')
@section('content')
<section role="main" class="content-body">
  <header class="page-header">
    <h2>Reporte de Marcaciones: Descuentos</h2>
    <div class="right-wrapper pull-right">
            <ol class="breadcrumbs">
                <li style="margin-left: -25%"><a href="{{route('marcaciones.verReportes')}}"><button class="btn btn-default"><span class="fa fa-chevron-left"> </span> Regresar </button></a></li>
            </ol>
        </div>
  </header>
  <!-- start: page -->
  <div class="row">
    <div class="col-lg-12">

      @if ($errors->any())
      <div class="alert alert-danger">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

        <div class="errors">
          <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
          </ul>
        </div>
        @endif

      </div>
    </div>
    @if($vacio == null)
    <section class="panel">
      @include('flash::message') 
      <header class="panel-heading">
        <h2 class="panel-title">Buscar por Fecha</h2>
      </header>
      <div class="row">
        <div class="col-md-12">
         <div class="panel-body">

        
            <div class="col-md-2"></div>
            <div class="col-md-8">

              <form method="post" action="{{route('marcaciones.reporte.excel.descuentoImp')}}" role="form" class="contactForm" enctype="multipart/form-data" target="_blank">
                {{ csrf_field() }}
                <div class="col-md-1"></div>
                <div class="col-md-10">
                  <input type="file" name="reporteImp" class="form-control "> 
                </div>
                <div class="form-group"></div>
                <div class="col-md-1"></div>
                <div class="col-md-5">
                  <label class="col-md-12 control-label " for="w4-first-name">Fecha Inicio: </label>
                  <div class="col-md-12">

                    <input type="date"  class="form-control field-input" name="fecha_inicio" id="fecha_inicio"  style="background: white" placeholder="dd/mm/yyyy"> 
                  </div>
                </div>
                <div class="col-md-5">
                  <label class="col-md-12 control-label " for="w4-first-name">Fecha Final: </label>
                  <div class="col-md-12">

                    <input type="date"  class="form-control field-input" name="fecha_final" id="fecha_final"  style="background: white" placeholder="dd/mm/yyyy">
                  </div>
                </div>
                <div class="form-group"></div>
                <div class="col-md-1"></div>
                <div class="col-md-10">
                  <label class="col-md-8 control-label" style="text-align: left" for="w4-first-name">Listado de Reporte: </label>
                  <div class="col-md-12">
                    <select name="tipoReporte" id="tipoReporte"  data-plugin-selectTwo class="form-control populate" required>
                      <option value="">Seleccionar Reporte</option>
                      <option value="Ausencia">Ausencia</option>
                      <option value="Intermedia">Intermedia</option>
                      <option value="Llegada Tarde">Llegada Tarde</option>
                      <option value="Salida Temprano">Salida Temprano</option>
                      <option value="Falta Entrada">Falta Entrada</option>
                      <option value="Falta Salida">Falta Salida</option>
                    </select>
                  </div>
                </div>
                <div class="form-group"></div>
                <div class="text-center">
                  <button type="submit" class="btn btn-default" style="">Importar</button>
                </div>


              </form> 
            </div>
            <!--Boton de Generar Reporte-->
            <div class="form-group"></div>

    
     </div>
   </div>
 </div>
</section>
@endif
@if($vacio != null)
<section class="panel">
  <header class="panel-heading">
    <div class="panel-actions">
      @can('reporte.marcaciones')
      <div class="form" style="align-content: center;">
         <form action="{{ route('marcaciones.reporte.descuento.pdf')}}" method="post" role="form" class="contactForm" target="_blank">
           {{ csrf_field() }}
           <div class="col-md-12">
            @if($vacio != null)
            <input id="fecha_inicio" type="hidden" class="form-control" value="{{$fecha_inicio}}" name="fecha_inicio">
            <input id="fecha_final" type="hidden" class="form-control" value="{{$fecha_final}}" name="fecha_final">
            <input type="hidden" name="tipoReporte" value="{{$tipoReporte}}" id="tipoReporte">
          

            <button class="btn btn-info"><span class="fa fa-print"></span></button>
            <a href="{{route('marcaciones.reporte.descuento.buscar')}}"><p class="btn btn-success"><span class="fa fa-refresh"></span></p></a>                    
          </div>
          @endif
        </form>
    @endcan
  </div>


</div>
<h2 class="panel-title">Reporte de Marcaciones: Descuentos</h2>
</header>
<div class="row">
  <div class="col-md-12"> 
    <div class="panel-body">
      <div class="col-md-2">
        <img src="{{asset('img/Logo-Institucional.svg')}}" width="100%" style="padding-top: 10%">
      </div>
      <div class="col-md-8">
        <h5 class="text-center" style="text-transform: uppercase;padding-bottom: 0.75%">Instituto Salvadoreño de Transformación Agraria</h5>
        <h5 class="text-center" style="text-transform: uppercase; padding-bottom: 0.75%">Sección de Permisos</h5>
        <h5 class="text-center" style="text-transform: uppercase; padding-bottom: 0.75%">Reporte de Personal con Minutos Descuento</h5>
        <h5 class="text-center" style="text-transform: uppercase;padding-bottom: 0.75%">Tipo de Reporte: {{$tipoReporte}}</h5>
        <h5 class="text-center"> <b>Desde  : </b> {{date('j', strtotime($fecha_inicio))}} de {{ $mesIni }} del {{date('Y', strtotime($fecha_inicio))}} <b>  Hasta : </b> {{date('j', strtotime($fecha_final))}} de {{ $mesFin }} del {{date('Y', strtotime($fecha_final))}} </h5>
      </div>
      <div class="col-md-2">
        <br/>
        <h5 class="text-center">Fecha: {{ $day}} </h5>
        <h5 class="text-center">Hora: {{ $hora }} </h5>
      </div>
      <table class="table table-bordered table-striped mb-none">
        <thead>
          <tr>

            <th>Nombre</th>
            <th>Tipo Plaza</th>
            <th>Salario</th>
            <th>Horas</th>
            <th>Minutos</th>
            <th>DESC</th>
          </tr>
        </thead>

        <tbody> 
          
          @foreach($descuentoArray as $key => $desc)
          <!--Aqui va el forech-->
          <tr class="gradeX">
            <td>{{$desc[1]}}</td>
            <td>{{$desc[2]}}</td>
            <td>{{$desc[3]}}</td>
            <td>{{$desc[4]}}</td>
            <td>{{$desc[5]}}</td>
            <td>${{$desc[6]}}</td>
          </tr>
          <!--Fin de for-->
          @endforeach
        </tbody>
      </table>
    </div>
  </div>
</div>

</section>



</div>
</div>
@endif
<!-- end: page -->
</section>

@endsection
