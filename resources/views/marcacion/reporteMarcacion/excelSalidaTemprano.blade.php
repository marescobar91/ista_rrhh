<table>
    <thead>
        <tr class="encabezado">
            <th>Codigo</th>
            <th>Nombre del Empleado</th>
            <th>Unidad ADM</th>
            <th>Fecha</th>
            <th>Contrato</th>
            <th>Salario</th>
            <th>Mes</th>
            <th>Horas</th>
            <th>Minutos</th>
        </tr>
    </thead>
    <tbody>
     @foreach($marcacionMax as $marc)
     <!--Aqui va el forech-->
   
     <tr>
        <td class="celda">{{$marc->codigo_empleado}}</td>
        <td class="celda">{{$marc->nombre}} {{$marc->apellido}}</td>
        <td class="celda">{{$marc->oficina}}</td>
        <td class="celda">{{date('d-m-Y', strtotime($marc->fecha_marcacion))}}</td>
        <td class="celda">{{$marc->tipo}}</td>
        <td class="celda">{{$marc->salario}}</td>
        <td class="celda">{{$marc->mes}}</td>
        <td class="celda">{{date('H', strtotime($marc->minutos))}}</td>
        <td class="celda">{{date('i', strtotime($marc->minutos))}}</td>

    </tr>
    
    @endforeach
    <!--Fin de for-->
    
</tbody>
</table>
<br/>
<br/>
