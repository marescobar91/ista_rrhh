@extends('layouts.template')
@section('content')
<section role="main" class="content-body">
  <header class="page-header">
    <h2>Reporte de Marcaciones: Llegadas Tarde</h2>
    <div class="right-wrapper pull-right">
            <ol class="breadcrumbs">
                <li style="margin-left: -25%"><a href="{{route('marcaciones.verReportes')}}"><button class="btn btn-default"><span class="fa fa-chevron-left"> </span> Regresar </button></a></li>
            </ol>
        </div>
  </header>
  <!-- start: page -->
  <div class="row">
    <div class="col-lg-12">

      @if ($errors->any())
      <div class="alert alert-danger">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

        <div class="errors">
          <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
          </ul>
        </div>
        @endif

      </div>
    </div>
    @if($vacio == null)
    <section class="panel">
      @include('flash::message') 
      <header class="panel-heading">
        <h2 class="panel-title">Buscar por Fecha</h2>
      </header>
      <div class="row">
        <div class="col-md-12">
         <div class="panel-body">

           <form class="form-horizontal" method="POST" action="{{ route('marcaciones.llegadaTarde') }}" >
            {{ csrf_field() }} 


            <!-- fecha inicio-->
            <div class="form-group">
              <div class="col-md-1"></div>
              <label class="col-md-2 control-label text-right" for="w4-first-name">Fecha Inicio: </label>
              <div class="col-md-2">
                <div class="input-group">
                  <input type="date"  class="form-control field-input" name="fecha_inicio" id="fecha_inicio"  style="background: white" placeholder="dd/mm/yyyy"> 

                </div>
              </div>
              <!-- fecha inicio-->
              <label class="col-md-2 control-label text-right" for="w4-first-name">Fecha Final: </label>
              <div class="col-md-2">
                <div class="input-group">
                  <input type="date"  class="form-control field-input" name="fecha_final" id="fecha_final"  style="background: white" placeholder="dd/mm/yyyy"> 

                </div>
              </div>
            </div>


            <!--Boton de Generar Reporte-->
            <div class="form-group" align="center">
             <button type="submit" class="btn btn-success">
              Generar
            </button>
          </div>

        </form> 

      </div>
    </div>
  </div>
</section>
@endif
@if($vacio != null)
<section class="panel">
  <header class="panel-heading">
    <div class="panel-actions">
      <div class="col-md-4"></div>
      <div class="col-md-8">
        <div class="col-md-3">
          <form action="{{ route('marcaciones.descuento.llegadaTarde')}}" method="post" role="form" class="contactForm" target="_blank">
           {{ csrf_field() }}
           
            @if($vacio != null)
            <input id="fecha_ini" type="hidden" class="form-control" value="{{$fecha_inicio}}" name="fecha_ini">
            <input id="fecha_fin" type="hidden" class="form-control" value="{{$fecha_final}}" name="fecha_fin">
            <button class="btn btn-success"><span class="fa fa-dollar"></span></button>                    
          @endif
        </form> 
      </div>
        <div class="col-md-3">
          <form action="{{ route('marcaciones.reporte.excel.llegadaTarde')}}" method="post" role="form" class="contactForm">
           {{ csrf_field() }}
           
            @if($vacio != null)
            <input id="fecha_ini" type="hidden" class="form-control" value="{{$fecha_inicio}}" name="fecha_ini">
            <input id="fecha_fin" type="hidden" class="form-control" value="{{$fecha_final}}" name="fecha_fin">
            <button class="btn btn-default"><span class="fa fa-file-excel-o"></span></button>                    
          @endif
        </form> 
      </div>
      <div class="col-md-3">
        @can('reporte.marcaciones')
        <form action="{{ route('marcaciones.reporte.llegadaTarde')}}" method="post" role="form" class="contactForm" target="_blank">
         {{ csrf_field() }}
       
          @if($vacio != null)
          <input id="fecha_ini" type="hidden" class="form-control" value="{{$fecha_inicio}}" name="fecha_ini">
          <input id="fecha_fin" type="hidden" class="form-control" value="{{$fecha_final}}" name="fecha_fin">

          <button class="btn btn-info"><span class="fa fa-print"></span></button>

        @endif
      </form>
      @endcan
    </div>
    <div class="col-md-3">
      <a href="{{route('marcaciones.buscar.llegadaTarde')}}"><p class="btn btn-default"><span class="fa fa-refresh"></span></p></a>
    </div>

  </div>




</div>
<h2 class="panel-title">Reporte de Marcaciones: Llegadas Tarde</h2>
</header>
<div class="row">
  <div class="col-md-12"> 
    <div class="panel-body">
      <div class="col-md-2">
        <img src="{{asset('img/Logo-Institucional.svg')}}" width="100%" style="padding-top: 10%">
      </div>
      <div class="col-md-8">
        <h5 class="text-center" style="text-transform: uppercase;padding-bottom: 0.75%">Instituto Salvadoreño de Transformación Agraria</h5>
        <h5 class="text-center" style="text-transform: uppercase; padding-bottom: 0.75%">Sección de Permisos</h5>
        <h5 class="text-center" style="text-transform: uppercase; padding-bottom: 0.75%">Reporte de Personal con Minutos Descuento</h5>
        <h5 class="text-center" style="text-transform: uppercase;padding-bottom: 0.75%">Tipo de Descuento: Llegadas Tarde</h5>
        <h5 class="text-center"> <b>Desde  : </b> {{date('j', strtotime($fecha_inicio))}} de {{ $mesIni }} del {{date('Y', strtotime($fecha_inicio))}} <b>  Hasta : </b> {{date('j', strtotime($fecha_final))}} de {{ $mesFin }} del {{date('Y', strtotime($fecha_final))}} </h5>
      </div>
      <div class="col-md-2">
        <br/>
        <h5 class="text-center">Fecha: {{ $dias }} </h5>
        <h5 class="text-center">Hora: {{ $hora }}</h5>
      </div>
      <table class="table table-bordered table-striped mb-none">
        <thead>
          <tr>
            <th>Codigo</th>
            <th>Nombre del Empleado</th>
            <th>Unidad</th>
            <th>Oficina</th>

            <th>Fecha</th>

            <th>Horas</th>
          </tr>
        </thead>

        <tbody> 

          @foreach($llegadaTarde as $key =>$marc)

          <tr class="gradeX" >

            
            <td>{{$marc[0]}}</td>

            <td>{{$marc[1]}} {{ $marc[2]}}</td>
            <td>{{ $marc[3]}}</td>
            <td>{{ $marc[4]}}</td>
            <td>{{date('d-m-Y', strtotime($marc[7]))}}</td>

            <td>{{$marc[8]}}</td>
          

          </tr>


          <!--Fin de for-->
          @endforeach

        </tbody>
      </table>
    </div>
  </div>
</div>

</section>

</div>
</div>
@endif
<!-- end: page -->
</section>

@endsection
