@extends('layouts.template')
@section('content')
<section role="main" class="content-body">
    <header class="page-header">
        <h2>Gestión de Marcaciones</h2>
        <div class="right-wrapper pull-right">
            <ol class="breadcrumbs">
                <li style="margin-left: -25%"><a href="{{url('/')}}"><button class="btn btn-default"><span class="fa fa-chevron-left"> </span> Regresar </button></a></li>
            </ol>
        </div>
    </header>
    <!-- start: page -->
    <div class="row">
        <div class="col-lg-12">
            <div>
                @if ($errors->any())
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    
                    <div class="errors">
                        <ul>
                            @foreach ($errors->all() as $error)

                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif

                </div>
            </div>
            <section class="panel">
                @include('flash::message')
                
                <header class="panel-heading">
                    <div class="panel-actions">
                        @can('reporte.marcaciones')
                        <a href="{{route('marcaciones.verReportes')}}"><button class="btn btn-success"><span class="fa fa-file-pdf-o"></span> Reportes </button></a>
                        @endcan
                        @can('consultar.marcaciones')
                        <a href="{{route('marcaciones.buscar')}}"><button class="btn btn-info"><span class="fa fa-search"></span> Consulta de Marcaciones </button></a>
                        @endcan

                    </div>
                    <h2 class="panel-title">Listado de Historial de Marcaciones</h2>
                </header>
                <div class="row">
                    <div class="col-md-12">

                       <div class="panel-body">
                        <table class="table table-bordered table-striped mb-none" id="datatable-default">
                            <thead>
                                <tr>
                                    <th>Fecha de Importación</th>
                                    <th>Hora de Importación</th>
                                    <th>N° de registros</th>
                                  
                                </tr>
                            </thead>
                            <tbody>
                               @foreach($historial as $historial)
                               <tr class="gradeX" >
                                    <td>{{date('d-m-Y', strtotime($historial->fecha_importar))}}</td>
                                    <td>{{$historial->hora_importar}}</td>
                                    <td>{{$historial->cantidad_registros}}</td>
                                    
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
<!-- end: page -->
</section>

@endsection
