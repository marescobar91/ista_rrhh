@extends('layouts.template')
@section('content')
<section role="main" class="content-body">
    <header class="page-header">
        <h2>Gesti&oacute;n de Asuetos</h2>
        <div class="right-wrapper pull-right">
            <ol class="breadcrumbs">
                <li style="margin-left: -25%"><a href="{{url('/')}}"><button class="btn btn-default"><span class="fa fa-chevron-left"> </span> Regresar </button></a></li>
            </ol>
        </div>
    </header>

    <!-- start: page -->
    <div class="row">
        <div class="col-lg-12">
            <div>
                @if ($errors->any())
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    
                    <div class="errors">
                        <ul>
                            @foreach ($errors->all() as $error)

                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif

                </div>
            </div>
            <section class="panel">
                @include('flash::message')
                <header class="panel-heading">
                    <div class="panel-actions">
                        @can('crear.asuetos')
                        <a href="{{route('asuetos.crear')}}"><button class="btn btn-primary"><span class="fa fa-plus"></span> Agregar Asuetos </button></a>
                        @endcan
                        @can('cargar.asuetos')
                        <a href="{{route('asuetos.generar')}}"><button class="btn btn-success"><span class="fa fa-refresh"></span> Generar Asuetos </button></a>
                        @endcan
                    </div>
                    <h2 class="panel-title">Listado de Asuetos</h2>
                </header>
                <div class="row">
                    <div class="col-md-12">

                       <div class="panel-body">
                        <table class="table table-bordered table-striped mb-none" id="datatable-default">
                            <thead>
                                <tr>
                                    <th ></th>
                                    <th >Nombre del Asuetos</th>
                                    <th >Fecha del Asuetos</th>
                                    <th> Dia de Asueto</th>
                                    <th class="col-md-2">Accion</th>
                                </tr>
                            </thead>
                            <tbody>
                               @foreach($asueto as $asuetos)
                               <tr class="gradeX" >
                                <td></td>
                                <td>{{$asuetos->asueto}}</td>
                                <td>{{date('d-m-Y', strtotime($asuetos->fecha))}}</td>
                                <td>@php
                                            if (date('l', strtotime($asuetos->fecha))=='Sunday')
                                            {echo 'Domingo';}
                                        elseif (date('l', strtotime($asuetos->fecha))=='Monday'){
                                        {echo 'Lunes';}
                                        } elseif (date('l', strtotime($asuetos->fecha))=='Tuesday'){
                                        {echo 'Martes';}
                                        } elseif (date('l', strtotime($asuetos->fecha))=='Wednesday'){
                                        {echo 'Miércoles';}
                                        } elseif (date('l', strtotime($asuetos->fecha))=='Thursday'){
                                        {echo 'Jueves';}
                                        } elseif (date('l', strtotime($asuetos->fecha))=='Friday'){
                                        {echo 'Viernes';}
                                        } elseif (date('l', strtotime($asuetos->fecha))=='Saturday'){
                                        {echo 'Sábado';}
                                        }
                                        @endphp</td>
                                <td class="text-center">
                                    @can('editar.asuetos')
                                    <a href="{{ route('asuetos.editar', $asuetos->id_asueto)}}" class="btn btn-warning" ><span class="fa fa-pencil"></span></a>
                                    @endcan
                                    @can('eliminar.asuetos')
                                    <a href="{{ route('asuetos.eliminar', $asuetos->id_asueto)}}" class="btn btn-danger eliminar"  ><span class="fa fa-trash-o"></span></a>
                                    @endcan
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
<!-- end: page -->
</section>

@endsection
