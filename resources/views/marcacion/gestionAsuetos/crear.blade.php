@extends('layouts.template')
@section('content')
<section role="main" class="content-body">
    <header class="page-header">
        <h2>Gestión de Asuetos</h2>
        <div class="right-wrapper pull-right">
            <ol class="breadcrumbs">
                <li style="margin-left: -25%"><a href="{{route('asuetos.listar')}}"><button class="btn btn-default"><span class="fa fa-chevron-left"> </span> Regresar </button></a></li>
            </ol>
        </div>
    </header>

    <!-- start: page -->
    <div class="row">
        <div class="col-lg-12">
            <div>
                @if ($errors->any())
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    
                    <div class="errors">
                        <ul>
                            @foreach ($errors->all() as $error)

                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif

                </div>
            </div>
            <section class="panel">
                <header class="panel-heading">
                    <div class="panel-actions">
                    </div>
                    <h2 class="panel-title">Crear Asuetos</h2>
                </header>
                <div class="panel-body">
                    <form class="form-horizontal form-bordered" method="POST" action="{{route('asuetos.guardar')}}">
                        @csrf
                        <div class="col-md-2"></div>
                        <div class="col-md-8">
                            <div class="form-group">
                                
                                 <label class="col-sm-12 control-label" style="text-align: left;">Nombre Asueto</label>
                                <div class="col-md-12">
                                    <input id="asueto" type="text" value="{{ old('asueto') }}" name="asueto" required autocomplete="asueto" autofocus placeholder="Nombre del Asueto" class="form-control field-input {{ $errors->has('asueto') ? 'field-error' : '' }}">
                                    @if ($errors->has('asueto'))<span class="error-message">{{ $errors->first('asueto') }}</span>
                                    @endif
                                </div>
                            </div>                         
                            <div class="form-group">
                                
                                 <label class="col-sm-12 control-label" style="text-align: left;">Fecha de Asueto</label>
                                <div class="col-md-12">
                                   
                                        <input type="date" class="form-control field-input {{ $errors->has('fecha') ? 'field-error' : '' }}" name="fecha" id="fecha"  > 
                                        @if ($errors->has('fecha'))<span class="error-message">{{ $errors->first('fecha') }}</span>
                                            @endif 
                                      
                                    
                                </div>
                            </div> 
                        </div>
                        <div class="form-group">
                            
                        </div>
     
                        
                        <div class="col-lg-12 text-center" >    
                         <button type="submit" class="btn btn-primary">Guardar</button>
                         <a href="{{route('home')}}" class="btn btn-default">Cancelar</a>
                     </div>                              
                 </form>
             </div>
         </section>
         
     </div>
 </div>
 <!-- end: page -->
</section>

@endsection