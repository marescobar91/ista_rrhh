@extends('layouts.app')

@section('content')
<link href="{{asset('css/login.css') }}" rel="stylesheet" id="bootstrap-css">
    <script src="{{asset('//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js')}}"></script> 
<div class="wrapper fadeInDown" style="padding-top: 3%" >
    <div id="formContent">
        
                        <div class="fadeIn first" style="border-bottom: #000000 2px solid; margin-right: 5%;margin-left: 5%">
                            <h1 style="padding-top: 3%; padding-bottom: 2%">Restablecer Contraseña</h1>
                            </div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form method="POST" action="{{ route('password.email') }}">
                        @csrf

                        <div class="form-group row">
                        
                            <div class="col-md-12" >
                                <input id="email" type="text" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required  autofocus placeholder="Correo Electronico">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-12">
                                <input type="submit" class="fadeIn fourth" value="Enviar Contraseña">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
