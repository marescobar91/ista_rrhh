@extends('layouts.app')

@section('content')
<link href="{{asset('css/login.css') }}" rel="stylesheet" id="bootstrap-css">
    <script src="{{asset('//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js')}}"></script> 
@guest

<div class="wrapper fadeInDown" style="padding-top: 3%" >
    <div class="col-md-6">
                                 @if ($errors->any())
                                                <div class="errors">
                                                    <ul>
                                                        @foreach ($errors->all() as $error)
                                                            <li>{{ $error }}</li>
                                                        @endforeach
                                                    </ul>
                                                </div>
                                            @endif
                            </div>
    <div id="formContent">
            
                        <div class="fadeIn first" style="border-bottom: #000000 2px solid; margin-right: 5%;margin-left: 5%">
                            <h1 style="padding-top: 3%; padding-bottom: 2%">Iniciar Sesion</h1>
                            </div>
                             
                        <div class="card-body">
                            <form method="POST" action="{{ route('iniciarSesion') }}">
                                @csrf
                                
                                <div class="form-group row">
                                    <div class="col-md-12">
                                        <input id="nombre_usuario" type="text" name="nombre_usuario"placeholder="Nombre de Usuario" class="field-input {{ $errors->has('nombre_usuario') ? 'field-error' : '' }}" required>
                                       @if ($errors->has('nombre_usuario'))
                                            <span class="error-message">{{ $errors->first('nombre_usuario') }}</span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-md-12">
                                        <input id="password" type="password" name="password" placeholder="Contraseña" class="field-input {{ $errors->has('password') ? 'field-error' : '' }}" required>
                                       @if ($errors->has('password'))
                                            <span class="error-message">{{ $errors->first('password') }}</span>
                                        @endif
                                    </div>
                                </div>
                               <div class="form-group row mb-0"> 
                                    <div>
                                        <input type="submit" class="fadeIn fourth" value="Iniciar Sesion">
                                            <a class="btn btn-link" href="{{route('password.request')}}">
                                                {{ __('Olvide mi contraseña') }}
                                            </a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    
                    </div>
                </div>
@endguest
@endsection
