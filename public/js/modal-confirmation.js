   $('.eliminar').on('click', function (event) {
    event.preventDefault();
    const url = $(this).attr('href');
    swal({
        title: '¿Esta seguro de eliminar?',
        text: 'El registro sera eliminado/deshabilitado',
        icon: 'error',
        buttons: ["Cancelar","Aceptar"],
    }).then(function(value) {
        if (value) {
            window.location.href = url;
        }
    });
});
    $('.habilitar').on('click', function (event) {
    event.preventDefault();
    const url = $(this).attr('href');
    swal({
        title: '¿Esta seguro de habilitar?',
        text: 'El registro sera habilitado',
        icon: 'warning',
        buttons: ["Cancelar","Aceptar"],
    }).then(function(value) {
        if (value) {
            window.location.href = url;
        }
    });
});
       $('#cerrar').submit(function (event) {
    event.preventDefault();
    url = $(this).attr('action');
    swal({
        title: '¿Esta seguro de cerrar?',
        text: 'El registro sera cerrado',
        icon: 'warning',
        buttons: ["Cancelar","Aceptar"],
    }).then(function(value) {
        if (value) {
            window.location.href = url;
        }
    });
});
          $('.abrir').on('click', function (event) {
    event.preventDefault();
    const url = $(this).attr('action');
    swal({
        title: '¿Esta seguro de abrir?',
        text: 'El registro sera abierto',
        icon: 'warning',
        buttons: ["Cancelar","Aceptar"],
    }).then(function(value) {
        if (value) {
            window.location.href = url;
        }
    });
});
    