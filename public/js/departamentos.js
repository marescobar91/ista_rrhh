function cargarDepartamentos() {
    var array = ["San Salvador", "La Libertad", "Santa Ana"];
    array.sort();
    addOptions("departamento", array);
}


//Función para agregar opciones a un <select>.
function addOptions(domElement, array) {
    var selector = document.getElementsByName(domElement)[0];
    for (departamento in array) {
        var opcion = document.createElement("option");
        opcion.text = array[departamento];
        // Añadimos un value a los option para hacer mas facil escoger los pueblos
        opcion.value = array[departamento].toLowerCase()
        selector.add(opcion);
    }
}
function cargarMunicipios() {
    // Objeto de departamentos con municipios
    var listaMunicipios = {
      1: ["Apopa", "Ciudad Delgado", "Soyapango", "San Salvador", "Ilopango"],
      2: ["Santa Tecla", "Antiguo Cuzcatlan", "Quezaltepeque"],
      3: ["Santa Ana", "Metapan", "San Juan Opico"],
    }
    
    var departamentos = document.getElementById('departamento')
    var municipios = document.getElementById('municipio')
    var departamentoSeleccionado = departamentos.value
    
    // Se limpian los municipios
    municipios.innerHTML = '<option value="">Seleccione un Municipio...</option>'
    
    if(departamentoSeleccionado !== ''){
      // Se seleccionan los municipios y se ordenan
      departamentoSeleccionado = listaMunicipios[departamentoSeleccionado]
      departamentoSeleccionado.sort()
    
      // Insertamos los municipios
      departamentoSeleccionado.forEach(function(municipio){
        let opcion = document.createElement('option')
        opcion.value = municipio
        opcion.text = municipio
        municipios.add(opcion)
      });
      }
    
  }
  
 // Iniciar la carga de provincias solo para comprobar que funciona
cargarDepartamentos();