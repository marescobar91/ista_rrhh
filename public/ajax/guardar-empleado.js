<script type="text/javascript">
    $(document).ready(function()
    {
        // boton de registrar
        $('#btn-guardar-empleado').click(function(event) {
            event.preventDefault();
            var dataString = $('#formulario-empleado').serialize(); // carga todos los campos para enviarlos
            // AJAX
            $.ajax({
                type: "POST",
                url: "{{route('empleados.guardar')}}",
                data: dataString,
            });
        });
    }); 

</script>